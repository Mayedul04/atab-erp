﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class SettingsController : Controller
    {
        // GET: Membership/Settings
        private readonly SettingsFacade _facade = new SettingsFacade();
        private readonly EnumFacade _enumFacade = new EnumFacade();
        public ActionResult Association()
        {
            return View();
        }
        public JsonResult SaveAssociation(AssociationDto dto)
        {
            try
            {
                var result = _facade.SaveAssociation(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetAssociations()
        {
            var data = _facade.GetAssociations();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Chamber()
        {
            return View();
        }
        public JsonResult SaveChamber(ChamberDto dto)
        {
            try
            {
                var result = _facade.SaveChamber(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetChambers()
        {
            var data = _facade.GetChambers();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DocumentType()
        {
            return View();
        }
        public JsonResult SaveDocumentType(DocumentTypeDto dto)
        {
            try
            {
                var result = _facade.SaveDocumentType(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetDocumentTypes()
        {
            var data = _facade.GetDocumentTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeeType()
        {
            return View();
        }

        public JsonResult SaveFeeType(FeeTypeDto dto)
        {
            try
            {
                var result = _facade.SaveFeeType(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetFeeTypes()
        {
            var data = _facade.GetFeeTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BusinessCategory()
        {
            return View();
        }

        public JsonResult SaveBusinessCategory(BusinessCategoryDto dto)
        {
            try
            {
                var result = _facade.SaveBusinessCategory(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetBusinessCategories()
        {
            var data = _facade.GetBusinessCategories();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCountryList()
        {
            var data = _facade.GetCountryList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDivisionList(long countryId)
        {
            var data = _facade.GetDivisionList(countryId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDistrictList(long divisionId)
        {
            var data = _facade.GetDistrictList(divisionId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetThanaList(long districtId)
        {
            var data = _facade.GetThanaList(districtId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAreaList(long thanaId)
        {
            var data = _facade.GetAreaList(thanaId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOwnershipStatusList()
        {
            var data = _enumFacade.GetOwnershipStatus();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetZoneList()
        {
            var data = _enumFacade.GetZoneList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPosts()
        {
            var data = _enumFacade.GetPosts();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCommitteeTypes()
        {
            var data = _enumFacade.GetCommitteeTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }//GetCommitteeTypes
        public JsonResult GetBusinessCategoryList()
        {
            var data = _facade.GetBusinessCategoryList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChamberList()
        {
            var data = _facade.GetChamberList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAssociationList()
        {
            var data = _facade.GetAssociationList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AreaEntry()
        {
            return View();
        }


        public JsonResult GetAreas()
        {
            var data = _facade.GetAreas();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public string GetThanaListByDistrict(long distId)
        {
            List<ThanaDto> list = _facade.GetThanaListByDistrict(distId);
            string strret = "<select><option>Choose one...</option>";
            foreach (ThanaDto item in list)
            {
                strret += "<option value='" + item.Id + "'>" + item.ThanaNameEng + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public JsonResult SaveArea(AreaDto dto)
        {
            try
            {
                var result = _facade.SaveArea(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }

        }


        #region Certificate Register
        public ActionResult CertificateRegister()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }
        public JsonResult GetCertificateTypes(long? companyid, long? courseid)
        {
            var data = _enumFacade.GetCertificateTypes(companyid, courseid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveCertificateRegister(CertificateRegisterDto dto)
        {
            var result = _facade.SaveCertificateRegister(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CertificateRegisterStatusSummary()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }
        public JsonResult GetCertificateRegisterStatusSummary(long? CompanyProfileId, CertificateType? certype)
        {
            var result = _facade.GetCertificateBookStatusSummary(CompanyProfileId, certype);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCertificateRegisterList()
        {
            var banks = _facade.GetCertificateRegisterList();

            return Json(banks, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}