﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{

    
    public class StudentsController : Controller
    {
        private readonly EnumFacade _enum = new EnumFacade();
        private readonly TraineeFacade _facade = new TraineeFacade();
        //private readonly CourseFeeCollectionFacade _coursefacade = new CourseFeeCollectionFacade();
        // GET: Membership/Students

        public ActionResult Entry()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GeStudentListForAutoFill(AutofillDto dto)
        {
            var result = _facade.GetStudentListForAutoFill(dto.prefix, dto.exclusionList);
            var data = result.Select(r => new { key = r.Id, value = r.Name }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllCourses()
        {
            var result = _facade.GetAllCourses();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllCoursesExceptOtherFees()
        {
            var result = _facade.GetAllCoursesExceptOtherFees();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllGender()
        {
            var result = _enum.GetGenderList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllReligions()
        {
            var result = _enum.GetReligionList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllSourceOfInformation()
        {
            var result = _enum.GetSourceOfInformationList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentInformation(long id)
        {
            var result = _facade.GetStudentInformation(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveTrainee(TraineeDto dto)
        {
            ResponseDto response = new ResponseDto();
            var result = _facade.SaveTrainee(dto, (long)SessionHelper.UserProfile.SelectedCompanyId, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
           
        }

        [HttpPost]
        public JsonResult TraineeAdmission(TraineeDto dto)
        {
            DateTime admissionDate = DateTime.Now.Date;
            var admissionDateConverted = DateTime.TryParseExact(dto.DateOfAdmissionText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out admissionDate);
            
            var success = _facade.TraineeAdmission(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveAddress(TraineeDto dto)
        {
            var success = _facade.SaveAddress(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCourses(TraineeDto dto)
        {
            var success = _facade.SaveCourses(dto, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult SaveImages(TraineeFilesDto dto)
        {
            var success = _facade.SaveImages(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PendingApplication(string sortOrder, string currentFilter, string searchString,  Course? course, int page = 1, int pageSize = 20)
        {
            //string fromDate, string toDate,
            //DateTime FromDate = DateTime.MinValue;
            //DateTime ToDate = DateTime.MaxValue;
            //DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            //var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            //if (!toDateConverted)
            //    ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (course != null)
                ViewBag.Course = (int)course;
            ViewBag.Courses = _facade.GetAllCoursesExceptOtherFees();
            //ViewBag.FromDateText = fromDate;
            //ViewBag.ToDateText = toDate;
            var applications = _facade.PendingTraineeApplicationList(pageSize, page, searchString,course);

            return View(applications);
        }

        public ActionResult ActiveStudents(string sortOrder, string currentFilter, string searchString, string batch,  Course? course, int page = 1, int pageSize = 20)
        {
            //string fromDate, string toDate,
            //DateTime FromDate = DateTime.MinValue;
            //DateTime ToDate = DateTime.MaxValue;
            //DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            //var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            //if (!toDateConverted)
            //    ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.Batch = batch;
            ViewBag.CurrentSort = sortOrder;
            if (course != null)
                ViewBag.Course = (int)course;
            ViewBag.Courses = _facade.GetAllCoursesExceptOtherFees();
            //ViewBag.FromDateText = fromDate;
            //ViewBag.ToDateText = toDate;
            var students = _facade.ActiveStudentList(pageSize, page, searchString,batch, course);
            return View(students);
        }

        public ActionResult TraineeApplictionReport(long traineeid,string reportTypeId)
        {
            var data = _facade.GetStudentInformation(traineeid);
            List<TraineeDto> basics = new List<TraineeDto>();
            basics.Add(data);
            var contact = data.Address;
            List<TraineeAddressDto> contacts = new List<TraineeAddressDto>();
            contacts.Add(contact);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptTraineeAdmissionForm.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("Trainee", basics);
            ReportDataSource rd1 = new ReportDataSource("TraineeAddress", contacts);
            ReportDataSource rd2 = new ReportDataSource("TraineeCourses", data.Courses);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);

            lr.EnableHyperlinks = true;
            lr.EnableExternalImages = true;

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        #region Certificate Issue to Trainee
        public ActionResult CertificatePrint()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAvailableCertificateNo(AutofillDto dto, long bookid)
        {
            var chequeList = _facade.GetCertificteListForAutoFill(dto.prefix, dto.exclusionList, bookid);
            return Json(chequeList.Select(c => new { key = c.CertificateNo, value = c.Id }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCertificateBooks(CertificateType type)
        {
            var success = _facade.GetBooks(type);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTraineeCourses(long trid)
        {
            var success = _facade.GetTraineeCourses(trid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCertificate(CertificateRegisterDetailDto dto)
        {
            var success = _facade.SaveCertificate(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CertificateList(long traineeid)
        {
            var success = _facade.GetMembersCirtificates(traineeid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CertificatebyId(long cerid)
        {
            var success = _facade.GetCertificate(cerid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStudentCertificate(string reportTypeId, long certificateid)
        {
            string path;
            string deviceInfo;
            CertificateRegisterDetailDto info = new CertificateRegisterDetailDto();
            var data = _facade.GetTraineeCertificateforPrint(certificateid);
            info = data.Where(x => x.Id == certificateid).FirstOrDefault();
            LocalReport lr = new LocalReport();
            
            path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptStudentCertificate.rdlc");

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("StudentCertificate", data);

            lr.DataSources.Add(rd);
            lr.EnableExternalImages = true;


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10.8in</PageWidth>" +
                "  <PageHeight>8.51in</PageHeight>" +
                "  <MarginTop>0</MarginTop>" +
                "  <MarginLeft>0</MarginLeft>" +
                "  <MarginRight>0</MarginRight>" +
                "  <MarginBottom>0</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        #endregion
    }
}