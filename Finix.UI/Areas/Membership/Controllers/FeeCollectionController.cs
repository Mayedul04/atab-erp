﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.Accounts.Dto;
using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Microsoft.Reporting.WebForms;
using Finix.Membership.Infrastructure;
using Finix.Auth.Facade;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class FeeCollectionController : Controller 
    {
        private readonly FeeCollectionFacade _facade = new FeeCollectionFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        private readonly CourseFeeCollectionFacade _tfacade = new CourseFeeCollectionFacade();
        public readonly MembeshipAccountFacade _memberAccounts = new MembeshipAccountFacade();
        private readonly MembeshipAccountFacade _accounts = new MembeshipAccountFacade();
        private readonly EnumFacade _enum = new EnumFacade();
        public ActionResult PendingFeeCollection(string sortOrder, string currentFilter, string searchString,  long? btype, Zone? zone, int page = 1, int pageSize = 20)
        {
            //string fromDate, string toDate,
            //DateTime FromDate = DateTime.MinValue;
            //DateTime ToDate = DateTime.MaxValue;
            //DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            //var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            //if (!toDateConverted)
            //    ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (btype != null)
                ViewBag.BType = (long)btype;
            ViewBag.BTypes = _facade.GetAllCategories();
            if (zone != null)
                ViewBag.Zone = (long)zone;
            ViewBag.Zones = _enum.GetZoneList();
            //ViewBag.FromDateText = fromDate;
            //ViewBag.ToDateText = toDate;
            var students = _facade.PendingFeeCollection( pageSize, page, searchString, btype,zone);
            return View(students);
        }

        public ActionResult NewMemberPendingFees(string sortOrder, string currentFilter, string searchString, string fromDate, string toDate, long? btype, Zone? zone, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            if (btype != null)
                ViewBag.BType = (long)btype;
            ViewBag.BTypes = _facade.GetAllCategories();
            if (zone != null)
                ViewBag.Zone = (long)zone;
            ViewBag.Zones = _enum.GetZoneList();
            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
            var students = _facade.NewMemberPendingFees(FromDate, ToDate, pageSize, page, searchString, btype, zone);
            return View(students);
        }

        public JsonResult GetAllAreas()
        {
            var data = _facade.GetAllAreas();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllCategories()
        {
            var data = _facade.GetAllCategories();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMembersOfPendingFees(string memberName, string memberNo, long? catId, long? areaId, string fromDate, string toDate)
        {
            DateTime from = DateTime.Now.Date;
            DateTime end = DateTime.Now.Date;

            var requiredDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out from);
            var requiredEndDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out end);
            var data = _facade.GetMembersOfPendingFees(memberName, memberNo, catId, areaId, from, end);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MemberFeeCollection()
        {
            return View();
        }
        public JsonResult GetFeePendingMemberWise(long? memberId)
        {
            var data = _facade.GetFeePendingMemberWise(memberId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMemberDetails(long? memberId, long? memberFeeId)
        {
            var data = _facade.GetMemberDetails(memberId, memberFeeId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //GetPaymentTypes SaveFeePAyment
        public JsonResult GetPaymentTypes()
        {
            var data = _facade.GetPaymentTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBankAccHeads()
        {
            var data = _memberAccounts.GetAccHeads(AccountHeadRefType.CashAtBank, (long)SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveFeePayment(MemberFeeCollectionDto dto)
        {
            try
            {
               
                ResponseDto response = new ResponseDto();
                foreach (var dt in dto.FeeCollectionTrans)
                {
                    if (!string.IsNullOrEmpty(dt.SubscriptionDateTxt))
                    {
                        DateTime offerLetterDate = DateTime.Now;
                        var offerLetterConverted = DateTime.TryParseExact(dt.SubscriptionDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                        if (!offerLetterConverted)
                        {
                            response.Message = "Please Provide Subscription Date.";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        dt.SubscriptionDate = offerLetterDate;
                    }
                }
                var result = _facade.SaveFeePayment(dto, SessionHelper.UserProfile.UserId,SessionHelper.UserProfile.SelectedCompanyId);
                return Json(new { Success = result.Success, Message = result.Message, MrId = result.Id }, JsonRequestBehavior.AllowGet);
                //return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetMoneyReceiptById(string reportTypeId,long receiptId,int mtype)
        {
            MoneyReceiptDto data = new MoneyReceiptDto();
            TraineeMoneyReceiptDto datatrainee = new TraineeMoneyReceiptDto();
            var mrList = new List<MoneyReceiptDto>();
            var mrTList = new List<TraineeMoneyReceiptDto>();
            LocalReport lr = new LocalReport();
            if (mtype == 1)
            {
                data = _facade.GetMoneyReceiptById(receiptId);
                data.AccountOfficer = SessionHelper.UserProfile.UserName;
                mrList.Add(data);

                string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "MoneyReceipt.rdlc");
                if (System.IO.File.Exists(path))
                {
                    lr.ReportPath = path;
                }
                else
                {
                    return View("MoneyReceiptList");
                }

                ReportDataSource rd = new ReportDataSource("dsMoneyReceipt", mrList);
                ReportDataSource rd1 = new ReportDataSource("dsMoneyReceiptDetails", data.MoneyReceiptDetails);
                lr.DataSources.Add(rd);
                lr.DataSources.Add(rd1);
            }

            else
            {
                datatrainee = _tfacade.GetMoneyReceiptById(receiptId);
                datatrainee.AccountOfficer = SessionHelper.UserProfile.UserName;
                mrTList.Add(datatrainee);

                string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptTraineeMoneyReceipt.rdlc");
                if (System.IO.File.Exists(path))
                {
                    lr.ReportPath = path;
                }
                else
                {
                    return View("MoneyReceiptList");
                }

                ReportDataSource rd = new ReportDataSource("dsTraineeMoneyReceipt", mrTList);
                ReportDataSource rd1 = new ReportDataSource("dsTraineeMoneyReceiptDetails", datatrainee.MoneyReceiptDetails);
                lr.DataSources.Add(rd);
                lr.DataSources.Add(rd1);
            }
                
            

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0in</MarginTop>" +
                "  <MarginLeft>0.10in</MarginLeft>" +
                "  <MarginRight>0in</MarginRight>" +
                "  <MarginBottom>0in</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        
        public ActionResult MoneyReceiptList(long memberId, string sortOrder, string currentFilter, string fromDate, string toDate, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;

            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
            ViewBag.MemberId = memberId;
            var students = _facade.MoneyReceiptList(memberId, FromDate, ToDate, pageSize, page);
            return View(students);
        }

        public ActionResult AllMoneyReceiptList(string sortOrder, string currentFilter, string fromDate, string toDate, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;

            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
           // ViewBag.MemberId = memberId;
            var students = _facade.AllMoneyReceiptList(FromDate, ToDate, pageSize, page);
            return View(students);
        }

        [HttpGet]
        public JsonResult GetMemberPaymentHistory(long? memberId)
        {
            var data = _facade.GetFeePendingMemberWise(memberId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet] SaveMemberFeeCollectionRenewal
        //public JsonResult GetFeeTypes()
        //{
        //    var data = _facade.GetFeeTypes();
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public JsonResult GetFeeTypes(List<long> feeIds)
        {
            var data = _facade.GetFeeTypes(feeIds);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllFeetypes()
        {
            var result = _facade.GetAllFeetypes();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetQuantitiveFeetypes()
        {
            var result = _facade.GetQuantitiveFeetypes();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetEmployeesForFeeCollection(List<long?> feeTypeIds,long? zone,long? division, string name)
        {
            var data = _facade.GetEmployeesForFeeCollection(feeTypeIds,zone,division,name,SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveMemberFeeCollectionRenewal(FeeCollectionRenewalDto dto)
        {
            try
            {
                //ResponseDto response = new ResponseDto();
                //foreach (var dt in dto.FeeCollectionTrans)
                //{
                //    if (!string.IsNullOrEmpty(dt.SubscriptionDateTxt))
                //    {
                //        DateTime offerLetterDate = DateTime.Now;
                //        var offerLetterConverted = DateTime.TryParseExact(dt.SubscriptionDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                //        if (!offerLetterConverted)
                //        {
                //            response.Message = "Please Provide Subscription Date.";
                //            return Json(response, JsonRequestBehavior.AllowGet);
                //        }
                //        dt.SubscriptionDate = offerLetterDate;
                //    }
                //}
                var result = _facade.SaveMemberFeeCollectionRenewal(dto, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetYearList()
        {
            var result = _facade.GetYears();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExistingMemberFeeCollection()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveExistingMemberFeePayment(MemberFeeCollectionDto dto)
        {
            try
            {
                ResponseDto response = new ResponseDto();
                foreach (var dt in dto.FeeCollectionTrans)
                {
                    if (!string.IsNullOrEmpty(dt.SubscriptionDateTxt))
                    {
                        DateTime offerLetterDate = DateTime.Now;
                        var offerLetterConverted = DateTime.TryParseExact(dt.SubscriptionDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                        if (!offerLetterConverted)
                        {
                            response.Message = "Please Provide Subscription Date.";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        dt.SubscriptionDate = offerLetterDate;
                    }
                    if (!string.IsNullOrEmpty(dt.ChequeDateTxt))
                    {
                        DateTime offerLetterDate = DateTime.Now;
                        var offerLetterConverted = DateTime.TryParseExact(dt.ChequeDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                        if (!offerLetterConverted)
                        {
                            response.Message = "Please Provide Cheque Date.";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        dt.ChequeDate = offerLetterDate;
                    }
                }
                var result = _facade.SaveFeePaymentExistingMember(dto, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
                return Json(new { Success = result.Success, Message = result.Message, MrId = result.Id, VoucherNo= result.Complementary, CompanyId= SessionHelper.UserProfile.SelectedCompanyId }, JsonRequestBehavior.AllowGet);
                //return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSystemDate()
        {
            var result = _companyProfileFacade.DateToday(1).Date;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FeeWiseMemberList()
        {
            return View();
        }

        public ActionResult RectifyFeeCollectionTran(string fromDate, string toDate)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _facade.RectifyTranData(FromDate, ToDate);
            return View();
        }
    }
}