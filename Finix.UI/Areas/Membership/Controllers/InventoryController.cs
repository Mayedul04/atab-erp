﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class InventoryController : Controller
    {
        private readonly InventoryFacade _inventory = new InventoryFacade();
        // GET: Membership/Inventory
        [HttpGet]
        public ActionResult StockIn()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        [HttpPost]
        public JsonResult SaveStockIn(StockTranDto dto)
        {
            var response = _inventory.SaveReceive(dto, SessionHelper.UserProfile.SelectedCompanyId, SessionHelper.UserProfile.UserId);
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        [HttpGet]
        public ActionResult StockTransfer()
        {
            return View();
        }
        [HttpGet]
        public ActionResult StockOut()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        [HttpPost]
        public JsonResult SaveStockOut(StockTranDto dto)
        {
            var response = _inventory.SaveDisburse(dto, SessionHelper.UserProfile.UserId);
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        [HttpGet]
        public JsonResult GetProducts()
        {
            var result = _inventory.GetProducts();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetStockedProducts(long officeId)
        {
            var result = _inventory.GetStockedProducts(officeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult StockLedgerReport()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        [HttpGet]
        public ActionResult GetStockLedgerReport(string reportTypeId, string fromDate, string toDate, long? officeId, long? productId)
        {
            DateTime startDt;
            DateTime endDt;
            var fromConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDt);
            if (!fromConverted)
                startDt = DateTime.Now.Date;

            var fromConvertedend = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDt);
            if (!fromConvertedend)
                endDt = DateTime.Now.Date;
            
            var stockLedger = _inventory.GetStockLedger(productId, officeId, startDt, endDt);
            LocalReport lr = new LocalReport();
            //string path = "";
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptStockLedger.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                //return View("CIF_Personal");
            }
            //stockPositionList.Add(sp);

            //var cs = stockLedger != null && stockLedger.Count > 0 ? stockLedger.FindLast(p => p != null).ClosingQuantity : 0;
            ReportParameter rp = new ReportParameter("FromDate", startDt.ToString());
            ReportParameter rp1 = new ReportParameter("ToDate", endDt.ToString());
            ReportDataSource rd = new ReportDataSource("dsStockLedger", stockLedger);

            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp, rp1});


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
    }
}