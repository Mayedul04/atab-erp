﻿using Finix.Auth.Service;
using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class MembershipReportController : Controller
    {
        MembershipReportFacade _facade = new MembershipReportFacade();
        // GET: Membership/MembershipReport
        public ActionResult PeriodWiseReport()
        {
            return View();
        }
        public ActionResult PaymentReport()
        {
            return View();
        }
        public ActionResult ZoneWiseReport()
        {
            return View();
        }

        public ActionResult FeeWiseMembersReport()
        {
            return View();
        }
        public ActionResult GetActiveMemberListReport(string reportTypeId)
        {
            List<MembershipReportDto> memberList = new List<MembershipReportDto>();
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptActiveMemberList.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
           
            ReportDataSource rd = new ReportDataSource("dsMembership", sp);

            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetLatePaymentsReport(string reportTypeId)
        {
            List<MembershipReportDto> memberList = new List<MembershipReportDto>();
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptLatePaymentsReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("dsMembership", sp);

            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetAreaWiseMembersList(string reportTypeId)
        {
            List<MembershipReportDto> memberList = new List<MembershipReportDto>();
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptAreaWiseMembersList.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("dsMembership", sp);

            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetDefaulterMembersReport(string reportTypeId)
        {
            List<MembershipReportDto> memberList = new List<MembershipReportDto>();
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptDefaulterMembersReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("dsMembership", sp);

            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetAllMemberListReport(string reportTypeId)
        {
            List<MembershipReportDto> memberList = new List<MembershipReportDto>();
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptPeriodicalMemberList.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("dsMembership", sp);

            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetMembershipApplicationReport(string reportTypeId, long memid)
        {
            List<MemberApplicationDto> memberList = new List<MemberApplicationDto>();
            var basicinfo = _facade.GetBasicInfoById(memid);
            memberList.Add(basicinfo);

            List<OwnerDto> contactPerson = _facade.GetContactPerson(memid);
            List<OwnerDto> ownerList = _facade.GetOwners(memid); 
            List<ChamberDto> chamberList = new List<ChamberDto>();
            chamberList = _facade.GetChamber(memid);
            List<AssociationDto> associationList = new List<AssociationDto>();
            associationList = _facade.GetAssociation(memid);
            List<DocumentTypeDto> documentList = new List<DocumentTypeDto>();
            List<BusinessCategoryDto> businessCategoryList = new List<BusinessCategoryDto>();
            businessCategoryList = _facade.GetBusinessCategory(memid);
            List<AttachmentDto> attachmentList = new List<AttachmentDto>();
            attachmentList = _facade.GetAttachment(memid);
            var sp = _facade.GetReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMemberApplication.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("Member", memberList);
            ReportDataSource rd1 = new ReportDataSource("Owner", ownerList);
            ReportDataSource rd2 = new ReportDataSource("Chambers", chamberList);
            ReportDataSource rd3 = new ReportDataSource("Association", associationList);
            ReportDataSource rd4 = new ReportDataSource("DocumentType", documentList);            
            ReportDataSource rd5 = new ReportDataSource("BusinessCategory", businessCategoryList);
            ReportDataSource rd6 = new ReportDataSource("Attachment", attachmentList);
            ReportDataSource rd7 = new ReportDataSource("ContactPerson", contactPerson);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.DataSources.Add(rd3);
            lr.DataSources.Add(rd4);
            lr.DataSources.Add(rd5);
            lr.DataSources.Add(rd6);
            lr.DataSources.Add(rd7);

            lr.EnableHyperlinks = true;
            lr.EnableExternalImages = true;
            

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }


        public ActionResult GetMemberListWithPhotoReport(string reportTypeId)
        {
            List<MemberDto> memberList = new List<MemberDto>();
            var sp = _facade.GetMemberListReport();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMemberListWithPhotoReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

           
            ReportDataSource rd = new ReportDataSource("Member", sp);

            lr.DataSources.Add(rd);
            lr.EnableExternalImages = true;


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.3in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.50in</MarginLeft>" +
                "  <MarginRight>0.50in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        public ActionResult GetMemberLedgerReport(string reportTypeId, long memberid, string fromDate, string toDate)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            
            var members = _facade.GetMemberLedgerInfo(memberid,FromDate,ToDate);
            var ledgerList = _facade.GetMemberFeeCollections(memberid, FromDate, ToDate);
            
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMemberLedger.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            ReportParameter fromdate = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter todate = new ReportParameter("ToDate", ToDate.ToString());

           
            

            ReportDataSource rd = new ReportDataSource("dsMemberLedger", members);
            ReportDataSource rd1 = new ReportDataSource("dsCollections", ledgerList);
            lr.SetParameters(new ReportParameter[] { fromdate, todate });

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            
            lr.EnableExternalImages = true;
            lr.EnableHyperlinks = true;
            


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

       
        public ActionResult GetMemberMembershipCertificate(string reportTypeId, long certificateid)
        {
            string path;
            string deviceInfo;
            CertificateRegisterDetailDto info = new CertificateRegisterDetailDto();
            var data = _facade.GetMembershipCertificate(certificateid);
            info = data.Where(x => x.Id == certificateid).FirstOrDefault();
            LocalReport lr = new LocalReport();
            if (info.CertificateType == CertificateType.Membership)
                path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMembershipCertificate.rdlc");
            else if (info.CertificateType == CertificateType.Renewal)
                path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptRenewalCertificate.rdlc");
            else
                path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMembershipCertificate.rdlc");

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }

            ReportDataSource rd = new ReportDataSource("MainCertificate", data);

            lr.DataSources.Add(rd);
            lr.EnableExternalImages = true;


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            if (info.CertificateType == CertificateType.Membership)
            {
                deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10.7in</PageWidth>" +
                "  <PageHeight>8.8in</PageHeight>" +
                "  <MarginTop>0</MarginTop>" +
                "  <MarginLeft>0</MarginLeft>" +
                "  <MarginRight>0</MarginRight>" +
                "  <MarginBottom>0</MarginBottom>" +
                "</DeviceInfo>";
            }
            else if (info.CertificateType == CertificateType.Renewal)
            {
                deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10.65in</PageWidth>" +
                "  <PageHeight>8.65in</PageHeight>" +
                "  <MarginTop>0</MarginTop>" +
                "  <MarginLeft>0</MarginLeft>" +
                "  <MarginRight>0</MarginRight>" +
                "  <MarginBottom>0</MarginBottom>" +
                "</DeviceInfo>";
            }
            else
                deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10.65in</PageWidth>" +
                "  <PageHeight>8.65in</PageHeight>" +
                "  <MarginTop>0</MarginTop>" +
                "  <MarginLeft>0</MarginLeft>" +
                "  <MarginRight>0</MarginRight>" +
                "  <MarginBottom>0</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult GetFeeWiseReceiveBreakdown(string reportTypeId,string fromDate, string toDate)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _facade.GetFeewiseReceiptAmount(FromDate, ToDate);
           
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptMembershipFeeDetailsReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            ReportParameter fromdate = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter todate = new ReportParameter("ToDate", ToDate.ToString());




            ReportDataSource rd = new ReportDataSource("MembershipFeeDetailsReport", data);
            
            lr.SetParameters(new ReportParameter[] { fromdate, todate });

            lr.DataSources.Add(rd);
           

            lr.EnableExternalImages = true;
            lr.EnableHyperlinks = true;



            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        public ActionResult GetFeeWiseMemberList(string reportTypeId, long feetype, string fromDate, string toDate)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _facade.GetFeeWiseMemberList(feetype,FromDate, ToDate);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptFeeWiseMemberList.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            ReportParameter fromdate = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter todate = new ReportParameter("ToDate", ToDate.ToString());




            ReportDataSource rd = new ReportDataSource("dsFeeWiseMemberList", data);

            lr.SetParameters(new ReportParameter[] { fromdate, todate });

            lr.DataSources.Add(rd);


            lr.EnableExternalImages = true;
            lr.EnableHyperlinks = true;



            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
    }
}