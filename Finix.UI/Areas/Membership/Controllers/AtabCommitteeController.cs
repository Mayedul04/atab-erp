﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.Membership.Dto;
using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Microsoft.Reporting.WebForms;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class AtabCommitteeController :Controller
    {
        private readonly AtabCommitteeFacade _atabCommitteeFacade = new AtabCommitteeFacade();

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult SaveAtabCommittee(ATABCommitteeDto dto)
        {
            try
            {
                DateTime creditedDate = DateTime.Now;
                if (dto.TenureHistoryDetails != null)
                {
                    foreach (var item in dto.TenureHistoryDetails)
                    {
                        var JoinDate = DateTime.TryParseExact(item.JoiningDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out creditedDate);
                        if (JoinDate)
                            item.JoiningDate = creditedDate;
                        var EndDate = DateTime.TryParseExact(item.EndOfTenureTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out creditedDate);
                        if (JoinDate)
                            item.EndOfTenure = creditedDate;
                    }
                }
                var userId = SessionHelper.UserProfile.UserId;
                var result = _atabCommitteeFacade.SaveAtabCommittee(dto, userId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.DenyGet);
            }

        }
        [HttpPost]
        public JsonResult UploadPicture(UploadDto dto)
        {
            var userId = SessionHelper.UserProfile.UserId;
            var result = _atabCommitteeFacade.UploadPicture(dto, userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }//LoadAtabCommittee
        [HttpGet]
        public JsonResult LoadAtabCommittee(long id)
        {
            var loanApp = _atabCommitteeFacade.LoadAtabCommittee(id);
            return Json(loanApp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAddressById(long addressId)
        {
            var loanApp = _atabCommitteeFacade.GetAddressById(addressId);
            return Json(loanApp, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetMemberAutoFill(AutofillDto dto)
        {
            var result = _atabCommitteeFacade.GetMemberAutoFill(dto.prefix, dto.exclusionList);
            var data = result.Select(r => new { key = r.Id, value = r.NameOfOrganization , address  = r.PresentAddressId}).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AtabCommitteeList(string sortOrder, string currentFilter, string searchString, long? sourceid, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            var applications = _atabCommitteeFacade.AtabCommitteeList(pageSize, page, searchString);
            return View(applications);
        }
        public ActionResult AtabCommitteeReportList(string sortOrder, string currentFilter, string searchString, long? sourceid, string range, int page = 1,  int pageSize = 20) // string fromDate, string toDate,
        {
            //DateTime FromDate = DateTime.MinValue;
            //DateTime ToDate = DateTime.MaxValue;
            //DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            //var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            //if (!toDateConverted)
            //    ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CommitteeType = sourceid;
            //ViewBag.FromDate = fromDate;
            //ViewBag.Todate = toDate;
            ViewBag.TimeRange = range;
            var applications = _atabCommitteeFacade.AtabCommitteeReportList(range, pageSize, page, searchString, sourceid); /*FromDate, ToDate*/
            return View(applications);
        }
        //GetAtabCommitteeReportList
        public ActionResult GetAtabCommitteeReportList(string reportTypeId, string search,long? type, string range) //, string fromDate, string toDate
        { 
            string Year = "";
            var poDto = _atabCommitteeFacade.GetAtabCommitteeReportList( search,type,range);/* FromDate, ToDate,*/
            LocalReport lr = new LocalReport();
            if (poDto.Any())
            {
                Year = poDto.FirstOrDefault()!= null? poDto.FirstOrDefault().YearRange :"";

            }
            string path = "";
            path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "AtabCommitteeReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("AtabCommitteeReportList");
            }
            ReportParameter rpTimeSpan = new ReportParameter("TimeSpan", Year);
            ReportDataSource rd = new ReportDataSource("dsAtabCommitteeReport", poDto);
            lr.DataSources.Add(rd);
            //lr.DataSources.Add(rd1);
            lr.SetParameters(new ReportParameter[] { rpTimeSpan });


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public JsonResult GetAllAtabCommitteeList()
        {
            var data = _atabCommitteeFacade.GetAllAtabCommitteeList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}