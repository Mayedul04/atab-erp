﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class AccountsController : Controller
    {
        public readonly MembeshipAccountFacade _memberAccounts = new MembeshipAccountFacade();
        private readonly EnumFacade _enum = new EnumFacade();
        // GET: Membership/Accounts
        public ActionResult AccountHeadMapping(string searchString, int pageSize = 10, int pageCount = 1)
        {
            ViewBag.CurrentSort = searchString;
            var data = _memberAccounts.GetAccHeadMappingList(pageSize, pageCount, searchString);
            return View(data);
        }
        public JsonResult GetRefOptions(AccountHeadRefType refType)
        {
            if (refType == AccountHeadRefType.Income || refType == AccountHeadRefType.Receivable || refType == AccountHeadRefType.Liability)
            {
                var memberfees = _memberAccounts.GetMemberFees();
                return Json(memberfees, JsonRequestBehavior.AllowGet);
            }
            else if (refType == AccountHeadRefType.Income_ATTI || refType == AccountHeadRefType.Receivable_ATTI)
            {
                var traineefees = _enum.GetCourseList();
                return Json(traineefees, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var traineefees = _enum.GetCourseList();
                return Json(traineefees, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetAccHeads(AccountHeadRefType refType)
        {
            var data = _memberAccounts.GetAccHeads(refType, (long)SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveAccHeadMapping(AccHeadMappingDto dto)
        {
            var result = _memberAccounts.SaveAccHeadMapping(dto, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}