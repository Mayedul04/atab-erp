﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class CourseFeeCollectionController : Controller
    {
        private readonly CourseFeeCollectionFacade _facade = new CourseFeeCollectionFacade();
        private readonly EnumFacade _enum = new EnumFacade();
        // GET: Membership/CourseFeeCollection
        #region Course Fee Collection
        public ActionResult CourseFeeCollection()
        {
            return View();
        }

        public JsonResult GetPendingCourseFeeTraineeWise(long traineeId)
        {
            var data = _facade.GetPendingCourseFeeTraineeWise(traineeId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPaymentTypes()
        {
            var data = _facade.GetPaymentTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourses(List<long> courseids)
        {
            var data = _facade.GetCourses(courseids);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        public JsonResult SaveFeePayment(TraineeCourseFeeCollectionDto dto)
        {
            try
            {

                ResponseDto response = new ResponseDto();
                foreach (var dt in dto.FeeCollectionTrans)
                {
                    if (!string.IsNullOrEmpty(dt.SubscriptionDateTxt))
                    {
                        DateTime offerLetterDate = DateTime.Now;
                        var offerLetterConverted = DateTime.TryParseExact(dt.SubscriptionDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                        if (!offerLetterConverted)
                        {
                            response.Message = "Please Provide Subscription Date.";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        dt.SubscriptionDate = offerLetterDate;
                    }
                    if (!string.IsNullOrEmpty(dt.ChequeDateTxt))
                    {
                        DateTime offerLetterDate = DateTime.Now;
                        var offerLetterConverted = DateTime.TryParseExact(dt.ChequeDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out offerLetterDate); ;
                        if (!offerLetterConverted)
                        {
                            response.Message = "Please Provide Cheque Date.";
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                        dt.ChequeDate = offerLetterDate;
                    }
                }


                var result = _facade.SaveFeePayment(dto, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
                return Json(new { Success = result.Success, Message = result.Message, MrId = result.Id, VoucherNo = result.Complementary, CompanyId = SessionHelper.UserProfile.SelectedCompanyId }, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public ActionResult PaymentHistory(long traineeid,string sortOrder, string currentFilter, PayType? payby, string fromDate, string toDate, long? course, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;
            if (payby != null)
                ViewBag.PayType = (int)payby;
            ViewBag.CurrentSort = sortOrder;
            if (course != null)
                ViewBag.Course = (int)course;
            ViewBag.Courses = _enum.GetCourseList();
            ViewBag.PayTypes = _enum.GetPayTypeList();
            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
            ViewBag.TraineeID = traineeid;
            var students = _facade.PaymentHistory(traineeid,FromDate, ToDate, pageSize, page, payby, course);
            return View(students);
        }

        #region Money Receipt
        public ActionResult MoneyReceipt(long traineeid, string sortOrder, string currentFilter, string fromDate, string toDate, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;

            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
            ViewBag.TraineeID = traineeid;
            var students = _facade.MoneyReceipt(traineeid, FromDate, ToDate, pageSize, page);
            return View(students);
        }

        public ActionResult GetMoneyReceiptById(string reportTypeId, long receiptId)
        {
            var data = _facade.GetMoneyReceiptById(receiptId);
            var mrList = new List<TraineeMoneyReceiptDto>();
            mrList.Add(data);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Membership/Reports"), "rptTraineeMoneyReceipt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("MoneyReceipts");
            }

            ReportDataSource rd = new ReportDataSource("dsTraineeMoneyReceipt", mrList);
            ReportDataSource rd1 = new ReportDataSource("dsTraineeMoneyReceiptDetails", data.MoneyReceiptDetails);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        #endregion

        #region Fee Setup for Students
        public ActionResult QuantitiveFeeSelectionForTrainees()
        {
            return View();
        }
        public JsonResult GetQuantitiveFeetypes()
        {
            var result = _facade.GetQuantitiveFeetypes();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNormalQuantitiveFeetypes()
        {
            var result = _facade.GetNormalQuantitiveFeetypes();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTraineesForFeeSetup(List<long?> feeTypeIds, string batch, string traineeno, string name)
        {
            var data = _facade.GetTraineeForFeeSetup(feeTypeIds, batch, traineeno, name);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOtherFees(long feeTypeId, List<TraineeDto> trainees)
        {
            var result = _facade.SetTraineeFees(feeTypeId, trainees, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
            return Json(result, JsonRequestBehavior.AllowGet);
        } 
        #endregion

    }
}