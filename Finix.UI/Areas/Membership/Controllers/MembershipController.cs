﻿using Finix.Membership.DTO;
using Finix.Membership.Facade;
using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Membership.Controllers
{
    public class MembershipController : Controller
    {
        // GET: Membership/Membership
        private readonly MemberFacade _facade = new MemberFacade();
        private readonly EnumFacade _enumFacade = new EnumFacade();
        
        public ActionResult PaymentHistory()
        {
            return View();
        }
        public ActionResult MembershipApplication()
        {
            return View();
        }
        public ActionResult MembershipList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GeMemberListForAutoFill(AutofillDto dto)
        {
            var result = _facade.GetMemberListForAutoFill(dto.prefix, dto.exclusionList);
            var data = result.Select(r => new { key = r.Id, value = r.NameOfOrganization }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MembershipFeeCollection()
        {
            return View();
        }
        public ActionResult bk_MembershipApplication()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveMembership(MemberDto dto)
        {
            DateTime incorporationDate = DateTime.Now.Date;
            var incorporationDateConverted = DateTime.TryParseExact(dto.IncorporationDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out incorporationDate);
            if (incorporationDateConverted)
                dto.IncorporationDate = incorporationDate;

            DateTime renewalDate = DateTime.Now.Date;
            var renewalDateConverted = DateTime.TryParseExact(dto.RenewalDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out renewalDate);
            if (renewalDateConverted)
                dto.RenewalDate = renewalDate;

            DateTime renewalIssueDate = DateTime.Now.Date;
            var renewalIssueDateConverted = DateTime.TryParseExact(dto.RenewalIssueDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out renewalIssueDate);
            if (renewalIssueDateConverted)
                dto.RenewalIssueDate = renewalIssueDate;

            DateTime licenseDate = DateTime.Now.Date;
            var licenseDateConverted = DateTime.TryParseExact(dto.LicenseDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out licenseDate);
            if (licenseDateConverted)
                dto.LicenseDate = licenseDate;

            var success = _facade.SaveMember(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public FileResult Download(string imageName)
        {
            return File(imageName, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(imageName));

        }

        public ActionResult PendingApplication(string sortOrder, string currentFilter, string searchString,  long? businessType, Finix.Membership.Infrastructure.Zone? zone, long? sourceid, int page = 1, int pageSize = 20)
        {
            //string fromDate, string toDate,
            //DateTime FromDate = DateTime.MinValue;
            //DateTime ToDate = DateTime.MaxValue;
            //DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            //var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            //if(!toDateConverted)
            //    ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.BusinessType = businessType;
            ViewBag.BusinessTypes = new SettingsFacade().GetBusinessCategories();
            if(zone != null)
                ViewBag.Zone = (int)zone;
            ViewBag.Zones = _enumFacade.GetZoneList();
            //ViewBag.FromDateText = fromDate;
            //ViewBag.ToDateText = toDate;
            var applications = _facade.PendingMemberApplicationList( pageSize, page, searchString, businessType, zone);
                              
            return View(applications);
        }

        [HttpPost]
        public JsonResult SaveOwner(OwnerDto dto)
        {
            var success = _facade.SaveOwner(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveAddress(MemberDto dto)
        {
            var success = _facade.SaveAddress(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOwnerList(long memberId)
        {
            var success = _facade.GetOwnerList(memberId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveAttachment(AttachmentDto dto)
        {
            var success = _facade.SaveAttachment(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDocumentList(long memberId)
        {
            var success = _facade.GetDocumentList(memberId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApprovedMemberList(string sortOrder, string currentFilter, string searchString, string fromDate, string toDate, long? businessType, Finix.Membership.Infrastructure.Zone? zone, long? sourceid, int page = 1, int pageSize = 20)
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MaxValue;
            DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            var toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!toDateConverted)
                ToDate = DateTime.MaxValue;

            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.BusinessType = businessType;
            ViewBag.BusinessTypes = new SettingsFacade().GetBusinessCategories();
            if (zone != null)
                ViewBag.Zone = (int)zone;
            ViewBag.Zones = _enumFacade.GetZoneList();
            ViewBag.FromDateText = fromDate;
            ViewBag.ToDateText = toDate;
            var applications = _facade.ApprovedMemberList(FromDate, ToDate, pageSize, page, searchString, businessType, zone);

            return View(applications);
        }


        [HttpGet]
        public JsonResult GetMemberApplication(long memberId)
        {
            var requisition = _facade.GetMemberApplication(memberId);
            return Json(requisition, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult RemoveOwner(long Id)
        {
            var requisition = _facade.RemoveOwner(Id, SessionHelper.UserProfile.UserId);
            return Json(requisition, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult RemoveAttachment(long Id)
        {
            var requisition = _facade.RemoveAttachment(Id, SessionHelper.UserProfile.UserId);
            return Json(requisition, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetFees(MemberPendingFeeDto dto)
        {
            var success = _facade.SetFees(dto, SessionHelper.UserProfile.UserId, SessionHelper.UserProfile.SelectedCompanyId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFeeTypeList(long? memid)
        {
            var success = _facade.GetFeeTypeList(memid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult MemberApprove(MemberDto dto)
        {
            DateTime issueDate = DateTime.Now.Date;
            var issueDateConverted = DateTime.TryParseExact(dto.IssueDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out issueDate);
            if (issueDateConverted)
                dto.IssueDate = issueDate;
            DateTime dateOfReceived = DateTime.Now.Date;
            var dateOfReceivedConverted = DateTime.TryParseExact(dto.DateOfReceivedText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOfReceived);
            if (dateOfReceivedConverted)
                dto.DateOfReceived = dateOfReceived;
            var success = _facade.MemberApprove(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetApprove(MemberDto dto)
        {
            DateTime issueDate = DateTime.Now.Date;
            var issueDateConverted = DateTime.TryParseExact(dto.IssueDateText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out issueDate);
            if (issueDateConverted)
                dto.IssueDate = issueDate;

            DateTime dateOfReceived = DateTime.Now.Date;
            var dateOfReceivedConverted = DateTime.TryParseExact(dto.DateOfReceivedText, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOfReceived);
            if (dateOfReceivedConverted)
                dto.DateOfReceived = dateOfReceived;

            var success = _facade.SetApprove(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMemberPaymentHistory(long memberId)
        {
            var success = _facade.GetPaymentHistory(memberId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FeeSelectionForMembers()
        {
            return View();
        }

        #region Certificate Issue to Member
        public ActionResult CertificatePrint()
        {
            return View();
        }
        public ActionResult QuantitiveFeeSelectionForMembers()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetAvailableCertificateNo(AutofillDto dto, long bookid)
        {
            var chequeList = _facade.GetCertificteListForAutoFill(dto.prefix, dto.exclusionList, bookid);
            return Json(chequeList.Select(c => new { key = c.CertificateNo, value = c.Id }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCertificateBooks(CertificateType type)
        {
            var success = _facade.GetBooks(type);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCertificate(CertificateRegisterDetailDto dto)
        {
            var success = _facade.SaveCertificate(dto, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CertificateList(long memid)
        {
            var success = _facade.GetMembersCirtificates(memid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CertificatebyId(long cerid)
        {
            var success = _facade.GetCertificate(cerid);
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult MembersFeeCollectionReport()
        {
            return View();
        }

        public ActionResult FeeCollectionBreakdown()
        {
            return View();
        }
    }
}