﻿$(document).ready(function () {
    function ApprovedMemberListVM() {
        var self = this;

        self.PageData = ko.observableArray(pageData);
        self.FromDate = ko.observable(fromDate ? moment(fromDate,"DD/MM/YYYY") : moment());
        self.ToDate = ko.observable(toDate ? moment(toDate, "DD/MM/YYYY") : moment());
        self.Zones = ko.observableArray(zones);
        self.Zone = ko.observable(zone);
        self.BusinessTypes = ko.observableArray(bcs);
        self.BusinessType = ko.observable(businessType);
        self.FromDate.subscribe(function () {
            $('#fromDate').val(moment(self.FromDate()).format("DD/MM/YYYY"));
        });
        self.ToDate.subscribe(function () {
            $('#toDate').val(moment(self.ToDate()).format("DD/MM/YYYY"));
        });

        self.Details = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '136_' + data.Id,
                Menu: 'Membership Application',
                Url: '/Membership/Membership/MembershipApplication',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
        self.PaymentDetails = function (data) {
            var parameters = [{
                Name: 'Id',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '139_' + data.Id,
                Menu: 'Member Payment History',
                Url: '/Membership/Membership/PaymentHistory',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
        self.MoneyReceipt = function (data) {
            var parameters = [{
                Name: 'MemberId',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '140_' + data.Id,
                Menu: 'Money Receipts',
                Url: '/Membership/FeeCollection/MoneyReceiptList',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }

        self.PrintCertificate = function (data) {
            var parameters = [{
                Name: 'MemberId',
                Value: data.Id
            }];
            var menuInfo = {
                Id: '141_' + data.Id,
                Menu: 'Print Certificate',
                Url: '/Membership/Membership/CertificatePrint',
                Parameters: parameters
            }
            window.parent.AddTabFromExternal(menuInfo);
        }
        
        self.GetMemberListWithPhotoReport = function () {
            window.open('/Membership/MembershipReport/GetMemberListWithPhotoReport?reportTypeId=PDF', '_blank');

        }
    }

    var pavm = new ApprovedMemberListVM();
    ko.applyBindings(pavm, document.getElementById("ApprovedMemberList"));
});