﻿using Finix.Auth.DTO;
using Finix.Auth.Facade;
using Finix.UI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.Accounts.Facade;
using Finix.Auth.Infrastructure;
using Finix.Auth.Util;

namespace Finix.UI.Areas.Auth.Controllers
{
    public class CompanyProfileController : BaseController
    {
        private readonly CompanyProfileFacade _companyProfileFacade;

        public CompanyProfileController(CompanyProfileFacade companyProfileFacade)
        {
            this._companyProfileFacade = companyProfileFacade;
        }
        [HttpGet]
        public JsonResult GetAllActiveCompanies()
        {
            var companyList = _companyProfileFacade.GetAllActiveCompanyProfiles();
            return Json(companyList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAllCompanyList()
        {
            var companyList = _companyProfileFacade.GetAllCompanyProfiles().Select(c => new { c.Name, c.Id }).ToList();
            return Json(companyList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveCompanyProfile(OfficeProfileDto company)
        {
            var response = _companyProfileFacade.SaveCompanyProfile(company, SessionHelper.UserProfile.UserId);
            return Json(response);
        }

        //
        // GET: /CompanyProfile/
        public ActionResult Index()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        public ActionResult ClosingDate()
        {
            return View();
        }
        public JsonResult UpdateClosingDate(long CompanyProfileId)
        {
            DateTime ReceiveDateFrom = _companyProfileFacade.LastClosingDate(CompanyProfileId);
            DateTime ReceiveDateTo;
            ReceiveDateTo = ReceiveDateFrom.AddDays(1).AddMilliseconds(-3);
            var _accounts = new AccountsFacade();
            var childIds = new List<long>();
            var company = _companyProfileFacade.GetCompanyProfileById(CompanyProfileId);
            if (company != null && company.CompanyType == CompanyType.Project)
            {
                var eligibility = new ResponseDto();
                eligibility.Message = "Please ask your parent company to close the date.";
                return Json(eligibility, JsonRequestBehavior.AllowGet);

            }
            var test = _companyProfileFacade.GetDependentIds(CompanyProfileId);
            if (test != null && test.Count > 0)
                childIds.AddRange(test);

            childIds.Add(CompanyProfileId);

            var response = new AccountsFacade().CheckPendingVouchers(childIds);
            if (response.Success == true)
                return Json(response, JsonRequestBehavior.AllowGet);
            try
            {
                new AccountsFacade().ArchieveVouchers(childIds, ReceiveDateTo);
            }
            catch (Exception)
            {
                var successResponse = new ResponseDto();
                successResponse.Message = "Some error occured, please contact system administrator.";
                return Json(successResponse, JsonRequestBehavior.AllowGet);
            }

            var cp = _companyProfileFacade.UpdateClosingDate(childIds);
            return Json(cp, JsonRequestBehavior.AllowGet);

        }
        //[HttpGet]
        //public JsonResult UpdateClosingDate(long? CompanyProfileId)
        //{

        //    //var _accounts = new AccountsFacade();
        //    var closingDate = _companyProfileFacade.DateToday(CompanyProfileId);
        //    //var productionCostPerKg = _production.CalculateCostPerKG(closingDate);
        //    //_accounts.ArchieveVouchers(closingDate);

        //    //var _reportAccount = new ReportAccountFacade();
        //    //_reportAccount.GetInventoryAmount(DateTime.Now.AddYears(-1), closingDate);

        //    var cp = _companyProfileFacade.UpdateClosingDate();
        //    return Json(cp, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public JsonResult TakeDBBackup()
        {
            var response = _companyProfileFacade.TakeDBBackup();
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DateToday(long? CompanyProfileId)
        {
            var cp = _companyProfileFacade.DateToday(CompanyProfileId);
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCurrentFiscalYear(long CompanyProfileId)
        {
            var cp = _companyProfileFacade.GetCurrentFiscalYear(CompanyProfileId);
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        /*sabiha*/
        public JsonResult GetUpdateBillNo()
        {
            var cp = _companyProfileFacade.GetUpdateBillNo();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUpdateVoucherNo()
        {
            var cp = _companyProfileFacade.GetUpdateVoucherNo();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUpdateSpecificVoucherNo(string type)
        {
            string voucherNo = "";// = _companyProfileFacade.GetUpdateCDvNo();
            switch (type)
            {
                case "CDV":
                    voucherNo = _companyProfileFacade.GetUpdateCDvNo();
                    break;
                case "BDV":
                    voucherNo = _companyProfileFacade.GetUpdateBDvNo();
                    break;
                case "CCV":
                    voucherNo = _companyProfileFacade.GetUpdateCCvNo();
                    break;
                case "BCV":
                    voucherNo = _companyProfileFacade.GetUpdateBCvNo();
                    break;
                case "JV":
                    voucherNo = _companyProfileFacade.GetUpdateJvNo();
                    break;
                default:
                    break;
                    //return Json(voucherNo, JsonRequestBehavior.AllowGet);
            }

            return Json(voucherNo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInvoiceNo()
        {
            var cp = _companyProfileFacade.GetUpdateInvoiceNo();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChalanNo()
        {
            var cp = _companyProfileFacade.GetUpdatedChalanNo();
            return Json(cp, JsonRequestBehavior.AllowGet);
        }
        /*Sabiha Modified 18.10.2016*/
        public string GetAllCompanyListForGrid()
        {
            var companyList = _companyProfileFacade.GetAllCompanyProfiles().Select(c => new { c.Name, c.Id }).ToList();

            string strret = "<select>";
            foreach (var item in companyList)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        public string GetCompanyProfile()
        {
            List<KeyValuePair<int, string>> leaveApps = UiUtil.EnumToKeyVal<CompanyType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in leaveApps)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        #region Closing Date
        public ActionResult DayClosing()
        {

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();

            var response = _companyProfileFacade.LastClosingDate(SessionHelper.UserProfile.SelectedCompanyId);
            ViewBag.Active = true;
            ViewBag.Message = "Current system date is " + response.ToString("dd-MM-yyyy");
            return View();
        }
        [HttpGet]
        public JsonResult GetLastClosingDate(long companyId)
        {
            var response = _companyProfileFacade.LastClosingDate(companyId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}