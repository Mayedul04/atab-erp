﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class DegreeController : Controller
    {
        //
        // GET: /Degree/
        private readonly EducationFacade educationfacade;
        public DegreeController()
        {
            educationfacade = new EducationFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDegree()
        {
            return Json(educationfacade.GetDegrees(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDegree(DegreeInfoDto degreeinfodto)
        {
           
            try
            {
                educationfacade.SaveDegree(degreeinfodto);
                return Json(new { Result = "OK", Message = "Degree saved succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
            
        }

        [HttpPost]
        public JsonResult DeleteDegree(int id)
        {
           
            try
            {
                educationfacade.DeleteDegree(id);
                return Json(new { Result = "OK", Message = "Degree deleted succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
           
        }

        [HttpGet]
        public string GetDegreeLevels()
        {
            List<KeyValuePair<int, string>> degreelevellist = UiUtil.EnumToKeyVal<DegreeLevel>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in degreelevellist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetDegreeMajors()
        {
            List<KeyValuePair<int, string>> degreemajorlist = UiUtil.EnumToKeyVal<DegreeMajor>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in degreemajorlist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public string GetInstitutes()
        {
            var institutes = educationfacade.GetInstitutes();
            string strret = "<select>";
            foreach (InstituteDto item in institutes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public JsonResult GetDegreeByInstitute(int instituteid)
        {
            return Json(educationfacade.GetDegreeByInstitute(instituteid), JsonRequestBehavior.AllowGet);
        }
	}
}