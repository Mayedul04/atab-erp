﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class AppraisalController : Controller
    {
        private AppraisalFacade _appraisal = new AppraisalFacade();
        private readonly LeaveSupervisionFacade leaveSupervisionFacade;
        public AppraisalController()
        {
            leaveSupervisionFacade = new LeaveSupervisionFacade();
        }

        public ActionResult AppraisalIndex()
        {
            return View();
        }

        public JsonResult GetAllAppraisals()
        {
            var result = _appraisal.GetAllAppraisals();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetAllAppraisalTypes()
        {
            List<KeyValuePair<int, string>> appraisalTypes = UiUtil.EnumToKeyVal<AppraisalType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in appraisalTypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetAllAppraisalStatus()
        {
            List<KeyValuePair<int, string>> appraisalStatus = UiUtil.EnumToKeyVal<AppraisalStatus>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in appraisalStatus)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpPost]
        public JsonResult SaveAppraisal(AppraisalDto model)
        {
            if (!string.IsNullOrEmpty(model.LastE1DateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.LastE1DateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.LastE1Date = changeDate;
            }
            if (!string.IsNullOrEmpty(model.LastE2DateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.LastE2DateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.LastE2Date = changeDate;
            }
            if (!string.IsNullOrEmpty(model.LastSubmissionDateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.LastSubmissionDateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.LastSubmissionDate = changeDate;
            }
            if (!string.IsNullOrEmpty(model.EffectiveDateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.EffectiveDateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.EffectiveDate = changeDate;
            }
            if (!string.IsNullOrEmpty(model.StartDateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.StartDateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.StartDate = changeDate;
            }
            if (!string.IsNullOrEmpty(model.EndDateTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.EndDateTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.EndDate = changeDate;
            }
            var responce = _appraisal.SaveAppraisal(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteAppraisal(int id)
        {
            var response = _appraisal.DeleteAppraisal(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AppraisalKpiGroupSettingIndex()
        {
            return View();
        }

        public JsonResult GetAllAppraisalKpiGroupSettings()
        {
            var result = _appraisal.GetAllAppraisalKpiGroupSettings();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetAllAppraisalForGroup()
        {
            var result = _appraisal.GetAllAppraisals();
            string strret = "<select>";
            foreach (var item in result)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpPost]
        public JsonResult SaveAppraisalGroupSettings(AppraisalKpiGroupSettingDto model)
        {
            var responce = _appraisal.SaveAppraisalGroupSettings(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteAppraisalGroupSettings(int id)
        {
            var response = _appraisal.DeleteAppraisalGroupSettings(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult KPIGroupSettingIndex()
        {
            return View();
        }

        public ActionResult AppraisalInterface()
        {

            return View();
        }

        [HttpGet]
        public JsonResult GetAppraisalPerformance(int empid, int appraisalid)
        {
            return Json(_appraisal.GetAppraisalPerformances(empid, appraisalid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AppraisalView()
        {
            return View();
        }
        public JsonResult GetAppraisals(int DesignationId)
        {
            var data = _appraisal.GetAppraisals(DesignationId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePerformanceAppraisals(PerformanceAppraisalDto pdto)
        {
            string Message = "";
            try
            {
                _appraisal.SavePerformanceAppraisals(pdto);
                Message = "Saved successfully";
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }
            return Json(Message);
        }
        public ActionResult EmployeeSelfEvaluation()
        {
            long employeeid = leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            if (_appraisal.IsTopmost(employeeid))
            {
                ViewBag.Message = "You are The topmost in the Office!!";
                return View("EvaluationDoneMessage");
            }
            else
            {
                var data = _appraisal.GetSelfEvaluation(employeeid);
                ViewBag.UserName = SessionHelper.UserProfile.UserName;
                if (data == null)
                {
                    ViewBag.Message = "You have already submitted you self evaluation!!";
                    return View("EvaluationDoneMessage");
                }
                else
                {
                    return View();
                }
            }

        }

        [HttpGet]
        public JsonResult GetSelfEvaluation()
        {
            long employeeid = leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = _appraisal.GetSelfEvaluation(employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSelfEvaluation(EmployeeEvaluationDto evaldto)
        {
            string Message = "";
            if (_appraisal.IsEvaluationSubmitted(evaldto.EmployeeId, evaldto.PeriodFrom.Year))
            {
                Message = "You have already submitted your evaluation!!";
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    _appraisal.SubmitSelfEvaluation(evaldto);
                    Message = "Submitted successfully";
                }
                catch (Exception ex)
                {

                    Message = ex.Message;
                }
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult SubmitSupervisorOneEvaluation(EmployeeEvaluationDto evaldto)
        {
            string Message = "";
            if (_appraisal.IsEvaluationForwarded(evaldto.EmployeeId, evaldto.PeriodFrom.Year, leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId)))
            {
                Message = "You have already submitted your evaluation!!";
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    _appraisal.SubmitSupervisorOneEvaluation(evaldto, leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId));
                    Message = "Submitted successfully";
                }
                catch (Exception ex)
                {

                    Message = ex.Message;
                }
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult SubmitSupervisorTwoEvaluation(EmployeeEvaluationDto evaldto)
        {
            string Message = "";
            if (_appraisal.IsEvaluationForwarded(evaldto.EmployeeId, evaldto.PeriodFrom.Year, leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId)))
            {
                Message = "You have already submitted your evaluation!!";
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    _appraisal.SubmitSupervisorTwoEvaluation(evaldto, leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId));
                    Message = "Submitted successfully";
                }
                catch (Exception ex)
                {

                    Message = ex.Message;
                }
                return Json(Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult GetPendingEvaluationList()
        {
            long employeeid = leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = _appraisal.GetPendingEvaluationList(employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PendingEvaluationList()
        {
            long employeeid = leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = _appraisal.GetPendingEvaluationList(employeeid);
            if (data.Count() == 0)
            {
                ViewBag.Message = "No pending evaluation";
                return View("EvaluationDoneMessage");
            }
            else
            {
                ViewBag.Username = SessionHelper.UserProfile.UserName;
                return View();
            }
        }


        public ActionResult SuperVisor1Evaluation(long employeeId, long evaluationYear)
        {
            ViewBag.EmployeeId = employeeId;
            ViewBag.EvaluationYear = evaluationYear;
            return View();
        }

        public ActionResult SuperVisor2Evaluation(long employeeId, long evaluationYear)
        {
            ViewBag.EmployeeId = employeeId;
            ViewBag.EvaluationYear = evaluationYear;
            return View();
        }

        [HttpGet]
        public JsonResult GetSupervisor1Evaluation(long empid, long evaluationYear)
        {
            return Json(_appraisal.GetSupervisor1Evaluation(empid, evaluationYear), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSupervisor2Evaluation(long empid, long evaluationYear)
        {
            return Json(_appraisal.GetSupervisor2Evaluation(empid, evaluationYear), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSeniorsList()
        {
            long employeeid = leaveSupervisionFacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = _appraisal.GetSeniorsList((int)employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EvaluationReport()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEvaluationReport(long EmployeeId, string periodFrom, string periodTo)
        {


            DateTime PeriodFrom = DateTime.ParseExact(periodFrom, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime PeriodTo = DateTime.ParseExact(periodTo, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);



            return Json(_appraisal.GetEvaluationReport(EmployeeId, PeriodFrom, PeriodTo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EvaluationWeightSetup()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEvaluationWeightSetup()
        {
            return Json(_appraisal.GetEvaluationWeightSetup(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEvaluationWeightSetup(EvaluationWeightSetupDto dto)
        {
            string Message = "";
            try
            {
                _appraisal.SaveEvaluationWeightSetup(dto);
                Message = "Evaluation weight saved successfully";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return Json(Message, JsonRequestBehavior.AllowGet);
        }
    }
}