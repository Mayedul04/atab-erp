﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class WorkFlowController : Controller
    {
        //
        // GET: /WorkFlow/
        private readonly WorkFlowFacade workflowfacade;
        private readonly OfficeFacade officefacade;
        public WorkFlowController()
        {
            workflowfacade = new WorkFlowFacade();
            officefacade = new OfficeFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllWorkFlowSettings()
        {
            return Json(workflowfacade.getAllWFSettings(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveWorkFlowSettings(WFSettingDto wfsettingdto)
        {
            try
            {
                workflowfacade.SaveWFSetting(wfsettingdto);
                return Json(new { Result = "OK", Message = "WF Setting saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteWFSetting(int id)
        {
            try
            {
                workflowfacade.DeleteWFSetting(id);
                return Json(new { Result = "OK", Message = "WF Setting deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetWFTypes()
        {
            List<KeyValuePair<int, string>> wftypes = UiUtil.EnumToKeyVal<WFTypes>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in wftypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public ActionResult WFStep()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllWFSteps()
        {
            return Json(workflowfacade.GetAllWFSteps(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveWFStep(WFStepDto wfstepdto)
        {
            try
            {
                workflowfacade.SaveWFStep(wfstepdto);
                return Json(new { Result = "OK", Message = "WF step saved successfully" });
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteWFStep(int id)
        {
            try
            {
                workflowfacade.DeleteWFStep(id);
                return Json(new { Result = "Ok", Message = "WF Step deleted successfully" });

            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public string GetAllOfficeSettings()
        {
            var officesettinglist = workflowfacade.getAllWFSettings();
            string strret = "<select>";
            foreach (WFSettingDto item in officesettinglist)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
            //return Json(unitList, JsonRequestBehavior.AllowGet);
        }


	}
}