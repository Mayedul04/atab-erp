﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class KPITypeController : Controller
    {
        //
        // GET: /KPI/
        private readonly KPIFacade kpifacade;
        public KPITypeController()
        {
            kpifacade = new KPIFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetKPITypes()
        {
            var data = kpifacade.GetKPITypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SaveKPIType(KPITypeDto kpitypedto)
        {
            try
            {
                kpifacade.SaveKPIType(kpitypedto);
                return Json(new { Result = "OK", Record = "", Message = "KPI type saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteKPIType(int id)
        {
            try
            {
                kpifacade.DeleteKPIType(id);
                return Json(new { Result = "OK", Message = "KPI type deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
	}
}