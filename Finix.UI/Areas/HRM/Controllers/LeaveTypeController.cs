﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Finix.UI.Areas.HRM.Controllers
{
    public class LeaveTypeController : Controller
    {
        //
        // GET: /LeaveType/
        private readonly MenuFacade menuFacade;
        public LeaveTypeController()
        {
            this.menuFacade = new MenuFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetLeaveTypes()
        {
            var LeaveTypeList = menuFacade.GetLeaveTypes();
            return Json(LeaveTypeList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveLeaveType(LeaveTypeDto leavetypedto)
        {
            try
            {
                menuFacade.SaveLeaveTypes(leavetypedto);
                return Json(new { Result = "OK", Record = "", Message = "Leave type saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetLeaveApplicableTo()
        {
            List<KeyValuePair<int, string>> leaveAppto = UiUtil.EnumToKeyVal<LeaveApplicableTo>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in leaveAppto)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpPost]
        public JsonResult DeleteLeaveType(int id)
        {
            try
            {
                menuFacade.DeleteLeaveType(id);
                return Json(new { Result = "OK", Message = "Leave type deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        //public string GetLeaveApplicationStatus()
        //{
           
        //    //List<ModuleDto> modules = menufacade.GetModules();
        //    //// return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
        //    //string strret = "<select>";
        //    //foreach (ModuleDto item in modules)
        //    //{
        //    //    strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
        //    //}
        //    //strret += "</select>";
        //    //return strret;
        //}
	}
}