﻿using Finix.HRM.DTO;
using Finix.HRM.Facade;
using Finix.HRM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class AccountsController : Controller
    {
        public readonly HRMAccountsFacade _hrmAccounts = new HRMAccountsFacade();
        //public readonly BasicDataFacade _basicData = new BasicDataFacade();
        // GET: Southern/Accounts
        public ActionResult AccountHeadMapping(string searchString, int pageSize = 20, int pageCount = 1)
        {
            ViewBag.CurrentSort = searchString;
            var data = _hrmAccounts.GetAccHeadMappingList(pageSize, pageCount, searchString);
            return View(data);
        }
        public JsonResult GetRefOptions(AccountHeadRefType refType)
        {
            //if (refType == AccountHeadRefType.SalaryExpense || refType == AccountHeadRefType.SalaryPayable)
            //{
                
            //}
            //if (refType == AccountHeadRefType.PurchaseProductWise || refType == AccountHeadRefType.SalesProductWise)
            //{
            //    var data = new ProductFacade().GetProductCategories();
            //    if (data != null)
            //        return Json(data.Select(d => new { Id = d.Id, Name = d.Name }), JsonRequestBehavior.AllowGet);
            //}
            //if (refType == AccountHeadRefType.SupplierPayable)
            //{
            //    var data = _basicData.GetSuppliers();
            //    if (data != null)
            //        return Json(data.Select(d => new { Id = d.Id, Name = d.Name }), JsonRequestBehavior.AllowGet);
            //}
            //if (refType == AccountHeadRefType.SupplierPayable)
            //{
            //    var data = _basicData.GetBanks();
            //    if (data != null)
            //        return Json(data.Select(d => new { Id = d.Id, Name = d.Name }), JsonRequestBehavior.AllowGet);
            //}
            return Json("", JsonRequestBehavior.AllowGet);
            
        }
        public JsonResult GetAccHeads(AccountHeadRefType refType)
        {
            var data = _hrmAccounts.GetAccHeads(refType, (long)SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveAccHeadMapping(AccHeadMappingDto dto)
        {
            var result = _hrmAccounts.SaveAccHeadMapping(dto, SessionHelper.UserProfile.UserId);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}