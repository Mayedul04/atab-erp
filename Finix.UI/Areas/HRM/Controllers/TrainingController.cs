﻿using Finix.HRM.DTO;
using Finix.HRM.Facade;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class TrainingController : Controller
    {
        // GET: HRM/Training

        private TrainingFacade trainingfacade = new TrainingFacade();
        public ActionResult Training()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetTrainings()
        {
            return Json(trainingfacade.GetTrainings(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTraining(TrainingDto trainingdto)
        {
            try
            {
                trainingfacade.SaveTraining(trainingdto);
                return Json(new { Result = "OK", Message = "Training saved successfully" });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteTraining(int id)
        {
            try
            {
                trainingfacade.DeleteTraining(id);
                return Json(new { Result = "OK", Message = "Training deleted successfully" });

            }
            catch (Exception ex)
            {

                return Json(new { Result = "OK", Message = ex.Message });

            }
        }

        [HttpGet]
        public string GetTrainingTypes()
        {
            List<KeyValuePair<int, string>> trainingtypelist = UiUtil.EnumToKeyVal<TrainingType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in trainingtypelist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetCorrespondingVendors()
        {
            List<VendorDto> vendors = trainingfacade.GetVendors();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (VendorDto item in vendors)
            {
                strret += "<option value='" + item.Id + "'>" + item.VendorName + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetTrainingByVendors(int vendorid)
        {
            return Json(trainingfacade.GetTrainingByVendors(vendorid), JsonRequestBehavior.AllowGet);
        }

        
        # region Vendor
        private EducationFacade educationfacade = new EducationFacade();
        public ActionResult Vendor()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetVendors()
        {
            return Json(trainingfacade.GetVendors(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAddressFromPostCode(long postalid)
        {
            return Json(trainingfacade.GetAddressFromPostCode(postalid), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveVendors(VendorDto vendordto)
        {
            try
            {
                trainingfacade.SaveVendor(vendordto);
                return Json(new { Result = "OK", Message = "Vendor saved successfully" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteVendor(int id)
        {
            try
            {
                trainingfacade.DeleteVendor(id);
                return Json(new { Result = "OK", Message = "Vendor deleted successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message });
            }
        }
        #endregion
        
        #region Degree
        public ActionResult Degree()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetDegree()
        {
            return Json(educationfacade.GetDegrees(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDegree(DegreeInfoDto degreeinfodto)
        {

            try
            {
                educationfacade.SaveDegree(degreeinfodto);
                return Json(new { Result = "OK", Message = "Degree saved succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteDegree(int id)
        {

            try
            {
                educationfacade.DeleteDegree(id);
                return Json(new { Result = "OK", Message = "Degree deleted succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public string GetDegreeLevels()
        {
            List<KeyValuePair<int, string>> degreelevellist = UiUtil.EnumToKeyVal<DegreeLevel>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in degreelevellist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetDegreeMajors()
        {
            List<KeyValuePair<int, string>> degreemajorlist = UiUtil.EnumToKeyVal<DegreeMajor>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in degreemajorlist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public string GetInstitutes()
        {
            var institutes = educationfacade.GetInstitutes();
            string strret = "<select>";
            foreach (InstituteDto item in institutes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public JsonResult GetDegreeByInstitute(int instituteid)
        {
            return Json(educationfacade.GetDegreeByInstitute(instituteid), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Institute
        public ActionResult Institute()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetInstitute()
        {
            return Json(educationfacade.GetInstitutes(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveInstitute(InstituteDto institutedto)
        {

            try
            {
                educationfacade.SaveInstitute(institutedto);
                return Json(new { Result = "OK", Message = "Institute saved successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteInstitute(int id)
        {

            try
            {
                educationfacade.DeleteInstitute(id);
                return Json(new { Result = "OK", Message = "Institute deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public string GetCorrespondingInstitutes()
        {
            var institutes = educationfacade.GetInstitutes();
            string strret = "<select>";
            foreach (InstituteDto item in institutes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        #endregion
    }
}