﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class SalaryItemController : Controller
    {
        private SalaryItemFacade _salaryItem = new SalaryItemFacade();
        public SalaryItemController()
        {

        }

        public ActionResult SalaryItemIndex()
        {
            return View();
        }

        public JsonResult GetAllSalaryItems()
        {
            var result = _salaryItem.GetAllSalaryItems();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public string GetAllSalaryType()
        {

            List<KeyValuePair<int, string>> salaryItemType = UiUtil.EnumToKeyVal<SalaryItemType>();
            string strret = "<select>";
            foreach (var item in salaryItemType)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetAllSalaryStatus()
        {
            List<KeyValuePair<int, string>> salaryStatuses = UiUtil.EnumToKeyVal<SalaryItemStatus>();
            string strret = "<select>";
            foreach (var item in salaryStatuses)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetAllContributionTypes()
        {
            List<KeyValuePair<int, string>> contributionTypes = UiUtil.EnumToKeyVal<SalaryItemContributionType>();
            string strret = "<select>";
            foreach (var item in contributionTypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpPost]
        public JsonResult SaveSalaryItem(SalaryItemDto model)
        {
            var responce = _salaryItem.SaveSalaryItem(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }

        public string GetSalaryItemList()
        {
            List<SalaryItemDto> salaryitems = _salaryItem.GetAllSalaryItems();
            string strret = "<select>";
            foreach (var item in salaryitems)
            {
                if (item.IspercentOfBasic==true)
                {
                    strret += "<option value='" + item.Id + "'>" + item.Name + "%</option>";
                }
                else
                {
                    strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
                }
               
            }
            strret += "</select>";
            return strret;
        }

       
        #region Salary Old
        //public ActionResult SalaryGradeIndex()
        //{
        //    return View();
        //}

        //public JsonResult GetAllSalaryGrade()
        //{
        //    var result = _salaryItem.GetAllSalaryGrade();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        //public JsonResult SaveSalaryGrade(SalaryGradeDto model)
        //{
        //    var responce = _salaryItem.SaveSalaryGrade(model);
        //    return Json(responce, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult FieldIndex()
        //{
        //    return View();
        //}

        //public JsonResult GetAllFields()
        //{
        //    var result = _salaryItem.GetAllFields();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult SaveFields(FieldDto model)
        //{
        //    var responce = _salaryItem.SaveFields(model);
        //    return Json(responce, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult PayScale()
        //{
        //    return View();
        //}

        //public JsonResult GetAllPayScales()
        //{
        //    var result = _salaryItem.GetAllPayScales();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        //public JsonResult SaveGetAllPayScale(PayScaleDto model)
        //{
        //    var responce = _salaryItem.SaveGetAllPayScale(model);
        //    return Json(responce, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public string GetAllSalaryGradeForPayScale()
        //{
        //    List<SalaryGradeDto> salaryGradeDto = _salaryItem.GetAllSalaryGrade();
        //    string strret = "<select>";
        //    foreach (SalaryGradeDto item in salaryGradeDto)
        //    {
        //        strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
        //    }
        //    strret += "</select>";
        //    return strret;
        //}

        //[HttpGet] public string GetAllFieldsForPayScale()
        //{
        //    List<FieldDto> fieldDto = _salaryItem.GetAllFields(); 
        //    string strret = "<select>";
        //    foreach (FieldDto item in fieldDto)
        //    {
        //        strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
        //    }
        //    strret += "</select>";
        //    return strret;
        //}
        #endregion

    }
}