﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OfficeUnitController : Controller
    {
        //
        // GET: /OfficeUnit/
        private readonly OfficeFacade officefacade;
        public OfficeUnitController()
        {
            officefacade = new OfficeFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOfficeUnits()
        {
            var unitList = officefacade.GetOfficeUnit();
            return Json(unitList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOfficeUnit(OfficeUnitDto officeunitdto)
        {
            try
            {
                if (officeunitdto.UnitType > 0)
                {
                    officefacade.SaveOfficeUnit(officeunitdto);
                    return Json(new { Result = "OK", Message = "Office unit saved successfully" },
                        JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = "OK", Message = "Please Provide Unit type" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult DeleteOfficeUnit(int id)
        {
            try
            {
                officefacade.DeleteOfficeUnit(id);
                return Json(new { Result = "OK", Message = "Office Unit deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public string GetUnitTypes()
        {
            List<KeyValuePair<int, string>> unittypelist = UiUtil.EnumToKeyVal<UnitType>();
            string strret = "<select>";
            strret += "<option value='" + 0 + "'>" + "-Select--" + "</option>";
            foreach (KeyValuePair<int, string> item in unittypelist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetParentUnits(int unittype)
        {
            if (unittype == 1)
            {

                var list = new List<Object>
                {
                    new { Id =0, Name = "no parent" },

                };
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(officefacade.GetCorrespondingParentUnit(unittype), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public string GetAllOfficeUnits()
        {
            var unitList = officefacade.GetOfficeUnit();
            string strret = "<select>";
            foreach (OfficeUnitDto item in unitList)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
            //return Json(unitList, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //public JsonResult GetOfficeLayerLevels(int officelayerid)
        //{
        //    return Json(officefacade.GetOfficeLayerLevels(officelayerid), JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public string GetAllUnits()
        {
            List<KeyValuePair<int, string>> leaveApps = UiUtil.EnumToKeyVal<UnitType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in leaveApps)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetOfficeLayerById(int officelayerid)
        {
            return Json(officefacade.GetOfficeLayerById(officelayerid), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetOfficetUnits(int unittype)
        {
            // return Json(officefacade.GetOfficetUnits(unittype), JsonRequestBehavior.AllowGet);  
            var officeunits = officefacade.GetOfficetUnits(unittype);
            var parentunits = GetParentUnits(unittype);
            Object data = new { OfficeUnits = officeunits, ParentUnits = parentunits.Data };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetOfficeUnitsByUnitType(int unittype)
        {
            return Json(officefacade.GetOfficetUnits(unittype), JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetOfficeUnitSettingsByOfficeId(int officeid)
        {
            var data = officefacade.GetOfficeUnitSettingsByOfficeId(officeid);
            if (data != null)
            {
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(string.Empty, JsonRequestBehavior.AllowGet);

        }

        public JsonResult SaveOfficeUnitSetting(OfficeUnitSettingDto officeunitsettingdto)
        {
            try
            {
                officefacade.SaveOfficeUnitSetting(officeunitsettingdto);
                return Json(new { Result = "OK", Message = "Saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteOfficeUnitSetting(int id)
        {
            try
            {
                officefacade.DeleteOfficeUnitSetting(id);
                return Json(new { Result = "OK", Message = "Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOfficeUnitByOffice(int officeid)
        {
            return Json(officefacade.GetOfficeUnitByOffice(officeid), JsonRequestBehavior.AllowGet);
        }

    }
}