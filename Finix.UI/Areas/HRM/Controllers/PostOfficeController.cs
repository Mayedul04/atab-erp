﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class PostOfficeController : Controller
    {
        //
        // GET: /PostOffice/

        private readonly LocationFacade locationfacade;
        public PostOfficeController()
        {
            locationfacade = new LocationFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetPostOffices()
        {
            return Json(locationfacade.GetPostOffices(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePostOffice(PostOfficeDto postofficedto)
        {
            try
            {
                locationfacade.SavePostOffice(postofficedto);
                return Json(new { Result = "OK", Message = "Post office saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeletePostOffice(int id)
        {
            try
            {
                locationfacade.DeletePostOffice(id);
                return Json(new { Result = "OK", Message = "Post office deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public string GetLastLocations()
        {
            List<LocationDto> locationlist = locationfacade.GetLastLocations();
            string strret = "<select>";
            foreach (LocationDto item in locationlist)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
	}
}