﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class InstituteController : Controller
    {
        //
        // GET: /Institute/
        private readonly EducationFacade educationfacade;
        public InstituteController()
        {
            educationfacade = new EducationFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetInstitutes()
        {
            return Json(educationfacade.GetInstitutes(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveInstitute(InstituteDto institutedto)
        {
            
            try
            {
                educationfacade.SaveInstitute(institutedto);
                return Json(new { Result = "OK", Message = "Institute saved successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
            
        }

        [HttpPost]
        public JsonResult DeleteInstitute(int id)
        {
            
            try
            {
                educationfacade.DeleteInstitute(id);
                return Json(new { Result = "OK", Message = "Institute deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
           
        }

        [HttpGet]
        public string GetCorrespondingInstitutes()
        {
            var institutes = educationfacade.GetInstitutes();
            string strret = "<select>";
            foreach (InstituteDto item in institutes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        

	}
}