﻿using Finix.HRM.DTO;
using Finix.HRM.Facade;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class CalendarController : Controller
    {
        // GET: HRM/Calendar

        private CalendarFacade _calendar = new CalendarFacade();
        public CalendarController()
        { }

        public ActionResult Assign()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllDateData(long year, long month, string dayOne, string dayTwo)
        {
            var result = _calendar.GetAllDateData(year, month, dayOne, dayTwo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveDate(List<CalendarDto> model)
        {
            var result = _calendar.SaveDate(model);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public ActionResult HolidayIndex()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetHolidayTypes()
        {
            var HolidayTypes = _calendar.GetHolidayTypes();
            return Json(HolidayTypes, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveHolidayTypes(HolidayTypesDto model)
        {
            var responce = _calendar.SaveHolidayTypes(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteHolidayTypes(int id)
        {
            var response = _calendar.DeleteHolidayTypes(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #region FiscalYear

        public ActionResult FiscalYearIndex()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetAllFiscalYears()
        {
            var fiscalYears = _calendar.GetAllFiscalYears();
            return Json(fiscalYears, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult SaveFiscalYear(FiscalYearDto model)
        //{
        //    var responce = _calendar.SaveFiscalYear(model);
        //    return Json(responce, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public JsonResult SaveFiscalYear(FiscalYearDto model)
        {
            if (!string.IsNullOrEmpty(model.QuarterOneTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.QuarterOneTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.QuarterOne = changeDate;
            }
            if (!string.IsNullOrEmpty(model.QuarterTwoTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.QuarterTwoTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.QuarterTwo = changeDate;
            }
            if (!string.IsNullOrEmpty(model.QuarterThreeTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.QuarterThreeTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.QuarterThree = changeDate;
            }
            if (!string.IsNullOrEmpty(model.QuarterFourTxt))
            {
                DateTime changeDate;
                var fromConverted = DateTime.TryParseExact(model.QuarterFourTxt, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out changeDate);
                if (fromConverted)
                    model.QuarterFour = changeDate;
            }
            var responce = _calendar.SaveFiscalYear(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteFiscalYear(int id)
        {
            var response = _calendar.DeleteFiscalYear(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion
        [HttpGet]
        public string GetAllAppraisalFiscalYear()
        {
            var fiscalYears = _calendar.GetAllFiscalYears();
            //List<KeyValuePair<int, string>> appraisalStatus = UiUtil.EnumToKeyVal<AppraisalStatus>();
            string strret = "<select>";
            foreach (var item in fiscalYears)
            {
                strret += "<option value='" + item.Id + "'>" + item.Year + "</option>";
            }
            strret += "</select>";
            return strret;
        }
    }
}