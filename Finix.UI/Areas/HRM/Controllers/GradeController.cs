﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class GradeController : Controller
    {
        //
        // GET: /Grade/
         private readonly MenuFacade menuFacade;
         private readonly GradeStepFacade gradestepfacade;
        public GradeController()
        {
            this.menuFacade = new MenuFacade();
            gradestepfacade = new GradeStepFacade();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetGrades()
        {
            var GradeList = menuFacade.GetGrades();
            return Json(GradeList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveGrade(GradeDto gradedto)
        {
            try
            {
                menuFacade.SaveGrade(gradedto);
                return Json(new { Result = "OK", Record = "", Message = "Grade saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteGrade(int id)
        {
            try
            {
                menuFacade.DeleteGrade(id);
                return Json(new { Result = "OK", Message = "Grade deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult GetCorrespondingDesignation(int id)
        {
            var desList = menuFacade.getCorrespondingDesignations(id);
            return Json(desList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult EmptyJson()
        {
            var str = "";
            return Json(str, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GradeStep()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetGradeSteps()
        {
            return Json(gradestepfacade.GetAllGradeSteps(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveGradeStep(GradeStepDto gradestepdto)
        {
            try
            {
                gradestepfacade.SaveGradeStep(gradestepdto);
                return Json(new { Result = "OK", Message = "Grade step saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteGradeStep(int id)
        {
            try
            {
                gradestepfacade.DeleteGradeStep(id);
                return Json(new { Result = "OK", Message = "Grade step deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetGradeStepByGrade(int gradeid)
        {
            return Json(gradestepfacade.GetGradeStepByGrade(gradeid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GradeStepSalaryItem()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetGradeStepSalaryItems()
        {
            return Json(gradestepfacade.GetGradeStepSalaryItems(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveGradeStepSalaryItem(GradeStepSalaryItemDto gradestepsalaryitemdto)
        {
            try
            {
                gradestepfacade.SaveGradeStepSalary(gradestepsalaryitemdto);
                return Json(new { Result = "OK", Message = "Grade step salary saved" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveGradeStepSalarySetUp(int gradeid,int gradestepid,List<GradeStepSalaryItemDto> list)
        {
            try
            {
                gradestepfacade.SaveGradeStepSalarySetUp(gradeid, gradestepid, list);
                return Json(new { Result = "OK", Message = "Grade step salary saved" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteGradeStepSalaryItem(int id)
        {
            try
            {
                gradestepfacade.DeleteGradeStepSalaryItem(id);
                return Json(new { Result = "OK", Message = "Grade step salary deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        
        [HttpGet]
        public JsonResult GetGradeStepSalaryDto(int gradeid,int? gradestepid)
        {
            var data = gradestepfacade.GetGradeStepSalaryDto(gradeid, gradestepid);
            return Json(data,JsonRequestBehavior.AllowGet);
        }

        public ActionResult GradeStepSalarySetup()
        {
            return View();
        }
	}
}