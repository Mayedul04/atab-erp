﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using OfficeOpenXml;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OfficeOutTimeController : Controller
    {
        private OfficeOutTimeFacade _officeOutTime = new OfficeOutTimeFacade();
        public ActionResult OfficeOutTimeIndex()
        {
            return View();
        }

        public JsonResult GetAllAppraisals()
        {
            var result = _officeOutTime.GetAllOfficeOutTime();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //SaveOfficeOutTime
        [HttpPost]
        public JsonResult SaveOfficeOutTime(OfficeOutTimeDto model)
        {
            var responce = _officeOutTime.SaveOfficeOutTime(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteOfficeOutTime(int id)
        {
            var response = _officeOutTime.DeleteOfficeOutTime(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetAllEmployees()
        {
            var emplist = _officeOutTime.GetEmployees();
            string strret = "<select>";
            foreach (EmployeeDto item in emplist)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public ActionResult EmployeeAttendance()
        {
            return View();
        }

        public JsonResult GetTodaysAttendance()
        {
            return Json(_officeOutTime.GetTodaysAttendance(),JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetEmployeeAttendances()
        {

            return Json(_officeOutTime.GetEmployeeAttendances(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEmployeeAttendances(EmployeeAttendanceDto empattenddto)
        {
            try
            {
                _officeOutTime.SaveEmployeeAttendance(empattenddto);
                return Json(new { Result = "OK", Message = "Saved successfully" },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteEmployeeAttendance(int id)
        {
            try
            {
                _officeOutTime.DeleteEmployeeAttendance(id);
                return Json(new { Result = "OK", Message = "Deleted successfully" },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetInAndOutTime()
        {
            Object data=_officeOutTime.GetInAndOutTime();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalaryConfiguration()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOverTimeRules()
        {
            return Json(_officeOutTime.GetOverTimeRules(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOverTimeRule(List<OverTimeRuleDto> dtos)
        {
            try
            {
                _officeOutTime.SaveOverTimeRule(dtos);
                return Json(new { Result = "OK", Message = "Saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmployeeAttendanceExcel()
        {
            return View();
        }

        public JsonResult GetEmployeeAttendanceExcel(FormCollection formcollection)
        {
            var AttendanceList = new List<EmployeeAttendanceDto>();
            List<int> excludedRows = new List<int>();
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;


                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.Where(s => s.Name == "Attendance").FirstOrDefault();
                        if (workSheet == null)
                        {
                            workSheet = currentSheet.FirstOrDefault();
                        }
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        Object InAndOutTime = _officeOutTime.GetInAndOutTime();
                        System.Type type = InAndOutTime.GetType();
                        DateTime dtoffin = (DateTime)type.GetProperty("OfficeInTime").GetValue(InAndOutTime, null);
                        DateTime dtoffout = (DateTime)type.GetProperty("OfficeOutTime").GetValue(InAndOutTime, null);

                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                           // try
                           // {
                                var Attend = new EmployeeAttendanceDto();
                                //POD.ProductSizeCode = workSheet.Cells[rowIterator, 1].Value.ToString();
                                Attend.EmpCode =Convert.ToInt32(workSheet.Cells[rowIterator, 1].Value);
                                //string qty = workSheet.Cells[rowIterator, 2].Value.ToString();
                                Attend.EmpName = workSheet.Cells[rowIterator, 2].Value.ToString();
                                //string unitPrice = workSheet.Cells[rowIterator, 3].Value.ToString();
                                Attend.DesignationName = workSheet.Cells[rowIterator, 3].Value.ToString();
                                string date =workSheet.Cells[rowIterator, 4].Value.ToString().Substring(0,10);
                                Attend.Date = DateTime.ParseExact(date, "dd/mmm/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
                                DateTime t_in=(DateTime)workSheet.Cells[rowIterator, 5].Value;
                                var t_inn = t_in.TimeOfDay;
                                Attend.InTime = Attend.Date.Date + t_inn;
                                var t_out=(DateTime)workSheet.Cells[rowIterator, 6].Value;
                                var t_outt = t_out.TimeOfDay;
                                Attend.OutTime =Attend.Date.Date+t_outt;
                                Attend.EmpId = _officeOutTime.GetEmpByEmpCode(Attend.EmpCode).Id;
                                if (Attend.InTime.TimeOfDay>dtoffin.TimeOfDay)
                                {
                                    int late =(int)(Attend.InTime.TimeOfDay - dtoffin.TimeOfDay).TotalMinutes;
                                    Attend.Late = late;
                                }
                                else
                                {
                                    Attend.Late = 0;
                                }
                                int lategone = Attend.OutTime.TimeOfDay > dtoffout.TimeOfDay ? (int)(Attend.OutTime.TimeOfDay - dtoffout.TimeOfDay).TotalMinutes : 0;
                                if (lategone>Attend.Late)
                                {
                                    Attend.OverTime = lategone - Attend.Late;
                                }
                                else
                                {
                                    Attend.OverTime = 0;
                                }
                                if (_officeOutTime.GetAttendanceByEmpAndDate(Attend.EmpId,Attend.Date)==null)
                                {
                                    _officeOutTime.SaveEmployeeAttendance(Attend);
                                   
                                }

                                AttendanceList.Add(Attend);
                                
                           // }
                            //catch (Exception)
                            //{
                            //    excludedRows.Add(rowIterator);
                            //}
                        }
                    }

                }
            }

            return Json(AttendanceList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBasicConfiguration()
        {
            var result = _officeOutTime.GetBasicConfiguration();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}