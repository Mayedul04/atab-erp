﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class TourController : Controller
    {
        //
        // GET: /Tour/
        private readonly TourFacade tourfacade;
        private readonly EmployeeFacade employeefacade;
        public TourController()
        {
            tourfacade = new TourFacade();
            employeefacade = new EmployeeFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllTours()
        {
            return Json(tourfacade.GetAllTours(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTour(TourDto tourdto)
        {
            try
            {
                tourfacade.SaveTour(tourdto);
                return Json(new { Result = "OK", Message = "Tour saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message },JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteTour(int id)
        {
            try
            {
                tourfacade.DeleteTour(id);
                return Json(new { Result = "OK", Message = "Tour deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message },JsonRequestBehavior.AllowGet);
            }
        }

        public string GetTourTypes()
        {
            List<KeyValuePair<int, string>> tourtypes = UiUtil.EnumToKeyVal<TourType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in tourtypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetTourStatuses()
        {
            List<KeyValuePair<int, string>> tourstatuses = UiUtil.EnumToKeyVal<TourStatus>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in tourstatuses)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetAllTourParticipants()
        {
            return Json(tourfacade.GetAllTourParticipants(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTourParticipant(TourParticipantDto tourparticipantdto)
        {
            try
            {
                tourfacade.SaveTourParticipant(tourparticipantdto);
                return Json(new { Result = "OK", Message = "Tour participant saved successfully" },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result="ERROR",Message=ex.Message},JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult DeleteTourParticipant(int id)
        {
            try
            {
                tourfacade.DeleteTourParticipant(id);
                return Json(new { Result = "OK", Message = "Tour participant deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetCorrespondingTourParticipants(int id)
        {
            return Json(tourfacade.GetCorrespondingTourParticipants(id), JsonRequestBehavior.AllowGet);
        }

        public string GetCorrespondingSubstitutes(int id)
        {
            var obj = employeefacade.GetSubstituteLeave(id);
            System.Type type=obj.GetType();
            List<EmpBasicInfoDto> emplist = (List<EmpBasicInfoDto>)type.GetProperty("emplist").GetValue(obj, null);
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (EmpBasicInfoDto item in emplist)
            {
                strret += "<option value='" + item.Id + "'>" + item.FirstName + " " + item.LastName + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetCorrespondingEmployees()
        {
            List<EmpBasicInfoDto> employees = employeefacade.GetEmployeeList();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (EmpBasicInfoDto item in employees)
            {
                strret += "<option value='" + item.Id + "'>" + item.FirstName + " " + item.LastName + "</option>";
            }
            strret += "<option value='0'>none</option></select>";
            return strret;
        }
	}
}