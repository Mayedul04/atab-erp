﻿using Finix.HRM.DTO;
using Finix.HRM.Facade;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class EmployeeController : Controller
    {
        //private EmployeeFacade _employee = new EmployeeFacade();

        private readonly EmployeeFacade employeefacade;
        private readonly EducationFacade educationfacade;
        private readonly TrainingFacade trainingfacade;
        private readonly LocationFacade locationfacade;
        private readonly PickerFacade pickerFacade;
        private readonly MenuFacade menufacade;
        public EmployeeController()
        {
            menufacade = new MenuFacade();
            educationfacade = new EducationFacade();
            locationfacade = new LocationFacade();
            employeefacade = new EmployeeFacade();
            trainingfacade = new TrainingFacade();
            pickerFacade = new PickerFacade();
        }
        //
        // GET: /Employee/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEmployees()
        {
            var emplist = employeefacade.GetEmployees();
            return Json(emplist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBasicInfoById(int id)
        {
            return Json(employeefacade.GetBasicInfoById(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEmployeeList()
        {
            return Json(employeefacade.GetEmployeeListForEntryPage(SessionHelper.UserProfile.CurrentlyAccessibleCompanies), JsonRequestBehavior.AllowGet);
            //return Json(employeefacade.GetEmployeeList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEmployee(EmpBasicInfoDto basicinfodto, int? id = null)
        {
            string Message = "";
            try
            {
                employeefacade.SaveEmployee(basicinfodto, id);
                Message = "Basic info saved successfully";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return Json(Message);
        }


        [HttpGet]
        public JsonResult GetJoiningInfoById(int id)
        {
            JoiningInformationDto b = employeefacade.GetJoiningInfo(id);
            return Json(b, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult SaveContactInfo(ContactInformationDto contactinfo, int empid)
        {
            string Message = "";
            try
            {
                employeefacade.SaveContactInfo(contactinfo, empid);
                Message = "Contact info saved successfully";

            }
            catch (Exception ex)
            {
                Message = ex.Message;

            }
            return Json(Message);
        }

        [HttpGet]
        public JsonResult GetContactInfoById(int id)
        {
            return Json(employeefacade.GetContactInfoById(id), JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet]
        public JsonResult GetEducationInfo(int id)
        {
            List<EmployeeEducationDto> empedu = educationfacade.GetEmpEducations(id);
            return Json(empedu, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SaveEducationInfo(EmployeeEducationDto empedudto)
        {

            try
            {
                educationfacade.SaveEmpEducation(empedudto);

                return Json(new { Result = "OK", Message = "Education info saved successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteEducationInfo(int id)
        {
            try
            {
                educationfacade.DeleteEmpEducation(id);
                return Json(new { Result = "OK", Message = "Education Info deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult GetTrainingInfo(int empid)
        {
            List<TrainingInformationDto> traininginfo = employeefacade.GetTrainingInfo(empid);
            return Json(traininginfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveTrainingInfo(TrainingInformationDto traininginfodto)
        {

            try
            {
                employeefacade.SaveEmployeeTraining(traininginfodto);
                return Json(new { Result = "OK", Message = "Training info saved succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteTrainingInfo(int id)
        {
            try
            {
                employeefacade.DeleteEmployeeTraining(id);
                return Json(new { Result = "OK", Message = "Training info deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public JsonResult GetEmploymentHistory(int empid)
        {
            List<EmploymentHistoryDto> traininginfo = employeefacade.GetEmploymentHistory(empid);
            return Json(traininginfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveEmploymentHistory(EmploymentHistoryDto traininginfodto)
        {

            try
            {
                employeefacade.SaveEmploymentHistory(traininginfodto);
                return Json(new { Result = "OK", Message = "Employment History saved succesfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteEmploymentHistory(int id)
        {
            try
            {
                employeefacade.DeleteEmploymentHistory(id);
                return Json(new { Result = "OK", Message = "Employment History deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult SaveJoiningInfo(JoiningInformationDto joininginfodto)
        {
            string message = "";
            try
            {
                employeefacade.SaveJoiningInfo(joininginfodto);
                message = "Joining info saved successfully";

            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SaveUserInfo(UserInformationDto userinfo, int id)
        {
            string Message = "";
            try
            {
                employeefacade.SaveUserInformation(userinfo, id);
                Message = "User info saved successfully.";

            }
            catch (Exception ex)
            {
                Message = ex.Message;

            }
            return Json(Message);

        }

        [HttpGet]
        public JsonResult GetUserInfoById(int id)
        {
            return Json(employeefacade.GetUserInfoById(id), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteEmployee(int id)
        {
            try
            {
                employeefacade.DeleteEmployee(id);
                return Json(new { Result = "OK", Message = "Employee deleted successfully" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message });
            }

        }

        [HttpGet]
        public JsonResult GetGender()
        {
            List<KeyValuePair<int, string>> genderlist = UiUtil.EnumToKeyVal<Gender>();
            return Json(genderlist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEmployeeType()
        {
            List<KeyValuePair<int, string>> emptypelist = UiUtil.EnumToKeyVal<EmployeeType>();
            return Json(emptypelist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetWorkShift()
        {
            List<KeyValuePair<int, string>> workshiftlist = UiUtil.EnumToKeyVal<Workshift>();
            return Json(workshiftlist, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public string GetGenders()
        {
            List<KeyValuePair<int, string>> genderlist = UiUtil.EnumToKeyVal<Gender>();
            string strret = "<select>";
            foreach (var item in genderlist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public JsonResult GetMaritalStatus()
        {
            List<KeyValuePair<int, string>> maritalstatuslist = UiUtil.EnumToKeyVal<MaritalStatus>();
            return Json(maritalstatuslist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetMaritalStatuses()
        {
            List<KeyValuePair<int, string>> maritalstatuslist = UiUtil.EnumToKeyVal<MaritalStatus>();
            string strret = "<select>";
            foreach (var item in maritalstatuslist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetBloodGroup()
        {
            List<KeyValuePair<int, string>> bloodgrouplist = UiUtil.EnumToKeyVal<BloodGroup>();
            return Json(bloodgrouplist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetBloodGroups()
        {
            List<KeyValuePair<int, string>> bloodgrouplist = UiUtil.EnumToKeyVal<BloodGroup>();
            string strret = "<select>";
            foreach (var item in bloodgrouplist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetReligionList()
        {
            List<KeyValuePair<int, string>> religionlist = UiUtil.EnumToKeyVal<Religion>();
            return Json(religionlist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetReligions()
        {
            List<KeyValuePair<int, string>> religionlist = UiUtil.EnumToKeyVal<Religion>();
            string strret = "<select>";
            foreach (var item in religionlist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetEmployeeTypes()
        {
            List<KeyValuePair<int, string>> emptypes = UiUtil.EnumToKeyVal<EmployeeType>();
            string strret = "<select>";
            foreach (var item in emptypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        public JsonResult GetCorrespondingLeaveApplication(int id)
        {
            //List<LeaveApplicationDto> leaveapplications = new List<LeaveApplicationDto>();

            var apps = menufacade.GetLeaveApplications().ToList();
            return Json(apps, JsonRequestBehavior.AllowGet);

        }
        public string GetCorrespondingDesignationsJQGrid()
        {
            List<DesignationDto> designations = menufacade.GetDesignations();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (DesignationDto item in designations)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetCountries()
        {
            return Json(locationfacade.GetCountryList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetCorrespondingCountries()
        {
            List<CountryDto> countries = locationfacade.GetCountry();
            string strret = "<select>";
            foreach (CountryDto item in countries)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetCorrespondingCompanyProfile()
        {
            List<CompanyProfileDto> companyprofiles = menufacade.GetCompanyProfiles();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (CompanyProfileDto item in companyprofiles)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetCompanyProfiles()
        {

            return Json(menufacade.GetCompanyProfiles(), JsonRequestBehavior.AllowGet);
        }


        public string GetCorrespondingGrade()
        {
            List<GradeDto> grades = menufacade.GetGrades();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (GradeDto item in grades)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetCorrespondingDegrees()
        {
            List<DegreeInfoDto> degrees = educationfacade.GetDegrees();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (DegreeInfoDto item in degrees)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetCorrespondingInstitute()
        {
            List<InstituteDto> institutes = educationfacade.GetInstitutes();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (InstituteDto item in institutes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetPostOfficeByPostalCode(long postalcode)
        {
            return Json(menufacade.GetPostOfficeByPostalCode(postalcode), JsonRequestBehavior.AllowGet);
            // long AreaId = postoffice.LocationId;
            // addresslist.Add(PostOfficeId);
        }

        [HttpGet]
        public string GetCorrespondingPostOffice()
        {
            List<PostOfficeDto> postoffices = locationfacade.GetPostOffices();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (PostOfficeDto item in postoffices)
            {
                strret += "<option value='" + item.Id + "'>" + item.Code + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpGet]
        public JsonResult GetAreaId(long locid)
        {
            return Json(menufacade.GetAreaId(locid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetParentId(long locid)
        {
            return Json(menufacade.GetParentId(locid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGrades()
        {
            return Json(menufacade.GetGrades(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCorrespondingDesignations(int gradeid)
        {
            Object data = menufacade.GetCorrespondingDesignationsAndSteps(gradeid);
            //Object data = menufacade.GetCorrespondingDesignation(gradeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region EmployeeTransfer

        public ActionResult EmployeeTransferIndex()
        {
            return View();
        }

        public JsonResult GetAllEmployeeInfo()
        {
            var employees = employeefacade.GetAllEmployeeInfo();
            return Json(employees, JsonRequestBehavior.AllowGet);
        }
        //Employee/SaveEmployeeTransfer
        [HttpGet]
        public JsonResult GetEmployeeTransfers()
        {
            return Json(employeefacade.GetEmployeeTransfers(), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult SaveEmployeeTransfer(EmployeeTransferDto model)
        {
            try
            {
                employeefacade.SaveEmployeeTransfer(model);
                return Json(new { Result = "OK", Message = "Saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public string GetAllEmployees()
        {
            var emplist = employeefacade.GetEmployees();
            string strret = "<select>";
            foreach (EmployeeDto item in emplist)
            {
                strret += "<option value='" + item.Id + "'>" + item.BasicInfo.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public JsonResult DeleteEmployeeTransfer(int id)
        {
            try
            {
                employeefacade.DeleteEmployeeTransfer(id);
                return Json(new { Result = "OK", Message = "Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        #endregion
        #region EmployeePicker

        public ActionResult EmployeePicker()
        {
            return View();
        }
        //[HttpGet]
        //public JsonResult GetSelectedEmployees(int? unitId, int? officeUnitId, int? officeLayerId, int? officeId, int? gradeId, int? designationId, int? genderId)
        //{
        //    var response = pickerFacade.GetSelectedEmployees(unitId, officeUnitId, officeLayerId, officeId, gradeId, designationId, genderId);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public JsonResult GetSelectedEmployeesForOrganogram(long? unitId, long? officeUnitId, long? officeLayerId, long? officeId, long? parentId, long? designationId, long? empId, long? positionId)
        {
            var response = pickerFacade.GetSelectedEmployeesForOrganogram(unitId, officeUnitId, officeLayerId, officeId, parentId, designationId, empId, positionId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSelectedEmployees(long? unitId, long? officeUnitId, long? officeLayerId, long? officeId, long? parentId, long? designationId, long? empId, long? positionId)
        {
            var response = pickerFacade.GetSelectedOrganograms(unitId, officeUnitId, officeLayerId, officeId, parentId, designationId, empId, positionId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetPickedEmployees(List<EmployeePickerDto> model)
        {
            var data = model.Where(x => x.Pick == true).ToList();
            pickerFacade.GetPickedEmployees(data);
            var responce = "";
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult EmployeeJobHistoryOfficeWise()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetEmployeeDetails(int? id)
        {
            return Json(employeefacade.GetEmployeeDetails(id), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllActiveEmployees()
        {
            var employees = employeefacade.GetAllActiveEmployees();
            return Json(employees.Select(r => new { Id = r.Id, Name = r.Name }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReceivedReport(string reportTypeId, int? id)
        {

            var data = employeefacade.GetEmployeeDetails(id);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptEmpJbHistoryReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeJobHistoryOfficeWise");
            }

            ReportDataSource rd = new ReportDataSource("EmpJobHistoryDataSet", data);


            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);

        }
        public ActionResult EmployeeJobHistory()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEmployeeDetailsOfficewise(int? id)
        {
            return Json(employeefacade.GetEmployeeDetailsOfficewise(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobHistoryReport(string reportTypeId, int? id)
        {

            var data = employeefacade.GetEmployeeDetailsOfficewise(id);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptEmpJbHistoryReport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeJobHistoryOfficeWise");
            }

            ReportDataSource rd = new ReportDataSource("EmpJobHistoryDataSet", data);


            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);
        }

        public ActionResult PaySlip()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetPayslipForEmployee(int? year, int? month, int? id)
        {
            return Json(employeefacade.GetPayslipForEmployee(year, month, id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPaySlipReport(string reportTypeId, int? year, int? month, int? id)
        {
            var data = employeefacade.GetPayslipForEmployee(year, month, id);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptPaySlipReportForAllEmp.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeJobHistoryOfficeWise");
            }

            ReportDataSource rd = new ReportDataSource("dsPaySlip", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

               "<DeviceInfo>" +
             "  <OutputFormat>EMF</OutputFormat>" +
             "  <PageWidth>7.8in</PageWidth>" +
             "  <PageHeight>5.83in</PageHeight>" +
             "  <MarginTop>0.25in</MarginTop>" +
             "  <MarginLeft>0.25in</MarginLeft>" +
             "  <MarginRight>0.25in</MarginRight>" +
             "  <MarginBottom>0.25in</MarginBottom>" +
             "</DeviceInfo>";
           
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);
        }
        /* GetEmployeeDetails
         * Developed By - Sabiha Mehnaz GetEmployeeDepartmentWise
         */

        public JsonResult GetEmployeesCurrentlyOnLeave()
        {
            return Json(employeefacade.GetEmployeesCurrentlyOnLeave(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult EmployeeReportDepartmentWise()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetEmployeeDepartmentWise(int? officeId, int? empType)
        {
            return Json(employeefacade.GetEmployeeDepartmentWise(officeId, empType), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDepartmentWiseEmployee(string reportTypeId, int? officeId, int? empType)
        {
            var data = employeefacade.GetEmployeeDepartmentWise(officeId, empType);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptDeptWiseEmployee.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeReportDepartmentWise");
            }

            ReportDataSource rd = new ReportDataSource("dsDeptWiseEmp", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);
        }
        //GetSalaryWiseEmployee
        public ActionResult EmployeeSalWiseEmployee()
        {
            return View();
        }
        public JsonResult GetSalaryWiseEmployee(int? fromAmount, int? toAmount)
        {
            return Json(employeefacade.GetSalaryWiseEmployee(fromAmount, toAmount), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalaryRangeWiseEmp(string reportTypeId, int? fromAmount, int? toAmount)
        {
            var data = employeefacade.GetSalaryWiseEmployee(fromAmount, toAmount);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptSalWiseEmployee.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeSalWiseEmployee");
            }

            ReportDataSource rd = new ReportDataSource("dsPaySlip", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);
        }

        public ActionResult EmployeeBasicInfotWiseReport()
        {
            return View();
        }

        //GetAllTrainings GetAllEducations GetBasicInfoWiseEmp

        [HttpGet]
        public JsonResult GetAllTrainings()
        {
            var trainingList = trainingfacade.GetTrainings();
            return Json(trainingList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllEducations()
        {
            var educationList = educationfacade.GetAllDegrees();
            return Json(educationList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetBasicWiseEmployee(int? trainId, int? educationId, int? bloodId, DateTime? fromDate, DateTime? toDate)
        {
            return Json(employeefacade.GetBasicWiseEmployee(trainId, educationId, bloodId, fromDate, toDate), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBasicInfoWiseEmp(string reportTypeId, int? trainId, int? educationId, int? bloodId, DateTime? fromDate, DateTime? toDate)
        {
            var data = employeefacade.GetBasicWiseEmployee(trainId, educationId, bloodId, fromDate, toDate);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptBasicInfo.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeBasicInfotWiseReport");
            }

            ReportDataSource rd = new ReportDataSource("dsBasicInfo", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult EmployeeHistoryReport()
        {
            return View();
        }
        //GetJobHisWiseEmployee GetJobHisWiseEmployee GetJobHistoryEmpReport
        [HttpGet]
        public JsonResult GetJobHisWiseEmployee(int? empHistoryId, DateTime? fromDate, DateTime? toDate)
        {
            return Json(employeefacade.GetJobHisWiseEmployee(empHistoryId, fromDate, toDate), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobHistoryEmpReport(string reportTypeId, int? empHistoryId, DateTime? fromDate, DateTime? toDate)
        {
            var data = employeefacade.GetJobHisWiseEmployee(empHistoryId, fromDate, toDate);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptEmpJbHistoryReport.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeHistoryReport");
            }

            ReportDataSource rd = new ReportDataSource("EmpJobHistoryDataSet", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        /*Employee Promotion Report*/
        public ActionResult EmployeePromotionHistoryReport()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetPromotionHisWiseEmployee(DateTime? fromDate, DateTime? toDate)
        {
            return Json(employeefacade.GetPromotionWiseEmployee(fromDate, toDate), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPromotionEmpReport(string reportTypeId, DateTime? fromDate, DateTime? toDate)
        {
            var data = employeefacade.GetPromotionWiseEmployee(fromDate, toDate);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptEmpPromotionReport.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeePromotionHistoryReport");
            }

            ReportDataSource rd = new ReportDataSource("dsPromotionHistoryDataSet", data);

            lr.DataSources.Add(rd);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }


        [HttpGet]
        public JsonResult GetGradeStepsByGradeId(int? id)
        {
            var gradeStepList = employeefacade.GetGradeStepsByGradeId(id);
            return Json(gradeStepList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetEmployeeChildOffices()
        {
            var gradeStepList = employeefacade.GetEmployeeChildOffices(SessionHelper.UserProfile.UserId);
            return Json(gradeStepList, JsonRequestBehavior.AllowGet);
        }
    }
}