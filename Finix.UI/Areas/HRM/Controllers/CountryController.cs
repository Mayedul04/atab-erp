﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class CountryController : Controller
    {
        //
        // GET: /Country/
        private readonly LocationFacade locationfacade;
        public CountryController()
        {
            locationfacade = new LocationFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetCountries()
        {
            var data = locationfacade.GetCountry();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCountry(CountryDto countrydto)
        {
            try
            {
                locationfacade.SaveCountry(countrydto);
                return Json(new { Result = "OK", Record = "", Message = "Country saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteCountry(int id)
        {
            try
            {
                locationfacade.DeleteCountry(id);
                return Json(new { Result = "OK", Message = "Country deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        private void Test()
        {

        }
    }
}