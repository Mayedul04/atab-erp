﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WinForms;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class SalaryProcessController : Controller
    {
        //
        // GET: /SalaryProcess/
        private readonly SalaryItemFacade salaryItemFacade;
        private readonly SalaryProcessFacade salaryProcessFacade;
        private readonly LeaveSupervisionFacade leavesupervisionfacade;
        public SalaryProcessController()
        {
            salaryItemFacade = new SalaryItemFacade();
            salaryProcessFacade = new SalaryProcessFacade();
            leavesupervisionfacade = new LeaveSupervisionFacade();
        }
        public ActionResult SalaryProcessEdit()
        {
            return View();
        }
        public JsonResult GetYearMonthCombo()
        {
            return Json(salaryItemFacade.GetYearMonthCombo(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSalaryProcessesForPeriod(string YearMonth,int? empid, long officeid)
        {
            return Json(salaryItemFacade.GetSalaryProcessesForPeriod(YearMonth,empid,officeid),JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSalaryProcessDetail(SalaryProcessEditDto dto)
        {
            try
            {
                salaryItemFacade.SaveSalaryProcessDetail(dto);
                return Json(new { Result = "OK", Message = "Saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public JsonResult GetTotalSalaryByTimePeriod(string YearMonth, int? empid, long officeid)
        {
            Object data = salaryItemFacade.GetTotalSalaryByTimePeriod(YearMonth, empid, officeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSalaryProcessDetail(int id)
        {
            try
            {
                salaryItemFacade.DeleteSalaryProcess(id);
                return Json(new { Result = "OK", Message = "Deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SalaryProcessInterface()
        {
            return View();
        }

        public JsonResult GetSalaryComponents()
        {
            return Json(salaryItemFacade.GetSalaryComponents(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DoSalaryProcess(List<SalaryComponentDto> salcomponent)
        {
            return Json(salaryItemFacade.DoSalaryProcess(salcomponent), JsonRequestBehavior.AllowGet);
            
        }
        /****************************************Sabiha Modified (Salary Process) ********************************/
        // GetDesignationWiseOffice  GetOfficeWiseEmployee?officeId
        public ActionResult SalaryProcessIndex()
        {
            return View(); 
        }
        public JsonResult GetDesignationList()
        {
            var result = salaryProcessFacade.GetDesignationList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDesignationWiseGradestep(int designationId)
        {
            var result = salaryProcessFacade.GetDesignationWiseGradestep(designationId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignationWiseOfficeUnit(int designationId)
        {
            var result = salaryProcessFacade.GetDesignationWiseOfficeUnit(designationId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignationWiseOffice(int officeUnit)
        {
            var result = salaryProcessFacade.GetDesignationWiseOffice(officeUnit);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOfficeWiseEmployee(int officeId)
        {
            var result = salaryProcessFacade.GetOfficeWiseEmployee(officeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSalaryProcess(SalaryProcessDto salaryProcess)
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            salaryProcess.EditorId = employeeid ;
            //try
            //{

                var result=salaryProcessFacade.SaveSalaryProcess(salaryProcess);
                //System.Threading.Thread.Sleep(5000);   
                return Json(result, JsonRequestBehavior.AllowGet); //new { Result = "OK", Record = "", Message = "Leave application saved successfully" }
            //}
            //catch (Exception ex)
            //{
            //    return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            //}
        }

        public ActionResult SalaryItemReport()
        {
            return View();
        }

        public JsonResult GetSalaryProcesses(int? salaryItemId, int? year, int? month)
        {
            return Json(salaryProcessFacade.GetSalaryProcesses(salaryItemId, year, month), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSalaryItems()
        {
            var result = salaryProcessFacade.GetSalaryItems();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalaryProcessReport(string reportTypeId, int? salaryItemId, int? year, int? month)
        {

            var data = salaryProcessFacade.GetSalaryProcesses(salaryItemId, year, month);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptSalaryItemAmount.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("SalaryItemReport");
            }

            ReportDataSource rd = new ReportDataSource("dsSalaryItemAmount", data);
            lr.DataSources.Add(rd);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        /****************************************Sabiha Modified (Salary Process) ********************************/ //SaveSalaryProcess

        public JsonResult SalaryPosting(long fiscalYearId, long monthId,long officeId)
        {
            var result = salaryProcessFacade.SalaryPosting( fiscalYearId,  monthId, officeId,SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}