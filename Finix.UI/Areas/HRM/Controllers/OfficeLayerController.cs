﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OfficeLayerController : Controller
    {
        //
        // GET: /OfficeLayer/
        private readonly OfficeFacade officefacade;
        public OfficeLayerController()
        {
            this.officefacade = new OfficeFacade();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOfficeLayers()
        {
            var officeLayers = officefacade.GetOfficeLayers();
            return Json(officeLayers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOfficeLayer(OfficeLayerDto officelayerdto)
        {
            try
            {
                officefacade.SaveOfficeLayer(officelayerdto);
                return Json(new { Result = "OK", Message = "Office layer saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteOfficeLayer(long id)
        {
            try
            {
                officefacade.DeleteOfficeLayer(id);
                return Json(new { Result = "OK", Message = "Office layer deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public string GetAllOfficeLayers()
        {
            //return Json(officefacade.GetOffice(), JsonRequestBehavior.AllowGet);
            var officeList = officefacade.GetOfficeLayers();
            string strret = "<select>";
            foreach (OfficeLayerDto item in officeList)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetAllOfficeLayersJsonResult()
        {
            var data = officefacade.GetAllOfficeLayersJsonResult();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
	}
}