﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OfficeController : BaseController
    {
        //
        // GET: /Office/
        private readonly OfficeFacade officefacade;
        private readonly Finix.FAMS.Facade.LocationFacade _facade = new Finix.FAMS.Facade.LocationFacade();

        public OfficeController()
        {
            officefacade = new OfficeFacade();

        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOffice()
        {
            ResolveJqFilterData(officefacade);
            return Json(officefacade.GetOffice(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public string GetOffices()
        {
            List<OfficeDto> countries = officefacade.GetOffices();
            string strret = "<select>";
            foreach (OfficeDto item in countries)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        [HttpPost]
        public JsonResult SaveOffice(OfficeDto officedto)
        {
            
            try
            {
               long officeId = officefacade.SaveOffice(officedto, SessionHelper.UserProfile.UserId,SessionHelper.UserProfile.SelectedCompanyId);
                _facade.SaveLocation(officeId,officedto.Name, SessionHelper.UserProfile.UserId);
               return Json(new { Result = "OK", Message = "Office saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteOffice(int id)
        {
            try
            {
                officefacade.DeleteOffice(id);
                return Json(new { Result = "OK", Message = "Office deleted successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public string GetOfficeLayerLevels()
        {
            var officelevels = officefacade.GetOfficeLayers();
            string strret = "<select>";
            foreach (OfficeLayerDto item in officelevels)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetParentOffices(int officelayerid)
        {
            return Json(officefacade.GetCorrespondingParentOffices(officelayerid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOfficeByLayers(int officelevel)
        {
            return Json(officefacade.GetOfficesByLayer(officelevel), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOfficeByLayer(int officelayerid)
        {
            return Json(officefacade.GetOfficesByLayerId(officelayerid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetAllOffice()
        {
            //return Json(officefacade.GetOffice(), JsonRequestBehavior.AllowGet);
            var officeList = officefacade.GetOffice();
            string strret = "<select>";
            foreach (OfficeDto item in officeList)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetAllOffices(int officeLayerId)
        {
            return Json(officefacade.GetAllOffices(officeLayerId), JsonRequestBehavior.AllowGet);
        }

        /*
         * Developed By Sabiha
         */
        [HttpGet]
        public JsonResult GetAllActiveOffices()
        {
            return Json(officefacade.GetAllActiveOffices(), JsonRequestBehavior.AllowGet);
        }
        /**/
        [HttpGet]
        public string GetOfficeByOfficeLayer(int officelayerid)
        {
            //return Json(officefacade.GetOffice(), JsonRequestBehavior.AllowGet);
            var officeList = officefacade.GetOffice().ToList();
            string strret = "<select>";
            if (officeList.Any())
            {
                officeList = officeList.Where(x => x.OfficeLayerId == officelayerid).ToList();
                foreach (OfficeDto item in officeList)
                {
                    strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
                }
            }
            strret += "</select>";
            return strret;
        }

    }
}