﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class LocationController : BaseController
    {
        //
        // GET: /Location/
        private readonly LocationFacade locationfacade;

        public LocationController()
        {

            this.locationfacade = new LocationFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [JqGridActionFilter]
        public JsonResult GetLocations()
        {
            ResolveJqFilterData(locationfacade);
            var locationlist = locationfacade.GetLocation();
            return Json(locationlist, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLocationHierarchy(long? parentid)
        {
            var locationlist = locationfacade.GetLocationHierarchy(parentid);
            return Json(locationlist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveLocation(LocationDto locationdto)
        {
            try
            {
                locationfacade.SaveLocation(locationdto);
                return Json(new { Result = "OK", Message = "Location saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteLocation(int id)
        {
            try
            {
                locationfacade.DeleteLocation(id);
                return Json(new { Result = "OK", Message = "Location deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public string GetLocationTypes()
        {
            List<KeyValuePair<int, string>> locationtypelist = UiUtil.EnumToKeyVal<LocationType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in locationtypelist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public string GetLocationLevels()
        {
            List<KeyValuePair<int, string>> locationlevellist = UiUtil.EnumToKeyVal<LocationLevel>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in locationlevellist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetParentLocations(int loclevel)
        {
            if (loclevel==1)
            {

                var list = new List<Object>
                { 
                    new { Id =0, Name = "no parent" },
                    
                };
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(locationfacade.GetCorrespondingParentLocations(loclevel), JsonRequestBehavior.AllowGet);
            }
        }

       
	}
}