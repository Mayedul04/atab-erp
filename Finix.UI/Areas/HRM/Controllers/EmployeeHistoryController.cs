﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class EmployeeHistoryController : Controller
    {
        //
        // GET: /EmployeeHistory/
        private readonly EmployeeHistoryFacade EmpHistoryFacade;
        public EmployeeHistoryController()
        {
            EmpHistoryFacade = new EmployeeHistoryFacade();
        }
        public ActionResult EmployeeTranfer()
        {
            return View();
        }

        public JsonResult SaveTransferHistory(EmployeeTransferDto transferdto)
        {
            string Message = "";
            try
            {
                EmpHistoryFacade.SaveTransferHistory(transferdto);
                Message = "Transfered successfully";
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }
            return Json(Message);
        }

        public JsonResult GetRespectiveOfficeInfo(int empid)
        {
            Object data=EmpHistoryFacade.GetRespectiveOfficeInfo(empid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRespectiveEmpInfo(int empid)
        {
            Object data = EmpHistoryFacade.GetRespectiveEmpInfo(empid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeePromotion()
        {
            return View();
        }

        public JsonResult GetRespectivePostingInfo(int empid)
        {
            Object data = EmpHistoryFacade.GetRespectivePostingInfo(empid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePromotionHistory(EmployeeTransferDto transferdto)
        {
            string Message = "";
            try
            {
                EmpHistoryFacade.SavePromotionHistory(transferdto);
                Message = "Promoted successfully";
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }
            return Json(Message);
        }

        public ActionResult EmployeeResignation()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveResignationHistory(EmployeeTransferDto transferdto)
        {
            string Message = "";
            try
            {
                EmpHistoryFacade.SaveResignationHistory(transferdto);
                Message = "Resigned successfully";
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }
            return Json(Message);
        }
	}
}