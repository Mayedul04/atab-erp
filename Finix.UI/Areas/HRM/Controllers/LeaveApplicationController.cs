﻿using Finix.HRM.Facade;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.DTO;
using Finix.HRM.Util;
using Finix.HRM.Infrastructure;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WinForms;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class LeaveApplicationController : Controller
    {
        //
        // GET: /LeaveApplication/
         private readonly MenuFacade menuFacade;
         private readonly EmployeeFacade employeefacade;
         private readonly LeaveSupervisionFacade leavesupervisionfacade;
        public LeaveApplicationController()
        {
            this.menuFacade = new MenuFacade();
            employeefacade = new EmployeeFacade();
            leavesupervisionfacade = new LeaveSupervisionFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetLeaveApplications()
        {
            var LeaveAppList = menuFacade.GetLeaveApplications();
            return Json(LeaveAppList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveLeaveApplication(LeaveApplicationDto leaveappdto)
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            leaveappdto.EmployeeId = employeeid;
            try
            {
                menuFacade.SaveLeaveApplication(leaveappdto);
                return Json(new { Result = "OK", Record = "", Message = "Leave application saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteLeaveApplication(int id)
        {
            try
            {
                menuFacade.DeleteLeaveApplication(id);
                return Json(new { Result = "OK", Message = "Leave application deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public string GetCorrespondingEmployees()
        {
            List<EmpBasicInfoDto> employees = employeefacade.GetEmployeeList();
           
            string strret = "<select>";
            foreach (EmpBasicInfoDto item in employees)
            {
                strret += "<option value='" + item.Id + "'>" + item.FirstName+" "+item.LastName + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetCorrespondingLeaveTypes() 
        {
            List<LeaveTypeDto> leavetypes = menuFacade.GetLeaveTypes();
            string strret = "<select>";
            foreach (LeaveTypeDto item in leavetypes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetCorrespondingLeaveApplicationStatus()
        {
             List<KeyValuePair<int,string>> leaveApps=UiUtil.EnumToKeyVal<LeaveApplicationStatus>();
             string strret = "<select>";
             foreach (KeyValuePair<int,string> item in leaveApps)
             {
                 strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
             }
             strret += "</select>";
             return strret;
        }

        public ActionResult LeaveSupervision()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetLeaveSupervisions()
        {
            return Json(leavesupervisionfacade.GetApplicationsForSupervisions(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCommentHistory()
        {
            return Json(leavesupervisionfacade.GetCommentHistory(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveLeaveSupervisions(LeaveSupervisionDto leavesupervisiondto)
        {
            string Message = "";
            try
            {
                leavesupervisionfacade.ModifySupervisedLeaveApplication(leavesupervisiondto);
                Message = "Updated successfully";
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }
            return Json(Message);
        }

        public ActionResult LeaveStatus()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEmployeeLeaveStatus()
        {
            return Json(leavesupervisionfacade.GetAllEmployees(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLeaveStatusesForEmployee(int id)
        {
            return Json(leavesupervisionfacade.GetLeaveStatusesForEmployee(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContactForEmployee()
        {
            return Json(GetContactForEmployee(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubstitutorForLeave(int id)
        {
            Object employees = employeefacade.GetSubstituteLeave(id);
            return Json(employees, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LeaveApplicationForSession()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetLeaveApplicationForSession(int id)
        {
            //return Json(leavesupervisionfacade.GetLeaveApplicationForSession(id),JsonRequestBehavior.AllowGet);
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            return Json(leavesupervisionfacade.GetLeaveApplicationForSession((int)employeeid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLeaveApplicationDetailForSession(int leaveid)
        {
            return Json(leavesupervisionfacade.GetLeaveApplicationDetailForSession(leaveid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LeaveReport()
        {
            return View();
        }
       

        [HttpGet]
        public JsonResult GetLeaveApplicationsForReport(long? monthId, long? leaveId, DateTime? fromDate, DateTime? toDate)
        {
            var data = leavesupervisionfacade.GetLeaveApplicationsForReport(monthId, leaveId, fromDate, toDate);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLeaveApplicationReport(string reportTypeId, long? monthId, long? leaveId, DateTime? fromDate, DateTime? toDate)
        {

            var data = leavesupervisionfacade.GetLeaveApplicationsForReport(monthId, leaveId, fromDate, toDate);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptLeaveApplication.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("LeaveReport");
            }

            ReportDataSource rd = new ReportDataSource("dsLeaveApp", data);
            lr.DataSources.Add(rd);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult LeaveReportOfficeWise()
        {
            return View();
        }
        //GetLeaveApplicationsForReport

        [HttpGet]
        public JsonResult GetLeaveApplicationsForOfficeReport(long? officeId)
        {
            var data = leavesupervisionfacade.GetLeaveApplicationsForOfficeReport(officeId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLeaveApplicationReportOfficeWise(string reportTypeId, long? monthId, long? officeId)
        {

            var data = leavesupervisionfacade.GetLeaveApplicationsForOfficeReport(officeId);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptLeaveApplicationOfficeWise.rdlc"); //rptPaySlipReport
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("LeaveReport");
            }

            ReportDataSource rd = new ReportDataSource("dsLeaveAppOfficeWise", data);
            lr.DataSources.Add(rd);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult AdminLeaveApplictionIndex()
        {
            return View();
        }
        //GetLeaveApplicationsForApproval ForwardLeaveApplication
        [HttpGet]
        public JsonResult GetLeaveApplicationsForApproval()
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = leavesupervisionfacade.GetLeaveApplicationsForApproval(employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ForwardLeaveApplication(int id)
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = leavesupervisionfacade.GetLeaveApplicationsForApproval(employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LeaveSupervisionAsAdmin()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetSeniorsList()
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            var data = leavesupervisionfacade.GetSeniorsList((int)employeeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //SaveLeaveSupervisor
        [HttpPost]
        public JsonResult SaveLeaveSupervisor(LeaveApplicationDto leaveApplication)
        {
            var data = leavesupervisionfacade.SaveLeaveSupervisor(leaveApplication);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public string GetSeniorsLeaveApplicationStatus()
        {
            long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            //List<KeyValuePair<int, string>> leaveApps = UiUtil.EnumToKeyVal<LeaveApplicationStatus>();
            var leaveApps = leavesupervisionfacade.GetSeniorsList((int)employeeid);
            string strret = "<select>";
            foreach (var item in leaveApps)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
	}
}