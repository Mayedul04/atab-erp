﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class KPIController : Controller
    {
        //
        // GET: /KPI/
        private readonly KPIFacade kpifacade;
        public KPIController()
        {
            kpifacade = new KPIFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetKPIList(int kpitypeid = 0)
        {
            var data = kpifacade.GetKPIs(kpitypeid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveKPI(KPIDto kpidto)
        {
            try
            {
                kpifacade.SaveKPI(kpidto);
                return Json(new { Result = "OK", Message = "KPI saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteKPI(int id)
        {
            try
            {
                kpifacade.DeleteKPI(id);
                return Json(new { Result = "OK", Message = "KPI deleted successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }

        }

        [HttpGet]
        public string GetCorrespondingKPIType()
        {
            List<KPITypeDto> kpitypes = kpifacade.GetKPITypes();
            // return "<select><option value='1'>Sideboom</option><option value='2'>Truck</option><option value='3'>Car</option></select>";
            string strret = "<select>";
            foreach (KPITypeDto item in kpitypes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public ActionResult KPIGroup()
        {
            return View();
        }
        [HttpGet]
        public JsonResult getKPIGroups()
        {
            return Json(kpifacade.GetKPIGroups(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveKPIGroup(KPIGroupDto kpigroupdto)
        {
            try
            {
                kpifacade.SaveKPIGroup(kpigroupdto);
                return Json(new { Result = "OK", Message = "KPI Group saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message =ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteKPIGroup(int id)
        {
            try
            {
                kpifacade.DeleteKPIGroup(id);
                return Json(new { Result = "OK", Message = "KPI Group deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message =ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetCorrespondingKPIGroups()
        {
            List<KPIGroupDto> kpigroups = kpifacade.GetKPIGroups();
            string strret = "<select>";
            foreach (KPIGroupDto item in kpigroups)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetKpiForKpiGroups(int id)
        {
            return Json(kpifacade.GetKPIs(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetKPIGroupsForAppraisal()
        {
            List<KPIGroupDto> kpitypes = kpifacade.GetKPIGroups();

            string strret = "<select>";
            foreach (KPIGroupDto item in kpitypes)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;

        }

        [HttpGet]
        public string GetKPIForAppraisal()
        {
            List<KPIDto> kpis = kpifacade.GetKPIForAppraisal();

            string strret = "<select>";
            foreach (KPIDto item in kpis)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;

        }
        //GetAllKpiGroupSettings
        public JsonResult GetAllKpiGroupSettings()
        {
            var result = kpifacade.GetAllKpiGroupSettings();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveKPIGroupSettings(KPIGroupSettingDto model)
        {
            var responce = kpifacade.SaveKPIGroupSettings(model);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteKPIGroupSettings(int id)
        {
            var response = kpifacade.DeleteKPIGroupSettings(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

	}
}