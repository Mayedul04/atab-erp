﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Util;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OrganogramController : Controller
    {
        //
        // GET: /Organogram/
        private readonly OfficeFacade officefacade;
        public OrganogramController()
        {
            officefacade = new OfficeFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOrganograms()
        {
            return Json(officefacade.GetOrganograms(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveOrganogram(OrganogramDto organogramdto)
        {

            try
            {
                var response = officefacade.SaveOrganogram(organogramdto, SessionHelper.UserProfile.UserId);
                return Json(response, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult DeleteOrganogram(int id)
        {
            
            try
            {
                officefacade.DeleteOrganogram(id);
                return Json(new { Result = "OK", Message = "Organogram deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public string GetUnitTypes()
        {
            List<KeyValuePair<int, string>> unittypelist = UiUtil.EnumToKeyVal<UnitType>();
            string strret = "<select>";
            foreach (KeyValuePair<int, string> item in unittypelist)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetCorrespondingOfficeUnit(int unittype)
        {
            return Json(officefacade.GetCorrespondingOfficeUnit(unittype), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string GetOfficePositions()
        {
            var officepositions = officefacade.GetOfficePositions();
            string strret = "<select>";
            foreach (OfficePositionDto item in officepositions)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        [HttpGet]
        public JsonResult GetParentOrganogramsByPosition(int positionid, int officeid)
        {
            var parent=officefacade.GetParentOrganogramsByPosition(positionid, officeid);
            return Json(parent, JsonRequestBehavior.AllowGet);
        }

      
        [HttpGet]
        public string GetParentOrganogramsByPositionByGrid(int positionid, int officeid)
        {
            var parent = officefacade.GetParentOrganogramsByPosition(positionid, officeid);
            //var officepositions = officefacade.GetOfficePositions();
            string strret = "<select>";
            foreach (OrganogramDto item in parent)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
        //[HttpGet]
        //public JsonResult GetEmployeesByOffice(int officeid)
        //{
        //    var data = officefacade.GetEmployeesByOffice(officeid);
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public JsonResult GetEmployeesByOffice(int officeid,long ? officeLayerId ,long? designationId)
        {
            var data = officefacade.GetEmployeeListByOffice(officeid, officeLayerId, designationId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetOrganogramByParentId(int parentId)
        {
            var data = officefacade.GetOrganogramByParentId(parentId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Test_Organogram()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOrganogramsTest()
        {
            return Json(officefacade.GetOrganogramsTest(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrganogramMain()
        {
            return View();
        }
    }
}