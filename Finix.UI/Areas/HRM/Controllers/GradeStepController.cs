﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class GradeStepController  : Controller
    {
        private readonly GradeStepFacade gradeStepFacade;
        public GradeStepController()
        {
            gradeStepFacade = new GradeStepFacade();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetGradeSteps()
        {
            var gradeStepList = gradeStepFacade.GetAllGradeSteps();
            return Json(gradeStepList, JsonRequestBehavior.AllowGet);
        }
    }
}