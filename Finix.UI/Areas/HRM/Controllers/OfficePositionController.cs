﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class OfficePositionController : Controller
    {
        //
        // GET: /OfficePosition/
        private readonly OfficeFacade officefacade;
        public OfficePositionController()
        {
            officefacade = new OfficeFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetOfficePosition()
        {
            return Json(officefacade.GetOfficePositions(), JsonRequestBehavior.AllowGet);
        }
        //GetOfficePositionByOfficeSettings
        [HttpGet]
        public JsonResult GetOfficePositionByOfficeSettings(long? officeLayerId,long? officeId,long? designationId)
        {
            return Json(officefacade.GetOfficePositionByOfficeSettings(officeLayerId, officeId, designationId), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveOfficePosition(OfficePositionDto officepositiondto)
        {
            try
            {
                officefacade.SaveOfficePosition(officepositiondto);
                return Json(new { Result = "OK", Message = "Office position saved succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "OK", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
           
        }

        [HttpPost]
        public JsonResult DeleteOfficePosition(int id)
        {
           
            try
            {
                officefacade.DeleteOfficePosition(id);
                return Json(new { Result = "OK", Message = "Office position deleted succesfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "OK", Message =ex.Message}, JsonRequestBehavior.AllowGet);
            }
            
        }
	}
}