﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class DesignationKPIController : Controller
    {
        private readonly KPIFacade kpifacade;
        public DesignationKPIController()
        {
            kpifacade = new KPIFacade();
        }
        //
        // GET: /DesignationKPI/
        public ActionResult Index()
        {
            return View();
        }

       

        

       
        public JsonResult GetCorrespondingKPI(int kpitypeid)
        {
            return Json(kpifacade.GetCorrespondingKPI(kpitypeid), JsonRequestBehavior.AllowGet);
        }

	}
}