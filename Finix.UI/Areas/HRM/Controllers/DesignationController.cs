﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.HRM.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class DesignationController : Controller
    {
        //
        // GET: /Design/

         private readonly MenuFacade menuFacade;
        public DesignationController()
        {
            this.menuFacade = new MenuFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDesign()
        {
            var DesignList = menuFacade.GetDesignations();
            return Json(DesignList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDesign(DesignationDto designdto)
        {
            try
            {
                menuFacade.SaveDesignation(designdto);
                return Json(new { Result = "OK", Message = "Designation saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpPost]
        public JsonResult DeleteDesign(int id)
        {
            try
            {
                menuFacade.DeleteDesignation(id);
                return Json(new { Result = "OK", Message = "Designation deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public string GetCorrespondingGrade()
        {
            List<GradeDto> grades = menuFacade.GetGrades();
            string strret = "<select>";
            foreach (GradeDto item in grades)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }

        public string GetDesignations()
        {
            List<DesignationDto> designations = menuFacade.GetDesignations();
            string strret = "<select>";
            foreach (DesignationDto item in designations)
            {
                strret += "<option value='" + item.Id + "'>" + item.Name + "</option>";
            }
            strret += "</select>";
            return strret;
        }
	}
}