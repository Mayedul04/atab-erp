﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RnD.Controllers
{
    public class PurchaseController : Controller
    {
        // GET: Purchase
        public ActionResult Index()
        {
            return View();
        }
        public JavaScriptResult ShowAlert()
        {
            var script = "alert('Hello');";
            return new JavaScriptResult() { Script = script };
        }
    }
}