﻿using Finix.HRM.Facade;
using Finix.HRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class VendorController : Controller
    {
        //
        // GET: /Vendor/
         private readonly TrainingFacade trainingfacade;
         public VendorController()
        {
            trainingfacade = new TrainingFacade();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetVendors()
        {
            return Json(trainingfacade.GetVendors(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAddressFromPostCode(long postalid)
        {
            return Json(trainingfacade.GetAddressFromPostCode(postalid), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveVendors(VendorDto vendordto)
        {
            try
            {
                trainingfacade.SaveVendor(vendordto);
                return Json(new { Result = "OK", Message = "Vendor saved successfully" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteVendor(int id)
        {
            try
            {
                trainingfacade.DeleteVendor(id);
                return Json(new { Result = "OK", Message = "Vendor deleted successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "OK", Message = ex.Message });
            }
        }

	}
}