﻿using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Facade;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.HRM.Controllers
{
    public class EmployeeReportsController : Controller
    {
        private readonly BiographyFacade biofacade=new BiographyFacade();
        private readonly EmployeeFacade employeefacade=new EmployeeFacade();
        private readonly OfficeFacade officefacade=new OfficeFacade();
        private readonly EducationFacade educationfacade;
        // GET: HRM/EmployeeReports
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEmployeeDepartmentWise(long? officeid, long? officeUnitId)
        {
            var data = biofacade.GetEmployeeDepartmentWise(officeid,officeUnitId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetOfficeUnits(long officeid)
        {
            var unitList = biofacade.GetOfficeUnit(officeid);
            return Json(unitList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeBiography(string reportTypeId, int employeeId)
        {
            var basicinfo = biofacade.GetBasicInfoById(employeeId);
            List<EmpBasicInfoDto> basics = new List<EmpBasicInfoDto>();
            basics.Add(basicinfo);
            
            List<EmployeeEducationDto> education =  biofacade.GetEmpEducations(employeeId);
            List<TrainingInformationDto> training = biofacade.GetTrainingInfo(employeeId);
            List<EmploymentHistoryDto> experiences = biofacade.GetExperinceInfo(employeeId);

            var contact = biofacade.GetContactInfoById(employeeId);
            List<ContactInformationDto> contacts = new List<ContactInformationDto>();
            contacts.Add(contact);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/HRM/Reports"), "rptBiography.rdlc"); 
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("EmployeeBasicInfotWiseReport");
            }

            ReportDataSource rd  = new ReportDataSource("BasicInfo", basics);
            ReportDataSource rd1 = new ReportDataSource("EducationInfo", education);
            ReportDataSource rd2 = new ReportDataSource("TrainingInfo", training);
            ReportDataSource rd3 = new ReportDataSource("ExperienceInfo", experiences);
            ReportDataSource rd4 = new ReportDataSource("ContactInfo", contacts);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.DataSources.Add(rd3);
            lr.DataSources.Add(rd4);
            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

       

    }
}