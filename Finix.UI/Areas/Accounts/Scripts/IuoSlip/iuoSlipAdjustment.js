﻿var actions = [{ 'Id': 1, 'Name': 'Rectify' },
                { 'Id': 2, 'Name': 'Replace' },
                { 'Id': 3, 'Name': 'Waiver' },
                { 'Id': 4, 'Name': 'Deferral' },
                { 'Id': 5, 'Name': 'Others' },
                 { 'Id': 6, 'Name': 'Obtained' }];
$(document).ready(function () {
    ko.validation.init({
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
        decorateInputElement: true,
        grouping: { deep: true, observable: true }
    });

    function voucher(data) {
        var self = this;
        self.Id = ko.observable();
        self.AccountHeadCode = ko.observable();
        self.Description = ko.observable();
        self.Debit = ko.observable(); //moment(data.ApplicationReceiveDate).format('DD/MM/YYYY')
        self.Credit = ko.observable();
        self.AccountHeadCodeName = ko.observable();
        self.LoadData = function (data) {
            var self = this;
            self.Id(data.Id);
            self.AccountHeadCode(data.AccountHeadCode);
            self.Description(data.Description);
            self.Debit(data.Debit); //moment(data.ApplicationReceiveDate).format('DD/MM/YYYY')
            self.Credit(data.Credit);
            self.AccountHeadCodeName(data.AccountHeadCodeName);
        }
    }
    function iuoSlipVm() {
        var self = this;
        self.Id = ko.observable();
        self.ReceiverName = ko.observable();
        self.Designation = ko.observable();
        self.Amount = ko.observable();
        self.ReceiveDate = ko.observable();
        self.Remarks = ko.observable();
        self.IuoSlipStatus = ko.observable();
        self.IuoPurpose = ko.observable();
        self.IsBank = ko.observable();
        self.CreditAccHeadCode = ko.observable();
        self.DebitAccHeadCode = ko.observable();
        self.CompanyProfileId = ko.observable(userCompanyId);
        self.BankId = ko.observable();

        /////////////////Newly Modified
        self.IuoSlipStatusList = ko.observableArray([]);
        self.AccountHeads = ko.observableArray([]);
        self.Banks = ko.observableArray([]);
        self.VoucherList = ko.observableArray([]);
        self.CompanyList = ko.observableArray(Companies);
        self.IuoPurposeList = ko.observableArray();

        self.BankName = ko.observable();
        self.BranchName = ko.observable();
        self.ChequeNo = ko.observable();
        self.ChequeDate = ko.observable();
        self.AccountHeadCode = ko.observable();
        self.ChequeNo = ko.observable('');
        self.ChequeDate = ko.observable();
        self.ChequeDateTxt = ko.observable();
        function getPaytypes() {
            return ['cash', 'bank'];
        }
        function getTransactiontypes() {
            return ['monthly', 'yearly'];
        }

        self.paytypes = ko.observableArray(getPaytypes());
        self.trantypes = ko.observableArray(getTransactiontypes());
        self.selectedTransactiontype = ko.observable(self.trantypes()[0]);
        self.selectedPaytype = ko.observable(self.paytypes()[0]);
        self.selectedBankAccountHeadCode = ko.observable(0).extend({ required: { onlyIf: function () { return (self.selectedPaytype() === "bank"); } } });

        self.IsMonthly = ko.computed(
             function () {
                 if (self.selectedTransactiontype() === 'monthly') {
                     $("#month").show();
                     return true;
                 }
                 if (self.selectedTransactiontype() === 'yearly') {
                     $("#month").hide();
                     return true;

                 }
                 return false;
             });
        self.AccountHeadCode = ko.observable('');
        self.Description = ko.observable('');
        self.Debit = ko.observable('');
        self.AccHead = ko.observable('');
        self.AddHead = function () {
            var vrchr = new voucher();
            vrchr.AccountHeadCode(self.AccHead().AccountHeadCode);
            vrchr.AccountHeadCodeName(self.AccHead().name);
            vrchr.Description(self.Description());
            vrchr.Debit(self.Debit());
            vrchr.Credit(0);
            self.VoucherList.push(vrchr);
        }

        self.CheckDebitCredit = ko.computed(function () {
            var totalDebit = 0;
            var totalCredit = 0.0;
            $.each(self.VoucherList(), function (index, value) {
                if (value.Debit() > 0) {
                    totalDebit += parseFloat(value.Debit() ? value.Debit() : 0);
                }
                if (value.Credit() > 0) {
                    totalCredit += parseFloat(value.Credit() ? value.Credit() : 0);
                }
            });
            if (totalDebit ===totalCredit) {
                return true;
            }
          
            return false;
        });
        self.SaveOfficeExpense = function() {
            var submitData = {
                vouchers: self.VoucherList(),
                id :self.Id()
            }
            debugger;
            console.log(self.CheckDebitCredit());
            console.log(typeof (self.CheckDebitCredit()));
            if (self.CheckDebitCredit() === true) {
                $.ajax({
                    type: "POST",
                    url: '/Accounts/IuoSlip/SaveIndividualExpenceIuoSlip',
                    data: ko.toJSON(submitData),
                    contentType: "application/json",
                    success: function(data) {
                        $('#SuccessModal').modal('show');
                        $('#SuccessModalText').text(data.Message);
                    },
                    error: function() {
                        alert(error.status + "<--and--> " + error.statusText);
                    }
                });
            } else {
                $('#SuccessModal').modal('show');
                $('#SuccessModalText').text("Debit and Credit Amount isn't same");
            }
        }
        //self.IsBank = ko.computed(
        //   function () {
        //       if (self.selectedPaytype() === 'bank') {
        //           return true;
        //       }
        //       return false;
        //   });

        self.getBanks = function () {
            var currentCompany = self.CompanyProfileId() > 0 ? self.CompanyProfileId() : null;
            return $.ajax({
                type: "GET",
                url: '/Accounts/Bank/GetBankList?CompanyProfileId=' + currentCompany,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.Banks(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.getAccHeads = function () {
            var currentCompany = self.CompanyProfileId() > 0 ? self.CompanyProfileId() : 1;
            return $.ajax({
                type: "GET",
                url: '/Accounts/Accounts/GetConcatedAccHeads?companyProfileId=' + currentCompany,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.AccountHeads(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        }
        self.LoadIuoSlip = function () {
            if (self.Id() > 0) {
                return $.ajax({
                    type: "GET",
                    url: '/Accounts/IuoSlip/LoadIuoSlipByIdForAdjustment/?id=' + self.Id(),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        self.Id(data.Id);
                        self.ReceiverName(data.ReceiverName);
                        self.Designation(data.Designation);
                        self.Amount(data.Amount);
                        self.ReceiveDate(moment(data.ReceiveDate));
                        //moment(self.ReceiveDate()).format('DD/MM/YYYY'),
                        self.Remarks(data.Remarks);
                        //self.CompanyProfileId(data.CompanyProfileId);
                        $.when(self.GetIuoStatuses()).done(function () {
                            self.IuoSlipStatus(data.IuoSlipStatus);
                        });
                        $.when(self.GetIuoPurposes()).done(function () {
                            self.IuoPurpose(data.IuoPurpose);
                        });
                        $.when(self.getBanks()).done(function () {
                            self.BankId(data.BankId);
                        });
                        self.IsBank(data.IsBank);
                        $.when(self.getAccHeads()).done(function () {
                            self.DebitAccHeadCode(data.DebitAccHeadCode);
                        });
                        self.CreditAccHeadCode(data.CreditAccHeadCode);
                        self.DebitAccHeadCode(data.DebitAccHeadCode);
                        self.ChequeNo(data.ChequeNo);
                        self.ChequeDate(data.ChequeDate);
                        if (data.VoucherList != null) {
                            $.each(data.VoucherList, function (index, value) {
                                var doc = new voucher();
                                if (typeof (value) != 'undefined') {
                                    doc.LoadData(value);
                                    self.VoucherList.push(doc);
                                }
                            });
                        };
                    }
                });
            }
        };
        self.Check = function () {
            if (self.IuoPurpose() === 1) {
                return $.ajax({
                    type: "GET",
                    url: '/Accounts/IuoSlip/LoadVoucherForIndividual/?id=' + self.Id(),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log(ko.toJSON(data));
                        console.log(typeof (data.Success));
                        if (data.Success === true) {
                            console.log(data.Message);
                            $('#Individual').modal('show');
                            $('#IndividualModalText').text(data.Message);
                        }
                    }
                });
            }
            if (self.IuoPurpose() === 3) {
                return $.ajax({
                    type: "GET",
                    url: '/Accounts/IuoSlip/LoadVoucherForGroup/?id=' + self.Id(),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Success === true) {
                            $('#Individual').modal('show');
                            $('#IndividualModalText').text(data.Message);
                        }
                    }
                });
            }
        }
        self.SubmitIndividual = function () {
            if (self.IuoPurpose() === 1) {
                return $.ajax({
                    type: "GET",
                    url: '/Accounts/IuoSlip/SaveIndividualIuoSlip/?id=' + self.Id(),
                    contentType: "application/json",
                    dataType: "json",
                    success: function(data) {
                        if (data.Success === true) {
                            $('#SuccessModal').modal('show');
                            $('#SuccessModalText').text(data.Message);
                        }
                    }
                });
            }
            if (self.IuoPurpose() === 3) {
                return $.ajax({
                    type: "GET",
                    url: '/Accounts/IuoSlip/SaveGroupIuoSlip/?id=' + self.Id(),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        if (data.Success === 'true') {
                            $('#SuccessModal').modal('show');
                            $('#SuccessModalText').text(data.Message);
                        }
                    }
                });
            }
        }
        self.GetIuoStatuses = function () {
            return $.ajax({
                type: "GET",
                url: '/Accounts/IuoSlip/GetIuoSlipStatuses',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.IuoSlipStatusList(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        };
        self.GetIuoPurposes = function () {
            return $.ajax({
                type: "GET",
                url: '/Accounts/IuoSlip/GetIuoPurposes',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    self.IuoPurposeList(data);
                },
                error: function (error) {
                    alert(error.status + "<--and--> " + error.statusText);
                }
            });
        };


        self.Initialize = function () {
            if (self.Id() > 0) {
                self.LoadIuoSlip();
            }
        }
        self.queryString = function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var laDclVm = new iuoSlipVm();
    //var appId = laDclVm.queryString('ApplicationId');
    //laDclVm.ApplicationId(appId);
    //var propId = laDclVm.queryString('ProposalId');
    //laDclVm.ProposalId(propId);
    var id = laDclVm.queryString('IuoId');
    laDclVm.Id(id);
    laDclVm.Initialize();
    ko.applyBindings(laDclVm, $('#IUOIndexVw')[0]);

});