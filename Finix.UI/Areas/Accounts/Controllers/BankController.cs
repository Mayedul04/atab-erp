﻿using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Util;
using Finix.UI.Controllers;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class BankController : BaseController
    {
        private readonly BankFacade _bankFacade;

        public BankController(BankFacade bankFacade)
        {
            this._bankFacade = bankFacade;
        }
        // GET: Bank
        public ActionResult Index()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        /* Developed by - Siddique
         * Developed on - 10 March 2016
         * Objective    - Create and list view for Banks
         */
        public ActionResult Create()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        /* Developed by - Siddique
         * Developed on - 10 March 2016
         * Objective    - Provide all active bank list
         */
        public JsonResult GetBankList(long? CompanyProfileId)
        {
            var banks = _bankFacade.GetAllBanks(CompanyProfileId).OrderBy(b => b.Name);

            return Json(banks, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetBankAccountTypes()
        {
            var types = new List<object>();
            types.Add(new { Id = 0, Name = "None" });
            types.Add(new { Id = 1, Name = "Savings" });
            types.Add(new { Id = 2, Name = "Current" });
            types.Add(new { Id = 3, Name = "STD" });

            return Json(types, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBank(BankDto bank)
        {
            AccountsFacade _accountsFacade = new AccountsFacade();
            var bankId = _bankFacade.SaveBank(bank);
            //if (bank.Id == 0 || bankId > 0)
            //{
            //    //AccHeadDto accHead = new AccHeadDto();
            //    //accHead.AccGroupId = 1;
            //    //accHead.AccSubGroupId = 2;
            //    //accHead.AccHeadGroupId = 1;
            //    //accHead.AccHeadSubGroupId = 7;
            //    //accHead.AccHeadName = bank.Name + " - " + bank.BranchName;
            //    //accHead.Name = accHead.AccHeadName;
            //    //accHead.RefId = bankId;
            //    //accHead.RefType = Finix.Accounts.Infrastructure.ReferenceAccountType.Bank;
            //    //accHead.CurrentCode = "1020102";

            //    //accHead.TierId = 4;
            //    //accHead.Code = _accountsFacade.GetLatestCode("1020102");
            //    //accHead.CreatedBy = 1;//SessionHelper.UserProfile.Id;
            //    //accHead.CreateDate = DateTime.Now;
            //    //accHead.EditedBy = 1;//SessionHelper.UserProfile.Id;
            //    //accHead.EditDate = DateTime.Now;


            //    //var success = _accountsFacade.SaveAccHead(accHead);
            //}
            //else if (bank.Id > 0 && bankId > 0)
            //{
            //    //var accHead = _accountsFacade.GetAccHeadByRefIdAndRefType(bank.Id, ReferenceAccountType.Bank, bank.CompanyProfileId);
            //    //accHead.AccHeadName = bank.Name + " - " + bank.BranchName;
            //    //accHead.Name = accHead.AccHeadName;
            //    //accHead.EditedBy = 1;
            //    //accHead.EditDate = DateTime.Now;

            //    //var success = _accountsFacade.SaveAccHead(accHead);
            //}
            return Json((bankId > 0) ? "Record Saved Successfully" : "Record not saved.\nPlease try again or contact Support.", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChequeRegisterEntry()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        [HttpPost]
        public JsonResult SaveChequeRegister(ChequeRegisterDto chequeRegister)
        {
            var chequeRegisterId = _bankFacade.SaveChequeRegister(chequeRegister, SessionHelper.UserProfile.UserId);
            return Json(chequeRegisterId);
        }
        [HttpGet]
        public JsonResult GetAvailableChequeNo(long BankId, string prefix)
        {
            var chequeList = _bankFacade.GetAvailableCheques(BankId, prefix);
            return Json(chequeList.Select(c => new { key = c.ChequeNo, value = c.ChequeNo }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChequeBookStatusSummary()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        public JsonResult GetChequeBookStatusSummary(long? CompanyProfileId, long? BankId, ChequeStatus? ChequeStatus)
        {
            var result = _bankFacade.GetChequeBookStatusSummary(CompanyProfileId, BankId, ChequeStatus);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /* Developed by - Sabiha
        * Developed on - 18 October 2016
        * Objective    - Create and list view for Banks
        */
        public ActionResult BankCreate()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }
        public string GetBankAccountTypesForGrid()
        {
            List<KeyValuePair<int, string>> accountTypes = UiUtil.EnumToKeyVal<AccountTypes>();
            string strret = "<select>";
            foreach (var item in accountTypes)
            {
                strret += "<option value='" + item.Key + "'>" + item.Value + "</option>";
            }
            strret += "</select>";
            return strret;

        }

        [HttpGet]
        public ActionResult ChequeRegisterEntryIndex()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }

        public JsonResult GetChequeRegisterList()
        {
            var banks = _bankFacade.GetChequeRegisterList();

            return Json(banks, JsonRequestBehavior.AllowGet);
        }
        /* Developed by - Sabiha
         * Developed on - 18 October 2016
         * Objective    - Provide all active bank list
         */

        //public ActionResult BankReconsilation()
        //{
        //    ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
        //    return View();
        //}

        public ActionResult ChequePrint(string reportTypeId, string ChequeNo, long? chequeId)
        {
            var chequeData = _bankFacade.GetCheque(ChequeNo, chequeId);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "Cheque_trustBank.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("BankReconsilation");
            }
            
            ReportParameter ChequeDate = new ReportParameter("ChequeDate", "");
            ReportParameter PayTo = new ReportParameter("PayTo", "");
            ReportParameter Amount = new ReportParameter("Amount", "");
            if(chequeData != null)
            {
                ChequeDate = new ReportParameter("ChequeDate", chequeData.IssueDate.ToString());
                PayTo = new ReportParameter("PayTo", chequeData.PaidTo);
                Amount = new ReportParameter("Amount", chequeData.Amount.ToString());
            }
            lr.SetParameters(new ReportParameter[] { ChequeDate, PayTo, Amount });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;


            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>7.5in</PageWidth>" +
                "  <PageHeight>3.4in</PageHeight>" +
                "  <MarginTop>0in</MarginTop>" +
                "  <MarginLeft>0in</MarginLeft>" +
                "  <MarginRight>0in</MarginRight>" +
                "  <MarginBottom>0in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }
    }
}