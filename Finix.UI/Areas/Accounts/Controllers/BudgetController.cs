﻿using Finix.Accounts.Dto;
using Finix.Accounts.Facade;
using Finix.Auth.Facade;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class BudgetController : Controller
    {
        private readonly BudgetFacade _budget = new BudgetFacade();
        private readonly CompanyProfileFacade _companyFacade = new CompanyProfileFacade();
        [HttpGet]
        public ActionResult BudgetConfig()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        [HttpGet]
        public JsonResult GetBudgetConfigsByOfficeId(long officeId)
        {
            var result = _budget.GetBudgetConfigsByOffice(officeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBudgetConfigs(List<BudgetConfigDto> dtos, long? officeId)
        {
            if (officeId == null)
                officeId = SessionHelper.UserProfile.SelectedCompanyId;
            var result = _budget.SaveBudgetConfig(dtos, SessionHelper.UserProfile.UserId, (long)officeId);
            return Json(result, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult GetExistingControlHeads(AutofillDto dto)
        {
            var result = _budget.GetUsedControlHeads(dto.prefix);
            var data = result.Select(r => new { key = r, value = r }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult BudgetCreate()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        [HttpGet]
        public JsonResult GetTemplateForCreatingNewBudget(long officeId, string fromDate, string toDate)
        {
            DateTime FromDate, ToDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var result = _budget.GetLatestBudgetByOfficeForCreatingNewBudget(officeId, FromDate, ToDate);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBudget(BudgetDto dto)
        {
            var result = _budget.SaveBudget(dto,SessionHelper.UserProfile.UserId, (long)dto.OfficeId);
            return Json(result, JsonRequestBehavior.DenyGet);
        }
        [HttpGet]
        public JsonResult GetBudgetById(long id)
        {
            var result = _budget.GetBudgetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BudgetReport(long id, string reportTypeId)
        {
            var budget = _budget.GetBudgetById(id);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptBudget.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("BudgetCreate");
            }
            ReportDataSource rd = new ReportDataSource("dsBudgetDetail", budget.Details);
            ReportParameter rp = new ReportParameter("FromDate", budget.FromDate.ToString("dd/MM/yyyy"));
            ReportParameter rp1 = new ReportParameter("ToDate", budget.ToDate.ToString("dd/MM/yyyy"));

            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            var company = _companyFacade.GetCompanyProfileById((long)budget.OfficeId);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                //rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }

            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp, rp1 , rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        public ActionResult BudgetList(string sortOrder, string searchString, List<long> officeIds, string fromDate, string toDate, int page = 1, int pageSize = 20)
        {
            DateTime FromDate, ToDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            ViewBag.SortOrder = sortOrder;
            ViewBag.SearchString = searchString;
            ViewBag.OfficeIds = officeIds;
            ViewBag.FromDate = fromDate;
            ViewBag.ToDate = toDate;

            var result = _budget.BudgetList(FromDate, ToDate, pageSize, page, searchString, officeIds);

            return View(result);
        }
    }
}