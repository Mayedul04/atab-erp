﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finix.Accounts.Dto;
using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using Finix.UI.Areas.FAMS.Controllers;
using Microsoft.Reporting.WebForms;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class IuoSlipController : Controller
    {
        private readonly IuoSlipFacade _facade = new IuoSlipFacade();

        public ActionResult Index()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        public JsonResult SaveIuoSlip(IUOSlipDto dto)
        {
            var userId = SessionHelper.UserProfile.UserId;
            //dto.VerifiedByUserId = userId;
            DateTime verificationDate;
            DateTime dob;
            var fromConverted = DateTime.TryParseExact(dto.ReceiveDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out verificationDate);
            if (fromConverted)
            {
                dto.ReceiveDate = verificationDate;
            }
            var chequeConverted = DateTime.TryParseExact(dto.ChequeDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out verificationDate);
            if (chequeConverted)
            {
                dto.ChequeDate = verificationDate;
            }
            var verification = _facade.SaveIuoSlip(dto, SessionHelper.UserProfile.UserId);
            return Json(verification, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIuoSlipStatuses()
        {
            var applicationCustomerTypes = _facade.GetIuoSlipStatuses();
            return Json(applicationCustomerTypes, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIuoPurposes()
        {
            var applicationCustomerTypes = _facade.GetIuoPurposes();
            return Json(applicationCustomerTypes, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IouSlipList(string sortOrder, string currentFilter, string searchString, long? sourceid, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            var applications = _facade.IouSlipList(pageSize, page, searchString);
            return View(applications);
        }
        public JsonResult GetIuoSlipById(long id)
        {
            var applicationCustomerTypes = _facade.GetIuoSlipById(id);
            return Json(applicationCustomerTypes, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIuoSlipReportById(string reportTypeId, long id)
        {
            var iuoSlip = _facade.GetIuoSlipById(id);
            var iuoSlipList = new List<IUOSlipDto>();
            iuoSlipList.Add(iuoSlip);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "IUOSlip.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("IouSlipList");
            }

            ReportDataSource rd = new ReportDataSource("dsIuoSlip", iuoSlipList);

            lr.DataSources.Add(rd);
            //ReportParameter rp1 = new ReportParameter("CustomRefNo", CustomRefNo);
            //ReportParameter rp2 = new ReportParameter("GeneratedBy", disbursmentMemo.CreatedByName);
            //lr.SetParameters(new ReportParameter[] { rp1, rp2 });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            //string deviceInfo =
            //      "<DeviceInfo>" +
            //    "  <OutputFormat>EMF</OutputFormat>" +
            //    "  <PageWidth>8.2in</PageWidth>" +
            //    "  <PageHeight>11.6in</PageHeight>" +
            //    "  <MarginTop>0.25in</MarginTop>" +
            //    "  <MarginLeft>0.25in</MarginLeft>" +
            //    "  <MarginRight>0.25in</MarginRight>" +
            //    "  <MarginBottom>0.25in</MarginBottom>" +
            //    "</DeviceInfo>";
            string deviceInfo =

              "<DeviceInfo>" +
            "  <OutputFormat>EMF</OutputFormat>" +
            "  <PageWidth>6.25in</PageWidth>" +
            "  <PageHeight>5.83in</PageHeight>" +
            "  <MarginTop>0.25in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.25in</MarginBottom>" +
            "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public ActionResult IuoSlipAdjustment()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        //LoadIuoSlipByIdForAdjustment
        public JsonResult LoadIuoSlipByIdForAdjustment(long id)
        {
            var data = _facade.LoadIuoSlipByIdForAdjustment(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadVoucherForIndividual(long id)
        {
            var data = _facade.LoadVoucherForIndividual(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadVoucherForGroup(long id)
        {
            var data = _facade.LoadVoucherForGroup(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //SaveIndividualIuoSlip LoadVoucherForGroup GetAccHeadNameByCode
      
        public ActionResult SaveIndividualIuoSlip(long id)
        {
            var data = _facade.SaveIndividualIuoSlip(id,SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveGroupIuoSlip(long id)
        {
            var data = _facade.SaveGroupIuoSlip(id, SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveIndividualExpenceIuoSlip(List<VoucherDto> vouchers,long id)
        {
            var data = _facade.SaveIndividualExpenceIuoSlip(vouchers, id,SessionHelper.UserProfile.SelectedCompanyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}