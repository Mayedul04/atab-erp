﻿using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Service;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using Finix.UI.Controllers;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class ReportAccountController : BaseController
    {
        private readonly GenService _service = new GenService();
        private readonly ReportAccountFacade _reportAccountFacade;
        private readonly CompanyProfileFacade _companyProfile;
        private readonly CompanyDataFacade _companyData;
        private readonly CompanyProfileFacade _office = new CompanyProfileFacade();
        public ReportAccountController(ReportAccountFacade reportAccountFacade)
        {
            this._reportAccountFacade = reportAccountFacade;
            this._companyProfile = new CompanyProfileFacade();
            this._companyData = new CompanyDataFacade();
        }

        #region "Ledger Statement Report"

        public ActionResult GroupLedger()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetGroupLedgers(DateTime fromDate, DateTime toDate, string code, long? companyProfileId)
        {
            var rptVoucherlst = _reportAccountFacade.GetGroupLedgerReport(fromDate, toDate, code, companyProfileId);

            return Json(rptVoucherlst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupLedgerReport(string reportTypeId, DateTime fromDate, DateTime toDate, string code, long? companyProfileId)
        {
            companyProfileId = companyProfileId ?? 1;
            var rptVoucherList = _reportAccountFacade.GetGroupLedgerReport(fromDate, toDate, code, companyProfileId);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptGroupLedger.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("LedgerReport");
            }

            ReportDataSource rd = new ReportDataSource("ReportGroupLedgerDataSet", rptVoucherList);
            //lr.DataSources.Add(rd);
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            ReportParameter rp1 = new ReportParameter("FromDate", fromDate.ToString());
            ReportParameter rp2 = new ReportParameter("ToDate", toDate.ToString());

            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp1, rp2, rpTitle, rpAddress });


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);

        }

        // GET: ReportAccount
        public ActionResult LedgerReport()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetLedgerReportByAccHead(DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        {
            IEnumerable<LedgerDto> rptVoucherlst = new List<LedgerDto>();
            ResponseDto response = new ResponseDto();
            if (accHeadCode != "undefined")
            {
                rptVoucherlst = _reportAccountFacade.GetLedgerReportByAccHead(fromDate, toDate, accHeadCode, officeId);
                response.Message = "";
                return Json(new { rptVoucherlst, response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                response.Success = false;
                response.Message = "Please Select Account Head First";
                return Json(new { rptVoucherlst, response }, JsonRequestBehavior.AllowGet);
            }


        }
        //GetLedgerAmount

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetLedgerAmount(string toDate, string accHeadCode)
        {
            LedgerDto result = new LedgerDto();
            ResponseDto response = new ResponseDto();
            DateTime receiveDate = DateTime.Now;
            DateTime todate = DateTime.Now;
            DateTime firstDayOfMonth = DateTime.Now;
            var FromConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out receiveDate);
            if (FromConverted)
            {
                todate = receiveDate;
                firstDayOfMonth = new DateTime(todate.Year, todate.Month, 1);
            }
            if (accHeadCode != "undefined")
            {
                _reportAccountFacade.GetLedgerAmount(firstDayOfMonth, todate, accHeadCode, SessionHelper.UserProfile.SelectedCompanyId);
                response.Message = "";
                result = _reportAccountFacade.GetLedgerAmount(firstDayOfMonth, todate, accHeadCode, SessionHelper.UserProfile.SelectedCompanyId);
                return Json(new { result, response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                response.Success = false;
                response.Message = "Please Select Account Head First";
                return Json(new { result, response }, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult GetLedgerReport(string reportTypeId, DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        {
            var rptVoucherlst = _reportAccountFacade.GetLedgerReportByAccHead(fromDate, toDate, accHeadCode, officeId);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptLedgerStatementReport_Revised.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("LedgerReport");
            }
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            //var company = _companyData.GetCompanyProfileByAccHeadCode(accHeadCode);
            var company = _companyProfile.GetCompanyProfileById(officeId);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("ReportLedgerDataSet", rptVoucherlst);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);


        }

        #endregion

        #region "Bank Book & CashBook Report"

        public ActionResult CashbookReport()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        public ActionResult BankbookReport()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetBankBookReport(DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        {
            var rptVoucherlst = _reportAccountFacade.GetBankBookReport(fromDate, toDate, accHeadCode, officeId);
            return Json(rptVoucherlst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBankBookReports(string reportTypeId, DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        {
            //var spDtoList = _reportStockFacade.GetStockPositionByWarehouseId(warehouseId);
            var rptVoucherlst = _reportAccountFacade.GetBankBookReport(fromDate, toDate, accHeadCode, officeId);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptBankBook.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("BankbookReport");
            }
            AccountsFacade _accountsFacade = new AccountsFacade();
            string bankName = _accountsFacade.GetAccHeadNameByAccHeadCode(accHeadCode);
            ReportDataSource rd = new ReportDataSource("ReportCashBookDataSet", rptVoucherlst);
            ReportParameter rp = new ReportParameter("BankName", bankName);

            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            var company = _companyData.GetCompanyProfileByAccHeadCode(accHeadCode);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }

            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }

        public JsonResult GetCashBookReport(DateTime fromDate, DateTime toDate, long CompanyProfileId)
        {
            var rptVoucherlst = _reportAccountFacade.GetCashBookReport(fromDate, toDate, CompanyProfileId);
            return Json(rptVoucherlst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCashBookReports(string reportTypeId, DateTime fromDate, DateTime toDate, long CompanyProfileId)
        {
            //var spDtoList = _reportStockFacade.GetStockPositionByWarehouseId(warehouseId);
            var rptVoucherlst = _reportAccountFacade.GetCashBookReport(fromDate, toDate, CompanyProfileId);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptCashBook.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("CashbookReport");
            }

            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            var company = _companyProfile.GetCompanyProfileById(CompanyProfileId);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("ReportCashBookDataSet", rptVoucherlst);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }

        #endregion

        #region "Trail Balance  Report"


        public ActionResult TrialBalanceReport()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetTrailBalanceReport(DateTime toDate, long CompanyProfileId)
        {
            var rptTrailBalanceData = _reportAccountFacade.GetTrailBalanceReport(toDate, CompanyProfileId);
            return Json(rptTrailBalanceData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReportTrailBalance(string reportTypeId, DateTime toDate, long CompanyProfileId)
        {
            var rptTrailBalanceData = _reportAccountFacade.GetTrailBalanceReport(toDate, CompanyProfileId);

            /////////////////////////////////////////////
            ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");
            //CompanyProfileId = CompanyProfileId ?? 1;
            if (CompanyProfileId != null)
            {
                var company = _companyProfile.GetCompanyProfileById((long)CompanyProfileId);
                if (company != null)
                {
                    rpTitle = new ReportParameter("CompanyName", company.Name);
                    rpAddress = new ReportParameter("CompanyAddress", company.Address);
                }
            }
            //ReportDataSource rd = new ReportDataSource("ReportLedgerDataSet", rptVoucherlst);
            //lr.DataSources.Add(rd);
            //lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });
            //////////////////////////////////////////////

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptTrialBalance.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("TrialBalanceReport");
            }

            ReportDataSource rd = new ReportDataSource("ReportTrialBalanceDataSet", rptTrailBalanceData);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }

        #endregion

        #region Generic Account

        #endregion

        #region Income Statement Report
        public ActionResult IncomeStatement()
        {

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            ViewBag.MaxDate = _office.DateToday(null).AddDays(-1);
            ViewBag.MaxDateText = ((DateTime)ViewBag.MaxDate).ToString("yyyy-MM-dd");
            return View();
        }
        public ActionResult IncomeStatementNotes()
        {

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            ViewBag.MaxDate = _office.DateToday(null).AddDays(-1);
            ViewBag.MaxDateText = ((DateTime)ViewBag.MaxDate).ToString("yyyy-MM-dd");
            return View();
        }
        public JsonResult GetIncomeStatement(string fromDate, string toDate, List<long> CompanyProfileIds)
        {
            DateTime FromDate, ToDate;
            var currentSystemDate = _office.DateToday(null);
            FromDate = currentSystemDate.AddDays(-1);
            ToDate = currentSystemDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var notes = _reportAccountFacade.GetIncomeStatement(FromDate, ToDate, CompanyProfileIds);
            return Json(notes, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIncomeStatementNotes(string fromDate, string toDate)
        {
            DateTime FromDate, ToDate;
            var currentSystemDate = _office.DateToday(null);
            FromDate = currentSystemDate.AddDays(-1);
            ToDate = currentSystemDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var notes = _reportAccountFacade.GetIncomeStatementNotes(FromDate, ToDate);
            return Json(notes, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIncomeStatementNotesReport(string reportTypeId, string fromDate, string toDate, List<long?> CompanyProfileIds)
        {
            DateTime FromDate, ToDate;
            var currentSystemDate = _office.DateToday(null);
            FromDate = currentSystemDate.AddDays(-1);
            ToDate = currentSystemDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var notes = _reportAccountFacade.GetIncomeStatementNotes(FromDate, ToDate);

            LocalReport lr = new LocalReport();

            ReportDataSource rd = new ReportDataSource("dsFinancialStatementNotesReport", notes);

            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptIncomeStatementNotesReport_revised.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportParameter rp1 = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter rp2 = new ReportParameter("ToDate", ToDate.ToString());
            ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");

            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                var company = _companyProfile.GetCompanyProfileById((long)CompanyProfileIds.FirstOrDefault());
                if (company != null)
                {
                    rpTitle = new ReportParameter("CompanyName", company.Name);
                    rpAddress = new ReportParameter("CompanyAddress", company.Address);
                }
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp1, rp2, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        public ActionResult GetIncomeStatementReport(string reportTypeId, string fromDate, string toDate, List<long> CompanyProfileIds)
        {
            DateTime FromDate, ToDate;
            var currentSystemDate = _office.DateToday(null);
            FromDate = currentSystemDate.AddDays(-1);
            ToDate = currentSystemDate;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var notes = _reportAccountFacade.GetIncomeStatement(FromDate, ToDate, CompanyProfileIds);


            LocalReport lr = new LocalReport();

            ReportDataSource rd = new ReportDataSource("dsFinancialStatementNotesReport", notes);

            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptIncomeStatement.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportParameter rp1 = new ReportParameter("FromDate", fromDate.ToString());
            ReportParameter rp2 = new ReportParameter("ToDate", toDate.ToString());
            ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");

            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                var company = _companyProfile.GetCompanyProfileById((long)CompanyProfileIds.FirstOrDefault());
                if (company != null)
                {
                    rpTitle = new ReportParameter("CompanyName", company.Name);
                    rpAddress = new ReportParameter("CompanyAddress", company.Address);
                }
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp1, rp2, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        #endregion

        #region Retained Earnings Schedule
        public ActionResult RetainedEarnings()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        public ActionResult GetRetainedEarningsSchedule(string reportTypeId, string fromDate, string toDate, long CompanyProfileId)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);

            var data = _reportAccountFacade.GetRetainedEarningsSchedule(FromDate, ToDate);
            LocalReport lr = new LocalReport();

            ReportDataSource rd = new ReportDataSource("dsRetainedEarningsSchedule", data);

            string path = Path.Combine(Server.MapPath("~/Reports"), "rptRetainedEarningsSchedule.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("RetainedEarnings");
            }

            ReportParameter rp1 = new ReportParameter("FromDate", fromDate.ToString());
            ReportParameter rp2 = new ReportParameter("ToDate", toDate.ToString());
            var company = _companyProfile.GetCompanyProfileById((long)CompanyProfileId);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rp1, rp2, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);

        }
        #endregion

        #region Cash flow statement
        public ActionResult CashFlow()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        //public ActionResult GetCashFlow(string reportTypeId, string fromDate, string toDate, List<long?> CompanyProfileIds)
        //{
        //    DateTime FromDate = DateTime.Now;
        //    DateTime ToDate = DateTime.Now;
        //    bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
        //    bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
        //    //bool fromDateConvertedPrev = DateTime.TryParseExact(fromDate.AddYears(-1), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
        //    //bool toDateConvertedPrev = DateTime.TryParseExact(toDate.AddYears(-1), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
        //    var data = _reportAccountFacade.GetCashFlowMergingData(FromDate, ToDate, CompanyProfileIds);
        //    List<CashFlowDto> result = new List<CashFlowDto>();
        //    result.AddRange(data);

        //    ReportParameter rp1 = new ReportParameter("FromDate", fromDate.ToString());
        //    ReportParameter rp2 = new ReportParameter("ToDate", toDate.ToString());
        //    ReportParameter rp3 = new ReportParameter("FromDatePrev", FromDate.AddYears(-1).ToString());
        //    ReportParameter rp4 = new ReportParameter("ToDatePrev", ToDate.AddYears(-1).ToString());
        //    ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
        //    ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");

        //    if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
        //    {
        //        var company = _companyProfile.GetCompanyProfileById((long)CompanyProfileIds.FirstOrDefault());
        //        if (company != null)
        //        {
        //            rpTitle = new ReportParameter("CompanyName", company.Name);
        //            rpAddress = new ReportParameter("CompanyAddress", company.Address);
        //        }
        //    }
        //    LocalReport lr = new LocalReport();

        //    ReportDataSource rd = new ReportDataSource("dsCashFlow", result);
        //    //ReportDataSource rd1 = new ReportDataSource("dsCashFlowprev", data);

        //    string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptCashFlow.rdlc");
        //    if (System.IO.File.Exists(path))
        //    {
        //        lr.ReportPath = path;
        //    }
        //    else
        //    {
        //        return View("RetainedEarnings");
        //    }


        //    lr.DataSources.Add(rd);
        //    //lr.DataSources.Add(rd1);
        //    lr.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4 , rpTitle , rpAddress });

        //    string reportType = reportTypeId;
        //    string mimeType;
        //    string encoding;
        //    string fileNameExtension;




        //    //if (getParameters.CompanyProfileIds != null && getParameters.CompanyProfileIds.Count > 0)
        //    //{
        //    //    var company = _company.GetCompanyProfileById((long)getParameters.CompanyProfileIds.FirstOrDefault());
        //    //    if (company != null)
        //    //    {
        //    //        rpTitle = new ReportParameter("CompanyName", company.Name);
        //    //        rpAddress = new ReportParameter("CompanyAddress", company.Address);
        //    //    }
        //    //}

        //    //LocalReport lr = new LocalReport();
        //    //string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptBalanceSheet.rdlc");//rptBalanceSheet
        //    //if (System.IO.File.Exists(path))
        //    //{
        //    //    lr.ReportPath = path;
        //    //}
        //    //else
        //    //{
        //    //    ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
        //    //    return View("Index");
        //    //}

        //    //ReportDataSource rd = new ReportDataSource("Asset", Asset);
        //    //ReportDataSource rd1 = new ReportDataSource("Equity", Equity);
        //    //ReportDataSource rd2 = new ReportDataSource("Liability", Liability);
        //    //lr.DataSources.Add(rd);
        //    //lr.DataSources.Add(rd1);
        //    //lr.DataSources.Add(rd2);
        //    //lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress, rpFromDate, rpToDate });

        //    string deviceInfo = "<DeviceInfo>" +
        //        "  <OutputFormat>EMF</OutputFormat>" +
        //        "  <PageWidth>8.2in</PageWidth>" +
        //        "  <PageHeight>11.6in</PageHeight>" +
        //        "  <MarginTop>0.25in</MarginTop>" +
        //        "  <MarginLeft>0.25in</MarginLeft>" +
        //        "  <MarginRight>0.25in</MarginRight>" +
        //        "  <MarginBottom>0.25in</MarginBottom>" +
        //        "</DeviceInfo>";
        //    Warning[] warnings;
        //    string[] streams;

        //    var renderedBytes = lr.Render(
        //        reportType,
        //        deviceInfo,
        //        out mimeType,
        //        out encoding,
        //        out fileNameExtension,
        //        out streams,
        //        out warnings
        //        );

        //    return File(renderedBytes, mimeType);

        //}
        public ActionResult GetCashFlow(string reportTypeId, string fromDate, string toDate, List<long?> CompanyProfileIds)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _reportAccountFacade.GetCashFlowMergingData(FromDate, ToDate, CompanyProfileIds);
            List<CashFlowDto> result = new List<CashFlowDto>();
            result.AddRange(data);
            LocalReport lr = new LocalReport();

            ReportDataSource rd = new ReportDataSource("dsCashFlow", result);
            //ReportDataSource rd1 = new ReportDataSource("dsCashFlowprev", data);
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptCashFlow.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("RetainedEarnings");
            }

            ReportParameter rp1 = new ReportParameter("FromDate", FromDate.ToString("dd/MM/yyyy"));
            ReportParameter rp2 = new ReportParameter("ToDate", ToDate.ToString("dd/MM/yyyy"));
            ReportParameter rp3 = new ReportParameter("FromDatePrev", FromDate.AddYears(-1).ToString("dd/MM/yyyy"));
            ReportParameter rp4 = new ReportParameter("ToDatePrev", ToDate.AddYears(-1).ToString("dd/MM/yyyy"));
            lr.DataSources.Add(rd);
            //lr.DataSources.Add(rd1);
            lr.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4 });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);

        }
        #endregion

        #region Receive-Payment Report
        public ActionResult ReceivePayment()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }

        
        public ActionResult GetReceivePaymentReport(string reportTypeId, string fromDate, string toDate, long CompanyProfileId)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _reportAccountFacade.GetReceivePaymentRecords(FromDate, ToDate, CompanyProfileId);

            LocalReport lr = new LocalReport();
            var receives = new List<ReceivePaymentDto>();
            var payments = new List<ReceivePaymentDto>();
            if (data.Where(d => d.AccountHead.Contains("Opening")).Any())
                receives.AddRange(data.Where(d => d.AccountHead.Contains("Opening")).ToList());
            if (data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && d.IsCredit).Any())
                receives.AddRange(data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && d.IsCredit).ToList());
            if (data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && !d.IsCredit).Any())
                payments.AddRange(data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && !d.IsCredit).ToList());
            if (data.Where(d => d.AccountHead.Contains("Closing")).Any())
                payments.AddRange(data.Where(d => d.AccountHead.Contains("Closing")).ToList());
            ReportDataSource rd = new ReportDataSource("receive", receives);
            ReportDataSource rd1 = new ReportDataSource("payment", payments);
            //ReportDataSource rd1 = new ReportDataSource("dsCashFlowprev", data);
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptReceivePayment.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ReceivePayment");
            }

            ReportParameter fromDate_rp = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter toDate_rp = new ReportParameter("ToDate", ToDate.ToString());
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            var company = new CompanyProfileFacade().GetCompanyProfileById(CompanyProfileId);//_companyData.GetCompanyProfileByAccHeadCode(accHeadCode);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.SetParameters(new ReportParameter[] { fromDate_rp, toDate_rp, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);

        }
        public ActionResult GetReceivePaymentTransactionalReport(string reportTypeId, string fromDate, string toDate, long CompanyProfileId)
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            bool fromDateConverted = DateTime.TryParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FromDate);
            bool toDateConverted = DateTime.TryParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            var data = _reportAccountFacade.GetReceivePaymentTransactionalRecords(FromDate, ToDate, CompanyProfileId);

            LocalReport lr = new LocalReport();
            var receives = new List<ReceivePaymentDto>();
            var payments = new List<ReceivePaymentDto>();
            var closing = new List<ReceivePaymentDto>();
            if (data.Where(d => d.AccountHead.Contains("Opening")).Any())
                receives.AddRange(data.Where(d => d.AccountHead.Contains("Opening")).ToList());
            if (data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && d.IsCredit).Any())
                receives.AddRange(data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && d.IsCredit).OrderBy(d => d.VoucherDate).ToList());
            if (data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && !d.IsCredit).Any())
                payments.AddRange(data.Where(d => !d.AccountHead.Contains("Opening") && !d.AccountHead.Contains("Closing") && !d.IsCredit).OrderBy(d => d.VoucherDate).ToList());
            if (data.Where(d => d.AccountHead.Contains("Closing")).Any())
                closing.AddRange(data.Where(d => d.AccountHead.Contains("Closing")).ToList());
            ReportDataSource rd = new ReportDataSource("receive", receives);
            ReportDataSource rd1 = new ReportDataSource("payment", payments);
            ReportDataSource rd2 = new ReportDataSource("closing", closing);
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptReceivePaymentTransactional.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("ReceivePayment");
            }

            ReportParameter fromDate_rp = new ReportParameter("FromDate", FromDate.ToString());
            ReportParameter toDate_rp = new ReportParameter("ToDate", ToDate.ToString());
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            var company = new CompanyProfileFacade().GetCompanyProfileById(CompanyProfileId);//_companyData.GetCompanyProfileByAccHeadCode(accHeadCode);
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.SetParameters(new ReportParameter[] { fromDate_rp, toDate_rp, rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);

        }
        #endregion



    }
}