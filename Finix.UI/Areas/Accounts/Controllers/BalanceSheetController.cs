﻿using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using Finix.Auth.Facade;
using Finix.UI.Controllers;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Finix.Accounts.Infrastructure;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class BalanceSheetController : BaseController
    {

        private readonly BalanceSheetFacade _balanceSheetFacade = new BalanceSheetFacade();
        private readonly CompanyProfileFacade _office = new CompanyProfileFacade();
        private DateTime globalFromDate;
        private DateTime globalToDate;
        private List<long> globalCompanyProfileIds;

        // GET: BalanceSheet
        public ActionResult Index()
        {
            ViewBag.MaxDate = _office.DateToday(null).AddDays(-1);
            ViewBag.MaxDateText = ((DateTime)ViewBag.MaxDate).ToString("yyyy-MM-dd");
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        public ActionResult Details()
        {
            ViewBag.MaxDate = _office.DateToday(null).AddDays(-1);
            ViewBag.MaxDateText = ((DateTime)ViewBag.MaxDate).ToString("yyyy-MM-dd");
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        //public JsonResult GetAllBalanceSheet_Old(GetReportDto getParameters)
        //{
        //    DateTime ToDate = DateTime.Now;
        //    bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
        //    CompanyProfileFacade _company = new CompanyProfileFacade();
        //    DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();
        //    List<BalanceSheetDto> balanceSheet;
        //    if (success)
        //        balanceSheet = _balanceSheetFacade.GetAllBalanceSheet(getParameters.CompanyProfileIds, CurrentFiscalYear, ToDate);
        //    else
        //        balanceSheet = _balanceSheetFacade.GetAllBalanceSheet(getParameters.CompanyProfileIds, CurrentFiscalYear, null);
        //    return Json(balanceSheet, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetAllBalanceSheet(GetReportDto getParameters)
        {
            DateTime ToDate;
            bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!success)
                ToDate = DateTime.Now;
            CompanyProfileFacade _company = new CompanyProfileFacade();
            DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();

            var _reportAccount = new ReportAccountFacade();
            var Asset = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Asset, getParameters.CompanyProfileIds, null, ToDate, false);
            var Equity = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Equity, getParameters.CompanyProfileIds, null, ToDate, false);
            var Liability = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Liability, getParameters.CompanyProfileIds, null, ToDate, false);
            Asset.Reverse();
            Equity.Reverse();
            Liability.Reverse();

            return Json(new { Asset, Equity, Liability }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllBalanceSheetDetails(GetReportDto getParameters)//string toDate
        {
            DateTime ToDate = DateTime.Now;
            bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            CompanyProfileFacade _company = new CompanyProfileFacade();
            DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();
            List<BalanceSheetDto> balanceSheet;
            if (success)
                balanceSheet = _balanceSheetFacade.GetAllBalanceSheetDetails(getParameters.CompanyProfileIds, CurrentFiscalYear, ToDate);
            else
                balanceSheet = _balanceSheetFacade.GetAllBalanceSheetDetails(getParameters.CompanyProfileIds, CurrentFiscalYear, null);
            return Json(balanceSheet, JsonRequestBehavior.AllowGet);
        }
        public void SetSubDataSource(object balanceSheet, SubreportProcessingEventArgs e)
        {
            var notes = _balanceSheetFacade.GetBalanceSheetNotes(globalCompanyProfileIds, globalFromDate, globalToDate);
            e.DataSources.Add(new ReportDataSource("dsBalanceSheetNotes", notes));
        }
        //public ActionResult GetAllBalanceSheetReport_old(GetReportDto getParameters)
        //{
        //    DateTime ToDate;
        //    bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
        //    if (!success)
        //        ToDate = DateTime.Now;
        //    CompanyProfileFacade _company = new CompanyProfileFacade();
        //    DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();
        //    List<BalanceSheetDto> balanceSheet;
        //    if (success)
        //        balanceSheet = _balanceSheetFacade.GetAllBalanceSheet(getParameters.CompanyProfileIds, CurrentFiscalYear, ToDate);
        //    else
        //        balanceSheet = _balanceSheetFacade.GetAllBalanceSheet(getParameters.CompanyProfileIds, CurrentFiscalYear, null);

        //    /////////////////////////////////////////////
        //    var _companyProfile = new CompanyProfileFacade();
        //    ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
        //    ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");
        //    //CompanyProfileId = CompanyProfileId ?? 1;
        //    if (getParameters.CompanyProfileIds != null && getParameters.CompanyProfileIds.Count > 0)
        //    {
        //        var company = _companyProfile.GetCompanyProfileById((long)getParameters.CompanyProfileIds.FirstOrDefault());
        //        if (company != null)
        //        {
        //            rpTitle = new ReportParameter("CompanyName", company.Name);
        //            rpAddress = new ReportParameter("CompanyAddress", company.Address);
        //        }
        //    }
        //    //ReportDataSource rd = new ReportDataSource("ReportLedgerDataSet", rptVoucherlst);
        //    //lr.DataSources.Add(rd);
        //    //lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });
        //    //////////////////////////////////////////////

        //    LocalReport lr = new LocalReport();
        //    string path = Path.Combine(Server.MapPath("~/Areas/Cardiac/Reports"), "rptBalanceSheet_experiment.rdlc");//rptBalanceSheet
        //    if (System.IO.File.Exists(path))
        //    {
        //        lr.ReportPath = path;
        //    }
        //    else
        //    {
        //        ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
        //        return View("Index");
        //    }

        //    ReportDataSource rd = new ReportDataSource("dsBalanceSheet", balanceSheet);
        //    lr.DataSources.Add(rd);
        //    lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

        //    string reportType = getParameters.ReportTypeId;
        //    string mimeType;
        //    string encoding;
        //    string fileNameExtension;



        //    string deviceInfo =

        //          "<DeviceInfo>" +
        //        "  <OutputFormat>EMF</OutputFormat>" +
        //        "  <PageWidth>10in</PageWidth>" +
        //        "  <PageHeight>11in</PageHeight>" +
        //        "  <MarginTop>0.25in</MarginTop>" +
        //        "  <MarginLeft>0.25in</MarginLeft>" +
        //        "  <MarginRight>0.25in</MarginRight>" +
        //        "  <MarginBottom>0.25in</MarginBottom>" +
        //        "</DeviceInfo>";
        //    Warning[] warnings;
        //    string[] streams;

        //    var renderedBytes = lr.Render(
        //        reportType,
        //        deviceInfo,
        //        out mimeType,
        //        out encoding,
        //        out fileNameExtension,
        //        out streams,
        //        out warnings
        //        );

        //    return File(renderedBytes, mimeType);
        //}
        public ActionResult GetAllBalanceSheetReport(GetReportDto getParameters)
        {
            DateTime ToDate;
            bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!success)
                ToDate = DateTime.Now;
            CompanyProfileFacade _company = new CompanyProfileFacade();
            DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();

            var _reportAccount = new ReportAccountFacade();
            var Asset = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Asset, getParameters.CompanyProfileIds, null, ToDate, false);
            var Equity = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Equity, getParameters.CompanyProfileIds, null, ToDate, false);
            var Liability = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Liability, getParameters.CompanyProfileIds, null, ToDate, false);
            Asset.Reverse();
            Equity.Reverse();
            Liability.Reverse();
            var prevAsset = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Asset, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), false, true, 0);
            var prevEquity = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Equity, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), false, true, 0);
            var prevLiability = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Liability, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), false, true, 0);
            foreach (var pa in prevAsset)
            {
                var current = Asset.Where(a => a.CompanyProfileId == pa.CompanyProfileId && a.IsPositive == pa.IsPositive && a.ItemAccGroupCode == a.ItemAccGroupCode && a.ItemName == pa.ItemName && a.ParentId == pa.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pa.Amount;
            }
            foreach (var pe in prevEquity)
            {
                var current = Equity.Where(e => e.CompanyProfileId == pe.CompanyProfileId && e.IsPositive == pe.IsPositive && e.ItemAccGroupCode == e.ItemAccGroupCode && e.ItemName == pe.ItemName && e.ParentId == pe.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pe.Amount;
            }
            foreach (var pl in prevLiability)
            {
                var current = Liability.Where(a => a.CompanyProfileId == pl.CompanyProfileId && a.IsPositive == pl.IsPositive && a.ItemAccGroupCode == a.ItemAccGroupCode && a.ItemName == pl.ItemName && a.ParentId == pl.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pl.Amount;
            }

            ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");
            ReportParameter rpFromDate = new ReportParameter("FromDate", CurrentFiscalYear.ToString());
            ReportParameter rpToDate = new ReportParameter("ToDate", ToDate.ToString());

            if (getParameters.CompanyProfileIds != null && getParameters.CompanyProfileIds.Count > 0)
            {
                var company = _company.GetCompanyProfileById((long)getParameters.CompanyProfileIds.FirstOrDefault());
                if (company != null)
                {
                    rpTitle = new ReportParameter("CompanyName", company.Name);
                    rpAddress = new ReportParameter("CompanyAddress", company.Address);
                }
            }

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptBalanceSheet.rdlc");//rptBalanceSheet
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("Asset", Asset);
            ReportDataSource rd1 = new ReportDataSource("Equity", Equity);
            ReportDataSource rd2 = new ReportDataSource("Liability", Liability);
            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress, rpFromDate, rpToDate });

            string reportType = getParameters.ReportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        public ActionResult GetAllBalanceSheetDetailsReport(GetReportDto getParameters)
        {
            DateTime ToDate;
            bool success = DateTime.TryParseExact(getParameters.ToDateText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ToDate);
            if (!success)
                ToDate = DateTime.Now;
            CompanyProfileFacade _company = new CompanyProfileFacade();
            DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();

            var _reportAccount = new ReportAccountFacade();
            var Asset = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Asset, getParameters.CompanyProfileIds, null, ToDate, true);
            var Equity = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Equity, getParameters.CompanyProfileIds, null, ToDate, true);
            var Liability = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Liability, getParameters.CompanyProfileIds, null, ToDate, true);
            //Asset.Reverse();
            //Equity.Reverse();
            //Liability.Reverse();
            var prevAsset = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Asset, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), true, true, 0);
            var prevEquity = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Equity, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), true, true, 0);
            var prevLiability = _reportAccount.CalculateReport(BizConstants.BalanceSheetReportId_Liability, getParameters.CompanyProfileIds, null, ToDate.AddYears(-1), true, true, 0);
            foreach (var pa in prevAsset)
            {
                var current = Asset.Where(a => a.CompanyProfileId == pa.CompanyProfileId && a.IsPositive == pa.IsPositive && a.ItemAccGroupCode == a.ItemAccGroupCode && a.ItemName == pa.ItemName && a.ParentId == pa.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pa.Amount;
            }
            foreach (var pe in prevEquity)
            {
                var current = Equity.Where(e => e.CompanyProfileId == pe.CompanyProfileId && e.IsPositive == pe.IsPositive && e.ItemAccGroupCode == e.ItemAccGroupCode && e.ItemName == pe.ItemName && e.ParentId == pe.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pe.Amount;
            }
            foreach (var pl in prevLiability)
            {
                var current = Liability.Where(a => a.CompanyProfileId == pl.CompanyProfileId && a.IsPositive == pl.IsPositive && a.ItemAccGroupCode == a.ItemAccGroupCode && a.ItemName == pl.ItemName && a.ParentId == pl.ParentId).FirstOrDefault();
                if (current != null)
                    current.AdditionalAmount = pl.Amount;
            }

            ReportParameter rpTitle = new ReportParameter("CompanyName", "Consolidated Report");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "...");
            ReportParameter rpFromDate = new ReportParameter("FromDate", CurrentFiscalYear.ToString());
            ReportParameter rpToDate = new ReportParameter("ToDate", ToDate.ToString());

            if (getParameters.CompanyProfileIds != null && getParameters.CompanyProfileIds.Count > 0)
            {
                var company = _company.GetCompanyProfileById((long)getParameters.CompanyProfileIds.FirstOrDefault());
                if (company != null)
                {
                    rpTitle = new ReportParameter("CompanyName", company.Name);
                    rpAddress = new ReportParameter("CompanyAddress", company.Address);
                }
            }

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptBalanceSheet.rdlc");

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("Asset", Asset);
            ReportDataSource rd1 = new ReportDataSource("Equity", Equity);
            ReportDataSource rd2 = new ReportDataSource("Liability", Liability);
            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);
            lr.DataSources.Add(rd2);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress, rpFromDate, rpToDate });

            string reportType = getParameters.ReportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }

    }
}