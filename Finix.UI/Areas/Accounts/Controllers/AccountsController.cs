﻿using Finix.Accounts.Dto;
using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using Finix.Accounts.Infrastructure;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using Finix.HRM.Facade;
using Finix.UI.Controllers;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Finix.UI.Areas.Accounts.Controllers
{
    public class AccountsController : BaseController
    {
        private readonly AccountsFacade _accountsFacade;
        private readonly CompanyProfileFacade _companyProfile = new CompanyProfileFacade();
        public AccountsController(AccountsFacade accountsFacade)
        {
            this._accountsFacade = accountsFacade;
        }
        public ActionResult VoucherEntry()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        public ActionResult ChartOfAccount()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            return View();
        }
        public ActionResult BillCollection()
        {
            return View();
        }
        public ActionResult VoucherSearch()
        {
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            return View();
        }
        public ActionResult VoucherEdit()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveVoucher(AccountDto model)
        {
            string voucherNo = "";
            var cpFacade = new CompanyProfileFacade();
            ResponseDto response = new ResponseDto();
            var firstElement = model.VoucherDetails.FirstOrDefault();
            if (firstElement == null)
            {
                response.Success = false;
                response.Message = "Please Give Voucher Entry First";
                return Json(new { response, VoucherNo = "0" });
                //return Json(new { Success = false, Message = "Please Give Voucher Entry First", VoucherNo = "0" });
            }
            if (firstElement.AccTranType == AccTranType.Journal)
            {
                voucherNo = "JV-" + cpFacade.GetUpdateJvNo();
            }
            else if (firstElement.AccTranType == AccTranType.BankTran)
            {
                voucherNo = "JV-C-" + cpFacade.GetUpdateJvNo();
            }
            else if (firstElement.AccTranType == AccTranType.Payment)
            {
                if (firstElement.PaymentType == 1)
                    voucherNo = "DV-" + cpFacade.GetUpdateCDvNo();
                else
                    voucherNo = "BDV-" + cpFacade.GetUpdateBDvNo();
                //var acceptance = _accountsFacade.PaymentBalanceCheck((long)firstElement.CompanyProfileId, firstElement.PaymentType, firstElement.BankAccountHead, model.VoucherDetails.Sum(m=>m.Debit));
                //if (acceptance.Success == true)
                //{
                //    if (firstElement.PaymentType == 1)
                //        voucherNo = "DV-" + cpFacade.GetUpdateCDvNo();
                //    else
                //        voucherNo = "BDV-" + cpFacade.GetUpdateBDvNo();
                //}
                //else
                //    return Json(acceptance, JsonRequestBehavior.AllowGet);
            }
            else if (firstElement.AccTranType == AccTranType.Receive)
            {
                if (firstElement.PaymentType == 1)
                    voucherNo = "CV-" + cpFacade.GetUpdateCCvNo();
                else
                    voucherNo = "BCV-" + cpFacade.GetUpdateBCvNo();
            }

            model.VoucherNo = voucherNo;
            if (model.VoucherDetails.Any())
            {
                foreach (var item in model.VoucherDetails)
                {
                    item.VoucherNo = voucherNo;

                    DateTime date = DateTime.Now;
                    DateTime date2 = DateTime.Now;
                    var vochrDt = DateTime.TryParseExact(item.VoucherDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date2);
                    if (vochrDt)
                        item.VoucherDate = date2;
                    var cheq = DateTime.TryParseExact(item.ChequeDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                    if (cheq)
                        item.ChequeDate = date;
                }
            }

            var account = new ResponseDto();
            if (firstElement.AccTranType == AccTranType.Journal)//firstElement.VoucherType == "jrnl"
                account = _accountsFacade.SaveJournalVoucher(model, SessionHelper.UserProfile.UserId);
            else
                account = _accountsFacade.SaveVoucher(model, SessionHelper.UserProfile.UserId);
            
            return Json(new { Success = account.Success, Message = account.Message, VoucherNo = (bool)account.Success ? model.VoucherNo : "", ChequeId = account.Id}, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult SearchVoucher(string accTranType, string payType, string bankAccHeadCode,
                                        DateTime? dateFrom, DateTime? dateTo, string tranType, string voucherNo, long officeId)
        {
            List<VoucherDto> result = null;
            if (dateFrom == null)
                dateFrom = DateTime.MinValue;
            if (dateTo == null)
                dateTo = DateTime.MaxValue;

            if (string.IsNullOrEmpty(accTranType))
                result = _accountsFacade.SearchVoucher(null, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "pmnt")
                result = _accountsFacade.SearchVoucher(AccTranType.Payment, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "rcv")
                result = _accountsFacade.SearchVoucher(AccTranType.Receive, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "jrnl")
                result = _accountsFacade.SearchVoucher(AccTranType.Journal, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "contr")
                result = _accountsFacade.SearchVoucher(AccTranType.BankTran, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            return Json(result.OrderByDescending(r => r.VoucherDate).ThenBy(r => r.Id), JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveAccHead(AccHeadDto model)
        {
            bool success = false;
            try
            {
                if (model.Id == 0)
                {

                    string code = model.AccGroupId != 0 ? model.AccGroupId.ToString() : "0";
                    if (code != "0")
                    {

                        code = model.CurrentCode;
                        if (code.Length == 1)
                        {
                            code = code + "000000";
                            model.TierId = 1;
                        }
                        else if (code.Length == 3)
                        {
                            code = code + "0000";
                            model.TierId = 2;
                        }
                        else if (code.Length == 5)
                        {
                            code = code + "00";
                            model.TierId = 3;
                        }
                        else
                        {
                            model.TierId = 4;
                        }

                        model.Name = model.Name;
                        model.Code = _accountsFacade.GetLatestCode(code);

                        model.CreatedBy = SessionHelper.UserProfile.UserId;
                        model.CreateDate = DateTime.Now;
                        model.EditedBy = SessionHelper.UserProfile.UserId;
                        model.EditDate = DateTime.Now;

                    }
                }

                success = _accountsFacade.SaveAccHead(model);

                if (success)
                    return Json(new { Success = true, Message = "Account Head Added Successfully." });
                else
                    return Json(new { Success = false, Message = "Model is not valid." });
            }
            catch
            { return Json(new { Success = false, Message = "Model is not valid." }); }

        }
        //public JsonResult SaveAccHeadMapping(AccHeadDto model)
        //{
        //    var success = _accountsFacade.SaveAccHeadMapping(model);
        //    if (success)
        //        return Json(new { Success = true, Message = "Account Head Saved Successfully." });
        //    else
        //        return Json(new { Success = false, Message = "Account Head Not Saved. Please try again or contact Admin." });
        //}
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetAccountHeadsByAccountGroupCode(string accountGroupCode)
        {
            var accountHeads = _accountsFacade.GetAccountHeadsByAccountGroupCode(accountGroupCode);
            accountHeads = accountHeads.Where(x => x.Code.Contains(accountGroupCode)).OrderBy(x => x.AccHeadName).ToList();
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccountHeadsByStartWithCode(string code, long? officeId)
        {
            if (officeId == null)
            {
                var accountHeads = _accountsFacade.GetAccountHeadsByAccountGroupCode(code).OrderBy(x => x.AccHeadName);
                return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var accountHeads = _accountsFacade.GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId).OrderBy(x => x.AccHeadName);
                return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAccHeadsForAssetExpenditure(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetAccHeadsForAssetExpenditure(companyProfileId).OrderBy(x => x.AccHeadName);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllAccHeads(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetAllAccHeads(companyProfileId);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetConcatedAccHeads(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetConcatedAccHeads(companyProfileId);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetCashHeads()
        //{
        //    var accountHeads = _accountsFacade.GetCashHeads();
        //    return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetAllAccHeadGroup()
        {
            var data = _accountsFacade.GetAllAccHeadGroup();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllAccSubGroup()
        {
            var data = _accountsFacade.GetAllAccSubGroup();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllAccGroups()
        {
            var accGroups = _accountsFacade.GetAllAccGroups();
            return Json(accGroups.Select(x => new { Id = x.Id, AccountGroupCode = x.Code, Name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccSubGroupsByAccGroupId(int accGroupId)
        {

            var accSubGroups = _accountsFacade.GetAccSubGroupsByAccGroupId(accGroupId);
            return Json(accSubGroups.Select(x => new { Id = x.Id, AccountSubGroupCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccHeadGroupsByAccSubGroupId(int accSubGroupId)
        {
            var accHeadGroups = _accountsFacade.GetAccHeadGroupsByAccSubGroupId(accSubGroupId);
            return Json(accHeadGroups.Select(x => new { Id = x.Id, AccountHeadGroupCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccHeadSubGroupsByAccHeadGroupId(int accHeadGroupId)
        {
            var accHeadSubGroups = _accountsFacade.GetAccHeadSubGroupsByAccHeadGroupId(accHeadGroupId);
            return Json(accHeadSubGroups.Select(x => new { Id = x.Id, AccountHeadSubGroupCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccHeadsByAccHeadSubGroupId(int accHeadSubGroupId, long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetAccHeadsByAccHeadSubGroupId(accHeadSubGroupId, companyProfileId).OrderBy(x => x.AccHeadName);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, name = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccHeadByAccHeadId(long accHeadId)
        {
            var accountHeads = _accountsFacade.GetAccHeadByAccHeadId(accHeadId);
            return Json(accountHeads, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBankAccHeads(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetAccHeadsByRefType(2, companyProfileId).OrderBy(x => x.AccHeadName);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, BankName = x.Name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBankAccHeadByBankId(long BankId)
        {
            var accHead = _accountsFacade.GetAccHeadByRefIdAndRefType(BankId, ReferenceAccountType.Bank, null);
            return Json(accHead, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFixedAssetAccHeads(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetFixedAssetAccHeads(companyProfileId).OrderBy(x => x.AccHeadName);
            return Json(accountHeads.Select(x => new { Id = x.Id, AccountHeadCode = x.Code, Name = x.Name }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExpenseAccHeads(long? companyProfileId)
        {
            var accountHeads = _accountsFacade.GetExpenseAccHeads(companyProfileId);
            return Json(accountHeads, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAccHeadsForAutoComplete(AutofillDto dto)
        {
            var result = _accountsFacade.GetCustomerListForAutoFill(dto.prefix, dto.exclusionList, (long)dto.officeId, (long)dto.groupid);
            var data = result.Select(r => new { key = r.Id, value = r.Name }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetJurnalVoucherByVoucherNo(string reportTypeId, string voucherNo, long? companyProfileId)
        {
            var vouchers = _accountsFacade.GetVoucherJurnalVoucherByVoucherNo(voucherNo);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptJournalVoucher.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherEntry");
            }
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("dsVoucher", vouchers);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }
        public ActionResult GetVoucherReport(string reportTypeId, string voucherNo, long? companyProfileId)
        {
            var vouchers = _accountsFacade.GetVoucherByVoucherNo(voucherNo);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptVoucher.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherEntry");
            }
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("dsVoucher", vouchers);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }
        public ActionResult GetDrVoucherReport(string reportTypeId)
        {
            var vouchers = new List<VoucherDto>();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptDebitVoucherDemo.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }

            ReportDataSource rd = new ReportDataSource("DataSet1", vouchers);
            lr.DataSources.Add(rd);


            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>10in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        public ActionResult GetCrVoucherReport(string reportTypeId, long? companyProfileId)
        {
            var vouchers = new List<VoucherDto>();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptCreditVoucher.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("DataSet1", vouchers);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        public ActionResult GetJvVoucherReport(string reportTypeId, long? companyProfileId)
        {
            var vouchers = new List<VoucherDto>();
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptJournalVoucher.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            ReportDataSource rd = new ReportDataSource("DataSet1", vouchers);
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }
        public JsonResult GetVoucherById(long vId)
        {
            var result = _accountsFacade.GetVoucherById(vId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveEditVoucher(VoucherDto model)
        {
            var Success = _accountsFacade.SaveEditVoucher(model);
            if (Success)
                return Json(new { Success, Message = "Voucher is successfully updated", VoucherNo = model.VoucherNo });
            else
                return Json(new { Success, Message = "Voucher not updated", VoucherNo = model.VoucherNo });
        }
        public ActionResult VoucherListForCreator(string sortOrder, string currentFilter, string searchDate, List<long?> companyIds, int page = 1)
        {
            DateTime SearchDate;

            var converted = DateTime.TryParseExact(searchDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SearchDate);
            if (!converted)
                SearchDate = _companyProfile.DateToday((long)SessionHelper.UserProfile.SelectedCompanyId);

            ViewBag.CurrentFilter = searchDate;
            ViewBag.SearchDate = SearchDate.ToString("yyyy-MM-dd");
            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.companyIds = companyIds;
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            if (companyIds == null)
            {
                companyIds = new List<long?>();
                companyIds.Add(SessionHelper.UserProfile.SelectedCompanyId);
            }
            var temp = _accountsFacade.GetVoucherListForCreator(SearchDate, 20, page, currentFilter, sortOrder, SessionHelper.UserProfile.UserId, companyIds);

            return View(temp);
        }
        public ActionResult VoucherListForVerification(string sortOrder, string currentFilter, string searchDate, List<long?> companyIds, int page = 1)
        {
            DateTime SearchDate;

            var converted = DateTime.TryParseExact(searchDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SearchDate);
            if (!converted)
                SearchDate = _companyProfile.DateToday((long)SessionHelper.UserProfile.SelectedCompanyId);

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.companyIds = companyIds;
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            if (companyIds == null)
            {
                companyIds = new List<long?>();
                companyIds.Add(SessionHelper.UserProfile.SelectedCompanyId);
            }

            ViewBag.CurrentFilter = searchDate;
            ViewBag.SearchDate = SearchDate.ToString("yyyy-MM-dd");
            var temp = _accountsFacade.GetVoucherListForVerification(20, page, SearchDate, companyIds);

            return View(temp);
        }
        [HttpPost]
        public JsonResult SaveVouchersVerified(List<PostingStatusUpdate> poList)
        {
            //var acceptable = _companyProfile.GetDailyTransactionState();
            //if (acceptable.Success == false)
            //    return Json(new { Success = false, Message = "System is not open for daily transactions. Please contact system administrator." }, JsonRequestBehavior.DenyGet);

            var result = _accountsFacade.SaveVouchersVerified(poList, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.DenyGet);
        }
        public ActionResult VoucherListForAuthorization(string sortOrder, string currentFilter, string searchDate, List<long?> companyIds, int page = 1)
        {
            DateTime SearchDate;

            var converted = DateTime.TryParseExact(searchDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SearchDate);
            if (!converted)
                SearchDate = _companyProfile.DateToday((long)SessionHelper.UserProfile.SelectedCompanyId);

            ViewBag.CompanyId = SessionHelper.UserProfile.SelectedCompanyId;
            ViewBag.Companies = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Select(s => new { s.Id, s.Name }).ToList();
            ViewBag.companyIds = companyIds;
            ViewBag.CompanyCount = SessionHelper.UserProfile.CurrentlyAccessibleCompanies.Count();
            if (companyIds == null)
            {
                companyIds = new List<long?>();
                companyIds.Add(SessionHelper.UserProfile.SelectedCompanyId);
            }

            ViewBag.CurrentFilter = searchDate;
            ViewBag.SearchDate = SearchDate.ToString("yyyy-MM-dd");
            var temp = _accountsFacade.GetVoucherListForAuthorization(20, page, SearchDate, companyIds);

            return View(temp);
        }
        [HttpPost]
        public JsonResult SaveVouchersAuthorization(List<PostingStatusUpdate> poList)
        {
            //var acceptable = _companyProfile.GetDailyTransactionState();
            //if (acceptable.Success == false)
            //    return Json(new { Success = false, Message = "System is not open for daily transactions. Please contact system administrator." }, JsonRequestBehavior.DenyGet);

            var result = _accountsFacade.SaveVouchersAuthorized(poList, SessionHelper.UserProfile.UserId);
            return Json(result, JsonRequestBehavior.DenyGet);
        }

        public ActionResult GetVoucherListForCreatorReport(string reportTypeId, string date, List<long?> companyIds)
        {
            DateTime Date;
            var currentSystemDate = _companyProfile.DateToday(null);
            Date = currentSystemDate;

            bool DateConverted = DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date);

            UserFacade _user = new UserFacade();

            var empId = _user.GetEmployeeIdByUserId(SessionHelper.UserProfile.UserId);
            EmployeeFacade _employee = new EmployeeFacade();

            var employee = _employee.GetBasicInfoById(empId);
            var empName = employee.Name;

            var rptVoucherlst = _accountsFacade.GetVoucherListForCreatorReport(Date, SessionHelper.UserProfile.UserId, companyIds);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "voucherList.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherListForCreator");
            }

            ReportParameter rp = new ReportParameter("UserName", empName);
            ReportParameter rp1 = new ReportParameter("ReportDate", date);

            ReportDataSource rd = new ReportDataSource("voucherList", rptVoucherlst);
            lr.DataSources.Add(rd);
            lr.SetParameters(rp);
            lr.SetParameters(rp1);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.2in</PageWidth>" +
                "  <PageHeight>7.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );


            return File(renderedBytes, mimeType);


        }

        public ActionResult GetDateWiseVoucherSummary(string reportTypeId, string date, List<long?> companyIds)
        {
            DateTime Date;
            var currentSystemDate = _companyProfile.DateToday(null);
            Date = currentSystemDate;

            bool DateConverted = DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date);

            UserFacade _user = new UserFacade();

            var empId = _user.GetEmployeeIdByUserId(SessionHelper.UserProfile.UserId);
            EmployeeFacade _employee = new EmployeeFacade();

            var employee = _employee.GetBasicInfoById(empId);
            var empName = employee.Name;

            var rptVoucherlst = _accountsFacade.GetDateWiseVoucherSummary(Date, SessionHelper.UserProfile.UserId, companyIds);

            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "dateWiseVoucherSummary.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherListForCreator");
            }

            //ReportParameter rp = new ReportParameter("UserName", empName);
            ReportParameter rp1 = new ReportParameter("ReportDate", date);

            ReportDataSource rd = new ReportDataSource("voucherList", rptVoucherlst);
            lr.DataSources.Add(rd);
            //lr.SetParameters(rp);
            lr.SetParameters(rp1);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.2in</PageWidth>" +
                "  <PageHeight>7.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);
        }

        public ActionResult AccHeadMapping()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetAccHeadMappingData()
        {
            var data = _accountsFacade.GetAccHeadMappingData();
            //var jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            //jsonResult.MaxJsonLength = int.MaxValue;
            //return jsonResult;
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetCurrentOfficeAccHeads(long officeId)
        {
            var data = _accountsFacade.GetAccHeadMappingData(officeId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveAccHeadMappings(AccHeadPermissionDto accHeadPermissions)
        {
            var success = _accountsFacade.SaveAccHeadMappings(accHeadPermissions.Ids, accHeadPermissions.OfficeId, SessionHelper.UserProfile.UserId);
            return Json(success, JsonRequestBehavior.DenyGet);
        }

        public ActionResult AccountGroupsEntryPage()
        {
            return View();
        }
        public JsonResult SaveAccountGroup(AccountGroupDto dto)
        {
            var result = new ResponseDto();
            try
            {
                if (dto.AccGroup == 1)
                {
                    result = _accountsFacade.SaveAccountSubGroup(dto, SessionHelper.UserProfile.UserId);
                }
                else if (dto.AccGroup == 2)
                {
                    result = _accountsFacade.SaveAccountHeadGroup(dto, SessionHelper.UserProfile.UserId);
                }
                else if (dto.AccGroup == 3)
                {
                    result = _accountsFacade.SaveAccountHeadSubGroup(dto, SessionHelper.UserProfile.UserId);
                }




                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetVoucherPrintByVoucherNo(string reportTypeId, string voucherNo, long? companyProfileId)
        {
            var vouchers = _accountsFacade.GetVoucherListPrintReportByVoucherNo(voucherNo);
            var company = _companyProfile.GetCompanyProfileById((long)companyProfileId);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptVoucherPrint.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherSearch");
            }

            ReportDataSource rd = new ReportDataSource("dsVoucher", vouchers);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            ReportParameter rpType = new ReportParameter("Type", "Voucher");
            if (voucherNo.ToLower().Contains("cv"))
                rpType = new ReportParameter("Type", "Receipt Voucher");
            else if (voucherNo.ToLower().Contains("dv"))
                rpType = new ReportParameter("Type", "Payment Voucher");
            else if (voucherNo.ToLower().Contains("jv"))
                rpType = new ReportParameter("Type", "Journal Voucher");
            else if (voucherNo.ToLower().Contains("bn"))
                rpType = new ReportParameter("Type", "Sales Voucher");
            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress, rpType });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>11.6in</PageWidth>" +
                "  <PageHeight>8.2in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }

        public ActionResult GetVoucherList(string reportTypeId, string accTranType, string payType, string bankAccHeadCode,
                                        DateTime? dateFrom, DateTime? dateTo, string tranType, string voucherNo, long officeId)
        {
            List<VoucherDto> result = null;
            if (dateFrom == null)
                dateFrom = DateTime.MinValue;
            if (dateTo == null)
                dateTo = DateTime.MaxValue;
            if (string.IsNullOrEmpty(accTranType))
                result = _accountsFacade.SearchVoucher(null, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "pmnt")
                result = _accountsFacade.SearchVoucher(AccTranType.Payment, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "rcv")
                result = _accountsFacade.SearchVoucher(AccTranType.Receive, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);
            else if (accTranType == "jrnl")
                result = _accountsFacade.SearchVoucher(AccTranType.Journal, payType, bankAccHeadCode, (DateTime)dateFrom, (DateTime)dateTo, tranType, voucherNo, officeId);

            // result = result.OrderByDescending(r => r.VoucherDate).ThenBy(r => r.Id);
            var company = _companyProfile.GetCompanyProfileById((long)officeId);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "rptVoucherSearchResult.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("VoucherSearch");
            }

            ReportDataSource rd = new ReportDataSource("VoucherSearchresult", result);
            ReportParameter rpTitle = new ReportParameter("CompanyName", "");
            ReportParameter rpAddress = new ReportParameter("CompanyAddress", "");
            ReportParameter rpType = new ReportParameter("Type", "Voucher");

            if (accTranType == "pmnt")
                rpType = new ReportParameter("Type", "Payment Voucher");
            else if (accTranType == "rcv")
                rpType = new ReportParameter("Type", "Receive Voucher");
            else if (accTranType == "jrnl")
                rpType = new ReportParameter("Type", "Journal Voucher");
            else 
                rpType = new ReportParameter("Type", "All Voucher");

            if (company != null)
            {
                rpTitle = new ReportParameter("CompanyName", company.Name);
                rpAddress = new ReportParameter("CompanyAddress", company.Address);
            }
            lr.DataSources.Add(rd);
            lr.SetParameters(new ReportParameter[] { rpTitle, rpAddress, rpType });

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;

            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);


        }

        #region Bank Reconciliation

        public ActionResult BankReconsilation()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetBankReconsilationTypes()
        {
            var data = _accountsFacade.GetBankReconsilationTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBankReconsilation(BankReconsilationDto dto)
        {
            try
            {
                DateTime receiveDate = DateTime.Now;
                var FromConverted = DateTime.TryParseExact(dto.MonthEndingDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out receiveDate);
                if (FromConverted)
                    dto.MonthEndingDate = receiveDate;
                var crmConverted = DateTime.TryParseExact(dto.ReviewDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out receiveDate);
                if (crmConverted)
                    dto.ReviewDate = receiveDate;
                
                if (dto.Details != null)
                {
                    var deatils = new List<BankReconsilationDetailsDto>();
                    foreach (var fdrpsDetailDto in dto.Details)
                    {
                        DateTime mtDate = DateTime.Now;
                        FromConverted = DateTime.TryParseExact(fdrpsDetailDto.TranDateTxt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out mtDate);
                        if (FromConverted)
                            fdrpsDetailDto.TranDate = mtDate;
                        deatils.Add(fdrpsDetailDto);
                    }
                    dto.Details = deatils;
                }
                
                var result = _accountsFacade.SaveBankReconsilation(dto, SessionHelper.UserProfile.UserId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BankReconsilationList(string sortOrder, string currentFilter, string searchString, long? sourceid, int page = 1, int pageSize = 20)
        {
            ViewBag.SearchString = searchString;
            ViewBag.CurrentSort = sortOrder;
            var applications = _accountsFacade.BankReconsilationList(pageSize, page, searchString);
            return View(applications);
        }

        public ActionResult GetBankReconsilationReportById(string reportTypeId, long id)
        {
            var data = _accountsFacade.GetBankReconsilationById(id);
            var mrList = new List<BankReconsilationDto>();
            mrList.Add(data);
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Areas/Accounts/Reports"), "BankReconsilation.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("BankReconsilationList");
            }

            ReportDataSource rd = new ReportDataSource("dsBankReconsilation", mrList);
            ReportDataSource rd1 = new ReportDataSource("dsBankReconsilationDetails", data.Details);

            lr.DataSources.Add(rd);
            lr.DataSources.Add(rd1);

            string reportType = reportTypeId;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                  "<DeviceInfo>" +
                "  <OutputFormat>EMF</OutputFormat>" +
                "  <PageWidth>8.2in</PageWidth>" +
                "  <PageHeight>11.6in</PageHeight>" +
                "  <MarginTop>0.25in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.25in</MarginBottom>" +
                "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );
            return File(renderedBytes, mimeType);
        }
        #endregion
    }
}