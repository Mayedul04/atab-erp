﻿using Finix.Auth.Facade;
using Finix.UI.Areas.Auth.Controllers;
using Quartz;
using Quartz.Impl;
using System;
using System.Linq;

namespace Finix.UI.ScheduledTasks
{
    //public class EmailJob : IJob
    //{
    //    public void Execute(IJobExecutionContext context)
    //    {
    //        using (var message = new MailMessage("user@gmail.com", "user@live.co.uk"))
    //        {
    //            message.Subject = "Test";
    //            message.Body = "Test at " + DateTime.Now;
    //            using (SmtpClient client = new SmtpClient
    //            {
    //                EnableSsl = true,
    //                Host = "smtp.gmail.com",
    //                Port = 587,
    //                Credentials = new NetworkCredential("user@gmail.com", "password")
    //            })
    //            {
    //                client.Send(message);
    //            }
    //        }
    //    }
    //}
    //public class JobScheduler
    //{
    //    public static void Start()
    //    {
    //        IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
    //        scheduler.Start();

    //        IJobDetail job = JobBuilder.Create<EmailJob>().Build();

    //        ITrigger trigger = TriggerBuilder.Create()
    //            .WithDailyTimeIntervalSchedule
    //              (s =>
    //                 s.WithIntervalInHours(24)
    //                .OnEveryDay()
    //                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
    //              )
    //            .Build();

    //        scheduler.ScheduleJob(job, trigger);
    //    }
    //}
    public class DayClosingJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var companyProfileFacade = new CompanyProfileFacade();
            var companies = companyProfileFacade.GetAllActiveCompanyProfiles();
            var companyProfileController = new CompanyProfileController(companyProfileFacade);
            var companiesWithExceptions = "";
            if (companies != null && companies.Where(c => c.CompanyType != Auth.Infrastructure.CompanyType.Project).Count() > 0)
            {
                foreach (var company in companies.Where(c => c.CompanyType != Auth.Infrastructure.CompanyType.Project))
                {
                    try
                    {
                        var response = companyProfileController.UpdateClosingDate(company.Id);
                        if (response.Data.ToString().Contains("false"))
                            companiesWithExceptions += company.Name + ", ";
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            if (!string.IsNullOrEmpty(companiesWithExceptions))
            {
                //todo - email to admin regarding the problems
                //using (var message = new MailMessage("user@gmail.com", "user@live.co.uk"))
                //{
                //    message.Subject = "Test";
                //    message.Body = "Test at " + DateTime.Now;
                //    using (SmtpClient client = new SmtpClient
                //    {
                //        EnableSsl = true,
                //        Host = "smtp.gmail.com",
                //        Port = 587,
                //        Credentials = new NetworkCredential("user@gmail.com", "password")
                //    })
                //    {
                //        client.Send(message);
                //    }
                //}
            }

        }
    }

    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<DayClosingJob>().Build();
            
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .WithCronSchedule("0 30 23 * * ?", t => t.InTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Bangladesh Standard Time")))//fires at 11:30 PM
                //.WithCronSchedule("0 45 11 * * ?",t=>t.InTimeZone(TimeZoneInfo.FindSystemTimeZoneById("Bangladesh Standard Time")))//fires at 11:30 PM
                //.WithCronSchedule("0 17 11 * * ?")//for testing
                
                .WithPriority(10)
                .Build();
            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithDailyTimeIntervalSchedule
            //      (s =>
            //         s
            //         .WithIntervalInHours(23)
            //         .WithIntervalInMinutes(30)
            //         //.WithIntervalInMinutes(1)
            //         .OnEveryDay()
            //         .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
            //      )
            //    .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}