﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Finix.Accounts.Infrastructure.Models;
//using Finix.Auth.Infrastructure.Models_and_Mappings.Models;
using Finix.Accounts.Util;

namespace Finix.Accounts.Infrastructure
{
    public interface IFinixAccountsContext
    {
    }

    public partial class FinixAccountsContext : DbContext, IFinixAccountsContext
    {
        #region ctor
        static FinixAccountsContext()
        {
            Database.SetInitializer<FinixAccountsContext>(null);
        }

        public FinixAccountsContext()
            : base("Name=FinixAccountsContext")
        {
            Database.SetInitializer(new FinixAccountsContextInitializer());
        }
        #endregion

        #region Models
        public DbSet<AccGroup> AccGroups { get; set; }
        public DbSet<AccSubGroup> AccSubGroups { get; set; }
        public DbSet<AccHeadGroup> AccHeadGroups { get; set; }
        public DbSet<AccHeadSubGroup> AccHeadSubGroups { get; set; }
        public DbSet<AccHead> AccHeads { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<ChequeRegister> ChequeRegisters { get; set; }
        public DbSet<ChequeRegisterDetail> ChequeRegisterDetails { get; set; }
        public DbSet<ReportConfig> ReportConfigs { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<VoucherArchieve> VoucherArchieves { get; set; }
        public DbSet<VoucherLog> VoucherLog { get; set; }
        public DbSet<BankReconsilation> BankReconsilation { get; set; }
        public DbSet<BankReconsilationDetails> BankReconsilationDetails { get; set; }
        public DbSet<IUOSlip> IUOSlips { get; set; }
        public DbSet<BudgetConfig> BudgetConfig { get; set; }
        public DbSet<Budget> Budget { get; set; }
        public DbSet<BudgetDetail> BudgetDetail { get; set; }
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            FluentConfigurator.ConfigureGenericSettings(modelBuilder);
            FluentConfigurator.ConfigureMany2ManyMappings(modelBuilder);
            FluentConfigurator.ConfigureOne2OneMappings(modelBuilder);
        }
    }

    internal class FinixAccountsContextInitializer : CreateDatabaseIfNotExists<FinixAccountsContext> //CreateDatabaseIfNotExists<CardiacCareContext>
    {
        protected override void Seed(FinixAccountsContext context)
        {
            var seedDataPath = AccountsSystem.SeedDataPath;
            var dropDb = AccountsSystem.DropDB;
            if (string.IsNullOrWhiteSpace(seedDataPath))
                return;
            var folders = Directory.GetDirectories(seedDataPath).ToList().OrderBy(x => x);
            var msg = "";
            bool error = false;
            try
            {
                foreach (var folder in folders)
                {
                    msg += string.Format("processing for: {0}{1}", folder, Environment.NewLine);

                    var fileDir = Path.Combine(new[] { seedDataPath, folder });
                    var sqlFiles = Directory.GetFiles(fileDir, "*.sql").OrderBy(x => x).ToList();
                    foreach (var file in sqlFiles)
                    {
                        try
                        {
                            msg += string.Format("processing for: {0}{1}", file, Environment.NewLine);
                            context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                            msg += string.Format("Done....{0}", Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            error = true;
                            msg += string.Format("Failed!....{0}", Environment.NewLine);
                            msg += string.Format("{0}{1}", ex.Message, Environment.NewLine);
                        }
                    }
                }
                if (error)
                {
                    throw new Exception(msg);
                }
                base.Seed(context);
            }
            catch (Exception ex)
            {
                msg = "Error Occured while inserting seed data" + Environment.NewLine;
                if (dropDb)
                {
                    context.Database.Delete();
                    msg += ("Database is droped" + Environment.NewLine);
                }
                var errFile = seedDataPath + "\\seed_data_error.txt";
                msg += ex.Message;
                File.WriteAllText(errFile, msg);

            }
        }

    }
}