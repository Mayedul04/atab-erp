﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Accounts.Infrastructure
{
    [Table("BudgetConfig")]
    public class BudgetConfig : Entity
    {
        public long AccGroupId { get; set; }
        [ForeignKey("AccGroupId")]
        public virtual AccGroup AccGroup { get; set; }
        public long AccHeadId { get; set; }
        [ForeignKey("AccHeadId")]
        public virtual AccHead AccHead { get; set; }
        public string ControlHead { get; set; }
        public bool HasUnit { get; set; }
        public long OfficeId { get; set; }
    }
}
