﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure.Models
{
    public class IUOSlip :Entity
    {
        public string ReceiverName { get; set; }
        public string Designation { get; set; }
        public decimal Amount { get; set; }
        public DateTime ReceiveDate { get; set; }
        public string Remarks { get; set; }
        public IuoSlipStatus? IuoSlipStatus { get; set; }
        public IuoPurpose? IuoPurpose { get; set; }
        public bool? IsBank { get; set; }
        public string CreditAccHeadCode { get; set; }
        public string DebitAccHeadCode { get; set; }

    }
}
