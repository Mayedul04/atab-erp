﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Accounts.Infrastructure
{
    [Table("ChequeRegisterDetail")]
    public class ChequeRegisterDetail : Entity
    {
        public string ChequeNo { get; set; }
        public long ChequeRegisterId { get; set; }
        public ChequeStatus ChequeStatus { get; set; }
        public DateTime? IssueDate { get; set; }
        public long? IssuedBy { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string PaidTo { get; set; }
        public string PaidFor { get; set; }
        public decimal Amount { get; set; }
        public string VoucherNo { get; set; }
        public string AccHeadCode { get; set; }
        [ForeignKey("ChequeRegisterId"), InverseProperty("Details")]
        public virtual ChequeRegister ChequeRegister { get; set; }
    }
}
