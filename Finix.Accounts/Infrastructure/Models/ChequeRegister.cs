﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Accounts.Infrastructure
{
    [Table("ChequeRegister")]
    public class ChequeRegister : Entity
    {
        public long BankId { get; set; }
        [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }
        public string BranchName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public long CompanyProfileId { get; set; }
        public string ChequePrefix { get; set; }
        public string StartingNumber { get; set; }
        public string EndingNumber { get; set; }
        public long TotalCheques { get; set; }
        public string BankAccountHeadCode { get; set; }
        public DateTime BookIssueDate { get; set; }
        public ICollection<ChequeRegisterDetail> Details { get; set; }
        //[ForeignKey("CompanyProfileId")]
        //public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
