﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccSubGroup")]
    public class AccSubGroup : Entity
    {
        public AccSubGroup()
        {
            AccHeadGroups = new List<AccHeadGroup>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public long AccGroupId { get; set; }


        [ForeignKey("AccGroupId"), InverseProperty("AccSubGroups")]
        public virtual AccGroup AccGroup { get; set; }

        public virtual ICollection<AccHeadGroup> AccHeadGroups { get; set; }

    }
}
