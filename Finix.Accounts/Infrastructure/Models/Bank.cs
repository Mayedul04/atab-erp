﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("Bank")]
    public class Bank : Entity
    {
        public Bank()
        {
            // LCs = new List<LC>();
        }

        public string Name { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string AccountNo { get; set; }
        public string RoutingNo { get; set; }
        public BankAccountType AccountType { get; set; }
        public long CompanyProfileId { get; set; }
        //[ForeignKey("CompanyProfileId")]
        //public virtual CompanyProfile CompanyProfile { get; set; }
        /// <summary>
        /// Comma separated value of signatories
        /// </summary>
        public string Signatories { get; set; }
        //  public virtual ICollection<LC> LCs { get; set; }
    }    
}
