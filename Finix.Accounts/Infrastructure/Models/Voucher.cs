﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("Voucher")]
    public class Voucher : Entity
    {
        public string VoucherNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public long SerialNo { get; set; }
        public string TransactionNo { get; set; }
        public string AccountHeadCode { get; set; }
        public AccTranType AccTranType { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public bool IsCheque { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public long? CompanyProfileId { get; set; }
        public RecordStatus? RecordStatus { get; set; }
        //[ForeignKey("CompanyProfileId")]
        //public virtual CompanyProfile CompanyProfile { get; set; }


    }
}