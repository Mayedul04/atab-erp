﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccGroup")]
    public class AccGroup : Entity
    {
        public AccGroup()
        {
            AccSubGroups = new List<AccSubGroup>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AccSubGroup> AccSubGroups { get; set; }

    }
}
