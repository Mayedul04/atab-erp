﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Finix.Accounts.Infrastructure.Models;

namespace Finix.Accounts.Infrastructure.Models
{
    [Table("BankReconsilation")]
    public class BankReconsilation : Entity
    {
        public long BankId { get; set; }
        [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }
        public string AccontNumber { get; set; }
        public DateTime? MonthEndingDate { get; set; }
        public decimal ActualBankBalance { get; set; }
        public decimal AdjustedBankBalance { get; set; }
        public decimal BalancePerLedger { get; set; }
        public decimal AdjustedLedgerBalance { get; set; }
        public decimal Variance { get; set; }
        public virtual ICollection<BankReconsilationDetails> Details { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewDate { get; set; }
    }
}
