﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Accounts.Infrastructure.Models
{
    [Table("BankReconsilationDetails")]
    public class BankReconsilationDetails : Entity
    {
        public long BRId { get; set; }
        [ForeignKey("BRId"), InverseProperty("Details")]
        public BankReconsilation BankReconsilation { get; set; }
        public BankReconsilationType BankReconsilationType { get; set; }
        public DateTime? TranDate { get; set; }
        public decimal Amount { get; set; }
    }
}
