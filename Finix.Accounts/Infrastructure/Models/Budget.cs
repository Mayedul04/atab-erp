﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Accounts.Infrastructure
{
    [Table("Budget")]
    public class Budget : Entity
    {
        public BudgetStatus BudgetStatus { get; set; }
        public long OfficeId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public virtual ICollection<BudgetDetail> Details { get; set; }
        public long? ApprovedBy { get; set; }
        public string ApprovedByName { get; set; }
    }
    [Table("BudgetDetail")]
    public class BudgetDetail : Entity
    {
        public long BudgetId { get; set; }
        [ForeignKey("BudgetId"), InverseProperty("Details")]
        public virtual Budget Budget { get; set; }
        public long BudgetConfigId { get; set; }
        [ForeignKey("BudgetConfigId")]
        public virtual BudgetConfig BudgetConfig { get; set; }
        public decimal Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal PreviousBudget { get; set; }
        public decimal PreviousActualAmount { get; set; }
        public decimal IncreasePercentage { get; set; }
        public decimal IncreaseAmount { get; set; }
        public decimal ProposedAmount { get; set; }
    }
}
