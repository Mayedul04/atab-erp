﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccHeadGroup")]
    public class AccHeadGroup : Entity
    {
        public AccHeadGroup()
        {
            AccHeadSubGroups = new List<AccHeadSubGroup>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public long AccSubGroupId { get; set; }
        public CashFlowStatementActivities? CashFlowStatementActivity { get; set; }

        [ForeignKey("AccSubGroupId"), InverseProperty("AccHeadGroups")]
        public virtual AccSubGroup AccSubGroup { get; set; }

        public virtual ICollection<AccHeadSubGroup> AccHeadSubGroups { get; set; }

    }
}
