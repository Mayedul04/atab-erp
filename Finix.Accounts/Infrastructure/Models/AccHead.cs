﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccHead")]
    public class AccHead : Entity
    {
        public long AccHeadMainId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AccGroupId { get; set; }
        public long? AccSubGroupId { get; set; }
        public long? AccHeadGroupId { get; set; }
        public long? AccHeadSubGroupId { get; set; }
        public int? TierId { get; set; }
        public long? RefId { get; set; }
        public ReferenceAccountType? RefType { get; set; }
        public long? CompanyProfileId { get; set; }

        [ForeignKey("AccHeadMainId")]
        public virtual AccHeadMain AccHeadMain { get; set; }

        //[ForeignKey("CompanyProfileId")]
        //public virtual CompanyProfile CompanyProfile { get; set; }

        //[ForeignKey("AccHeadSubGroupId"), InverseProperty("AccHeads")]
        //public virtual AccHeadSubGroup AccHeadSubGroup { get; set; }

    }
}
