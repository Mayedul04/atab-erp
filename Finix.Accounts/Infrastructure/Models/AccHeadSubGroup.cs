﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccHeadSubGroup")]
    public class AccHeadSubGroup : Entity
    {
        public AccHeadSubGroup()
        {
            AccHeads = new List<AccHead>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public long AccHeadGroupId { get; set; }


        [ForeignKey("AccHeadGroupId"), InverseProperty("AccHeadSubGroups")]
        public virtual AccHeadGroup AccHeadGroup { get; set; }

        public virtual ICollection<AccHead> AccHeads { get; set; }
    }
}
