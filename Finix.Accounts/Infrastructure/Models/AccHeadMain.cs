﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccHeadMain")]
    public class AccHeadMain : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AccGroupId { get; set; }
        public long? AccSubGroupId { get; set; }
        public long? AccHeadGroupId { get; set; }
        public long? AccHeadSubGroupId { get; set; }
        [ForeignKey("AccGroupId")]
        public virtual AccGroup AccGroup { get; set; }
        [ForeignKey("AccSubGroupId")]
        public virtual AccSubGroup AccSubGroup { get; set; }
        [ForeignKey("AccHeadGroupId")]
        public virtual AccHeadGroup AccHeadGroup { get; set; }
        [ForeignKey("AccHeadSubGroupId")]
        public virtual AccHeadSubGroup AccHeadSubGroup { get; set; }
        public int? TierId { get; set; }
        public long? RefId { get; set; }
        public ReferenceAccountType? RefType { get; set; }
    }
}
