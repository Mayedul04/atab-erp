﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("ReportConfig")]
    public class ReportConfig : Entity
    {
        public long ReportId { get; set; }
        public string ReportName { get; set; }
        public long ParentId { get; set; }
        public string ItemName { get; set; }
        public string ItemAccGroupCode { get; set; }
        public bool? GetAccHeads { get; set; }
        public bool IsPositive { get; set; }
        public bool HasChild { get; set; }
        public long? CompanyProfileId { get; set; }
        public ReportExternalFunction? ExternalFunction { get; set; }
    }
}
