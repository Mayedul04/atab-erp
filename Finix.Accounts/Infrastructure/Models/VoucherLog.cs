﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("VoucherLog")]
    public class VoucherLog : Entity
    {
        public long VoucherId { get; set; }
        public RecordStatus? FromRecordStatus { get; set; }
        public RecordStatus ToRecordStatus { get; set; }
        [ForeignKey("VoucherId")]
        public virtual Voucher Voucher { get; set; }

    }
}
