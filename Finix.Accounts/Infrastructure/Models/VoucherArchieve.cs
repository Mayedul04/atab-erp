﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("VoucherArchieve")]
    public class VoucherArchieve : Entity
    {
        public DateTime ArchieveDate { get; set; }
        public string AccountHeadCode { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public long? CompanyProfileId { get; set; }

        //[ForeignKey("CompanyProfileId")]
        //public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
