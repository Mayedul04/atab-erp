﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    [Table("AccBalance")]
    public class AccBalance : Entity
    {
        public DateTime TranDate { get; set; }
        public string AccheadCode { get; set; }
        public int Balance { get; set; }

    }
}
