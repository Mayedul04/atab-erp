﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Infrastructure
{
    public enum AccountGroupEnum
    {
        None = 0,
        Asset = 1,
        Liability = 2,
        Equity = 3,
        Income = 4,
        Expenditure = 5
    }
    public enum AssetCategory { Building = 1, Vehicle = 2, Decoration = 3 }
    public enum ReferenceAccountType { Bank = 2, Supplier = 3, Customer = 4 }
    public enum BankAccountType { None = 0, Savings = 1, Current = 2, STD }
    public enum BankReconsilationType
    {
        [Display(Name = "Deposit in transit")]
        DepositInTransit = 1,
        [Display(Name = "Outstanding cheque")]
        OutstandingCheque = 2,
        [Display(Name = "Deposits not recorded")]
        DepositsNotRecorded = 3,
        [Display(Name = "Cheque/E Transfer not recorded")]
        ChequeOrETransferNotRecorded = 4
    }
    public enum BudgetStatus { Rejected = -1, Drafted = 0, Approved = 1}
    public enum ChequeStatus { UnUsed = 1, Issued = 2, Bounced = 3, Ruined = 4 }
    public enum AccTranType { Receive = 1, Payment = 2, Journal = 3, BankTran = 4, Adjustment = 5 }
    public enum AccountTypes
    {
        None = 1,
        Savings = 2,
        Current = 3,
        STD = 4
    }
    public enum CashFlowStatementActivities
    {
        [Display(Name = "Cash Flow from Operating Activities")]
        OperatingActivities = 1,
        [Display(Name = "Cash Flow from Investing Activities")]
        InvestingActivities = 2,
        [Display(Name = "Cash Flow from Financing Activities")]
        FinancingActivities = 3,
        [Display(Name = "Cash & Cash Equivalents At the End Period")]
        CashEquivalents = 4
    }
    public enum ReportExternalFunction
    {
        OpeningStock = 1,
        ClosingStock = 2,
        Income = 3
    }
    public enum RecordStatus
    {
        Rejected = -1,
        Drafted = 0,
        Submitted = 1,
        Verified = 2,
        Authorized = 3
    }
    public enum IuoSlipStatus
    {
        [Display(Name = "Paid")]
        Paid = 1,
        [Display(Name = "Adjusted")]
        Adjusted = 2,
        //[Display(Name = "Rejected")]
        //FinancingActivities = 3,
    }
    public enum IuoPurpose
    {
        [Display(Name = "Individual Advance")]
        Individual = 1,
        [Display(Name = "Individual Office Expense")]
        OfficeExpense = 2,
        [Display(Name = "Group Advance")]
        GroupAdvance = 3,
    }

    
}