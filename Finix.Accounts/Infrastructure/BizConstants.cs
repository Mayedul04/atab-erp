﻿namespace Finix.Accounts.Infrastructure
{
    public static class BizConstants
    {
        public const string CashInHand = "1020201001";
        public const string CashInHandATTI = "1020201002";

        //public const string CharityAccountHeadCode = "2040300002";
        public const string CashAccHeadSubGroup = "1020201";
        public const string BankAccHeadSubGroup = "1020202";
        //public const string PayableAccHeadGroup = "20403";
        public const string RetainedEarnings = "3030100001";
        public const string SalaryPayable = "5010300003";
        //public const string ReceivableAccHeadGroup = "10202";
        //public const string PurchaseAccHeadGroup = "10205";
        public const string AssetAccGroup = "1";
        public const string EquityAccGroup = "3";
        public const string IncomeAccGropu = "4";
        public const string LiabilityAccGroup = "2";
        public const string ExpendatureAccGroup = "5";

        //public const string FixedAssetDepreciationGroupCode = "501";
        //public const string AccumulatedDepreciation = "1030000001";
        //public const string ShareCapital = "305";
        //public const string Equity_Provision = "301";
        //public const string Equity_Provision_Asset = "30101";
        //public const string Equity_Provision_Liability = "30102";
        //public const string Reserve = "3000000003";
        //public const string AccountsReceivable = "10202";
        //public const long ReportConfigId_IncomeStatement = 1;

        public const long CompanyProfileId = 1;        
        public const long IncomeStatementReportId = 1;
        public const long BalanceSheetReportId = 2;
        public const long BalanceSheetReportId_Asset = 12;
        public const long BalanceSheetReportId_Equity = 13;
        public const long BalanceSheetReportId_Liability = 14;

        //public const string OpeningStock = "openingstock";
        //public const string CloingStock = "cloingstock";
    }
}
