﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Util
{
    public class ReadEnum
    {
        public AccountGroupEnum GetAccountGroupName(string accHead)
        {
            if (accHead.StartsWith("1"))
                return AccountGroupEnum.Asset;
            else if (accHead.StartsWith("2"))
                return AccountGroupEnum.Liability;
            else if (accHead.StartsWith("3"))
                return AccountGroupEnum.Equity;
            else if (accHead.StartsWith("4"))
                return AccountGroupEnum.Income;
            else if (accHead.StartsWith("5"))
                return AccountGroupEnum.Expenditure;

            return AccountGroupEnum.None;

        }
       
    }
}
