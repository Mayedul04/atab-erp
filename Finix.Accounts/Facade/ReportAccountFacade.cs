﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Service;
using Finix.Accounts.Util;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using Finix.Auth.Facade;


namespace Finix.Accounts.Facade
{
    public class ReportAccountFacade : BaseFacade
    {
        private readonly ReadEnum _readEnum = new ReadEnum();
        private readonly ConncetionFacade _connection = new ConncetionFacade();

        public IEnumerable<LedgerDto> GetLedgerReportByAccHead(DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        {
            var lstLedger = new List<LedgerDto>();
            var returnLedger = new List<LedgerDto>();
            var aLedger = new LedgerDto();

            try
            {
                if (accHeadCode.StartsWith("10109") || accHeadCode.StartsWith("10110"))
                {
                    var selectedAccountHead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == accHeadCode && x.CompanyProfileId == officeId);
                    fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
                    toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);

                    var selectedVoucherList = GenService.GetAll<Voucher>().Where(r => r.AccountHeadCode == accHeadCode && r.CompanyProfileId == officeId && r.VoucherDate >= fromDate && r.VoucherDate < toDate && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).ToList();
                    var rptVoucherlst = new List<Voucher>();
                    foreach (var aVoucher in selectedVoucherList)
                    {
                        var accHeadvoucher =
                            GenService.GetAll<Voucher>()
                                .Where(r => r.VoucherNo == aVoucher.VoucherNo && r.AccountHeadCode != aVoucher.AccountHeadCode && r.CompanyProfileId == officeId && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).ToList();//&& r.TransactionNo == aVoucher.TransactionNo
                        accHeadvoucher = accHeadvoucher.Where(r => r.AccountHeadCode != accHeadCode).ToList();
                        rptVoucherlst.AddRange(accHeadvoucher);
                    }
                    rptVoucherlst = rptVoucherlst.OrderBy(l => l.VoucherDate).ThenBy(l => l.Id).ToList();


                    var openingBalanceVouchers = GenService.GetAll<Voucher>()
                            .Where(q => q.AccountHeadCode == accHeadCode && q.CompanyProfileId == officeId && q.VoucherDate < fromDate && q.Status == EntityStatus.Active && q.RecordStatus == RecordStatus.Authorized);

                    var openingBalanceVoucher = openingBalanceVouchers.GroupBy(r => new { r.AccountHeadCode })
                        .Select(x => new
                        {
                            AccountHeadCode = x.Key.AccountHeadCode,
                            Debit = x.Sum(y => y.Debit),
                            Credit = x.Sum(y => y.Credit)
                        }).FirstOrDefault();
                    if (openingBalanceVoucher == null)
                    {
                        aLedger.Debit = 0;
                        aLedger.Credit = 0;
                    }
                    else
                    {
                        aLedger.Debit = openingBalanceVoucher.Debit;
                        aLedger.Credit = openingBalanceVoucher.Credit;
                    }
                    aLedger.VoucherDate = fromDate.AddDays(-1);
                    aLedger.Description = "Opening Balance";
                    aLedger.FromDate = fromDate;
                    aLedger.ToDate = toDate;
                    aLedger.SelectedAccountHeadCode = accHeadCode;
                    aLedger.SelectedAccountHeadName = selectedAccountHead.Name;

                    var accGroup = _readEnum.GetAccountGroupName(accHeadCode);

                    if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                    {
                        aLedger.Debit = 0;
                        aLedger.Credit = 0;
                        aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Debit - openingBalanceVoucher.Credit;
                    }

                    else
                    {
                        aLedger.Credit = 0;
                        aLedger.Debit = 0;
                        aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Credit - openingBalanceVoucher.Debit;
                    }
                    lstLedger.Add(aLedger);
                    var totalAmount = aLedger.Balance;
                    //bool isTranExist = false;
                    foreach (var aTran in rptVoucherlst)
                    {
                        var acchead = GenService.GetAll<AccHead>().FirstOrDefault(y => y.Code == aTran.AccountHeadCode && y.CompanyProfileId == officeId);
                        aLedger = new LedgerDto();
                        aLedger.Id = aTran.Id;
                        aLedger.VoucherNo = aTran.VoucherNo;
                        aLedger.VoucherDate = aTran.VoucherDate;
                        aLedger.SelectedAccountHeadCode = accHeadCode;
                        aLedger.SelectedAccountHeadName = selectedAccountHead.Name;
                        aLedger.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
                        aLedger.AccountHeadCode = aTran.AccountHeadCode;
                        aLedger.AccountHeadCodeName = acchead.Name;
                        aLedger.Description = aTran.Description;
                        aLedger.TransactionNo = aTran.TransactionNo;
                        aLedger.Debit = aTran.Credit;
                        aLedger.Credit = aTran.Debit;
                        aLedger.FromDate = fromDate;
                        aLedger.ToDate = toDate;



                        aLedger.IsDebit = aLedger.Debit > 0;
                        if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                            aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
                        else
                            aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
                        totalAmount = aLedger.Balance;
                        lstLedger.Add(aLedger);
                        //isTranExist = true;
                    }
                }
                else
                {
                    var selectedAccountHead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == accHeadCode && x.CompanyProfileId == officeId);
                    fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
                    toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);

                    var selectedVoucherList = GenService.GetAll<Voucher>().Where(r => r.AccountHeadCode == accHeadCode && r.CompanyProfileId == officeId && r.VoucherDate >= fromDate && r.VoucherDate < toDate && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).ToList();
                    var rptVoucherlst = new List<Voucher>();

                    rptVoucherlst = selectedVoucherList.OrderBy(l => l.VoucherDate).ThenBy(l => l.Id).ToList();

                    var openingBalanceVouchers = GenService.GetAll<Voucher>()
                            .Where(q => q.AccountHeadCode == accHeadCode && q.CompanyProfileId == officeId && q.VoucherDate < fromDate && q.Status == EntityStatus.Active && q.RecordStatus == RecordStatus.Authorized);

                    var openingBalanceVoucher = openingBalanceVouchers.GroupBy(r => new { r.AccountHeadCode })
                        .Select(x => new
                        {
                            AccountHeadCode = x.Key.AccountHeadCode,
                            Debit = x.Sum(y => y.Debit),
                            Credit = x.Sum(y => y.Credit)
                        }).FirstOrDefault();
                    if (openingBalanceVoucher == null)
                    {
                        aLedger.Debit = 0;
                        aLedger.Credit = 0;
                    }
                    else
                    {
                        aLedger.Debit = openingBalanceVoucher.Debit;
                        aLedger.Credit = openingBalanceVoucher.Credit;
                    }
                    aLedger.VoucherDate = fromDate.AddDays(-1);
                    aLedger.Description = "Opening Balance";
                    aLedger.FromDate = fromDate;
                    aLedger.ToDate = toDate;
                    aLedger.SelectedAccountHeadCode = accHeadCode;
                    aLedger.SelectedAccountHeadName = selectedAccountHead.Name;

                    var accGroup = _readEnum.GetAccountGroupName(accHeadCode);

                    if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                    {
                        aLedger.Debit = 0;
                        aLedger.Credit = 0;
                        aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Debit - openingBalanceVoucher.Credit;
                    }
                    else
                    {
                        aLedger.Credit = 0;
                        aLedger.Debit = 0;
                        aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Credit - openingBalanceVoucher.Debit;
                    }
                    lstLedger.Add(aLedger);
                    var totalAmount = aLedger.Balance;
                    //bool isTranExist = false;
                    foreach (var aTran in rptVoucherlst)
                    {
                        var acchead = GenService.GetAll<AccHead>().FirstOrDefault(y => y.Code == aTran.AccountHeadCode && y.CompanyProfileId == officeId);
                        aLedger = new LedgerDto();
                        aLedger.Id = aTran.Id;
                        aLedger.VoucherNo = aTran.VoucherNo;
                        aLedger.VoucherDate = aTran.VoucherDate;
                        aLedger.SelectedAccountHeadCode = accHeadCode;
                        aLedger.SelectedAccountHeadName = selectedAccountHead.Name;
                        aLedger.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
                        aLedger.AccountHeadCode = aTran.AccountHeadCode;
                        aLedger.AccountHeadCodeName = acchead.Name;
                        aLedger.Description = aTran.Description;
                        aLedger.TransactionNo = aTran.TransactionNo;
                        aLedger.Debit = aTran.Debit;
                        aLedger.Credit = aTran.Credit;
                        aLedger.FromDate = fromDate;
                        aLedger.ToDate = toDate;


                        aLedger.IsDebit = aLedger.Debit > 0;
                        if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                            aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
                        else
                            aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
                        totalAmount = aLedger.Balance;
                        lstLedger.Add(aLedger);
                    }
                }

                return lstLedger;
            }
            catch
            {
                return new List<LedgerDto>();
            }

        }
        //public IEnumerable<LedgerDto> GetLedgerReportByAccHead(DateTime fromDate, DateTime toDate, string accHeadCode, long officeId)
        //{
        //    try
        //    {
        //        var selectedAccountHead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == accHeadCode && x.CompanyProfileId == officeId);
        //        fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
        //        toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);

        //        //var selectedVoucherList = GenService.GetAll<Voucher>()
        //        //    .Where(r => r.AccountHeadCode == accHeadCode && r.VoucherNo != null && r.VoucherDate >= fromDate && r.VoucherDate < toDate)
        //        //    .Select(c => c.VoucherNo)
        //        //    .Distinct()
        //        //    .ToList();
        //        var selectedVoucherList = GenService.GetAll<Voucher>().Where(r => r.AccountHeadCode == accHeadCode && r.CompanyProfileId == officeId && r.VoucherDate >= fromDate && r.VoucherDate < toDate && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).ToList();
        //        var rptVoucherlst = new List<Voucher>();
        //        foreach (var aVoucher in selectedVoucherList)
        //        {
        //            var accHeadvoucher =
        //                GenService.GetAll<Voucher>()
        //                    .Where(r => r.VoucherNo == aVoucher.VoucherNo  && r.AccountHeadCode != aVoucher.AccountHeadCode && r.CompanyProfileId == officeId && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).ToList();//&& r.TransactionNo == aVoucher.TransactionNo
        //            //var tranVoucher =
        //            //    GenService.GetAll<Voucher>()
        //            //        .Where(
        //            //            t =>
        //            //                t.VoucherNo == voucherNo && t.TransactionNo == accHeadvoucher.TransactionNo &&
        //            //                t.AccountHeadCode != accHeadCode);
        //            accHeadvoucher = accHeadvoucher.Where(r => r.AccountHeadCode != accHeadCode).ToList();
        //            rptVoucherlst.AddRange(accHeadvoucher);
        //        }
        //        rptVoucherlst = rptVoucherlst.OrderBy(l => l.VoucherDate).ThenBy(l => l.Id).ToList();
        //        //rptVoucherlst = GenService.GetAll<Voucher>()
        //        //    .Where(r => selectedVoucherList.Contains(r.VoucherNo) && r.AccountHeadCode != accHeadCode && r.TransactionNo==selectedVoucherList.).ToList();

        //        var lstLedger = new List<LedgerDto>();
        //        var returnLedger = new List<LedgerDto>();
        //        var aLedger = new LedgerDto();

        //        //DateTime openingBalanceDate = fromDate.Date.AddDays(-1);

        //        var openingBalanceVouchers =
        //            GenService.GetAll<Voucher>()
        //                .Where(q => q.AccountHeadCode == accHeadCode && q.CompanyProfileId == officeId && q.VoucherDate < fromDate && q.Status == EntityStatus.Active && q.RecordStatus == RecordStatus.Authorized);

        //        var openingBalanceVoucher = openingBalanceVouchers.GroupBy(r => new { r.AccountHeadCode })
        //            .Select(x => new
        //            {
        //                AccountHeadCode = x.Key.AccountHeadCode,
        //                Debit = x.Sum(y => y.Debit),
        //                Credit = x.Sum(y => y.Credit)
        //            }).FirstOrDefault();
        //        if (openingBalanceVoucher == null)
        //        {
        //            aLedger.Debit = 0;
        //            aLedger.Credit = 0;
        //        }
        //        else
        //        {
        //            aLedger.Debit = openingBalanceVoucher.Debit;
        //            aLedger.Credit = openingBalanceVoucher.Credit;
        //        }
        //        aLedger.VoucherDate = fromDate.AddDays(-1);
        //        //aLedger.AccountHeadCode = accHeadCode;
        //        aLedger.Description = "Opening Balance";
        //        aLedger.FromDate = fromDate;
        //        aLedger.ToDate = toDate;
        //        aLedger.SelectedAccountHeadCode = accHeadCode;
        //        aLedger.SelectedAccountHeadName = selectedAccountHead.Name;

        //        var accGroup = _readEnum.GetAccountGroupName(accHeadCode);

        //        if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
        //        {
        //            aLedger.Debit = 0;
        //            aLedger.Credit = 0;
        //            aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Debit - openingBalanceVoucher.Credit;
        //        }

        //        else
        //        {
        //            aLedger.Credit = 0;
        //            aLedger.Debit = 0;
        //            aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Credit - openingBalanceVoucher.Debit;
        //        }
        //        lstLedger.Add(aLedger);
        //        var totalAmount = aLedger.Balance;
        //        //bool isTranExist = false;
        //        foreach (var aTran in rptVoucherlst)
        //        {
        //            var acchead = GenService.GetAll<AccHead>().FirstOrDefault(y => y.Code == aTran.AccountHeadCode && y.CompanyProfileId == officeId);
        //            aLedger = new LedgerDto();
        //            aLedger.Id = aTran.Id;
        //            aLedger.VoucherNo = aTran.VoucherNo;
        //            aLedger.VoucherDate = aTran.VoucherDate;
        //            aLedger.SelectedAccountHeadCode = accHeadCode;
        //            aLedger.SelectedAccountHeadName = selectedAccountHead.Name;
        //            aLedger.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
        //            aLedger.AccountHeadCode = aTran.AccountHeadCode;
        //            aLedger.AccountHeadCodeName = acchead.Name;
        //            aLedger.Description = aTran.Description;
        //            aLedger.TransactionNo = aTran.TransactionNo;
        //            aLedger.Debit = aTran.Credit;
        //            aLedger.Credit = aTran.Debit;
        //            aLedger.FromDate = fromDate;
        //            aLedger.ToDate = toDate;


        //            aLedger.IsDebit = aLedger.Debit > 0;
        //            if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
        //                aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
        //            else
        //                aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
        //            totalAmount = aLedger.Balance;
        //            lstLedger.Add(aLedger);
        //            //isTranExist = true;
        //        }
        //        return lstLedger;
        //    }
        //    catch
        //    {
        //        return new List<LedgerDto>();
        //    }

        //}
        public IEnumerable<GroupLedgerDto> GetGroupLedgerReport(DateTime fromDate, DateTime toDate, string code, long? companyProfileId)
        {
            //if (companyProfileId == null)
            //    companyProfileId = 1;
            var listLedger = new List<GroupLedgerDto>();

            try
            {

                fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
                toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);


                SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
                string query = "SELECT J1.LedgerAccHeadCode, A1.Name AS LedgerAccHeadName, J1.VoucherNo, J1.VoucherDate, J1.AccountHeadCode," +
                               "A2.Name AS AccountHeadName, J1.Debit, J1.Credit, G1.Name AS AccGroup, G2.Name AS AccSubGroup, G3.Name AS AccHeadGroup,G4.Name AS AccHeadSubGroup " +
                               "FROM (SELECT V2.VoucherNo, V2.VoucherDate, V2.AccountHeadCode, V2.Debit, V2.Credit, V1.AccountHeadCode as LedgerAccHeadCode FROM Voucher V2 " +
                               "JOIN (SELECT VoucherNo, TransactionNo, AccountHeadCode FROM Voucher V1 " +
                               "WHERE AccountHeadCode LIKE '" + code + "%' ";
                if (companyProfileId != null && companyProfileId > 0)
                    query += "AND CompanyProfileId = " + companyProfileId;

                query += " AND VoucherDate >= '" + fromDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' AND VoucherDate < '" + toDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "') V1 " +
                         "ON V2.VoucherNo = V1.VoucherNo AND V2.TransactionNo = V1.TransactionNo WHERE V2.AccountHeadCode <> V1.AccountHeadCode) J1 LEFT JOIN AccHead A1 ON J1.LedgerAccHeadCode = A1.Code " +
                         "LEFT JOIN AccHead A2 ON J1.AccountHeadCode = A2.Code " +
                         "LEFT OUTER JOIN AccGroup     G1 ON SUBSTRING(J1.LedgerAccHeadCode,1,1) = G1.Code " +
                         "LEFT OUTER JOIN AccSubGroup  G2 ON SUBSTRING(J1.LedgerAccHeadCode,1,3) = G2.Code " +
                         "LEFT OUTER JOIN AccHeadGroup G3 ON SUBSTRING(J1.LedgerAccHeadCode,1,5) = G3.Code " +
                         "LEFT OUTER JOIN AccHeadGroup G4 ON SUBSTRING(J1.LedgerAccHeadCode,1,7) = G4.Code " +
                         "ORDER BY J1.LedgerAccHeadCode, J1.VoucherDate DESC";

                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable dt = new DataTable();
                conn.Open();
                da.Fill(dt);
                conn.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    GroupLedgerDto ledger = new GroupLedgerDto();
                    ledger.AccGroup = dr["AccGroup"].ToString();
                    ledger.AccHeadGroup = dr["AccHeadGroup"].ToString();
                    ledger.AccHeadSubGroup = dr["AccHeadSubGroup"].ToString();
                    ledger.AccountHeadCode = dr["AccountHeadCode"].ToString();
                    ledger.AccountHeadName = dr["AccountHeadName"].ToString();
                    ledger.AccSubGroup = dr["AccSubGroup"].ToString();
                    ledger.Credit = Convert.ToDecimal(dr["Credit"]);
                    ledger.Debit = Convert.ToDecimal(dr["Debit"]);
                    ledger.LedgerAccHeadCode = dr["LedgerAccHeadCode"].ToString();
                    ledger.LedgerAccHeadName = dr["LedgerAccHeadName"].ToString();
                    ledger.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    ledger.VoucherNo = dr["VoucherNo"].ToString();

                    listLedger.Add(ledger);
                }

            }
            catch { }

            return listLedger;

        }
        public IEnumerable<LedgerDto> GetBankBookReport(DateTime fromDate, DateTime toDate, string accountHeadCode, long officeId)
        {
            try
            {
                string bankHeadCode = accountHeadCode;

                fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
                toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);

                //var accTrantype = new List<AccTranType> { AccTranType.Receive, AccTranType.Payment, AccTranType.Adjustment };

                var cashVoucherList = GenService.GetAll<Voucher>()
                    .Where(r => r.AccountHeadCode == bankHeadCode && r.VoucherNo != null && r.CompanyProfileId == officeId && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized)
                    .Select(c => c.VoucherNo)
                    .Distinct()
                    .ToList();
                var rptVoucherlst = GenService.GetAll<Voucher>()
                    .Where(r => cashVoucherList.Contains(r.VoucherNo) && r.AccountHeadCode != bankHeadCode && r.CompanyProfileId == officeId && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).OrderBy(l => l.VoucherDate).ThenBy(l => l.Id).ToList();

                var lstLedger = new List<LedgerDto>();
                var aLedger = new LedgerDto();
                DateTime openingBalanceDate = fromDate.Date.AddDays(-1);

                var openingBalanceVouchers =
                    GenService.GetAll<Voucher>()
                        .Where(q => q.AccountHeadCode == bankHeadCode && q.VoucherDate < fromDate && q.CompanyProfileId == officeId && q.Status == EntityStatus.Active && q.RecordStatus == RecordStatus.Authorized);

                var openingBalanceVoucher = openingBalanceVouchers.GroupBy(r => new { r.AccountHeadCode })
                    .Select(x => new
                    {
                        AccountHeadCode = x.Key.AccountHeadCode,
                        Debit = x.Sum(y => y.Debit),
                        Credit = x.Sum(y => y.Credit)
                    }).FirstOrDefault();
                if (openingBalanceVoucher == null)
                {
                    aLedger.Debit = 0;
                    aLedger.Credit = 0;
                }
                else
                {
                    aLedger.Debit = openingBalanceVoucher.Debit;
                    aLedger.Credit = openingBalanceVoucher.Credit;
                }

                aLedger.VoucherDate = openingBalanceDate;// openingBalance.TranDate >= DateTime.MaxValue ? toDate : openingBalance.TranDate;
                //aLedger.AccountHeadCode = accHeadCode;
                aLedger.Description = "Opening Balance";


                var accGroup = _readEnum.GetAccountGroupName(bankHeadCode);

                if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                {
                    aLedger.Debit = 0;
                    aLedger.Credit = 0;
                    aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Debit - openingBalanceVoucher.Credit;
                }

                else
                {
                    aLedger.Credit = 0;
                    aLedger.Debit = 0;
                    aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Credit - openingBalanceVoucher.Debit;
                }
                lstLedger.Add(aLedger);
                var totalAmount = aLedger.Balance;
                //bool isTranExist = false;
                foreach (var aTran in rptVoucherlst)
                {
                    var acchead = GenService.GetAll<AccHead>().FirstOrDefault(y => y.Code == aTran.AccountHeadCode && y.CompanyProfileId == officeId);
                    aLedger = new LedgerDto();
                    aLedger.Id = aTran.Id;
                    aLedger.ChequeNo = aTran.ChequeNo;
                    aLedger.VoucherNo = aTran.VoucherNo;
                    aLedger.VoucherDate = aTran.VoucherDate;
                    aLedger.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
                    aLedger.AccountHeadCode = aTran.AccountHeadCode;
                    aLedger.AccountHeadCodeName = acchead.Name;
                    aLedger.Description = aTran.Description;
                    aLedger.TransactionNo = aTran.TransactionNo;
                    aLedger.Debit = aTran.Credit;
                    aLedger.Credit = aTran.Debit;

                    aLedger.IsDebit = aLedger.Debit > 0;
                    if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                        aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
                    else
                        aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
                    totalAmount = aLedger.Balance;
                    lstLedger.Add(aLedger);
                }
                return lstLedger;
            }
            catch
            {
                return new List<LedgerDto>();
            }
        }
        public IEnumerable<LedgerDto> GetCashBookReport(DateTime fromDate, DateTime toDate, long CompanyProfileId)
        { 
            try
            {
                fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
                toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);

                //var accTrantype = new List<AccTranType> { AccTranType.Receive, AccTranType.Payment, AccTranType.Adjustment };

                var cashVoucherList = GenService.GetAll<Voucher>()
                    .Where(r => r.AccountHeadCode == BizConstants.CashInHand
                        && r.CompanyProfileId == CompanyProfileId
                        && r.VoucherNo != null
                        && r.VoucherDate >= fromDate
                        && r.Status == EntityStatus.Active
                        && r.RecordStatus == RecordStatus.Authorized
                        && r.VoucherDate < toDate)
                    .Select(c => c.VoucherNo)
                    .Distinct();
                //.ToList();
                var rptVoucherlst = GenService.GetAll<Voucher>()
                    .Where(r => cashVoucherList.Contains(r.VoucherNo) && r.AccountHeadCode != BizConstants.CashInHand && r.CompanyProfileId == CompanyProfileId && r.Status == EntityStatus.Active && r.RecordStatus == RecordStatus.Authorized).OrderBy(l => l.VoucherDate).ThenBy(l => l.Id).ToList();

                var lstLedger = new List<LedgerDto>();
                var aLedger = new LedgerDto();

                DateTime openingBalanceDate = fromDate.Date.AddDays(-1);

                var openingBalanceVouchers =
                    GenService.GetAll<Voucher>()
                        .Where(q => q.AccountHeadCode == BizConstants.CashInHand && q.VoucherDate < fromDate && q.CompanyProfileId == CompanyProfileId && q.Status == EntityStatus.Active && q.RecordStatus == RecordStatus.Authorized);//openingBalanceDate

                var openingBalanceVoucher = openingBalanceVouchers.GroupBy(r => new { r.AccountHeadCode })
                    .Select(x => new
                    {
                        AccountHeadCode = x.Key.AccountHeadCode,
                        Debit = x.Sum(y => y.Debit),
                        Credit = x.Sum(y => y.Credit)
                    }).FirstOrDefault();
                if (openingBalanceVoucher == null)
                {
                    aLedger.Debit = 0;
                    aLedger.Credit = 0;
                }
                else
                {
                    aLedger.Debit = openingBalanceVoucher.Debit;
                    aLedger.Credit = openingBalanceVoucher.Credit;
                }
                aLedger.VoucherDate = openingBalanceDate;
                //aLedger.AccountHeadCode = accHeadCode;
                aLedger.Description = "Opening Balance";

                aLedger.VoucherDate = openingBalanceDate;
                //aLedger.AccountHeadCode = accHeadCode;
                aLedger.AccountHeadCodeName = "Opening Balance";


                var accGroup = _readEnum.GetAccountGroupName(BizConstants.CashInHand);

                if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                {
                    aLedger.Debit = 0;
                    aLedger.Credit = 0;
                    aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Debit - openingBalanceVoucher.Credit;
                }

                else
                {
                    aLedger.Credit = 0;
                    aLedger.Debit = 0;
                    aLedger.Balance = openingBalanceVoucher == null ? 0 : openingBalanceVoucher.Credit - openingBalanceVoucher.Debit;
                }
                lstLedger.Add(aLedger);
                var totalAmount = aLedger.Balance;
                bool isTranExist = false;

                #region Optimization
                //var acchead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == aTran.AccountHeadCode);
                var accHeads = GenService.GetAll<AccHead>().Where(a => a.CompanyProfileId == CompanyProfileId).OrderBy(r => r.Code);
                var ledgerlist = (from ldgr in rptVoucherlst
                                  join aheads in accHeads
                                  on ldgr.AccountHeadCode equals aheads.Code into joined
                                  from j in joined.DefaultIfEmpty()
                                      //orderby aheads.Name descending
                                  select new LedgerDto()
                                  {
                                      Id = ldgr.Id,
                                      VoucherNo = ldgr.VoucherNo,
                                      VoucherDate = ldgr.VoucherDate,
                                      AccountHeadCode = ldgr.AccountHeadCode,
                                      AccountHeadCodeName = j != null ? j.Name : "",
                                      Description = ldgr.Description,
                                      TransactionNo = ldgr.TransactionNo,
                                      Debit = ldgr.Credit,
                                      Credit = ldgr.Debit,
                                      IsDebit = ldgr.Debit > 0,
                                      CompanyProfileId = j.CompanyProfileId
                                  }).ToList();

                foreach (var aTran in ledgerlist)
                {
                    aTran.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
                    aTran.IsDebit = aTran.Debit > 0;
                    if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                        aTran.Balance = aTran.IsDebit ? totalAmount + aTran.Debit : totalAmount - aTran.Credit;
                    else
                        aTran.Balance = aTran.IsDebit ? totalAmount - aTran.Debit : totalAmount + aTran.Credit;
                    totalAmount = aTran.Balance;
                    lstLedger.Add(aTran);
                }
                #region old code
                //foreach (var aTran in rptVoucherlst)
                //{
                //    aLedger = new LedgerDto();

                //    aLedger.AccountGroupName = _readEnum.GetAccountGroupName(aTran.AccountHeadCode).ToString();
                //    var acchead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == aTran.AccountHeadCode);
                //    aLedger.Id = aTran.Id;
                //    aLedger.VoucherNo = aTran.VoucherNo;
                //    aLedger.VoucherDate = aTran.VoucherDate;
                //    aLedger.AccountHeadCode = aTran.AccountHeadCode;
                //    aLedger.AccountHeadCodeName = acchead.Name;
                //    aLedger.Description = aTran.Description;
                //    aLedger.TransactionNo = aTran.TransactionNo;
                //    aLedger.Debit = aTran.Credit;
                //    aLedger.Credit = aTran.Debit;


                //    aLedger.IsDebit = aLedger.Debit > 0;
                //    if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                //        aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
                //    else
                //        aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
                //    totalAmount = aLedger.Balance;
                //    lstLedger.Add(aLedger);

                //aLedger.IsDebit = aLedger.Debit > 0;
                //if (accGroup == AccountGroupEnum.Asset || accGroup == AccountGroupEnum.Expenditure)
                //    aLedger.Balance = aLedger.IsDebit ? totalAmount - aLedger.Debit : totalAmount + aLedger.Credit;
                //else
                //    aLedger.Balance = aLedger.IsDebit ? totalAmount + aLedger.Debit : totalAmount - aLedger.Credit;
                //totalAmount = aLedger.Balance;
                //lstLedger.Add(aLedger);
                // isTranExist = true;
                //}
                #endregion
                #endregion
                return lstLedger;

            }
            catch (Exception ex)
            {
                return new List<LedgerDto>();
            }

        }
        public IEnumerable<TrialBalanceDto> GetTrailBalanceReport(DateTime toDate, long companyProfileId)
        {
            //toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.Date.AddDays(1);
            IQueryable<VoucherArchieve> rptTrailBalanceData = GenService.GetAll<VoucherArchieve>().Where(t => t.ArchieveDate == toDate && t.CompanyProfileId == companyProfileId);

            var trialBalanceData = (from data in rptTrailBalanceData
                                    select new TrialBalanceDto
                                    {
                                        AccountHeadCode = data.AccountHeadCode,
                                        AccountHeadCodeName = "",
                                        Balance = 0,
                                        Debit = (data.AccountHeadCode.Substring(0, 1) == AccountGroupEnum.Asset.ToString()
                                            || data.AccountHeadCode.Substring(0, 1) == AccountGroupEnum.Expenditure.ToString())
                                            && (data.Debit - data.Credit) > 0 ? (data.Debit - data.Credit) :
                                            (data.AccountHeadCode.Substring(0, 1) != AccountGroupEnum.Asset.ToString()
                                            && data.AccountHeadCode.Substring(0, 1) != AccountGroupEnum.Expenditure.ToString())
                                            && (data.Debit - data.Credit) > 0 ? (data.Debit - data.Credit) : 0,
                                        Credit = (data.AccountHeadCode.Substring(0, 1) == AccountGroupEnum.Asset.ToString()
                                            || data.AccountHeadCode.Substring(0, 1) == AccountGroupEnum.Expenditure.ToString())
                                            && (data.Credit - data.Debit) > 0 ? (data.Credit - data.Debit) :
                                            (data.AccountHeadCode.Substring(0, 1) != AccountGroupEnum.Asset.ToString()
                                            && data.AccountHeadCode.Substring(0, 1) != AccountGroupEnum.Expenditure.ToString())
                                            && (data.Credit - data.Debit) > 0 ? (data.Credit - data.Debit) : 0,
                                        ToDate = toDate,
                                        AccountGroupName = "",
                                        AccountSubGroupName = "",
                                        AccountHeadGroupName = "",
                                        AccountHeadSubGroupName = ""
                                    });

            var accHeads = GenService.GetAll<AccHead>().Where(a => a.CompanyProfileId == companyProfileId).OrderBy(r => r.Code);


            var rptTrialBalanceDetailList = from aheads in accHeads
                                            join tbdetails in trialBalanceData
                          on aheads.Code equals tbdetails.AccountHeadCode into joined
                                            from j in joined.DefaultIfEmpty()
                                            join accGroup in GenService.GetAll<AccGroup>() on aheads.Code.Substring(0, 1) equals accGroup.Code into accGrouped
                                            from ag in accGrouped.DefaultIfEmpty()
                                            join accSubGroup in GenService.GetAll<AccSubGroup>() on aheads.Code.Substring(0, 3) equals accSubGroup.Code into accSubGrouped
                                            from asg in accSubGrouped.DefaultIfEmpty()
                                            join accHeadGroup in GenService.GetAll<AccHeadGroup>() on aheads.Code.Substring(0, 5) equals accHeadGroup.Code into accHeadGrouped
                                            from ahg in accHeadGrouped.DefaultIfEmpty()
                                            join accHeadSubGroup in GenService.GetAll<AccHeadSubGroup>() on aheads.Code.Substring(0, 7) equals accHeadSubGroup.Code into accHeadSubGrouped
                                            from ahsg in accHeadSubGrouped.DefaultIfEmpty()
                                            select new TrialBalanceDto()
                                            {
                                                AccountHeadCode = aheads.Code,
                                                AccountHeadCodeName = aheads.Name,
                                                Balance = 0,
                                                Debit = j != null ? j.Debit : 0,
                                                Credit = j != null ? j.Credit : 0,
                                                ToDate = toDate,
                                                AccountGroupName = ag != null ? ag.Name : "",
                                                AccountSubGroupName = asg != null ? asg.Name : "",
                                                AccountHeadGroupName = ahg != null ? ahg.Name : "",
                                                AccountHeadSubGroupName = ahsg != null ? ahsg.Name : ""
                                            };

            return rptTrialBalanceDetailList.OrderBy(r => r.AccountGroupName).ToList();
        }

        #region inventory calculation
        //public List<ProductionCostCalculationDto> GetInventoryAmount(DateTime receiveDate, DateTime toDate)
        //{
        //    var ProducedMasterCartons = (from stocktran in GenService.GetAll<StockTran>().Where(s => s.StockTranType == StockTranType.Produced && s.TranDate <= toDate)
        //                                 from stockTranDetails in stocktran.StockTranDetails
        //                                 select stockTranDetails).OrderBy(s => s.StockTran.TranDate).ToList();

        //    if (ProducedMasterCartons == null || !ProducedMasterCartons.Any())
        //        return null;

        //    var SoldMasterCartons = GenService.GetAll<StockTran>().Where(s => s.StockTranType == StockTranType.Sold && s.TranDate <= toDate).ToList();

        //    List<StockTranDetail> SoldMasterCartonsGrouped = null;
        //    if(SoldMasterCartons.Any())
        //        SoldMasterCartonsGrouped = (from stockTran in SoldMasterCartons
        //                                    from stockTranDetails in stockTran.StockTranDetails
        //                                    where GenService.GetAll<StockTran>().Where(s => s.StockTranType == StockTranType.Sold && s.TranDate <= toDate).Any()
        //                                    group stockTranDetails by new
        //                                    {
        //                                        stockTranDetails.ProductId,
        //                                        stockTranDetails.Product,
        //                                        stockTranDetails.SizeId,
        //                                        stockTranDetails.Size,
        //                                        stockTranDetails.ProductTypeId,
        //                                        stockTranDetails.ProductType,
        //                                        stockTranDetails.FreezingType,
        //                                        stockTranDetails.NetWeight,
        //                                        stockTranDetails.GlacePercentage,
        //                                        stockTranDetails.CartonSizeId,
        //                                        stockTranDetails.CartonSize
        //                                    } into grouped
        //                                    //from g in grouped.DefaultIfEmpty()
        //                                    select new StockTranDetail
        //                                    {
        //                                        CartonSize = grouped.Key.CartonSize,
        //                                        CartonSizeId = grouped.Key.CartonSizeId,
        //                                        FreezingType = grouped.Key.FreezingType,
        //                                        GlacePercentage = grouped.Key.GlacePercentage,
        //                                        NetWeight = grouped.Key.NetWeight,
        //                                        Product = grouped.Key.Product,
        //                                        ProductId = grouped.Key.ProductId,
        //                                        ProductType = grouped.Key.ProductType,
        //                                        ProductTypeId = grouped.Key.ProductTypeId,
        //                                        Quantity = grouped.Sum(s => s.Quantity),
        //                                        Size = grouped.Key.Size,
        //                                        SizeId = grouped.Key.SizeId
        //                                    }).ToList();

        //    //var reducedStockTranDetail = new List<StockTranDetail>();

        //    if (SoldMasterCartonsGrouped != null)
        //        foreach (var item in ProducedMasterCartons)
        //        {
        //            var soldItem = SoldMasterCartonsGrouped.Where(s => s.ProductId == item.ProductId && s.SizeId == item.SizeId && s.NetWeight == item.NetWeight && s.GlacePercentage == item.GlacePercentage && s.ProductTypeId == item.ProductTypeId && s.FreezingType == item.FreezingType && s.Quantity > 0).FirstOrDefault();
        //            if(soldItem == null)
        //                continue;

        //            //SoldMasterCartonsGrouped.Remove(soldItem);

        //            if (soldItem.Quantity >= item.Quantity)
        //                soldItem.Quantity -= item.Quantity;
        //            else
        //                item.Quantity -= soldItem.Quantity;

        //            //SoldMasterCartonsGrouped.Add(soldItem);
        //        }

        //    var reducedStockTranDetail = ProducedMasterCartons.Where(d => d.Quantity > 0).ToList();

        //    var priceList = new List<ProductionCostCalculationDto>();

        //    foreach (var item in reducedStockTranDetail)
        //    {
        //        var ProdTranInstanceList = GenService.GetAll<ProductionTran>().Where(pt => 
        //            pt.ProductId == item.ProductId && 
        //            pt.SizeId == item.SizeId && 
        //            pt.NetWeight == item.NetWeight && 
        //            pt.GlacePercentage == item.GlacePercentage && 
        //            pt.ProductTypeId == item.ProductTypeId && 
        //            pt.FreezingType == item.FreezingType).OrderByDescending(pt => pt.ProductionDate).OrderByDescending(pt => pt.Id).ToList();

        //        foreach (var each in ProdTranInstanceList)
        //        {
        //            if (item.Quantity <= 0)
        //                continue;

        //            var eachReceiveDate = GenService.GetById<PreProductionDetail>(each.PreProductionDetailId).PreProduction.ReceiveDate;
        //            var productionPrice = GenService.GetAll<DailyFishProductionCost>()
        //                .Where(c =>
        //                    c.ProductId == each.ProductId &&
        //                    c.SizeId == each.SizeId &&
        //                    c.NetWeight == each.NetWeight &&
        //                    c.GlacePercentage == each.GlacePercentage &&
        //                    c.ProductTypeId == each.ProductTypeId &&
        //                    c.ReceiveDate == eachReceiveDate).FirstOrDefault();


        //            if (productionPrice != null)
        //            {
        //                var calculatedPrice = new ProductionCostCalculationDto();

        //                calculatedPrice.ProductId = (long)each.ProductId;
        //                calculatedPrice.SizeId = each.SizeId;
        //                calculatedPrice.NetWeight = each.NetWeight;
        //                calculatedPrice.GlacePercentage = each.GlacePercentage;
        //                calculatedPrice.ProductTypeId = each.ProductTypeId;
        //                calculatedPrice.FreezingType = each.FreezingType;

        //                var reducedCartons = item.Quantity;
        //                if((item.Quantity * item.CartonSize.BlockPerCarton) >= each.Quantity)
        //                {
        //                    calculatedPrice.BlockCount = each.Quantity;
        //                    item.Quantity -= (Int64)(each.Quantity / item.CartonSize.BlockPerCarton);
        //                }
        //                else
        //                {
        //                    calculatedPrice.BlockCount = item.Quantity * item.CartonSize.BlockPerCarton;
        //                    item.Quantity = 0;
        //                }
        //                reducedCartons -= item.Quantity;
        //                calculatedPrice.RatePerBlock = productionPrice.RatePerBlock;

        //                calculatedPrice.Price = (decimal)calculatedPrice.RatePerBlock * calculatedPrice.BlockCount //block price
        //                    + reducedCartons * each.MasterCartonConfig.TotalPrice; //other costs per carton

        //                priceList.Add(calculatedPrice);
        //            }
        //            //calculatedPrice.BlockCount = 
        //        }
        //    }

        //    return priceList;

        //}


        //public FinancialStatementNotesDto GetInventoryAmountHead(DateTime ToDate, string Name, string AccountHeadCode)
        //{
        //    FinancialStatementNotesDto note = new FinancialStatementNotesDto();

        //    note.Note = 17;
        //    note.AccountHeadCode = AccountHeadCode;
        //    note.Name = Name;
        //    note.Year = ToDate.Date.AddDays(-1).ToString();
        //    ToDate = ToDate.AddDays(1).AddMilliseconds(-3);

        //    var inventoryDetails = GetInventoryAmount(DateTime.MinValue, ToDate);

        //    note.Expense = 0;
        //    if(inventoryDetails != null)
        //        note.Expense = inventoryDetails.Sum(m=>m.Price);
        //    return note;
        //}        

        #endregion
        
        public List<CashFlowStatementDto> GetCashFlow(DateTime FromDate, DateTime ToDate, List<long?> CompanyProfileIds)
        {

            FromDate = FromDate.Date;
            ToDate = ToDate.Date;
            var resultList = new List<CashFlowStatementDto>();
            var voucherList = new List<VoucherDto>();

            #region Operating Activity

            var operatingActivityCodes =
                GenService.GetAll<AccHeadGroup>()
                    .Where(r => r.CashFlowStatementActivity == CashFlowStatementActivities.OperatingActivities);
            //.Select(r => r.Code)
            //.ToList();
            var oaCodes = operatingActivityCodes.Select(s => s.Code).ToList();
            var latest = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == ToDate &&
                                                                        va.Status == EntityStatus.Active &&
                                                                        oaCodes.Contains(va.AccountHeadCode.Substring(0, 5)) &&
                                                                        CompanyProfileIds.Contains(va.CompanyProfileId));
            var prev = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == FromDate &&
                                                                      va.Status == EntityStatus.Active &&
                                                                      oaCodes.Contains(va.AccountHeadCode.Substring(0, 5)) &&
                                                                      CompanyProfileIds.Contains(va.CompanyProfileId));
            var accheadForOa = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate >= FromDate && va.ArchieveDate <= ToDate);
            if (latest.Any() && prev.Any())
            {
                var test =
              (from vatdr in latest
               join vafdr in prev on new { vatdr.AccountHeadCode, vatdr.CompanyProfileId } equals new { vafdr.AccountHeadCode, vafdr.CompanyProfileId } into operatingActivityVouchers
               from fromVoucher in operatingActivityVouchers.DefaultIfEmpty()
               join accHd in GenService.GetAll<AccHead>() on fromVoucher.AccountHeadCode equals accHd.Code
               join headGrp in GenService.GetAll<AccHeadGroup>() on accHd.AccHeadGroupId equals headGrp.Id
               select new VoucherArchieveDto()
               {
                   AccGroupId = accHd.AccGroupId,
                   AccHeadGroupId = accHd.AccHeadGroupId,
                   AccHeadGroupName = headGrp.Code,
                   AccountHeadCode = vatdr.AccountHeadCode,
                   Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                   Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                   CompanyProfileId = vatdr.CompanyProfileId,
                   Amount = 0
               }).ToList();//va.AccountHeadCode.StartsWith()

                var secData = (from dt in test
                               group dt by new { dt.AccountHeadCode } into grp
                               let voucherArchieveDto = grp.FirstOrDefault()
                               where voucherArchieveDto != null
                               select new VoucherArchieveDto()
                               {
                                   AccGroupId = voucherArchieveDto.AccGroupId,
                                   AccHeadGroupId = voucherArchieveDto.AccHeadGroupId,
                                   AccHeadGroupName = voucherArchieveDto.AccHeadGroupName,
                                   AccountHeadCode = grp.Key.AccountHeadCode,
                                   Credit = grp.Sum(l => l.Credit),
                                   Debit = grp.Sum(l => l.Debit),
                                   CompanyProfileId = voucherArchieveDto.CompanyProfileId,
                                   Amount = (grp.FirstOrDefault().AccGroupId == 1 || grp.FirstOrDefault().AccGroupId == 5) ?
                                                       (grp.Sum(l => l.Debit) - grp.Sum(l => l.Credit)) :
                                                       (grp.Sum(l => l.Credit) - grp.Sum(l => l.Debit)),

                               }).ToList();

                resultList.AddRange((from dt in secData
                                     group dt by new { dt.AccHeadGroupId } into grp
                                     select new CashFlowStatementDto()
                                     {
                                         Activity = CashFlowStatementActivities.OperatingActivities,
                                         ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                         AccountHead = grp.FirstOrDefault().AccountHeadCode.Substring(0, 5),
                                         Amount = grp.Sum(l => l.Amount),
                                         Number = grp.Count()
                                     }).ToList());
            }


            #endregion
            #region Investing Activities
            var Investing =
                GenService.GetAll<AccHeadGroup>()
                    .Where(r => r.CashFlowStatementActivity == CashFlowStatementActivities.InvestingActivities);
            var invCodes = Investing.Select(s => s.Code).ToList();
            var latestInv = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == ToDate &&
                                                                           va.Status == EntityStatus.Active &&
                                                                           invCodes.Contains(va.AccountHeadCode.Substring(0, 5)) && CompanyProfileIds.Contains(va.CompanyProfileId));
            var prevInv = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == FromDate &&
                                                                      va.Status == EntityStatus.Active &&
                                                                      invCodes.Contains(va.AccountHeadCode.Substring(0, 5)) &&
                                                                      CompanyProfileIds.Contains(va.CompanyProfileId));
            if (latestInv.Any() && prevInv.Any())
            {
                var testInvt =
                    from vatdr in latestInv
                    join vafdr in prevInv
                        on new { vatdr.AccountHeadCode, vatdr.CompanyProfileId } equals
                        new { vafdr.AccountHeadCode, vafdr.CompanyProfileId } into operatingActivityVouchers
                    from fromVoucher in operatingActivityVouchers.DefaultIfEmpty()
                    join accHd in GenService.GetAll<AccHead>() on fromVoucher.AccountHeadCode equals accHd.Code
                    join headGrp in GenService.GetAll<AccHeadGroup>() on accHd.AccHeadGroupId equals headGrp.Id
                    select new VoucherArchieveDto()
                    {
                        AccGroupId = accHd.AccGroupId,
                        AccHeadGroupId = accHd.AccHeadGroupId,
                        AccHeadGroupName = headGrp.Code,
                        AccountHeadCode = vatdr.AccountHeadCode,
                        Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                        Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                        CompanyProfileId = vatdr.CompanyProfileId,
                        Amount = 0
                    }; //va.AccountHeadCode.StartsWith()

                var secDataInvt = (from dt in testInvt
                                   group dt by new { dt.AccountHeadCode }
                    into grp
                                   select new VoucherArchieveDto()
                                   {
                                       AccGroupId = grp.FirstOrDefault().AccGroupId,
                                       AccHeadGroupId = grp.FirstOrDefault().AccHeadGroupId,
                                       AccHeadGroupName = grp.FirstOrDefault().AccHeadGroupName,
                                       AccountHeadCode = grp.Key.AccountHeadCode,
                                       Credit = grp.Sum(l => l.Credit),
                                       Debit = grp.Sum(l => l.Debit),
                                       CompanyProfileId = grp.FirstOrDefault().CompanyProfileId,
                                       //Activity = CashFlowStatementActivities.OperatingActivities,
                                       //ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                       //Debit = grp.Sum(l=>l.Debit),
                                       //Credit = vrch.Credit,
                                       Amount =
                                           (grp.FirstOrDefault().AccGroupId == 1 || grp.FirstOrDefault().AccGroupId == 5)
                                               ? (grp.Sum(l => l.Debit) - grp.Sum(l => l.Credit))
                                               : (grp.Sum(l => l.Credit) - grp.Sum(l => l.Debit))
                                   }).ToList();

                resultList.AddRange((from dt in secDataInvt
                                     group dt by new { dt.AccHeadGroupId }
                    into grp
                                     select new CashFlowStatementDto()
                                     {
                                         //AccHeadGroupId = grp.Key.AccHeadGroupId,
                                         //AccHeadGroupName = grp.FirstOrDefault().AccHeadGroupName,
                                         //AccountHeadCode = grp.FirstOrDefault().,
                                         //Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                                         //Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                                         //CompanyProfileId = vatdr.CompanyProfileId
                                         Activity = CashFlowStatementActivities.InvestingActivities,
                                         ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                         AccountHead = grp.FirstOrDefault().AccountHeadCode.Substring(0, 5),
                                         Amount = grp.Sum(l => l.Amount),
                                         Number = grp.Count()
                                     }).ToList());
            }

            #endregion
            #region Financing Activities
            var finance =
               GenService.GetAll<AccHeadGroup>()
                   .Where(r => r.CashFlowStatementActivity == CashFlowStatementActivities.FinancingActivities);
            var finCodes = finance.Select(s => s.Code).ToList();
            var latestFin = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == ToDate &&
                                                                              va.Status == EntityStatus.Active &&
                                                                              finCodes.Contains(va.AccountHeadCode.Substring(0, 5)) &&
                                                                              CompanyProfileIds.Contains(va.CompanyProfileId));
            var prevFin = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == FromDate &&
                                                                             va.Status == EntityStatus.Active &&
                                                                             finCodes.Contains(va.AccountHeadCode.Substring(0, 5)) &&
                                                                             CompanyProfileIds.Contains(va.CompanyProfileId));
            //var finCodes = finance.Select(s => s.Code).ToList();
            if (latestFin.Any() && prevFin.Any())
            {
                var testFin = from vatdr in latestFin
                              join vafdr in prevFin
                                  on new { vatdr.AccountHeadCode, vatdr.CompanyProfileId } equals
                                  new { vafdr.AccountHeadCode, vafdr.CompanyProfileId } into operatingActivityVouchers
                              from fromVoucher in operatingActivityVouchers.DefaultIfEmpty()
                              join accHd in GenService.GetAll<AccHead>() on fromVoucher.AccountHeadCode equals accHd.Code
                              join headGrp in GenService.GetAll<AccHeadGroup>() on accHd.AccHeadGroupId equals headGrp.Id
                              select new VoucherArchieveDto()
                              {
                                  AccGroupId = accHd.AccGroupId,
                                  AccHeadGroupId = accHd.AccHeadGroupId,
                                  AccHeadGroupName = headGrp.Code,
                                  AccountHeadCode = vatdr.AccountHeadCode,
                                  Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                                  Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                                  CompanyProfileId = vatdr.CompanyProfileId,
                                  Amount = 0
                              }; //va.AccountHeadCode.StartsWith()

                var secDataFin = (from dt in testFin
                                  group dt by new { dt.AccountHeadCode }
                    into grp
                                  select new VoucherArchieveDto
                                  {
                                      AccGroupId = grp.FirstOrDefault().AccGroupId,
                                      AccHeadGroupId = grp.FirstOrDefault().AccHeadGroupId,
                                      AccHeadGroupName = grp.FirstOrDefault().AccHeadGroupName,
                                      AccountHeadCode = grp.Key.AccountHeadCode,
                                      Credit = grp.Sum(l => l.Credit),
                                      Debit = grp.Sum(l => l.Debit),
                                      CompanyProfileId = grp.FirstOrDefault().CompanyProfileId,
                                      //Activity = CashFlowStatementActivities.OperatingActivities,
                                      //ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                      //Debit = grp.Sum(l=>l.Debit),
                                      //Credit = vrch.Credit,
                                      Amount =
                                          (grp.FirstOrDefault().AccGroupId == 1 || grp.FirstOrDefault().AccGroupId == 5)
                                              ? (grp.Sum(l => l.Debit) - grp.Sum(l => l.Credit))
                                              : (grp.Sum(l => l.Credit) - grp.Sum(l => l.Debit))

                                  }).ToList();

                resultList.AddRange((from dt in secDataFin
                                     group dt by new { dt.AccHeadGroupId }
                    into grp
                                     let voucherArchieveDto = grp.FirstOrDefault()
                                     where voucherArchieveDto != null
                                     select new CashFlowStatementDto()
                                     {
                                         Activity = CashFlowStatementActivities.FinancingActivities,
                                         ActivityName = voucherArchieveDto.AccHeadGroupName,
                                         AccountHead = voucherArchieveDto.AccountHeadCode.Substring(0, 5),
                                         Amount = grp.Sum(l => l.Amount),
                                         Number = grp.Count()
                                     }).ToList());
            }

            #endregion
            #region Cash And Cash Equivalents

            var latestCashAndCashEqu = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == ToDate &&
                                                                             va.Status == EntityStatus.Active &&
                                                                          va.AccountHeadCode.StartsWith("10201") &&
                                                                             CompanyProfileIds.Contains(va.CompanyProfileId));
            var prevCashAndCashEqu = GenService.GetAll<VoucherArchieve>().Where(va => va.ArchieveDate == FromDate &&
                                                                             va.Status == EntityStatus.Active &&
                                                                              va.AccountHeadCode.StartsWith("10201") &&
                                                                             CompanyProfileIds.Contains(va.CompanyProfileId));
            var testCashAndCashEqu =
                from vatdr in latestCashAndCashEqu
                join vafdr in prevCashAndCashEqu
                           on new { vatdr.AccountHeadCode, vatdr.CompanyProfileId } equals new { vafdr.AccountHeadCode, vafdr.CompanyProfileId } into operatingActivityVouchers
                from fromVoucher in operatingActivityVouchers.DefaultIfEmpty()
                join accHd in GenService.GetAll<AccHead>() on fromVoucher.AccountHeadCode equals accHd.Code
                join headGrp in GenService.GetAll<AccHeadGroup>() on accHd.AccHeadGroupId equals headGrp.Id
                select new VoucherArchieveDto()
                {
                    AccGroupId = accHd.AccGroupId,
                    AccHeadGroupId = accHd.AccHeadGroupId,
                    AccHeadGroupName = headGrp.Code,
                    AccountHeadCode = vatdr.AccountHeadCode,
                    Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                    Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                    CompanyProfileId = vatdr.CompanyProfileId,
                    Amount = 0
                };
            var secDataCashAndCashEqu = (from dt in testCashAndCashEqu
                                         group dt by new { dt.AccountHeadCode } into grp
                                         select new VoucherArchieveDto()
                                         {
                                             AccGroupId = grp.FirstOrDefault().AccGroupId,
                                             AccHeadGroupId = grp.FirstOrDefault().AccHeadGroupId,
                                             AccHeadGroupName = grp.FirstOrDefault().AccHeadGroupName,
                                             AccountHeadCode = grp.Key.AccountHeadCode,
                                             Credit = grp.Sum(l => l.Credit),
                                             Debit = grp.Sum(l => l.Debit),
                                             CompanyProfileId = grp.FirstOrDefault().CompanyProfileId,
                                             //Activity = CashFlowStatementActivities.OperatingActivities,
                                             //ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                             //Debit = grp.Sum(l=>l.Debit),
                                             //Credit = vrch.Credit,
                                             Amount = (grp.FirstOrDefault().AccGroupId == 1 || grp.FirstOrDefault().AccGroupId == 5) ? (grp.Sum(l => l.Debit) - grp.Sum(l => l.Credit)) : (grp.Sum(l => l.Credit) - grp.Sum(l => l.Debit))
                                         }).ToList();

            resultList.AddRange((from dt in secDataCashAndCashEqu
                                 group dt by new { dt.AccHeadGroupId } into grp
                                 select new CashFlowStatementDto()
                                 {
                                     //AccHeadGroupId = grp.Key.AccHeadGroupId,
                                     //AccHeadGroupName = grp.FirstOrDefault().AccHeadGroupName,
                                     //AccountHeadCode = grp.FirstOrDefault().,
                                     //Credit = fromVoucher != null ? vatdr.Credit - fromVoucher.Credit : vatdr.Credit,
                                     //Debit = fromVoucher != null ? vatdr.Debit - fromVoucher.Debit : vatdr.Debit,
                                     //CompanyProfileId = vatdr.CompanyProfileId
                                     Activity = CashFlowStatementActivities.CashEquivalents,
                                     ActivityName = grp.FirstOrDefault().AccHeadGroupName,
                                     AccountHead = grp.FirstOrDefault().AccountHeadCode.Substring(0, 5),
                                     Amount = grp.Sum(l => l.Amount),
                                     Number = grp.Count()
                                 }).ToList());

            #endregion
            return resultList;
        }
        public List<CashFlowDto> GetCashFlowMergingData(DateTime FromDate, DateTime ToDate, List<long?> CompanyProfileIds)
        {
            var data = GetCashFlow(FromDate, ToDate, CompanyProfileIds);
            var dataPrev = GetCashFlow(FromDate.AddYears(-1), ToDate.AddYears(-1), CompanyProfileIds);
            var result = (from dt in data
                          join prev in dataPrev on dt.AccountHead equals prev.AccountHead
                          select new CashFlowDto()
                          {
                              Activity = dt.Activity,
                              ActivityName = dt.ActivityName,
                              AccountHead = dt.AccountHead,
                              Amount = dt.Amount,
                              PrevAmount = prev.Amount,
                              Number = dt.Number
                          }).ToList();

            //var result = 
            return result;
        }
        public List<RetainedEarningsScheduleDto> GetRetainedEarningsSchedule(DateTime FromDate, DateTime ToDate)
        {
            FromDate = FromDate.Date;
            ToDate = ToDate.Date;

            //var opening = new RetainedEarningsScheduleDto();
            //opening.Particular = "Opening";

            //if (GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode.StartsWith(BizConstants.ShareCapital) && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Count() > 0)
            //    opening.ShareCapital = GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode.StartsWith(BizConstants.ShareCapital) && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Sum(v => (v.Credit - v.Debit));
            //if (GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode.StartsWith(BizConstants.RetainedEarnings) && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Count() > 0)
            //    opening.RetainedEarnings = GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode.StartsWith(BizConstants.RetainedEarnings) && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Sum(v => (v.Credit - v.Debit));
            //if (GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode == BizConstants.Reserve && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Count() > 0)
            //    opening.Reserve = GenService.GetAll<Voucher>().Where(v => v.VoucherDate < FromDate && v.AccountHeadCode == BizConstants.Reserve && v.Status == EntityStatus.Active && v.RecordStatus == RecordStatus.Authorized).Sum(v => v.Credit - v.Debit);

            //var netProfit = new RetainedEarningsScheduleDto();
            //netProfit.Particular = "Net Profit";
            //netProfit.ShareCapital = 0;
            //netProfit.RetainedEarnings = 0;
            //netProfit.Reserve = 0;
            //netProfit.CalculateTotal();

            //var notes = GetIncomeStatementNotesRevised(FromDate, ToDate);
            //List<FinancialStatementNotesReportDto> reportNotes = processFinancialStatementNotes(notes, FromDate, ToDate);
            //List<FinancialStatementNotesReportDto> reportEntities = processNotesToGenerateIncomeStatementReport(reportNotes);
            //var Income = reportEntities.Where(n => n.NoteName == "Net Income").FirstOrDefault();
            //if (Income != null && Income.CurrentExpense != null && Income.CurrentExpense > 0)
            //    netProfit.RetainedEarnings = (decimal)Income.CurrentExpense;

            //opening.CalculateTotal();

            var dtoList = new List<RetainedEarningsScheduleDto>();
            //dtoList.Add(opening);
            //dtoList.Add(netProfit);


            return dtoList;
        }

        #region Income Statement 
        public IEnumerable<FinancialStatementNotesReportDto> GetIncomeStatement(DateTime fromDate, DateTime toDate, List<long> companies)
        {
            var newnotes = CalculateReport(BizConstants.IncomeStatementReportId, companies, fromDate, toDate, false);
            List<FinancialStatementNotesReportDto> reportEntities = new List<FinancialStatementNotesReportDto>();
            foreach (var item in newnotes)
            {
                var reportEntity = new FinancialStatementNotesReportDto();
                reportEntity.NoteName = item.ItemName;
                reportEntity.CurrentExpense = item.Amount;

                reportEntities.Add(reportEntity);
            }

            return reportEntities;
        }
        public IEnumerable<FinancialStatementNotesReportDto> GetIncomeStatementNotes(DateTime fromDate, DateTime toDate)
        {
            var companies = new List<long>();
            companies.Add(BizConstants.CompanyProfileId);
            var newnotes = CalculateReport(BizConstants.IncomeStatementReportId, companies, fromDate, toDate, true);
            List<FinancialStatementNotesReportDto> reportEntities = new List<FinancialStatementNotesReportDto>();
            foreach (var item in newnotes)
            {
                var reportEntity = new FinancialStatementNotesReportDto();
                reportEntity.AccountHeadName = item.ItemName;
                if (item.ParentId > 0)
                {
                    reportEntity.NoteId = (int)item.ParentId;
                    reportEntity.NoteName = newnotes.First(n => n.Id == item.ParentId).ItemName;
                }
                reportEntity.CurrentExpense = item.IsPositive ? item.Amount : (-1) * (item.Amount);
                //reportEntity.CurrentExpense = item.Amount;

                reportEntities.Add(reportEntity);
            }

            return reportEntities;
        }

        #endregion

        #region Calculation 
        public List<ReportConfigDto> CalculateReport(long ReportConfigId, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate, bool getDetails, bool getOldCalculation = false, int hopCount = 0)
        {
            var resultList = new List<ReportConfigDto>();
            var parentConfig = Mapper.Map<ReportConfigDto>(GenService.GetById<ReportConfig>(ReportConfigId));

            parentConfig.Amount = 0;
            parentConfig.StepCount = hopCount;
            if (parentConfig.HasChild)
            {
                //parentConfig.StepCount = BizStaticVariables.StepCount++;
                //BizStaticVariables.StepCount++;
                var childs = Mapper.Map<List<ReportConfigDto>>(GenService.GetAll<ReportConfig>().Where(rc => rc.ParentId == parentConfig.Id).ToList());
                foreach (var child in childs)
                    resultList.AddRange(CalculateReport((long)child.Id, CompanyProfileIds, fromDate, toDate, getDetails, false));
                foreach (var item in resultList.Where(r => r.ParentId != null && r.ParentId == parentConfig.Id))
                {
                    //if (item.IsPositive)
                    parentConfig.Amount += item.Amount;
                    //else
                    //    parentConfig.Amount -= item.Amount;
                }

                resultList.Add(parentConfig);
                resultList.ForEach(r => r.StepCount += 1);
                return resultList;
            }
            else if (!string.IsNullOrEmpty(parentConfig.ItemAccGroupCode))
            {

                parentConfig.Amount = CalculateLeafNode(parentConfig.ItemAccGroupCode, CompanyProfileIds, fromDate, toDate);
                ReportConfigDto income = new ReportConfigDto();
                if (parentConfig.ExternalFunction == ReportExternalFunction.Income && !getOldCalculation)
                {
                    var currentFiscalYear = new CompanyProfileFacade().GetCurrentFiscalYear();
                    var inventory = CalculateReport(BizConstants.IncomeStatementReportId, CompanyProfileIds, currentFiscalYear, toDate, false);
                    if (inventory != null && inventory.First(i => i.ParentId == 0) != null)
                    {
                        income = inventory.First(i => i.ParentId == 0);
                        parentConfig.Amount += income.Amount;
                    }
                    resultList.Add(parentConfig);
                }
                resultList.Add(parentConfig);

                if (getDetails && parentConfig.GetAccHeads != null && (bool)parentConfig.GetAccHeads)
                {
                    if (income.Amount > 0)
                    {
                        income.ItemName = "Current Income";
                        resultList.Add(income);
                    }

                    resultList.AddRange(CalculateLeafNodeDetails(parentConfig.ItemAccGroupCode, CompanyProfileIds, fromDate, toDate, (long)parentConfig.Id));
                }


            }
            //else if (string.IsNullOrEmpty(parentConfig.ItemAccGroupCode) && parentConfig.ExternalFunction == ReportExternalFunction.OpeningStock)
            //{
            //    var inventory = GetInventoryAmount(((DateTime)fromDate).AddDays(-1), "Add(1) - OpeningBalance", "OpeningBalance");
            //    parentConfig.Amount = inventory != null && inventory.Expense != null ? (decimal)inventory.Expense : 0;
            //    resultList.Add(parentConfig);
            //}
            //else if (string.IsNullOrEmpty(parentConfig.ItemAccGroupCode) && parentConfig.ExternalFunction == ReportExternalFunction.ClosingStock)
            //{
            //    var inventory = GetInventoryAmount(((DateTime)toDate), "Less - ClosingStock", "ClosingStock");
            //    parentConfig.Amount = inventory != null && inventory.Expense != null ? (decimal)inventory.Expense : 0;
            //    resultList.Add(parentConfig);
            //}
            else if (parentConfig.ExternalFunction == ReportExternalFunction.Income && !getOldCalculation)
            {
                var inventory = CalculateReport(BizConstants.IncomeStatementReportId, CompanyProfileIds, fromDate, toDate, false);
                parentConfig.Amount += inventory != null && inventory.First(i => i.ParentId == 0) != null ? inventory.First(i => i.ParentId == 0).Amount : 0;
                resultList.Add(parentConfig);
            }
            resultList.ForEach(r => r.StepCount += 1);
            return resultList;
        }
        private decimal CalculateLeafNode(string ItemAccGroupCode, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate)
        {
            decimal result = 0;
            IQueryable<VoucherArchieve> resultElementFrom;
            IQueryable<VoucherArchieve> resultElementTo;

            resultElementFrom = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(ItemAccGroupCode));
            resultElementTo = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(ItemAccGroupCode));


            if (CompanyProfileIds != null)
            {
                resultElementFrom = resultElementFrom.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
                resultElementTo = resultElementTo.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
            }

            if (fromDate != null)
            {
                fromDate = fromDate.Value.Date;
                resultElementFrom = resultElementFrom.Where(r => r.ArchieveDate <= fromDate);
            }
            if (toDate != null)
            {
                toDate = toDate.Value.Date;
                resultElementTo = resultElementTo.Where(r => r.ArchieveDate <= toDate);
            }

            if (fromDate != null)
            {
                decimal toCredit = 0, toDebit = 0, fromCredit = 0, fromDebit = 0;
                if (resultElementFrom != null && resultElementFrom.Count() > 0)
                {
                    var fromArchieveDate = resultElementFrom.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var fromElement = resultElementFrom.Where(r => r.ArchieveDate == fromArchieveDate);
                    fromDebit = fromElement.Sum(t => t.Debit);
                    fromCredit = fromElement.Sum(t => t.Credit);
                }
                if (resultElementTo != null && resultElementTo.Count() > 0)
                {
                    var toArchieveDate = resultElementTo.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var toElement = resultElementTo.Where(r => r.ArchieveDate == toArchieveDate);
                    toCredit = toElement.Sum(t => t.Credit);
                    toDebit = toElement.Sum(t => t.Debit);
                }

                if (ItemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || ItemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                    result = (toDebit - toCredit) - (fromDebit - fromCredit);
                else
                    result = (toCredit - toDebit) - (fromCredit - fromDebit);
            }
            else
            {
                decimal toCredit = 0, toDebit = 0;
                if (resultElementTo != null && resultElementTo.Count() > 0)
                {
                    var toArchieveDate = resultElementTo.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var toElement = resultElementTo.Where(r => r.ArchieveDate == toArchieveDate);
                    toCredit = toElement.Sum(t => t.Credit);
                    toDebit = toElement.Sum(t => t.Debit);
                }

                if (ItemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || ItemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                    result = (toDebit - toCredit);
                else
                    result = (toCredit - toDebit);
            }

            return result;
        }
        private List<ReportConfigDto> CalculateLeafNodeDetails(string itemAccGroupCode, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate, long parentId)
        {
            List<ReportConfigDto> result = new List<ReportConfigDto>();
            IQueryable<VoucherArchieve> resultElementFrom;
            IQueryable<VoucherArchieve> resultElementTo;

            resultElementFrom = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(itemAccGroupCode));
            resultElementTo = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(itemAccGroupCode));


            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                resultElementFrom = resultElementFrom.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
                resultElementTo = resultElementTo.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
            }

            if (fromDate != null)
            {
                fromDate = fromDate.Value.Date;
                resultElementFrom = resultElementFrom.Where(r => r.ArchieveDate == fromDate);
            }
            if (toDate != null)
            {
                toDate = toDate.Value.Date;
                resultElementTo = resultElementTo.Where(r => r.ArchieveDate == toDate);
            }
            var allAccHeads = GenService.GetAll<AccHead>().ToList();
            //-----------------------------------------------------------------------------------------------
            if (fromDate != null)
            {
                var fromElements = resultElementFrom.ToList();
                var toElements = resultElementTo.ToList();
                var tempList = toElements.ToList();
                if (itemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || itemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                {
                    foreach (var toItem in tempList)
                    {
                        var item = new ReportConfigDto();
                        item.Amount = 0;
                        var fromItem = fromElements.Where(f => f.AccountHeadCode == toItem.AccountHeadCode && f.CompanyProfileId == toItem.CompanyProfileId).FirstOrDefault();
                        if (fromItem != null)
                        {
                            item.Amount = (toItem.Debit - toItem.Credit) - (fromItem.Debit - fromItem.Credit);
                            fromElements.Remove(fromItem);
                        }
                        else
                            item.Amount = (toItem.Debit - toItem.Credit);

                        //item.Amount = amount;
                        item.CompanyProfileId = toItem.CompanyProfileId;
                        item.GetAccHeads = false;
                        item.HasChild = false;
                        item.IsPositive = true;
                        item.ItemAccGroupCode = toItem.AccountHeadCode;
                        item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                        item.ParentId = parentId;

                        toElements.Remove(toItem);
                        result.Add(item);
                    }
                }
                else
                {
                    foreach (var toItem in tempList)
                    {
                        var item = new ReportConfigDto();
                        item.Amount = 0;
                        var fromItem = fromElements.Where(f => f.AccountHeadCode == toItem.AccountHeadCode && f.CompanyProfileId == toItem.CompanyProfileId).FirstOrDefault();
                        if (fromItem != null)
                        {
                            item.Amount = (toItem.Credit - toItem.Debit) - (fromItem.Credit - fromItem.Debit);
                            fromElements.Remove(fromItem);
                        }
                        else
                            item.Amount = (toItem.Credit - toItem.Debit);

                        //item.Amount = amount;
                        item.CompanyProfileId = toItem.CompanyProfileId;
                        item.GetAccHeads = false;
                        item.HasChild = false;
                        item.IsPositive = true;
                        item.ItemAccGroupCode = toItem.AccountHeadCode;
                        item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                        item.ParentId = parentId;

                        toElements.Remove(toItem);
                        result.Add(item);
                    }
                    //result = (toElements.Credit - toElements.Debit) - (fromElements.Credit - fromElements.Debit);
                }
            }
            else
            {

                var toElements = resultElementFrom.ToList();
                var tempList = toElements.ToList();
                foreach (var toItem in tempList)
                {
                    var item = new ReportConfigDto();
                    item.Amount = 0;
                    if (itemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || itemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                        item.Amount = (toItem.Debit - toItem.Credit);
                    else
                        item.Amount = (toItem.Credit - toItem.Debit);

                    item.CompanyProfileId = toItem.CompanyProfileId;
                    item.GetAccHeads = false;
                    item.HasChild = false;
                    item.IsPositive = true;
                    item.ItemAccGroupCode = toItem.AccountHeadCode;
                    item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                    item.ParentId = parentId;

                    toElements.Remove(toItem);
                    result.Add(item);

                }
            }
            return result;
        }
        #endregion

        #region Receive Payment

        public List<string> GetMembershipVouchers(DateTime fromDate, DateTime toDate, long officeId)
        {
            //var reportData = new List<MembershipFeeDetailsReportDto>();
            DateTime openingDay = fromDate.AddDays(-1);

            #region receive vouchers
            var removeFromReceived = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active && v.Credit > 0 && v.AccountHeadCode.StartsWith("10202")).Select(v => v.VoucherNo).ToList();
            var receiveVoucherNos =  GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Debit > 0 &&
                                                                            (v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) || v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)) &&
                                                                            !removeFromReceived.Contains(v.VoucherNo))
                                                                           .Select(v => v.VoucherNo);

            #endregion
            return receiveVoucherNos.ToList();
        }
        public List<ReceivePaymentDto> GetReceivePaymentRecords(DateTime fromDate, DateTime toDate, long officeId)
        {
            var reportData = new List<ReceivePaymentDto>();
            DateTime openingDay = fromDate.AddDays(-1);

            #region receive vouchers
            var removeFromReceived = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active && v.Credit > 0 && v.AccountHeadCode.StartsWith("10202")).Select(v => v.VoucherNo).ToList();
            var receiveVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Debit > 0 &&
                                                                            (v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) || v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)) &&
                                                                            !removeFromReceived.Contains(v.VoucherNo))
                                                                .Select(v => v.VoucherNo);
            var receiveVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          receiveVoucherNos.Contains(v.VoucherNo) &&
                                                                          !(v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) || v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)) &&
                                                                          !removeFromReceived.Contains(v.VoucherNo))
                                                              .GroupBy(v => v.AccountHeadCode)
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.Key,
                                                                  AccountHead = "",
                                                                  Amount = v.Sum(e => e.Credit - e.Debit),
                                                                  IsCredit = true
                                                              }).ToList();
            if (receiveVouchers != null && receiveVouchers.Count > 0)
                reportData.AddRange(receiveVouchers);
            #endregion

            #region payment vouchers
            var paymentVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Credit > 0 &&
                                                                            (v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) || v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)))
                                                                .Select(v => v.VoucherNo);
            var paymentVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          paymentVoucherNos.Contains(v.VoucherNo) &&
                                                                          !(v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) || v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)))
                                                              .GroupBy(v => v.AccountHeadCode)
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.Key,
                                                                  AccountHead = "",
                                                                  Amount = v.Sum(e => e.Debit - e.Credit),
                                                                  IsCredit = false
                                                              }).ToList();
            if (paymentVouchers != null && paymentVouchers.Count > 0)
                reportData.AddRange(paymentVouchers);
            #endregion

            if (reportData.Count > 0)
            {
                reportData = (from data in reportData
                              join acchead in GenService.GetAll<AccHeadMain>() on data.AccountCode equals acchead.Code into withNames
                              from newData in withNames.DefaultIfEmpty()
                              select new ReceivePaymentDto
                              {
                                  AccountCode = data.AccountCode,
                                  AccountHead = newData.Name,
                                  Amount = data.Amount,
                                  IsCredit = data.IsCredit
                              }).ToList();

            }
            var openCash = GenService.GetAll<VoucherArchieve>().Where( v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId);
            decimal openingCashInHand = (decimal) 0.0;
            if (openCash.Any())
            {
                openingCashInHand = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }
             
            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "00",
                AccountHead = "Opening - Cash",
                Amount = openingCashInHand,
                IsCredit = false
            });
            var closingCashInHandData = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId);
            decimal closingCashInHand = (decimal) 0.0;
            if (closingCashInHandData.Any())
            {
                 closingCashInHand = closingCashInHandData.Sum(v => v.Debit - v.Credit);
            }
           
            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "90",
                AccountHead = "Closing - Cash",
                Amount = closingCashInHand,
                IsCredit = false
            });
            var openingBankData =GenService.GetAll<VoucherArchieve>().Where( v =>  v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId);
            decimal openingBank = (decimal)0.0;
            if (openingBankData.Any())
            {
                openingBank = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }
             
            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "01",
                AccountHead = "Opening - Bank",
                Amount = openingBank,
                IsCredit = false
            });
            var closingBankData =GenService.GetAll<VoucherArchieve>().Where( v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId);
            decimal closingBank = (decimal) 0.0;
            if (closingBankData.Any())
            {
                closingBank = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }
            
            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "91",
                AccountHead = "Closing - Bank",
                Amount = closingBank,
                IsCredit = false
            });
            reportData = reportData.OrderBy(r => r.AccountCode).ToList();
            return reportData;
        }
        public List<ReceivePaymentDto> GetReceivePaymentTransactionalRecords(DateTime fromDate, DateTime toDate, long officeId)
        {
            var reportData = new List<ReceivePaymentDto>();
            DateTime openingDay = fromDate.AddDays(-1);

            #region receive vouchers
            var removeFromReceived = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active && v.Credit > 0 && v.AccountHeadCode.StartsWith("10202")).Select(v=>v.VoucherNo).ToList();
            var receiveCashVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Debit > 0 &&
                                                                            v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) &&
                                                                            !removeFromReceived.Contains(v.VoucherNo))
                                                                .Select(v => v.VoucherNo);
            var receiveBankVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Debit > 0 &&
                                                                            v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) &&
                                                                            !removeFromReceived.Contains(v.VoucherNo))
                                                                .Select(v => v.VoucherNo);
            var receiveCashVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          receiveCashVoucherNos.Contains(v.VoucherNo) &&
                                                                          !(v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup)))
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.AccountHeadCode,
                                                                  AccountHead = "",
                                                                  CashAmount = (v.Credit - v.Debit),
                                                                  IsCredit = true,
                                                                  VoucherDate = v.VoucherDate,
                                                                  VoucherNo = v.VoucherNo,
                                                                  VoucherDetails=v.Description
                                                              }).ToList();
            var receiveBankVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          receiveBankVoucherNos.Contains(v.VoucherNo) &&
                                                                          !( v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)))
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.AccountHeadCode,
                                                                  AccountHead = "",
                                                                  BankAmount = (v.Credit - v.Debit),
                                                                  IsCredit = true,
                                                                  VoucherDate = v.VoucherDate,
                                                                  VoucherNo = v.VoucherNo,
                                                                  VoucherDetails = v.Description
                                                              }).ToList();
            if (receiveCashVouchers != null && receiveCashVouchers.Count > 0)
                reportData.AddRange(receiveCashVouchers);
            if (receiveBankVouchers != null && receiveBankVouchers.Count > 0)
                reportData.AddRange(receiveBankVouchers);
            #endregion

            #region payment vouchers
            var paymentCashVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Credit > 0 &&
                                                                            v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup))
                                                                .Select(v => v.VoucherNo);
            var paymentBankVoucherNos = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                            v.RecordStatus == RecordStatus.Authorized &&
                                                                            v.VoucherDate >= fromDate &&
                                                                            v.VoucherDate <= toDate &&
                                                                            v.CompanyProfileId == officeId &&
                                                                            v.Credit > 0 &&
                                                                            v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup))
                                                                .Select(v => v.VoucherNo);
            var paymentCashVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          paymentCashVoucherNos.Contains(v.VoucherNo) &&
                                                                          !(v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup)) &&
                                                                          v.AccountHeadCode.StartsWith("5"))
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.AccountHeadCode,
                                                                  AccountHead = "",
                                                                  CashAmount = (v.Debit - v.Credit),
                                                                  IsCredit = false,
                                                                  VoucherDate = v.VoucherDate,
                                                                  VoucherNo = v.VoucherNo,
                                                                  VoucherDetails = v.Description
                                                              }).ToList();
            var paymentBankVouchers = GenService.GetAll<Voucher>().Where(v => v.Status == EntityStatus.Active &&
                                                                          v.RecordStatus == RecordStatus.Authorized &&
                                                                          v.VoucherDate >= fromDate &&
                                                                          v.VoucherDate <= toDate &&
                                                                          v.CompanyProfileId == officeId &&
                                                                          paymentBankVoucherNos.Contains(v.VoucherNo) &&
                                                                          !(v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)) &&
                                                                          v.AccountHeadCode.StartsWith("5"))
                                                              .Select(v => new ReceivePaymentDto()
                                                              {
                                                                  AccountCode = v.AccountHeadCode,
                                                                  AccountHead = "",
                                                                  BankAmount = (v.Debit - v.Credit),
                                                                  IsCredit = false,
                                                                  VoucherDate = v.VoucherDate,
                                                                  VoucherNo = v.VoucherNo,
                                                                  VoucherDetails = v.Description
                                                              }).ToList();
            if (paymentCashVouchers != null && paymentCashVouchers.Count > 0)
                reportData.AddRange(paymentCashVouchers);
            if (paymentBankVouchers != null && paymentBankVouchers.Count > 0)
                reportData.AddRange(paymentBankVouchers);
            #endregion

            if (reportData.Count > 0)
            {
                reportData = (from data in reportData
                              join acchead in GenService.GetAll<AccHeadMain>() on data.AccountCode equals acchead.Code into withNames
                              from newData in withNames.DefaultIfEmpty()
                              select new ReceivePaymentDto
                              {
                                  AccountCode = data.AccountCode,
                                  AccountHead = newData.Name,
                                  CashAmount = data.CashAmount,
                                  BankAmount = data.BankAmount,
                                  IsCredit = data.IsCredit,
                                  VoucherDate = data.VoucherDate,
                                  VoucherNo=data.VoucherNo,
                                  VoucherDetails = data.VoucherDetails
                              }).ToList();

            }
            var openCash = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId);
            decimal openingCashInHand = (decimal)0.0;
            if (openCash.Any())
            {
                openingCashInHand = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }
            var openingBankData = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId);
            decimal openingBank = (decimal)0.0;
            if (openingBankData.Any())
            {
                openingBank = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == openingDay && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }
            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "00",
                AccountHead = "Opening Balance",
                CashAmount = openingCashInHand,
                BankAmount = openingBank,
                IsCredit = false,
                VoucherDate = openingDay
            });
            var closingCashInHandData = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId);
            decimal closingCashInHand = (decimal)0.0;
            if (closingCashInHandData.Any())
            {
                closingCashInHand = closingCashInHandData.Sum(v => v.Debit - v.Credit);
            }            
            var closingBankData = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId);
            decimal closingBank = (decimal)0.0;
            if (closingBankData.Any())
            {
                closingBank = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup) && v.ArchieveDate == toDate && v.CompanyProfileId == officeId)
                                                                        .Sum(v => v.Debit - v.Credit);
            }

            reportData.Add(new ReceivePaymentDto()
            {
                AccountCode = "90",
                AccountHead = "Closing Balance",
                CashAmount = closingCashInHand,
                BankAmount = closingBank,
                IsCredit = false,
                VoucherDate = toDate
            });
            reportData = reportData.OrderBy(r => r.AccountCode).ToList();
            return reportData;
        }
        #endregion

        public LedgerDto GetLedgerAmount(DateTime firstDayOfMonth, DateTime todate, string accHeadCode, long selectedCompanyId)
        {
            var result = new LedgerDto();
            var data = GetLedgerReportByAccHead(firstDayOfMonth, todate, accHeadCode, selectedCompanyId);
            if (data.Any())
            {
                result = data.OrderByDescending(r => r.Id).FirstOrDefault();
                return result;
            }
            return result;
        }
    }
}
