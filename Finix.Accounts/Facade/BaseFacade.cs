﻿using Finix.Accounts.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace Finix.Accounts.Facade
{
    public abstract class BaseFacade
    {
        private readonly GenService genService;
        
        protected BaseFacade()
        {
            genService = new GenService();
        }

        public GenService GenService
        {
            get { return genService; }
        }
        
    }
}
