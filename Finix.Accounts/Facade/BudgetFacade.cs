﻿using AutoMapper;
using Finix.Accounts.Dto;
using Finix.Accounts.Infrastructure;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Finix.Accounts.Facade
{
    public class BudgetFacade : BaseFacade
    {
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        private readonly UserFacade _user = new UserFacade();

        #region Budget Config
        public ResponseDto SaveBudgetConfig(List<BudgetConfigDto> dtos, long userId, long officeId)
        {
            var response = new ResponseDto();
            var configList = new List<BudgetConfig>();
            foreach(var config in dtos)
            {
                if(config.Id > 0)
                {
                    var entity = GenService.GetById<BudgetConfig>((long)config.Id);
                    if(entity != null)
                    {
                        config.CreateDate = entity.CreateDate;
                        config.CreatedBy = entity.CreatedBy;
                        config.EditDate = DateTime.Now;
                        config.EditedBy = userId;
                        config.Status = entity.Status;
                        Mapper.Map(config, entity);
                        configList.Add(entity);
                    }
                }
                else
                {
                    config.CreateDate = DateTime.Now;
                    config.CreatedBy = userId;
                    config.Status = EntityStatus.Active;
                    var entity = Mapper.Map<BudgetConfig>(config);
                    configList.Add(entity);
                }
            }
            var idsToKeep = configList.Where(c => c.Id > 0).Select(c => c.Id).Distinct(); //todo - might face a null exception
            var deletableConfigs = GenService.GetAll<BudgetConfig>().Where(c => c.Status == EntityStatus.Active && c.OfficeId == officeId && !idsToKeep.Contains(c.Id));
            if (deletableConfigs.Any())
            {
                var deletable = deletableConfigs.ToList();
                deletable.ForEach(c => { c.Status = EntityStatus.Deleted; c.EditDate = DateTime.Now; c.EditedBy = userId; });
                configList.AddRange(deletable);
            }
            using(var tran = new TransactionScope())
            {
                try
                {
                    GenService.Save(configList);
                    GenService.SaveChanges();
                    tran.Complete();
                    response.Message = "Budget configuration saved.";
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    tran.Dispose();
                    response.Message = "Budget configuration could not be saved";
                }
            }
            return response;
        }
        public List<BudgetConfigDto> GetBudgetConfigsByOffice(long officeId)
        {
            var result = new List<BudgetConfigDto>();
            var configs = GenService.GetAll<BudgetConfig>().Where(c => c.Status == EntityStatus.Active && c.OfficeId == officeId);

            if (configs.Any())
                result = Mapper.Map<List<BudgetConfigDto>>(configs);

            return result;
        }
        public List<string> GetUsedControlHeads(string prefix)
        {
            var result = new List<string>();
            prefix = prefix.ToLower();
            result = GenService.GetAll<BudgetConfig>().Where(c => c.ControlHead.ToLower().Contains(prefix)).Select(c => c.ControlHead).ToList();
            return result;
        }
        #endregion

        #region Budget
        public List<BudgetDto> GetBudgetsByOffice(long officeId)
        {
            var result = new List<BudgetDto>();
            if(GenService.GetAll<Budget>().Where(b => b.Status == EntityStatus.Active && b.OfficeId == officeId).Any())
                result = Mapper.Map<List<BudgetDto>>(GenService.GetAll<Budget>().Where(b => b.Status == EntityStatus.Active && b.OfficeId == officeId).ToList());
            return result;
        }
        public ResponseDto SaveBudget(BudgetDto dto, long userId, long officeId)
        {
            var response = new ResponseDto();
            Budget entity;
            if (dto.Id > 0)
            {
                entity = GenService.GetById<Budget>((long)dto.Id);
                if(entity == null)
                {
                    response.Message = "Budget not found";
                    return response;
                }
                if(entity.BudgetStatus == BudgetStatus.Approved)
                {
                    response.Message = "Budget is already approved and cannot be edited.";
                    return response;
                }

                dto.CreateDate = entity.CreateDate;
                dto.CreatedBy = entity.CreatedBy;
                dto.Status = entity.Status;
                dto.EditDate = DateTime.Now;
                dto.EditedBy = userId;
                Mapper.Map(dto, entity);

                foreach(var detail in dto.Details)
                {
                    if(detail.Id > 0)
                    {
                        var eDetail = entity.Details.Where(d => d.Id == detail.Id).First();
                        detail.CreateDate = eDetail.CreateDate;
                        detail.CreatedBy = eDetail.CreatedBy;
                        detail.Status = eDetail.Status;
                        detail.EditDate = DateTime.Now;
                        detail.EditedBy = userId;
                        Mapper.Map(detail, eDetail);
                    }
                    else
                    {
                        detail.CreateDate = DateTime.Now;
                        detail.CreatedBy = userId;
                        detail.Status = EntityStatus.Active;
                        entity.Details.Add(Mapper.Map<BudgetDetail>(detail));
                    }
                }

                var idsToKeep = dto.Details.Where(c => c.Id > 0).Select(c => c.Id).Distinct(); //todo - might face a null exception
                var deletableDetails = entity.Details.Where(c => c.Status == EntityStatus.Active &&  !idsToKeep.Contains(c.Id)).ToList();
                if (deletableDetails.Any())//todo - experimental code - check if original data is deleted or not
                {
                    deletableDetails.ForEach(c => { c.Status = EntityStatus.Deleted; c.EditDate = DateTime.Now; c.EditedBy = userId; });                    
                }

            }
            else
            {
                entity = Mapper.Map<Budget>(dto);
                entity.OfficeId = officeId;
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.Status = EntityStatus.Active;
                dto.Details.ForEach(d => { d.CreateDate = DateTime.Now; d.CreatedBy = userId; d.Status = EntityStatus.Active; }); 
                entity.Details = new List<BudgetDetail>();
                entity.Details = Mapper.Map<List<BudgetDetail>>(dto.Details);

            }
            try
            {
                GenService.Save(entity);
                GenService.SaveChanges();
                response.Message = "Budget Saved.";
                response.Success = true;
                response.Id = entity.Id;
            }
            catch (Exception)
            {
                response.Message = "Budget save failed.";
            }
            return response;
        }
        public BudgetDto GetBudgetById(long id)
        {
            var budget = GenService.GetById<Budget>(id);
            if (budget != null)
                return Mapper.Map<BudgetDto>(budget);
            return new BudgetDto();
        }
        public BudgetDto GetLatestBudgetByOfficeForCreatingNewBudget(long officeId, DateTime fromDate, DateTime toDate)
        {
            var result = new BudgetDto();
            result.Details = new List<BudgetDetailDto>();
            var dateDiff = (toDate - fromDate).Days;
            var maxDate = fromDate.AddDays(-1);
            var minDate = fromDate.AddDays(-1).AddDays(dateDiff);
            var currentBudgetConfigs = GenService.GetAll<BudgetConfig>().Where(b => b.OfficeId == officeId && b.Status == EntityStatus.Active).ToList();

            var data = GenService.GetAll<Budget>().Where(b => b.OfficeId == officeId && b.Status == EntityStatus.Active)
                                                  .OrderByDescending(b => b.ToDate)
                                                  .ThenByDescending(b => b.BudgetStatus)
                                                  .ThenByDescending(b => b.FromDate)
                                                  .FirstOrDefault();
            if (data != null)
            {
                foreach(var item in data.Details)
                {
                    var detail = new BudgetDetailDto();
                    detail.BudgetConfigId = item.BudgetConfigId;
                    detail.AccGroupId = item.BudgetConfig.AccGroupId;
                    detail.Amount = item.Amount;
                    detail.IncreaseAmount = item.IncreaseAmount;
                    detail.IncreasePercentage = item.IncreasePercentage;
                    detail.AccHeadName = item.BudgetConfig.AccHead.Name;
                    
                    #region setting previous amount
                    var fromArchieve = GenService.GetAll<VoucherArchieve>().Where(v => v.CompanyProfileId == officeId && v.Status == EntityStatus.Active && v.AccountHeadCode == item.BudgetConfig.AccHead.Code && v.ArchieveDate == minDate).FirstOrDefault();
                    var toArchieve = GenService.GetAll<VoucherArchieve>().Where(v => v.CompanyProfileId == officeId && v.Status == EntityStatus.Active && v.AccountHeadCode == item.BudgetConfig.AccHead.Code && v.ArchieveDate == maxDate).FirstOrDefault();
                    if (fromArchieve != null && toArchieve != null)
                        detail.PreviousActualAmount = detail.AccGroupId == 4 ? (fromArchieve.Credit - fromArchieve.Debit - toArchieve.Credit + toArchieve.Debit) : (fromArchieve.Debit - fromArchieve.Credit - toArchieve.Debit + toArchieve.Credit);
                    else if (toArchieve != null)
                        detail.PreviousActualAmount = detail.AccGroupId == 4 ? (toArchieve.Credit - toArchieve.Debit) : (toArchieve.Debit - toArchieve.Credit);
                    else
                        detail.PreviousActualAmount = 0;
                    #endregion

                    detail.PreviousBudget = item.ProposedAmount;
                    detail.ProposedAmount = item.ProposedAmount;
                    detail.Quantity = item.Quantity;
                    detail.Unit = item.Unit;

                    result.Details.Add(detail);

                    var config = currentBudgetConfigs.Where(c => c.Id == item.BudgetConfigId).FirstOrDefault();
                    if (config != null)
                        currentBudgetConfigs.Remove(config);
                }

                //result = Mapper.Map<BudgetDto>(data);
            }
            if(currentBudgetConfigs.Count > 0)
            {
                foreach(var configItem in currentBudgetConfigs)
                {
                    var detail = new BudgetDetailDto();
                    detail.BudgetConfigId = configItem.Id;
                    detail.AccGroupId = configItem.AccGroupId;
                    detail.AccHeadName = configItem.AccHead.Name;
                    detail.Amount = 0;
                    detail.IncreaseAmount = 0;
                    detail.IncreasePercentage = 0;
                    detail.PreviousActualAmount = 0;
                    detail.PreviousBudget = 0;
                    detail.ProposedAmount = 0;
                    detail.Quantity = 1;
                    detail.Unit = 1;

                    result.Details.Add(detail);
                }
            }

            return result;
        }
        public IPagedList<BudgetDto> BudgetList(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, string searchString, List<long> officeIds)
        {
            //fromDate = fromDate.AddDays(-1);
            //toDate = toDate.AddDays(1);
            var budgets = GenService.GetAll<Budget>().Where(m => m.Status == EntityStatus.Active);

            if (!string.IsNullOrEmpty(searchString))
            {
                //searchString = searchString.ToLower();
                //budgets = budgets.Where(m => m.NameOfOrganization.ToLower().Contains(searchString));
            }
            if (officeIds != null && officeIds.Count > 0)
                budgets = budgets.Where(b => officeIds.Contains(b.OfficeId));
            //if (zone != null)
            //    budgets = budgets.Where(m => m.Zone == zone);
            if (fromDate != null && fromDate > DateTime.MinValue)
                budgets = budgets.Where(b => b.FromDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                budgets = budgets.Where(m => m.ToDate <= toDate);
            var budgetList = from budget in budgets
                              select new BudgetDto
                              {
                                  Id = budget.Id,
                                  OfficeId = budget.OfficeId,
                                  OfficeName = "",
                                  FromDate = budget.FromDate,
                                  ToDate = budget.ToDate,
                                  BudgetStatus = budget.BudgetStatus,
                                  BudgetStatusName = budget.BudgetStatus.ToString()                                  
                              };
            var temp = budgetList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }
        #endregion
    }
}
