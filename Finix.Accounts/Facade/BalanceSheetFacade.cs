﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Service;
using System.Globalization;
using Finix.Auth.Facade;

namespace Finix.Accounts.Facade
{
    public class BalanceSheetFacade : BaseFacade
    {
        private readonly ConncetionFacade _connection = new ConncetionFacade();
        public List<BalanceSheetDto> GetAllBalanceSheet(List<long> CompanyProfileIds, DateTime fromDate, DateTime? toDate)
        {
            SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
            CompanyProfileFacade _company = new CompanyProfileFacade();
            //DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();

            if (toDate == null || toDate >= DateTime.MaxValue || toDate <= DateTime.MinValue)
                toDate = DateTime.Now;

            var originalTodate = ((DateTime)toDate).Date;
            toDate = ((DateTime)toDate).AddDays(1).Date;

            #region other data
            ReportAccountFacade _reportAccountFacade = new ReportAccountFacade();
            //var notes = _reportAccountFacade.GetIncomeStatementNotesRevised(fromDate, originalTodate);//Convert.ToDateTime("01/01/2016")
            //List<FinancialStatementNotesReportDto> reportNotes = _reportAccountFacade.processFinancialStatementNotes(notes, fromDate, originalTodate);// Convert.ToDateTime("01/01/2016")
            //List<FinancialStatementNotesReportDto> reportEntities = _reportAccountFacade.processNotesToGenerateIncomeStatementReport(reportNotes);
            var _accounts = new AccountsFacade();
            //get income statement---
            var notes = _accounts.CalculateReport(BizConstants.IncomeStatementReportId, CompanyProfileIds, null, toDate, false);
            List<FinancialStatementNotesReportDto> reportEntities = new List<FinancialStatementNotesReportDto>();
            foreach (var item in notes)
            {
                var reportEntity = new FinancialStatementNotesReportDto();
                reportEntity.NoteName = item.ItemName;
                reportEntity.CurrentExpense = item.Amount;

                reportEntities.Add(reportEntity);
            }

            var balanceSheetCurrentItems = _accounts.CalculateReport(BizConstants.BalanceSheetReportId, CompanyProfileIds, null, toDate, false);


            //var lastYearNotes = _reportAccountFacade.GetIncomeStatementNotesRevised(CurrentFiscalYear.AddYears(-1), CurrentFiscalYear);
            //List<FinancialStatementNotesReportDto> lastYearReportNotes = _reportAccountFacade.processFinancialStatementNotes(lastYearNotes, CurrentFiscalYear.AddYears(-1), CurrentFiscalYear);
            //List<FinancialStatementNotesReportDto> lastYearReportEntities = _reportAccountFacade.processNotesToGenerateIncomeStatementReport(lastYearReportNotes);
            decimal retainedEarnings = 0;
            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                var vchr = GenService.GetAll<Voucher>().Where(v => v.AccountHeadCode == BizConstants.RetainedEarnings &&
                                                                 CompanyProfileIds.Contains((long) v.CompanyProfileId));
                if (vchr.Any())
                {
                    retainedEarnings = vchr.Sum(v => (v.Credit - v.Debit));
                }
               
            }
                
            //.Sum();
            else
            {
                var vrchr = GenService.GetAll<Voucher>().Where(v => v.AccountHeadCode == BizConstants.RetainedEarnings).ToList();
                if (vrchr != null)
                {
                    retainedEarnings = vrchr.Select(v => (v.Credit - v.Debit)).Sum();
                }
            }
            #endregion

            #region prepare current year query
            //decimal currentInventoryAmount = 0;
            decimal currentNetIncome = 0;
            decimal previousNetIncome = 0;
            //var currentInventory = (notes.Where(n => n.AccountHeadCode == "ClosingStock" && n.Year.Contains(fromDate.Year.ToString())).FirstOrDefault());
            //var Income = reportEntities.Where(n => n.NoteName == "Net Income").FirstOrDefault();
            //if (Income != null && Income.CurrentExpense != null && Income.CurrentExpense > 0)
            //    currentNetIncome = (decimal)Income.CurrentExpense;
            //if (Income != null && Income.PreviousExpense != null && Income.PreviousExpense > 0)
            //    previousNetIncome = (decimal)Income.PreviousExpense;
            //if (currentInventory != null && currentInventory.Expense != null)
            //    currentInventoryAmount = (decimal)currentInventory.Expense * (-1);
            string query = "(select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, " +
                           "IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, " +
                           "IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, " +
                           "case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount  " +
                           "from Voucher v " +
                           "left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) " +
                           "left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) " +
                           "left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) " +
                           "left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) " +
                           "where ag.Code not in (4,5) AND v.AccountHeadCode not like '" + BizConstants.RetainedEarnings + "%'  AND v.VoucherDate < '" + ((DateTime)toDate).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' ";
            //if (CompanyProfileId != null && CompanyProfileId > 0)
            //    query += "AND v.CompanyProfileId = " + CompanyProfileId ; 
            query +=       " group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code )" +
                           //"union ( select 'Asset' as AccGroupName, 'Current Asset' as AccSubGroupName, 'Inventory' as AccHeadGroupName, " +
                            //"'' as AccHeadSubGroupName, '1' as AccGroupCode, '102' as AccSubGroupCode, " +
                            //"'10204' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + currentInventoryAmount.ToString() + " as CurrentYearAmount )" +
                           "union ( select 'Equity' as AccGroupName, 'Retained Earning' as AccSubGroupName, 'Retained Earning' as AccHeadGroupName, " +
                           "'' as AccHeadSubGroupName, '3' as AccGroupCode, '3_' as AccSubGroupCode, " +
                           "'' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + (retainedEarnings + currentNetIncome).ToString() + " as CurrentYearAmount )" +
                           "order by AccGroupName, AccSubGroupName, AccHeadGroupName, AccHeadSubGroupName, AccGroupCode, AccSubGroupCode, AccHeadGroupCode, AccHeadSubGroupCode";
            #endregion

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dt = new DataTable();
            conn.Open();
            da.Fill(dt);
            conn.Close();

            #region prepare previous year query

            //decimal previousInventoryAmount = 0;
            //var previousInventory = (notes.Where(n => n.AccountHeadCode == "ClosingStock" && n.Year.Contains(fromDate.AddYears(-1).Year.ToString())).FirstOrDefault());
            //if (previousInventory != null && previousInventory.Expense != null)
            //    previousInventoryAmount = (decimal)previousInventory.Expense * (-1);

            string qry = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, " +
                "IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, " +
                "IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, " +
                "case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount " +
                "from Voucher v " +
                "left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) " +
                "left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) " +
                "left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) " +
                "left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) " +
                "where ag.Code not in (4,5) and v.VoucherDate < '" + fromDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' ";//"'01/01/2016'"
            //if (CompanyProfileId != null && CompanyProfileId > 0)
            //    qry += "AND v.CompanyProfileId = " + CompanyProfileId; 

            qry +=  " group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code " +
                    //"union ( select 'Asset' as AccGroupName, 'Current Asset' as AccSubGroupName, 'Inventory' as AccHeadGroupName, " +
                    //"'' as AccHeadSubGroupName, '1' as AccGroupCode, '102' as AccSubGroupCode, " +
                    //"'10204' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + previousInventoryAmount.ToString() + " as CurrentYearAmount )" +
                    "union ( select 'Equity' as AccGroupName, 'Retained Earning' as AccSubGroupName, 'Retained Earning' as AccHeadGroupName, " +
                    "'' as AccHeadSubGroupName, '3' as AccGroupCode, '3_' as AccSubGroupCode, " +
                    "'' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + previousNetIncome.ToString() + " as CurrentYearAmount )" +
                    "   order by AccGroupName, AccSubGroupName, AccHeadGroupName, AccHeadSubGroupName, AccGroupCode, AccSubGroupCode, AccHeadGroupCode, AccHeadSubGroupCode";
            #endregion

            SqlDataAdapter daa = new SqlDataAdapter(qry, conn);
            DataTable dtt = new DataTable();
            conn.Open();
            daa.Fill(dtt);
            conn.Close();

            dt.Columns.Add("PreviousYearAmount");

            int previousDtColNum = dtt.Rows.Count;
            for (int i = 0; i < previousDtColNum; i++)
            {
                string previousAccGroupCode = dtt.Rows[i]["AccGroupCode"].ToString();
                string previousAccSubGroupCode = dtt.Rows[i]["AccSubGroupCode"].ToString();
                string previousAccHeadGroupCode = dtt.Rows[i]["AccHeadGroupCode"].ToString();
                string previousAccHeadSubGroupCode = dtt.Rows[i]["AccHeadSubGroupCode"].ToString();



                int presentDtColNum = dt.Rows.Count;

                for (int j = 0; j < presentDtColNum; j++)
                {
                    string presentAccGroupCode = dt.Rows[j]["AccGroupCode"].ToString();
                    string presentAccSubGroupCode = dt.Rows[j]["AccSubGroupCode"].ToString();
                    string presentAccHeadGroupCode = dt.Rows[j]["AccHeadGroupCode"].ToString();
                    string presentAccHeadSubGroupCode = dt.Rows[j]["AccHeadSubGroupCode"].ToString();

                    string presentYearAmount = dt.Rows[j]["PreviousYearAmount"].ToString();
                    if (previousAccGroupCode == presentAccGroupCode && previousAccSubGroupCode == presentAccSubGroupCode && previousAccHeadGroupCode == presentAccHeadGroupCode && previousAccHeadSubGroupCode == presentAccHeadSubGroupCode)
                    {
                        dt.Rows[j]["PreviousYearAmount"] = dtt.Rows[i]["PreviousYearAmount"];
                        break;
                    }
                }

            }

            List<BalanceSheetDto> list = new List<BalanceSheetDto>();

            foreach (DataRow dr in dt.Rows)
            {


                decimal currentYearAmount = 0;
                decimal.TryParse(dr["CurrentYearAmount"].ToString(), out currentYearAmount);
                decimal previousYearAmount = 0;
                decimal.TryParse(dr["PreviousYearAmount"].ToString(), out previousYearAmount);

                BalanceSheetDto bdto = new BalanceSheetDto();
                bdto.AccSubGroupName = dr["AccSubGroupName"].ToString();
                bdto.AccHeadGroupName = dr["AccHeadGroupName"].ToString();
                bdto.AccGroupName = dr["AccGroupName"].ToString();
                bdto.AccHeadSubGroupName = dr["AccHeadSubGroupName"].ToString();
                bdto.CurrentYearAmount = currentYearAmount;
                bdto.PreviousYearAmount = previousYearAmount;
                //bdto.Debit = debit;
                //bdto.Credit = credit;
                list.Add(bdto);
            }


            return list;
        }

        public List<BalanceSheetNotesDto> GetBalanceSheetNotes(List<long> CompanyProfileIds, DateTime fromDate, DateTime toDate)
        {
            List<BalanceSheetNotesDto> notes = new List<BalanceSheetNotesDto>();
            SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
            
            string query = "SELECT t1.AccHeadCode as 'AccGroupId', t1.CurrentAdd AS 'CurrentAdd', t1.CurrentLess AS 'CurrentLess', ISNULL(t2.PreviousAdd,0) as 'PreviousAdd', " +
            "ISNULL(t2.PreviousLess,0) as 'PreviousLess', ISNULL(t3.PreviousOpening,0) as 'PreviousOpening' " +
            "FROM (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode', case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 " +
            "then Sum(ISNULL(Debit,0)) else Sum(ISNULL(Credit,0)) end as 'CurrentAdd', " +
            "case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Credit,0)) else Sum(ISNULL(Debit,0)) end as 'CurrentLess' " +
            "FROM Voucher WHERE VoucherDate < '" + toDate.AddDays(1).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' ";
            
            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                query += "AND v.CompanyProfileId in (";// + CompanyProfileIds;
                for(int i = 0; i < CompanyProfileIds.Count; i++)
                {
                    query += CompanyProfileIds[i];
                    if (i < (CompanyProfileIds.Count - 1))
                        query += ", ";
                }
                query += ")";

            }

            query += " GROUP BY SUBSTRING(AccountHeadCode,1,1)) t1 " +
            "left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  " +
            "case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Debit,0)) else Sum(ISNULL(Credit,0)) end as 'PreviousAdd', " +
            "case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Credit,0)) else Sum(ISNULL(Debit,0)) end as 'PreviousLess' " +
            "FROM Voucher WHERE VoucherDate < '" + fromDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' AND AccountHeadCode not like '10204%'  GROUP BY SUBSTRING(AccountHeadCode,1,1)) t2 on t1.AccHeadCode = t2.AccHeadCode " +
            "left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  (Sum(ISNULL(Debit,0)) - Sum(ISNULL(Credit,0))) as 'PreviousOpening' " +
            "from Voucher where VoucherDate < '" + fromDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' AND AccountHeadCode not like '10204%'  group by SUBSTRING(AccountHeadCode,1,1)) t3 on t1.AccHeadCode = t3.AccHeadCode";

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dataTable = new DataTable();
            conn.Open();
            da.Fill(dataTable);
            conn.Close();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var item = new BalanceSheetNotesDto();
                item.AccGroupId = Convert.ToInt64(dataTable.Rows[i]["AccGroupId"]);
                item.CurrentAdd = (decimal?)dataTable.Rows[i]["CurrentAdd"];
                item.CurrentLess = (decimal?)dataTable.Rows[i]["CurrentLess"];
                item.PreviousAdd = (decimal?)dataTable.Rows[i]["PreviousAdd"];
                item.PreviousLess = (decimal?)dataTable.Rows[i]["PreviousLess"];
                item.PreviousOpening = (decimal?)dataTable.Rows[i]["PreviousOpening"];

                notes.Add(item);

            }
            return notes;
        }
        public List<BalanceSheetDto> GetAllBalanceSheetDetails(List<long> CompanyProfileIds, DateTime fromDate, DateTime? toDate)
        {
            #region other data
            CompanyProfileFacade _company = new CompanyProfileFacade();
            //DateTime CurrentFiscalYear = _company.GetCurrentFiscalYear();
            ReportAccountFacade _reportAccountFacade = new ReportAccountFacade();
            

            if (toDate == null || toDate >= DateTime.MaxValue || toDate <= DateTime.MinValue)
                toDate = DateTime.Now;
            var originalToDate = ((DateTime)toDate).Date;
            toDate = ((DateTime)toDate).AddDays(1).Date;
            var _accounts = new AccountsFacade();
            //var notes = _reportAccountFacade.GetIncomeStatementNotesRevised(fromDate, originalToDate);
            var notes = _accounts.CalculateReport(BizConstants.IncomeStatementReportId, CompanyProfileIds, null, toDate, true);
            //List<FinancialStatementNotesReportDto> reportNotes = _reportAccountFacade.processFinancialStatementNotes(notes, CurrentFiscalYear, DateTime.Now);
            //List<FinancialStatementNotesReportDto> reportEntities = _reportAccountFacade.processNotesToGenerateIncomeStatementReport(reportNotes);
            //List<FinancialStatementNotesReportDto> reportNotes = _reportAccountFacade.processFinancialStatementNotes(notes, (DateTime)fromDate, originalToDate);// Convert.ToDateTime("01/01/2016")
            //List<FinancialStatementNotesReportDto> reportEntities = _reportAccountFacade.processNotesToGenerateIncomeStatementReport(reportNotes);
            List<FinancialStatementNotesReportDto> reportEntities = new List<FinancialStatementNotesReportDto>();
            foreach (var item in notes)
            {
                var reportEntity = new FinancialStatementNotesReportDto();
                reportEntity.NoteName = item.ItemName;
                reportEntity.CurrentExpense = item.Amount;

                reportEntities.Add(reportEntity);
            }
            #endregion

            #region Current year query
            decimal currentInventoryAmount = 0;
            decimal currentNetIncome = 0;
            decimal previousNetIncome = 0;
            //var currentInventory = (notes.Where(n => n.AccountHeadCode == "ClosingStock" && n.Year.Contains(fromDate.Year.ToString())).FirstOrDefault());
            var Income = reportEntities.Where(n => n.NoteName == "Net Income").FirstOrDefault();
            if (Income != null && Income.CurrentExpense != null && Income.CurrentExpense > 0)
                currentNetIncome = (decimal)Income.CurrentExpense;
            //if (currentInventory != null && currentInventory.Expense != null)
            //    currentInventoryAmount = (decimal)currentInventory.Expense * (-1);
            SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
            //string query = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0)))        CurrentYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            string query =
                "(select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, " +
                "IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, " +
                "IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, " +
                "case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount  " +
                "from Voucher v " +
                "left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) " +
                "left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) " +
                "left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) " +
                "left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) " +
                "where ag.Code not in (4,5) AND v.AccountHeadCode not like '10204%' AND v.VoucherDate < '" + ((DateTime)toDate).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' " +
                "group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code )" +
                "union ( select 'Asset' as AccGroupName, 'Current Asset' as AccSubGroupName, 'Inventory' as AccHeadGroupName, " +
                "'' as AccHeadSubGroupName, '1' as AccGroupCode, '102' as AccSubGroupCode, " +
                "'10204' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + currentInventoryAmount.ToString() + " as CurrentYearAmount )" +
                "union ( select 'Equity' as AccGroupName, 'Retained Earning' as AccSubGroupName, 'Retained Earning' as AccHeadGroupName, " +
                "'' as AccHeadSubGroupName, '3' as AccGroupCode, '3_' as AccSubGroupCode, " +
                "'' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + currentNetIncome.ToString() + " as CurrentYearAmount )" +
                "order by AccGroupName, AccSubGroupName, AccHeadGroupName, AccHeadSubGroupName, AccGroupCode, AccSubGroupCode, AccHeadGroupCode, AccHeadSubGroupCode";



            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dataTable = new DataTable();
            conn.Open();
            da.Fill(dataTable);
            conn.Close();
            #endregion

            DataTable dt = new DataTable();
            string y = "0";
            dt = GetCurrentYearDataTable(y, (DateTime)toDate);

            DataTable dtToMargeCurrent = new DataTable();

            int presentDtColNum = dataTable.Rows.Count;

            for (int i = 0; i < presentDtColNum; i++)
            {
                string presentAccGroupCode = dataTable.Rows[i]["AccGroupCode"].ToString();
                string presentAccSubGroupCode = dataTable.Rows[i]["AccSubGroupCode"].ToString();
                string presentAccHeadGroupCode = dataTable.Rows[i]["AccHeadGroupCode"].ToString();
                string presentAccHeadSubGroupCode = dataTable.Rows[i]["AccHeadSubGroupCode"].ToString();

                if (presentAccHeadSubGroupCode != "")
                {
                    dtToMargeCurrent = GetCurrentYearDataTable(presentAccHeadSubGroupCode, (DateTime)toDate);
                    dt.Merge(dtToMargeCurrent);
                }

                else if (presentAccHeadGroupCode != "")
                {
                    dtToMargeCurrent = GetCurrentYearDataTable(presentAccHeadGroupCode, (DateTime)toDate);
                    dt.Merge(dtToMargeCurrent);
                }

                else if (presentAccSubGroupCode != "")
                {
                    dtToMargeCurrent = GetCurrentYearDataTable(presentAccSubGroupCode, (DateTime)toDate);
                    dt.Merge(dtToMargeCurrent);
                }

            }

            #region Previous Year Query

            if (Income != null && Income.PreviousExpense != null && Income.PreviousExpense > 0)
                previousNetIncome = (decimal)Income.PreviousExpense;
            decimal previousInventoryAmount = 0;
            //var previousInventory = (notes.Where(n => n.AccountHeadCode == "ClosingStock" && n.Year.Contains(fromDate.AddYears(-1).Year.ToString())).FirstOrDefault());
            //if (previousInventory != null && previousInventory.Expense != null)
            //    previousInventoryAmount = (decimal)previousInventory.Expense * (-1);

            string querys =
                "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, " +
                "IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, " +
                "IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, " +
                "case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount " +
                "from Voucher v " +
                "left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) " +
                "left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) " +
                "left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) " +
                "left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) " +
                "where ag.Code not in (4,5) and  v.AccountHeadCode not like '10204%' and v.VoucherDate < '" + fromDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' " +//"'01/01/2016'"
                "group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code " +
                "union ( select 'Asset' as AccGroupName, 'Current Asset' as AccSubGroupName, 'Inventory' as AccHeadGroupName, " +
                "'' as AccHeadSubGroupName, '1' as AccGroupCode, '102' as AccSubGroupCode, " +
                "'10204' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + previousInventoryAmount.ToString() + " as CurrentYearAmount )" +
                "union ( select 'Equity' as AccGroupName, 'Retained Earning' as AccSubGroupName, 'Retained Earning' as AccHeadGroupName, " +
                "'' as AccHeadSubGroupName, '3' as AccGroupCode, '3_' as AccSubGroupCode, " +
                "'' as AccHeadGroupCode, '' as AccHeadSubGroupCode, " + previousNetIncome.ToString() + " as CurrentYearAmount )" +
                "order by AccGroupName, AccSubGroupName, AccHeadGroupName, AccHeadSubGroupName, AccGroupCode, AccSubGroupCode, AccHeadGroupCode, AccHeadSubGroupCode";

            SqlDataAdapter daa = new SqlDataAdapter(querys, conn);
            DataTable dataTablePreviousYear = new DataTable();
            conn.Open();
            daa.Fill(dataTablePreviousYear);
            conn.Close();
            #endregion

            DataTable dtForPrevious = new DataTable();
            dtForPrevious = GetPreviousYearDataTable(y, fromDate);

            DataTable dtToMargePrevious = new DataTable();

            int previousDtColNum = dataTablePreviousYear.Rows.Count;

            for (int j = 0; j < previousDtColNum; j++)
            {
                string previousAccGroupCode = dataTable.Rows[j]["AccGroupCode"].ToString();
                string previousAccSubGroupCode = dataTable.Rows[j]["AccSubGroupCode"].ToString();
                string previousAccHeadGroupCode = dataTable.Rows[j]["AccHeadGroupCode"].ToString();
                string previousAccHeadSubGroupCode = dataTable.Rows[j]["AccHeadSubGroupCode"].ToString();

                if (previousAccHeadSubGroupCode != "")
                {
                    dtToMargePrevious = GetPreviousYearDataTable(previousAccHeadSubGroupCode, fromDate);
                    dtForPrevious.Merge(dtToMargePrevious);
                }

                else if (previousAccHeadGroupCode != "")
                {
                    dtToMargePrevious = GetPreviousYearDataTable(previousAccHeadGroupCode, fromDate);
                    dtForPrevious.Merge(dtToMargePrevious);
                }

                else if (previousAccSubGroupCode != "")
                {
                    dtToMargePrevious = GetPreviousYearDataTable(previousAccSubGroupCode, fromDate);
                    dtForPrevious.Merge(dtToMargePrevious);
                }
            }
            // For Previous -----------------------------------------------------


            dt.Columns.Add("PreviousYearAmount");

            int previousDtColNumber = dtForPrevious.Rows.Count;
            for (int i = 0; i < previousDtColNumber; i++)
            {
                string previousAccHeadName = dtForPrevious.Rows[i]["AccHeadName"].ToString();
                string previousAccGroupCode = dtForPrevious.Rows[i]["AccGroupCode"].ToString();
                string previousAccSubGroupCode = dtForPrevious.Rows[i]["AccSubGroupCode"].ToString();
                string previousAccHeadGroupCode = dtForPrevious.Rows[i]["AccHeadGroupCode"].ToString();
                string previousAccHeadSubGroupCode = dtForPrevious.Rows[i]["AccHeadSubGroupCode"].ToString();



                int presentDtColNumber = dt.Rows.Count;

                for (int j = 0; j < presentDtColNumber; j++)
                {
                    string presentAccHeadName = dtForPrevious.Rows[i]["AccHeadName"].ToString();
                    string presentAccGroupCode = dt.Rows[j]["AccGroupCode"].ToString();
                    string presentAccSubGroupCode = dt.Rows[j]["AccSubGroupCode"].ToString();
                    string presentAccHeadGroupCode = dt.Rows[j]["AccHeadGroupCode"].ToString();
                    string presentAccHeadSubGroupCode = dt.Rows[j]["AccHeadSubGroupCode"].ToString();

                    string presentYearAmount = dt.Rows[j]["PreviousYearAmount"].ToString();
                    if (previousAccHeadName == presentAccHeadName && previousAccGroupCode == presentAccGroupCode && previousAccSubGroupCode == presentAccSubGroupCode && previousAccHeadGroupCode == presentAccHeadGroupCode && previousAccHeadSubGroupCode == presentAccHeadSubGroupCode)
                    {
                        dt.Rows[j]["PreviousYearAmount"] = dtForPrevious.Rows[i]["PreviousYearAmount"];
                        break;
                    }
                    //else
                    //{
                    //    dt.Rows[j]["PreviousYearAmount"] = "";
                    //}

                }

            }

            List<BalanceSheetDto> list = new List<BalanceSheetDto>();

            foreach (DataRow dr in dt.Rows)
            {


                decimal currentYearAmount = 0;
                decimal.TryParse(dr["CurrentYearAmount"].ToString(), out currentYearAmount);
                decimal previousYearAmount = 0;
                decimal.TryParse(dr["PreviousYearAmount"].ToString(), out previousYearAmount);

                BalanceSheetDto bdto = new BalanceSheetDto();
                bdto.AccHeadName = dr["AccHeadName"].ToString();
                bdto.AccGroupId = Convert.ToInt32(dr["AccGroupCode"]);
                bdto.AccSubGroupName = dr["AccSubGroupName"].ToString();
                bdto.AccHeadGroupName = dr["AccHeadGroupName"].ToString();
                bdto.AccGroupName = dr["AccGroupName"].ToString();
                bdto.AccHeadSubGroupName = dr["AccHeadSubGroupName"].ToString();
                bdto.CurrentYearAmount = currentYearAmount;
                bdto.PreviousYearAmount = previousYearAmount;
                //bdto.Debit = debit;
                //bdto.Credit = credit;
                list.Add(bdto);
            }


            return list;
        }


        public DataTable GetCurrentYearDataTable(string code, DateTime toDate)
        {
            SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
            //string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) CurrentYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, " +
                           "IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, " +
                           " IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) " +
                           "else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode " +
                           "left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) " +
                           "left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) " +
                           "left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) " +
                           "left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) " +
                           "where ag.Code not in (4,5) and v.AccountHeadCode LIKE '" + code + "%' " +
                           "AND  v.AccountHeadCode not like '10204%' AND v.VoucherDate < '" + ((DateTime)toDate).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' " +
                           "group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name " +
                           "order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dataTable = new DataTable();
            conn.Open();
            da.Fill(dataTable);
            conn.Close();
            return dataTable;
        }

        public DataTable GetPreviousYearDataTable(string code, DateTime toDate)
        {
            SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
            string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) " +
                "and v.AccountHeadCode LIKE '" + code + "%' AND  v.AccountHeadCode not like '10204%' AND v.VoucherDate < '" + ((DateTime)toDate).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "' " +
                "group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            //string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) PreviousYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            //CurrentYearAmount 
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            DataTable dataTable = new DataTable();
            conn.Open();
            da.Fill(dataTable);
            conn.Close();
            return dataTable;
        }
    }

    //public class BalanceSheetFacade : IBalanceSheetFacade
    //{
    //    private readonly ConncetionFacade _connection = new ConncetionFacade();
    //    private readonly GenService _service = new GenService();
    //    public List<BalanceSheetDto> GetAllBalanceSheet()
    //    {
    //        SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
    //        //string query = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) CurrentYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        string query = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount  from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            
    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dt = new DataTable();
    //        conn.Open();
    //        da.Fill(dt);
    //        conn.Close();

    //        //string qry = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) PreviousYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        string qry   = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";

    //        SqlDataAdapter daa = new SqlDataAdapter(qry, conn);
    //        DataTable dtt = new DataTable();
    //        conn.Open();
    //        daa.Fill(dtt);
    //        conn.Close();

    //        dt.Columns.Add("PreviousYearAmount");

    //        int previousDtColNum = dtt.Rows.Count;
    //        for (int i = 0; i < previousDtColNum; i++)
    //        {
    //            string previousAccGroupCode = dtt.Rows[i]["AccGroupCode"].ToString();
    //            string previousAccSubGroupCode = dtt.Rows[i]["AccSubGroupCode"].ToString();
    //            string previousAccHeadGroupCode = dtt.Rows[i]["AccHeadGroupCode"].ToString();
    //            string previousAccHeadSubGroupCode = dtt.Rows[i]["AccHeadSubGroupCode"].ToString();
                       


    //            int presentDtColNum = dt.Rows.Count;

    //            for (int j = 0; j < presentDtColNum; j++)
    //            {
    //                string presentAccGroupCode = dt.Rows[j]["AccGroupCode"].ToString();
    //                string presentAccSubGroupCode = dt.Rows[j]["AccSubGroupCode"].ToString();
    //                string presentAccHeadGroupCode = dt.Rows[j]["AccHeadGroupCode"].ToString();
    //                string presentAccHeadSubGroupCode = dt.Rows[j]["AccHeadSubGroupCode"].ToString();
                           
    //                string presentYearAmount = dt.Rows[j]["PreviousYearAmount"].ToString();
    //                if (previousAccGroupCode == presentAccGroupCode && previousAccSubGroupCode == presentAccSubGroupCode && previousAccHeadGroupCode == presentAccHeadGroupCode && previousAccHeadSubGroupCode == presentAccHeadSubGroupCode)
    //                {
    //                    dt.Rows[j]["PreviousYearAmount"] = dtt.Rows[i]["PreviousYearAmount"];
    //                    break;                      
    //                }
    //                //else
    //                //{
    //                //    dt.Rows[j]["PreviousYearAmount"] = "";
    //                //}

    //            }

    //        }

    //        List<BalanceSheetDto> list = new List<BalanceSheetDto>();

    //        foreach (DataRow dr in dt.Rows)
    //        {


    //            decimal currentYearAmount = 0;
    //            decimal.TryParse(dr["CurrentYearAmount"].ToString(), out currentYearAmount);
    //            decimal previousYearAmount = 0;
    //            decimal.TryParse(dr["PreviousYearAmount"].ToString(), out previousYearAmount);

    //            BalanceSheetDto bdto = new BalanceSheetDto();
    //            bdto.AccSubGroupName = dr["AccSubGroupName"].ToString();
    //            bdto.AccHeadGroupName = dr["AccHeadGroupName"].ToString();
    //            bdto.AccGroupName = dr["AccGroupName"].ToString();
    //            bdto.AccHeadSubGroupName = dr["AccHeadSubGroupName"].ToString();
    //            bdto.CurrentYearAmount = currentYearAmount;
    //            bdto.PreviousYearAmount = previousYearAmount;
    //            //bdto.Debit = debit;
    //            //bdto.Credit = credit;
    //            list.Add(bdto);
    //        }


    //        return list;
    //    }

    //    public List<BalanceSheetNotesDto> GetBalanceSheetNotes() 
    //    {
    //        List<BalanceSheetNotesDto> notes = new List<BalanceSheetNotesDto>();
    //        SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
    //        //string query = "SELECT t1.AccHeadCode as 'AccGroupId', t1.CurrentAdd AS 'CurrentAdd', t1.CurrentLess AS 'CurrentLess', ISNULL(t2.PreviousAdd,0) as 'PreviousAdd', ISNULL(t2.PreviousLess,0) as 'PreviousLess', ISNULL(t3.PreviousOpening,0) as 'PreviousOpening' FROM (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode', Sum(ISNULL(Debit,0)) as 'CurrentAdd', Sum(ISNULL(Credit,0)) as 'CurrentLess' FROM Voucher WHERE VoucherDate between DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) and  DATEADD(ms, -3, DATEADD(yy, DATEDIFF(yy,0,getdate()) + 1, 0)) GROUP BY SUBSTRING(AccountHeadCode,1,1)) t1 left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  Sum(ISNULL(Debit,0)) as 'PreviousAdd', Sum(ISNULL(Credit,0)) as 'PreviousLess' FROM Voucher WHERE VoucherDate < DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) GROUP BY SUBSTRING(AccountHeadCode,1,1)) t2 on t1.AccHeadCode = t2.AccHeadCode left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  (Sum(ISNULL(Debit,0)) - Sum(ISNULL(Credit,0))) as 'PreviousOpening' from Voucher where VoucherDate < DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) group by SUBSTRING(AccountHeadCode,1,1)) t3 on t1.AccHeadCode = t3.AccHeadCode";
    //        string query = "SELECT t1.AccHeadCode as 'AccGroupId', t1.CurrentAdd AS 'CurrentAdd', t1.CurrentLess AS 'CurrentLess', ISNULL(t2.PreviousAdd,0) as 'PreviousAdd', ISNULL(t2.PreviousLess,0) as 'PreviousLess', ISNULL(t3.PreviousOpening,0) as 'PreviousOpening' FROM (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode', case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Debit,0)) else Sum(ISNULL(Credit,0)) end as 'CurrentAdd', case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Credit,0)) else Sum(ISNULL(Debit,0)) end as 'CurrentLess' FROM Voucher WHERE VoucherDate between DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) and  DATEADD(ms, -3, DATEADD(yy, DATEDIFF(yy,0,getdate()) + 1, 0)) GROUP BY SUBSTRING(AccountHeadCode,1,1)) t1 left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Debit,0)) else Sum(ISNULL(Credit,0)) end as 'PreviousAdd', case when SUBSTRING(AccountHeadCode,1,1) = 1 or SUBSTRING(AccountHeadCode,1,1) = 5 then Sum(ISNULL(Credit,0)) else Sum(ISNULL(Debit,0)) end as 'PreviousLess' FROM Voucher WHERE VoucherDate < DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) GROUP BY SUBSTRING(AccountHeadCode,1,1)) t2 on t1.AccHeadCode = t2.AccHeadCode left join (SELECT SUBSTRING(AccountHeadCode,1,1) as 'AccHeadCode',  (Sum(ISNULL(Debit,0)) - Sum(ISNULL(Credit,0))) as 'PreviousOpening' from Voucher where VoucherDate < DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) group by SUBSTRING(AccountHeadCode,1,1)) t3 on t1.AccHeadCode = t3.AccHeadCode";

    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dataTable = new DataTable();
    //        conn.Open();
    //        da.Fill(dataTable);
    //        conn.Close();
    //        for (int i = 0; i < dataTable.Rows.Count; i++ )
    //        {
    //            var item = new BalanceSheetNotesDto();
    //            item.AccGroupId = Convert.ToInt64(dataTable.Rows[i]["AccGroupId"]);
    //            item.CurrentAdd = (decimal?)dataTable.Rows[i]["CurrentAdd"];
    //            item.CurrentLess = (decimal?)dataTable.Rows[i]["CurrentLess"];
    //            item.PreviousAdd = (decimal?)dataTable.Rows[i]["PreviousAdd"];
    //            item.PreviousLess = (decimal?)dataTable.Rows[i]["PreviousLess"];
    //            item.PreviousOpening = (decimal?)dataTable.Rows[i]["PreviousOpening"];

    //            notes.Add(item);

    //        }
    //        return notes;
    //    }
    //    public List<BalanceSheetDto> GetAllBalanceSheetDetails()
    //    {
    //        // For Present -----------------------------------------------------
    //        SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
    //        //string query = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0)))        CurrentYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        string query = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            


    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dataTable = new DataTable();
    //        conn.Open();
    //        da.Fill(dataTable);
    //        conn.Close();

    //        DataTable dt = new DataTable();
    //        string y = "0";
    //        dt = GetCurrentYearDataTable(y);

    //        DataTable dtToMargeCurrent = new DataTable();

    //        int presentDtColNum = dataTable.Rows.Count;

    //        for (int i = 0; i < presentDtColNum; i++)
    //        {
    //            string presentAccGroupCode = dataTable.Rows[i]["AccGroupCode"].ToString();
    //            string presentAccSubGroupCode = dataTable.Rows[i]["AccSubGroupCode"].ToString();
    //            string presentAccHeadGroupCode = dataTable.Rows[i]["AccHeadGroupCode"].ToString();
    //            string presentAccHeadSubGroupCode = dataTable.Rows[i]["AccHeadSubGroupCode"].ToString();

    //            if (presentAccHeadSubGroupCode != "")
    //            {
    //                dtToMargeCurrent = GetCurrentYearDataTable(presentAccHeadSubGroupCode);
    //                dt.Merge(dtToMargeCurrent);
    //            }

    //            else if (presentAccHeadGroupCode != "")
    //            {
    //                dtToMargeCurrent = GetCurrentYearDataTable(presentAccHeadGroupCode);
    //                dt.Merge(dtToMargeCurrent);
    //            }

    //            else if (presentAccSubGroupCode != "")
    //            {
    //                dtToMargeCurrent = GetCurrentYearDataTable(presentAccSubGroupCode);
    //                dt.Merge(dtToMargeCurrent);
    //            }

    //        }
    //        // For Present -----------------------------------------------------

    //        // For Previous -----------------------------------------------------
    //        //string querys = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) PreviousYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        string querys = "select ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount from Voucher v left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";

    //        SqlDataAdapter daa = new SqlDataAdapter(querys, conn);
    //        DataTable dataTablePreviousYear = new DataTable();
    //        conn.Open();
    //        daa.Fill(dataTablePreviousYear);
    //        conn.Close();

    //        DataTable dtForPrevious = new DataTable();
    //        dtForPrevious = GetPreviousYearDataTable(y);

    //        DataTable dtToMargePrevious = new DataTable();

    //        int previousDtColNum = dataTablePreviousYear.Rows.Count;

    //        for (int j = 0; j < previousDtColNum; j++)
    //        {
    //            string previousAccGroupCode = dataTable.Rows[j]["AccGroupCode"].ToString();
    //            string previousAccSubGroupCode = dataTable.Rows[j]["AccSubGroupCode"].ToString();
    //            string previousAccHeadGroupCode = dataTable.Rows[j]["AccHeadGroupCode"].ToString();
    //            string previousAccHeadSubGroupCode = dataTable.Rows[j]["AccHeadSubGroupCode"].ToString();

    //            if (previousAccHeadSubGroupCode != "")
    //            {
    //                dtToMargePrevious = GetPreviousYearDataTable(previousAccHeadSubGroupCode);
    //                dtForPrevious.Merge(dtToMargePrevious);
    //            }

    //            else if (previousAccHeadGroupCode != "")
    //            {
    //                dtToMargePrevious = GetPreviousYearDataTable(previousAccHeadGroupCode);
    //                dtForPrevious.Merge(dtToMargePrevious);
    //            }

    //            else if (previousAccSubGroupCode != "")
    //            {
    //                dtToMargePrevious = GetPreviousYearDataTable(previousAccSubGroupCode);
    //                dtForPrevious.Merge(dtToMargePrevious);
    //            }
    //        }
    //        // For Previous -----------------------------------------------------


    //        dt.Columns.Add("PreviousYearAmount");

    //        int previousDtColNumber = dtForPrevious.Rows.Count;
    //        for (int i = 0; i < previousDtColNumber; i++)
    //        {
    //            string previousAccHeadName = dtForPrevious.Rows[i]["AccHeadName"].ToString();
    //            string previousAccGroupCode = dtForPrevious.Rows[i]["AccGroupCode"].ToString();
    //            string previousAccSubGroupCode = dtForPrevious.Rows[i]["AccSubGroupCode"].ToString();
    //            string previousAccHeadGroupCode = dtForPrevious.Rows[i]["AccHeadGroupCode"].ToString();
    //            string previousAccHeadSubGroupCode = dtForPrevious.Rows[i]["AccHeadSubGroupCode"].ToString();



    //            int presentDtColNumber = dt.Rows.Count;

    //            for (int j = 0; j < presentDtColNumber; j++)
    //            {
    //                string presentAccHeadName = dtForPrevious.Rows[i]["AccHeadName"].ToString();
    //                string presentAccGroupCode = dt.Rows[j]["AccGroupCode"].ToString();
    //                string presentAccSubGroupCode = dt.Rows[j]["AccSubGroupCode"].ToString();
    //                string presentAccHeadGroupCode = dt.Rows[j]["AccHeadGroupCode"].ToString();
    //                string presentAccHeadSubGroupCode = dt.Rows[j]["AccHeadSubGroupCode"].ToString();

    //                string presentYearAmount = dt.Rows[j]["PreviousYearAmount"].ToString();
    //                if (previousAccHeadName == presentAccHeadName && previousAccGroupCode == presentAccGroupCode && previousAccSubGroupCode == presentAccSubGroupCode && previousAccHeadGroupCode == presentAccHeadGroupCode && previousAccHeadSubGroupCode == presentAccHeadSubGroupCode)
    //                {
    //                    dt.Rows[j]["PreviousYearAmount"] = dtForPrevious.Rows[i]["PreviousYearAmount"];
    //                    break;
    //                }
    //                //else
    //                //{
    //                //    dt.Rows[j]["PreviousYearAmount"] = "";
    //                //}

    //            }

    //        }

    //        List<BalanceSheetDto> list = new List<BalanceSheetDto>();

    //        foreach (DataRow dr in dt.Rows)
    //        {


    //            decimal currentYearAmount = 0;
    //            decimal.TryParse(dr["CurrentYearAmount"].ToString(), out currentYearAmount);
    //            decimal previousYearAmount = 0;
    //            decimal.TryParse(dr["PreviousYearAmount"].ToString(), out previousYearAmount);

    //            BalanceSheetDto bdto = new BalanceSheetDto();
    //            bdto.AccHeadName = dr["AccHeadName"].ToString();
    //            bdto.AccGroupId = Convert.ToInt32(dr["AccGroupCode"]);
    //            bdto.AccSubGroupName = dr["AccSubGroupName"].ToString();
    //            bdto.AccHeadGroupName = dr["AccHeadGroupName"].ToString();
    //            bdto.AccGroupName = dr["AccGroupName"].ToString();
    //            bdto.AccHeadSubGroupName = dr["AccHeadSubGroupName"].ToString();
    //            bdto.CurrentYearAmount = currentYearAmount;
    //            bdto.PreviousYearAmount = previousYearAmount;
    //            //bdto.Debit = debit;
    //            //bdto.Credit = credit;
    //            list.Add(bdto);
    //        }


    //        return list;
    //    }


    //    public DataTable GetCurrentYearDataTable(string code)
    //    {
    //        SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
    //        //string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) CurrentYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as CurrentYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
            
    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dataTable = new DataTable();
    //        conn.Open();
    //        da.Fill(dataTable);
    //        conn.Close();
    //        return dataTable;
    //    }

    //    public DataTable GetPreviousYearDataTable(string code)
    //    {
    //        SqlConnection conn = new SqlConnection(_connection.GetConnectionString());
    //        string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, case when ag.Code = '1' or ag.Code = '5' then (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) else (sum(IsNull(v.credit,0)) - sum(IsNull(v.debit,0))) end as PreviousYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        //string query = "select ah.Name AccHeadName, ag.Name as AccGroupName, IsNull(asg.Name,'') as AccSubGroupName, IsNull(ahg.Name,'') as AccHeadGroupName, IsNull(ahsg.Name,'') as AccHeadSubGroupName, ag.Code as AccGroupCode, IsNull(asg.Code,'') as AccSubGroupCode, IsNull(ahg.Code,'') as AccHeadGroupCode, IsNull(ahsg.Code,'') as AccHeadSubGroupCode, (sum(IsNull(v.debit,0)) - sum(IsNull(v.credit,0))) PreviousYearAmount from Voucher v left join AccHead ah on ah.Code=v.AccountHeadCode left join AccGroup ag on ag.Code=SUBSTRING(v.AccountHeadCode,1,1) left join AccSubGroup asg on asg.Code=SUBSTRING(v.AccountHeadCode,1,3) left join AccHeadGroup ahg on ahg.Code=SUBSTRING(v.AccountHeadCode,1,5) left join AccHeadSubGroup ahsg on ahsg.Code=SUBSTRING(v.AccountHeadCode,1,7) where ag.Code not in (4,5) and v.VoucherDate<'12-31-2015' and v.AccountHeadCode LIKE '" + code + "%' group by ahsg.Name,ahsg.Code,ahg.name,ahg.code,asg.name, asg.code,ag.name, ag.Code,ah.Name order by ag.Name, asg.Name, ahg.Name, ahsg.Name, ag.Code, asg.Code, ahg.Code, ahsg.Code";
    //        //CurrentYearAmount 
    //        SqlDataAdapter da = new SqlDataAdapter(query, conn);
    //        DataTable dataTable = new DataTable();
    //        conn.Open();
    //        da.Fill(dataTable);
    //        conn.Close();
    //        return dataTable;
    //    }
    //}
}
