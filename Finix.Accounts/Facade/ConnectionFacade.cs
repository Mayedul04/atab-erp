﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Facade
{
    public class ConncetionFacade
    {
        public string GetConnectionString()
        {
            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["FinixAccountsContext"].ToString();
            return conString;
        }
    }
}
