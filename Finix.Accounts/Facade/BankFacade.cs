﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Accounts.Service;
using Finix.Accounts.Infrastructure;
using System;
using System.Transactions;

namespace Finix.Accounts.Facade
{
    public class BankFacade : BaseFacade
    {

        /* Developed by - Siddique
         * Developed on - 10 March 2016
         * Objective    - Get all active bank list
         */
        public List<BankDto> GetAllBanks(long? CompanyProfileId)
        {
            var data = GenService.GetAll<Bank>().Where(b => b.Status == EntityStatus.Active);
            if (CompanyProfileId != null && CompanyProfileId > 0)
                data = data.Where(d => d.CompanyProfileId == CompanyProfileId);
            return Mapper.Map<List<BankDto>>(data.ToList());
        }
        public long SaveBank(BankDto model)
        {
            var entity = new Bank();
            if (model.Id == 0)
            {
                entity = Mapper.Map<Bank>(model);
                entity.CreateDate = DateTime.Now;
            }
            else
            {
                entity = GenService.GetById<Bank>(model.Id);
                model.CreateDate = entity.CreateDate;
                model.CreatedBy = entity.CreatedBy;

                entity = Mapper.Map(model, entity);
                entity.EditDate = DateTime.Now;
            }
            using (var tran = new TransactionScope())
            {
                entity.Status = EntityStatus.Active;
                GenService.Save(entity);
                tran.Complete();
            }
            return entity.Id;
        }
        public long SaveChequeRegister(ChequeRegisterDto chequeRegister, long userId)
        {
            var entity = new ChequeRegister();
            if (chequeRegister.Id != null && chequeRegister.Id > 0)
            {
                entity = GenService.GetById<ChequeRegister>((long)chequeRegister.Id);
                chequeRegister.CreateDate = entity.CreateDate;
                chequeRegister.CreatedBy = entity.CreatedBy;

                entity = Mapper.Map(chequeRegister, entity);
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
            }
            else
            {
                entity.Details = new List<ChequeRegisterDetail>();
                long ChequeSequence = 0;
                try
                {
                    ChequeSequence = Convert.ToInt64(chequeRegister.StartingNumber);
                }
                catch (Exception)
                {
                    return 0;
                }
                entity = Mapper.Map<ChequeRegister>(chequeRegister);
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                int chequeNoLength = chequeRegister.StartingNumber.Length;
                
                for (int i = 0; i < chequeRegister.TotalCheques; i++)
                {
                    var detail = new ChequeRegisterDetail();
                    detail.ChequeNo = chequeRegister.ChequePrefix + ChequeSequence.ToString("D" + chequeNoLength.ToString());
                    ChequeSequence++;
                    detail.CreateDate = entity.CreateDate;
                    detail.CreatedBy = userId;
                    detail.ChequeStatus = ChequeStatus.UnUsed;

                    entity.Details.Add(detail);
                }
            }
            try
            {
                entity.Status = EntityStatus.Active;
                GenService.Save(entity);
                return entity.Id;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public List<ChequeRegisterDetailDto> GetAvailableCheques(long BankId, string ChequeNo)
        {
            var chequeList = GenService.GetAll<ChequeRegisterDetail>().Where(c => c.ChequeStatus == ChequeStatus.UnUsed && c.ChequeRegister.BankId == BankId);
            if (!string.IsNullOrEmpty(ChequeNo))
                chequeList = chequeList.Where(c => c.ChequeNo.StartsWith(ChequeNo));

            return Mapper.Map<List<ChequeRegisterDetailDto>>(chequeList.ToList());
        }
        public List<ChequeBookStatusSummaryDto> GetChequeBookStatusSummary(long? CompanyProfileId, long? BankId, ChequeStatus? chequeStatus)
        {
            var chequeBooks = GenService.GetAll<ChequeRegister>();
            if (CompanyProfileId != null && CompanyProfileId > 0)
                chequeBooks = chequeBooks.Where(c => c.CompanyProfileId == CompanyProfileId);
            if (BankId != null && BankId > 0)
                chequeBooks = chequeBooks.Where(c => c.BankId == BankId);
            //if (ChequeStatus != null && ChequeStatus > 0)
            //    cheques = cheques.Where(c => c.ChequeStatus == ChequeStatus);

            var summary = (from cheque in chequeBooks
                           select new ChequeBookStatusSummaryDto
                           {
                               BankId = cheque.BankId,
                               BankName = cheque.Bank.Name,
                               BranchName = cheque.Bank.BranchName,
                               BouncedCount = cheque.Details.Where(cd => cd.ChequeStatus == ChequeStatus.Bounced).Count(),
                               ChequeStartingNumber = cheque.ChequePrefix + cheque.StartingNumber,
                               CompanyProfileId = cheque.CompanyProfileId,
                               //CompanyProfileName = cheque.CompanyProfile.Name,
                               IssuedCount = cheque.Details.Where(cd => cd.ChequeStatus == ChequeStatus.Issued).Count(),
                               RuinedCount = cheque.Details.Where(cd => cd.ChequeStatus == ChequeStatus.Ruined).Count(),
                               UnUsedCount = cheque.Details.Where(cd => cd.ChequeStatus == ChequeStatus.UnUsed).Count(),
                               TotalLeafs = (int)cheque.TotalCheques
                           }).ToList();
            return summary;
        }
        public List<ChequeRegisterDto> GetChequeRegisterList()
        {
            var data = GenService.GetAll<ChequeRegister>().Where(b => b.Status == EntityStatus.Active);
            return Mapper.Map<List<ChequeRegisterDto>>(data.ToList());
        }
        public ChequeRegisterDetailDto GetCheque(string ChequeNo, long? Id)
        {
            var result = new ChequeRegisterDetailDto();
            if (Id != null)
            {
                var data = GenService.GetAll<ChequeRegisterDetail>().Where(c=>c.Id == Id && c.ChequeStatus == ChequeStatus.Issued).FirstOrDefault();
                if (data != null)
                    result = Mapper.Map<ChequeRegisterDetailDto>(data);
            }
            else if (!string.IsNullOrEmpty(ChequeNo))
            {
                var data = GenService.GetAll<ChequeRegisterDetail>().Where(c => c.ChequeNo == ChequeNo && c.ChequeStatus == ChequeStatus.Issued).FirstOrDefault();
                if (data != null)
                    result = Mapper.Map<ChequeRegisterDetailDto>(data);
            }
            return result;
        }
    }
}
