﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Finix.Accounts.Dto;
using Finix.Accounts.DTO;
using Finix.Accounts.Facade.AutoMaps;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Infrastructure.Models;
using Finix.Accounts.Service;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using PagedList;

namespace Finix.Accounts.Facade
{
    public class AccountsFacade : BaseFacade
    {
        //private readonly GenService GenService = new GenService();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        private readonly UserFacade _user = new UserFacade();
        
        //public AccountsFacade(CompanyProfileFacade companyProfileFacade)
        //{
        //    this._companyProfileFacade = companyProfileFacade;
        //}
        public IEnumerable<AccHeadDto> GetAccountHeadsByAccountGroupCode(string accountGroupCode)
        {
            var accHeads = GenService.GetAll<AccHeadMain>()
                .Where(r => r.Code.StartsWith(accountGroupCode)
                    && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads);
        }
        public IEnumerable<AccHeadDto> GetAccountHeadsByAccountGroupCodeAndOfficeId(string accountGroupCode, long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHeads = GenService.GetAll<AccHead>()
                .Where(r => r.Code.StartsWith(accountGroupCode)
                    && r.CompanyProfileId == companyProfileId
                    && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads);
        }
        public List<AccHeadDto> GetAllAccHeads(long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHeads = GenService.GetAll<AccHead>()
                .Where(r => r.CompanyProfileId == companyProfileId
                    && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name).ToList();
            return Mapper.Map<List<AccHeadDto>>(accHeads);
        }
        public List<AccHeadGroupDto> GetAllAccHeadGroup()
        {
            var accHeadGroup = GenService.GetAll<AccHeadGroup>().ToList();
            var temp = (from subGroup in accHeadGroup
                        select new AccHeadGroupDto()
                        {
                            Id = subGroup.Id,
                            Name = subGroup.AccSubGroup.AccGroup.Name + " - " + subGroup.AccSubGroup.Name + " - " + subGroup.Name,
                            Code = subGroup.Code,
                            AccSubGroupId = subGroup.AccSubGroupId
                        }).OrderBy(s => s.Name).ToList();
            return temp;
        }
        public List<AccSubGroupDto> GetAllAccSubGroup()
        {
            var accHeadGroup = GenService.GetAll<AccSubGroup>().ToList();
            var temp = (from subGroup in accHeadGroup
                        select new AccSubGroupDto()
                        {
                            Id = subGroup.Id,
                            Name = subGroup.AccGroup.Name + " - " + subGroup.Name,
                            Code = subGroup.Code,
                            AccGroupId = subGroup.AccGroupId
                        }).OrderBy(s => s.Name).ToList();
            return temp;
        }
        //public List<AccHeadDto> GetAllAccHeadGroup()
        //{
        //    var accHeadGroup = GenService.GetAll<AccHeadGroup>().ToList();
        //    return Mapper.Map<List<AccHeadDto>>(accHeadGroup);
        //}
        public IEnumerable<AccGroupDto> GetAllAccGroups()
        {
            var accGroups = GenService.GetAll<AccGroup>().OrderBy(s => s.Name).ToList();
            var test = Mapper.Map<List<AccGroupDto>>(accGroups);
            return Mapper.Map<List<AccGroupDto>>(accGroups);
        }
        public IEnumerable<AccSubGroupDto> GetAccSubGroupsByAccGroupId(int accGroupId)
        {
            var accSubGroups = GenService.GetAll<AccSubGroup>()
                .Where(r => r.AccGroupId == accGroupId)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccSubGroupDto>>(accSubGroups);
        }
        public IEnumerable<AccHeadGroupDto> GetAccHeadGroupsByAccSubGroupId(int accSubGroupId)
        {
            var accHeadGroups = GenService.GetAll<AccHeadGroup>()
                .Where(r => r.AccSubGroupId == accSubGroupId)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadGroupDto>>(accHeadGroups);
        }
        public IEnumerable<AccHeadSubGroupDto> GetAccHeadSubGroupsByAccHeadGroupId(int accHeadGroupId)
        {
            var accHeadSubGroups = GenService.GetAll<AccHeadSubGroup>()
                .Where(r => r.AccHeadGroupId == accHeadGroupId)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadSubGroupDto>>(accHeadSubGroups);
        }
        public IEnumerable<AccHeadDto> GetAccHeadsByAccHeadSubGroupId(int accHeadSubGroupId, long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHeads = GenService.GetAll<AccHead>()
                .Where(r => r.AccHeadSubGroupId == accHeadSubGroupId && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads);
        }
        public IEnumerable<AccHeadDto> GetAccHeadsByRefType(int RefType, long? companyProfileId)
        {
            ReferenceAccountType refType = ReferenceAccountType.Bank;
            if (RefType == 2 || RefType < 0)
                refType = ReferenceAccountType.Bank;

            var accHeads = GenService.GetAll<AccHead>().Where(r => r.RefType == refType && r.Status == EntityStatus.Active);

            if (companyProfileId != null && companyProfileId > 0)
                accHeads = accHeads.Where(r => r.CompanyProfileId == companyProfileId);

            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads.OrderBy(r => r.Name).ToList());
        }
        public IEnumerable<AccHeadDto> GetFixedAssetAccHeads(long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHeads = GenService.GetAll<AccHead>()
                .Where(r => r.Code.StartsWith("101")
                    && r.CompanyProfileId == companyProfileId
                    && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name).ToList();
            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads);
        }
        public AccHeadDto GetAccHeadByAccHeadId(long accHeadId)
        {
            var accHead = GenService.GetById<AccHeadMain>(accHeadId);
            return Mapper.Map<AccHeadDto>(accHead);
        }
        public AccHeadDto GetAccHeadByRefIdAndRefType(long refId, ReferenceAccountType refType, long? CompanyProfileId)
        {
            var accHead = GenService.GetAll<AccHead>().Where(ac => ac.RefId == refId && ac.RefType == refType);
            if (CompanyProfileId != null && CompanyProfileId > 0)
                accHead.Where(ac => ac.CompanyProfileId == CompanyProfileId);
            return Mapper.Map<AccHeadDto>(accHead.FirstOrDefault());
        }
        public IEnumerable<AccHeadDto> GetAccHeadsForAssetExpenditure(long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHeads = GenService.GetAll<AccHead>()
                .Where(r => r.CompanyProfileId == companyProfileId && (r.Code.StartsWith("1") || r.Code.StartsWith("5")) && r.Status == EntityStatus.Active)
                .OrderBy(s => s.Name)
                .ToList();
            return Mapper.Map<IEnumerable<AccHeadDto>>(accHeads);
        }
        public bool SaveAccHead(AccHeadDto model)
        {
            var entity = new AccHeadMain();
            var oldMappedAccHeads = new List<AccHead>();
            if (model.CompanyProfileId == null)
                model.CompanyProfileId = 1;
            if (model.Id == 0 && (model.Status == null || model.Status != EntityStatus.Deleted))
            {
                model.CreateDate = DateTime.Now;
                model.CreatedBy = 1;
                entity = Mapper.Map<AccHeadMain>(model);
                entity.Status = EntityStatus.Active;
            }
            else if (model.Status == null || model.Status != EntityStatus.Deleted)
            {
                entity = GenService.GetById<AccHeadMain>(model.Id);
                model.CreateDate = entity.CreateDate;
                if (entity.CreatedBy != null)
                    model.CreatedBy = (long)entity.CreatedBy;

                //entity = GenService.GetById<AccHead>(model.Id);
                if (!string.IsNullOrEmpty(model.Name))
                    entity.Name = model.Name;
                if (model.RefType != null && model.RefId != null && model.RefId > 0)
                {
                    entity.RefType = model.RefType;
                    entity.RefId = model.RefId;
                }
                oldMappedAccHeads = GenService.GetAll<AccHead>().Where(a => a.AccHeadMainId == model.Id).ToList();
                oldMappedAccHeads.ForEach(o =>
                {
                    o.Code = entity.Code;
                    o.Name = entity.Name;
                    o.RefId = entity.RefId;
                    o.RefType = entity.RefType;
                    o.AccGroupId = entity.AccGroupId;
                    o.AccSubGroupId = entity.AccSubGroupId;
                    o.AccHeadGroupId = entity.AccHeadGroupId;
                    o.AccHeadSubGroupId = entity.AccHeadSubGroupId;
                });
                entity.EditDate = DateTime.Now;
                entity.Status = EntityStatus.Active;
            }
            else if (model.Status == EntityStatus.Deleted || model.Status == EntityStatus.Inactive)
            {
                entity = GenService.GetById<AccHeadMain>(model.Id);
                model.CreateDate = entity.CreateDate;
                if (entity.CreatedBy != null)
                    model.CreatedBy = (long)entity.CreatedBy;

                entity.EditDate = DateTime.Now;
                entity.Status = (EntityStatus)model.Status;
            }
            try
            {
                //entity.Status = EntityStatus.Active;
                GenService.Save(entity);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //public bool SaveAccHeadMapping(AccHeadDto model)
        //{
        //    try
        //    {
        //        var accHead = GenService.GetById<AccHead>(model.Id);
        //        if (!string.IsNullOrEmpty(model.Name))
        //            accHead.Name = model.Name;
        //        if (model.RefType != null && model.RefId != null && model.RefId > 0)
        //        {
        //            accHead.RefType = model.RefType;
        //            accHead.RefId = model.RefId;
        //        }
        //        GenService.Save(accHead);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //public bool SaveJournalVoucher(AccountDto accountDto)
        //{
        //    var voucherList = new List<Voucher>();
        //    try
        //    {
        //        int i = 1;
        //        var vdList = new List<VoucherDto>();
        //        foreach (var vd in accountDto.VoucherDetails)
        //        {
        //            vd.SerialNo = i++;
        //            //todo - revise the following condition
        //            if (vd.CompanyProfileId == null)
        //                vd.CompanyProfileId = 1;
        //            vd.TransactionNo = accountDto.VoucherNo;
        //            vd.VoucherDate = vd.VoucherDate;//DateTime.Now.Date; //companyProfile.SystemDate;
        //            vdList.Add(vd);
        //        }
        //        accountDto.VoucherDetails = vdList;
        //        var chequeList = new List<ChequeRegisterDetail>();
        //        using (var tran = new TransactionScope())
        //        {
        //            foreach (var vd in accountDto.VoucherDetails)
        //            {
        //                vd.Status = 1;
        //                var entity = Mapper.Map<Voucher>(vd);

        //                voucherList.Add(entity);

        //                if (vd.IsCheque)
        //                {
        //                    var cheque = GenService.GetAll<ChequeRegisterDetail>()
        //                        .Where(c => c.ChequeNo == vd.ChequeNo
        //                            && c.ChequeRegister.BankId == vd.BankId
        //                            && c.ChequeStatus == ChequeStatus.UnUsed).FirstOrDefault();
        //                    if (cheque == null)
        //                        return false;

        //                    cheque.AccHeadCode = vd.AccountHeadCode;
        //                    cheque.Amount = vd.Amount;
        //                    cheque.ChequeStatus = ChequeStatus.Issued;
        //                    cheque.EditDate = DateTime.Now;
        //                    cheque.IssueDate = DateTime.Now;
        //                    cheque.VoucherNo = vd.VoucherNo;

        //                    chequeList.Add(cheque);
        //                }
        //            }

        //            GenService.Save(voucherList);
        //            GenService.Save(chequeList);
        //            //var cp = _companyProfileService.GetCompanyProfileById(1);

        //            //if (model.VoucherType == "rv")
        //            //    cp.RvVoucherNo = model.VoucherNo;
        //            //else
        //            //    cp.PvVoucherNo = model.VoucherNo;

        //            //_companyProfileService.UpdateCompanyProfile(cp);

        //            tran.Complete();

        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
        #region voucher
        public ResponseDto SaveJournalVoucher(AccountDto accountDto, long UserId)
        {
            ResponseDto response = new ResponseDto();
            var voucherList = new List<Voucher>();
            try
            {
                int i = 1;
                var vdList = new List<VoucherDto>();
                foreach (var vd in accountDto.VoucherDetails)
                {
                    vd.SerialNo = i++;
                    vd.TransactionNo = accountDto.VoucherNo;
                    vd.VoucherDate = vd.VoucherDate;
                    vdList.Add(vd);
                }
                accountDto.VoucherDetails = vdList;
                var chequeList = new List<ChequeRegisterDetail>();
                using (var tran = new TransactionScope())
                {
                    foreach (var vd in accountDto.VoucherDetails)
                    {
                        vd.Status = 1;
                        var entity = Mapper.Map<Voucher>(vd);
                        entity.RecordStatus = RecordStatus.Authorized;
                        entity.CreateDate = DateTime.Now;
                        entity.CreatedBy = UserId;
                        entity.Status = EntityStatus.Active;

                        var log = new VoucherLog();
                        log.CreateDate = DateTime.Now;
                        log.CreatedBy = UserId;
                        log.Status = EntityStatus.Active;
                        log.ToRecordStatus = RecordStatus.Authorized;

                        GenService.Save(entity);
                        log.VoucherId = entity.Id;
                        GenService.Save(log);

                        //voucherList.Add(entity);

                        if (vd.IsCheque)
                        {
                            var cheque = GenService.GetAll<ChequeRegisterDetail>()
                                .Where(c => c.ChequeNo == vd.ChequeNo
                                    && c.ChequeRegister.BankId == vd.BankId
                                    && c.ChequeStatus == ChequeStatus.UnUsed).FirstOrDefault();
                            if (cheque == null)
                            {
                                response.Message = "Please Provide Cheque Information";
                                return response;
                            }


                            cheque.AccHeadCode = vd.AccountHeadCode;
                            cheque.Amount = vd.Amount;
                            cheque.ChequeStatus = ChequeStatus.Issued;
                            cheque.EditDate = DateTime.Now;
                            cheque.IssueDate = DateTime.Now;
                            cheque.VoucherNo = vd.VoucherNo;

                            //chequeList.Add(cheque);
                            GenService.Save(cheque);
                        }
                    }

                    //GenService.Save(voucherList);
                    //GenService.Save(chequeList);
                    //var cp = _companyProfileService.GetCompanyProfileById(1);

                    //if (model.VoucherType == "rv")
                    //    cp.RvVoucherNo = model.VoucherNo;
                    //else
                    //    cp.PvVoucherNo = model.VoucherNo;

                    //_companyProfileService.UpdateCompanyProfile(cp);

                    tran.Complete();

                }
                response.Success = true;
                response.Message = "Journal Voucher Saved Successfully";
                return response;
            }
            catch
            {
                response.Message = "Journal Voucher Save Failed";
                return response;
            }
        }
        public ResponseDto SaveVoucher(AccountDto accountDto, long userId)
        {
            ResponseDto response = new ResponseDto();
            //List<Voucher> voucherList = new List<Voucher>();
            int i = 1;
            int tranNo = 1;
            var vdList = new List<VoucherDto>();
            var chequeList = new List<ChequeRegisterDetail>();
            var systemDate = _companyProfileFacade.DateToday(accountDto.VoucherDetails.FirstOrDefault().CompanyProfileId).Date;
            using (var tran = new TransactionScope())
            {
                try
                {
                    foreach (var vd in accountDto.VoucherDetails)
                    {
                        vd.SerialNo = i++;
                        vd.TransactionNo = tranNo.ToString();
                        vd.VoucherDate = vd.VoucherDate;//DateTime.Now; //companyProfile.SystemDate;
                                                        //todo - revise the following condition
                        if (vd.CompanyProfileId == null)
                            vd.CompanyProfileId = 1;
                        if (vd.VoucherDate != systemDate) 
                        {
                            response.Message = "Vouchers not saved. Voucher date doesn't match current system date. Please contact administrator.";
                            return response;
                        }

                        if (vd.IsCheque)
                        {
                            var cheque = GenService.GetAll<ChequeRegisterDetail>()
                                .Where(c => c.ChequeNo == vd.ChequeNo
                                    && c.ChequeRegister.BankId == vd.BankId
                                    && c.ChequeStatus == ChequeStatus.UnUsed).FirstOrDefault();
                            if (cheque == null)
                            {
                                response.Message = "Please Provide Cheque Information";
                                return response;
                            }

                            cheque.AccHeadCode = vd.AccountHeadCode;
                            cheque.Amount = vd.Amount;
                            cheque.ChequeStatus = ChequeStatus.Issued;
                            cheque.PaidFor = vd.Description;

                            cheque.IssueDate = systemDate;
                            cheque.IssuedBy = userId;
                            cheque.VoucherNo = vd.VoucherNo;
                            cheque.EditDate = DateTime.Now;
                            cheque.EditedBy = userId;
                            cheque.Status = EntityStatus.Active;
                            cheque.PaidTo = vd.ChequePayTo;

                            chequeList.Add(cheque);
                            response.Id = cheque.Id;
                            //GenService.Save(cheque);
                        }


                        vdList.Add(vd);
                        if (vd.AccTranType == AccTranType.Payment || vd.AccTranType == AccTranType.Receive)
                        {
                            VoucherDto oppositeVoucher = vdList.Where(v => v.AccountHeadCode == (vd.PaymentType == 2 ? vd.BankAccountHead : BizConstants.CashInHand)).FirstOrDefault();
                            if(oppositeVoucher == null)
                            {
                                oppositeVoucher = new VoucherDto();
                                oppositeVoucher.SerialNo = i++;
                                oppositeVoucher.TransactionNo = tranNo.ToString();
                                oppositeVoucher.VoucherDate = vd.VoucherDate;
                                oppositeVoucher.CompanyProfileId = vd.CompanyProfileId;
                                oppositeVoucher.CreateDate = DateTime.Now;

                                oppositeVoucher.Description = vd.Description;
                                oppositeVoucher.VoucherNo = accountDto.VoucherNo;
                                oppositeVoucher.AccTranType = vd.AccTranType;

                                if (vd.PaymentType == 2)//2 for bank
                                {
                                    oppositeVoucher.AccountHeadCode = vd.BankAccountHead;
                                    oppositeVoucher.IsCheque = true;
                                    oppositeVoucher.ChequeDate = vd.ChequeDate;
                                }
                                else    // for cash
                                {
                                    oppositeVoucher.AccountHeadCode = (vd.CompanyProfileId==2?BizConstants.CashInHandATTI: BizConstants.CashInHand); //account head for cashInHand
                                    oppositeVoucher.IsCheque = false;
                                }
                                oppositeVoucher.Credit = vd.Debit;
                                oppositeVoucher.Debit = vd.Credit;

                                vdList.Add(oppositeVoucher);
                            }
                            else
                            {
                                oppositeVoucher.Credit += vd.Debit;
                                oppositeVoucher.Debit += vd.Credit;
                            }

                        }
                        ++tranNo;
                    }
                    accountDto.VoucherDetails = vdList;

                    foreach (var vd in accountDto.VoucherDetails)
                    {
                        vd.Status = 1;
                        var entity = Mapper.Map<Voucher>(vd);

                        entity.RecordStatus = RecordStatus.Authorized;
                        entity.CreateDate = DateTime.Now;
                        entity.CreatedBy = userId;
                        entity.Status = EntityStatus.Active;

                        var log = new VoucherLog();
                        log.CreateDate = DateTime.Now;
                        log.CreatedBy = userId;
                        log.Status = EntityStatus.Active;
                        log.ToRecordStatus = RecordStatus.Authorized;

                        GenService.Save(entity);
                        log.VoucherId = entity.Id;
                        GenService.Save(log);
                        //voucherList.Add(entity);
                    }
                    //GenService.Save(voucherList);
                    GenService.Save(chequeList);

                    tran.Complete();

                    response.Success = true;
                    response.Message = "Voucher Saved Successfully";
                    return response;
                }
                catch (Exception ex)
                {
                    tran.Dispose();
                    response.Message = "Voucher Save Failed";
                    return response;
                }
            }
        }
        public ResponseDto SaveVoucher(List<VoucherDto> vouchers, long officeId, long userId)
        {
            var response = new ResponseDto();
            int i = 1;
            int tranNo = 1;
            //var vdList = new List<Voucher>();
            using (var tran = new TransactionScope())
            {
                try
                {
                    foreach (var vd in vouchers)
                    {
                        vd.SerialNo = i++;
                        vd.TransactionNo = tranNo.ToString();
                        //vd.VoucherDate = _companyProfileFacade.DateToday(officeId);
                        if (vd.CompanyProfileId == null)
                            vd.CompanyProfileId = officeId;
                        var systemDate = _companyProfileFacade.DateToday(vd.CompanyProfileId).Date;
                        if (vd.VoucherDate != systemDate)
                        {
                            response.Message = "Vouchers not saved. Voucher date doesn't match current system date. Please contact administrator.";
                            return response;
                        }

                        #region turn on if cheque register is enabled
                        //if (vd.IsCheque)
                        //{
                        //    var cheque = GenService.GetAll<ChequeRegisterDetail>()
                        //        .Where(c => c.ChequeNo == vd.ChequeNo
                        //            && c.ChequeRegister.BankId == vd.BankId
                        //            && c.ChequeStatus == ChequeStatus.UnUsed).FirstOrDefault();
                        //    if (cheque == null)
                        //    {
                        //        response.Message = "Please Provide Cheque Information";
                        //        return response;
                        //    }
                        //    cheque.AccHeadCode = vd.AccountHeadCode;
                        //    cheque.Amount = vd.Amount;
                        //    cheque.ChequeStatus = ChequeStatus.Issued;
                        //    cheque.EditDate = DateTime.Now;
                        //    cheque.IssueDate = DateTime.Now;
                        //    cheque.VoucherNo = vd.VoucherNo;
                        //    chequeList.Add(cheque);
                        //}
                        #endregion

                        //var voucher = Mapper.Map<Voucher>(vd);
                        //voucher.Status = EntityStatus.Active;
                        //voucher.CreateDate = DateTime.Now;
                        //voucher.CreatedBy = userId;
                        //vdList.Add(voucher);
                        vd.Status = 1;
                        var entity = Mapper.Map<Voucher>(vd);

                        entity.RecordStatus = RecordStatus.Authorized;
                        entity.CreateDate = DateTime.Now;
                        entity.CreatedBy = userId;
                        entity.Status = EntityStatus.Active;

                        var log = new VoucherLog();
                        log.CreateDate = DateTime.Now;
                        log.CreatedBy = userId;
                        log.Status = EntityStatus.Active;
                        log.ToRecordStatus = RecordStatus.Authorized;

                        GenService.Save(entity);
                        log.VoucherId = entity.Id;
                        GenService.Save(log);

                        ++tranNo;
                    }

                    tran.Complete();
                    response.Success = true;
                    response.Message = "Voucher Saved Successfully";

                }
                catch (Exception)
                {
                    tran.Dispose();
                    response.Message = "Voucher save failed.";
                }
            }

            return response;
        }
        public string GetLatestCode(string code)
        {
            var accHeads = GenService.GetAll<AccHeadMain>() //_accHeadService.GetAccHeadsByAccCode(code);
                .Where(r => r.Code.StartsWith(code));
            var accHead = new AccHeadMain();
            if (accHeads.Any())
                accHead = accHeads.OrderByDescending(x => x.Code).First();
            else
            {
                return code + "001";
            }
            //var maxCode = accHead.Code;
            if (accHead != null)
            {
                int incrementalValue = Convert.ToInt32(accHead.Code.Substring(7, 3)) + 1;
                string value;
                if (incrementalValue < 10)
                    value = "00" + incrementalValue;
                else if (incrementalValue < 100 && incrementalValue > 9)
                    value = "0" + incrementalValue;
                else
                    value = incrementalValue.ToString();
                return code + value;
            }
            else
                return code + "001";
        }
        //public List<AccHeadDto> GetCashHeads()
        //{
        //    //var accHeads=GenService.GetAll<AccHead>().Where()
        //    return new List<AccHeadDto>();
        //}

        //public List<TreeViewAccountsDto> GetCoATreeHeads()
        //{
        //    var accGroups = GenService.GetAll<AccGroup>().ToList();
        //    var trVwList = new List<TreeViewAccountsDto>();
        //    //foreach (var accGroup in accGroups)
        //    //{
        //    //    var trVw = new TreeViewAccountsDto();
        //    //    trVw.Id = accGroup.Id;
        //    //    trVw.Name = accGroup.Name;

        //    //    foreach (var accSubGroup in accGroup.AccSubGroups)
        //    //    {
        //    //        var trVwSg = new TreeViewAccountsDto();
        //    //        trVwSg.Id = accSubGroup.Id;
        //    //        trVwSg.Name = accSubGroup.Name;

        //    //        foreach (var accHeadGroup in accSubGroup.AccHeadGroups)
        //    //        {
        //    //            var trVwHg = new TreeViewAccountsDto();
        //    //            trVwHg.Id = accHeadGroup.Id;
        //    //            trVwHg.Name = accHeadGroup.Name;

        //    //            foreach (var accHeadSubGroup in accHeadGroup.AccHeadSubGroups)
        //    //            {
        //    //                var trVwHsg = new TreeViewAccountsDto();
        //    //                trVwHsg.Id = accHeadSubGroup.Id;
        //    //                trVwHsg.Name = accHeadSubGroup.Name;

        //    //                foreach (var accHead in accHeadSubGroup.AccHeads)
        //    //                {
        //    //                    var trVwHd = new TreeViewAccountsDto();
        //    //                    trVwHd.Id = accHead.Id;
        //    //                    trVwHd.Name = accHead.Name;

        //    //                    trVwHsg.ChildLocations.Add(trVwHd);
        //    //                }
        //    //                trVwHg.ChildLocations.Add(trVwHsg);
        //    //            }
        //    //            trVwSg.ChildLocations.Add(trVwHg);
        //    //        }
        //    //        trVw.ChildLocations.Add(trVwSg);
        //    //    }
        //    //    trVwList.Add(trVw);
        //    //}
        //    return trVwList;
        //}
        public List<VoucherDto> GetVoucherJurnalVoucherByVoucherNo(string voucherNo)
        {
            var vouchers = GenService.GetAll<Voucher>()
                .Where(r => r.VoucherNo == voucherNo)
                .OrderBy(t => t.SerialNo)
                .ToList();
            var voucherList = new List<VoucherDto>();
            foreach (var voucher in vouchers)
            {
                var voucherDto = Mapper.Map<VoucherDto>(voucher);
                var accHead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == voucher.AccountHeadCode);
                if (accHead == null) return new List<VoucherDto>();
                voucherDto.AccountHeadCodeName = accHead.Name;

                if (voucherNo.Contains("DR"))
                {
                    voucherDto.VoucherName = "Receive Voucher";
                    voucherDto.Amount = voucherDto.Debit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }

                else if (voucherNo.Contains("CR"))
                {
                    voucherDto.VoucherName = "Payment Voucher";
                    voucherDto.Amount = voucherDto.Credit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }

                else if (voucherNo.Contains("JV"))
                {
                    voucherDto.VoucherName = "Journal Voucher";
                    voucherDto.Amount = voucherDto.Credit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }


            }
            return voucherList;
        }
        public List<VoucherDto> GetVoucherByVoucherNo(string voucherNo)
        {
            var vouchers = GenService.GetAll<Voucher>()
                .Where(r => r.VoucherNo == voucherNo)
                .OrderBy(t => t.SerialNo)
                .ToList();
            var voucherList = new List<VoucherDto>();
            foreach (var voucher in vouchers)
            {
                var voucherDto = Mapper.Map<VoucherDto>(voucher);
                var accHead = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == voucher.AccountHeadCode);
                if (accHead == null) return new List<VoucherDto>();
                voucherDto.AccountHeadCodeName = accHead.Name;

                if (voucherNo.Contains("DV"))
                {
                    voucherDto.VoucherName = "Receive Vouher";
                    voucherDto.Amount = voucherDto.Debit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }

                else if (voucherNo.Contains("CV"))
                {
                    voucherDto.VoucherName = "Payment Vouher";
                    voucherDto.Amount = voucherDto.Credit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }

                else if (voucherNo.Contains("JV"))
                {
                    voucherDto.VoucherName = "Journal Vouher";
                    voucherDto.Amount = voucherDto.Credit;
                    if (voucherDto.Amount > 0)
                        voucherList.Add(voucherDto);
                }


            }
            return voucherList;
        }
        public List<AccHeadDto> GetConcatedAccHeads(long? companyProfileId)
        {
            var accHeadListDto = new List<AccHeadDto>();
            var accHeads = GenService.GetAll<AccHead>().ToList();
            if (companyProfileId != null)
                accHeads = accHeads.Where(a => a.CompanyProfileId == companyProfileId).ToList();
            foreach (var aHead in accHeads.ToList())
            {
                var aheadDto = Mapper.Map<AccHeadDto>(aHead);

                string subCode = aHead.Code.Substring(1, 2);
                if (subCode == "00")
                {
                    var acc = GenService.GetAll<AccGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 1));
                    aheadDto.Name = aHead.Name + " - " + acc.Name;
                    var head = GenService.GetAll<AccGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 1));
                    aheadDto.Name = aheadDto.Name + " - " + head.Name;
                    accHeadListDto.Add(aheadDto);
                    continue;
                }

                subCode = aHead.Code.Substring(3, 2);
                if (subCode == "00")
                {
                    var acc = GenService.GetAll<AccSubGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 3));
                    aheadDto.Name = aHead.Name + " - " + acc.Name;
                    var head = GenService.GetAll<AccGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 1));
                    aheadDto.Name = aheadDto.Name + " - " + head.Name;
                    accHeadListDto.Add(aheadDto);
                    continue;
                }

                subCode = aHead.Code.Substring(5, 2);
                if (subCode == "00")
                {
                    var acc = GenService.GetAll<AccHeadGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 5));
                    aheadDto.Name = aHead.Name + " - " + acc.Name;
                    var head = GenService.GetAll<AccGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 1));
                    aheadDto.Name = aheadDto.Name + " - " + head.Name;
                    accHeadListDto.Add(aheadDto);
                }
                else
                {
                    var acc = GenService.GetAll<AccHeadSubGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 7));
                    aheadDto.Name = aHead.Name + " - " + acc.Name;
                    var head = GenService.GetAll<AccGroup>().FirstOrDefault(r => r.Code == aHead.Code.Substring(0, 1));
                    aheadDto.Name = aheadDto.Name + " - " + head.Name;
                    accHeadListDto.Add(aheadDto);
                }

            }
            return accHeadListDto.OrderBy(r => r.Name).ToList();
        }
        public AccHeadDto GetAccHeadByReferenceAccType(ReferenceAccountType referenceAccountType, long refId, string AccCode, long? companyProfileId)
        {
            if (companyProfileId == null)
                companyProfileId = 1;
            var accHead = GenService.GetAll<AccHead>()
                .FirstOrDefault(r => r.RefType == referenceAccountType && r.RefId == refId && r.Code.StartsWith(AccCode));

            return Mapper.Map<AccHeadDto>(accHead);
        }
        public List<VoucherDto> SearchVoucher(AccTranType? accTranType, string payType, string bankAccHeadCode,
                                        DateTime dateFrom, DateTime dateTo, string tranType, string voucherNo, long officeId)
        {
            dateFrom = dateFrom <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : dateFrom;
            dateTo = dateTo >= DateTime.MaxValue ? DateTime.MaxValue : dateTo.AddDays(1);

            var filter = GenService.GetAll<Voucher>().Where(f => f.VoucherDate >= dateFrom && f.VoucherDate <= dateTo && f.CompanyProfileId == officeId);


            if (accTranType != null)
                filter = filter.Where(f => f.AccTranType == accTranType);
            if (!string.IsNullOrEmpty(tranType))
            {
                if (tranType == "cr")
                    filter = filter.Where(f => f.Credit > 0);
                else if (tranType == "dr")
                    filter = filter.Where(f => f.Debit > 0);
            }
            if (!string.IsNullOrEmpty(payType))
            {
                if (payType == "bank")
                {
                    filter = filter.Where(f => f.AccountHeadCode.StartsWith(BizConstants.BankAccHeadSubGroup)); //"1020102";

                    if (!string.IsNullOrEmpty(bankAccHeadCode) && bankAccHeadCode != "undefined")
                        filter = filter.Where(f => f.AccountHeadCode.Contains(bankAccHeadCode));
                }
                else if (payType == "cash")
                    filter = filter.Where(f => f.AccountHeadCode.StartsWith(BizConstants.CashAccHeadSubGroup));//"1020101" .Contains("1020101001")
            }
            if (!string.IsNullOrEmpty(voucherNo))
                filter = filter.Where(f => f.VoucherNo.Contains(voucherNo));

            var results = (from fil in filter
                           join accHead in GenService.GetAll<AccHead>().Where(a => a.CompanyProfileId == officeId) on fil.AccountHeadCode equals accHead.Code
                           select new VoucherDto
                           {
                               AccountHeadCode = fil.AccountHeadCode,
                               AccountHeadCodeName = accHead.Name,
                               AccTranType = fil.AccTranType,
                               //BankAccountHead = fil.
                               Credit = fil.Credit,
                               Debit = fil.Debit,
                               Description = fil.Description,
                               VoucherDate = fil.VoucherDate,
                               VoucherNo = fil.VoucherNo,
                               Id = fil.Id
                           }).ToList();
            return results;
        }
        public VoucherDto GetVoucherById(long VId)
        {
            var result = GenService.GetById<Voucher>(VId);
            return Mapper.Map<VoucherDto>(result);
        }
        public bool SaveEditVoucher(VoucherDto model)
        {
            try
            {
                var entity = GenService.GetById<Voucher>(model.Id);
                entity.Description = model.Description;
                entity.Credit = model.Credit;
                entity.Debit = model.Debit;
                entity.AccountHeadCode = model.AccountHeadCode;
                if (model.VoucherDate > DateTime.MinValue && model.VoucherDate < DateTime.MaxValue)
                    entity.VoucherDate = model.VoucherDate;

                GenService.Save(entity);
                return true;
            }
            catch (Exception) { }
            return false;

        }
        public IPagedList<VoucherDto> GetVoucherListForCreator(DateTime systemDate, int pageSize, int pageCount, string searchString, string sortOrder, long userId, List<long?> companyProfileId)
        {
            systemDate = systemDate.Date;
            var toDate = systemDate.AddDays(1);
            var queryList = GenService.GetAll<Voucher>().Where(p => p.Status == EntityStatus.Active && p.VoucherDate >= systemDate && p.VoucherDate < toDate && p.CreatedBy == userId && companyProfileId.Contains(p.CompanyProfileId)).OrderByDescending(p => p.Id);
            //if(queryList.Count() > 0)
            //{
                var poList = (from v in queryList
                             select new VoucherDto()
                             {
                                 Id = v.Id,
                                 VoucherNo = v.VoucherNo,
                                 VoucherDate = v.VoucherDate,
                                 RecordStatus = v.RecordStatus,
                                 RecordStatusName = v.RecordStatus.ToString(),
                                 Debit = v.Debit,
                                 Credit = v.Credit
                             });

                var temp = poList.ToPagedList(pageCount, pageSize);

                return temp;
            //}
            //return null;
        }
        public List<VoucherDto> GetVoucherListForCreatorReport(DateTime systemDate, long userId, List<long?> companyProfileId)
        {
            systemDate = systemDate.Date;
            var toDate = systemDate.AddDays(1);

            var voucherDto = new VoucherDto();
            var voucherListva = new List<VoucherDto>();
            var voucherVerified = new VoucherLog();
            var voucherAuthorized = new VoucherLog();
            var verifiedempName = "";
            var authorizedempName = "";

            var voucherList = GenService.GetAll<Voucher>().Where(p => p.Status == EntityStatus.Active && p.VoucherDate >= systemDate && p.VoucherDate < toDate && p.CreatedBy == userId && companyProfileId.Contains(p.CompanyProfileId)).OrderByDescending(p => p.Id);

            foreach (var v in voucherList)
            {
                var a = GenService.GetAll<AccHead>().FirstOrDefault(h => h.Code == v.AccountHeadCode);
                voucherVerified = GenService.GetAll<VoucherLog>().FirstOrDefault(p => p.ToRecordStatus == RecordStatus.Verified && p.VoucherId == v.Id);
                if (voucherVerified != null)
                {
                    if (voucherVerified.CreatedBy != null)
                    {
                        var verfiedempId = _user.GetEmployeeIdByUserId((long)voucherVerified.CreatedBy);
                        
                        //var vefiedemployee = GenService.GetById<Employee>(verfiedempId);
                        //verifiedempName = vefiedemployee.Name;
                    }
                }

                voucherAuthorized = GenService.GetAll<VoucherLog>().FirstOrDefault(p => p.ToRecordStatus == RecordStatus.Authorized && p.VoucherId == v.Id);
                if (voucherAuthorized != null)
                {
                    if (voucherAuthorized.CreatedBy != null)
                    {
                        var authorizedempId = _user.GetEmployeeIdByUserId((long)voucherAuthorized.CreatedBy);
                        //var authorizedemployee = GenService.GetById<Employee>(authorizedempId);
                        //authorizedempName = authorizedemployee.Name;
                    }
                }

                voucherDto = new VoucherDto();
                voucherDto.Id = v.Id;
                voucherDto.Debit = v.Debit;
                voucherDto.Credit = v.Credit;
                voucherDto.VoucherNo = v.VoucherNo;
                voucherDto.VoucherDate = v.VoucherDate;
                voucherDto.RecordStatus = v.RecordStatus;
                voucherDto.AccountHeadCode = v.AccountHeadCode;
                voucherDto.RecordStatusName = v.RecordStatus.ToString();
                voucherDto.AccountHeadCodeName = a != null ? a.Name : "";
                voucherDto.CreateDate = v.CreateDate;
                if (voucherVerified != null)
                {
                    voucherDto.VerificationDate = voucherVerified.CreateDate;
                    voucherDto.VerifiedBy = (long)voucherVerified.CreatedBy;
                    voucherDto.VerifiedByName = verifiedempName;
                }
                if (voucherAuthorized != null)
                {
                    voucherDto.AuthorizationDate = voucherAuthorized.CreateDate;
                    voucherDto.AuthorizedBy = (long)voucherAuthorized.CreatedBy;
                    voucherDto.AuthorizedByName = authorizedempName;
                }
                voucherListva.Add(voucherDto);

            }


            return voucherListva.ToList();
        }
        public List<VoucherDto> GetDateWiseVoucherSummary(DateTime systemDate, long userId, List<long?> companyProfileId)
        {


            systemDate = systemDate.Date;
            var toDate = systemDate.AddDays(1);

            var voucherDto = new VoucherDto();
            var voucherListva = new List<VoucherDto>();
            var voucherVerified = new VoucherLog();
            var voucherAuthorized = new VoucherLog();
            var verifiedempName = "";
            var authorizedempName = "";

            var voucherList = GenService.GetAll<Voucher>().Where(p => p.Status == EntityStatus.Active && p.VoucherDate >= systemDate && p.VoucherDate < toDate && companyProfileId.Contains(p.CompanyProfileId)).OrderByDescending(p => p.Id);

            foreach (var v in voucherList)
            {
                var empId = _user.GetEmployeeIdByUserId((long)v.CreatedBy);
                //EmployeeFacade _employee = new EmployeeFacade();

                //var employee = _employee.GetEmployeeInfoById(empId);
                //var empName = employee.Name;

                var a = GenService.GetAll<AccHead>().FirstOrDefault(h => h.Code == v.AccountHeadCode);
                voucherVerified = GenService.GetAll<VoucherLog>().FirstOrDefault(p => p.ToRecordStatus == RecordStatus.Verified && p.VoucherId == v.Id);
                if (voucherVerified != null)
                {
                    if (voucherVerified.CreatedBy != null)
                    {
                        var verfiedempId = _user.GetEmployeeIdByUserId((long)voucherVerified.CreatedBy);
                        //var vefiedemployee = GenService.GetById<Employee>(verfiedempId);
                        //verifiedempName = vefiedemployee.Name;
                    }
                }

                voucherAuthorized = GenService.GetAll<VoucherLog>().FirstOrDefault(p => p.ToRecordStatus == RecordStatus.Authorized && p.VoucherId == v.Id);
                if (voucherAuthorized != null)
                {
                    if (voucherAuthorized.CreatedBy != null)
                    {
                        var authorizedempId = _user.GetEmployeeIdByUserId((long)voucherAuthorized.CreatedBy);
                        //var authorizedemployee = GenService.GetById<Employee>(authorizedempId);
                        //authorizedempName = authorizedemployee.Name;
                    }
                }

                voucherDto = new VoucherDto();
                voucherDto.Id = v.Id;
                voucherDto.Debit = v.Debit;
                voucherDto.Credit = v.Credit;
                voucherDto.VoucherNo = v.VoucherNo;
                voucherDto.VoucherDate = v.VoucherDate;
                voucherDto.RecordStatus = v.RecordStatus;
                voucherDto.AccountHeadCode = v.AccountHeadCode;
                voucherDto.RecordStatusName = v.RecordStatus.ToString();
                voucherDto.AccountHeadCodeName = a != null ? a.Name : "";
                //voucherDto.CreatedBy = v.CreatedBy;
                //voucherDto.CreatedByName = empName;
                voucherDto.CreateDate = v.CreateDate;
                if (voucherVerified != null)
                {
                    voucherDto.VerificationDate = voucherVerified.CreateDate;
                    voucherDto.VerifiedBy = (long)voucherVerified.CreatedBy;
                    voucherDto.VerifiedByName = verifiedempName;
                }
                if (voucherAuthorized != null)
                {
                    voucherDto.AuthorizationDate = voucherAuthorized.CreateDate;
                    voucherDto.AuthorizedBy = (long)voucherAuthorized.CreatedBy;
                    voucherDto.AuthorizedByName = authorizedempName;
                }
                voucherListva.Add(voucherDto);

            }

            return voucherListva.ToList();
        }
        public IPagedList<VoucherDto> GetVoucherListForVerification(int pageSize, int pageCount, DateTime systemDate, List<long?> companyProfileId)
        {
            systemDate = systemDate.Date;
            var toDate = systemDate.AddDays(1);
            var poList = from v in GenService.GetAll<Voucher>().Where(p => (p.RecordStatus == RecordStatus.Submitted || p.RecordStatus == RecordStatus.Verified || p.RecordStatus == RecordStatus.Rejected) && 
                                                                            p.Status == EntityStatus.Active &&
                                                                            p.VoucherDate >= systemDate && 
                                                                            p.VoucherDate < toDate &&
                                                                            companyProfileId.Contains(p.CompanyProfileId)).OrderByDescending(p => p.Id)
                         select new VoucherDto()
                         {
                             Id = v.Id,
                             VoucherNo = v.VoucherNo,
                             VoucherDate = v.VoucherDate,
                             RecordStatus = v.RecordStatus,
                             RecordStatusName = v.RecordStatus.ToString(),
                             Debit = v.Debit,
                             Credit = v.Credit
                         };

            var temp = poList.ToPagedList(pageCount, pageSize);

            return temp;
        }
        public ResponseDto SaveVouchersVerified(List<PostingStatusUpdate> updateList, long userId)
        {
            var response = new ResponseDto();

            var VoucherList = new List<Voucher>();
            var VoucherLogs = new List<VoucherLog>();

            foreach (var voucherElement in updateList)
            {
                var voucher = GenService.GetById<Voucher>(voucherElement.Id);
                if (voucher != null && voucher.RecordStatus != null && voucher.RecordStatus == RecordStatus.Submitted)
                {
                    var oldLogs = GenService.GetAll<VoucherLog>().Where(v => v.VoucherId == voucher.Id && v.Status == EntityStatus.Active).ToList();
                    if (oldLogs != null)
                    {
                        oldLogs.ForEach(l => { l.Status = EntityStatus.Inactive; l.EditDate = DateTime.Now; l.EditedBy = userId; });
                        VoucherLogs.AddRange(oldLogs);
                    }
                    var log = new VoucherLog();
                    log.VoucherId = voucher.Id;
                    log.Status = EntityStatus.Active;
                    log.FromRecordStatus = voucher.RecordStatus;
                    log.CreateDate = DateTime.Now;
                    log.CreatedBy = userId;
                    if (voucherElement.Pass)
                    {
                        voucher.RecordStatus = RecordStatus.Verified;
                        log.ToRecordStatus = RecordStatus.Verified;
                    }
                    else
                    {
                        voucher.RecordStatus = RecordStatus.Rejected;
                        log.ToRecordStatus = RecordStatus.Rejected;
                    }
                    voucher.EditedBy = userId;
                    voucher.EditDate = DateTime.Now;
                    VoucherLogs.Add(log);
                    VoucherList.Add(voucher);
                }

            }
            try
            {
                if (VoucherList.Count > 0)
                {
                    GenService.Save(VoucherList, false);
                    GenService.Save(VoucherLogs, false);
                    GenService.SaveChanges();
                    response.Success = true;
                    response.Message = "Submitted Vouchers Verified.";
                }
                else
                {
                    response.Message = "No Voucher Updated.";
                }
            }
            catch (Exception ex)
            {
                response.Message = "Something went wrong, please contact system administrator.";
            }

            return response;
        }
        public IPagedList<VoucherDto> GetVoucherListForAuthorization(int pageSize, int pageCount, DateTime systemDate, List<long?> companyProfileId)
        {
            systemDate = systemDate.Date;
            var toDate = systemDate.AddDays(1);
            var poList = from v in GenService.GetAll<Voucher>().Where(p => (p.RecordStatus == RecordStatus.Authorized || p.RecordStatus == RecordStatus.Verified || p.RecordStatus == RecordStatus.Rejected) && 
                                                                           p.Status == EntityStatus.Active &&
                                                                           p.VoucherDate >= systemDate && 
                                                                           p.VoucherDate < toDate &&
                                                                           companyProfileId.Contains(p.CompanyProfileId)).OrderByDescending(p => p.Id)
                         select new VoucherDto()
                         {
                             Id = v.Id,
                             VoucherNo = v.VoucherNo,
                             VoucherDate = v.VoucherDate,
                             RecordStatus = v.RecordStatus,
                             RecordStatusName = v.RecordStatus.ToString(),
                             Debit = v.Debit,
                             Credit = v.Credit
                         };

            var temp = poList.ToPagedList(pageCount, pageSize);

            return temp;
        }
        public ResponseDto SaveVouchersAuthorized(List<PostingStatusUpdate> updateList, long userId)
        {
            var response = new ResponseDto();

            var VoucherList = new List<Voucher>();
            var VoucherLogs = new List<VoucherLog>();

            foreach (var voucherElement in updateList)
            {
                var voucher = GenService.GetById<Voucher>(voucherElement.Id);
                if (voucher != null && voucher.RecordStatus != null && voucher.RecordStatus == RecordStatus.Verified)
                {
                    var oldLogs = GenService.GetAll<VoucherLog>().Where(v => v.Id == voucher.Id && v.Status == EntityStatus.Active).ToList();
                    if (oldLogs != null)
                    {
                        oldLogs.ForEach(l => { l.Status = EntityStatus.Inactive; l.EditDate = DateTime.Now; l.EditedBy = userId; });
                        VoucherLogs.AddRange(oldLogs);
                    }
                    var log = new VoucherLog();
                    log.VoucherId = voucher.Id;
                    log.Status = EntityStatus.Active;
                    log.FromRecordStatus = voucher.RecordStatus;
                    log.CreateDate = DateTime.Now;
                    log.CreatedBy = userId;
                    if (voucherElement.Pass)
                    {
                        voucher.RecordStatus = RecordStatus.Authorized;
                        log.ToRecordStatus = RecordStatus.Authorized;
                    }
                    else
                    {
                        voucher.RecordStatus = RecordStatus.Rejected;
                        log.ToRecordStatus = RecordStatus.Rejected;
                    }
                    voucher.EditedBy = userId;
                    voucher.EditDate = DateTime.Now;
                    VoucherLogs.Add(log);
                    VoucherList.Add(voucher);
                }

            }
            try
            {
                if (VoucherList.Count > 0)
                {
                    GenService.Save(VoucherList, false);
                    GenService.Save(VoucherLogs, false);
                    GenService.SaveChanges();
                    response.Success = true;
                    response.Message = "Submitted Vouchers Authorized.";
                }
                else
                {
                    response.Message = "No Voucher Updated.";
                }
            }
            catch (Exception ex)
            {
                response.Message = "Something went wrong, please contact system administrator.";
            }

            return response;
        }
        public void ArchieveVouchers(List<long> officeProfileIds, DateTime toDate)
        {
            toDate = toDate.Date;
            var allAccHeads = GenService.GetAll<AccHead>().Where(a => officeProfileIds.Contains((long)a.CompanyProfileId)).ToList();

            var voucherArchieveList = new List<VoucherArchieve>();

            foreach (var accHead in allAccHeads)
            {
                bool update = false;
                var existingArchieve = GenService.GetAll<VoucherArchieve>()
                    .Where(va => va.AccountHeadCode == accHead.Code
                        && va.CompanyProfileId == accHead.CompanyProfileId
                        && va.ArchieveDate < toDate)
                    .OrderByDescending(va => va.ArchieveDate).FirstOrDefault();

                var vArchieve = new VoucherArchieve();
                vArchieve.AccountHeadCode = accHead.Code;
                vArchieve.CompanyProfileId = accHead.CompanyProfileId;
                vArchieve.ArchieveDate = toDate;
                vArchieve.CreateDate = DateTime.Now;

                if (existingArchieve != null)
                {
                    if (existingArchieve.ArchieveDate.Date == toDate)
                        update = true;

                    var maxTime = toDate.AddDays(1).AddMilliseconds(-3);
                    var minTime = toDate.AddDays(-1).AddMilliseconds(3);
                    var sum = GenService.GetAll<Voucher>().Where(v => v.AccountHeadCode == accHead.Code
                        && v.CompanyProfileId == accHead.CompanyProfileId
                        && v.VoucherDate <= maxTime
                        && v.RecordStatus == RecordStatus.Authorized
                        && v.VoucherDate >= minTime)
                        .GroupBy(v => v.AccountHeadCode)
                        .Select(v => new { Debit = v.Sum(x => x.Debit), Credit = v.Sum(x => x.Credit) }).FirstOrDefault();

                    if (update)
                    {
                        DateTime previousDate = toDate.AddDays(-1);
                        var previousArchieve = GenService.GetAll<VoucherArchieve>()
                            .Where(va => va.AccountHeadCode == accHead.Code
                                && va.CompanyProfileId == accHead.CompanyProfileId
                                && va.ArchieveDate <= previousDate)
                            .OrderByDescending(va => va.ArchieveDate).FirstOrDefault();
                        if (previousArchieve != null)
                        {
                            existingArchieve.Debit = previousArchieve.Debit;
                            existingArchieve.Credit = previousArchieve.Credit;
                        }
                        existingArchieve.Debit += sum.Debit;
                        existingArchieve.Credit += sum.Credit;

                        existingArchieve.EditDate = DateTime.Now;
                        voucherArchieveList.Add(existingArchieve);
                    }
                    else
                    {
                        vArchieve.Debit = existingArchieve.Debit + (sum != null ? sum.Debit : 0);
                        vArchieve.Credit = existingArchieve.Credit + (sum != null ? sum.Credit : 0);

                        voucherArchieveList.Add(vArchieve);
                    }

                }
                else
                {
                    var maxTime = toDate.AddDays(1).AddMilliseconds(-3);
                    var sum = GenService.GetAll<Voucher>().Where(v => v.AccountHeadCode == accHead.Code
                        && v.CompanyProfileId == accHead.CompanyProfileId
                        && v.RecordStatus == RecordStatus.Authorized
                        && v.VoucherDate <= maxTime)
                        .GroupBy(v => v.AccountHeadCode)
                        .Select(v => new { Debit = v.Sum(x => x.Debit), Credit = v.Sum(x => x.Credit) }).FirstOrDefault();
                    if (sum == null)
                    {
                        vArchieve.Debit = 0;
                        vArchieve.Credit = 0;
                    }
                    else
                    {
                        var amount = sum.Debit - sum.Credit;
                        vArchieve.Debit = amount > 0 ? amount : 0;
                        vArchieve.Credit = amount < 0 ? -amount : 0;
                    }
                }

                voucherArchieveList.Add(vArchieve);
            }

            GenService.Save(voucherArchieveList);

            return;
        }
        #endregion
        public string GetAccHeadNameByAccHeadCode(string accHeadCode)
        {
            try
            {
                return GenService.GetAll<AccHead>().Where(ah => ah.Code == accHeadCode).FirstOrDefault().Name;
            }
            catch (Exception) { }
            return "";
        }
        public List<AccHeadDto> GetExpenseAccHeads(long? CompanyProfileId)
        {
            var accHeads = GenService.GetAll<AccHead>().Where(a => a.Code.StartsWith(BizConstants.ExpendatureAccGroup));
            if (CompanyProfileId != null && CompanyProfileId > 0)
                accHeads = accHeads.Where(a => a.CompanyProfileId == CompanyProfileId);

            var result = Mapper.Map<List<AccHeadDto>>(accHeads.OrderBy(a => a.Name).ToList());

            return result;
        }
        public List<AccHeadDto> GetCustomerListForAutoFill(string prefix, List<long> exclusionList, long officeId, long groupId)
        {
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();
            var data = GenService.GetAll<AccHead>()
                .Where(x => x.CompanyProfileId == officeId && x.AccGroupId == groupId && x.Status == EntityStatus.Active && (x.Name.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id))
                .OrderBy(x => x.Name)
                .Select(x => new AccHeadDto { Id = x.Id, Name = x.Name })
                .Take(20)
                .ToList();
            return data;
        }
        //public string SaveBankReconsilation(BankReconsilationDto reconsilation)
        //{
        //    var voucher = new Voucher();

        //    voucher.AccountHeadCode = GetAccHeadByRefIdAndRefType(reconsilation.BankId, ReferenceAccountType.Bank, reconsilation.CompanyProfileId).Code;
        //    voucher.AccTranType = AccTranType.Adjustment;

        //    voucher.BankName = GenService.GetById<Bank>(reconsilation.BankId).Name;
        //    voucher.CompanyProfileId = reconsilation.CompanyProfileId;
        //    voucher.CreateDate = DateTime.Now;
        //    voucher.VoucherNo = "JV-" + _companyProfileFacade.GetUpdateJvNo();
        //    if (reconsilation.IsExpense)
        //    {
        //        voucher.Credit = reconsilation.Amount;
        //        voucher.Debit = 0;
        //    }
        //    else
        //    {
        //        voucher.Debit = reconsilation.Amount;
        //        voucher.Credit = 0;
        //    }
        //    voucher.Description = reconsilation.Description;
        //    voucher.VoucherDate = reconsilation.ReconsilationDate;
        //    voucher.SerialNo = 1;

        //    var contra = new Voucher();

        //    contra.AccountHeadCode = reconsilation.ExpenseAccHeadCode;
        //    contra.AccTranType = AccTranType.Adjustment;
        //    contra.CompanyProfileId = voucher.CompanyProfileId;
        //    contra.CreateDate = voucher.CreateDate;
        //    contra.VoucherNo = voucher.VoucherNo;
        //    contra.Debit = voucher.Credit;
        //    contra.Credit = voucher.Debit;
        //    contra.Description = voucher.Description;
        //    contra.VoucherDate = voucher.VoucherDate;
        //    contra.SerialNo = 2;

        //    using (var tran = new TransactionScope())
        //    {
        //        try
        //        {
        //            GenService.Save(voucher);
        //            GenService.Save(contra);
        //            tran.Complete();
        //            return voucher.VoucherNo;
        //        }
        //        catch (Exception)
        //        {
        //            tran.Dispose();
        //            return "";
        //        }

        //    }
        //}
        public List<ReportConfigDto> CalculateReport(long ReportConfigId, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate, bool getDetails)
        {
            var resultList = new List<ReportConfigDto>();
            var parentConfig = Mapper.Map<ReportConfigDto>(GenService.GetById<ReportConfig>(ReportConfigId));
            parentConfig.Amount = 0;
            if (parentConfig.HasChild)
            {
                var childs = Mapper.Map<List<ReportConfigDto>>(GenService.GetAll<ReportConfig>().Where(rc => rc.ParentId == parentConfig.Id).ToList());
                foreach (var child in childs)
                    resultList.AddRange(CalculateReport((long)child.Id, CompanyProfileIds, fromDate, toDate, getDetails));
                foreach (var item in resultList.Where(r => r.ParentId != null && r.ParentId == parentConfig.Id))
                {
                    if (item.IsPositive)
                        parentConfig.Amount += item.Amount;
                    else
                        parentConfig.Amount -= item.Amount;
                }

                resultList.Add(parentConfig);
                return resultList;
            }
            else if (!string.IsNullOrEmpty(parentConfig.ItemAccGroupCode))
            {
                parentConfig.Amount = CalculateLeafNode(parentConfig.ItemAccGroupCode, CompanyProfileIds, fromDate, toDate);
                resultList.Add(parentConfig);

                if (getDetails != null && getDetails && parentConfig.GetAccHeads != null && (bool)parentConfig.GetAccHeads)
                {
                    resultList.AddRange(CalculateLeafNodeDetails(parentConfig.ItemAccGroupCode, CompanyProfileIds, fromDate, toDate, (long)parentConfig.Id));
                }


            }
            return resultList;
        }
        private decimal CalculateLeafNode(string ItemAccGroupCode, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate)
        {
            decimal result = 0;
            IQueryable<VoucherArchieve> resultElementFrom;
            IQueryable<VoucherArchieve> resultElementTo;

            resultElementFrom = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(ItemAccGroupCode));
            resultElementTo = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(ItemAccGroupCode));


            if (CompanyProfileIds != null)
            {
                resultElementFrom = resultElementFrom.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
                resultElementTo = resultElementTo.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
            }

            if (fromDate != null)
            {
                fromDate = fromDate.Value.Date;
                resultElementFrom = resultElementFrom.Where(r => r.ArchieveDate <= fromDate);
            }
            if (toDate != null)
            {
                toDate = toDate.Value.Date;
                resultElementTo = resultElementTo.Where(r => r.ArchieveDate <= toDate);
            }

            if (fromDate != null)
            {
                decimal toCredit = 0, toDebit = 0, fromCredit = 0, fromDebit = 0;
                if (resultElementFrom != null && resultElementFrom.Count() > 0)
                {
                    var fromArchieveDate = resultElementFrom.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var fromElement = resultElementFrom.Where(r => r.ArchieveDate == fromArchieveDate);
                    fromDebit = fromElement.Sum(t => t.Debit);
                    fromCredit = fromElement.Sum(t => t.Credit);
                }
                if (resultElementTo != null && resultElementTo.Count() > 0)
                {
                    var toArchieveDate = resultElementTo.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var toElement = resultElementTo.Where(r => r.ArchieveDate == toArchieveDate);
                    toCredit = toElement.Sum(t => t.Credit);
                    toDebit = toElement.Sum(t => t.Debit);
                }

                if (ItemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || ItemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                    result = (toDebit - toCredit) - (fromDebit - fromCredit);
                else
                    result = (toCredit - toDebit) - (fromCredit - fromDebit);
            }
            else
            {
                decimal toCredit = 0, toDebit = 0;
                if (resultElementTo != null && resultElementTo.Count() > 0)
                {
                    var toArchieveDate = resultElementTo.OrderByDescending(r => r.ArchieveDate).FirstOrDefault().ArchieveDate;
                    var toElement = resultElementTo.Where(r => r.ArchieveDate == toArchieveDate);
                    toCredit = toElement.Sum(t => t.Credit);
                    toDebit = toElement.Sum(t => t.Debit);
                }

                if (ItemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || ItemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                    result = (toDebit - toCredit);
                else
                    result = (toCredit - toDebit);
            }

            return result;
        }
        private List<ReportConfigDto> CalculateLeafNodeDetails(string itemAccGroupCode, List<long> CompanyProfileIds, DateTime? fromDate, DateTime? toDate, long parentId)
        {
            List<ReportConfigDto> result = new List<ReportConfigDto>();
            IQueryable<VoucherArchieve> resultElementFrom;
            IQueryable<VoucherArchieve> resultElementTo;

            resultElementFrom = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(itemAccGroupCode));
            resultElementTo = GenService.GetAll<VoucherArchieve>().Where(va => va.AccountHeadCode.StartsWith(itemAccGroupCode));


            if (CompanyProfileIds != null && CompanyProfileIds.Count > 0)
            {
                resultElementFrom = resultElementFrom.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
                resultElementTo = resultElementTo.Where(r => CompanyProfileIds.Contains((long)r.CompanyProfileId));
            }

            if (fromDate != null)
            {
                fromDate = fromDate.Value.Date;
                resultElementFrom = resultElementFrom.Where(r => r.ArchieveDate <= fromDate);
            }
            if (toDate != null)
            {
                toDate = toDate.Value.Date;
                resultElementTo = resultElementTo.Where(r => r.ArchieveDate <= toDate);
            }
            var allAccHeads = GenService.GetAll<AccHead>().ToList();
            //-----------------------------------------------------------------------------------------------
            if (fromDate != null)
            {
                var fromElements = resultElementFrom.ToList();
                var toElements = resultElementTo.ToList();
                if (itemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || itemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                {
                    foreach (var toItem in toElements)
                    {
                        var item = new ReportConfigDto();
                        item.Amount = 0;
                        var fromItem = fromElements.Where(f => f.AccountHeadCode == toItem.AccountHeadCode && f.CompanyProfileId == toItem.CompanyProfileId).FirstOrDefault();
                        if (fromItem != null)
                        {
                            item.Amount = (toItem.Debit - toItem.Credit) - (fromItem.Debit - fromItem.Credit);
                            fromElements.Remove(fromItem);
                        }
                        else
                            item.Amount = (toItem.Debit - toItem.Credit);

                        //item.Amount = amount;
                        item.CompanyProfileId = toItem.CompanyProfileId;
                        item.GetAccHeads = false;
                        item.HasChild = false;
                        item.IsPositive = true;
                        item.ItemAccGroupCode = toItem.AccountHeadCode;
                        item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                        item.ParentId = parentId;

                        toElements.Remove(toItem);
                        result.Add(item);
                    }
                }
                else
                {
                    foreach (var toItem in toElements)
                    {
                        var item = new ReportConfigDto();
                        item.Amount = 0;
                        var fromItem = fromElements.Where(f => f.AccountHeadCode == toItem.AccountHeadCode && f.CompanyProfileId == toItem.CompanyProfileId).FirstOrDefault();
                        if (fromItem != null)
                        {
                            item.Amount = (toItem.Credit - toItem.Debit) - (fromItem.Credit - fromItem.Debit);
                            fromElements.Remove(fromItem);
                        }
                        else
                            item.Amount = (toItem.Credit - toItem.Debit);

                        //item.Amount = amount;
                        item.CompanyProfileId = toItem.CompanyProfileId;
                        item.GetAccHeads = false;
                        item.HasChild = false;
                        item.IsPositive = true;
                        item.ItemAccGroupCode = toItem.AccountHeadCode;
                        item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                        item.ParentId = parentId;

                        toElements.Remove(toItem);
                        result.Add(item);
                    }
                    //result = (toElements.Credit - toElements.Debit) - (fromElements.Credit - fromElements.Debit);
                }
            }
            else
            {

                var toElements = resultElementFrom.ToList();
                var tempList = toElements.ToList();
                foreach (var toItem in tempList)
                {
                    var item = new ReportConfigDto();
                    item.Amount = 0;
                    if (itemAccGroupCode.StartsWith(BizConstants.AssetAccGroup) || itemAccGroupCode.StartsWith(BizConstants.ExpendatureAccGroup))
                        item.Amount = (toItem.Debit - toItem.Credit);
                    else
                        item.Amount = (toItem.Credit - toItem.Debit);

                    item.CompanyProfileId = toItem.CompanyProfileId;
                    item.GetAccHeads = false;
                    item.HasChild = false;
                    item.IsPositive = true;
                    item.ItemAccGroupCode = toItem.AccountHeadCode;
                    item.ItemName = allAccHeads.Where(a => a.CompanyProfileId == toItem.CompanyProfileId && a.Code == toItem.AccountHeadCode).FirstOrDefault().Name;
                    item.ParentId = parentId;

                    toElements.Remove(toItem);
                    result.Add(item);

                }
            }


            return result;
        }
        public string SaveVoucherForMemberSC(List<Voucher> voucherList)
        {
            string response = string.Empty;
            try
            {
                GenService.Save(voucherList);
                response = "Vouchers Saved Successfully.";
                return response;
            }
            catch (Exception) { }

            response = "Vouchers not saved. Please try again or contact support.";
            return response;
        }
        public object GetAccHeadMappingData()
        {
            List<AccHeadMain> currentPermissions = new List<AccHeadMain>();

            var accGroup = GenService.GetAll<AccGroup>().Where(i => i.Status == EntityStatus.Active).ToList();
            var accSubGroup = GenService.GetAll<AccSubGroup>().Where(i => i.Status == EntityStatus.Active).ToList();
            var accHeadGroup = GenService.GetAll<AccHeadGroup>().Where(i => i.Status == EntityStatus.Active).ToList();
            var accHeadSubGroup = GenService.GetAll<AccHeadSubGroup>().Where(i => i.Status == EntityStatus.Active).ToList();
            var accHeadMain = GenService.GetAll<AccHeadMain>().Where(i => i.Status == EntityStatus.Active).ToList();

            #region main data population
            var data = (from ag in accGroup
                            //group ag by new { ag.SubModule.ModuleId, ag.SubModule.Module.Name } into grp1
                        select new TreeDataDto
                        {
                            id = "AG_" + ag.Id,
                            text = ag.Name,
                            children = (from asg in accSubGroup.Where(a => a.AccGroupId == ag.Id)
                                            //group set by new { set.SubModuleId, set.SubModule.Name } into grp2
                                        select new TreeDataDto
                                        {
                                            id = "AG_" + ag.Id
                                                    + "_ASG_" + asg.Id,
                                            text = asg.Name,
                                            children = (from ahg in accHeadGroup.Where(a => a.AccSubGroupId == asg.Id)
                                                        select new TreeDataDto
                                                        {
                                                            id = "AG_" + ag.Id
                                                                    + "_ASG_" + asg.Id
                                                                    + "_AHG_" + ahg.Id,
                                                            text = ahg.Name,
                                                            children = (from ahsg in accHeadSubGroup.Where(a => a.AccHeadGroupId == ahg.Id)
                                                                        select new TreeDataDto
                                                                        {
                                                                            id = "AG_" + ag.Id
                                                                                    + "_ASG_" + asg.Id
                                                                                    + "_AHG_" + ahg.Id
                                                                                    + "_AHSG_" + ahsg.Id,
                                                                            text = ahsg.Name,
                                                                            children = (from ah in accHeadMain.Where(i => i.AccHeadSubGroupId == ahsg.Id)
                                                                                        select new TreeDataDto
                                                                                        {
                                                                                            id = "AG_" + ag.Id
                                                                                                + "_ASG_" + asg.Id
                                                                                                + "_AHG_" + ahg.Id
                                                                                                + "_AHSG_" + ahsg.Id
                                                                                                + "_AH_" + ah.Id,
                                                                                            text = ah.Name
                                                                                        }).ToList()
                                                                        }
                                                                        ).ToList().Union(
                                                                            from ah in accHeadMain.Where(i => i.AccHeadSubGroupId == null && i.AccHeadGroupId == ahg.Id)
                                                                            select new TreeDataDto
                                                                            {
                                                                                id = "AG_" + ag.Id
                                                                                        + "_ASG_" + asg.Id
                                                                                        + "_AHG_" + ahg.Id
                                                                                        + "_AH_" + ah.Id,
                                                                                text = ah.Name,
                                                                                //children = null
                                                                            }
                                                                ).ToList()
                                                        }).ToList().Union(
                                                            from ah in accHeadMain.Where(i => i.AccHeadSubGroupId == null && i.AccHeadGroupId == null && i.AccSubGroupId == asg.Id)
                                                            select new TreeDataDto
                                                            {
                                                                id = "AG_" + ag.Id
                                                                        + "_ASG_" + asg.Id
                                                                        + "_AH_" + ah.Id,
                                                                text = ah.Name
                                                            }
                                                ).ToList()

                                        }).ToList().Union(
                                                            from ah in accHeadMain.Where(i => i.AccHeadSubGroupId == null && i.AccHeadGroupId == null && i.AccSubGroupId == null && i.AccGroupId == ag.Id)
                                                            select new TreeDataDto
                                                            {
                                                                id = "AG_" + ag.Id
                                                                     + "_AH_" + ah.Id,
                                                                text = ah.Name
                                                            }
                                                ).ToList()
                        }
                ).ToList();

            #endregion


            return data;
        }
        public object GetAccHeadMappingData(long officeId)
        {
            List<string> data = new List<string>();
            List<AccHead> currentPermissions = new List<AccHead>();

            currentPermissions = GenService.GetAll<AccHead>().Where(r => r.CompanyProfileId == officeId).ToList();

            foreach (var item in currentPermissions)
            {
                var ah = "";

                if (item.AccSubGroupId == null)
                    ah = "AG_" + item.AccGroupId + "_AH_" + item.AccHeadMainId;
                else if (item.AccSubGroupId != null && item.AccHeadGroupId == null)
                    ah = "AG_" + item.AccGroupId + "_ASG_" + item.AccSubGroupId + "_AH_" + item.AccHeadMainId;
                else if (item.AccHeadGroupId != null && item.AccHeadSubGroupId == null)
                    ah = "AG_" + item.AccGroupId + "_ASG_" + item.AccSubGroupId + "_AHG_" + item.AccHeadGroupId + "_AH_" + item.AccHeadMainId;
                else if (item.AccHeadSubGroupId != null)
                    ah = "AG_" + item.AccGroupId + "_ASG_" + item.AccSubGroupId + "_AHG_" + item.AccHeadGroupId + "_AHSG_" + item.AccHeadSubGroupId + "_AH_" + item.AccHeadMainId;

                data.Add(ah);
            }

            return data;
        }
        public ResponseDto SaveAccHeadMappings(List<string> Ids, long officeId, long userId)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                foreach (var id in Ids)
                {
                    var IdList = id.Replace("_anchor", "");
                    var parts = IdList.Split('_');

                    var index = Array.IndexOf(parts, "AH");

                    if (index > -1)
                    {
                        long ahmId = Convert.ToInt64(parts[index + 1]);
                        var ah = GenService.GetAll<AccHead>().Where(a => a.CompanyProfileId == officeId && a.AccHeadMainId == ahmId).FirstOrDefault();
                        if (ah == null)
                        {
                            var ahm = GenService.GetById<AccHeadMain>(ahmId);
                            ah = new AccHead();
                            ah = Mapper.Map<AccHead>(ahm);
                            ah.CompanyProfileId = officeId;

                            GenService.Save(ah, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Message = "Account Head Save Failed.";
                return response;
            }
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Account Head Saved Successfully.";
            return response;
        }
        public string GetAccountGroupLatestCode(long groupId, string code)
        {
            string accCode = null;
            if (groupId == 1)
            {
                var accHeads = GenService.GetAll<AccSubGroup>() //_accHeadService.GetAccHeadsByAccCode(code);
             .Where(r => r.Code.StartsWith(code));
                var accHead = new AccSubGroup();
                if (accHeads.Any())
                {
                    accHead = accHeads.OrderByDescending(x => x.Code).First();
                    if (accHead != null)
                    {
                        accCode = accHead.Code.Substring(1, 2);
                    }
                }

            }
            else if (groupId == 2)
            {
                var accHeads = GenService.GetAll<AccHeadGroup>() //_accHeadService.GetAccHeadsByAccCode(code);
             .Where(r => r.Code.StartsWith(code));
                var accHead = new AccHeadGroup();
                if (accHeads.Any())
                {
                    accHead = accHeads.OrderByDescending(x => x.Code).First();
                    if (accHead != null)
                    {
                        accCode = accHead.Code.Substring(3, 2);
                    }
                }

            }
            else if (groupId == 3)
            {
                var accHeads = GenService.GetAll<AccHeadSubGroup>() //_accHeadService.GetAccHeadsByAccCode(code);
             .Where(r => r.Code.StartsWith(code));
                var accHead = new AccHeadSubGroup();
                if (accHeads.Any())
                {
                    accHead = accHeads.OrderByDescending(x => x.Code).First();
                    if (accHead != null)
                    {
                        accCode = accHead.Code.Substring(5, 2);
                    }
                }

            }

            else
            {
                return code + "01";
            }
            //var maxCode = accHead.Code;
            if (accCode != null)
            {
                int incrementalValue = Convert.ToInt32(accCode) + 1;
                string value;
                if (incrementalValue < 10)
                    value = "0" + incrementalValue;
                //else if (incrementalValue < 100 && incrementalValue > 9)
                //    value = "0" + incrementalValue;
                else
                    value = incrementalValue.ToString();
                return code + value;
            }
            else
                return code + "01";
        }
        public ResponseDto SaveAccountSubGroup(AccountGroupDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.AccGroupId < 0)
                {
                    response.Message = "Please Select Account Group";
                    return response;
                }

                #region Add
                var data = new AccSubGroup();
                data.EditDate = DateTime.Now;
                data.Status = EntityStatus.Active;
                data.CreateDate = DateTime.Now;
                data.CreatedBy = userId;
                data.AccGroupId = (long)dto.AccGroupId;
                data.Name = dto.Name;
                var sub = GenService.GetById<AccGroup>(data.AccGroupId);
                if (sub.Id > 0)
                {
                    data.Code = GetAccountGroupLatestCode(1, sub.Code);
                }
                else
                {
                    response.Message = "Account Group doesn't exist";
                    return response;
                }

                GenService.Save(data);
                response.Id = data.Id;
                response.Success = true;
                response.Message = "Account Sub Group Saved Successfully";

                #endregion

            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }
        public ResponseDto SaveAccountHeadGroup(AccountGroupDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.AccSubGroupId < 0)
                {
                    response.Message = "Please Select Account Sub Group";
                    return response;
                }

                #region Add
                var data = new AccHeadGroup();
                data.EditDate = DateTime.Now;
                data.Status = EntityStatus.Active;
                data.CreateDate = DateTime.Now;
                data.CreatedBy = userId;
                data.AccSubGroupId = (long)dto.AccSubGroupId;
                data.Name = dto.Name;
                var sub = GenService.GetById<AccSubGroup>((long)dto.AccSubGroupId);
                if (dto.CashFlowStatementActivity != null)
                    data.CashFlowStatementActivity = (CashFlowStatementActivities)dto.CashFlowStatementActivity;
                if (sub.Id > 0)
                {
                    data.Code = GetAccountGroupLatestCode(2, sub.Code);
                }
                else
                {
                    response.Message = "Account Sub Group doesn't exist";
                    return response;
                }

                GenService.Save(data);
                response.Id = data.Id;
                response.Success = true;
                response.Message = "Account Head Group Saved Successfully";

                #endregion

            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }
        public ResponseDto SaveAccountHeadSubGroup(AccountGroupDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (dto.AccSubGroupId < 0)
                {
                    response.Message = "Please Select Account Head Group";
                    return response;
                }

                #region Add
                var data = new AccHeadSubGroup();
                data.EditDate = DateTime.Now;
                data.Status = EntityStatus.Active;
                data.CreateDate = DateTime.Now;
                data.CreatedBy = userId;
                data.AccHeadGroupId = (long)dto.AccHeadGroupId;
                data.Name = dto.Name;
                var sub = GenService.GetById<AccHeadGroup>((long)dto.AccHeadGroupId);
                if (sub.Id > 0)
                {
                    data.Code = GetAccountGroupLatestCode(3, sub.Code);
                }
                else
                {
                    response.Message = "Account Head Group doesn't exist";
                    return response;
                }

                GenService.Save(data);
                response.Id = data.Id;
                response.Success = true;
                response.Message = "Account Head Sub Group Saved Successfully";

                #endregion

            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }
        
        public List<VoucherDto> GetVoucherListPrintReportByVoucherNo(string voucherNo)
        {
            var vouchers = GenService.GetAll<Voucher>()
                .Where(r => r.VoucherNo == voucherNo)
                .OrderBy(t => t.SerialNo)
                .ToList();
            var userId = (from v in vouchers select v.CreatedBy).FirstOrDefault();

           
            var vouchersList = from voucher in vouchers
                               select new VoucherDto
                               {
                                   SerialNo = voucher.SerialNo,
                                   VoucherNo = voucher.VoucherNo,
                                   VoucherDate = voucher.VoucherDate,
                                   AccountHeadCodeName = GenService.GetAll<AccHead>().FirstOrDefault(x => x.Code == voucher.AccountHeadCode).Name,
                                   Description = voucher.Description,
                                   Debit = voucher.Debit,
                                   Credit = voucher.Credit,
                                 

                               };

            return vouchersList.ToList();
        }
        public bool CheckAccHeadByCode(long officeId, string code)
        {
            var accHead = GenService.GetAll<AccHead>().Where(a => a.CompanyProfileId == officeId && a.Code == code);
            if (accHead != null)
                return true;
            return false;
        }

        public ResponseDto CheckPendingVouchers(List<long> companyProfileIds)
        {
            var response = new ResponseDto();
            var vouchers = GenService.GetAll<Voucher>().Where(v => (v.RecordStatus == RecordStatus.Submitted || v.RecordStatus == RecordStatus.Verified) && v.Status == EntityStatus.Active);
            if (companyProfileIds != null && companyProfileIds.Count > 0)
                vouchers = vouchers.Where(v => v.CompanyProfileId != null && companyProfileIds.Contains((long)v.CompanyProfileId));
            if (vouchers.Any())
            {
                response.Success = true;
                response.Message = "Pending voucher numbers are ";
                var i = 0;
                foreach (var item in vouchers.Select(v => v.VoucherNo).Distinct())
                {
                    if (i == 0)
                        response.Message += item;
                    else
                        response.Message += ", " + item;
                    i++;
                }
                response.Message += ". ";
            }
            return response;
        }

        public List<EnumDto> GetBankReconsilationTypes()
        {
            var typeList = Enum.GetValues(typeof(BankReconsilationType))
           .Cast<BankReconsilationType>()
           .Select(t => new EnumDto
           {
               Id = ((int)t),
               Name = t.ToString(),
                   //DisplayName = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }

 
        public ResponseDto SaveBankReconsilation(BankReconsilationDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();

            #region Edit Bank Reconsilation

            if (dto.Id > 0 && dto.Id != null)
            {
             
                List<BankReconsilationDetails> bankReconsilationList = new List<BankReconsilationDetails>();
                var prev = GenService.GetById<BankReconsilation>((long)dto.Id);
                if (dto.RemovedDetails != null)
                {
                    foreach (var item in dto.RemovedDetails)
                    {
                        var crdCard = GenService.GetById<BankReconsilationDetails>(item);
                        if (crdCard != null)
                        {
                            crdCard.Status = EntityStatus.Inactive;
                            crdCard.EditDate = DateTime.Now;
                            crdCard.EditedBy = userId;
                        }
                        GenService.Save(crdCard);
                    }
                }
                #region Bank Reconsilation Details

                if (dto.Details != null)
                {
                    foreach (var dtl in dto.Details)
                    {
                        var reconsilationDtl = new BankReconsilationDetails();
                        if (dtl.Id != null && dtl.Id > 0)
                        {
                            reconsilationDtl = GenService.GetById<BankReconsilationDetails>((long)dtl.Id);
                            dtl.CreateDate = reconsilationDtl.CreateDate;
                            dtl.CreatedBy = reconsilationDtl.CreatedBy;
                            dtl.Status = reconsilationDtl.Status;
                            dtl.EditDate = DateTime.Now;
                            dtl.BRId = reconsilationDtl.BRId;
                            reconsilationDtl = Mapper.Map(dtl, reconsilationDtl);
                            bankReconsilationList.Add(reconsilationDtl);
                        }
                        else
                        {
                            reconsilationDtl = Mapper.Map<BankReconsilationDetails>(dtl);
                            reconsilationDtl.Status = EntityStatus.Active;
                            reconsilationDtl.CreatedBy = userId;
                            reconsilationDtl.CreateDate = DateTime.Now;
                            reconsilationDtl.BRId = prev.Id;
                            bankReconsilationList.Add(reconsilationDtl);
                            //GenService.Save(text);
                        }
                    }
                }

                #endregion
                dto.CreateDate = prev.CreateDate;
                dto.CreatedBy = prev.CreatedBy;
                dto.Status = prev.Status;
                dto.EditDate = DateTime.Now;
                Mapper.Map(dto, prev);
                GenService.Save(prev);    
                GenService.Save(bankReconsilationList);
           
                response.Id = prev.Id;
                response.Success = true;
                response.Message = "Bank Reconsilation Edited Successfully";
            }

            #endregion
            #region Add Bank Reconsilation

            else
            {
                var data = Mapper.Map<BankReconsilation>(dto);
                
                if (dto.Details != null && dto.Details.Count > 0)
                {
                    data.Details = new List<BankReconsilationDetails>();
                    data.Details = Mapper.Map<List<BankReconsilationDetails>>(dto.Details);
                    data.Details = data.Details.Select(d =>
                    {
                        d.Status = EntityStatus.Active;
                        return d;
                    }).ToList();
                }
                data.Status = EntityStatus.Active;
                data.CreateDate = DateTime.Now;
                GenService.Save(data);

                response.Id = data.Id;
                response.Success = true;
                response.Message = "Bank Reconsilation Saved Successfully";
            }

            #endregion

            return response;
        }
        public IPagedList<BankReconsilationDto> BankReconsilationList(int pageSize, int pageCount, string searchString)
        {
            var slips = GenService.GetAll<BankReconsilation>().Where(m => m.Status == EntityStatus.Active);
            var receipts = Mapper.Map<List<BankReconsilationDto>>(slips);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                receipts = receipts.Where(m => m.AccontNumber.ToLower().Contains(searchString)).ToList();
            }
            var slipList = (from receipt in receipts
                            select new BankReconsilationDto
                            {
                                Id = receipt.Id,
                                AccontNumber = receipt.AccontNumber,
                                MonthEndingDate = receipt.MonthEndingDate,
                                ReviewedBy = receipt.ReviewedBy
                            });
            var temp = slipList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }

        public BankReconsilationDto GetBankReconsilationById(long id)
        {
            var data = GenService.GetById<BankReconsilation>(id);
            var result = Mapper.Map<BankReconsilationDto>(data);
            return result;
        }
        public ResponseDto PaymentBalanceCheck(long companyProfileId, int paymentType, string accountHeadCode, decimal amount)
        {
            var response = new ResponseDto();
            decimal availableBalance = 0;
            if (paymentType == 1)
                accountHeadCode = BizConstants.CashInHand;
            if(string.IsNullOrEmpty(accountHeadCode))
            {
                response.Message = "No account head found.";
                return response;
            }
            var archieveData = GenService.GetAll<VoucherArchieve>().Where(v => v.AccountHeadCode == accountHeadCode).OrderByDescending(v => v.ArchieveDate).FirstOrDefault();
            if (archieveData != null)
            {
                availableBalance += archieveData.Debit - archieveData.Credit;
            }
            var vouchers = GenService.GetAll<Voucher>().Where(v => v.VoucherDate > archieveData.ArchieveDate && v.RecordStatus == RecordStatus.Authorized && v.AccountHeadCode == accountHeadCode);
            if (vouchers.Any())
                availableBalance += vouchers.Sum(v => v.Debit - v.Credit);
            availableBalance -= amount;
            if(availableBalance > 0)
            {
                response.Success = true;
                response.Message = "Balance is available for transaction.";
            }
            else
            {
                response.Message = "Insufficient balance for transaction.";
            }
            return response;
        }
    }

    public class TreeDataDto
    {
        public string id { get; set; }
        public string text { get; set; }
        public List<TreeDataDto> children { get; set; }
        public state state { get; set; }
    }

    public class state
    {
        public state()
        {
            opened = false;
            disabled = false;
            selected = false;
        }
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
}
