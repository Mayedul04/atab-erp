﻿using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Accounts.Infrastructure;
using Finix.Auth.DTO;
using Finix.Auth.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Dto;
using Finix.Accounts.Infrastructure.Models;

namespace Finix.Accounts.Facade.AutoMaps
{
    public class AccountsMappingProfile : Profile
    {
        protected override void Configure()
        {
            //base.Configure();
            CreateMap<AccBalance, AccBalanceDto>();
            CreateMap<AccBalanceDto, AccBalance>();

            CreateMap<AccGroup, AccGroupDto>();
            CreateMap<AccGroupDto, AccGroup>();

            CreateMap<AccSubGroup, AccSubGroupDto>();
            CreateMap<AccSubGroupDto, AccSubGroup>();

            CreateMap<AccHead, AccHeadDto>();
            CreateMap<AccHeadDto, AccHead>();

            CreateMap<AccHeadMain, AccHeadDto>();
            CreateMap<AccHeadDto, AccHeadMain>();

            CreateMap<AccHeadMain, AccHead>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.AccHeadMainId, o => o.MapFrom(s => s.Id));

            CreateMap<AccHeadGroup, AccHeadGroupDto>();
            CreateMap<AccHeadGroupDto, AccHeadGroup>();

            CreateMap<AccHeadSubGroup, AccHeadSubGroupDto>();
            CreateMap<AccHeadSubGroupDto, AccHeadSubGroup>();

            CreateMap<Bank, BankDto>();
            CreateMap<BankDto, Bank>();


            CreateMap<Voucher, VoucherDto>();
            CreateMap<VoucherDto, Voucher>();


            CreateMap<ChequeRegister, ChequeRegisterDto>()
                 .ForMember(dest => dest.BankName,
                   opts => opts.MapFrom(
                       src => src.Bank.Name));
            //.ForMember(dest => dest.CompanyProfileName,
            //    opts => opts.MapFrom(
            //        src => src.CompanyProfile.Name));
            CreateMap<ChequeRegisterDto, ChequeRegister>();

            CreateMap<ChequeRegisterDetail, ChequeRegisterDetailDto>();
            CreateMap<ChequeRegisterDetailDto, ChequeRegisterDetail>();

            CreateMap<ReportConfig, ReportConfigDto>();

            CreateMap<OfficeProfile, OfficeProfileDto>();
            CreateMap<OfficeProfileDto, OfficeProfile>();

            CreateMap<IUOSlip, IUOSlipDto>();
            CreateMap<IUOSlipDto, IUOSlip>();

            CreateMap<BankReconsilation, BankReconsilationDto>()
                .ForMember(d => d.BankName, o => o.MapFrom(s => s.Bank != null ? s.Bank.BranchName != null ? s.Bank.Name : null : ""));
            CreateMap<BankReconsilationDto, BankReconsilation>();

            CreateMap<BankReconsilationDetails, BankReconsilationDetailsDto>();
            CreateMap<BankReconsilationDetailsDto, BankReconsilationDetails>();

            CreateMap<IUOSlip, IUOSlipAdjustmentDto>();
            //CreateMap<IUOSlipDto, IUOSlip>();

            CreateMap<BudgetConfig, BudgetConfigDto>()
                .ForMember(d => d.AccHeadName, o => o.MapFrom(s => s.AccHead.Name))
                .ForMember(d => d.AccHead, o => o.MapFrom(s => new AutoKeyVal { key = s.AccHeadId.ToString(), value = s.AccHead.Name }));
            CreateMap<BudgetConfigDto, BudgetConfig>()
                .ForMember(d => d.AccHead, o => o.Ignore());

            CreateMap<Budget, BudgetDto>()
                .ForMember(d => d.BudgetStatusName, o => o.MapFrom(s => s.BudgetStatus.ToString()));
            CreateMap<BudgetDto, Budget>()
                .ForMember(d => d.Details, o => o.Ignore());

            CreateMap<BudgetDetail, BudgetDetailDto>()
                .ForMember(d => d.AccHeadName, o => o.MapFrom(s => s.BudgetConfig.AccHead.Name))
                .ForMember(d => d.AccGroupId, o => o.MapFrom(s => s.BudgetConfig.AccGroupId));
            CreateMap<BudgetDetailDto, BudgetDetail>();
        }
    }
}
