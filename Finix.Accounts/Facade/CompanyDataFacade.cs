﻿using AutoMapper;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Service;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Facade
{
    public class CompanyDataFacade : BaseFacade
    {
        private readonly CompanyProfileFacade _conpanyProfile;

        public CompanyDataFacade()
        {
            this._conpanyProfile = new CompanyProfileFacade();
        }


        public OfficeProfileDto GetCompanyProfileByAccHeadCode(string accHeadCode)
        {
            var companyProfileId = GenService.GetAll<AccHead>().Where(a => a.Code == accHeadCode).FirstOrDefault().CompanyProfileId;
            var companyProfile = _conpanyProfile.GetCompanyProfileById((long)companyProfileId);
            return Mapper.Map<OfficeProfileDto>(companyProfile);
        }
    }
}
