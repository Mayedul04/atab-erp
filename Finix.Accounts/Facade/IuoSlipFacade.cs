﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Mvc.Html;
using AutoMapper;
using Finix.Accounts.Dto;
using Finix.Accounts.DTO;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Infrastructure.Models;
using Finix.Accounts.Util;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using PagedList;

namespace Finix.Accounts.Facade
{
    public class IuoSlipFacade : BaseFacade
    {
        private readonly Auth.Facade.UserFacade _user = new Auth.Facade.UserFacade();
        private readonly CompanyProfileFacade _cpFacade = new CompanyProfileFacade();
        private readonly AccountsFacade _accFacade = new AccountsFacade();
        public List<EnumDto> GetIuoSlipStatuses()
        {
            var typeList = Enum.GetValues(typeof(IuoSlipStatus))
               .Cast<IuoSlipStatus>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
                   //DisplayName = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }

        public ResponseDto SaveIuoSlip(IUOSlipDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var empId = _user.GetEmployeeIdByUserId(userId);
            var VoucherList = new List<Voucher>();
            try
            {

                var data = Mapper.Map<IUOSlip>(dto);
                data.CreateDate = DateTime.Now;
                data.CreatedBy = userId;
                data.Status = EntityStatus.Active;
                //GenService.Save(data);
               
                string voucherNo = "IUO-" + _cpFacade.GetUpdateJvNo();
                #region Account Payable entry
                Voucher payableVoucher = new Voucher();
                if (!string.IsNullOrEmpty(dto.DebitAccHeadCode))
                {
                    payableVoucher.AccountHeadCode = dto.DebitAccHeadCode; //payableAccHead.Code;
                                                                           //payableVoucher.BankName = memberSavingDto.BankName != null ? memberSavingDto.BankName : "";
                                                                           //payableVoucher.ChequeNo = memberSavingDto.ChequeNo != null ? memberSavingDto.ChequeNo : "";
                                                                           //payableVoucher.ChequeDate = memberSavingDto.ChequeDate;

                    payableVoucher.VoucherNo = voucherNo;
                    payableVoucher.AccTranType = AccTranType.Receive;
                    payableVoucher.CreateDate = DateTime.Now;
                    payableVoucher.Debit = dto.Amount;
                    payableVoucher.Credit = 0;
                    payableVoucher.SerialNo = 1;
                    payableVoucher.CompanyProfileId = dto.CompanyProfileId;
                    payableVoucher.VoucherDate = DateTime.Now.Date;
                    payableVoucher.Description = dto.Remarks;
                    payableVoucher.Status = Finix.Accounts.Infrastructure.EntityStatus.Active;
                    VoucherList.Add(payableVoucher);
                }
                else
                {
                    response.Message = "Please Give Entry Of Debit Account Head Code ";
                    return response;
                }
                #endregion

                #region Account Credit entry
                Voucher contraVoucher = new Voucher();
                if (dto.IsBank != null && (bool)dto.IsBank)
                {
                    if (dto.BankId != null)
                    {
                        var bankInfo = GenService.GetById<Bank>((long)dto.BankId);
                        //payableVoucher.AccountHeadCode = BizConstants.Cash; //payableAccHead.Code; GetBankAccHeadByBankId
                        var creditHeadDetails = _accFacade.GetAccHeadByRefIdAndRefType((long)dto.BankId,
                            ReferenceAccountType.Bank, null);
                        if (creditHeadDetails != null)
                        {
                            contraVoucher.AccountHeadCode = creditHeadDetails.Code;

                            contraVoucher.BankName = bankInfo.Name != null ? bankInfo.Name : "";
                            contraVoucher.ChequeNo = dto.ChequeNo != null ? dto.ChequeNo : "";
                            contraVoucher.ChequeDate = dto.ChequeDate;
                        }
                        else
                        {
                            response.Message = "Please Give Entry Of Bank ";
                            return response;
                        }
                    }
                }
                else
                {
                    contraVoucher.AccountHeadCode = BizConstants.CashInHand;
                }
                data.CreditAccHeadCode = contraVoucher.AccountHeadCode;
                contraVoucher.VoucherNo = voucherNo;

                contraVoucher.AccTranType = AccTranType.Payment;
                contraVoucher.CreateDate = DateTime.Now;
                contraVoucher.Debit = 0;
                contraVoucher.Credit = dto.Amount;
                contraVoucher.SerialNo = 1;
                contraVoucher.CompanyProfileId = dto.CompanyProfileId;

                contraVoucher.VoucherDate = DateTime.Now.Date;
                contraVoucher.Description = dto.Remarks;
                contraVoucher.IsCheque = dto.ChequeNo != null ? true : false;
                contraVoucher.Status = Finix.Accounts.Infrastructure.EntityStatus.Active;
                VoucherList.Add(contraVoucher);
                #endregion
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        //if (dto.VerificationReportFile == null && dto.VettingReportFile == null)
                        //{
                        //    Mapper.Map(dto, entity);
                        //}

                        GenService.Save(data);
                        GenService.Save(VoucherList);
                        tran.Complete();
                        response.Id = data.Id;
                        response.Success = true;
                        response.Message = "IUO Saved Successfully";
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        response.Message = "IUO Save Operation Failed";
                        return response;
                    }
                }


            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }

        public IPagedList<IUOSlipDto> IouSlipList(int pageSize, int pageCount, string searchString)
        {
            var slips = GenService.GetAll<IUOSlip>().Where(m => m.IuoSlipStatus== IuoSlipStatus.Paid && m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                slips = slips.Where(m => m.ReceiverName.ToLower().Contains(searchString));
            }
            var slipList = from slip in slips
                           select new IUOSlipDto
                           {
                               Id = slip.Id,
                               ReceiverName = slip.ReceiverName,
                               Designation = slip.Designation,
                               Amount = slip.Amount,
                               //CompanyProfileId = slip.ReceiverName,
                               //BankId = slip.ReceiverName,
                               ReceiveDate = slip.ReceiveDate,
                               Remarks = slip.Remarks,
                               IuoSlipStatusName = slip.IuoSlipStatus.ToString(),
                               //IsBank = slip.ReceiverName,
                               CreditAccHeadCode = slip.ReceiverName,
                               DebitAccHeadCode = slip.ReceiverName,
                           };
            var temp = slipList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }

        public IUOSlipDto GetIuoSlipById(long id)
        {
            var data = GenService.GetById<IUOSlip>(id);
            return Mapper.Map<IUOSlipDto>(data);
        }

        public List<EnumDto> GetIuoPurposes()
        {
            var typeList = Enum.GetValues(typeof(IuoPurpose))
               .Cast<IuoPurpose>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
                   //DisplayName = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }

        public IUOSlipAdjustmentDto LoadIuoSlipByIdForAdjustment(long id)
        {
            var data = GenService.GetById<IUOSlip>(id);
            var result = Mapper.Map<IUOSlipAdjustmentDto>(data);
            var vchr = new VoucherDto();
            result.VoucherList = new List<VoucherDto>();
            if (result.IuoPurpose == IuoPurpose.GroupAdvance || result.IuoPurpose == IuoPurpose.Individual)
            {
                vchr.AccountHeadCode = result.CreditAccHeadCode;
                vchr.Description = result.CreditAccHeadCode;
                vchr.Debit = result.Amount;
                vchr.Credit = 0;
                vchr.VoucherDate = DateTime.Now;
                var accHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(result.CreditAccHeadCode));
                if (accHead.Any())
                {
                    vchr.AccountHeadCodeName = accHead.FirstOrDefault() != null ? accHead.FirstOrDefault().Name : "";
                }
                result.VoucherList.Add(vchr);
                var cntraVchr = new VoucherDto();
                cntraVchr.AccountHeadCode = result.DebitAccHeadCode;
                cntraVchr.Description = result.DebitAccHeadCode;
                cntraVchr.Debit = 0;
                cntraVchr.Credit = result.Amount;
                cntraVchr.VoucherDate = DateTime.Now;
                var contraAccHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(result.DebitAccHeadCode));
                if (contraAccHead.Any())
                {
                    cntraVchr.AccountHeadCodeName = contraAccHead.FirstOrDefault() != null
                        ? contraAccHead.FirstOrDefault().Name
                        : "";
                }
                result.VoucherList.Add(cntraVchr);
            }
            else
            {
                var cntraVchr = new VoucherDto();
                cntraVchr.AccountHeadCode = result.DebitAccHeadCode;
                cntraVchr.Description = result.DebitAccHeadCode;
                cntraVchr.Debit = 0;
                cntraVchr.Credit = result.Amount;
                cntraVchr.VoucherDate = DateTime.Now;
                var contraAccHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(result.DebitAccHeadCode));
                if (contraAccHead.Any())
                {
                    cntraVchr.AccountHeadCodeName = contraAccHead.FirstOrDefault() != null
                        ? contraAccHead.FirstOrDefault().Name
                        : "";
                }
                result.VoucherList.Add(cntraVchr);
            }
            return result;
        }

        public ResponseDto LoadVoucherForIndividual(long id)
        {
            var data = GenService.GetById<IUOSlip>(id);
            var result = new ResponseDto();
            var accHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(data.DebitAccHeadCode));
            if (accHead.Any())
            {
                result.Success = true;
                result.Message = "Credt in ";
                result.Message += accHead.FirstOrDefault() != null ? accHead.FirstOrDefault().Name : "";
                result.Message += " & Debit will be in Salary Payable. Are you sure about the adjustment";
            }
            return result;
        }

        public ResponseDto SaveIndividualIuoSlip(long id,long CompanyProfileId)
        {
            ResponseDto response = new ResponseDto();
            //var empId = _user.GetEmployeeIdByUserId(userId);
            var VoucherList = new List<Voucher>();
            try
            {
                var data = GenService.GetById<IUOSlip>(id);
                data.IuoSlipStatus = IuoSlipStatus.Adjusted;
                string voucherNo = "IUO-" + _cpFacade.GetUpdateJvNo();
                #region Account Payable entry
                Voucher contraVoucher = new Voucher();
                contraVoucher.AccountHeadCode = BizConstants.SalaryPayable;
                contraVoucher.VoucherNo = voucherNo;
                contraVoucher.AccTranType = AccTranType.Receive;
                contraVoucher.CreateDate = DateTime.Now;
                contraVoucher.Debit = data.Amount;
                contraVoucher.Credit = 0;
                contraVoucher.SerialNo = 1;
                contraVoucher.CompanyProfileId = CompanyProfileId;
                contraVoucher.VoucherDate = DateTime.Now.Date;
                contraVoucher.Description = data.Remarks;
                contraVoucher.Status =EntityStatus.Active;
                VoucherList.Add(contraVoucher);
                #endregion

                #region Account Credit entry
                Voucher payableVoucher = new Voucher();
                if (!string.IsNullOrEmpty(data.DebitAccHeadCode))
                {
                    payableVoucher.AccountHeadCode = data.DebitAccHeadCode;

                    payableVoucher.VoucherNo = voucherNo;
                    payableVoucher.AccTranType = AccTranType.Receive;
                    payableVoucher.CreateDate = DateTime.Now;
                    payableVoucher.Debit = 0;
                    payableVoucher.Credit = data.Amount;
                    payableVoucher.SerialNo = 2;
                    payableVoucher.CompanyProfileId = CompanyProfileId;
                    payableVoucher.VoucherDate = DateTime.Now.Date;
                    payableVoucher.Description = data.Remarks;
                    payableVoucher.Status = EntityStatus.Active;
                    VoucherList.Add(payableVoucher);
                }
                else
                {
                    response.Message = "Please Give Entry Of Debit Account Head Code ";
                    return response;
                }
                
                #endregion
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        GenService.Save(VoucherList);
                        GenService.Save(data);
                        tran.Complete();
                        response.Id = data.Id;
                        response.Success = true;
                        response.Message = "IUO Adjustment Successfully";
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        response.Message = "IUO Adjustment Failed";
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }

        public ResponseDto SaveGroupIuoSlip(long id, long CompanyProfileId)
        {
            ResponseDto response = new ResponseDto();
            //var empId = _user.GetEmployeeIdByUserId(userId);
            var VoucherList = new List<Voucher>();
            try
            {
                var data = GenService.GetById<IUOSlip>(id);
                data.IuoSlipStatus = IuoSlipStatus.Adjusted;
                string voucherNo = "IUO-" + _cpFacade.GetUpdateJvNo();
                #region Account Payable entry
                Voucher contraVoucher = new Voucher();
                contraVoucher.AccountHeadCode = data.CreditAccHeadCode;
                contraVoucher.VoucherNo = voucherNo;
                contraVoucher.AccTranType = AccTranType.Receive;
                contraVoucher.CreateDate = DateTime.Now;
                contraVoucher.Debit = data.Amount;
                contraVoucher.Credit = 0;
                contraVoucher.SerialNo = 1;
                contraVoucher.CompanyProfileId = CompanyProfileId;
                contraVoucher.VoucherDate = DateTime.Now.Date;
                contraVoucher.Description = data.Remarks;
                contraVoucher.Status = EntityStatus.Active;
                VoucherList.Add(contraVoucher);
                #endregion

                #region Account Credit entry
                Voucher payableVoucher = new Voucher();
                if (!string.IsNullOrEmpty(data.DebitAccHeadCode))
                {
                    payableVoucher.AccountHeadCode = data.DebitAccHeadCode;

                    payableVoucher.VoucherNo = voucherNo;
                    payableVoucher.AccTranType = AccTranType.Receive;
                    payableVoucher.CreateDate = DateTime.Now;
                    payableVoucher.Debit = 0;
                    payableVoucher.Credit = data.Amount;
                    payableVoucher.SerialNo = 2;
                    payableVoucher.CompanyProfileId = CompanyProfileId;
                    payableVoucher.VoucherDate = DateTime.Now.Date;
                    payableVoucher.Description = data.Remarks;
                    payableVoucher.Status = EntityStatus.Active;
                    VoucherList.Add(payableVoucher);
                }
                else
                {
                    response.Message = "Please Give Entry Of Debit Account Head Code ";
                    return response;
                }

                #endregion
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        GenService.Save(VoucherList);
                        GenService.Save(data);
                        tran.Complete();
                        response.Id = data.Id;
                        response.Success = true;
                        response.Message = "IUO Adjustment Successfully";
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        response.Message = "IUO Adjustment Failed";
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }

        public ResponseDto LoadVoucherForGroup(long id)
        {
            var data = GenService.GetById<IUOSlip>(id);
            var result = new ResponseDto();
            var accHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(data.DebitAccHeadCode));
            if (accHead.Any())
            {
                result.Success = true;
                result.Message = "Credt in ";
                result.Message += accHead.FirstOrDefault() != null ? accHead.FirstOrDefault().Name : "";
            }
            var creditAccHead = GenService.GetAll<AccHead>().Where(r => r.Code.Contains(data.CreditAccHeadCode));
            if (creditAccHead.Any())
            {
                result.Success = true;
                result.Message = "Debit in ";
                result.Message += creditAccHead.FirstOrDefault() != null ? creditAccHead.FirstOrDefault().Name : "";
                result.Message += ". Are you sure about the adjustment?";
            }
            return result;
        }

        public ResponseDto SaveIndividualExpenceIuoSlip(List<VoucherDto> vouchers, long id ,long CompanyProfileId)
        {
            ResponseDto response = new ResponseDto();
            //var empId = _user.GetEmployeeIdByUserId(userId);
            var VoucherList = new List<Voucher>();
            try
            {
                var data = GenService.GetById<IUOSlip>(id);
                data.IuoSlipStatus = IuoSlipStatus.Adjusted;
                string voucherNo = "IUO-" + _cpFacade.GetUpdateJvNo();

                foreach (var voucherDto in vouchers)
                {
                    Voucher contraVoucher = new Voucher();
                    contraVoucher.AccountHeadCode = voucherDto.AccountHeadCode;
                    contraVoucher.VoucherNo = voucherNo;
                    contraVoucher.AccTranType = AccTranType.Adjustment;
                    contraVoucher.CreateDate = DateTime.Now;
                    contraVoucher.Debit = voucherDto.Debit>0 ? voucherDto.Debit : 0;
                    contraVoucher.Credit = voucherDto.Credit > 0 ? voucherDto.Credit : 0;
                    contraVoucher.SerialNo = 1;
                    contraVoucher.CompanyProfileId = CompanyProfileId;
                    contraVoucher.VoucherDate = DateTime.Now.Date;
                    contraVoucher.Description = voucherDto.Description;
                    contraVoucher.Status = EntityStatus.Active;
                    VoucherList.Add(contraVoucher);
                }
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        GenService.Save(VoucherList);
                        GenService.Save(data);
                        tran.Complete();
                        //response.Id = data.Id;
                        response.Success = true;
                        response.Message = "IUO Adjustment Successfully";
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        response.Message = "IUO Adjustment Failed";
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }

     
    }


}
