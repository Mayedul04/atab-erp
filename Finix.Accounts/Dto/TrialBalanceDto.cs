﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class TrialBalanceDto
    {
        public int Id { get; set; }
        public string SelectedAccountHeadCode { get; set; }
        public string SelectedAccountHeadName { get; set; }
        public string TransactionNo { get; set; }
        public string AccountGroupName { get; set; }
        public string AccountSubGroupName { get; set; }
        public string AccountHeadGroupName { get; set; }
        public string AccountHeadSubGroupName { get; set; }
        public string AccountHeadCode { get; set; }
        public string AccountHeadCodeName { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}

//public long? AccountGroupId { get; set; }
//public long? AccountSubGroupId { get; set; }
//public long? AccountHeadGroupId { get; set; }
//public long? AccountHeadSubGroupId { get; set; }
