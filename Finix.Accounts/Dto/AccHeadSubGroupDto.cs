﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class AccHeadSubGroupDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; } 
        public int? AccHeadGroupId { get; set; }
        public int EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public AccHeadGroupDto AccHeadGroup { get; set; }
        //public List<AccHeadDto> AccHeads { get; set; }
    }
}
