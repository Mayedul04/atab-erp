﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class FinancialStatementNotesReportDto
    {
        public int? Id { get; set; }
        public int? NoteId { get; set; }
        public string NoteName { get; set; }
        public string AccountHeadCode { get; set; }
        public string AccountHeadName { get; set; }
        public decimal? CurrentExpense { get; set; }
        public decimal? PreviousExpense { get; set; }
    }
}
