﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class GroupLedgerDto
    {
        public string LedgerAccHeadCode { get; set; }
        public string LedgerAccHeadName { get; set; }
        public string VoucherNo { get; set; }
        public DateTime? VoucherDate { get; set; }
        public string AccountHeadCode { get; set; }
        public string AccountHeadName { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public string AccGroup { get; set; }
        public string AccSubGroup { get; set; }
        public string AccHeadGroup { get; set; }
        public string AccHeadSubGroup { get; set; }
    }
}
