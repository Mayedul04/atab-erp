﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class ReportConfigDto
    {
        public long? Id { get; set; }
        public long ReportId { get; set; }
        public string ReportName { get; set; }
        public long? ParentId { get; set; }
        public string ItemName { get; set; }
        public string ItemAccGroupCode { get; set; }
        public bool IsPositive { get; set; }
        public bool HasChild { get; set; }
        public bool? GetAccHeads { get; set; }
        public long? CompanyProfileId { get; set; }
        public decimal Amount { get; set; }
        public ReportExternalFunction? ExternalFunction { get; set; }
        public int? StepCount { get; set; }
        public decimal AdditionalAmount { get; set; }
    }
}
