﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Dto
{
    public class AutofillDto
    {
        public string prefix { get; set; }
        public List<long> exclusionList { get; set; }
        public long? officeId { get; set; }
        public long? groupid { get; set; }
    }
    public class AutoKeyVal
    {
        public long? Id { get; set; }
        public string key { get; set; }
        public string value { get; set; }
    }
}
