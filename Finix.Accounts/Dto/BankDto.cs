﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class BankDto
    {
        public long Id { get; set; }
        public long? CompanyProfileId { get; set; }
        public string Name { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string AccountNo { get; set; }
        public int AccountType { get; set; }
        public string Signatories { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public int EditedBy { get; set; }
        public int Status { get; set; }

    }
}
