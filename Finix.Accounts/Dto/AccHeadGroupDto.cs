﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class AccHeadGroupDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AccSubGroupId { get; set; }
        public CashFlowStatementActivities? CashFlowStatementActivity { get; set; }
        public long EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public AccSubGroupDto AccSubGroup { get; set; }
        //public List<AccHeadSubGroupDto> AccHeadSubGroups { get; set; }
    }
}
