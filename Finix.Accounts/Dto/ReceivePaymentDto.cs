﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class ReceivePaymentDto
    {
        public string AccountCode { get; set; }
        public string AccountHead { get; set; }
        public string VoucherDetails { get; set; }
        public bool IsCredit { get; set; }
        public decimal Amount { get; set; }
        public decimal CashAmount { get; set; }
        public decimal BankAmount { get; set; }
        public DateTime? VoucherDate { get; set; }
        public string VoucherNo { get; set; }
    }
}
