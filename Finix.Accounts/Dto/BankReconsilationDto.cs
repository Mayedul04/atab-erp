﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Dto;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class BankReconsilationDto
    {
        //public long CompanyProfileId { get; set; }
        //public long BankId { get; set; }
        //public string ExpenseAccHeadCode { get; set; }
        //public DateTime ReconsilationDate { get; set; }
        //public bool IsExpense { get; set; }
        //public decimal Amount { get; set; }
        //public string Description { get; set; }
        public long? Id { get; set; }
        public long BankId { get; set; }
        public string BankName { get; set; }
        public string AccontNumber { get; set; }
        public DateTime? MonthEndingDate { get; set; }
        public string MonthEndingDateTxt { get; set; }
        public decimal ActualBankBalance { get; set; }
        public decimal AdjustedBankBalance { get; set; }
        public decimal BalancePerLedger { get; set; }
        public decimal AdjustedLedgerBalance { get; set; }
        public decimal Variance { get; set; }
        public List<BankReconsilationDetailsDto> Details { get; set; }
        public List<long> RemovedDetails { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewDate { get; set; }
        public string ReviewDateTxt { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
