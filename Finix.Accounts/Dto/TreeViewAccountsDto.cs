﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class TreeViewAccountsDto
    {
        public TreeViewAccountsDto()
        {
            ChildLocations = new HashSet<TreeViewAccountsDto>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<TreeViewAccountsDto> ChildLocations { get; set; }
    }
}
