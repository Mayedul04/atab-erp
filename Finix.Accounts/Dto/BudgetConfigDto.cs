﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Dto
{
    public class BudgetConfigDto
    {
        public long? Id { get; set; }
        public long AccGroupId { get; set; }
        public long AccHeadId { get; set; }
        public string AccHeadName { get; set; }
        public AutoKeyVal AccHead { get; set; }
        public string ControlHead { get; set; }
        public bool HasUnit { get; set; }
        public long OfficeId { get; set; }
        public EntityStatus? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
