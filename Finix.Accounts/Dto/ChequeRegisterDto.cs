﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class ChequeRegisterDto
    {
        public void ChequeRegisterDetailDto()
        {
            Details = new List<ChequeRegisterDetailDto>();
        }
        public long? Id { get; set; }
        public long? BankId { get; set; }
        public string BranchName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string BankName { get; set; }
        public long? CompanyProfileId { get; set; }
        public string CompanyProfileName { get; set; }
        public string ChequePrefix { get; set; }
        public string StartingNumber { get; set; }
        public string EndingNumber { get; set; }
        public long? TotalCheques { get; set; }
        public string BankAccountHeadCode { get; set; }
        public DateTime? BookIssueDate { get; set; }
        public IEnumerable<ChequeRegisterDetailDto> Details { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
