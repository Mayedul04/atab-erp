﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.Dto
{
    public class IUOSlipDto
    {
        public long? Id { get; set; }
        public string ReceiverName { get; set; }
        public string Designation { get; set; }
        public decimal Amount { get; set; }
        public long? CompanyProfileId { get; set; }
        public long? BankId { get; set; }
        public DateTime ReceiveDate { get; set; }
        public string ReceiveDateTxt { get; set; }
        public string Remarks { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ChequeDateTxt { get; set; }
        public IuoSlipStatus? IuoSlipStatus { get; set; }
        public IuoPurpose? IuoPurpose { get; set; }
        public string IuoPurposeName { get; set; }
        public string IuoSlipStatusName { get; set; }
        public bool? IsBank { get; set; }
        public string CreditAccHeadCode { get; set; }
        public string DebitAccHeadCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
