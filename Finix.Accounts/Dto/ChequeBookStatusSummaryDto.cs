﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class ChequeBookStatusSummaryDto
    {
        public long? CompanyProfileId { get; set; }
        public string CompanyProfileName { get; set; }
        public long? BankId { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        //public ChequeStatus ChequeStatus { get; set; }
        //public string ChequeStatusName { get; set; }
        public string ChequeStartingNumber { get; set; }
        public int TotalLeafs { get; set; }
        public int UnUsedCount { get; set; }
        public int IssuedCount { get; set; }
        public int BouncedCount { get; set; }
        public int RuinedCount { get; set; }

    }
}
