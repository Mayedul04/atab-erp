﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class VoucherDto
    {
        public long Id { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherName { get; set; }
        public DateTime VoucherDate { get; set; }
        public string VoucherDateTxt { get; set; }
        public long SerialNo { get; set; }
        public string AccountHeadCode { get; set; }
        public string AccountHeadCodeName { get; set; }
        public string BankAccountHead { get; set; }
        public AccTranType AccTranType { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Amount { get; set; }
        public bool IsCheque { get; set; }
        public long? BankId { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ChequeDateTxt { get; set; }
        public string ChequePayTo { get; set; }
        public int PaymentType { get; set; }
        public string TransactionNo { get; set; }
        public decimal Balance { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string VoucherType { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? EditedBy { get; set; }
        public int? Status { get; set; }
        public long? CompanyProfileId { get; set; }
        public IEnumerable<AccHeadDto> AccHeadDetails { get; set; }
        public RecordStatus? RecordStatus { get; set; }
        public string RecordStatusName { get; set; }
        public string UserName { get; set; }
        public DateTime? VerificationDate { get; set; }
        public long VerifiedBy { get; set; }
        public string VerifiedByName { get; set; }
        public DateTime? AuthorizationDate { get; set; }
        public long AuthorizedBy { get; set; }
        public string AuthorizedByName { get; set; }
        public DateTime? RejectionDate { get; set; }
        public string RejectedBy { get; set; }
        //public IEnumerable<AccBalanceDto> AccBalanceDetails { get; set; }

    }
}
