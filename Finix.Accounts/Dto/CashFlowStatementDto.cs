﻿using Finix.Accounts.Infrastructure;
using Finix.Accounts.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class CashFlowStatementDto
    {
        public CashFlowStatementDto()
        {

        }
        public CashFlowStatementDto(CashFlowStatementActivities activity)
        {
            this.Activity = activity;
            this.ActivityName = UiUtil.GetDisplayName(activity);
        }
        public CashFlowStatementActivities Activity { get; set; }
        public string ActivityName { get; set; }
        public string AccountHead { get; set; }
        public decimal Amount { get; set; }
        public int Number { get; set; }
    }
}
