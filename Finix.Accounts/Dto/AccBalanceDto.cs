﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class AccBalanceDto
    {
        public int Id { get; set; }
        public DateTime TranDate { get; set; }
        public string AccheadCode { get; set; }
        public string Balance { get; set; }
        public int EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
