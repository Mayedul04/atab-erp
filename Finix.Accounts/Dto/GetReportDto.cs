﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class GetReportDto
    {
        public DateTime? FromDate { get; set; }
        public string FromDateText { get; set; }
        public DateTime? ToDate { get; set; }
        public string ToDateText { get; set; }
        public List<long> CompanyProfileIds { get; set; }
        public string ReportTypeId { get; set; }
    }
}
