﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.Dto
{
    public class AccountGroupDto
    {
        public long? AccGroup {get;set;} 
        public string Code {get;set;} 
        public string Name {get;set;} 
        public long? AccGroupId {get;set;} 
        public long? AccSubGroupId {get;set;} 
        public long? CashFlowStatementActivity {get;set;} 
        public long? AccHeadGroupId {get;set;} 
    }
}
