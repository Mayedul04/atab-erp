﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Dto
{
    public class PostingStatusUpdate
    {
        public long Id { get; set; }
        public bool Pass { get; set; }
    }
}
