﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class VoucherArchieveDto
    {
        public long? AccGroupId { get; set; }
        public long? AccHeadGroupId { get; set; }
        public string AccHeadGroupName { get; set; }
        public DateTime ArchieveDate { get; set; }
        public string AccountHeadCode { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Amount { get; set; }
        public long? CompanyProfileId { get; set; }
    }
}
