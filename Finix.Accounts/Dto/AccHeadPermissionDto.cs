﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.Dto
{
    public class AccHeadPermissionDto
    {
        public List<string> Ids { get; set; }
        public long OfficeId { get; set; }
    }
}
