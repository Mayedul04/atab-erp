﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class BalanceSheetDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int AccGroupId { get; set; }
        public int AccSubGroupId { get; set; }
        public string AccSubGroupName { get; set; }

        public string AccGroupName { get; set; }
        public int AccHeadGroupId { get; set; }
        public string AccHeadGroupName { get; set; }
        public int AccHeadSubGroupId { get; set; }
        public string AccHeadSubGroupName { get; set; }

        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal CurrentYearAmount { get; set; }
        public decimal PreviousYearAmount { get; set; }
        public int EntryBy { get; set; }
        public System.DateTime EntryDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string AccHeadName { get; set; }
        public string CurrentCode { get; set; }
        public AccHeadSubGroupDto AccHeadSubGroup { get; set; }
    }
}
