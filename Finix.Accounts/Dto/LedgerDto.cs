﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class LedgerDto
    {
        public long Id { get; set; }
        public DateTime VoucherDate { get; set; }
        public string VoucherNo { get; set; }
        public string PerameterName { get; set; }
        public string PerameterCode { get; set; }
        public string AccountHeadCode { get; set; }
        public string AccountHeadCodeName { get; set; }
        public string SelectedAccountHeadCode { get; set; }
        public string SelectedAccountHeadName { get; set; }
        public string AccountGroupName { get; set; }
        public string Description { get; set; }
        public string TransactionNo { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public bool IsDebit { get; set; }
        public long? CompanyProfileId { get; set; }
        public string ChequeNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
