﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;

namespace Finix.Accounts.Dto
{
    public class BudgetDto
    {
        public long? Id { get; set; }
        public BudgetStatus? BudgetStatus { get; set; }
        public string BudgetStatusName { get; set; }
        public long? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<BudgetDetailDto> Details { get; set; }
        public long? ApprovedBy { get; set; }
        public string ApprovedByName { get; set; }
        public EntityStatus? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
    public class BudgetDetailDto
    {
        public long? Id { get; set; }
        public long? BudgetId { get; set; }
        public long BudgetConfigId { get; set; }
        public string AccHeadName { get; set; }
        public long AccGroupId { get; set; }
        public decimal Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal PreviousBudget { get; set; }
        public decimal PreviousActualAmount { get; set; }
        public decimal IncreasePercentage { get; set; }
        public decimal IncreaseAmount { get; set; }
        public decimal ProposedAmount { get; set; }
        public EntityStatus? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
