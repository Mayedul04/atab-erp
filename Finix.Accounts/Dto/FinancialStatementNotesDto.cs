﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class FinancialStatementNotesDto
    {
        public int? Note { get; set; }
        public string Year { get; set; }
        public string Name { get; set; }
        public string AccountHeadCode { get; set; }
        public decimal? Expense { get; set; }
    }
}
