﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;
using Finix.Accounts.Infrastructure.Models;

namespace Finix.Accounts.Dto
{
    public class BankReconsilationDetailsDto
    {
        public long? Id { get; set; }
        public long BRId { get; set; }
        public BankReconsilationType BankReconsilationType { get; set; }
        public string BankReconsilationTypeName { get; set; }
        public DateTime? TranDate { get; set; }
        public string TranDateTxt { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
