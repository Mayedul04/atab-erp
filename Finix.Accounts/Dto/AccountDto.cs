﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class AccountDto
    {
        public AccTranType VoucherType { get; set; }
        public string VoucherNo { get; set; } 
        public IEnumerable<VoucherDto> VoucherDetails { get; set; }
        
    }
}
