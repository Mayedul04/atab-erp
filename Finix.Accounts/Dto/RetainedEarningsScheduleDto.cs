﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class RetainedEarningsScheduleDto
    {

        public string Particular { get; set; }
        public decimal ShareCapital { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal Reserve { get; set; }
        public decimal Total { get; set; }

        public void CalculateTotal()
        {
            this.Total = this.ShareCapital + this.RetainedEarnings + this.Reserve;
        }
    }
}
