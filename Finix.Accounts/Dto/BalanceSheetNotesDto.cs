﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class BalanceSheetNotesDto
    {
        public long AccGroupId { get; set; }
        public decimal? CurrentAdd { get; set; }
        public decimal? CurrentLess { get; set; }
        public decimal? PreviousAdd { get; set; }
        public decimal? PreviousLess { get; set; }
        public decimal? PreviousOpening { get; set; }
    }
}
