﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class AccSubGroupDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AccGroupId { get; set; }
        public long EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public AccGroupDto AccGroup { get; set; }
        //public List<AccHeadGroupDto> AccHeadGroups { get; set; }
    }
}
