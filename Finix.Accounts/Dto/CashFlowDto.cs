﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class CashFlowDto
    {
        public CashFlowStatementActivities Activity { get; set; }
        public string ActivityName { get; set; }
        public string AccountHead { get; set; }
        public decimal Amount { get; set; }
        public decimal PrevAmount { get; set; }
        public int Number { get; set; }
    }
}
