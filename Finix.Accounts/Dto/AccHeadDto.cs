﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Accounts.Infrastructure;

namespace Finix.Accounts.DTO
{
    public class AccHeadDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? AccGroupId { get; set; }
        public long? AccSubGroupId { get; set; }
        public long? AccHeadGroupId { get; set; }
        public long? AccHeadSubGroupId { get; set; }
        public int? TierId { get; set; }
        public long? RefId { get; set; }
        public ReferenceAccountType? RefType { get; set; }
        public string AccHeadName { get; set; }
        public string CurrentCode { get; set; }
        public AccGroupDto AccGroup { get; set; }
        public AccSubGroupDto AccSubGroup { get; set; }
        public AccHeadDto AccHead { get; set; }
        public AccHeadSubGroupDto AccHeadSubGroup { get; set; }

        public long? CompanyProfileId { get; set; }
        public EntityStatus? Status { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public long EditedBy { get; set; }
        public DateTime? EditDate { get; set; }

    }
}
