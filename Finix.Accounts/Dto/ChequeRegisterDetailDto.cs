﻿using Finix.Accounts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Accounts.DTO
{
    public class ChequeRegisterDetailDto
    {
        public long Id { get; set; }
        public string ChequeNo { get; set; }
        public long ChequeRegisterId { get; set; }
        public ChequeStatus ChequeStatus { get; set; }
        public DateTime? IssueDate { get; set; }
        public long? IssuedBy { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string PaidTo { get; set; }
        public string PaidFor { get; set; }
        public decimal Amount { get; set; }
        public string VoucherNo { get; set; }
        public string AccHeadCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
