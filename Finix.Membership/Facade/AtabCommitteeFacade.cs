﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using AutoMapper;
using Finix.Membership.Dto;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Util;
using Microsoft.Practices.ObjectBuilder2;
using PagedList;
using ResponseDto = Finix.Auth.DTO.ResponseDto;

namespace Finix.Membership.Facade
{
    public class AtabCommitteeFacade : BaseFacade
    {
        public ResponseDto SaveAtabCommittee(ATABCommitteeDto dto, long userId)
        {
            ATABCommittee entity = new ATABCommittee();
            ResponseDto responce = new ResponseDto();
            if (dto.Id != null && dto.Id > 0)
            {
                entity = GenService.GetById<ATABCommittee>((long)dto.Id);
                //var committee = GenService.GetById<ATABCommittee>((long)entity.Id);
                dto.CreateDate = entity.CreateDate;
                dto.CreatedBy = entity.CreatedBy;
                dto.Status = entity.Status;
                dto.Photo = entity.Photo;
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        Mapper.Map(dto, entity);
                        entity.EditDate = DateTime.Now;
                        //if (dto.Address.IsChanged)
                        //{
                        //    if (dto.AddressId != null)
                        //    {
                        //        address = GenService.GetById<Address>((long)dto.AddressId);
                        //        dto.Address.CreateDate = address.CreateDate;
                        //        dto.Address.CreatedBy = address.CreatedBy;
                        //        address = Mapper.Map(dto.Address, address);
                        //        GenService.Save(address);
                        //        dto.AddressId = address.Id;
                        //    }
                        //    else
                        //    {
                        //        var permanentAddress = Mapper.Map<Address>(dto.Address);
                        //        GenService.Save(permanentAddress);
                        //        dto.AddressId = permanentAddress.Id;
                        //    }
                        //}
                        //else
                        //{
                        //    dto.AddressId = entity.AddressId;
                        //}
                        //dto.AddressId = entity.AddressId;
                        GenService.Save(entity);
                        #region list updates

                        if (dto.TenureHistoryDetails != null)
                        {
                            foreach (var item in dto.TenureHistoryDetails)
                            {

                                TenureHistory tenure = new TenureHistory();
                                if (item.Id != null && item.Id > 0)
                                {
                                    tenure = GenService.GetById<TenureHistory>((long)item.Id);
                                    item.Status = tenure.Status;
                                    item.CreateDate = tenure.CreateDate;
                                    item.CreatedBy = tenure.CreatedBy;
                                    item.MemberCommitteId = tenure.MemberCommitteId;
                                    item.CreateDate = tenure.CreateDate;
                                    item.EditDate = DateTime.Now;
                                    item.EditedBy = userId;
                                    Mapper.Map(item, tenure);
                                    GenService.Save(tenure);
                                }
                                else
                                {
                                    item.CreateDate = DateTime.Now;
                                    item.CreatedBy = userId;
                                    item.Status = EntityStatus.Active;
                                    item.MemberCommitteId = entity.Id;
                                    tenure = Mapper.Map<TenureHistory>(item);
                                    GenService.Save(tenure);
                                }

                            }
                        }

                        #endregion

                        #region list deletes
                        if (dto.RemovedTenures != null)
                        {
                            foreach (var item in dto.RemovedTenures)
                            {
                                var fund = GenService.GetById<TenureHistory>(item);
                                if (fund != null)
                                {
                                    fund.Status = EntityStatus.Inactive;
                                    fund.EditDate = DateTime.Now;
                                    fund.EditedBy = userId;
                                }
                                GenService.Save(fund);
                            }
                        }
                        #endregion
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        responce.Message = "Atab Committee Save Failed";
                        return responce;
                    }
                }
                responce.Id = entity.Id;
                responce.Success = true;
                responce.Message = "Atab Committee Edited Successfully";
                return responce;
            }
            else
            {

                entity = Mapper.Map<ATABCommittee>(dto);
                entity.Status = EntityStatus.Active;
                entity.CreatedBy = userId;
                entity.CreateDate = DateTime.Now;
                // var presentAddress = Mapper.Map<Address>(dto.Address);
                using (var tran = new TransactionScope())
                {
                    try
                    {
                        #region populate list
                        if (dto.TenureHistoryDetails != null && dto.TenureHistoryDetails.Count > 0)
                        {
                            entity.TenureHistoryDetails = Mapper.Map<List<TenureHistory>>(dto.TenureHistoryDetails);
                            foreach (var item in entity.TenureHistoryDetails)
                            {
                                item.CreateDate = DateTime.Now;
                                item.CreatedBy = userId;
                                item.Status = EntityStatus.Active;
                            }
                        }
                        //if (presentAddress.CountryId != null)
                        //{
                        //    GenService.Save(presentAddress);
                        //    entity.AddressId = presentAddress.Id;
                        //}
                        #endregion
                        GenService.Save(entity);
                        tran.Complete();
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        responce.Message = "Atab Committee Save Failed";
                        return responce;
                    }
                }

                responce.Id = entity.Id;
                responce.Success = true;
                responce.Message = "Atab Committee Saved Successfully";
                return responce;
            }
        }
        public ResponseDto UploadPicture(UploadDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            string filePath = "~/Content/image/AtabCommittee/";//"~/Uploaded/ProfilePicture/";
            string path = HttpContext.Current.Server.MapPath(filePath);
            var com = GenService.GetById<ATABCommittee>((long)dto.Id);
            try
            {
                if (dto.PhotoFile != null)
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    string customfileName = "Photo_" + com.FirstName;
                    string fileExt = Path.GetExtension(dto.PhotoFile.FileName);
                    path = Path.Combine(path, customfileName + fileExt);
                    //path = Path.Combine(path, dto.Photo.FileName);
                    for (int i = 1; File.Exists(path);)
                    {
                        var length = path.Length;
                        if (!string.IsNullOrEmpty(fileExt))
                            path = path.Substring(0, (length - 3 - fileExt.Length)) + (++i).ToString("000") + fileExt;
                        else
                            path = path.Substring(0, (length - 3)) + (++i).ToString("000");
                    }
                    //path = Path.Combine(path, dto.Photo.FileName);
                    dto.PhotoFile.SaveAs(path);
                    com.Photo = path;
                }
                GenService.Save(com);
                response.Message = "Photo Uploaded";
            }
            catch (Exception)
            {

            }
            return response;
        }
        public ATABCommitteeDto LoadAtabCommittee(long id)
        {
            var data = GenService.GetById<ATABCommittee>(id);
            var result = Mapper.Map<ATABCommitteeDto>(data);
            result.PhotoName = result.Photo != null ? Path.GetFileName(result.Photo) : "";
            result.TenureHistoryDetails = Mapper.Map<List<TenureHistoryDto>>(data.TenureHistoryDetails);
            result.TenureHistoryDetails.RemoveAll(f => f.Status != EntityStatus.Active);
            result.LoadMemberautofilLoad = new MemberautofilLoadDto();//result.Member.(x => new MemberautofilLoadDto { Id = x.Id, NameOfOrganization = x.NameOfOrganization, PresentAddressId = x.PresentAddressId })
            var dt = new MemberautofilLoadDto();
            dt.key = (long)result.MemberId;
            dt.value = result.Member != null ? result.Member.NameOfOrganization : "";
            dt.address = result.AddressId != null ? (long)result.AddressId : 0;
            result.LoadMemberautofilLoad = dt;
            return result;
        }
        public IPagedList<ATABCommitteeDto> AtabCommitteeList(int pageSize, int pageCount, string searchString)
        {
            var slips = GenService.GetAll<ATABCommittee>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                slips = slips.Where(m => m.FirstName.ToLower().Contains(searchString));
            }
            var slipList = from slip in slips
                           select new ATABCommitteeDto()
                           {
                               Id = slip.Id,
                               FirstName = slip.FirstName,
                               LastName = slip.LastName,
                               Remarks = slip.Remarks,

                           };
            var temp = slipList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }
        public List<MemberDto> GetMemberAutoFill(string prefix, List<long> exclusionList)
        {
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();
            var data = GenService.GetAll<Member>()
                .Where(x => x.Status == EntityStatus.Active && (x.NameOfOrganization.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id))
                .OrderBy(x => x.Id)
                .Select(x => new MemberDto { Id = x.Id, NameOfOrganization = x.NameOfOrganization, PresentAddressId = x.PresentAddressId })
                .Take(20)
                .ToList();
            return data;
        }

        public AddressDto GetAddressById(long addressId)
        {
            var address = GenService.GetById<Address>(addressId);
            var data = Mapper.Map<AddressDto>(address);
            return data;
        }

        public IPagedList<ATABCommitteeDetailsDto> AtabCommitteeReportList(string range, int pageSize, int pageCount, string searchString, long? sourceid) /*DateTime? fromDate, DateTime? toDate,*/
        {
            var committees = GenService.GetAll<ATABCommittee>().Where(m => m.Status == EntityStatus.Active);
            var tenureHistories = GenService.GetAll<TenureHistory>().Where(m => m.Status == EntityStatus.Active);

            var slipList = (from committee in committees
                            join tenure in tenureHistories on committee.Id equals tenure.MemberCommitteId
                            select new ATABCommitteeDetailsDto()
                            {
                                Id = committee.Id,

                                MemberId = committee.MemberId,
                                NameOfOrganization = committee.Member != null ? committee.Member.NameOfOrganization : "",
                                FirstName = committee.FirstName,
                                LastName = committee.LastName,
                                FullName = committee.FirstName + " " + committee.LastName,
                                Zone = tenure.Zone,
                                ZoneName = tenure.Zone.ToString(),//"",//UiUtil.GetDisplayName(tenure.Zone) ,//tenure.Zone.ToString(),
                                Post = tenure.Post,
                                PostName = "",//UiUtil.GetDisplayName(tenure.Post),
                                CommitteeType = tenure.CommitteeType,
                                CommitteeTypeName = "",//UiUtil.GetDisplayName(tenure.CommitteeType),
                                JoiningDate = tenure.JoiningDate,
                                EndOfTenure = tenure.EndOfTenure

                            }).ToList();
            //slipList.ForEach(r => r.ZoneName = UiUtil.GetDisplayName(r.Zone));
            slipList.ForEach(r => r.CommitteeTypeName = UiUtil.GetDisplayName(r.CommitteeType));
            slipList.ForEach(r => r.PostName = UiUtil.GetDisplayName(r.Post));
            slipList.ForEach(r => r.YearRange = (r.JoiningDate != null ? DateTime.Parse(r.JoiningDate.ToString()).Year.ToString() : "") + "-" + (r.EndOfTenure != null ? DateTime.Parse(r.EndOfTenure.ToString()).Year.ToString() : ""));
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                slipList = slipList.Where(m => m.CommitteeTypeName.ToLower().Contains(searchString) ||
                m.ZoneName.ToLower().Contains(searchString) || m.PostName.ToLower().Contains(searchString) || m.FullName.ToLower().Contains(searchString)).ToList();
            }
            if (!string.IsNullOrEmpty(range))
            {
                slipList = slipList.Where(m => m.YearRange.ToLower().Contains(range)).ToList();
            }
            if (sourceid != null)
            {
                slipList = slipList.Where(m => m.CommitteeType == (CommitteeType)sourceid).ToList();
            }
            var temp = slipList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }

        public List<ATABCommitteeDetailsDto> GetAtabCommitteeReportList(string search, long? type, string range) /*DateTime? fromDate, DateTime? toDate,*/
        {
            var committees = GenService.GetAll<ATABCommittee>().Where(m => m.Status == EntityStatus.Active);
            var tenureHistories = GenService.GetAll<TenureHistory>().Where(m => m.Status == EntityStatus.Active);

            var slipList = (from committee in committees
                            join tenure in tenureHistories on committee.Id equals tenure.MemberCommitteId
                            select new ATABCommitteeDetailsDto()
                            {
                                Id = committee.Id,

                                MemberId = committee.MemberId,
                                NameOfOrganization = committee.Member != null ? committee.Member.NameOfOrganization : "",
                                FirstName = committee.FirstName,
                                LastName = committee.LastName,
                                FullName = committee.FirstName + " " + committee.LastName,
                                Zone = tenure.Zone,
                                ZoneName = tenure.Zone.ToString(),//"",//UiUtil.GetDisplayName(tenure.Zone) ,//tenure.Zone.ToString(),
                                Post = tenure.Post,
                                PostName = "",//UiUtil.GetDisplayName(tenure.Post),
                                CommitteeType = tenure.CommitteeType,
                                CommitteeTypeName = "",//UiUtil.GetDisplayName(tenure.CommitteeType),
                                JoiningDate = tenure.JoiningDate,
                                EndOfTenure = tenure.EndOfTenure

                            }).ToList();
            //slipList.ForEach(r => r.ZoneName = UiUtil.GetDisplayName(r.Zone));
            slipList.ForEach(r => r.CommitteeTypeName = UiUtil.GetDisplayName(r.CommitteeType));
            slipList.ForEach(r => r.PostName = UiUtil.GetDisplayName(r.Post));
            slipList.ForEach(r => r.YearRange = (r.JoiningDate != null ? DateTime.Parse(r.JoiningDate.ToString()).Year.ToString() : "") + "-" + (r.EndOfTenure != null ? DateTime.Parse(r.EndOfTenure.ToString()).Year.ToString() : ""));

            if (!string.IsNullOrEmpty(search) && search != "NaN")
            {
                search = search.ToLower();
                slipList = slipList.Where(m => m.CommitteeTypeName.ToLower().Contains(search) ||
                m.ZoneName.ToLower().Contains(search) || m.PostName.ToLower().Contains(search) || m.FullName.ToLower().Contains(search)).ToList();
            }
            if (type > 0)
            {
                slipList = slipList.Where(m => m.CommitteeType == (CommitteeType)type).ToList();
            }
            if (!string.IsNullOrEmpty(range))
            {
                slipList = slipList.Where(m => m.YearRange.ToLower().Contains(range)).ToList();
            }
            return slipList;
        }
        public List<YearRange> GetAllAtabCommitteeList() /*DateTime? fromDate, DateTime? toDate,*/
        {
            var committees = GenService.GetAll<ATABCommittee>().Where(m => m.Status == EntityStatus.Active);
            var tenureHistories = GenService.GetAll<TenureHistory>().Where(m => m.Status == EntityStatus.Active);

            var slipList = (from committee in committees
                            join tenure in tenureHistories on committee.Id equals tenure.MemberCommitteId
                            select new ATABCommitteeDetailsDto()
                            {
                                Id = committee.Id,

                                MemberId = committee.MemberId,
                                NameOfOrganization = committee.Member != null ? committee.Member.NameOfOrganization : "",
                                FirstName = committee.FirstName,
                                LastName = committee.LastName,
                                FullName = committee.FirstName + " " + committee.LastName,
                                Zone = tenure.Zone,
                                ZoneName = tenure.Zone.ToString(),//"",//UiUtil.GetDisplayName(tenure.Zone) ,//tenure.Zone.ToString(),
                                Post = tenure.Post,
                                PostName = "",//UiUtil.GetDisplayName(tenure.Post),
                                CommitteeType = tenure.CommitteeType,
                                CommitteeTypeName = "",//UiUtil.GetDisplayName(tenure.CommitteeType),
                                JoiningDate = tenure.JoiningDate,
                                EndOfTenure = tenure.EndOfTenure

                            }).ToList();
            slipList.ForEach(r => r.CommitteeTypeName = UiUtil.GetDisplayName(r.CommitteeType));
            slipList.ForEach(r => r.PostName = UiUtil.GetDisplayName(r.Post));
            slipList.ForEach(r => r.YearRange = (r.JoiningDate != null ? DateTime.Parse(r.JoiningDate.ToString()).Year.ToString() : "") + "-" + (r.EndOfTenure != null ? DateTime.Parse(r.EndOfTenure.ToString()).Year.ToString() : ""));
            var result = new List<YearRange>();
            foreach (var atabCommitteeDetailsDto in slipList)
            {
                var check = result.Where(m => m.Range.ToLower().Contains(atabCommitteeDetailsDto.YearRange));
                if (!result.Where(m => m.Range.ToLower().Contains(atabCommitteeDetailsDto.YearRange)).Any()) //result.Where(m => m.Range.ToLower().Contains(atabCommitteeDetailsDto.YearRange)) != null
                {
                    var aResult = new YearRange();
                    aResult.Range = atabCommitteeDetailsDto.YearRange;
                    result.Add(aResult);
                }
            }
            return result;
        }
    }

}
