﻿using AutoMapper;
using Finix.Membership.Dto;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Util;

namespace Finix.Membership.Facade.AutoMaps
{
    public class MembershipMappingProfile : Profile
    {
        protected override void Configure()
        {
            //base.Configure();

            CreateMap<Association, AssociationDto>();
            CreateMap<AssociationDto, Association>();

            CreateMap<Chamber, ChamberDto>();
            CreateMap<ChamberDto, Chamber>();

            CreateMap<DocumentType, DocumentTypeDto>();
            CreateMap<DocumentTypeDto, DocumentType>();

            CreateMap<FeeType, FeeTypeDto>();
            CreateMap<FeeTypeDto, FeeType>();

            CreateMap<AccountsMapping, AccHeadMappingDto>();
            CreateMap<AccHeadMappingDto, AccountsMapping>();

            CreateMap<BusinessCategory, BusinessCategoryDto>();
            CreateMap<BusinessCategoryDto, BusinessCategory>();

            CreateMap<Country, CountryDto>();
            CreateMap<CountryDto, Country>();

            CreateMap<Division, DivisionDto>();
            CreateMap<DivisionDto, Division>();

            CreateMap<District, DistrictDto>();
            CreateMap<DistrictDto, District>();

            CreateMap<Thana, ThanaDto>();
            CreateMap<ThanaDto, Thana>();

            CreateMap<Area, AreaDto>();
            CreateMap<AreaDto, Area>();

            CreateMap<Member, MemberDto>()
                .ForMember(d => d.ZoneName, o => o.MapFrom(s => s.Zone != null ? UiUtil.GetDisplayName(s.Zone) : ""))
                .ForMember(d => d.OwnershipStatusName, o => o.MapFrom(s => s.OwnershipStatus != null ? UiUtil.GetDisplayName(s.OwnershipStatus) : ""))
                .ForMember(d => d.TradeLicenseFile, o => o.Ignore())
                 .ForMember(d => d.RenewalFile, o => o.Ignore())
                  .ForMember(d => d.IncorporationFile, o => o.Ignore())
                   .ForMember(d => d.RepresentativePhotoFile, o => o.Ignore());
            CreateMap<MemberDto, Member>()
                 .ForMember(d => d.BusinessCategories, o => o.Ignore())
                  .ForMember(d => d.Associations, o => o.Ignore())
                   .ForMember(d => d.Chambers, o => o.Ignore());

            CreateMap<Trainee, TraineeDto>()
                .ForMember(d => d.GenderName, o => o.MapFrom(s => s.Gender > 0 ? s.Gender.ToString() : ""))
                .ForMember(d => d.ReligionName, o => o.MapFrom(s => s.Religion > 0 ? s.Religion.ToString() : ""))
                .ForMember(d => d.SourceOfInformationName, o => o.MapFrom(s => s.SourceOfInformation > 0 ? s.SourceOfInformation.ToString() : ""));
            CreateMap<TraineeDto, Trainee>()
                .ForMember(d => d.Courses, o => o.Ignore());

            CreateMap<Owner, OwnerDto>();
            //.ForMember(d => d.Photo, d => d.Ignore());
            CreateMap<OwnerDto, Owner>();

            // CreateMap<RequisitionTran, Requisition>()
            //     .ForMember(d => d.RequisitionDetails, o => o.Ignore());

            CreateMap<Address, AddressDto>();
            CreateMap<AddressDto, Address>();

            CreateMap<Address, TraineeAddressDto>()
            .ForMember(d => d.CountryName, o => o.MapFrom(s => s.CountryId > 0 ? s.Country.Name : ""))
            .ForMember(d => d.DivisionNameEng, o => o.MapFrom(s => s.DistrictId > 0 ? s.Division.DivisionNameEng : ""))
            .ForMember(d => d.DistrictNameEng, o => o.MapFrom(s => s.DistrictId > 0 ? s.District.DistrictNameEng : ""))
            .ForMember(d => d.ThanaNameEng, o => o.MapFrom(s => s.ThanaId > 0 ? s.Thana.ThanaNameEng : ""));
            CreateMap<TraineeAddressDto, Address>();

            CreateMap<TraineeCourses, TraineeCoursesDto>()
                .ForMember(d => d.CourseName, o => o.MapFrom(s => s.Course > 0 ? s.Courses.Name : ""));
            CreateMap<TraineeCoursesDto, TraineeCourses>();

            CreateMap<FeeCollectionTran, FeeCollectionTranDto>();
            CreateMap<FeeCollectionTranDto, FeeCollectionTran>();

            CreateMap<Attachment, AttachmentDto>();
            CreateMap<AttachmentDto, Attachment>();

            CreateMap<MemberBusinessCatagories, MemberBusinessCatagoriesDto>();
            CreateMap<MemberBusinessCatagoriesDto, MemberBusinessCatagories>();

            CreateMap<MemberAssociations, MemberAssociationsDto>();
            CreateMap<MemberAssociationsDto, MemberAssociations>();

            CreateMap<MemberChambers, MemberChambersDto>();
            CreateMap<MemberChambersDto, MemberChambers>();

            CreateMap<MemberPendingFee, MemberPendingFeeDto>();
            CreateMap<MemberPendingFeeDto, MemberPendingFee>();

            CreateMap<MoneyReceipt, MoneyReceiptDto>()
                .ForMember(d => d.MemberName, o => o.MapFrom(s => s.Member != null ? s.Member.NameOfOrganization != null ? s.Member.NameOfOrganization : null : ""));
            CreateMap<MoneyReceiptDto, MoneyReceipt>();

            CreateMap<MoneyReceiptDetails, MoneyReceiptDetailsDto>();
            CreateMap<MoneyReceiptDetailsDto, MoneyReceiptDetails>();

            CreateMap<CourseFeeCollectionTran, CourseFeeCollectionDto>();
            CreateMap<CourseFeeCollectionDto, CourseFeeCollectionTran>();

            CreateMap<TraineeMoneyReceipt, TraineeMoneyReceiptDto>()
                .ForMember(d => d.TraineeName, o => o.MapFrom(s => s.Trainee != null ? s.Trainee.Name != null ? s.Trainee.Name : null : ""));
            CreateMap<TraineeMoneyReceiptDto, TraineeMoneyReceipt>();

            CreateMap<TraineeMoneyReceiptDetails, TraineeMoneyReceiptDetailsDto>();
            CreateMap<TraineeMoneyReceiptDetailsDto, TraineeMoneyReceiptDetails>();

            CreateMap<ATABCommittee, ATABCommitteeDto>()
                 .ForMember(d => d.NameOfOrganization, o => o.MapFrom(s => s.Member != null ? s.Member.NameOfOrganization != null ? s.Member.NameOfOrganization : null : ""))
                 .ForMember(d => d.PhotoFile, o => o.Ignore())
                 .ForMember(d => d.TenureHistoryDetails, o => o.Ignore());
                 //.ForMember(d => d.Member, o => o.Ignore());
            CreateMap<ATABCommitteeDto, ATABCommittee>()
                .ForMember(d => d.TenureHistoryDetails, o => o.Ignore())
                .ForMember(d => d.Member, o => o.Ignore());

            CreateMap<TenureHistory, TenureHistoryDto>();
            CreateMap<TenureHistoryDto, TenureHistory>()
                 .ForMember(d => d.ATABCommittee, o => o.Ignore());

            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();

            CreateMap<StockPosition, StockPositionDto>()
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.Product.Name));
            CreateMap<StockPositionDto, StockPosition>();

            CreateMap<StockTran, StockTranDto>();
            CreateMap<StockTranDto, StockTran>()
                .ForMember(d => d.Details, o => o.Ignore());

            CreateMap<StockTranDetail, StockTranDetailDto>()
                .ForMember(d => d.ProductName, o => o.MapFrom(s => s.Product.Name));
            CreateMap<StockTranDetailDto, StockTranDetail>();

            CreateMap<CertificateRegister, CertificateRegisterDto>();
            CreateMap<CertificateRegisterDto, CertificateRegister>()
                .ForMember(d => d.Details, o => o.Ignore());

            CreateMap<CertificateRegisterDetail, CertificateRegisterDetailDto>()
                .ForMember(d => d.CertificateType, o => o.MapFrom(s => s.CertificateRegister !=null  ? s.CertificateRegister.CertificateType : 0))
                .ForMember(d => d.CertificateTypeName, o => o.MapFrom(s => s.CertificateRegister.CertificateType > 0 ? UiUtil.GetDisplayName(s.CertificateRegister.CertificateType) : ""))
                .ForMember(d => d.CertificateStatusName, o => o.MapFrom(s => s.CertificateStatus > 0 ? UiUtil.GetDisplayName(s.CertificateStatus) : ""));
            CreateMap<CertificateRegisterDetailDto, CertificateRegisterDetail>();
                
        }
    }
}