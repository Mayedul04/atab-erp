﻿using AutoMapper;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Facade
{
    public class SettingsFacade : BaseFacade
    {
        public List<AssociationDto> GetAssociations()
        {
            var data = GenService.GetAll<Association>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<AssociationDto>>(data.ToList());
        }
        public ResponseDto SaveAssociation(AssociationDto dto, long? userId)
        {
            var entity = new Association();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Association>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Association Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Association>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Association Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Association Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }
        public List<ChamberDto> GetChambers()
        {
            var data = GenService.GetAll<Chamber>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<ChamberDto>>(data.ToList());
        }
        public ResponseDto SaveChamber(ChamberDto dto, long? userId)
        {
            var entity = new Chamber();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Chamber>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Chamber Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Chamber>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Chamber Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Chamber Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<DocumentTypeDto> GetDocumentTypes()
        {
            var data = GenService.GetAll<DocumentType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<DocumentTypeDto>>(data.ToList());
        }
        public ResponseDto SaveDocumentType(DocumentTypeDto dto, long? userId)
        {
            var entity = new DocumentType();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<DocumentType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Document Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<DocumentType>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Document Type Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Document Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<FeeTypeDto> GetFeeTypes()
        {
            var data = GenService.GetAll<FeeType>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<FeeTypeDto>>(data.ToList());
        }
        public ResponseDto SaveFeeType(FeeTypeDto dto, long? userId)
        {
            var entity = new FeeType();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<FeeType>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Fee Type Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<FeeType>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Fee Type Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Fee Type Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<BusinessCategoryDto> GetBusinessCategories()
        {
            var data = GenService.GetAll<BusinessCategory>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<BusinessCategoryDto>>(data.ToList());
        }
        public ResponseDto SaveBusinessCategory(BusinessCategoryDto dto, long? userId)
        {
            var entity = new BusinessCategory();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<BusinessCategory>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Business Category Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<BusinessCategory>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Business Category Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Business Category Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public List<CountryDto> GetCountryList()
        {
            var data = GenService.GetAll<Country>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<CountryDto>>(data.ToList());
        }
        public List<DivisionDto> GetDivisionList(long countryId)
        {
            var data = GenService.GetAll<Division>().Where(x => x.CountryId == countryId)
                .Select(r => new DivisionDto()
                {
                    Id = r.Id,
                    DivisionNameEng = r.DivisionNameEng
                })
                .ToList();
            return data;
        }

        public List<DistrictDto> GetDistrictList(long divisionId)
        {
            var data = GenService.GetAll<District>().Where
                (c => c.Status == EntityStatus.Active && c.DivisionId == divisionId);
            return Mapper.Map<List<DistrictDto>>(data.ToList());
        }
        public List<ThanaDto> GetThanaList(long districtId)
        {
            var data = GenService.GetAll<Thana>().Where
                (c => c.Status == EntityStatus.Active && c.DistrictId == districtId);
            return Mapper.Map<List<ThanaDto>>(data.ToList());
        }
        public List<AreaDto> GetAreaList(long thanaId)
        {
            var data = GenService.GetAll<Area>().Where
                (c => c.Status == EntityStatus.Active && c.ThanaId == thanaId);
            return Mapper.Map<List<AreaDto>>(data.ToList());
        }
        public List<BusinessCategoryDto> GetBusinessCategoryList()
        {
            var data = GenService.GetAll<BusinessCategory>().Where
                (c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<BusinessCategoryDto>>(data.ToList());
        }
        public List<ChamberDto> GetChamberList()
        {
            MemberChambersDto mc = new MemberChambersDto();
            var chember = GenService.GetAll<Chamber>().Where
                (c => c.Status == EntityStatus.Active).ToList();
            var memberchem = GenService.GetAll<MemberChambers>().Where(x => x.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<ChamberDto>>(chember.ToList());
        }
        public List<AssociationDto> GetAssociationList()
        {
            var data = GenService.GetAll<Association>().Where
                (c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<AssociationDto>>(data.ToList());
        }
        public ResponseDto SaveMembership(MemberDto dto, long? userId)
        {
            var entity = new Member();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Member>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Member Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Member>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Member Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Business Category Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }



        public List<AreaDto> GetAreas()
        {
            var data = GenService.GetAll<Area>().Where(c => c.Status == EntityStatus.Active);
            return Mapper.Map<List<AreaDto>>(data.ToList());
        }

        public List<ThanaDto> GetThanaName()
        {
            var orgs = GenService.GetAll<Thana>();
            return Mapper.Map<List<ThanaDto>>(orgs.ToList());
        }
        public List<ThanaDto> GetThanaListByDistrict(long distId)
        {
            var orgs = GenService.GetAll<Thana>().Where(d=>d.DistrictId== distId);
            return Mapper.Map<List<ThanaDto>>(orgs.ToList());
        }

        public ResponseDto SaveArea(AreaDto dto, long? userId)
        {
            var entity = new Area();
            ResponseDto responce = new ResponseDto();

            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Area>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Area Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Area>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Area Saved Successfully";
                }
            }

            catch (Exception ex)
            {
                responce.Message = "Area Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public ResponseDto SaveCertificateRegister(CertificateRegisterDto dto, long? userId)
        {
            var entity = new CertificateRegister();
            ResponseDto responce = new ResponseDto();
            try
            {

                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<CertificateRegister>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Register Edited Successfully";
                }
                else
                {
                    var details = new List<CertificateRegisterDetail>();
                    long ChequeSequence = 0;
                    try
                    {
                        ChequeSequence = Convert.ToInt64(dto.StartingNumber);
                    }
                    catch (Exception)
                    {
                        responce.Success = true;
                        responce.Message = "Register Save Failed";
                    }
                    entity = Mapper.Map<CertificateRegister>(dto);
                    entity.CreatedBy = userId;
                    entity.CreateDate = DateTime.Now;
                    int chequeNoLength = dto.StartingNumber.Length;

                    for (int i = 0; i < dto.TotalPages; i++)
                    {
                        var detail = new CertificateRegisterDetail();
                        detail.CertificateNo = dto.Prefix + ChequeSequence.ToString("D" + chequeNoLength.ToString());
                        ChequeSequence++;
                        detail.CreateDate = entity.CreateDate;
                        detail.CreatedBy = userId;
                        detail.CertificateStatus = CertificateStatus.UnUsed;
                        details.Add(detail);
                    }

                    entity.Details = Mapper.Map<List<CertificateRegisterDetail>>(details);
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Register Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                responce.Message = "Register Save Failed";
            }
           // GenService.SaveChanges();
            return responce;
        }

        public List<CertificateRegisterDto> GetCertificateRegisterList()
        {
            var data = GenService.GetAll<CertificateRegister>().Where(b => b.Status == EntityStatus.Active);
            return Mapper.Map<List<CertificateRegisterDto>>(data.ToList());
        }

        public List<CertificateRegisterStatusSummaryDto> GetCertificateBookStatusSummary(long? CompanyProfileId, CertificateType? certype)
        {
            var certificateBooks = GenService.GetAll<CertificateRegister>();
            if (CompanyProfileId != null && CompanyProfileId > 0)
                certificateBooks = certificateBooks.Where(c => c.CompanyProfileId == CompanyProfileId);
            if (certype != null && certype > 0)
                certificateBooks = certificateBooks.Where(c => c.CertificateType == certype);
            

            var summary = (from certificate in certificateBooks
                           select new CertificateRegisterStatusSummaryDto
                           {
                               CertificateName = certificate.CertificateName,
                               CertificateTypeName = certificate.CertificateName.ToString(),
                               VoidCount = certificate.Details.Where(cd => cd.CertificateStatus == CertificateStatus.Void).Count(),
                               StartingNumber = certificate.Prefix + certificate.StartingNumber,
                               CompanyProfileId = certificate.CompanyProfileId,
                               IssuedCount = certificate.Details.Where(cd => cd.CertificateStatus == CertificateStatus.Issued).Count(),
                               RuinedCount = certificate.Details.Where(cd => cd.CertificateStatus == CertificateStatus.Ruined).Count(),
                               UnUsedCount = certificate.Details.Where(cd => cd.CertificateStatus == CertificateStatus.UnUsed).Count(),
                               TotalPages = (int)certificate.TotalPages
                           }).ToList();

            //Util.UiUtil.GetDisplayName(certificate.CertificateType)
            return summary;
        }
    }
}
