﻿using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Auth.Facade;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Service;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace Finix.Membership.Facade
{
    public class MemberFacade : BaseFacade
    {
        private readonly MembeshipAccountFacade _accounts = new MembeshipAccountFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        private readonly SequencerFacade _seqFacade = new SequencerFacade();
        public string commaSeperatedValue(List<long> Ids, bool coma)
        {
            var value = "";
            foreach (var i in Ids)
            {
                if (coma == true)
                {
                    value += i;
                    coma = false;
                }
                else
                {
                    value += "," + i;
                }

            }
            return value;
        }

        public List<MemberDto> GetMemberListForAutoFill(string prefix, List<long> exclusionList)
        {
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();
            var data = GenService.GetAll<Member>()
                .Where(x => (x.NameOfOrganization.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id))
                .OrderBy(x => x.NameOfOrganization)
                .Select(x => new MemberDto { Id = x.Id, NameOfOrganization = x.NameOfOrganization })
                .Take(10)
                .ToList();
            return data;
        }
        public long SaveMember(MemberDto dto, long userId)
        {

            ResponseDto response = new ResponseDto();
            long MemberId = 0;
            var entity = new Member();
            string path = "";
            //bool coma = true;
            #region edit
            if (dto.Id > 0)
            {
                entity = GenService.GetById<Member>(dto.Id);
                var bcList = entity.BusinessCategories;
                dto.PresentAddressId = entity.PresentAddressId;
                dto.ParmanentAddressId = entity.ParmanentAddressId;
                dto.TradeLicenseFilePath = entity.TradeLicenseFile;
                dto.RenewalFilePath = entity.RenewalFile;
                dto.IncorporationFilePath = entity.IncorporationFile;
                dto.RepresentativePhotoFilePath = entity.RepresentativePhotoFile;
                if (dto.FromYear < DateTime.Today.Year)
                {
                    DateTime theDate = new DateTime(dto.FromYear, 1, 1);
                    dto.IssueDate = theDate;
                }
                //dto.BusinessCategories = ;
                Mapper.Map(dto, entity);
                
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                entity.Status = EntityStatus.Active;
                entity.TradeLicenseFile = dto.TradeLicenseFilePath;
                entity.IncorporationFile = dto.IncorporationFilePath;
                entity.RenewalFile = dto.RenewalFilePath;
                entity.RepresentativePhotoFile = dto.RepresentativePhotoFilePath;
                List<MemberBusinessCatagories> memberBusinessCatagoryList = new List<MemberBusinessCatagories>();
                List<MemberChambers> memberChamberList = new List<MemberChambers>();
                List<MemberAssociations> memberAssociationList = new List<MemberAssociations>();

                MemberBusinessCatagories memberBusinessCatagory;
                MemberChambers memberChamber;
                MemberAssociations memberAssociation;
                List<long> cmRemove = new List<long>();
                List<long> bsRemove = new List<long>();
                List<long> asRemove = new List<long>();
                //GenService.Save(entity);

                #region Member Business Categories Edit
                if (entity.BusinessCategories.Count > 0)
                {
                    foreach (var l in entity.BusinessCategories)
                    {
                        bsRemove.Add((long)l.BusinessCategoryId);
                    }

                    foreach (var b in dto.BusinessCategoryIds)
                    {
                        foreach (var c in entity.BusinessCategories)
                        {
                            if (b == c.BusinessCategoryId)
                            {
                                bsRemove.Remove((long)c.BusinessCategoryId);
                                continue;

                            }

                        }
                    }
                    foreach (var b in bsRemove)
                    {
                        memberBusinessCatagory = GenService.GetAll<MemberBusinessCatagories>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.BusinessCategoryId == b).FirstOrDefault();
                        memberBusinessCatagory.Status = EntityStatus.Inactive;
                        memberBusinessCatagory.EditDate = DateTime.Now;
                        memberBusinessCatagory.EditedBy = userId;
                        GenService.Save(memberBusinessCatagory);
                    }
                }

                if (dto.BusinessCategoryIds != null)
                {
                    foreach (var b in dto.BusinessCategoryIds)
                    {
                        memberBusinessCatagory = GenService.GetAll<MemberBusinessCatagories>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.BusinessCategoryId == b).FirstOrDefault();
                        if (memberBusinessCatagory == null)
                        {
                            memberBusinessCatagory = new MemberBusinessCatagories();
                            memberBusinessCatagory.Status = EntityStatus.Active;
                            memberBusinessCatagory.CreateDate = DateTime.Now;
                            memberBusinessCatagory.CreatedBy = userId;
                            memberBusinessCatagory.BusinessCategoryId = b;
                            memberBusinessCatagory.MemberId = dto.Id;
                            GenService.Save(memberBusinessCatagory);
                        }
                        else
                        {
                            if (memberBusinessCatagory.Status == EntityStatus.Inactive)
                            {
                                memberBusinessCatagory.Status = EntityStatus.Active;
                                memberBusinessCatagory.EditDate = DateTime.Now;
                                memberBusinessCatagory.EditedBy = userId;
                                memberBusinessCatagory.BusinessCategoryId = b;
                                memberBusinessCatagory.MemberId = dto.Id;
                                GenService.Save(memberBusinessCatagory);
                            }

                        }

                    }
                }
                

                #endregion

                #region Member Chambers Edit

                if (entity.Chambers.Count > 0)
                {
                    foreach (var l in entity.Chambers)
                    {
                        cmRemove.Add((long)l.ChamberId);
                    }

                    foreach (var b in dto.ChamberIds)
                    {
                        foreach (var c in entity.Chambers)
                        {
                            if (b == c.ChamberId)
                            {
                                cmRemove.Remove((long)c.ChamberId);
                                continue;

                            }

                        }
                    }
                    foreach (var b in cmRemove)
                    {
                        memberChamber = GenService.GetAll<MemberChambers>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.ChamberId == b).FirstOrDefault();
                        memberChamber.Status = EntityStatus.Inactive;
                        memberChamber.EditDate = DateTime.Now;
                        memberChamber.EditedBy = userId;
                        GenService.Save(memberChamber);
                    }
                }
                if (dto.ChamberIds != null)
                {
                    foreach (var b in dto.ChamberIds)
                    {
                        memberChamber = GenService.GetAll<MemberChambers>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.ChamberId == b).FirstOrDefault();
                        if (memberChamber == null)
                        {
                            memberChamber = new MemberChambers();
                            memberChamber.Status = EntityStatus.Active;
                            memberChamber.CreateDate = DateTime.Now;
                            memberChamber.CreatedBy = userId;
                            memberChamber.ChamberId = b;
                            memberChamber.MemberId = dto.Id;
                            GenService.Save(memberChamber);
                        }
                        else
                        {
                            if (memberChamber.Status == EntityStatus.Inactive)
                            {
                                memberChamber.Status = EntityStatus.Active;
                                memberChamber.EditDate = DateTime.Now;
                                memberChamber.EditedBy = userId;
                                memberChamber.ChamberId = b;
                                memberChamber.MemberId = dto.Id;
                                GenService.Save(memberChamber);
                            }

                        }

                    }
                }
                

                #endregion

                #region Member Associations Edit
                if (entity.Associations.Count > 0)
                {
                    foreach (var l in entity.Associations)
                    {
                        asRemove.Add((long)l.AssociationId);
                    }

                    foreach (var b in dto.AssociationIds)
                    {
                        foreach (var c in entity.Associations)
                        {
                            if (b == c.AssociationId)
                            {
                                asRemove.Remove((long)c.AssociationId);
                                continue;

                            }

                        }
                    }
                    foreach (var b in asRemove)
                    {
                        memberAssociation = GenService.GetAll<MemberAssociations>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.AssociationId == b).FirstOrDefault();
                        memberAssociation.Status = EntityStatus.Inactive;
                        memberAssociation.EditDate = DateTime.Now;
                        memberAssociation.EditedBy = userId;
                        GenService.Save(memberAssociation);
                    }
                }
                if (dto.AssociationIds != null)
                {
                    foreach (var b in dto.AssociationIds)
                    {
                        memberAssociation = GenService.GetAll<MemberAssociations>()
                            .Where(m => m.MemberId == entity.Id &&
                            m.AssociationId == b).FirstOrDefault();
                        if (memberAssociation == null)
                        {
                            memberAssociation = new MemberAssociations();
                            memberAssociation.Status = EntityStatus.Active;
                            memberAssociation.CreateDate = DateTime.Now;
                            memberAssociation.CreatedBy = userId;
                            memberAssociation.AssociationId = b;
                            memberAssociation.MemberId = dto.Id;
                            GenService.Save(memberAssociation);
                        }
                        else
                        {
                            if (memberAssociation.Status == EntityStatus.Inactive)
                            {
                                memberAssociation.Status = EntityStatus.Active;
                                memberAssociation.EditDate = DateTime.Now;
                                memberAssociation.EditedBy = userId;
                                memberAssociation.AssociationId = b;
                                memberAssociation.MemberId = dto.Id;
                                GenService.Save(memberAssociation);
                            }

                        }

                    }
                }
      
                #endregion

                var memberNo = "Member_" + entity.Id;

                if (dto.TradeLicenseFile != null)
                {
                    path = "~/UploadedFiles/" + memberNo + "/";
                    string input = path.Replace("~/", "");
                    path = HttpContext.Current.Server.MapPath(path);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var fileName = "TradeLicense_" + dto.TradeLicenseFile.FileName;
                    path = Path.Combine(path, fileName);
                    dto.TradeLicenseFile.SaveAs(path);
                    var pathFile = input + fileName;
                    entity.TradeLicenseFile = pathFile;
                }
                if (dto.RenewalFile != null)
                {
                    path = "~/UploadedFiles/" + memberNo + "/";
                    string input = path.Replace("~/", "");
                    path = HttpContext.Current.Server.MapPath(path);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var fileName = "RenewalFile_" + dto.RenewalFile.FileName;
                    path = Path.Combine(path, fileName);
                    dto.RenewalFile.SaveAs(path);
                    var pathFile = input + fileName;
                    entity.RenewalFile = pathFile;
                }
                if (dto.IncorporationFile != null)
                {
                    path = "~/UploadedFiles/" + memberNo + "/";
                    string input = path.Replace("~/", "");
                    path = HttpContext.Current.Server.MapPath(path);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var fileName = "IncorporationFile_" + dto.IncorporationFile.FileName;
                    path = Path.Combine(path, fileName);
                    dto.IncorporationFile.SaveAs(path);
                    var pathFile = input + fileName;
                    entity.IncorporationFile = pathFile;
                }
                if (dto.RepresentativePhotoFile != null)
                {
                    path = "~/UploadedFiles/" + memberNo + "/";
                    string input = path.Replace("~/", "");
                    path = HttpContext.Current.Server.MapPath(path);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var fileName = "RepresentativePhoto_" + dto.RepresentativePhotoFile.FileName;
                    path = Path.Combine(path, fileName);
                    dto.RepresentativePhotoFile.SaveAs(path);
                    var pathFile = input + fileName;
                    entity.RepresentativePhotoFile = pathFile;
                }

                MemberId = entity.Id;

            }
            #endregion

            #region Add
            else
            {
                try
                {
                    entity = Mapper.Map<Member>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.DateOfReceived = DateTime.Now;
                    if (dto.FromYear < DateTime.Today.Year) 
                    {
                        DateTime theDate = new DateTime(dto.FromYear, 1, 1);
                        entity.IssueDate = theDate;
                    }



                    entity.MembershipStatus = MembershipStatus.Pending;
                    entity.Status = EntityStatus.Active;

                    List<MemberBusinessCatagories> memberBusinessCatagoryList = new List<MemberBusinessCatagories>();
                    List<MemberChambers> memberChamberList = new List<MemberChambers>();
                    List<MemberAssociations> memberAssociationList = new List<MemberAssociations>();

                    MemberBusinessCatagories memberBusinessCatagory;
                    MemberChambers memberChamber;
                    MemberAssociations memberAssociation;

                    //entity.MemberNo = _seqFacade.GetUpdatedMemberNo();

                    GenService.Save(entity);
                    if (dto.BusinessCategoryIds != null)
                    {
                        foreach (var b in dto.BusinessCategoryIds)
                        {
                            memberBusinessCatagory = new MemberBusinessCatagories();
                            memberBusinessCatagory.BusinessCategoryId = b;
                            memberBusinessCatagory.MemberId = entity.Id;
                            memberBusinessCatagoryList.Add(memberBusinessCatagory);
                        }
                    }

                    if (dto.AssociationIds != null)
                    {
                        foreach (var b in dto.AssociationIds)
                        {
                            memberAssociation = new MemberAssociations();
                            memberAssociation.AssociationId = b;
                            memberAssociation.MemberId = entity.Id;
                            memberAssociationList.Add(memberAssociation);
                        }
                    }

                    if (dto.ChamberIds != null)
                    {
                        foreach (var b in dto.ChamberIds)
                        {
                            memberChamber = new MemberChambers();
                            memberChamber.ChamberId = b;
                            memberChamber.MemberId = entity.Id;
                            memberChamberList.Add(memberChamber);
                        }
                    }
                       

                    var memberNo = "Member_" + entity.Id;

                    if (dto.TradeLicenseFile != null)
                    {
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "TradeLicenseFile_" + dto.TradeLicenseFile.FileName;
                        path = Path.Combine(path, fileName);
                        dto.TradeLicenseFile.SaveAs(path);
                        var pathFile = input + fileName;
                        entity.TradeLicenseFile = pathFile;
                    }
                    if (dto.RenewalFile != null)
                    {
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "RenewalFile_" + dto.RenewalFile.FileName;
                        path = Path.Combine(path, fileName);
                        dto.RenewalFile.SaveAs(path);
                        var pathFile = input + fileName;
                        entity.RenewalFile = pathFile;
                    }
                    if (dto.IncorporationFile != null)
                    {
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "IncorporationFile_" + dto.IncorporationFile.FileName;
                        path = Path.Combine(path, fileName);
                        dto.IncorporationFile.SaveAs(path);
                        var pathFile = input + fileName;
                        entity.IncorporationFile = pathFile;
                    }
                    else
                    {
                        dto.IncorporationDate = null;
                    }

                    if (dto.RepresentativePhotoFile != null)
                    {
                        //path = "~/UploadedFiles/" + memberNo + "/";
                        //path = HttpContext.Current.Server.MapPath(path);
                        //var p = HttpContext.Current.Server.MapPath("");

                        //if (!Directory.Exists(path))
                        //{
                        //    Directory.CreateDirectory(path);
                        //}
                        //var fileName = "RepresentativePhoto_" + dto.RepresentativePhotoFile.FileName;
                        //path = Path.Combine(path, fileName);
                        //dto.RepresentativePhotoFile.SaveAs(path);
                        //entity.RepresentativePhotoFile = path;
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "RepresentativePhoto_" + dto.RepresentativePhotoFile.FileName;
                        path = Path.Combine(path, fileName);
                        dto.RepresentativePhotoFile.SaveAs(path);
                        var pathFile = input + fileName;
                        var file = path.Replace("~/", "");
                        entity.RepresentativePhotoFile = pathFile;
                    }
                    GenService.Save(memberChamberList);
                    GenService.Save(memberBusinessCatagoryList);
                    GenService.Save(memberAssociationList);
                    //response.Message = "Membership basic information saved successfully";
                    MemberId = entity.Id;
                }
                catch (Exception ex)
                {
                    response.Message = "Membership Application not saved.";
                    return 0;
                }

            }
            #endregion

            GenService.SaveChanges();

            return MemberId;
        }

        public ResponseDto SaveOwner(OwnerDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Owner();
            string path = "";
            #region edit
            if (dto.Id > 0)
            {
                string memberNo = "";
                entity = GenService.GetById<Owner>((long)dto.Id);
                dto.PhotoPath = entity.Photo;
                Mapper.Map(dto, entity);
                entity.Photo = dto.PhotoPath;
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                entity.Status = EntityStatus.Active;
                entity.Id = (long)dto.Id;
                GenService.Save(entity);
                if (entity != null)
                {
                    memberNo = "Member_" + entity.MemberId;
                }

                if (dto.Photo != null)
                {
                    path = "~/UploadedFiles/" + memberNo + "/";
                    string input = path.Replace("~/", "");
                    path = HttpContext.Current.Server.MapPath(path);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var fileName = "OwnerPhoto_" + dto.Photo.FileName;
                    path = Path.Combine(path, fileName);
                    dto.Photo.SaveAs(path);
                    var pathFile = input + fileName;
                    entity.Photo = pathFile;
                }
                if (dto.IsContactPerson == true)
                {
                    response.Message = "Contact Person Information edited successfully";
                }
                else
                {
                    response.Message = "Owner Information edited successfully";
                }
            }
            #endregion

            #region Add
            else
            {
                try
                {
                    string memberNo = "";
                    entity = Mapper.Map<Owner>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    if (entity != null)
                    {
                        memberNo = "Member_" + entity.MemberId;
                    }

                    if (dto.Photo != null)
                    {
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "OwnerPhoto_" + dto.Photo.FileName;
                        path = Path.Combine(path, fileName);
                        dto.Photo.SaveAs(path);
                        var pathFile = input + fileName;
                        entity.Photo = pathFile;
                    }
                    if (dto.IsContactPerson == true)
                    {
                        response.Message = "Contact Person Information saved successfully";
                    }
                    else
                    {
                        response.Message = "Owner Information saved successfully";
                    }

                }
                catch (Exception ex)
                {
                    response.Message = "Owner Information not saved.";
                }

            }
            #endregion

            GenService.SaveChanges();

            return response;
        }
        public ResponseDto SaveAddress(MemberDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var presentAddress = new Address();
            var parmanentAddress = new Address();
            var member = new Member();
            member = GenService.GetById<Member>((long)dto.Id);

            #region Add

            try
            {
                if (member.PresentAddressId > 0)
                {
                    presentAddress = GenService.GetById<Address>((long)member.PresentAddressId);
                    presentAddress = Mapper.Map(dto.PresentAddress, presentAddress);
                    presentAddress.EditDate = DateTime.Now;
                    presentAddress.EditedBy = userId;
                    presentAddress.Status = EntityStatus.Active;
                    presentAddress.Id = (long)member.PresentAddressId;

                }
                else
                {
                    presentAddress = Mapper.Map<Address>(dto.PresentAddress);
                    presentAddress.CreateDate = DateTime.Now;
                    presentAddress.CreatedBy = userId;
                    presentAddress.Status = EntityStatus.Active;

                }
                if (member.ParmanentAddressId > 0)
                {
                    parmanentAddress = GenService.GetById<Address>((long)member.ParmanentAddressId);
                    parmanentAddress = Mapper.Map(dto.ParmanentAddress, parmanentAddress);
                    parmanentAddress.EditDate = DateTime.Now;
                    parmanentAddress.EditedBy = userId;
                    parmanentAddress.Status = EntityStatus.Active;
                    parmanentAddress.Id = (long)member.ParmanentAddressId;
                }
                else
                {
                    parmanentAddress = Mapper.Map<Address>(dto.ParmanentAddress);
                    parmanentAddress.CreateDate = DateTime.Now;
                    parmanentAddress.CreatedBy = userId;
                    parmanentAddress.Status = EntityStatus.Active;
                }
                GenService.Save(presentAddress);
                GenService.Save(parmanentAddress);

                member.Fax = dto.PresentAddress.Fax;
                member.Website = dto.PresentAddress.Website;
                member.MobileNo = dto.PresentAddress.CellPhoneNo;
                member.Phone = dto.PresentAddress.PhoneNo;
                member.PresentAddressId = presentAddress.Id;
                member.ParmanentAddressId = parmanentAddress.Id;
                member.Status = EntityStatus.Active;
                GenService.Save(member);

                response.Message = "Member Contact Information Saved Successfully";

            }
            catch (Exception ex)
            {
                response.Message = "Member Contact Information not saved.";
            }
            GenService.SaveChanges();

            return response;
            #endregion
        }

        public IPagedList<MemberDto> PendingMemberApplicationList( int pageSize, int pageCount, string searchString, long? businessCategory, Zone? zone)
        {
            //DateTime? fromDate, DateTime? toDate,
            //fromDate = fromDate.AddDays(-1);
            //toDate = toDate.AddDays(1);
            var members = GenService.GetAll<Member>().Where(m => m.Status == EntityStatus.Active && m.MembershipStatus == MembershipStatus.Pending);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                members = members.Where(m => m.NameOfOrganization.ToLower().Contains(searchString));
            }
            if (businessCategory > 0)
                members = members.Where(m => m.BusinessCategories.Any(b => b.BusinessCategoryId == businessCategory && b.Status == EntityStatus.Active));
            if (zone != null)
                members = members.Where(m => m.Zone == zone);
            //if (fromDate != null && fromDate > DateTime.MinValue)
            //    members = members.Where(m => m.DateOfReceived >= fromDate);
            //if (toDate != null && toDate < DateTime.MaxValue)
            //    members = members.Where(m => m.DateOfReceived <= toDate);
            var membersList = from member in members
                              select new MemberDto
                              {
                                  Id = member.Id,
                                  NameOfOrganization = member.NameOfOrganization,
                                  OwnershipStatusName = member.OwnershipStatus.ToString(),
                                  ZoneName = member.Zone.ToString(),
                                  MembershipStatusName = member.MembershipStatus.ToString(),
                                  DateOfReceived = member.DateOfReceived
                              };
            var temp = membersList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }

        public List<OwnerDto> GetOwnerList(long memberId)
        {
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            var ownerList = GenService.GetAll<Owner>().AsEnumerable()
                .Where(o => o.MemberId == memberId && o.Status == EntityStatus.Active
                && o.IsContactPerson == false).Select(r => new OwnerDto()
                {
                    Id = r.Id,
                    Name = r.Name,
                    Designation = r.Designation,
                    NID = r.NID,
                    Phone = r.Phone,
                    MobileNo = r.MobileNo,
                    PresentAddress = r.PresentAddress,
                    PhotoTest = r.Photo != null ? Path.GetFileName(r.Photo) : "",
                    PhotoPath = !string.IsNullOrWhiteSpace(r.Photo) ? "http://" + url + "/" + r.Photo.Replace("\\", "/") : ""
                }).ToList();

            return Mapper.Map<List<OwnerDto>>(ownerList);
        }

        public ResponseDto SaveAttachment(AttachmentDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Attachment();
            string path = "";
            #region edit
            if (dto.Id > 0)
            {

            }
            #endregion

            #region Add
            else
            {
                try
                {
                    string memberNo = "";
                    var FileName = GenService.GetAll<DocumentType>().Where(s => s.Id == dto.DocumentTypeId).FirstOrDefault().Name;
                    entity = Mapper.Map<Attachment>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    if (entity != null)
                    {
                        memberNo = "Member_" + entity.MemberId;
                    }

                    if (dto.PathName != null)
                    {
                        path = "~/UploadedFiles/" + memberNo + "/";
                        string input = path.Replace("~/", "");
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = FileName + "_" + dto.PathName.FileName;
                        path = Path.Combine(path, fileName);
                        dto.PathName.SaveAs(path);
                        var pathFile = input + fileName;
                        entity.Path = pathFile;
                    }
                    response.Message = "File Upload Successfully";

                }
                catch (Exception ex)
                {
                    response.Message = "File Upload not done";
                }

            }
            #endregion

            GenService.SaveChanges();

            return response;
        }

        public List<AttachmentDto> GetDocumentList(long memberId)
        {
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            var list = GenService.GetAll<Attachment>().AsEnumerable()
                .Where(o => o.MemberId == memberId && o.Status == EntityStatus.Active).Select(r => new AttachmentDto()
                {
                    Id = r.Id,
                    DocumentTypeName = r.DocumentType.Name,
                    PathTest = r.Path != null ? Path.GetFileName(r.Path) : "",
                    FilePath = !string.IsNullOrWhiteSpace(r.Path) ? "http://" + url + "/" + r.Path.Replace("\\", "/") : ""
                }).ToList();

            return list;
        }

        public MemberApplicationDto GetMemberApplication(long id)
        {
            var memberDto = new MemberApplicationDto();
            var addressDto = new AddressDto();
            memberDto.PresentAddress = new AddressDto();
            memberDto.ParmanentAddress = new AddressDto();
            var presentAddress = new Address();
            var permanentAddress = new Address();
            var contact = new OwnerDto();
            var ownerDtoList = new List<OwnerDto>();
            var contactPersonList = new List<Owner>();
            var ownerList = new List<Owner>();
            var members = new List<Member>();
            var member = new Member();
            string url = System.Web.HttpContext.Current.Request.Url.Authority;

            members = GenService.GetAll<Member>()
                   .Where(s => s.Id == id && s.Status == EntityStatus.Active).ToList();

            member = GenService.GetAll<Member>()
                 .Where(s => s.Id == id && s.Status == EntityStatus.Active).FirstOrDefault();

            ownerList = GenService.GetAll<Owner>()
                .Where(o => o.MemberId == id && o.IsContactPerson == false && o.Status == EntityStatus.Active).ToList();

            contactPersonList = GenService.GetAll<Owner>()
                .Where(c => c.MemberId == id && c.IsContactPerson == true && c.Status == EntityStatus.Active).ToList();
            if (member != null)
            {
                presentAddress = GenService.GetAll<Address>()
                    .Where(a => a.Id == member.PresentAddressId).FirstOrDefault();

                permanentAddress = GenService.GetAll<Address>()
                    .Where(a => a.Id == member.ParmanentAddressId && a.Status == EntityStatus.Active).FirstOrDefault();
            }

            memberDto.PresentAddress = Mapper.Map<AddressDto>(presentAddress);
            memberDto.ParmanentAddress = Mapper.Map<AddressDto>(permanentAddress);

            ownerDtoList = (from o in ownerList
                            select new OwnerDto
                            {
                                Id = o.Id,
                                Name = o.Name,
                                Designation = o.Designation,
                                NID = o.NID,
                                Phone = o.Phone,
                                MobileNo = o.MobileNo,
                                PresentAddress = o.PresentAddress,
                                PhotoTest = o.Photo != null ? Path.GetFileName(o.Photo) : "",
                                PhotoPath = !string.IsNullOrWhiteSpace(o.Photo) ? "http://" + url + "/" + o.Photo.Replace("\\", "/") : "",

                            }).ToList();

            contact = (from o in contactPersonList
                       select new OwnerDto
                       {
                           Id = o.Id,
                           Name = o.Name,
                           Designation = o.Designation,
                           NID = o.NID,
                           Email=o.Email,
                           Phone = o.Phone,
                           MobileNo = o.MobileNo,
                           PresentAddress = o.PresentAddress,
                           PermanentAddress = o.PermanentAddress

                       }).FirstOrDefault();

            memberDto = (from v in members
                         select new MemberApplicationDto
                         {
                             Id = v.Id,
                             MemberNo = v.MemberNo,
                             NameOfOrganization = v.NameOfOrganization,
                             TradeLicenseNo = v.TradeLicenseNo,
                             LicenseDate = v.LicenseDate,
                             EstablishmentDate = v.EstablishmentDate,
                             RenewalNo = v.RenewalNo,
                             RenewalDate = v.RenewalDate,
                             RenewalIssueDate = v.RenewalIssueDate,
                             IncorporationDate = v.IncorporationDate,
                             RepresentativeName = v.RepresentativeName,
                             RepresentativeNID = v.RepresentativeNID,
                             MRNo = v.MRNo,
                             PONo = v.PONo,
                             Bank = v.Bank,
                             Branch = v.Branch,
                             DateOfReceived = v.DateOfReceived,
                             IssueDate = v.IssueDate,
                             FromYear= (v.IssueDate!=null ? DateTime.Parse(v.IssueDate.ToString()).Year : DateTime.Today.Year),
                             BusinessCategories = Mapper.Map<List<MemberBusinessCatagoriesDto>>(v.BusinessCategories.Where(b => b.Status == EntityStatus.Active).ToList()),
                             Chambers = Mapper.Map<List<MemberChambersDto>>(v.Chambers.Where(b => b.Status == EntityStatus.Active).ToList()),
                             Associations = Mapper.Map<List<MemberAssociationsDto>>(v.Associations.Where(b => b.Status == EntityStatus.Active).ToList()),
                             Zone = v.Zone,
                             OwnershipStatus = v.OwnershipStatus,
                             MembershipStatus = v.MembershipStatus,
                             IsDisable = (v.MembershipStatus == MembershipStatus.Approved ? false : true),
                             TradeLicenseFileTest = v.TradeLicenseFile != null ? Path.GetFileName(v.TradeLicenseFile) : "",
                             TradeLicenseFilePath = !string.IsNullOrWhiteSpace(v.TradeLicenseFile) ? "http://" + url + "/" + v.TradeLicenseFile.Replace("\\", "/") : "",
                             RenewalFileTest = v.RenewalFile != null ? Path.GetFileName(v.RenewalFile) : "",
                             RenewalFilePath = !string.IsNullOrWhiteSpace(v.RenewalFile) ? "http://" + url + "/" + v.RenewalFile.Replace("\\", "/") : "",
                             IncorporationFileTest = v.IncorporationFile != null ? Path.GetFileName(v.IncorporationFile) : "",
                             IncorporationFilePath = !string.IsNullOrWhiteSpace(v.IncorporationFile) ? "http://" + url + "/" + v.IncorporationFile.Replace("\\", "/") : "",
                             RepresentativePhotoFileTest = v.RepresentativePhotoFile != null ? Path.GetFileName(v.RepresentativePhotoFile) : "",
                             //RepresentativePhotoFilePath = v.RepresentativePhotoFile != null ? v.RepresentativePhotoFile : "",
                             RepresentativePhotoFilePath = !string.IsNullOrWhiteSpace(v.RepresentativePhotoFile) ? "http://" + url + "/" + v.RepresentativePhotoFile.Replace("\\", "/") : "",

                             OwnerList = ownerDtoList,
                             PreDivisionId = memberDto.PresentAddress != null ? memberDto.PresentAddress.DivisionId : null,
                             PreDistrictId = memberDto.PresentAddress != null ? memberDto.PresentAddress.DistrictId : null,
                             PreThanaId = memberDto.PresentAddress != null ? memberDto.PresentAddress.ThanaId : null,
                             PreAreaId = memberDto.PresentAddress != null ? memberDto.PresentAddress.AreaId : null,
                             PreAddressLine1 = memberDto.PresentAddress != null ? memberDto.PresentAddress.AddressLine1 : "",
                             PreAddressLine2 = memberDto.PresentAddress != null ? memberDto.PresentAddress.AddressLine2 : "",
                             PreCellPhoneNo = memberDto.PresentAddress != null ? memberDto.PresentAddress.CellPhoneNo : "",
                             PreEmail = memberDto.PresentAddress != null ? memberDto.PresentAddress.Email : "",
                             Fax = member.Fax != null ? member.Fax : "",
                             Website = member.Website != null ? member.Website : "",
                             PrePostalCode = memberDto.PresentAddress != null ? memberDto.PresentAddress.PostalCode : "",
                             PrePhoneNo = memberDto.PresentAddress != null ? memberDto.PresentAddress.PhoneNo : "",

                             ParDivisionId = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.DivisionId : null,
                             ParDistrictId = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.DistrictId : null,
                             ParThanaId = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.ThanaId : null,
                             ParAreaId = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.AreaId : null,
                             ParAddressLine1 = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.AddressLine1 : "",
                             ParAddressLine2 = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.AddressLine2 : "",
                             ParCellPhoneNo = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.CellPhoneNo : "",
                             ParEmail = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.Email : "",
                             ParPostalCode = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.PostalCode : "",
                             ParPhoneNo = memberDto.ParmanentAddress != null ? memberDto.ParmanentAddress.PhoneNo : "",
                             ContactPerson = Mapper.Map<OwnerDto>(contact)

                         }).FirstOrDefault();
            //if (member != null)
            //    return Mapper.Map<MemberDto>(member);
            return memberDto;
        }
        public ResponseDto RemoveOwner(long Id, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Owner();
            entity = GenService.GetById<Owner>(Id);
            entity.Status = EntityStatus.Inactive;
            entity.EditDate = DateTime.Now;
            entity.EditedBy = userId;
            GenService.Save(entity);
            response.Message = "Owner Removed Successfully";
            return response;
        }
        public ResponseDto RemoveAttachment(long Id, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Attachment();
            entity = GenService.GetById<Attachment>(Id);
            entity.Status = EntityStatus.Inactive;
            entity.EditDate = DateTime.Now;
            entity.EditedBy = userId;
            GenService.Save(entity);
            response.Message = "Attachment Removed Successfully";
            return response;
        }


        public IPagedList<MemberDto> ApprovedMemberList(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, string searchString, long? businessCategory, Zone? zone)
        {
            //fromDate = fromDate.AddDays(-1);
            //toDate = toDate.AddDays(1);
            var members = GenService.GetAll<Member>().Where(m => m.Status == EntityStatus.Active && m.MembershipStatus == MembershipStatus.Approved);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                members = members.Where(m => m.NameOfOrganization.ToLower().Contains(searchString));
            }
            if (businessCategory > 0)
                members = members.Where(m => m.BusinessCategories.Any(b => b.BusinessCategoryId == businessCategory && b.Status == EntityStatus.Active));
            if (zone != null)
                members = members.Where(m => m.Zone == zone);
            if (fromDate != null && fromDate > DateTime.MinValue)
                members = members.Where(m => m.IssueDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                members = members.Where(m => m.IssueDate <= toDate);
            var membersList = from member in members
                              select new MemberDto
                              {
                                  Id = member.Id,
                                  NameOfOrganization = member.NameOfOrganization,
                                  OwnershipStatusName = member.OwnershipStatus.ToString(),
                                  ZoneName = member.Zone.ToString(),
                                  MembershipStatusName = member.MembershipStatus.ToString(),
                                  IssueDate = (member.IssueDate!=null? member.IssueDate:DateTime.Today)
                              };
            var temp = membersList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            return temp;
        }
        public ResponseDto SetFees(MemberPendingFeeDto dto, long userId, long selectedCompanyId)
        {
            ResponseDto response = new ResponseDto();
            var voucherdate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
            var entity = new MemberPendingFee();
            var pendingFeeDtoList = new List<MemberPendingFeeDto>();
            var pendingFeeList = new List<MemberPendingFee>();
            var voucherList = new List<VoucherDto>();
            var memberbasic = GenService.GetById<Member>((long)dto.MemberId);
            decimal totalmembershipfees = 0;
            decimal totaladvamnt = 0;
            #region Add

            try
            {
                foreach (var i in dto.FeeTypeList)
                {
                    entity = new MemberPendingFee();
                    entity.Amount = i.Amount;
                    entity.MemberId = dto.MemberId;
                    entity.FeeTypeId = i.Id;
                    entity.CollectionStatus = false;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;

                    var feeinfo = GenService.GetById<FeeType>(i.Id);
                    var currentyear = _companyProfileFacade.DateToday(selectedCompanyId).Date.Year;
                    //var afterexpiryyear = (memberbasic.ExpiryDate != null ? DateTime.Parse(memberbasic.ExpiryDate.ToString()).AddDays(+1).Year:);
                    if ((memberbasic.IssueDate!=null? DateTime.Parse(memberbasic.IssueDate.ToString()).Year :DateTime.Today.Year) < DateTime.Today.Year)
                        entity.EffectiveDate = memberbasic.IssueDate;
                    else
                        entity.EffectiveDate = voucherdate;
                    if (i.FeeCheck == true && !i.Exists)
                    {
                        pendingFeeList.Add(entity);
                        if (feeinfo.AccountReceivable)
                        {
                            if (i.Id == BizConstants.AnnualSubscriptionFee)
                            {
                                #region Fee Collection voucher entry
                                var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, i.Id); //aDetail.CategoryId
                                                                                                                           //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                                if (string.IsNullOrEmpty(accHeadCode))
                                {
                                    response.Message = "Account head not found";
                                    return response;
                                }
                               
                                var annualfee = GenService.GetById<FeeType>(BizConstants.AnnualSubscriptionFee);
                                totalmembershipfees = (annualfee.Amount / 3);
                                //totaladvamnt = totalmembershipfees * (3 - (DateTime.Today.Year - DateTime.Parse(memberbasic.IssueDate.ToString()).Year));
 
                                var voucher = new VoucherDto();
                                voucher.AccountHeadCode = accHeadCode;
                                voucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                voucher.Description = "Receivable for " + feeinfo.Name + " Fee";
                                voucher.Credit = 0;
                                voucher.Debit = totalmembershipfees;
                                voucher.CompanyProfileId = selectedCompanyId;
                                voucher.VoucherDate = voucherdate;
                                voucherList.Add(voucher);
                                #endregion

                                #region contra voucher entry 


                                var CreditVoucher = new VoucherDto();
                                CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                CreditVoucher.Description = "For the year of " + currentyear + " from " + memberbasic.NameOfOrganization + " .";
                                CreditVoucher.Credit = totalmembershipfees;
                                CreditVoucher.Debit = 0;
                                CreditVoucher.CompanyProfileId = selectedCompanyId;
                                CreditVoucher.VoucherDate = voucherdate;
                                var CreditAccHead = (i.Id == BizConstants.AnnualSubscriptionFee ? (_accounts.GetAccHeadCodeForVoucherZone(AccountHeadRefType.Income, i.Id, memberbasic.Zone)) : (_accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, i.Id)));
                                if (string.IsNullOrEmpty(CreditAccHead))
                                {
                                    response.Message = "Account head not found";
                                    return response;
                                }
                                CreditVoucher.AccountHeadCode = CreditAccHead;
                                voucherList.Add(CreditVoucher);

                                
                                #endregion
                            }
                            else
                            {
                                #region Fee Collection voucher entry
                                var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, i.Id); //aDetail.CategoryId
                                                                                                                           //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                                if (string.IsNullOrEmpty(accHeadCode))
                                {
                                    response.Message = "Account head not found";
                                    return response;
                                }
                                var voucher = new VoucherDto();
                                voucher.AccountHeadCode = accHeadCode;
                                voucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                voucher.Description = "Receivable for " + feeinfo.Name + " Fee";
                                voucher.Credit = 0;
                                voucher.Debit = (decimal)i.Amount;
                                voucher.CompanyProfileId = selectedCompanyId;
                                voucher.VoucherDate = voucherdate;
                                voucherList.Add(voucher);
                                #endregion

                                #region contra voucher entry 

                                var CreditVoucher = new VoucherDto();
                                CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                CreditVoucher.Description = "For the year of " + currentyear + " from " + memberbasic.NameOfOrganization + " .";
                                CreditVoucher.Credit = (decimal)i.Amount;
                                CreditVoucher.Debit = 0;
                                CreditVoucher.CompanyProfileId = selectedCompanyId;
                                CreditVoucher.VoucherDate = voucherdate;
                                var CreditAccHead = (i.Id == BizConstants.AnnualSubscriptionFee ? (_accounts.GetAccHeadCodeForVoucherZone(AccountHeadRefType.Income, i.Id, memberbasic.Zone)) : (_accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, i.Id)));
                                if (string.IsNullOrEmpty(CreditAccHead))
                                {
                                    response.Message = "Account head not found";
                                    return response;
                                }
                                CreditVoucher.AccountHeadCode = CreditAccHead;
                                voucherList.Add(CreditVoucher);


                                #endregion
                            }

                        }
                    }
                }
                

                if (voucherList.Count > 0)
                {

                    var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                    var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                    voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                    var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, selectedCompanyId, userId);
                    //responce.Id = entity.Id;
                    if (!(bool)accountsResponse.Success)
                    {
                        response.Message = "Fee Collection Saving Failed. " + accountsResponse.Message;
                        //tran.Dispose();
                        return response;
                    }
                    else
                    {
                        GenService.Save(pendingFeeList);
                    }
                    
                }
                response.Message = "Member Pending Fee Saved Successfully";
            }
            catch (Exception ex)
            {
                response.Message = "Member Pending Fee not saved.";
            }

            #endregion

            GenService.SaveChanges();

            return response;
        }
        public List<FeeTypeDto> GetFeeTypeList(long? memid)
        {
            var memberfeeList = new List<MemberPendingFee>();
            if (memid != null || memid > 0)
            {
                memberfeeList = GenService.GetAll<MemberPendingFee>()
               .Where(s => s.Status == EntityStatus.Active && s.MemberId == memid).ToList();
            }
            
            var feeList = GenService.GetAll<FeeType>().Where(x=> x.CompanyProfileId==1) //1 for ATAB Office
                .Where(s => s.Status == EntityStatus.Active).ToList();

            var querydb = (from w in feeList
                           select new FeeTypeDto
                           {
                               Id = w.Id,
                               Name = w.Name,
                               Amount = w.Amount,
                               FeeCheck = memberfeeList.Any(c => c.FeeTypeId == w.Id),
                               Exists = memberfeeList.Any(c => c.FeeTypeId == w.Id)
                           }
                         ).ToList();
            return querydb;
        }

        public ResponseDto MemberApprove(MemberDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Member();
            entity = GenService.GetById<Member>(dto.Id);

            #region Add

            try
            {
                entity.Bank = dto.Bank;
                entity.Branch = dto.Branch;
                entity.MRNo = dto.MRNo;
                entity.PONo = dto.PONo;
                entity.DateOfReceived = dto.DateOfReceived;
                entity.IssueDate = dto.IssueDate;
                entity.MembershipStatus = MembershipStatus.Approved;
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                entity.Status = EntityStatus.Active;
                if (DateTime.Parse(entity.IssueDate.ToString()).Year > 2017) //Base membership Year is 2015
                {
                    if (entity.MemberPendingFees.Count() > 0)
                    {
                        foreach (var v in entity.MemberPendingFees)
                        {
                            if (v.CollectionStatus == false)
                            {
                                response.Success = false;
                                response.Message = "Fees are Pending for this Member ?";
                                return response;
                            }
                        }
                    }
                    else
                    {
                        response.Message = "Set Member's Pending Fees";
                        return response;
                    }
                }
                else
                {
                    entity.ExpiryDate = DateTime.Parse(entity.IssueDate.ToString()).AddYears(+3).AddDays(-1);
                }
                
                entity.MemberNo = _seqFacade.GetUpdatedMemberNo();
                GenService.Save(entity);
                response.Success = true;
                response.Message = "Member Approved Successfully";
            }
            catch (Exception ex)
            {
                response.Message = "Member not approved.";
            }


            #endregion

            GenService.SaveChanges();

            return response;
        }

        public ResponseDto SetApprove(MemberDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Member();
            entity = GenService.GetById<Member>(dto.Id);

            #region Add

            try
            {
                entity.Bank = dto.Bank;
                entity.Branch = dto.Branch;
                entity.MRNo = dto.MRNo;
                entity.PONo = dto.PONo;
                entity.DateOfReceived = dto.DateOfReceived;
                entity.IssueDate = dto.IssueDate;
                entity.MembershipStatus = MembershipStatus.Approved;
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.Status = EntityStatus.Active;
                GenService.Save(entity);
                response.Message = "Member Approved Successfully";
            }
            catch (Exception ex)
            {
                response.Message = "Member not Approved.";
            }


            #endregion

            GenService.SaveChanges();

            return response;
        }
        public MemberApplicationDto GetPaymentHistory(long? memberId)
        {
            var payment = new MemberApplicationDto();
            var paymentHistory = GenService.GetAll<FeeCollectionTran>().Where(s => s.MemberId == memberId).ToList();
            if (paymentHistory.Count > 0)
            {
                payment.MemberNo = paymentHistory.FirstOrDefault().Member.MemberNo;
                payment.MemberName = paymentHistory.FirstOrDefault().Member.RepresentativeName;
            }
            payment.PaymentList = new List<FeeCollectionTranDto>();

            payment.PaymentList = (from m in paymentHistory
                                   select new FeeCollectionTranDto
                                   {
                                       FeeTypeName = m.FeeType != null ? m.FeeType.Name : "",
                                       SubscriptionDate = m.SubscriptionDate,
                                       PayTypeName = m.PayType.ToString(),
                                       CollectedAmount = m.CollectedAmount
                                   }).ToList();

            return payment;
        }


        #region Certificte Issue
        public List<CertificateRegisterDetailDto> GetCertificteListForAutoFill(string prefix, List<long> exclusionList, long bookid)
        {
            List<CertificateRegisterDetail> lists = new List<CertificateRegisterDetail>();
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();

            lists = GenService.GetAll<CertificateRegisterDetail>().Where(x => x.Status == EntityStatus.Active && (x.CertificateNo.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id) && x.CertificateStatus == CertificateStatus.UnUsed && x.CertificateRegisterId == bookid).ToList();
            
            var data = lists.OrderBy(x => x.CertificateNo)
                     .Select(x => new CertificateRegisterDetailDto { Id = x.Id, CertificateNo = x.CertificateNo })
                     .ToList();
            return data;
        }

       
        public List<CertificateRegisterDto> GetBooks(CertificateType? ctypes)
        {
            var books = GenService.GetAll<CertificateRegister>().Where(s => s.CertificateType == ctypes).ToList();
            return Mapper.Map<List<CertificateRegisterDto>>(books);
        }

        public ResponseDto SaveCertificate(CertificateRegisterDetailDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new CertificateRegisterDetail();
            entity = GenService.GetById<CertificateRegisterDetail>(dto.Id);
            #region Edit
            try
            {
                dto.CreateDate = entity.CreateDate;
                dto.CreatedBy = entity.CreatedBy;
                Mapper.Map(dto, entity);
                entity.CertificateStatus = CertificateStatus.Issued;
                entity.TransactionDate = DateTime.Now;
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                GenService.Save(entity);
                response.Message = "Certificate Saved Successfully";
            }
            catch (Exception ex)
            {
                response.Message = "Certificate not Saved.";
            }
            #endregion

            //GenService.SaveChanges();

            return response;
        }

        public List<CertificateRegisterDetailDto> GetMembersCirtificates(long memid)
        {
            var books = GenService.GetAll<CertificateRegisterDetail>().Where(s => s.MemberNo == memid).ToList();
            var result = from cer in books
                         select new CertificateRegisterDetailDto
                         {
                             CertificateNo = cer.CertificateNo,
                             CertificateRegisterId = cer.CertificateRegisterId,
                             CertificateStatus = cer.CertificateStatus,
                             CertificateStatusName = cer.CertificateStatus.ToString(),
                             Id = cer.Id,
                             IssueDate = cer.IssueDate,
                             ExpiryDate = cer.ExpiryDate,
                             CertificateType = cer.CertificateRegister.CertificateType,
                             CertificateTypeName = cer.CertificateRegister.CertificateType.ToString(),
                             MemberNo = cer.MemberNo,
                             TransactionDate = cer.TransactionDate
                         };
            return result.ToList();
        }

        public CertificateRegisterDetailDto GetCertificate(long cid)
        {
            var certificate = GenService.GetAll<CertificateRegisterDetail>().Where(x => x.Id == cid).FirstOrDefault();
            return Mapper.Map<CertificateRegisterDetailDto>(certificate);
        } 
        #endregion
    }
}