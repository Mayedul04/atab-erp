﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using Finix.Auth.Facade;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Util;
using PagedList;
using EnumDto = Finix.Membership.DTO.EnumDto;
using Finix.Accounts.DTO;
using Finix.Accounts.Facade;
using System.Data.Entity;

namespace Finix.Membership.Facade
{
    public class FeeCollectionFacade : BaseFacade
    {
        private readonly MembeshipAccountFacade _accounts = new MembeshipAccountFacade();
        private readonly AccountsFacade _accountsfacade = new AccountsFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        private readonly SequencerFacade _seqFacade = new SequencerFacade();


        public List<AreaDto> GetAllAreas()
        {
            var data = GenService.GetAll<Area>().ToList();
            return Mapper.Map<List<AreaDto>>(data);
        }

        public List<BusinessCategoryDto> GetAllCategories()
        {
            var data = GenService.GetAll<BusinessCategory>().ToList();
            return Mapper.Map<List<BusinessCategoryDto>>(data);
        }

        public List<PendingFeeMemberDto> GetMembersOfPendingFees(string memberName, string memberNo, long? catId, long? areaId, DateTime fromDate, DateTime toDate)
        {
            var memebers = GenService.GetAll<Member>().Where(r => r.IssueDate >= fromDate && r.IssueDate <= toDate);
            var result = new List<PendingFeeMemberDto>();
            if (memebers.Any())
            {
                var memberIds = memebers.Select(r => r.Id);
                var memberpendings = GenService.GetAll<MemberPendingFee>();
                if (memberpendings.Any())
                {
                    memberpendings = memberpendings.Where(r => memberIds.Contains((long)r.MemberId));
                    if (memberpendings.Any())
                    {
                        memberpendings = memberpendings.Where(r => r.EffectiveDate >= fromDate && r.EffectiveDate <= toDate && r.CollectionStatus == false);
                        result = (from mbr in memebers
                                  join cat in GenService.GetAll<MemberBusinessCatagories>() on mbr.Id equals cat.MemberId into extra
                                  from ext in extra.DefaultIfEmpty()
                                  join pnd in memberpendings on mbr.Id equals pnd.MemberId into tl
                                  from extr in tl.DefaultIfEmpty()
                                  select new PendingFeeMemberDto
                                  {
                                      Id = mbr.Id,
                                      MemberNo = mbr.MemberNo != null ? mbr.MemberNo : "",
                                      NameOfOrganization = mbr.NameOfOrganization != null ? mbr.NameOfOrganization : "",
                                      Phone = mbr.Phone != null ? mbr.Phone : "",
                                      MobileNo = mbr.MobileNo != null ? mbr.MobileNo : "",
                                      Fax = mbr.Fax != null ? mbr.Fax : "",
                                      Email = mbr.Email != null ? mbr.Email : "",
                                      Website = mbr.Email != null ? mbr.Email : "",
                                      EstablishmentDate = mbr.EstablishmentDate != null ? mbr.EstablishmentDate : DateTime.Now,
                                      IssueDate = mbr.IssueDate != null ? mbr.IssueDate : DateTime.Now,
                                      OwnershipStatus =
                                          mbr.OwnershipStatus != null ? mbr.OwnershipStatus : OwnershipStatus.None,
                                      MembershipStatus =
                                          mbr.MembershipStatus != null ? mbr.MembershipStatus : MembershipStatus.Approved,
                                      ZoneName = mbr.Zone.ToString(),
                                      OwnershipStatusName = mbr.OwnershipStatus.ToString(),
                                      BusinessCategoryId = ext != null ? ext.BusinessCategoryId : 0,
                                      AreaId = mbr != null ? mbr.PresentAddress != null ? mbr.PresentAddress.AreaId : 0 : 0,
                                      MemberPendingFeeId = extr != null ? extr.Id : 0,
                                  }).ToList();
                        result = result.Where(r => r.MemberPendingFeeId > 0).ToList();
                        if (memberName != null)
                        {
                            memberName = memberName.ToLower();
                            result = result.Where(r => r.NameOfOrganization.ToLower().Contains(memberName)).ToList();
                        }
                        if (memberNo != null)
                        {
                            memberNo = memberNo.ToLower();
                            result = result.Where(r => r.MemberNo.ToLower().Contains(memberNo)).ToList();
                        }
                        if (catId != null && catId > 0)
                        {
                            result = result.Where(r => r.BusinessCategoryId == catId).ToList();
                        }
                        if (areaId != null)
                        {
                            result = result.Where(r => r.AreaId == areaId).ToList();
                        }
                        var resultDt = (from dt in result
                                        group dt by new { dt.Id, dt.MemberNo } into grp
                                        let pendingFeeMemberDto = grp.FirstOrDefault()
                                        where pendingFeeMemberDto != null
                                        select new PendingFeeMemberDto
                                        {
                                            Id = grp.Key.Id,
                                            MemberNo = grp.Key.MemberNo,
                                            NameOfOrganization = pendingFeeMemberDto.NameOfOrganization,
                                            Phone = pendingFeeMemberDto.Phone,
                                            MobileNo = pendingFeeMemberDto.MobileNo,
                                            Fax = pendingFeeMemberDto.MobileNo,
                                            Email = pendingFeeMemberDto.MobileNo,
                                            Website = pendingFeeMemberDto.MobileNo,
                                            EstablishmentDate = pendingFeeMemberDto.EstablishmentDate,
                                            IssueDate = pendingFeeMemberDto.IssueDate,
                                            OwnershipStatus = pendingFeeMemberDto.OwnershipStatus,
                                            MembershipStatus = pendingFeeMemberDto.MembershipStatus,
                                            ZoneName = pendingFeeMemberDto.ZoneName,
                                            OwnershipStatusName = pendingFeeMemberDto.OwnershipStatusName,
                                            BusinessCategoryId = pendingFeeMemberDto.BusinessCategoryId,
                                            AreaId = pendingFeeMemberDto.AreaId,
                                            MemberPendingFeeId = pendingFeeMemberDto.MemberPendingFeeId
                                        }).ToList();
                        return resultDt;
                    }
                }
            }
            return result;
        }

        public IPagedList<PendingFeeMemberDto> NewMemberPendingFees(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, string searchString, long? btype, Zone? zone)
        {
            //var memebers = GenService.GetAll<Member>().Where(r => r.MembershipStatus==MembershipStatus.Approved && r.IssueDate >= fromDate && r.IssueDate <= toDate);
            //r.MembershipStatus != MembershipStatus.Approved
            //toDate = toDate.Value.AddDays(+1);
            var memebers = GenService.GetAll<Member>().Where(r => r.DateOfReceived >= fromDate && r.DateOfReceived <= toDate);
            var result = new List<PendingFeeMemberDto>();
            if (memebers.Any())
            {
                //var memberIds = memebers.Select(r => r.Id);
                var memberpendings = GenService.GetAll<MemberPendingFee>();
                if (memberpendings.Any())
                {
                    // memberpendings = memberpendings.Where(r => memberIds.Contains((long)r.MemberId));
                    if (memberpendings.Any())
                    {
                        memberpendings = memberpendings.Where(r => r.EffectiveDate >= fromDate && r.EffectiveDate <= toDate && r.CollectionStatus == false);
                        result = (from mbr in memebers
                                  join cat in GenService.GetAll<MemberBusinessCatagories>() on mbr.Id equals cat.MemberId into extra
                                  from ext in extra.DefaultIfEmpty()
                                  join pnd in memberpendings on mbr.Id equals pnd.MemberId into tl
                                  from extr in tl.DefaultIfEmpty()
                                  select new PendingFeeMemberDto
                                  {
                                      Id = mbr.Id,
                                      MemberNo = mbr.MemberNo != null ? mbr.MemberNo : "",
                                      NameOfOrganization = mbr.NameOfOrganization != null ? mbr.NameOfOrganization : "",
                                      Phone = mbr.Phone != null ? mbr.Phone : "",
                                      MobileNo = mbr.MobileNo != null ? mbr.MobileNo : "",
                                      Fax = mbr.Fax != null ? mbr.Fax : "",
                                      Email = mbr.Email != null ? mbr.Email : "",
                                      Website = mbr.Email != null ? mbr.Email : "",
                                      EstablishmentDate = mbr.EstablishmentDate != null ? mbr.EstablishmentDate : DateTime.Now,
                                      DateOfReceived = mbr.DateOfReceived != null ? mbr.DateOfReceived : DateTime.Now,
                                      OwnershipStatus =
                                          mbr.OwnershipStatus != null ? mbr.OwnershipStatus : OwnershipStatus.None,
                                      MembershipStatus =
                                          mbr.MembershipStatus != null ? mbr.MembershipStatus : MembershipStatus.Approved,
                                      ZoneName = mbr.Zone.ToString(),
                                      OwnershipStatusName = mbr.OwnershipStatus.ToString(),
                                      BusinessCategoryId = ext != null ? ext.BusinessCategoryId : 0,
                                      AreaId = mbr != null ? mbr.PresentAddress != null ? mbr.PresentAddress.AreaId : 0 : 0,
                                      MemberPendingFeeId = extr != null ? extr.Id : 0,
                                      PaymentStatus = (mbr.ExpiryDate > DateTime.Today ? (DbFunctions.DiffDays(DateTime.Today, mbr.ExpiryDate) > 60 ? BizConstants.Safe : BizConstants.Warning) : BizConstants.Deafaulter)
                                  }).ToList();
                        result = result.Where(r => r.MemberPendingFeeId > 0).ToList();
                        if (!string.IsNullOrEmpty(searchString))
                        {
                            searchString = searchString.ToLower();
                            result = result.Where(r => r.NameOfOrganization.ToLower().Contains(searchString) || r.MemberNo.ToLower().Contains(searchString)).ToList();
                        }

                        if (btype != null && btype > 0)
                        {
                            result = result.Where(r => r.BusinessCategoryId == btype).ToList();
                        }
                        if (zone != null)
                        {
                            result = result.Where(r => r.Zone == zone).ToList();
                        }
                        var resultDt = (from dt in result
                                        group dt by new { dt.Id } into grp
                                        let pendingFeeMemberDto = grp.FirstOrDefault()
                                        where pendingFeeMemberDto != null
                                        select new PendingFeeMemberDto
                                        {
                                            Id = grp.Key.Id,
                                            // MemberNo = grp.Key.MemberNo,
                                            NameOfOrganization = pendingFeeMemberDto.NameOfOrganization,
                                            Phone = pendingFeeMemberDto.Phone,
                                            MobileNo = pendingFeeMemberDto.MobileNo,
                                            Fax = pendingFeeMemberDto.MobileNo,
                                            Email = pendingFeeMemberDto.MobileNo,
                                            Website = pendingFeeMemberDto.MobileNo,
                                            EstablishmentDate = pendingFeeMemberDto.EstablishmentDate,
                                            DateOfReceived = pendingFeeMemberDto.DateOfReceived,
                                            OwnershipStatus = pendingFeeMemberDto.OwnershipStatus,
                                            MembershipStatus = pendingFeeMemberDto.MembershipStatus,
                                            ZoneName = pendingFeeMemberDto.ZoneName,
                                            OwnershipStatusName = pendingFeeMemberDto.OwnershipStatusName,
                                            BusinessCategoryId = pendingFeeMemberDto.BusinessCategoryId,
                                            AreaId = pendingFeeMemberDto.AreaId,
                                            MemberPendingFeeId = pendingFeeMemberDto.MemberPendingFeeId,
                                            PaymentStatus = pendingFeeMemberDto.PaymentStatus
                                        }).ToList();
                        return resultDt.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
                    }
                }
            }
            return result.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
        }

        public IPagedList<PendingFeeMemberDto> PendingFeeCollection( int pageSize, int pageCount, string searchString, long? btype, Zone? zone)
        {
            //DateTime? fromDate, DateTime? toDate,
            var memebers = GenService.GetAll<Member>().Where(r => r.MembershipStatus == MembershipStatus.Approved);
            var result = new List<PendingFeeMemberDto>();
            if (memebers.Any())
            {
                //var memberIds = memebers.Select(r => r.Id);
                var memberpendings = GenService.GetAll<MemberPendingFee>();
                if (memberpendings.Any())
                {
                    // memberpendings = memberpendings.Where(r => memberIds.Contains((long)r.MemberId));
                    if (memberpendings.Any())
                    {
                       // memberpendings = memberpendings.Where(r => r.EffectiveDate >= fromDate && r.EffectiveDate <= toDate);
                        //memberpendings = memberpendings.Where(r => r.EffectiveDate >= fromDate && r.EffectiveDate <= toDate && r.CollectionStatus == false);
                        result = (from mbr in memebers
                                  join cat in GenService.GetAll<MemberBusinessCatagories>() on mbr.Id equals cat.MemberId into extra
                                  from ext in extra.DefaultIfEmpty()
                                  join pnd in memberpendings on mbr.Id equals pnd.MemberId into tl
                                  from extr in tl.DefaultIfEmpty()
                                  select new PendingFeeMemberDto
                                  {
                                      Id = mbr.Id,
                                      MemberNo = mbr.MemberNo != null ? mbr.MemberNo : "",
                                      NameOfOrganization = mbr.NameOfOrganization != null ? mbr.NameOfOrganization : "",
                                      Phone = mbr.Phone != null ? mbr.Phone : "",
                                      MobileNo = mbr.MobileNo != null ? mbr.MobileNo : "",
                                      Fax = mbr.Fax != null ? mbr.Fax : "",
                                      Email = mbr.Email != null ? mbr.Email : "",
                                      Website = mbr.Email != null ? mbr.Email : "",
                                      EstablishmentDate = mbr.EstablishmentDate != null ? mbr.EstablishmentDate : DateTime.Now,
                                      IssueDate = mbr.IssueDate != null ? mbr.IssueDate : DateTime.Now,
                                      OwnershipStatus =
                                          mbr.OwnershipStatus != null ? mbr.OwnershipStatus : OwnershipStatus.None,
                                      MembershipStatus =
                                          mbr.MembershipStatus != null ? mbr.MembershipStatus : MembershipStatus.Approved,
                                      ZoneName = mbr.Zone.ToString(),
                                      OwnershipStatusName = mbr.OwnershipStatus.ToString(),
                                      BusinessCategoryId = ext != null ? ext.BusinessCategoryId : 0,
                                      AreaId = mbr != null ? mbr.PresentAddress != null ? mbr.PresentAddress.AreaId : 0 : 0,
                                      MemberPendingFeeId = extr != null ? extr.Id : 0,
                                      PaymentStatus = (mbr.ExpiryDate > DateTime.Today ? (DbFunctions.DiffDays(DateTime.Today, mbr.ExpiryDate) > 60 ? BizConstants.Safe : BizConstants.Warning) : BizConstants.Deafaulter)
                                  }).ToList();
                        result = result.Where(r => r.MemberPendingFeeId > 0).ToList();
                        if (!string.IsNullOrEmpty(searchString))
                        {
                            searchString = searchString.ToLower();
                            result = result.Where(r => r.NameOfOrganization.ToLower().Contains(searchString) || r.MemberNo.ToLower().Contains(searchString)).ToList();
                        }

                        if (btype != null && btype > 0)
                        {
                            result = result.Where(r => r.BusinessCategoryId == btype).ToList();
                        }
                        if (zone != null)
                        {
                            result = result.Where(r => r.Zone == zone).ToList();
                        }
                        var resultDt = (from dt in result
                                        group dt by new { dt.Id, dt.MemberNo } into grp
                                        let pendingFeeMemberDto = grp.FirstOrDefault()
                                        where pendingFeeMemberDto != null
                                        select new PendingFeeMemberDto
                                        {
                                            Id = grp.Key.Id,
                                            MemberNo = grp.Key.MemberNo,
                                            NameOfOrganization = pendingFeeMemberDto.NameOfOrganization,
                                            Phone = pendingFeeMemberDto.Phone,
                                            MobileNo = pendingFeeMemberDto.MobileNo,
                                            Fax = pendingFeeMemberDto.MobileNo,
                                            Email = pendingFeeMemberDto.MobileNo,
                                            Website = pendingFeeMemberDto.MobileNo,
                                            EstablishmentDate = pendingFeeMemberDto.EstablishmentDate,
                                            IssueDate = pendingFeeMemberDto.IssueDate,
                                            OwnershipStatus = pendingFeeMemberDto.OwnershipStatus,
                                            MembershipStatus = pendingFeeMemberDto.MembershipStatus,
                                            ZoneName = pendingFeeMemberDto.ZoneName,
                                            OwnershipStatusName = pendingFeeMemberDto.OwnershipStatusName,
                                            BusinessCategoryId = pendingFeeMemberDto.BusinessCategoryId,
                                            AreaId = pendingFeeMemberDto.AreaId,
                                            MemberPendingFeeId = pendingFeeMemberDto.MemberPendingFeeId,
                                            PaymentStatus = pendingFeeMemberDto.PaymentStatus
                                        }).ToList();
                        return resultDt.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
                    }
                }
            }
            return result.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
        }

        public MemberDetailsFeeWiseDto GetMemberDetails(long? memberId, long? memberFeeId)
        {
            var result = new MemberDetailsFeeWiseDto();
            var memberDetails = GenService.GetById<MemberPendingFee>((long)memberFeeId);
            var feeType = GenService.GetById<FeeType>((long)memberDetails.FeeTypeId);
            var collectionTran = GenService.GetAll<FeeCollectionTran>().Where(r => r.MemberId == memberId && r.FeeTypeId == feeType.Id);
            var totalAmount = collectionTran.Any() ? collectionTran.Sum(r => r.CollectedAmount) : 0;
            result.MemberId = (long)memberDetails.MemberId;
            result.MemberPendingFeeId = (long)memberFeeId;
            result.FeeTypeId = feeType.Id;
            //result.MemberNo = memberDetails.Member.MemberNo;
            //result.NameOfOrganization = memberDetails.Member.NameOfOrganization;
            //result.Phone = memberDetails.Member.Phone;
            result.TotalAmount = feeType.Amount;
            result.PaidAmount = totalAmount;
            result.Due = feeType.Amount - totalAmount;
            return result;
        }

        public string getFromYear(long? feeid, string effdate)
        {
            return ((long)feeid == BizConstants.AnnualSubscriptionFee ? DateTime.Parse(effdate).Year.ToString() + " - " + DateTime.Parse(effdate).AddYears(+2).Year.ToString() : DateTime.Parse(effdate).Year.ToString());
        }
        public MemberPendingFeeCollectionDto GetFeePendingMemberWise(long? memberId)
        {
            //var result = new MemberPendingFeeCollectionDto();
            //var memberPendings = GenService.GetAll<MemberPendingFee>().Where(r => r.MemberId == memberId && r.CollectionStatus == false);
            //var feeTypeIds = memberPendings.Select(r => r.FeeTypeId);
            //var feeType = GenService.GetAll<FeeType>().Where(l => feeTypeIds.Contains(l.Id));
            //var collectionTran = GenService.GetAll<FeeCollectionTran>().Where(r => r.MemberId == memberId && feeTypeIds.Contains(r.FeeTypeId));


            var result = new MemberPendingFeeCollectionDto(); 
            var memberPendings = GenService.GetAll<MemberPendingFee>().Where(r => r.MemberId == memberId && r.CollectionStatus==false);
            var pendingfeeIds = memberPendings.Select(r => r.Id);
            //var feeType = GenService.GetAll<FeeType>().Where(l => feeTypeIds.Contains(l.Id));
            var collectionTran = GenService.GetAll<FeeCollectionTran>().Where(r => r.MemberId == memberId && pendingfeeIds.Contains((long)r.PendingFeeId));




            //var totalAmount = collectionTran.Any() ? collectionTran.Sum(r => r.CollectedAmount) : 0;
            var memberDetails = GenService.GetById<Member>((long)memberId);
            result.MemberId = (long)memberDetails.Id;
            result.MemberNo = memberDetails.MemberNo;
            result.NameOfOrganization = memberDetails.NameOfOrganization;
            result.Phone = memberDetails.Phone;
            //var data = (from mbrRending in memberPendings
            //            from cat in collectionTran group cat by new {cat.FeeTypeId,cat.MemberId} into fee
            //            /*on*/
            //            //on mbrRending.FeeTypeId equals cat.FeeTypeId 
            //            //into extra
            //            //from ext in extra.DefaultIfEmpty()
            //            select new MemberDetailsFeeWiseDto()
            //            {
            //                MemberId = (long)fee.FirstOrDefault().MemberId,
            //                MemberPendingFeeId = (long)f,
            //                FeeTypeId = (long)mbrRending.FeeTypeId,
            //                FeeTypeName = mbrRending.FeeType != null ? mbrRending.FeeType.Name : "",
            //                TotalAmount = mbrRending.FeeType.Amount,
            //                PaidAmount =  cat.CollectedAmount,
            //                Due = 0
            //            }).ToList();

            var collection = from cat in collectionTran
                                 //join val in mee
                             group cat by new { cat.FeeType, cat.Member, cat.PendingFee }
                             into fee
                             select new FeeCollectionTranDto
                             {
                                 CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                 FeeTypeId = fee.Key.FeeType.Id,
                                 MemberId = fee.Key.Member.Id,
                                 PendingFeeId=fee.Key.PendingFee.Id,
                                 FeeTypeName = fee.Key.FeeType.Name
                             };

            if (collection.Count() > 0)
            {
                var data = (from mbrRending in memberPendings
                            join col in collection on mbrRending.Id equals col.PendingFeeId into extra
                            from ext in extra.DefaultIfEmpty()
                            select new MemberDetailsFeeWiseDto()
                            {
                                MemberId = (long)mbrRending.MemberId,
                                MemberPendingFeeId = (long)mbrRending.Id,
                                FeeTypeId = (long)mbrRending.FeeTypeId,
                                FeeTypeName = mbrRending.FeeType.Name,
                                TotalAmount = mbrRending.Amount,
                                ForYear = mbrRending.EffectiveDate.ToString(),
                                PaidAmount = ext.CollectedAmount
                            }).ToList();
                result.PendingFeeList = (from dt in data
                                         group dt by new { dt.MemberId, dt.MemberPendingFeeId, dt.FeeTypeId } into grp
                                         let memberDetailsFeeWiseDto = grp
                                         where memberDetailsFeeWiseDto != null
                                         select new MemberDetailsFeeWiseDto()
                                         {
                                             PendingFeeId= grp.Key.MemberPendingFeeId,
                                             MemberId = grp.Key.MemberId,
                                             MemberPendingFeeId = grp.Key.MemberPendingFeeId,
                                             FeeTypeId = grp.Key.FeeTypeId,
                                             FeeTypeName = memberDetailsFeeWiseDto.FirstOrDefault().FeeTypeName,
                                             TotalAmount = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount,
                                             PaidAmount = grp.Sum(l => l.PaidAmount),
                                             ForYear = getFromYear(grp.Key.FeeTypeId, memberDetailsFeeWiseDto.FirstOrDefault().ForYear),
                                             Due = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount),
                                             IsDue = (memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount)) > 0 ? true : false,
                                             SubscriptionDate = DateTime.Now


                                         }).ToList();
            }
            else
            {
                var data = (from mbrRending in memberPendings
                            join col in collection on mbrRending.Id equals col.PendingFeeId into extra
                            from ext in extra.DefaultIfEmpty()
                            select new MemberDetailsFeeWiseDto()
                            {
                                MemberId = (long)mbrRending.MemberId,
                                MemberPendingFeeId = (long)mbrRending.Id,
                                FeeTypeId = (long)mbrRending.FeeTypeId,
                                FeeTypeName = mbrRending.FeeType.Name,
                                TotalAmount = mbrRending.Amount,
                                ForYear = mbrRending.EffectiveDate.ToString(),
                                PaidAmount = mbrRending.CollectionStatus == true ? ext.CollectedAmount : 0
                            }).ToList();
                result.PendingFeeList = (from dt in data
                                         group dt by new { dt.MemberId, dt.MemberPendingFeeId, dt.FeeTypeId } into grp
                                         let memberDetailsFeeWiseDto = grp
                                         where memberDetailsFeeWiseDto != null
                                         select new MemberDetailsFeeWiseDto()
                                         {
                                             PendingFeeId = grp.Key.MemberPendingFeeId,
                                             MemberId = grp.Key.MemberId,
                                             MemberPendingFeeId = grp.Key.MemberPendingFeeId,
                                             FeeTypeId = grp.Key.FeeTypeId,
                                             FeeTypeName = memberDetailsFeeWiseDto.FirstOrDefault().FeeTypeName,
                                             TotalAmount = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount,
                                             PaidAmount = grp.Sum(l => l.PaidAmount),
                                             ForYear = getFromYear(grp.Key.FeeTypeId, memberDetailsFeeWiseDto.FirstOrDefault().ForYear),
                                             Due = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount),
                                             IsDue = (memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount)) > 0 ? true : false,
                                             SubscriptionDate = DateTime.Now


                                         }).ToList();
            }


            //if (collection.Count()>0)
            //{
            //    var data = (from mbrRending in memberPendings
            //                join col in collection on mbrRending.FeeTypeId equals col.FeeTypeId into extra
            //                from ext in extra.DefaultIfEmpty()
            //                select new MemberDetailsFeeWiseDto()
            //                {
            //                    MemberId = (long)mbrRending.MemberId,
            //                    MemberPendingFeeId = (long)mbrRending.Id,
            //                    FeeTypeId = (long)mbrRending.FeeTypeId,
            //                    FeeTypeName = mbrRending.FeeType.Name,
            //                    TotalAmount = mbrRending.Amount,
            //                    ForYear = mbrRending.EffectiveDate.ToString(),
            //                    PaidAmount =  ext.CollectedAmount
            //                }).ToList();
            //    result.PendingFeeList = (from dt in data
            //                             group dt by new { dt.MemberId, dt.MemberPendingFeeId, dt.FeeTypeId } into grp
            //                             let memberDetailsFeeWiseDto = grp
            //                             where memberDetailsFeeWiseDto != null
            //                             select new MemberDetailsFeeWiseDto()
            //                             {
            //                                 MemberId = grp.Key.MemberId,
            //                                 MemberPendingFeeId = grp.Key.MemberPendingFeeId,
            //                                 FeeTypeId = grp.Key.FeeTypeId,
            //                                 FeeTypeName = memberDetailsFeeWiseDto.FirstOrDefault().FeeTypeName,
            //                                 TotalAmount = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount,
            //                                 PaidAmount = grp.Sum(l => l.PaidAmount),
            //                                 ForYear = getFromYear(grp.Key.FeeTypeId, memberDetailsFeeWiseDto.FirstOrDefault().ForYear),
            //                                 Due = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount),
            //                                 IsDue = (memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount)) > 0 ? true : false,
            //                                 SubscriptionDate = DateTime.Now


            //                             }).ToList();
            //}
            //else
            //{
            //    var data = (from mbrRending in memberPendings
            //                join col in collection on mbrRending.FeeTypeId equals col.FeeTypeId into extra
            //                from ext in extra.DefaultIfEmpty()
            //                select new MemberDetailsFeeWiseDto()
            //                {
            //                    MemberId = (long)mbrRending.MemberId,
            //                    MemberPendingFeeId = (long)mbrRending.Id,
            //                    FeeTypeId = (long)mbrRending.FeeTypeId,
            //                    FeeTypeName = mbrRending.FeeType.Name,
            //                    TotalAmount = mbrRending.Amount,
            //                    ForYear = mbrRending.EffectiveDate.ToString(),
            //                    PaidAmount = mbrRending.CollectionStatus == true ? ext.CollectedAmount : 0
            //                }).ToList();
            //    result.PendingFeeList = (from dt in data
            //                             group dt by new { dt.MemberId, dt.MemberPendingFeeId, dt.FeeTypeId } into grp
            //                             let memberDetailsFeeWiseDto = grp
            //                             where memberDetailsFeeWiseDto != null
            //                             select new MemberDetailsFeeWiseDto()
            //                             {
            //                                 MemberId = grp.Key.MemberId,
            //                                 MemberPendingFeeId = grp.Key.MemberPendingFeeId,
            //                                 FeeTypeId = grp.Key.FeeTypeId,
            //                                 FeeTypeName = memberDetailsFeeWiseDto.FirstOrDefault().FeeTypeName,
            //                                 TotalAmount = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount,
            //                                 PaidAmount = grp.Sum(l => l.PaidAmount),
            //                                 ForYear = getFromYear(grp.Key.FeeTypeId, memberDetailsFeeWiseDto.FirstOrDefault().ForYear),
            //                                 Due = memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount),
            //                                 IsDue = (memberDetailsFeeWiseDto.FirstOrDefault().TotalAmount - grp.Sum(l => l.PaidAmount)) > 0 ? true : false,
            //                                 SubscriptionDate = DateTime.Now


            //                             }).ToList();
            //}

            return result;
        }
        public List<EnumDto> GetPaymentTypes()
        {
            var data = Enum.GetValues(typeof(PayType))
                       .Cast<PayType>()
                       .Select(t => new EnumDto
                       {
                           Id = ((int)t),
                           Name = UiUtil.GetDisplayName(t)
                       });
            return data.ToList();
        }

        public ResponseDto SaveFeePayment(MemberFeeCollectionDto dto, long userId, long companyId)
        {
            ResponseDto response = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            string voucherNumber;

            var total = dto.TotalPayableAmount;

            decimal totalmembershipfees = 0;
            decimal totaladvamnt = 0;
            var data = new List<FeeCollectionTran>();
            var list = new List<FeeCollectionTran>();
            var collect = new List<FeeCollectionTran>();
            var dataMoneyReceipt = new MoneyReceipt();
            var pendingFees = new List<MemberPendingFee>();
            var memberbasic = new Member();
            List<long> pendingFeeIds = new List<long>();
            pendingFeeIds = dto.PendingFeeList.Where(r => r.IsChecked == true).Select(l => l.MemberPendingFeeId).ToList();

            if (dto.FeeCollectionTrans != null && dto.FeeCollectionTrans.Count > 0)
            {
                dataMoneyReceipt.MemberId = dto.FeeCollectionTrans.FirstOrDefault().MemberId;
                dataMoneyReceipt.TotalCollectedAmount = (decimal)total;
                if(dto.PayType==PayType.Cash)
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedMRNo(1);
                else
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedBankMRNo(1);
                dataMoneyReceipt.MoneyReceiptDetails = new List<MoneyReceiptDetails>();
                memberbasic = GenService.GetById<Member>((long)dataMoneyReceipt.MemberId);

                foreach (var item in dto.FeeCollectionTrans)
                {
                    var fee = Mapper.Map<FeeCollectionTran>(item);
                    if (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee)
                        fee.Year += DateTime.Today.Year.ToString() + "-" + DateTime.Today.AddYears(+1).Year.ToString()+ "-" + DateTime.Today.AddYears(+2).Year.ToString();
                    else
                        fee.Year = DateTime.Today.Year.ToString();
                    fee.PayType = dto.PayType;
                    fee.CreateDate = DateTime.Now;
                    fee.CreatedBy = userId;
                    fee.Status = EntityStatus.Active;
                    data.Add(fee);

                    #region Payment Status Update
                    var pendingFee = GenService.GetAll<MemberPendingFee>()
                                        .Where(s => s.FeeTypeId == fee.FeeTypeId && s.MemberId == fee.MemberId && pendingFeeIds.Contains(s.Id)).FirstOrDefault();
                    if (fee.CollectedAmount == pendingFee.Amount)
                    {
                        if (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee)
                        {
                            memberbasic.ExpiryDate = pendingFee.EffectiveDate.Value.AddYears(+2);
                        }
                        else if(fee.FeeTypeId == BizConstants.MembershipFee)
                        {
                            memberbasic.MRNo = dataMoneyReceipt.MoneyReceiptNo;
                            memberbasic.PONo = fee.ChequeNo;
                            memberbasic.Bank = fee.Bank;
                        }
                        
                        pendingFee.CollectionStatus = true;
                        pendingFee.EditDate = DateTime.Now;
                        pendingFee.EditedBy = userId;
                        pendingFees.Add(pendingFee);
                    }
                    #endregion

                    #region Money Receipt
                    var moneyReceiptDtl = new MoneyReceiptDetails();
                    moneyReceiptDtl.CollectedAmount = fee.CollectedAmount;
                    moneyReceiptDtl.PayType = fee.PayType;
                    moneyReceiptDtl.SubscriptionDate = fee.SubscriptionDate;
                    moneyReceiptDtl.Year = fee.Year;
                    moneyReceiptDtl.ChequeNo = fee.ChequeNo != null ? fee.ChequeNo : "";
                    moneyReceiptDtl.CreateDate = DateTime.Now;
                    moneyReceiptDtl.CreatedBy = userId;
                    moneyReceiptDtl.Status = EntityStatus.Active;
                    dataMoneyReceipt.MoneyReceiptDetails.Add(moneyReceiptDtl);

                    #endregion

                    #region Voucher Entry 

                    if (fee.PayType == PayType.Cash)
                    {
                        var CashAccHead = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.CashInHand, null);
                        
                        if (string.IsNullOrEmpty(CashAccHead))
                        {
                            response.Message = "Cash in Hand, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = CashAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of "  + GetFeetypebyId((long)fee.FeeTypeId).Name  + " Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = fee.CollectedAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = _companyProfileFacade.DateToday(companyId).Date;
                        voucherList.Add(voucher);
                    }

                    if (fee.PayType == PayType.Cheque || fee.PayType == PayType.PayOrder)
                    {
                        var ChequeAccHead = dto.BankAccHead;
                        
                        if (string.IsNullOrEmpty(ChequeAccHead))
                        {
                            response.Message = "Cash at Bank, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = ChequeAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of "  + GetFeetypebyId((long)fee.FeeTypeId).Name + " Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = fee.CollectedAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = _companyProfileFacade.DateToday(companyId).Date;
                        voucherList.Add(voucher);
                    }

                    #endregion

                    #region contra voucher entry 
                    if (voucherList.Count > 0)
                    {
                        #region Membership Fee Head under Income

                        if (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee)
                        {
                            var annualfee = GenService.GetById<FeeType>(BizConstants.AnnualSubscriptionFee);
                            totalmembershipfees = (annualfee.Amount / 3);
                            totaladvamnt = (annualfee.Amount / 3) * 2;
                        }
                        else
                            totalmembershipfees = fee.CollectedAmount;

                        var CreditVoucher = new VoucherDto();

                        CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                        CreditVoucher.Description = "In time of Fee Collection against "+ GetFeetypebyId((long)fee.FeeTypeId).Name + " Fee.";
                        CreditVoucher.Credit = totalmembershipfees;
                        CreditVoucher.Debit = 0;
                        CreditVoucher.CompanyProfileId = companyId;
                        CreditVoucher.VoucherDate = _companyProfileFacade.DateToday(companyId).Date;

                        var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, fee.FeeTypeId);
                        if (string.IsNullOrEmpty(accHeadCode))
                        {
                            response.Message = "Membership Fee account head not found. Please contact system administrator.";
                            return response;
                        }
                        CreditVoucher.AccountHeadCode = accHeadCode;
                        voucherList.Add(CreditVoucher);

                        #endregion

                        if (totaladvamnt > 0)   //For Annual Subscribsion Fee
                        {
                            #region Membership Fee Advance Head under Current Asset
                            var CreditVoucherAdv = new VoucherDto();

                            CreditVoucherAdv.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                            CreditVoucherAdv.Description = "In time of Advance Annual Subscription  Fee.";
                            CreditVoucherAdv.Credit = totaladvamnt;
                            CreditVoucherAdv.Debit = 0;
                            CreditVoucherAdv.CompanyProfileId = companyId;
                            CreditVoucherAdv.VoucherDate = _companyProfileFacade.DateToday(companyId).Date;

                            var accHeadCodeadv = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Liability, fee.FeeTypeId);
                            if (string.IsNullOrEmpty(accHeadCodeadv))
                            {
                                response.Message = "Advance Membership Fee account head not found for the. Please contact system administrator.";
                                return response;
                            }
                            CreditVoucherAdv.AccountHeadCode = accHeadCodeadv;
                            voucherList.Add(CreditVoucherAdv);
                            #endregion
                        }
                    }

                    #endregion
                }

            }


            using (var tran = new TransactionScope())
            {
                try
                {
                    GenService.Save(data);
                    GenService.Save(dataMoneyReceipt);
                    GenService.Save(pendingFees);
                    GenService.Save(memberbasic);
                    response.Id = dataMoneyReceipt.Id;
                    if (voucherList.Count > 0)
                    {
                        var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                        if (dto.PayType == PayType.Cash)
                            voucherNumber = "CDV-" + _companyProfileFacade.GetUpdateCDvNo();
                        else
                            voucherNumber = "BDV-" + _companyProfileFacade.GetUpdateBDvNo();
                        voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                        var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, companyId, (long)userId);
                        if (!(bool)accountsResponse.Success)
                        {
                            response.Message = "Membership Fee Collection Failed. " + accountsResponse.Message;
                            return response;
                        }
                    }
                    GenService.SaveChanges();
                    tran.Complete();
                    response.Success = true;
                    response.Message = "Fee Collection Saved Successfully";

                }
                catch (Exception)
                {
                    tran.Dispose();
                    response.Message = "Fee Collection failed";
                }
            }

            return response;
        }

        public ResponseDto SaveFeePaymentExistingMember(MemberFeeCollectionDto dto, long userId, long companyId)
        {
            ResponseDto response = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            var voucherdate = _companyProfileFacade.DateToday(companyId).Date;
            string voucherNumber;
            var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
            

            var total = dto.TotalPayableAmount;

            decimal totalmembershipfees = 0;
            decimal totalchequeAmount = 0;
            decimal totalcashAmount = 0;
            decimal totaladvamnt = 0;
            var data = new List<FeeCollectionTran>();
            var list = new List<FeeCollectionTran>();
            var collect = new List<FeeCollectionTran>();
            var dataMoneyReceipt = new MoneyReceipt();
            var pendingFees = new List<MemberPendingFee>();
            var memberbasic = new Member();
            List<long> pendingFeeIds = new List<long>();
            pendingFeeIds = dto.PendingFeeList.Where(r => r.IsChecked == true).Select(l => l.MemberPendingFeeId).ToList();

            if (dto.FeeCollectionTrans != null && dto.FeeCollectionTrans.Count > 0)
            {
                dataMoneyReceipt.MemberId = dto.FeeCollectionTrans.FirstOrDefault().MemberId;
                dataMoneyReceipt.AuthorizedPerson = dto.MRAuthorizedPerson;
                dataMoneyReceipt.Description = dto.Description;
                dataMoneyReceipt.TotalCollectedAmount = (decimal)total;
                if (dto.PayType == PayType.Cash)
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedMRNo(1);
                else
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedBankMRNo(1);
                dataMoneyReceipt.MoneyReceiptDetails = new List<MoneyReceiptDetails>();
                memberbasic = GenService.GetById<Member>((long)dataMoneyReceipt.MemberId);

                foreach (var item in dto.FeeCollectionTrans)
                {
                    var fee = Mapper.Map<FeeCollectionTran>(item);
                    var feeinfo = GenService.GetById<FeeType>((long)fee.FeeTypeId);
                    if (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee)
                    {
                        string years = item.FromYear;
                        int temp = int.Parse(item.FromYear);
                        for (int i = 0; i < 2; i++)
                        {
                            temp++;
                            years = years + "-" + temp;
                        }
                        fee.Year += years;
                    }
                        
                    else
                        fee.Year = item.FromYear;
                    fee.PendingFeeId = item.PendingFeeId;
                    fee.PayType = dto.PayType;
                    fee.CreateDate = DateTime.Now;
                    fee.CreatedBy = userId;
                    fee.MoneyReceiptNo = dataMoneyReceipt.MoneyReceiptNo;
                    fee.SubscriptionDate = voucherdate;
                    fee.Status = EntityStatus.Active;
                    data.Add(fee);

                    #region Money Receipt
                    var moneyReceiptDtl = new MoneyReceiptDetails();
                    moneyReceiptDtl.CollectedAmount = fee.CollectedAmount;
                    moneyReceiptDtl.PayType = fee.PayType;
                    moneyReceiptDtl.SubscriptionDate = fee.SubscriptionDate;
                    moneyReceiptDtl.Year = fee.Year;
                    moneyReceiptDtl.ChequeNo = fee.ChequeNo != null ? fee.ChequeNo : "";
                    moneyReceiptDtl.CreateDate = DateTime.Now;
                    moneyReceiptDtl.CreatedBy = userId;
                    moneyReceiptDtl.Status = EntityStatus.Active;
                    dataMoneyReceipt.MoneyReceiptDetails.Add(moneyReceiptDtl);

                    #endregion
                    var yearsplits = fee.Year.ToString().Split('-');
                    var currentyear = _companyProfileFacade.DateToday(companyId).Date.Year;
                    var afterexpiryyear =(memberbasic.ExpiryDate!=null? (DateTime.Parse(memberbasic.ExpiryDate.ToString()).AddDays(+1).Year): _companyProfileFacade.DateToday(companyId).Date.Year);
                    if(fee.FeeTypeId== BizConstants.AnnualSubscriptionFee)
                    {
                        #region Voucher Entry 

                        if (fee.PayType == PayType.Cash)
                            totalcashAmount += fee.CollectedAmount;
                        else
                            totalchequeAmount += fee.CollectedAmount;
                        #region Fee Collection from Receivable
                            var annualfee = GenService.GetById<FeeType>(BizConstants.AnnualSubscriptionFee);
                        if (currentyear - afterexpiryyear<2)
                        {
                            totaladvamnt = (annualfee.Amount / 3) * (2 - (currentyear - afterexpiryyear));
                            totalmembershipfees = annualfee.Amount - totaladvamnt;
                        }
                        else
                        {
                            totaladvamnt = 0;
                            totalmembershipfees = annualfee.Amount - totaladvamnt;
                        }

                        if (totalmembershipfees > 0)
                        {
                            var CreditVoucher = new VoucherDto();

                            CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                            CreditVoucher.Description = "For year of " + (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee ? yearsplits[1] : fee.Year) + " from " + memberbasic.NameOfOrganization + " ."; //"In time of Fee Collection against " + GetFeetypebyId((long)fee.FeeTypeId).Name + " Fee.";
                            CreditVoucher.Credit = totalmembershipfees;
                            CreditVoucher.Debit = 0;
                            CreditVoucher.CompanyProfileId = companyId;
                            CreditVoucher.VoucherDate = voucherdate;

                            var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, fee.FeeTypeId);
                            if (string.IsNullOrEmpty(accHeadCode))
                            {
                                response.Message = "Subscription Fee account head not found. Please contact system administrator.";
                                return response;
                            }
                            CreditVoucher.AccountHeadCode = accHeadCode;
                            voucherList.Add(CreditVoucher);
                        }

                        #endregion

                        #region Advance Fee Collection under Liability
                        if (totaladvamnt > 0)   //For Annual Subscribsion Fee
                        {
                            #region Membership Fee Advance Head under Current Asset
                            var CreditVoucherAdv = new VoucherDto();

                            CreditVoucherAdv.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                            CreditVoucherAdv.Description = "For year of " + ((currentyear - afterexpiryyear)>0 ?yearsplits[2] :(yearsplits[1] + "-" + yearsplits[2]) )  + " from " + memberbasic.NameOfOrganization + " ."; //"In time of Advance Annual Subscription  Fee.";
                            CreditVoucherAdv.Credit = totaladvamnt;
                            CreditVoucherAdv.Debit = 0;
                            CreditVoucherAdv.CompanyProfileId = companyId;
                            CreditVoucherAdv.VoucherDate = voucherdate;

                            var accHeadCodeadv = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Liability, fee.FeeTypeId);
                            if (string.IsNullOrEmpty(accHeadCodeadv))
                            {
                                response.Message = "Advance Subscription Fee account head not found for the. Please contact system administrator.";
                                return response;
                            }
                            CreditVoucherAdv.AccountHeadCode = accHeadCodeadv;
                            voucherList.Add(CreditVoucherAdv);
                            #endregion
                        }
                        #endregion

                        #endregion
                    }
                    else   //For Normal Fee Collection
                    {
                        #region Voucher entry 
                       
                        if (fee.PayType == PayType.Cash)
                            totalcashAmount += fee.CollectedAmount;
                        else
                            totalchequeAmount += fee.CollectedAmount;

                        if (feeinfo.AccountReceivable)
                        {
                            #region Membership Fee Collection from Receivable
                            var CreditVoucher = new VoucherDto();

                            CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;

                            CreditVoucher.Description = "For year of " + fee.Year + " from " + memberbasic.NameOfOrganization + ". " + (dto.Description != null ? dto.Description : "");
                            //"In time of Fee Collection against " + GetFeetypebyId((long)fee.FeeTypeId).Name + " Fee.";
                            CreditVoucher.Credit = fee.CollectedAmount;
                            CreditVoucher.Debit = 0;
                            CreditVoucher.CompanyProfileId = companyId;
                            CreditVoucher.VoucherDate = voucherdate;

                            var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, fee.FeeTypeId);
                            //var accHeadCode = (fee.FeeTypeId == BizConstants.AnnualSubscriptionFee ? (_accounts.GetAccHeadCodeForVoucherZone(AccountHeadRefType.Income, fee.FeeTypeId, memberbasic.Zone)) : (_accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, fee.FeeTypeId)));
                            if (string.IsNullOrEmpty(accHeadCode))
                            {
                                response.Message = "Fee account head not found. Please contact system administrator.";
                                return response;
                            }
                            CreditVoucher.AccountHeadCode = accHeadCode;
                            voucherList.Add(CreditVoucher);

                            #endregion
                        }
                        else
                        {
                            #region Fee Collection Directly
                            var CreditVoucher = new VoucherDto();

                            CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                            CreditVoucher.Description = "For year of " + fee.Year + " from " + memberbasic.NameOfOrganization + ". " + (dto.Description != null ? dto.Description : "");
                            CreditVoucher.Credit = fee.CollectedAmount;
                            CreditVoucher.Debit = 0;
                            CreditVoucher.CompanyProfileId = companyId;
                            CreditVoucher.VoucherDate = voucherdate;

                            var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, fee.FeeTypeId);
                            if (string.IsNullOrEmpty(accHeadCode))
                            {
                                response.Message = "Membership Fee account head not found. Please contact system administrator.";
                                return response;
                            }
                            CreditVoucher.AccountHeadCode = accHeadCode;
                            voucherList.Add(CreditVoucher);

                            #endregion
                        }
                      

                        #endregion
                    }

                }

                #region Contra voucher entry for All 
                if (voucherList.Count > 0)
                {

                    if (dto.PayType == PayType.Cash)
                    {
                        var CashAccHead = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.CashInHand, null);

                        if (string.IsNullOrEmpty(CashAccHead))
                        {
                            response.Message = "Cash in Hand, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = CashAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of Various Membership Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = totalcashAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = voucherdate;
                        voucherList.Add(voucher);
                    }

                    else
                    {
                        var ChequeAccHead = dto.BankAccHead;

                        if (string.IsNullOrEmpty(ChequeAccHead))
                        {
                            response.Message = "Cash at Bank, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = ChequeAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of Various Membership Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = totalchequeAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = voucherdate;
                        voucherList.Add(voucher);
                    }
                }

                #endregion

                #region Payment Status Update
                var oldpayments = GenService.GetAll<FeeCollectionTran>().Where(x=>x.MemberId==memberbasic.Id);
                var Oldcollections = from cat in oldpayments
                                  group cat by new { cat.FeeTypeId, cat.MemberId, cat.PendingFeeId, cat.Year}
                                  into fee
                                  select new FeeCollectionTranDto
                                  {
                                      CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                      FeeTypeId = fee.Key.FeeTypeId,
                                      MemberId = fee.Key.MemberId,
                                      PendingFeeId = fee.Key.PendingFeeId,
                                      FromYear=fee.Key.Year

                                  };

                var presentcollections = from cat in dto.FeeCollectionTrans
                                  group cat by new { cat.FeeTypeId , cat.MemberId, cat.PendingFeeId, cat.FromYear}
                                  into fee
                                  select new FeeCollectionTranDto
                                 {
                                     CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                     FeeTypeId = fee.Key.FeeTypeId,
                                     MemberId = fee.Key.MemberId,
                                     PendingFeeId=fee.Key.PendingFeeId,
                                      FromYear = fee.Key.FromYear
                                  };
                var allcollections = new List<FeeCollectionTranDto>();
                allcollections.AddRange(Oldcollections);
                allcollections.AddRange(presentcollections);

                var collections= from col in allcollections
                                 group col by new { col.FeeTypeId, col.MemberId, col.PendingFeeId,col.FromYear }
                                 into fee
                                 select new FeeCollectionTranDto
                                 {
                                     CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                     FeeTypeId = fee.Key.FeeTypeId,
                                     MemberId = fee.Key.MemberId,
                                     PendingFeeId = fee.Key.PendingFeeId,
                                     FromYear = fee.Key.FromYear
                                 };

                foreach (var collection in collections)
                {
                    var pendingFee = GenService.GetAll<MemberPendingFee>()
                                    .Where(s => s.FeeTypeId == collection.FeeTypeId && s.MemberId == memberbasic.Id && pendingFeeIds.Contains(s.Id)).FirstOrDefault();

                    if (pendingFee != null)
                    {
                        if (collection.CollectedAmount == pendingFee.Amount)
                        {
                            if (pendingFee.CollectionStatus == false)
                            {
                                if (collection.FeeTypeId == BizConstants.AnnualSubscriptionFee)
                                {
                                    DateTime theDate;
                                    if (collection.FromYear.ToString().Contains('-'))
                                    {
                                        var splityear = collection.FromYear.ToString().Split('-');
                                        theDate = new DateTime(int.Parse(splityear[2]), 12, 31);
                                    }
                                    else
                                        theDate = new DateTime(int.Parse(collection.FromYear), 12, 31);
                                    if(pendingFee.EffectiveDate>voucherdate)
                                        memberbasic.ExpiryDate = theDate;
                                    else
                                        memberbasic.ExpiryDate = theDate.AddYears(+2);
                                }
                                if (collection.FeeTypeId == BizConstants.MembershipFee)
                                {
                                    memberbasic.MRNo = dataMoneyReceipt.MoneyReceiptNo;
                                    memberbasic.PONo = collection.ChequeNo;
                                    memberbasic.Bank = collection.Bank;
                                }

                                pendingFee.CollectionStatus = true;
                                pendingFee.EditDate = DateTime.Now;
                                pendingFee.EditedBy = userId;
                                pendingFees.Add(pendingFee);
                            }
                        }
                        else
                        {
                            pendingFee.CollectionStatus = false;
                            pendingFee.EditDate = DateTime.Now;
                            pendingFee.EditedBy = userId;
                            pendingFees.Add(pendingFee);
                        }
                    }
                    
                }
                    
                #endregion

            }


            using (var tran = new TransactionScope())
            {
                try
                {
                    GenService.Save(data);
                    if (dto.PayType == PayType.Cash)
                        voucherNumber = "CCV-" + _companyProfileFacade.GetUpdateCCvNo();
                    else
                        voucherNumber = "BCV-" + _companyProfileFacade.GetUpdateBCvNo();
                    if (voucherList.Count > 0)
                        dataMoneyReceipt.VoucherNo = voucherNumber;
                    
                    GenService.Save(dataMoneyReceipt);
                    GenService.Save(pendingFees);
                    GenService.Save(memberbasic);
                    response.Id = dataMoneyReceipt.Id;
                    
                    if (voucherList.Count > 0)
                    {
                        voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                        var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, companyId, (long)userId);
                        if (!(bool)accountsResponse.Success)
                        {
                            response.Message = "Membership Fee Collection Failed. " + accountsResponse.Message;
                            return response;
                            
                        }
                        response.Complementary = voucherNumber;
                    }
                    GenService.SaveChanges();
                    tran.Complete();
                    response.Success = true;
                    response.Message = "Fee Collection Saved Successfully";

                }
                catch (Exception)
                {
                    tran.Dispose();
                    response.Message = "Fee Collection failed";
                }
            }

            return response;
        }

        public MoneyReceiptDto GetMoneyReceiptById(long receiptId)
        {
                var data = GenService.GetById<MoneyReceipt>(receiptId);
                return Mapper.Map<MoneyReceiptDto>(data);
        }

        public IPagedList<MoneyReceiptDto> MoneyReceiptList(long memberId, DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount)
        {

            var transactions = GenService.GetAll<MoneyReceipt>().Where(m => m.MemberId == memberId);

            if (fromDate != null && fromDate > DateTime.MinValue)
                transactions = transactions.Where(m => m.CreateDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                transactions = transactions.Where(m => m.CreateDate <= toDate);
            var transactionList = from trans in transactions
                                  select new MoneyReceiptDto
                                  {
                                      Id = trans.Id,
                                      MemberId = trans.MemberId,
                                      MemberName = trans.Member.NameOfOrganization,
                                      TotalCollectedAmount = trans.TotalCollectedAmount,
                                      MoneyReceiptNo = trans.MoneyReceiptNo,
                                      CreateDate = trans.CreateDate
                                  };

            return transactionList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public IPagedList<MoneyReceiptDto> AllMoneyReceiptList(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount)
        {

            var transactions = GenService.GetAll<MoneyReceipt>();
            var studentMrs =   GenService.GetAll<TraineeMoneyReceipt>();

            if (fromDate != null && fromDate > DateTime.MinValue)
                transactions = transactions.Where(m => m.CreateDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                transactions = transactions.Where(m => m.CreateDate <= toDate);
            if (fromDate != null && fromDate > DateTime.MinValue)
                studentMrs = studentMrs.Where(m => m.CreateDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                studentMrs = studentMrs.Where(m => m.CreateDate <= toDate);
            var traineelist = (from trans in studentMrs
                              select new MoneyReceiptDto
                                  {
                                      Id = trans.Id,
                                      TraineeId = trans.TraineeId,
                                      MemberName = trans.Trainee.Name,
                                      MRType = "Trainee",
                                      TotalCollectedAmount = trans.TotalCollectedAmount,
                                      MoneyReceiptNo = trans.MoneyReceiptNo,
                                      CreateDate = trans.CreateDate,
                                      VoucherNo = trans.VoucherNo,
                                      IsVoucher = (trans.VoucherNo != null ? true : false)
                                  }).ToList();
            var transactionList = (from trans in transactions
                                  select new MoneyReceiptDto
                                  {
                                      Id = trans.Id,
                                      MemberId = trans.MemberId,
                                      MemberName = trans.Member.NameOfOrganization,
                                      MRType="Member",
                                      TotalCollectedAmount = trans.TotalCollectedAmount,
                                      MoneyReceiptNo = trans.MoneyReceiptNo,
                                      CreateDate = trans.CreateDate,
                                      VoucherNo=trans.VoucherNo,
                                      IsVoucher=(trans.VoucherNo!=null?true:false)
                                  }).ToList();
            transactionList.AddRange(traineelist);

            return transactionList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public List<FeeTypeDto> GetFeeTypes(List<long> ids)
        {
            var data = GenService.GetAll<FeeType>()
                .Where(s => s.Status == EntityStatus.Active)
                       .Select(t => new FeeTypeDto
                       {
                           Id = t.Id,
                           Name = t.Name
                       });
            if (ids == null)
                return data.ToList();
            else
                return data.Where(x => ids.Contains((int)x.Id)).ToList();
        }

        public List<FeeTypeDto> GetAllFeetypes()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x=>x.CompanyProfileId==1) //ATAB Office=1
               .Select(t => new FeeTypeDto()
               {
                   Id = t.Id,
                   Name = t.Name,
                   IsQuantitive = t.IsQuantitive
                   //DisplayName = UiUtil.GetDisplayName(t)
               });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == false && r.Id>1);
            }
            return typeList.ToList();
        }

        public FeeTypeDto GetFeetypebyId(long id)
        {
            var feetype = GenService.GetById<FeeType>(id);
           return Mapper.Map<FeeTypeDto>(feetype);
        }

        public List<MemberDto> GetEmployeesForFeeCollection(List<long?> feeTypeIds, long? zone, long? division, string name, long companyProfileId)
        {
            var systemDate = _companyProfileFacade.DateToday(companyProfileId).Date;

            // var members = GenService.GetAll<Member>().Where(r => r.ExpiryDate <= systemDate);
            var members = GenService.GetAll<Member>().Where(r => r.MembershipStatus == MembershipStatus.Approved);
            if (zone != null)
            {
                members = members.Where(t => t.Zone == (Zone)zone);
            }
            //if (feeTypeIds.Contains(BizConstants.AnnualSubscriptionFee))
            //    members = members.Where(x => x.ExpiryDate <= systemDate);
            if (division != null)
            {
                members = members.Where(t => t.PresentAddress != null && t.PresentAddress.DivisionId == division);
            }
            if (name != "")
                members = members.Where(t => t.NameOfOrganization.Contains(name));
            var memberIds = members.Select(r => r.Id);
            var memberFeeCol = GenService.GetAll<MemberPendingFee>().Where(r => r.CollectionStatus == false && memberIds.Contains((long)r.MemberId) && feeTypeIds.Contains((r.FeeTypeId)));
            var memberFeeColIds = memberFeeCol.Select(l => l.MemberId).Distinct();
            if (memberFeeColIds != null)
            {
                var eligibleMember = members.Where(l => !memberFeeColIds.Contains(l.Id));
                var result = Mapper.Map<List<MemberDto>>(eligibleMember);
                return result;
            }

            else
            {
                var result = Mapper.Map<List<MemberDto>>(members);
                return result;
            }
           
        }

        public ResponseDto SaveMemberFeeCollectionRenewal(FeeCollectionRenewalDto dto, long userId, long selectedCompanyId)
        {

            ResponseDto responce = new ResponseDto();
            var pendingFeeList = new List<MemberPendingFee>();
            var voucherList = new List<VoucherDto>();
            
            //var memberbasic = GenService.GetById<Member>((long)dto.MemberId);
            using (var tran = new TransactionScope())
            {
                try
                {
                    decimal total = 0;
                    var members = dto.Members.Any() ? dto.Members.Where(r => r.IsChecked == true) : null;
                    if (members != null && members.Any())
                    {
                        foreach (var dt in members)
                        {
                            foreach (var fee in dto.FeeTypeIds)
                            {
                                var feeInfo = GenService.GetById<FeeType>(fee);
                                var mbr = new MemberPendingFee();
                                var memberbasic = GenService.GetById<Member>((long)dt.Id);
                                
                                if (dt.IsQuantitive != null && dt.IsQuantitive == true)
                                {
                                    total = total + (feeInfo.Amount*dt.Quantity);
                                }
                                else
                                {
                                    total = total + feeInfo.Amount;
                                }
                                mbr.Amount = total;
                                mbr.MemberId = dt.Id;
                                mbr.FeeTypeId = fee;
                                mbr.CollectionStatus = false;
                                mbr.CreateDate = DateTime.Now;
                                mbr.CreatedBy = userId;
                                mbr.Status = EntityStatus.Active;
                                mbr.EffectiveDate = (fee==BizConstants.AnnualSubscriptionFee? (DateTime.Parse(memberbasic.ExpiryDate.ToString()).AddDays(+1)): _companyProfileFacade.DateToday(selectedCompanyId).Date) ;
                                pendingFeeList.Add(mbr);
                                total = 0;

                                if (feeInfo.AccountReceivable)
                                {
                                    var currentyear = _companyProfileFacade.DateToday(selectedCompanyId).Date.Year;
                                    
                                    #region Voucher Entry 
                                    if (fee == BizConstants.AnnualSubscriptionFee)
                                    {
                                        var yeardifference = (currentyear - DateTime.Parse(memberbasic.ExpiryDate.ToString()).AddDays(+1).Year);
                                        if ((yeardifference>0?yeardifference:10) < 2 )
                                        {
                                            var annualfee = feeInfo.Amount;
                                            var totalreceivable = (annualfee / 3);
                                            var totalincome = totalreceivable;

                                            #region Fee Collection voucher entry
                                            var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, feeInfo.Id); //aDetail.CategoryId
                                                                                                                                             //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                                            if (string.IsNullOrEmpty(accHeadCode))
                                            {
                                                responce.Message = "Account head not found";
                                                return responce;
                                            }
                                            var voucher = new VoucherDto();
                                            voucher.AccountHeadCode = accHeadCode;
                                            voucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                            voucher.Description = "Receivable for " + feeInfo.Name + " Fee";
                                            voucher.Credit = 0;
                                            voucher.Debit = totalreceivable;
                                            voucher.CompanyProfileId = selectedCompanyId;
                                            voucher.VoucherDate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
                                            voucherList.Add(voucher);
                                            #endregion

                                            #region contra voucher entry 
                                            var CreditVoucher = new VoucherDto();
                                            CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                            CreditVoucher.Description = "For the year of " + currentyear + " from " + memberbasic.NameOfOrganization + " .";
                                            CreditVoucher.Credit = totalincome;
                                            CreditVoucher.Debit = 0;
                                            CreditVoucher.CompanyProfileId = selectedCompanyId;
                                            CreditVoucher.VoucherDate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
                                            var CreditAccHead = (feeInfo.Id == BizConstants.AnnualSubscriptionFee ? (_accounts.GetAccHeadCodeForVoucherZone(AccountHeadRefType.Income, feeInfo.Id, dt.Zone)) : (_accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, feeInfo.Id)));
                                            if (string.IsNullOrEmpty(CreditAccHead))
                                            {
                                                responce.Message = "Account head not found";
                                                return responce;
                                            }
                                            CreditVoucher.AccountHeadCode = CreditAccHead;
                                            voucherList.Add(CreditVoucher);


                                            #endregion
                                        }

                                    }
                                    

                                    else
                                    {
                                        #region Fee Collection voucher entry
                                        var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable, feeInfo.Id); //aDetail.CategoryId
                                                                                                                                         //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                                        if (string.IsNullOrEmpty(accHeadCode))
                                        {
                                            responce.Message = "Account head not found";
                                            return responce;
                                        }
                                        var voucher = new VoucherDto();
                                        voucher.AccountHeadCode = accHeadCode;
                                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                        voucher.Description = "Receivable for " + feeInfo.Name + " Fee";
                                        voucher.Credit = 0;
                                        voucher.Debit = total;
                                        voucher.CompanyProfileId = selectedCompanyId;
                                        voucher.VoucherDate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
                                        voucherList.Add(voucher);
                                        #endregion

                                        #region contra voucher entry 
                                        var CreditVoucher = new VoucherDto();
                                        CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                                        CreditVoucher.Description = "For the year of " + currentyear + " from " + memberbasic.NameOfOrganization + " .";
                                        CreditVoucher.Credit = total;
                                        CreditVoucher.Debit = 0;
                                        CreditVoucher.CompanyProfileId = selectedCompanyId;
                                        CreditVoucher.VoucherDate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
                                        var CreditAccHead = (feeInfo.Id == BizConstants.AnnualSubscriptionFee ? (_accounts.GetAccHeadCodeForVoucherZone(AccountHeadRefType.Income, feeInfo.Id, dt.Zone)) : (_accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income, feeInfo.Id)));
                                        if (string.IsNullOrEmpty(CreditAccHead))
                                        {
                                            responce.Message = "Account head not found";
                                            return responce;
                                        }
                                        CreditVoucher.AccountHeadCode = CreditAccHead;
                                        voucherList.Add(CreditVoucher);
                                        #endregion
                                    }
                                    #endregion
                                }



                            }
                        }
                        GenService.Save(pendingFeeList);
                        if (voucherList.Count > 0)
                        {

                            var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                            var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                            voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                            var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, selectedCompanyId, userId);
                            //responce.Id = entity.Id;
                            if (!(bool)accountsResponse.Success)
                            {
                                responce.Message = "Fee Collection Saving Failed. " + accountsResponse.Message;
                                //tran.Dispose();
                                return responce;
                            }
                        }
                        tran.Complete();
                        responce.Success = true;
                        responce.Message = "Member Pending Fee Saved Successfully";
                    }
                    else
                    {
                        tran.Dispose();
                        responce.Message = "There is no selected Members.";
                    }
                }
                catch (Exception ex)
                {
                    tran.Dispose();
                    responce.Message = "Member Pending Fee Save Failed";
                }
            }
            return responce;
        }

        public List<FeeTypeDto> GetQuantitiveFeetypes()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x=>x.CompanyProfileId==1) // ATAB Office=1
          .Select(t => new FeeTypeDto()
          {
              Id = t.Id,
              Name = t.Name,
              IsQuantitive = t.IsQuantitive
              //DisplayName = UiUtil.GetDisplayName(t)
          });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == true);
            }
            return typeList.ToList();
        }

        public List<YearDto> GetYears()
        {
            var startDate = DateTime.Today.AddYears(-25);
            var years = Enumerable.Range(0, 30)
                       .Select(startDate.AddYears)
                       .Select(m => m.ToString("yyyy"))
                       .ToList();
            
           var result = from year in years
           select new YearDto()
           {
               Year = year
           };
            return result.ToList();
        }

 
        public ResponseDto RectifyTranData(DateTime fromDate, DateTime toDate)
        {

            var result = new ResponseDto();
            var members = GenService.GetAll<FeeCollectionTran>().Select(x=>x.MemberId).Distinct();
           //var members =new long[] {324,560};
            var entity = new FeeCollectionTran();
            var tranlist = new List<FeeCollectionTran>();
            var feecollectiontran = new List<FeeCollectionTran>();
            foreach (var member in members)
            {
                feecollectiontran = GenService.GetAll<FeeCollectionTran>().Where(x => x.MemberId == member).OrderBy(x => x.Id).ToList();
                var vouchers = GenService.GetAll<MoneyReceipt>().Where(x=>x.MemberId==member).Select(y => new { y.VoucherNo,y.Id,y.MoneyReceiptNo }).OrderBy(x=>x.Id).ToList();
                foreach (var item in vouchers)
                {
                    var voucherinfo = _accountsfacade.GetVoucherByVoucherNo(item.VoucherNo);
                    var totaldebit = voucherinfo.Sum(x => x.Amount);
                    var voucherdate = voucherinfo.Select(x=>x.VoucherDate).FirstOrDefault();
                    var count = 0;
                    foreach (var tran in feecollectiontran)
                    {
                        if (totaldebit - tran.CollectedAmount>=0)
                        {
                            entity = GenService.GetById<FeeCollectionTran>((long)tran.Id);
                            if(tran.SubscriptionDate!=voucherdate)
                                entity.SubscriptionDate = voucherdate;
                            entity.MoneyReceiptNo = item.MoneyReceiptNo;
                            entity.EditDate = DateTime.Now;
                            entity.EditedBy = (long)1;
                            tranlist.Add(entity);
                            totaldebit = totaldebit - tran.CollectedAmount;
                            count += 1;
                        }
                    }
                    feecollectiontran.RemoveRange(0, count);
                }
                
            }
            try
            {
                GenService.Save(tranlist);
            }
            catch (Exception ex)
            {
                result.Message = " Failed";
            }

            return result;
        }
    }
}
