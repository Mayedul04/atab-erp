﻿using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Auth.Facade;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace Finix.Membership.Facade
{
    public class TraineeFacade : BaseFacade
    {
        private readonly EnumFacade _enum = new EnumFacade();
        private readonly SequencerFacade _seqFacade = new SequencerFacade();
        private readonly MembeshipAccountFacade _accounts = new MembeshipAccountFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();

        public List<TraineeDto> GetStudentListForAutoFill(string prefix, List<long> exclusionList)
        {
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();
            var data = GenService.GetAll<Trainee>()
                .Where(x => (x.Name.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id))
                .OrderBy(x => x.Name)
                .Select(x => new TraineeDto { Id = x.Id, Name = x.Name})
                .Take(10)
                .ToList();
            return data;
        }
        public ResponseDto SaveTrainee(TraineeDto trainee, long officeId, long userId)
        {
            var response = new ResponseDto();

            if (trainee.Id > 0)
            {
                try
                {
                    var entity = GenService.GetById<Trainee>((long)trainee.Id);
                    trainee.UrlOfPhoto = entity.UrlOfPhoto;
                    trainee.UrlOfSignature = entity.UrlOfSignature;
                    Mapper.Map(trainee, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    GenService.Save(entity);
                    response.Success = true;
                    response.Message = "Trainee information is Updated.";
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = "Failed.";
                }
            }
            else
            {
                try
                {
                    var entity = Mapper.Map<Trainee>(trainee);
                    if (trainee.FromYear < DateTime.Today.Year)
                    {
                        DateTime theDate = new DateTime(trainee.FromYear, 1, 1);
                        entity.DateOfAdmission = theDate;
                    }
                    else
                        entity.DateOfAdmission = trainee.DateOfAdmission;
                    entity.Courses = new List<TraineeCourses>();
                    entity.Courses = Mapper.Map<List<TraineeCourses>>(trainee.Courses);
                    var traineeNo = _seqFacade.GetUpdatedTraineeNo(officeId);
                    entity.TraineeNo = traineeNo;
                    GenService.Save(entity);
                    response.Id = entity.Id;
                    response.Success = true;
                    response.Message = "Trainee information is saved. Trainee identification id is " + entity.TraineeNo;
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = "Failed.";
                }
            }
            // GenService.SaveChanges();
            return response;
        }

        public ResponseDto TraineeAdmission(TraineeDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Trainee();
            entity = GenService.GetById<Trainee>((long)dto.Id);

            #region Add

            try
            {
                entity.MRNo = dto.MRNo;
                entity.Batch = dto.Batch;
                entity.TotalFeeAmount = dto.TotalFeeAmount;
                //entity.TotalCollectedAmount = dto.TotalCollectedAmount;
                entity.DateOfAdmission = dto.DateOfAdmission;
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.Status = EntityStatus.Active;
                if (entity.Courses.Count() > 0)
                {
                    foreach (var v in entity.Courses)
                    {
                        if (v.Status == EntityStatus.Inactive)
                        {
                            response.Success = false;
                            response.Message = "Course Fee are Pending for this Student?";
                            return response;
                        }
                    }
                }
                else
                {
                    response.Message = "Set Student's Course!";
                    return response;
                }
                
                

                foreach (var v in entity.Courses)
                {
                    entity.TotalFeeAmount += v.CourseFee;
                }
                
                GenService.Save(entity);
                response.Success = true;
                response.Message = "Trainee Have been admitted succesfully. Trainee identification id is " + entity.TraineeNo;
            }
            catch (Exception ex)
            {
                response.Message = "Trainee Admission Failed!.";
            }


            #endregion

            GenService.SaveChanges();

            return response;
        }

        public ResponseDto SaveAddress(TraineeDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            Address entity = new Address();
            if (dto.Address.Id > 0)
            {
                entity = GenService.GetById<Address>((long)dto.Address.Id);
                Mapper.Map(dto.Address, entity);
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                GenService.Save(entity);
                response.Success = true;
                response.Message = "Trainee Addess is Updated.";
            }
            else
            {
                entity = AutoMapper.Mapper.Map<Address>(dto.Address);
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                GenService.Save(entity);
                response.Success = true;
                response.Message = "Trainee Addess is saved.";
            }
            GenService.SaveChanges();
            return response;
        }
        public ResponseDto SaveCourses(TraineeDto dto, long userId, long selectedCompanyId)
        {
            ResponseDto response = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            var existingcourses = GenService.GetAll<TraineeCourses>().Where(m => m.TraineeId == dto.Id);
            decimal totalreceivable = 0;
            decimal totalincome = 0;
            var pendingFeeList = new List<TraineeCourses>();
            var voucherdate= _companyProfileFacade.DateToday(selectedCompanyId).Date;
            var currentyear = voucherdate.Year;
            
            foreach (var item in dto.Courses)
            {
                if (existingcourses.Any(x => x.Course == item.Course))
                {
                    response.Success = false;
                    response.Message = item.CourseName + " Already have been taken!";
                    break;
                }
                TraineeCourses course;
                if (item.Id != null && item.Id > 0)
                {
                    course = GenService.GetById<TraineeCourses>((long)item.Id);
                    item.Status = course.Status;
                    item.CreateDate = course.CreateDate;
                    item.CreatedBy = course.CreatedBy;
                    item.EditDate = DateTime.Now;
                    item.EditedBy = userId;
                    item.TraineeId = (long)dto.Id;
                    Mapper.Map(item, course);
                    GenService.Save(course, false);
                }
                else
                {
                    if (item.Course == null || item.Course==0)
                    {
                        response.Message = "Please select a valid Course!";
                        return response;
                    }
                    else
                    {
                        course = new TraineeCourses();
                        item.CreateDate = DateTime.Now;
                        item.CreatedBy = userId;
                        if (dto.FromYear < currentyear)
                            item.Status = EntityStatus.Active;
                        else
                            item.Status = EntityStatus.Inactive;
                        item.EffectiveDate = DateTime.Now;
                        item.TraineeId = (long)dto.Id;
                        course = Mapper.Map<TraineeCourses>(item);

                        pendingFeeList.Add(course);
                        var feeinfo = GenService.GetById<FeeType>((long)item.Course);
                        if (feeinfo.AccountReceivable)
                        {
                            totalreceivable += item.CourseFee;
                            totalincome += item.CourseFee;
                        }
                        else
                        {
                            totalreceivable += 0;
                            totalincome += item.CourseFee;
                        }
                            
                     }
                  }

            }

            
            if (dto.FromYear == currentyear)
            {
                if (totalreceivable > 0)
                {
                    #region Fee Collection voucher entry
                    var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable_ATTI, null); //aDetail.CategoryId
                                                                                                                    //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                    if (string.IsNullOrEmpty(accHeadCode))
                    {
                        response.Message = "Account head not found";
                        return response;
                    }
                    var voucher = new VoucherDto();
                    voucher.AccountHeadCode = accHeadCode;
                    voucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                    voucher.Description = "";
                    voucher.Credit = 0;
                    voucher.Debit = totalreceivable;
                    voucher.CompanyProfileId = selectedCompanyId;
                    voucher.VoucherDate = voucherdate;
                    voucherList.Add(voucher);
                    #endregion

                    #region contra voucher entry 
                    var CreditVoucher = new VoucherDto();
                    CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Journal;
                    CreditVoucher.Description = "";
                    CreditVoucher.Credit = totalincome;
                    CreditVoucher.Debit = 0;
                    CreditVoucher.CompanyProfileId = selectedCompanyId;
                    CreditVoucher.VoucherDate = voucherdate;
                    var CreditAccHead = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income_ATTI, null);
                    if (string.IsNullOrEmpty(CreditAccHead))
                    {
                        response.Message = "Account head not found";
                        return response;
                    }
                    CreditVoucher.AccountHeadCode = CreditAccHead;
                    voucherList.Add(CreditVoucher);
                    #endregion
                }
            }


            try
            {
                GenService.Save(pendingFeeList);

                response.Success = true;
                response.Message = "Training Course Fee Saved Successfully";
                if (voucherList.Count > 0)
                {

                    var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                    var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                    voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                    var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, selectedCompanyId, userId);
                    //responce.Id = entity.Id;
                    if (!(bool)accountsResponse.Success)
                    {
                        response.Message = "Fee Collection Saving Failed. " + accountsResponse.Message;
                        return response;
                    }
                }
                GenService.SaveChanges();
                response.Success = true;
                response.Message = "Training Course are saved.";
            }
            catch (Exception ex)
            {
                response.Message = "Training Course are not saved!";
                return response;
            }

            return response;
        }
        public List<FeeTypeDto> GetAllCourses()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x => x.CompanyProfileId == 2) //ATTI Office=2
               .Select(t => new FeeTypeDto()
               {
                   Id = t.Id,
                   Name = t.Name,
                   IsQuantitive = t.IsQuantitive
                   
               });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == true);
            }
            return typeList.ToList();
        }
        public List<FeeTypeDto> GetAllCoursesExceptOtherFees()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x => x.CompanyProfileId == 2 && x.AccountReceivable==true) //ATTI Office=2
               .Select(t => new FeeTypeDto()
               {
                   Id = t.Id,
                   Name = t.Name,
                   IsQuantitive = t.IsQuantitive

               });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == true);
            }
            return typeList.ToList();
        }


        public TraineeDto GetStudentInformation(long id)
        {
            var trainee = GenService.GetById<Trainee>(id);
            string ImagePath=string.Empty;
            string Signature= string.Empty;
            TraineeDto trainedto = new TraineeDto();
            trainedto = Mapper.Map<TraineeDto>(trainee);
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            if(trainee.UrlOfPhoto!=null)
              ImagePath = "http://" + url + "/UploadedFiles/" + trainee.TraineeNo + "/" + System.IO.Path.GetFileName(trainee.UrlOfPhoto);
            if (trainee.UrlOfSignature != null)
                Signature = "http://" + url + "/UploadedFiles/" + trainee.TraineeNo + "/" + System.IO.Path.GetFileName(trainee.UrlOfSignature);
            //trainedto.UrlOfPhotoName = trainedto.VirtualUrlOfPhoto != null ? Path.GetFileName(trainedto.VirtualUrlOfPhoto) : "";
            //trainedto.UrlOfSignatureName = trainedto.UrlOfSignature != null ? Path.GetFileName(trainedto.UrlOfSignature) : "";
            trainedto.VirtualUrlOfPhoto = ImagePath;
            trainedto.VirtualUrlOfSignature = Signature;
            trainedto.FromYear = (trainedto.DateOfAdmission != null ? DateTime.Parse(trainedto.DateOfAdmission.ToString()).Year : DateTime.Today.Year);
            return trainedto;
        }

        public ResponseDto SaveImages(TraineeFilesDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Trainee();
            string path = "";
            #region edit
            if (dto.Id > 0)
            {

            }
            #endregion

            #region Add
            else
            {
                try
                {
                    string memberNo = "";
                    entity = GenService.GetById<Trainee>(dto.TraineeId);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    //entity.Status = EntityStatus.Active;

                    if (entity != null)
                    {
                        memberNo = entity.TraineeNo;
                    }

                    if (dto.PhotoPathName != null)
                    {
                        path = "~/UploadedFiles/";
                        path = path + "/" + memberNo + "/";
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "Photo" + "_" + dto.PhotoPathName.FileName;
                        path = Path.Combine(path, fileName);
                        dto.PhotoPathName.SaveAs(path);
                        entity.UrlOfPhoto = path;
                    }
                    if (dto.SignaurePathName != null)
                    {
                        path = "~/UploadedFiles/";
                        path = path + "/" + memberNo + "/";
                        path = HttpContext.Current.Server.MapPath(path);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        var fileName = "Signature" + "_" + dto.SignaurePathName.FileName;
                        path = Path.Combine(path, fileName);
                        dto.SignaurePathName.SaveAs(path);
                        entity.UrlOfSignature = path;
                    }
                    response.Message = "File Upload Successfully";
                    GenService.Save(entity);
                    GenService.SaveChanges();
                }
                catch (Exception ex)
                {
                    response.Message = "File Upload not done";
                }

            }
            #endregion



            return response;
        }

        public IPagedList<TraineeDto> PendingTraineeApplicationList( int pageSize, int pageCount, string searchString, Course? course)
        {
            //DateTime? fromDate, DateTime? toDate,
            var members = GenService.GetAll<Trainee>().Where(m => m.Status == EntityStatus.Inactive);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                members = members.Where(m => m.Name.ToLower().Contains(searchString));
            }

            //if (fromDate != null && fromDate > DateTime.MinValue)
            //    members = members.Where(m => m.DateOfAdmission >= fromDate);
            //if (toDate != null && toDate < DateTime.MaxValue)
            //    members = members.Where(m => m.DateOfAdmission <= toDate);
            var membersList = from member in members
                              select new TraineeDto
                              {
                                  Id = member.Id,
                                  Name = member.Name,
                                  DateOfAdmission = member.DateOfAdmission,
                                  LastAcademicQualification = member.LastAcademicQualification
                              };
            if (course != null)
            {
                var courses = GenService.GetAll<TraineeCourses>().Where(x => x.Id == (int)course).ToList();
                var membersListCourse = from c in courses
                                        select new TraineeDto
                                        {
                                            Id = c.Trainee.Id,
                                            Name = c.Trainee.Name,
                                            DateOfAdmission = c.Trainee.DateOfAdmission,
                                            LastAcademicQualification = c.Trainee.LastAcademicQualification
                                        };
                return membersListCourse.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            }
            else
                return membersList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public IPagedList<TraineeDto> ActiveStudentList( int pageSize, int pageCount, string searchString, string batch, Course? course)
        {
            //DateTime? fromDate, DateTime? toDate,
            var pendingfees = GenService.GetAll<TraineeCourses>();
            if (!string.IsNullOrEmpty(searchString))
                {
                    searchString = searchString.ToLower();
                    pendingfees = pendingfees.Where(m => m.Trainee.Name.ToLower().Contains(searchString));
                }
            if (!string.IsNullOrEmpty(batch))
            {
                batch = batch.ToLower();
                pendingfees = pendingfees.Where(m => m.Trainee.Batch.ToLower().Contains(batch));
            }
            if (course!=null)
            {
                pendingfees = pendingfees.Where(m => m.Trainee.Courses.Any(x=>x.Course==(long)course));
            }
            //if (fromDate != null && fromDate > DateTime.MinValue)
            //    pendingfees = pendingfees.Where(m => m.Trainee.DateOfAdmission >= fromDate);
            //if (toDate != null && toDate < DateTime.MaxValue)
            //    pendingfees = pendingfees.Where(m => m.Trainee.DateOfAdmission <= toDate);
            var membersList = from member in pendingfees
                              group member by new { member.Trainee}
                              into fee where fee.Key.Trainee.Status== EntityStatus.Active
                              select new TraineeDto
                              {
                                  Id = fee.Key.Trainee.Id,
                                  TraineeNo = fee.Key.Trainee.TraineeNo,
                                  Name = fee.Key.Trainee.Name,
                                  DateOfAdmission = fee.Key.Trainee.DateOfAdmission,
                                  Batch = fee.Key.Trainee.Batch,
                                  IsFeePending = (fee.Where(x=>x.Status == EntityStatus.Inactive).Count() >0 ) ? true : false
                              };


            //if (course != null)
            //{
            //    var courses = GenService.GetAll<TraineeCourses>().Where(x => x.Id == (int)course).ToList();
            //    var membersListCourse = from c in courses
            //                            group c by new { c.Trainee }
            //                            into fee
            //                            select new TraineeDto
            //                            {
            //                                Id = fee.Key.Trainee.Id,
            //                                TraineeNo = fee.Key.Trainee.TraineeNo,
            //                                Name = fee.Key.Trainee.Name,
            //                                DateOfAdmission = fee.Key.Trainee.DateOfAdmission,
            //                                Batch = fee.Key.Trainee.Batch,
            //                                IsFeePending = (fee.Where(x => x.Status == EntityStatus.Inactive).Count() > 0) ? true : false
            //                            };
            //    return membersListCourse.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);
            //}
            //else
             return membersList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        #region Certificte Issue
        public List<CertificateRegisterDetailDto> GetCertificteListForAutoFill(string prefix, List<long> exclusionList, long bookid)
        {
            List<CertificateRegisterDetail> lists = new List<CertificateRegisterDetail>();
            if (exclusionList == null)
                exclusionList = new List<long>();
            prefix = prefix.ToLower();
                    
           lists = GenService.GetAll<CertificateRegisterDetail>().Where(x => x.Status == EntityStatus.Active && (x.CertificateNo.ToLower().Contains(prefix)) && !exclusionList.Contains(x.Id) && x.CertificateStatus == CertificateStatus.UnUsed && x.CertificateRegisterId == bookid).ToList();
           
            var data = lists.OrderBy(x => x.CertificateNo)
                     .Select(x => new CertificateRegisterDetailDto { Id = x.Id, CertificateNo = x.CertificateNo })
                     .ToList();
            return data;
        }

        public List<CertificateRegisterDto> GetBooks(CertificateType? ctypes)
        {
            var books = GenService.GetAll<CertificateRegister>().Where(s => s.CertificateType == ctypes).ToList();
            return Mapper.Map<List<CertificateRegisterDto>>(books);
        }

        public List<TraineeCoursesDto> GetTraineeCourses(long trid)
        {
            var courses = GenService.GetAll<TraineeCourses>().Where(s => s.TraineeId == trid && s.Status==EntityStatus.Active).ToList();
            return Mapper.Map<List<TraineeCoursesDto>>(courses);
        }

        public ResponseDto SaveCertificate(CertificateRegisterDetailDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            var entity = new CertificateRegisterDetail();
            //var traineeinfo = GenService.GetById<Trainee>((long)dto.TraineeNo);
            //traineeinfo.Result = dto.Result;
            //traineeinfo.EditDate = DateTime.Now;
            //traineeinfo.EditedBy = userId;
            entity = GenService.GetById<CertificateRegisterDetail>(dto.Id);
            #region Edit
            try
            {
                dto.CreateDate = entity.CreateDate;
                dto.CreatedBy = entity.CreatedBy;
                Mapper.Map(dto, entity);
                entity.CertificateStatus = CertificateStatus.Issued;
                entity.TransactionDate = DateTime.Now;
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                GenService.Save(entity);
                //GenService.Save(traineeinfo);
                //GenService.SaveChanges();
                response.Message = "Certificate Saved Successfully";
            }
            catch (Exception ex)
            {
                response.Message = "Certificate not Saved.";
            }
            #endregion

            

            return response;
        }

        public List<CertificateRegisterDetailDto> GetMembersCirtificates(long traineeid)
        {
            var books = GenService.GetAll<CertificateRegisterDetail>().Where(s => s.TraineeNo == traineeid).ToList();
            var result = from cer in books
                         select new CertificateRegisterDetailDto
                         {
                             CertificateNo = cer.CertificateNo,
                             CertificateRegisterId = cer.CertificateRegisterId,
                             CertificateStatus = cer.CertificateStatus,
                             CertificateStatusName = cer.CertificateStatus.ToString(),
                             Id = cer.Id,
                             IssueDate = cer.IssueDate,
                             ExpiryDate = cer.ExpiryDate,
                             CertificateType = cer.CertificateRegister.CertificateType,
                             CertificateTypeName = cer.CertificateRegister.CertificateType.ToString(),
                             TraineeNo = cer.TraineeNo,
                             TransactionDate = cer.TransactionDate
                         };
            return result.ToList();
        }

        public CertificateRegisterDetailDto GetCertificate(long cid)
        {
            var certificate = GenService.GetAll<CertificateRegisterDetail>().Where(x => x.Id == cid).FirstOrDefault();
            return Mapper.Map<CertificateRegisterDetailDto>(certificate);
        }

        public List<CertificateRegisterDetailDto> GetTraineeCertificateforPrint(long cid)
        {
            //CertificateRegisterDetail certificate = new CertificateRegisterDetail();
            var certificate = GenService.GetAll<CertificateRegisterDetail>().Where(x => x.Id == cid).ToList();
            var result = (from cer in certificate
                          select new CertificateRegisterDetailDto
                          {
                              CertificateNo = cer.CertificateNo,
                              CertificateRegisterId = cer.CertificateRegisterId,
                              CertificateStatus = cer.CertificateStatus,
                              Id = cer.Id,
                              IssueDate = cer.IssueDate,
                              ExpiryDate = cer.ExpiryDate,
                              CertificateType = cer.CertificateRegister.CertificateType,
                              CertificateTypeName = cer.CertificateRegister.CertificateType.ToString(),
                              TraineeNo = cer.TraineeNo,
                              TransactionDate = cer.TransactionDate,
                              StudentName = cer.Trainee.Name,
                              FatherName = cer.Trainee.FathersName
                          }).ToList();
            return result;
        }
        #endregion
    }
}
