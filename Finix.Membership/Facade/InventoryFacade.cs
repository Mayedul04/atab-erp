﻿using AutoMapper;
using Finix.Membership.Infrastructure;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Finix.Membership.Facade
{
    public class InventoryFacade : BaseFacade
    {
        private readonly SequencerFacade _seqFacade = new SequencerFacade();
        private readonly Auth.Facade.CompanyProfileFacade _office = new Auth.Facade.CompanyProfileFacade();

        public ResponseDto SaveReceive(StockTranDto stocktranDto, long officeId, long userId)
        {
            var response = new ResponseDto();
            var spList = new List<StockPosition>();
            StockTran entity;
            if(stocktranDto.Id > 0)
            {
                entity = GenService.GetById<StockTran>((long)stocktranDto.Id);
            }
            else
            {
                stocktranDto.StockTranType = StockTranType.Receive;
                entity = Mapper.Map<StockTran>(stocktranDto);
                entity.FromOfficeId = 0;
                entity.ToOfficeId = officeId;
                entity.TranDate = _office.DateToday(officeId);
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.Status = EntityStatus.Active;
                stocktranDto.Details.ForEach(d => { d.CreateDate = DateTime.Now; d.CreatedBy = userId; d.Status = EntityStatus.Active; });
                entity.Details = Mapper.Map<List<StockTranDetail>>(stocktranDto.Details);
                
                foreach(var item in stocktranDto.Details)
                {
                    var sp = spList.Where(s => s.OfficeId == officeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active).FirstOrDefault();
                    if (sp == null)
                        sp = GenService.GetAll<StockPosition>().Where(s => s.OfficeId == officeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active).FirstOrDefault();
                    else
                        spList.Remove(sp);
                    if (sp == null)
                    {
                        sp = new StockPosition();
                        sp.OfficeId = officeId;
                        sp.ProductId = item.ProductId;
                        sp.Status = EntityStatus.Active;
                        sp.CreateDate = DateTime.Now;
                        sp.CreatedBy = userId;
                    }
                    sp.Balance += item.Quantity;
                    sp.EditDate = DateTime.Now;
                    sp.EditedBy = userId;
                    spList.Add(sp);
                }

                using(var tran = new TransactionScope())
                {
                    try
                    {
                        GenService.Save(entity);
                        GenService.Save(spList);
                        GenService.SaveChanges();
                        tran.Complete();
                        response.Message = "Products have been received.";
                        response.Success = true;
                        response.Id = entity.Id;
                    }
                    catch (Exception)
                    {
                        tran.Dispose();
                        response.Message = "Product receive failed.";
                    }
                }

                if ((bool)response.Success)
                {
                    var challanNo = _seqFacade.GetUpdatedChallanNo(officeId);
                    entity.ChallanNo = challanNo;
                    response.Message += " The challan number is " + challanNo;
                    GenService.Save(entity);
                    GenService.SaveChanges();
                }
            }
            return response;
        }
        //public ResponseDto SaveTransfer(StockTranDto stocktranDto, long officeId, long userId)
        //{
        //    var response = new ResponseDto();
        //    var spList = new List<StockPosition>();
        //    StockTran entity;
        //    if (stocktranDto.Id > 0)
        //    {
        //        entity = GenService.GetById<StockTran>((long)stocktranDto.Id);
        //    }
        //    else
        //    {
        //        //stocktranDto.StockTranType = StockTranType.Receive;
        //        entity = Mapper.Map<StockTran>(stocktranDto);
        //        entity.TranDate = _office.DateToday(officeId);
        //        entity.CreateDate = DateTime.Now;
        //        entity.CreatedBy = userId;
        //        entity.Status = EntityStatus.Active;
        //        stocktranDto.Details.ForEach(d => { d.CreateDate = DateTime.Now; d.CreatedBy = userId; d.Status = EntityStatus.Active; });
        //        entity.Details = Mapper.Map<List<StockTranDetail>>(stocktranDto.Details);

        //        foreach (var item in stocktranDto.Details)
        //        {
        //            var sp = spList.Where(s => s.OfficeId == officeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active).FirstOrDefault();
        //            if (sp == null)
        //                sp = GenService.GetAll<StockPosition>().Where(s => s.OfficeId == officeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active).FirstOrDefault();
        //            else
        //                spList.Remove(sp);
        //            if (sp == null)
        //            {
        //                sp = new StockPosition();
        //                sp.OfficeId = officeId;
        //                sp.ProductId = item.ProductId;
        //                sp.Status = EntityStatus.Active;
        //                sp.CreateDate = DateTime.Now;
        //                sp.CreatedBy = userId;
        //            }
        //            sp.Balance += item.Quantity;
        //            sp.EditDate = DateTime.Now;
        //            sp.EditedBy = userId;
        //            spList.Add(sp);
        //        }

        //        using (var tran = new TransactionScope())
        //        {
        //            try
        //            {
        //                GenService.Save(entity);
        //                GenService.Save(spList);
        //                GenService.SaveChanges();
        //                tran.Complete();
        //                response.Message = "Products have been received.";
        //                response.Success = true;
        //                response.Id = entity.Id;
        //            }
        //            catch (Exception)
        //            {
        //                tran.Dispose();
        //                response.Message = "Product receive failed.";
        //            }
        //        }

        //        if ((bool)response.Success)
        //        {
        //            var challanNo = _seqFacade.GetUpdatedChallanNoNo(officeId);
        //            entity.ChallanNo = challanNo;
        //            response.Message += " The challan number is " + challanNo;
        //            GenService.Save(entity);
        //            GenService.SaveChanges();
        //        }
        //    }


        //    return response;
        //}
        public ResponseDto SaveDisburse(StockTranDto stocktranDto, long userId)
        {
            var response = new ResponseDto();
            var spList = new List<StockPosition>();
            StockTran stockTran;
            if (stocktranDto.Id > 0)
            {
                stockTran = GenService.GetById<StockTran>((long)stocktranDto.Id);
            }
            else
            {
                stocktranDto.StockTranType = StockTranType.Disburse;
                stockTran = Mapper.Map<StockTran>(stocktranDto);
                stockTran.ToOfficeId = 0;
                stockTran.TranDate = _office.DateToday(stocktranDto.FromOfficeId);
                stockTran.CreateDate = DateTime.Now;
                stockTran.CreatedBy = userId;
                stockTran.Status = EntityStatus.Active;
                stocktranDto.Details.ForEach(d => { d.CreateDate = DateTime.Now; d.CreatedBy = userId; d.Status = EntityStatus.Active; });
                stockTran.Details = Mapper.Map<List<StockTranDetail>>(stocktranDto.Details);

                foreach (var item in stocktranDto.Details)
                {
                    var sp = spList.Where(s => s.OfficeId == stocktranDto.FromOfficeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active).FirstOrDefault();
                    if (sp == null)
                        sp = GenService.GetAll<StockPosition>().Where(s => s.OfficeId == stocktranDto.FromOfficeId && s.ProductId == item.ProductId && s.Status == EntityStatus.Active && s.Balance >= item.Quantity).FirstOrDefault();
                    else
                        spList.Remove(sp);
                    if (sp == null)
                    {
                        var product = GenService.GetById<Product>(item.ProductId);
                        if (product != null)
                            response.Message = "Not enough stock available for " + product.Name;
                        else
                            response.Message = "Product not found";
                        return response;
                    }
                    sp.Balance -= item.Quantity;
                    sp.EditDate = DateTime.Now;
                    sp.EditedBy = userId;
                    spList.Add(sp);
                }

                using (var tran = new TransactionScope())
                {
                    try
                    {
                        GenService.Save(stockTran);
                        GenService.Save(spList);
                        GenService.SaveChanges();
                        tran.Complete();
                        response.Message = "Products have been received.";
                        response.Success = true;
                        response.Id = stockTran.Id;
                    }
                    catch (Exception)
                    {
                        tran.Dispose();
                        response.Message = "Product receive failed.";
                    }
                }

                if ((bool)response.Success)
                {
                    var challanNo = _seqFacade.GetUpdatedInvoiceNo(stockTran.FromOfficeId);
                    stockTran.ChallanNo = challanNo;
                    response.Message += " The invoice number is " + challanNo;
                    GenService.Save(stockTran);
                    GenService.SaveChanges();
                }
            }
            return response;
        }
        public List<ProductDto> GetProducts()
        {
            var products = GenService.GetAll<Product>().Where(p => p.Status == EntityStatus.Active);
            return Mapper.Map<List<ProductDto>>(products);
        }
        public object GetStockedProducts(long officeId)
        {
            var result = from stock in GenService.GetAll<StockPosition>().Where(s => s.Status == EntityStatus.Active && s.Balance > 0 && s.OfficeId == officeId)
                         select new
                         {
                             Id = stock.ProductId,
                             Name = stock.Product.Name,
                             Balance = stock.Balance
                         };

            return result;
        }

        public List<StockTranDetailDto> GetStockLedger(long? productId, long? officeId, DateTime fromDate, DateTime toDate)
        {
            var maxDate = toDate.AddDays(1).Date;
            var productDetails = GenService.GetAll<StockTranDetail>().Where(c => c.Status == EntityStatus.Active);
            if(officeId > 0)
                productDetails = productDetails.Where(c => c.StockTran.FromOfficeId == officeId || c.StockTran.ToOfficeId == officeId);
            if (productId > 0)
                productDetails = productDetails.Where(c => c.ProductId == productId);

            var stockTranDetails = (from std in productDetails.Where(c => c.StockTran.TranDate > fromDate && c.StockTran.TranDate < maxDate)
                                    group std by std.Product into groupped
                                    //from g in groupped.DefaultIfEmpty()
                                    select new StockTranDetailDto()
                                    {
                                        ProductName = groupped.Key.Id > 0 ? groupped.Key.Name : "",
                                        OpeningQuantity = (productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Receive) &&
                                                                                    p.StockTran.TranDate < fromDate &&
                                                                                    p.ProductId == groupped.Key.Id).Any() ?
                                                                            productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Receive) &&
                                                                                                                        p.StockTran.TranDate < fromDate &&
                                                                                                                        p.ProductId == groupped.Key.Id).Sum(p => p.Quantity) : 0)
                                                          - (productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Damage
                                                                                     || p.StockTran.StockTranType == StockTranType.Disburse
                                                                                     || p.StockTran.StockTranType == StockTranType.Wastage
                                                                                     || p.StockTran.StockTranType == StockTranType.Return) &&
                                                                                    p.StockTran.TranDate < fromDate &&
                                                                                    p.ProductId == groupped.Key.Id).Any() ?
                                                          productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Damage
                                                                                     || p.StockTran.StockTranType == StockTranType.Disburse
                                                                                     || p.StockTran.StockTranType == StockTranType.Wastage
                                                                                     || p.StockTran.StockTranType == StockTranType.Return) &&
                                                                                    p.StockTran.TranDate < fromDate &&
                                                                                    p.ProductId == groupped.Key.Id).Sum(p => p.Quantity) : 0),
                                        ReceivedQuantity = productDetails.Where(p=> p.StockTran.StockTranType == StockTranType.Receive &&
                                                                                    p.ProductId == groupped.Key.Id &&
                                                                                    p.StockTran.TranDate >= fromDate && 
                                                                                    p.StockTran.TranDate <= maxDate).Sum(p=>p.Quantity),
                                        IssuedQuantity = productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Damage ||
                                                                                   p.StockTran.StockTranType == StockTranType.Wastage ||
                                                                                   p.StockTran.StockTranType == StockTranType.Disburse) &&
                                                                                   p.ProductId == groupped.Key.Id &&
                                                                                   p.StockTran.TranDate >= fromDate &&
                                                                                   p.StockTran.TranDate <= maxDate).Sum(p => p.Quantity),
                                        ClosingQuantity = (productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Receive) &&
                                                                                    p.StockTran.TranDate <= maxDate &&
                                                                                    p.ProductId == groupped.Key.Id).Any() ?
                                        productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Receive) &&
                                                                                    p.StockTran.TranDate <= maxDate &&
                                                                                    p.ProductId == groupped.Key.Id).Sum(p => p.Quantity) : 0)
                                                          - (productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Damage ||
                                                                                       p.StockTran.StockTranType == StockTranType.Disburse ||
                                                                                       p.StockTran.StockTranType == StockTranType.Wastage ||
                                                                                       p.StockTran.StockTranType == StockTranType.Return) &&
                                                                                    p.StockTran.TranDate <= maxDate &&
                                                                                    p.ProductId == groupped.Key.Id).Any() ?
                                                          productDetails.Where(p => (p.StockTran.StockTranType == StockTranType.Damage || 
                                                                                       p.StockTran.StockTranType == StockTranType.Disburse || 
                                                                                       p.StockTran.StockTranType == StockTranType.Wastage || 
                                                                                       p.StockTran.StockTranType == StockTranType.Return) &&
                                                                                    p.StockTran.TranDate <= maxDate &&
                                                                                    p.ProductId == groupped.Key.Id).Sum(p => p.Quantity) : 0)
                                    }).ToList();
            
            return stockTranDetails;
        }
    }
}
