﻿using AutoMapper;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Service;
using Finix.Membership.Util;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Facade
{
   public class MembeshipAccountFacade :BaseFacade
    {
        private readonly EnumFacade _enum = new EnumFacade();
        public IPagedList<AccHeadMappingDto> GetAccHeadMappingList(int pageSize, int pageCount, string searchString)
        {
            var categories = GenService.GetAll<FeeType>();
            
            var allAccHeads = (from accMap in GenService.GetAll<AccountsMapping>().Where(a => a.Status == EntityStatus.Active && (a.RefType == AccountHeadRefType.Income || a.RefType == AccountHeadRefType.Receivable || a.RefType == AccountHeadRefType.Liability))
                               join cat in categories
                               on accMap.RefId equals cat.Id
                               select new AccHeadMappingDto()
                               {
                                   Id = accMap.Id,
                                   AccountHeadCode = accMap.AccountHeadCode,
                                   RefId = accMap.RefId,
                                   RefType = accMap.RefType,
                                   RefTypeName = accMap.RefType.ToString(),
                                   RefName = cat.Name
                               }).ToList();
            var others = (from accMap in GenService.GetAll<AccountsMapping>().Where(a => a.Status == EntityStatus.Active && (a.RefType == AccountHeadRefType.Income_ATTI || a.RefType == AccountHeadRefType.Receivable_ATTI))
                          select new AccHeadMappingDto()
                          {
                              Id = accMap.Id,
                              AccountHeadCode = accMap.AccountHeadCode,
                              RefId = accMap.RefId,
                              RefType = accMap.RefType,
                              RefTypeName = accMap.RefType.ToString(),
                              RefName = ((Course)accMap.RefId).ToString()
                          }).ToList();
            allAccHeads.AddRange(others);
            var cashbank = (from accMap in GenService.GetAll<AccountsMapping>().Where(a => a.Status == EntityStatus.Active && (a.RefType == AccountHeadRefType.CashInHand || a.RefType == AccountHeadRefType.CashAtBank))
                          select new AccHeadMappingDto()
                          {
                              Id = accMap.Id,
                              AccountHeadCode = accMap.AccountHeadCode,
                              RefId = accMap.RefId,
                              RefType = accMap.RefType,
                              RefTypeName = accMap.RefType.ToString(),
                              RefName = ((Course)accMap.RefId).ToString()
                          }).ToList();
            allAccHeads.AddRange(cashbank);
            var temp = allAccHeads.OrderBy(r => r.Id).ToList().ToPagedList(pageCount, pageSize);
            return temp;
        }

        public object GetAccHeads(AccountHeadRefType refType, long officeId)
        {
            if (refType == AccountHeadRefType.Income || refType == AccountHeadRefType.Income_ATTI || refType == AccountHeadRefType.Receivable || refType == AccountHeadRefType.CashAtBank || refType == AccountHeadRefType.CashInHand || refType == AccountHeadRefType.Liability || refType == AccountHeadRefType.Receivable_ATTI)
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                    return new { Code = code, Name = refType.ToString() };
            }
            return null;
        }
        public ResponseDto SaveAccHeadMapping(AccHeadMappingDto dto, long userId)
        {
            var response = new ResponseDto();
            var entity = new AccountsMapping();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<AccountsMapping>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    response.Success = true;
                    response.Message = "Account head mapping Update successful";
                }
                else
                {
                    entity = AutoMapper.Mapper.Map<AccountsMapping>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    response.Success = true;
                    response.Message = "Account head mapping Entry successful";
                }
               
            }
            catch (Exception)
            {
                response.Message = "Mapping not saved, please contact administrator.";
            }
            return response;
        }
        public string GetAccHeadCodeForVoucher(AccountHeadRefType refType, long? refId)
        {
            var data =
                GenService.GetAll<AccountsMapping>()
                    .Where(a => a.RefId == refId && a.RefType == refType && a.Status == EntityStatus.Active)
                    .FirstOrDefault();

            if (data != null)
                return data.AccountHeadCode;
            else
                return "";
        }

        public string GetAccHeadCodeForVoucherZone(AccountHeadRefType refType, long? refId,Zone? zone)
        {
            var data =
                GenService.GetAll<AccountsMapping>()
                    .Where(a => a.RefId == refId && a.RefType == refType && a.zone==zone && a.Status == EntityStatus.Active)
                    .FirstOrDefault();

            if (data != null)
                return data.AccountHeadCode;
            else
                return "";
        }


        public List<FeeTypeDto> GetMemberFees()
        {
            
            var data = GenService.GetAll<FeeType>().Where(x => x.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<FeeTypeDto>>(data);
        }

        //public EnumDto GetCourseFee(int id)
        //{
        //    var data = Course.
        //    return Mapper.Map<List<FeeTypeDto>>(data);
        //}
    }
}
