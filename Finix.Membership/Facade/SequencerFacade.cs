﻿using Finix.Membership.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Facade
{

    public class SequencerFacade : BaseFacade
    {
        public string GetUpdatedTraineeNo(long officeId)
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().Where(i=> i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (string.IsNullOrEmpty(sequence.TraineeNo))
                    sequence.TraineeNo = "00000000";
                var slNo = (Convert.ToDecimal(sequence.TraineeNo.Trim()) + 1).ToString("00000000");
                sequence.TraineeNo = slNo;
                GenService.Save(sequence);
                return "STU-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public string GetUpdatedMemberNo()
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().First();
                if (sequence == null)
                {
                    IndexInitializer(1);
                    sequence = GenService.GetAll<IndexSequencer>().First();
                }
                if (string.IsNullOrEmpty(sequence.MemberNo))
                    sequence.MemberNo = "00000000";
                var slNo = (Convert.ToDecimal(sequence.MemberNo.Trim()) + 1).ToString("00000000");
                sequence.MemberNo = slNo;
                GenService.Save(sequence);
                return "MN-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public string GetUpdatedMRNo(long officeId)
        {
            try
            {
                string slNo = "";
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (sequence.MoneyReceiptNo == null)
                {
                    slNo = "00000001";
                }
                else
                {
                    slNo = (Convert.ToDecimal(sequence.MoneyReceiptNo.Trim()) + 1).ToString("00000000");
                }
                sequence.MoneyReceiptNo = slNo;
                GenService.Save(sequence);
                return "CR-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }

        public string GetUpdatedBankMRNo(long officeId)
        {
            try
            {
                string slNo = "";
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (sequence.BankMoneyReceiptNo == null)
                {
                    slNo = "00000001";
                }
                else
                {
                    slNo = (Convert.ToDecimal(sequence.BankMoneyReceiptNo.Trim()) + 1).ToString("00000000");
                }
                sequence.BankMoneyReceiptNo = slNo;
                GenService.Save(sequence);
                return "BR-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public string GetUpdatedAdvanceSlipNo(long officeId)
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (string.IsNullOrEmpty(sequence.AdvanceSlipNo))
                    sequence.AdvanceSlipNo = "00000000";
                var slNo = (Convert.ToDecimal(sequence.AdvanceSlipNo.Trim()) + 1).ToString("00000000");
                sequence.AdvanceSlipNo = slNo;
                GenService.Save(sequence);
                return "ADV-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public string GetUpdatedChallanNo(long officeId)
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (string.IsNullOrEmpty(sequence.ChallanNo))
                    sequence.ChallanNo = "00000000";
                var slNo = (Convert.ToDecimal(sequence.ChallanNo.Trim()) + 1).ToString("00000000");
                sequence.ChallanNo = slNo;
                GenService.Save(sequence);
                return "CLN-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public string GetUpdatedInvoiceNo(long officeId)
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    IndexInitializer(officeId);
                    sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).First();
                }
                if (string.IsNullOrEmpty(sequence.InvoiceNo))
                    sequence.InvoiceNo = "00000000";
                var slNo = (Convert.ToDecimal(sequence.InvoiceNo.Trim()) + 1).ToString("00000000");
                sequence.InvoiceNo = slNo;
                GenService.Save(sequence);
                return "INV-" + slNo;
            }
            catch (Exception ex)
            {
                //throw;
            }
            return null;
        }
        public bool IndexInitializer(long officeId)
        {
            try
            {
                var sequence = GenService.GetAll<IndexSequencer>().Where(i => i.OfficeId == officeId).FirstOrDefault();
                if (sequence == null)
                {
                    sequence = new IndexSequencer();
                    sequence.OfficeId = officeId;
                    sequence.TraineeNo = "00000000";
                    sequence.MemberNo = "00000000";
                    sequence.MoneyReceiptNo = "00000000";
                    sequence.AdvanceSlipNo = "00000000";
                    sequence.ChallanNo = "00000000";
                    sequence.CreateDate = DateTime.Now;
                    GenService.Save(sequence);
                    GenService.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
    }
}
