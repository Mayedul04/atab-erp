﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Util;
namespace Finix.Membership.Facade
{
    public class EnumFacade : BaseFacade
    {
        public List<EnumDto> GetDepreciationType()
        {
            var typeList = Enum.GetValues(typeof(DepriciationType))
               .Cast<DepriciationType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetDepreciationModel()
        {
            var typeList = Enum.GetValues(typeof(DepriciationModel))
               .Cast<DepriciationModel>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetSuppliers()
        {
            var typeList = Enum.GetValues(typeof(DemoSupplier))
               .Cast<DemoSupplier>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetEmployees()
        {
            var typeList = Enum.GetValues(typeof(DemoEmployee))
               .Cast<DemoEmployee>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetServiceType()
        {
            var typeList = Enum.GetValues(typeof(ServiceType))
               .Cast<ServiceType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetPointOfDepreciation()
        {
            var typeList = Enum.GetValues(typeof(PointOfDepreciation))
               .Cast<PointOfDepreciation>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetReportGroupby()
        {
            var typeList = Enum.GetValues(typeof(ReportGroupBy))
               .Cast<ReportGroupBy>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetTransferType()
        {
            var typeList = Enum.GetValues(typeof(TransferType))
               .Cast<TransferType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetLocationLevels()
        {
            var typeList = Enum.GetValues(typeof(LocationTier))
               .Cast<LocationTier>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetCategoryLevels()
        {
            var typeList = Enum.GetValues(typeof(CategoryLevel))
               .Cast<CategoryLevel>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetOwnershipStatus()
        {
            var typeList = Enum.GetValues(typeof(OwnershipStatus))
               .Cast<OwnershipStatus>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetZoneList()
        {
            var typeList = Enum.GetValues(typeof(Zone))
               .Cast<Zone>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetCourseList()
        {
            var typeList = Enum.GetValues(typeof(Course))
               .Cast<Course>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetPayTypeList()
        {
            var typeList = Enum.GetValues(typeof(PayType))
               .Cast<PayType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetGenderList()
        {
            var typeList = Enum.GetValues(typeof(Gender))
               .Cast<Gender>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetReligionList()
        {
            var typeList = Enum.GetValues(typeof(Religion))
               .Cast<Religion>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetSourceOfInformationList()
        {
            var typeList = Enum.GetValues(typeof(SourceOfInformation))
               .Cast<SourceOfInformation>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetPosts()
        {
            var typeList = Enum.GetValues(typeof(Post))
               .Cast<Post>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetCommitteeTypes()
        {
            var typeList = Enum.GetValues(typeof(CommitteeType))
              .Cast<CommitteeType>()
              .Select(t => new EnumDto
              {
                  Id = ((int)t),
                  Name = UiUtil.GetDisplayName(t)
              });
            return typeList.ToList();
        }

        public List<EnumDto> GetCertificateTypes(long? companyid,long? courseid)
        {
            var typeList = Enum.GetValues(typeof(CertificateType))
               .Cast<CertificateType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t),
               });
            if (companyid != null)
            {
                if(companyid==1)
                    typeList = typeList.Where(x => x.Id < 3);
                else
                    typeList = typeList.Where(x => x.Id > 2);
            }

            if (courseid != null)
            {
                typeList = typeList.Where(x => x.Id == (courseid+2));   //Differene between CourseId and Cetificate Type is 2
            }

            return typeList.ToList();
        }
    }
}
