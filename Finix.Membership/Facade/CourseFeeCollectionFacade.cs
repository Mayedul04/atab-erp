﻿using AutoMapper;
using Finix.Accounts.DTO;
using Finix.Auth.Facade;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Service;
using Finix.Membership.Util;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Finix.Membership.Facade
{
  public  class CourseFeeCollectionFacade :BaseFacade
    {
        private readonly MembeshipAccountFacade _accounts = new MembeshipAccountFacade();
        private readonly SequencerFacade _seqFacade = new SequencerFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        public TraineePendingFeeCollectionDto GetPendingCourseFeeTraineeWise(long traineeId)
        {
            var result = new TraineePendingFeeCollectionDto(); 
           // var data= new List<CourseFeeDetailsTraineeWiseDto>();
            var traineeCourses = GenService.GetAll<TraineeCourses>().Where(r => r.TraineeId == traineeId && r.Status == EntityStatus.Inactive);
            var pendingfeeIds = traineeCourses.Select(r => r.Id);
            var collectionTran = GenService.GetAll<CourseFeeCollectionTran>().Where(r => r.TraineeId == traineeId && pendingfeeIds.Contains((long)r.PendingFeeId));
            //var totalAmount = collectionTran.Any() ? collectionTran.Sum(r => r.CollectedAmount) : 0;
            var memberDetails = GenService.GetById<Trainee>(traineeId);
            result.TraineeId = traineeId;
            result.TraineeNo = memberDetails.TraineeNo;
            result.Name = memberDetails.Name;
            result.Phone = memberDetails.Address.CellPhoneNo;
            var year = (memberDetails.DateOfAdmission != null ? DateTime.Parse(memberDetails.DateOfAdmission.ToString()).Year : DateTime.Today.Year).ToString();

            var collection = from cat in collectionTran
                                 //join val in mee
                             group cat by new { cat.Courses, cat.Trainee, cat.PendingFee }
                             into fee
                             select new CourseFeeCollectionTranDto
                             {
                                 CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                 Course = fee.Key.Courses.Id,
                                 TraineeId = fee.Key.Trainee.Id,
                                 PendingFeeId = fee.Key.PendingFee.Id
                             };
            if (collection.Count() > 0)
            {
                var data = (from mbrRending in traineeCourses
                            join col in collection on mbrRending.Id equals col.PendingFeeId into extra
                            from ext in extra.DefaultIfEmpty()
                            select new CourseFeeDetailsTraineeWiseDto()
                            {
                                TraineeId = (long)mbrRending.TraineeId,
                                PendingId = (long)mbrRending.Id,
                                Course = mbrRending.Course,
                                CourseName = mbrRending.Courses.Name,
                                CourseFee = mbrRending.CourseFee,
                                ForYear = year,
                                PaidAmount =(ext.CollectedAmount!=null? ext.CollectedAmount:0) 
                                //PaidAmount = ext.Course != mbrRending.Course ? 0 : ext.CollectedAmount
                            }).ToList();
                result.PendingFeeList = (from dt in data
                                         group dt by new { dt.TraineeId, dt.PendingId, dt.Course, dt.CourseName } into grp
                                         let memberDetailsFeeWiseDto = grp.FirstOrDefault()
                                         where memberDetailsFeeWiseDto != null
                                         select new CourseFeeDetailsTraineeWiseDto()
                                         {
                                             PendingId = grp.Key.PendingId,
                                             TraineeId = grp.Key.TraineeId,
                                             Course = grp.Key.Course,
                                             CourseName = grp.Key.Course > 0 ? grp.Key.CourseName : "",
                                             CourseFee = memberDetailsFeeWiseDto.CourseFee,
                                             PaidAmount = grp.Sum(l => l.PaidAmount),
                                             ForYear = memberDetailsFeeWiseDto.ForYear,
                                             Due = memberDetailsFeeWiseDto.CourseFee - grp.Sum(l => l.PaidAmount),
                                             IsDue = (memberDetailsFeeWiseDto.CourseFee - grp.Sum(l => l.PaidAmount)) == 0 ? false : true

                                         }).ToList();
            }
            else
            {
                var data = (from mbrRending in traineeCourses
                            join col in collection on mbrRending.Id equals col.PendingFeeId into extra
                            from ext in extra.DefaultIfEmpty()
                            select new CourseFeeDetailsTraineeWiseDto()
                            {
                                TraineeId = (long)mbrRending.TraineeId,
                                PendingId = (long)mbrRending.Id,
                                Course = mbrRending.Course,
                                CourseName= mbrRending.Courses.Name,
                                CourseFee = mbrRending.CourseFee,
                                ForYear = year,
                                PaidAmount = 0
                                //(mbrRending.Status == 0 ? ext.CollectedAmount : 0)
                            }).ToList();
                result.PendingFeeList = (from dt in data
                                         group dt by new { dt.TraineeId, dt.PendingId, dt.Course,dt.CourseName } into grp
                                         let memberDetailsFeeWiseDto = grp.FirstOrDefault()
                                         where memberDetailsFeeWiseDto != null
                                         select new CourseFeeDetailsTraineeWiseDto()
                                         {
                                             PendingId = grp.Key.PendingId,
                                             TraineeId = grp.Key.TraineeId,
                                             Course = grp.Key.Course,
                                             CourseName = grp.Key.Course > 0 ? grp.Key.CourseName : "",
                                             CourseFee = memberDetailsFeeWiseDto.CourseFee,
                                             PaidAmount = grp.Sum(l => l.PaidAmount),
                                             ForYear = memberDetailsFeeWiseDto.ForYear,
                                             Due = memberDetailsFeeWiseDto.CourseFee - grp.Sum(l => l.PaidAmount),
                                             IsDue = (memberDetailsFeeWiseDto.CourseFee - grp.Sum(l => l.PaidAmount)) == 0 ? false : true

                                         }).ToList();
            }
            return result;
        }

        public List<EnumDto> GetPaymentTypes()
        {
            var data = Enum.GetValues(typeof(PayType))
                       .Cast<PayType>()
                       .Select(t => new EnumDto
                       {
                           Id = ((int)t),
                           Name = UiUtil.GetDisplayName(t)
                       });
            return data.ToList();
        } 
        //public List<EnumDto> GetCourses(List<int> ids)
        //{
        //    var data = Enum.GetValues(typeof(Course))
        //               .Cast<Course>()
        //               .Select(t => new EnumDto
        //               {
        //                   Id = ((int)t),
        //                   Name = UiUtil.GetDisplayName(t)
        //               });
        //    if (ids==null)
        //        return data.ToList();
        //    else
        //        return data.Where(x => ids.Contains((int)x.Id)).ToList();
        //}

        public List<FeeTypeDto> GetCourses(List<long> ids)
        { 
            var data = GenService.GetAll<FeeType>().Where(x=>x.CompanyProfileId==2) //ATTI Office=2
                .Where(s => s.Status == EntityStatus.Active)
                       .Select(t => new FeeTypeDto
                       {
                           Id = t.Id,
                           Name = t.Name
                       });
            if (ids == null)
                return data.ToList();
            else
                return data.Where(x => ids.Contains((int)x.Id)).ToList();
        }

        public ResponseDto SaveFeePayment(TraineeCourseFeeCollectionDto dto, long userId, long companyId)
        {
            ResponseDto response = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            var voucherdate = _companyProfileFacade.DateToday(companyId).Date;
            string voucherNumber;

            var total = dto.TotalPayableAmount;
            decimal totalreceivable = 0;
            decimal totalincome = 0;
            decimal totalchequeAmount = 0;
            decimal totalcashAmount = 0;
            long? traineeid;
            var data = new List<CourseFeeCollectionTran>();
            var dataMoneyReceipt = new TraineeMoneyReceipt();
            var pendingFees = new List<TraineeCourses>();
            var trainee = new Trainee();
            List<long> pendingFeeIds = new List<long>();
            pendingFeeIds = dto.PendingFeeList.Where(r => r.IsChecked == true).Select(l => l.PendingId).ToList();
            if (dto.FeeCollectionTrans != null && dto.FeeCollectionTrans.Count > 0)
            {
                dataMoneyReceipt.TraineeId = dto.FeeCollectionTrans.FirstOrDefault().TraineeId;
                traineeid = dataMoneyReceipt.TraineeId;
                dataMoneyReceipt.TotalCollectedAmount = (decimal)total;
                dataMoneyReceipt.Description = dto.Description;
                dataMoneyReceipt.AuthorizedPerson = dto.MRAuthorizedPerson;
                if (dto.PayType == PayType.Cash)
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedMRNo(1);
                else
                    dataMoneyReceipt.MoneyReceiptNo = _seqFacade.GetUpdatedBankMRNo(1);
                dataMoneyReceipt.MoneyReceiptDetails = new List<TraineeMoneyReceiptDetails>();
                trainee = GenService.GetById<Trainee>((long)dataMoneyReceipt.TraineeId);

                foreach (var item in dto.FeeCollectionTrans)
                {
                    var fee = Mapper.Map<CourseFeeCollectionTran>(item);
                    var feeinfo = GenService.GetById<FeeType>((long)fee.Course);
                    fee.PayType = dto.PayType;
                    fee.CreateDate = DateTime.Now;
                    fee.CreatedBy = userId;
                    fee.Status = EntityStatus.Active;
                    fee.SubscriptionDate = voucherdate;
                    fee.MoneyReceiptNo = dataMoneyReceipt.MoneyReceiptNo;
                    data.Add(fee);

                    var moneyReceiptDtl = new TraineeMoneyReceiptDetails();
                    moneyReceiptDtl.CollectedAmount = fee.CollectedAmount;
                    moneyReceiptDtl.PayType = fee.PayType;
                    moneyReceiptDtl.SubscriptionDate = fee.SubscriptionDate;
                    moneyReceiptDtl.Year = fee.ForYear;
                    moneyReceiptDtl.ChequeNo = fee.ChequeNo != null ? fee.ChequeNo : "";
                    moneyReceiptDtl.CreateDate = DateTime.Now;
                    moneyReceiptDtl.CreatedBy = userId;
                    moneyReceiptDtl.Status = EntityStatus.Active;
                    dataMoneyReceipt.MoneyReceiptDetails.Add(moneyReceiptDtl);

                    var currentyear = _companyProfileFacade.DateToday(companyId).Date.Year;
                    if (int.Parse(fee.ForYear.ToString()) == currentyear)
                    {
                        if (fee.PayType == PayType.Cash)
                            totalcashAmount += fee.CollectedAmount;
                        else
                            totalchequeAmount += fee.CollectedAmount;

                        if (feeinfo.AccountReceivable) 
                            totalreceivable += fee.CollectedAmount;
                        else
                            totalincome += fee.CollectedAmount;
                    }
                }
                if (totalcashAmount>0 || totalchequeAmount>0)
                {
                    #region Course fee Collection voucher entry

                    if (dto.PayType == PayType.Cash)
                    {

                        var CashAccHead = BizConstants.ATTICash;

                        if (string.IsNullOrEmpty(CashAccHead))
                        {
                            response.Message = "Cash in hand, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = CashAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of Trainee Course Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = totalcashAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = voucherdate;
                        voucherList.Add(voucher);
                    }

                    if (dto.PayType == PayType.Cheque || dto.PayType == PayType.PayOrder)
                    {
                        var ChequeAccHead = dto.BankAccHead;

                        if (string.IsNullOrEmpty(ChequeAccHead))
                        {
                            response.Message = "Cash at Bank, not found. Please contact system Administrator.";
                            return response;
                        }

                        var voucher = new VoucherDto();
                        voucher.AccountHeadCode = ChequeAccHead;
                        voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                        voucher.Description = "In time of Trainee Course Fee Collection.";
                        voucher.Credit = 0;
                        voucher.Debit = totalchequeAmount;
                        voucher.CompanyProfileId = companyId;
                        voucher.VoucherDate = voucherdate;
                        voucherList.Add(voucher);
                    }
                    #endregion
                }

                #region Contra Voucher List
                if (totalreceivable > 0)
                {
                    var CreditVoucher = new VoucherDto();

                    CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                    CreditVoucher.Description = "Course Fee Collection from " + trainee.Name;
                    CreditVoucher.Credit = totalreceivable;
                    CreditVoucher.Debit = 0;
                    CreditVoucher.CompanyProfileId = companyId;
                    CreditVoucher.VoucherDate = voucherdate;

                    var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Receivable_ATTI, null);
                    if (string.IsNullOrEmpty(accHeadCode))
                    {
                        response.Message = "Trainee Course Fee account head not found for the Receivable ATTI. Please contact system administrator.";
                        return response;
                    }
                    CreditVoucher.AccountHeadCode = accHeadCode;
                    voucherList.Add(CreditVoucher);
                }

                if (totalincome > 0)
                {
                    var CreditVoucher = new VoucherDto();

                    CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                    CreditVoucher.Description = "Other than Course Fee Collection from " + trainee.Name;
                    CreditVoucher.Credit = totalincome;
                    CreditVoucher.Debit = 0;
                    CreditVoucher.CompanyProfileId = companyId;
                    CreditVoucher.VoucherDate = voucherdate;

                    var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.Income_ATTI, null);
                    if (string.IsNullOrEmpty(accHeadCode))
                    {
                        response.Message = "Trainee Course Fee account head not found for the Income ATTI. Please contact system administrator.";
                        return response;
                    }
                    CreditVoucher.AccountHeadCode = accHeadCode;
                    voucherList.Add(CreditVoucher);
                }
                #endregion

                #region Payment Status Update

                var oldpayments = GenService.GetAll<CourseFeeCollectionTran>().Where(x => x.TraineeId == trainee.Id);

                var Oldcollections = from cat in oldpayments
                                     group cat by new { cat.Course, cat.TraineeId, cat.PendingFeeId, cat.ForYear }
                                     into fee
                                     select new CourseFeeCollectionTranDto
                                     {
                                         CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                         Course = fee.Key.Course,
                                         TraineeId = fee.Key.TraineeId,
                                         PendingFeeId = fee.Key.PendingFeeId,
                                         Year = fee.Key.ForYear

                                     };

                var presentcollections = from cat in dto.FeeCollectionTrans
                                         group cat by new { cat.Course, cat.TraineeId, cat.PendingFeeId, cat.ForYear }
                                  into fee
                                         select new CourseFeeCollectionTranDto
                                         {
                                             CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                             Course = fee.Key.Course,
                                             TraineeId = fee.Key.TraineeId,
                                             PendingFeeId = fee.Key.PendingFeeId,
                                             Year = fee.Key.ForYear
                                         };

                var allcollections = new List<CourseFeeCollectionTranDto>();
                allcollections.AddRange(Oldcollections);
                allcollections.AddRange(presentcollections);

                var collections = from col in allcollections
                                  group col by new { col.Course, col.TraineeId, col.PendingFeeId, col.Year }
                                  into fee
                                  select new CourseFeeCollectionTranDto
                                  {
                                      CollectedAmount = fee.Sum(s => s.CollectedAmount),
                                      Course = fee.Key.Course,
                                      TraineeId = fee.Key.TraineeId,
                                      PendingFeeId = fee.Key.PendingFeeId,
                                      Year = fee.Key.Year
                                  };


                foreach (var collection in collections)
                {
                    var pendingFee = GenService.GetAll<TraineeCourses>()
                                    .Where(s => s.Course == collection.Course && s.TraineeId == trainee.Id && pendingFeeIds.Contains(s.Id)).FirstOrDefault();

                    if (pendingFee != null)
                    {
                        if (collection.CollectedAmount == pendingFee.CourseFee)
                        {
                            if (pendingFee.Status == EntityStatus.Inactive)
                            {
                                if(pendingFee.Course<16)  //Only Course Fee Colleced MR will be updated
                                    trainee.MRNo = dataMoneyReceipt.MoneyReceiptNo;
                                pendingFee.Status = EntityStatus.Active; //PAyment Status=Paid
                                pendingFee.EditDate = DateTime.Now;
                                pendingFee.EditedBy = userId;
                                pendingFee.CollectedAmount = collection.CollectedAmount;
                                pendingFees.Add(pendingFee);
                            }
                        }
                        else
                        {
                            pendingFee.Status = EntityStatus.Inactive;
                            pendingFee.EditDate = DateTime.Now;
                            pendingFee.EditedBy = userId;
                            pendingFee.CollectedAmount = collection.CollectedAmount;
                            pendingFees.Add(pendingFee);
                        }
                    }

                }
               #endregion
            }

            using (var tran = new TransactionScope())
            {
                try
                {
                    GenService.Save(data);
                    if (dto.PayType == PayType.Cash)
                        voucherNumber = "CCV-" + _companyProfileFacade.GetUpdateCCvNo();
                    else
                        voucherNumber = "BCV-" + _companyProfileFacade.GetUpdateBCvNo();
                    
                    if (voucherList.Count > 0)
                        dataMoneyReceipt.VoucherNo = voucherNumber;
                    
                    GenService.Save(dataMoneyReceipt);
                    GenService.Save(pendingFees);
                    GenService.Save(trainee);
                    response.Id = dataMoneyReceipt.Id;
                    if (voucherList.Count > 0)
                    {
                        var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                       
                        voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                        var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, companyId, (long)userId);
                        if (!(bool)accountsResponse.Success)
                        {
                            response.Message = "Trainee Course Fee Collection Failed. " + accountsResponse.Message;
                            return response;
                        }
                        response.Complementary = voucherNumber;
                    }
                    GenService.SaveChanges();
                    tran.Complete();
                    response.Success = true;
                    response.Message = "Course Fee Collection Done Successfully";
                 }
                catch (Exception)
                {
                    tran.Dispose();
                    response.Message = "Collection failed";
                }
            }

            return response;
        }
        public List<FeeTypeDto> GetQuantitiveFeetypes()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x => x.CompanyProfileId == 2) // ATTI Office=2
          .Select(t => new FeeTypeDto()
          {
              Id = t.Id,
              Name = t.Name,
              IsQuantitive = t.IsQuantitive
              //DisplayName = UiUtil.GetDisplayName(t)
          });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == true);
            }
            return typeList.ToList();
        }

        public List<FeeTypeDto> GetNormalQuantitiveFeetypes()
        {
            var typeList = GenService.GetAll<FeeType>().Where(x => x.CompanyProfileId == 2 && x.AccountReceivable==false) // ATTI Office=2
          .Select(t => new FeeTypeDto()
          {
              Id = t.Id,
              Name = t.Name,
              IsQuantitive = t.IsQuantitive
              //DisplayName = UiUtil.GetDisplayName(t)
          });
            if (typeList.Any())
            {
                typeList = typeList.Where(r => r.IsQuantitive == true);
            }
            return typeList.ToList();
        }

        public List<TraineeDto> GetTraineeForFeeSetup(List<long?> feeTypeIds, string batch, string trno, string name)
        {
            var systemDate = _companyProfileFacade.DateToday(1).Date;  // ATAB Office =1

            var members = GenService.GetAll<Trainee>().Where(r => r.Status == EntityStatus.Active);
            if (batch != "")
            {
                members = members.Where(t => t.Batch.Contains(batch));
            }

            if (trno != "")
            {
                members = members.Where(t => t.TraineeNo.Contains(trno));
            }

            if (name != "")
                members = members.Where(t => t.Name.Contains(name));

            var memberIds = members.Select(r => r.Id);
            var memberFeeCol = GenService.GetAll<TraineeCourses>().Where(r => r.Status == EntityStatus.Inactive && memberIds.Contains((long)r.TraineeId) && feeTypeIds.Contains((r.Course)));
            var memberFeeColIds = memberFeeCol.Select(l => l.TraineeId).Distinct();
            if (memberFeeColIds != null)
            {
                var eligibleMember = members.Where(l => !memberFeeColIds.Contains(l.Id));
                var result = Mapper.Map<List<TraineeDto>>(eligibleMember);
                return result;
            }

            else
            {
                var result = Mapper.Map<List<TraineeDto>>(members);
                return result;
            }

        }

        public ResponseDto SetTraineeFees(long FeeTypeId, List<TraineeDto> trainees, long userId, long selectedCompanyId)
        {
            ResponseDto responce = new ResponseDto();
            var pendingFeeList = new List<TraineeCourses>();
            using (var tran = new TransactionScope())
            {
                try
                {
                    decimal total = 0;
                    trainees = trainees.Where(r => r.IsChecked == true).ToList();
                    if (trainees != null && trainees.Any())
                    {
                        foreach (var dt in trainees)
                        {
                            
                            var mbr = new TraineeCourses();
                            var trainee = GenService.GetById<Trainee>((long)dt.Id);
                            var feeInfo = GenService.GetById<FeeType>(FeeTypeId);

                            total = total + (feeInfo.Amount * dt.Quantity);
                               
                            mbr.CourseFee = total;
                            mbr.TraineeId = (long)dt.Id;
                            mbr.Course = FeeTypeId;
                            mbr.Status = EntityStatus.Inactive;
                            mbr.CreateDate = DateTime.Now;
                            mbr.CreatedBy = userId;
                            mbr.EffectiveDate = _companyProfileFacade.DateToday(selectedCompanyId).Date;
                            pendingFeeList.Add(mbr);
                        }
                        GenService.Save(pendingFeeList);
                        
                        tran.Complete();
                        responce.Success = true;
                        responce.Message = "Student Fee Setup Successfully";
                    }
                    else
                    {
                        tran.Dispose();
                        responce.Message = "There is no selected Members.";
                    }
                }
                catch (Exception ex)
                {
                    tran.Dispose();
                    responce.Message = "Student Fee Setup  Failed";
                }
            }
            return responce;
        }

        public IPagedList<CourseFeeCollectionDto> PaymentHistory(long traineeid, DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, PayType? payby, long? course)
        {

            var transactions = GenService.GetAll<CourseFeeCollectionTran>().Where(m => m.TraineeId == traineeid);
            var result = new List<CourseFeeCollectionTran>();
            if (course != null)
            {
                transactions = transactions.Where(m => m.Course== course);
            }
            if (payby != null)
            {
                transactions = transactions.Where(m => m.PayType == payby);
            }
            if (fromDate != null && fromDate > DateTime.MinValue)
                transactions = transactions.Where(m => m.SubscriptionDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                transactions = transactions.Where(m => m.SubscriptionDate <= toDate);
            
            var transactionList = from trans in transactions
                                  select new CourseFeeCollectionDto
                                  {
                                      Id = trans.Id,
                                      TraineeId = trans.TraineeId,
                                      PayType = trans.PayType,
                                      PayTypeName = trans.PayType.ToString(),
                                      Course = trans.Course,
                                      CourseName = trans.Courses.Name,
                                      ForYear = trans.ForYear,
                                      CollectedAmount = trans.CollectedAmount,
                                      ChequeNo = trans.ChequeNo,
                                      SubscriptionDate = trans.SubscriptionDate,

                                  };
           
            return transactionList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        #region Money Receipt
        public IPagedList<TraineeMoneyReceiptDto> MoneyReceipt(long traineeid, DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount)
        {

            var transactions = GenService.GetAll<TraineeMoneyReceipt>().Where(m => m.TraineeId == traineeid);

            if (fromDate != null && fromDate > DateTime.MinValue)
                transactions = transactions.Where(m => m.CreateDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                transactions = transactions.Where(m => m.CreateDate <= toDate);
            var transactionList = from trans in transactions
                                  select new TraineeMoneyReceiptDto
                                  {
                                      Id = trans.Id,
                                      TraineeId = trans.TraineeId,
                                      TraineeName = trans.Trainee.Name,
                                      TotalCollectedAmount = trans.TotalCollectedAmount,
                                      MoneyReceiptNo = trans.MoneyReceiptNo,
                                      CreateDate = trans.CreateDate
                                  };

            return transactionList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }

        public TraineeMoneyReceiptDto GetMoneyReceiptById(long receiptId)
        {
            var data = GenService.GetById<TraineeMoneyReceipt>(receiptId);
            return Mapper.Map<TraineeMoneyReceiptDto>(data);
        } 
        #endregion
    }
}
