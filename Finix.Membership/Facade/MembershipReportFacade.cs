﻿using AutoMapper;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Service;
using Finix.Membership.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;

namespace Finix.Membership.Facade
{
    public class MembershipReportFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        public List<MembershipReportDto> GetReport()
        {
            var list = new List<MembershipReportDto>();
            return list;
          
        }
        public MemberApplicationDto GetBasicInfoById(long memid)
        {
            var mem = _service.GetById<Member>(memid);
            var owners = GenService.GetAll<Owner>()
                .Where(o => o.MemberId == mem.Id && o.IsContactPerson == false).ToList();

            var ownerPhoto = owners.Count > 0 ? owners.FirstOrDefault().Photo : "";
            MemberApplicationDto basicdto = new MemberApplicationDto();
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            basicdto.NameOfOrganization = (mem.NameOfOrganization != null ? mem.NameOfOrganization : "");
            basicdto.Phone =mem.Phone;
            basicdto.Email = mem.Email;
            basicdto.MobileNo = mem.MobileNo;
            basicdto.Fax = mem.Fax;
            basicdto.Website = mem.Website;

            basicdto.OwnershipStatusName = (mem.OwnershipStatus != null ? UiUtil.GetDisplayName(mem.OwnershipStatus) : "");
            basicdto.MembershipStatusName = (mem.MembershipStatus != null ? UiUtil.GetDisplayName(mem.MembershipStatus) : "");
            basicdto.ZoneName = (mem.Zone != null ? UiUtil.GetDisplayName(mem.Zone) : "");

            basicdto.TradeLicenseNo =mem.TradeLicenseNo;
            basicdto.RenewalNo = mem.RenewalNo;
            basicdto.RepresentativeName = mem.RepresentativeName;
            basicdto.RepresentativeNID = mem.RepresentativeNID;
            basicdto.MRNo = mem.MRNo;
            basicdto.PONo = mem.PONo;
            basicdto.Bank = mem.Bank;
            basicdto.Branch = mem.Branch;

            //PhotoTest = r.Photo != null ? Path.GetFileName(r.Photo) : "",
            //PhotoPath = r.Photo
            basicdto.LicenseDate = mem.LicenseDate;
            basicdto.TradeLicenseFileTest = mem.TradeLicenseFile != null ? Path.GetFileName(mem.TradeLicenseFile) : "";
            basicdto.TradeLicenseFilePath = mem.TradeLicenseFile != null ? mem.TradeLicenseFile : "";
            basicdto.IncorporationDate = mem.IncorporationDate;
            basicdto.IncorporationFileTest = mem.IncorporationFile != null ? Path.GetFileName(mem.IncorporationFile) : "";
            basicdto.IncorporationFilePath = mem.IncorporationFile != null ? mem.RenewalFile : "";
            basicdto.RenewalDate = mem.RenewalDate;
            basicdto.RenewalFileTest = mem.RenewalFile != null ? Path.GetFileName(mem.RenewalFile) : "";
            basicdto.RenewalFilePath = mem.RenewalFile != null ? mem.RenewalFile : "";
            basicdto.RepresentativePhotoFileTest = mem.RepresentativePhotoFile;

            basicdto.TradeLicenseFilePath = mem.TradeLicenseFile;
            basicdto.IncorporationFilePath = mem.IncorporationFile;
            basicdto.RenewalFilePath = mem.RenewalFile;
            basicdto.RepresentativePhotoFilePath = mem.RepresentativePhotoFile;
            basicdto.PresentAddresses = mem.PresentAddress.AddressLine1 + " " + mem.PresentAddress.AddressLine2;
            basicdto.PrePostalCode = mem.PresentAddress.PostalCode;
            basicdto.OwnershipStatusName = mem.OwnershipStatus.ToString();
            basicdto.EstablishmentDate = mem.EstablishmentDate;
            basicdto.ParmanentAddresses = mem.ParmanentAddress != null ?
                mem.ParmanentAddress.AddressLine1 != null ? mem.ParmanentAddress.AddressLine1 :""
                + " " + mem.ParmanentAddress.AddressLine2 != null ? mem.ParmanentAddress.AddressLine2 : "" + " "
                + mem.ParmanentAddress.Division.DivisionNameEng != null ? mem.ParmanentAddress.Division.DivisionNameEng : "" + ","
                + mem.ParmanentAddress.District.DistrictNameEng != null ? mem.ParmanentAddress.District.DistrictNameEng : "" + ","
                + mem.ParmanentAddress.Thana.ThanaNameEng != null ? mem.ParmanentAddress.Thana.ThanaNameEng : "" + ","
                + mem.ParmanentAddress.Area.Name != null ? mem.ParmanentAddress.Area.Name : "" +
                mem.ParmanentAddress.PostalCode : "";
            basicdto.ParCellPhoneNo = mem.ParmanentAddress.CellPhoneNo;
            basicdto.RepresentativeNID = mem.RepresentativeNID;
            basicdto.RepresentativePhotoFilePath = !string.IsNullOrWhiteSpace(mem.RepresentativePhotoFile) ? "http://" + url + "/" + mem.RepresentativePhotoFile.Replace("\\", "/") : "";//"http://" + url + "/UploadedFiles/" + mem.MemberNo + "/" + System.IO.Path.GetFileName(mem.RepresentativePhotoFile);
            basicdto.OwnerPhotoFilePath = !string.IsNullOrWhiteSpace(ownerPhoto) ? "http://" + url + "/" + ownerPhoto.Replace("\\", "/") : "";//"http://" + url + "/UploadedFiles/" + mem.MemberNo + "/" + System.IO.Path.GetFileName(ownerPhoto);
            return basicdto;
        }

        public List<OwnerDto> GetOwners(long memid)
        {
            var owners = _service.GetAll<Owner>().Where(x => x.MemberId == memid && x.Status==Infrastructure.EntityStatus.Active);
            var data = owners.Select(x => new OwnerDto
            {
                Id = x.Id,
                Name = x.Name,
                Designation = x.Designation,
                NID = x.NID
            }).ToList();
            return data;
        }

        public List<MemberApplicationDto> GetMemberListReport()
        {
            MemberApplicationDto memberDto = new MemberApplicationDto();
            List<MemberApplicationDto> memberList = new List<MemberApplicationDto>();
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            var list = new List<MemberDto>();
            memberList = (from m in GenService.GetAll<Member>()
                .Where(s => s.MembershipStatus == Infrastructure.MembershipStatus.Approved).ToList()
                          select new MemberApplicationDto
                          {
                              MemberNo = m.MemberNo,
                              RepresentativeName = m.RepresentativeName,
                              Phone = m.Phone,
                              Email = m.Email,
                              NameOfOrganization = m.NameOfOrganization,
                              Address = GetAddress(m.Id),
                              MobileNo = m.MobileNo,
                              ImagePath = !string.IsNullOrWhiteSpace(m.RepresentativePhotoFile) ? "http://" + url + "/" + m.RepresentativePhotoFile.Replace("\\", "/") : "",//"/UploadedFiles/" + m.MemberNo + "/" + System.IO.Path.GetFileName(m.RepresentativePhotoFile),
                              Designation = GetDesignation(m.Id)
                         }).ToList();
           
            return memberList;

        }

        public string GetAddress(long id)
        {
            var contact = GenService.GetAll<Owner>().Where(d => d.MemberId == id && d.IsContactPerson == true).FirstOrDefault();
            var val = contact != null ? contact.PresentAddress : "";
            return val;        
        }
        public string GetDesignation(long id)
        {
            var contact = GenService.GetAll<Owner>().Where(d => d.MemberId == id && d.IsContactPerson == true).FirstOrDefault();
            var val = contact != null ? contact.Designation : "";
            return val;
        }
        public List<OwnerDto> GetContactPerson(long memid)
        {
            var owners = GenService.GetAll<Owner>()
                .Where(s => s.MemberId == memid && 
                s.Status == Infrastructure.EntityStatus.Active && 
                s.IsContactPerson == false).ToList();
            var data = (from x in owners
                        select new OwnerDto
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Designation = x.Designation,
                            PresentAddress = x.PresentAddress,
                            PermanentAddress = x.PermanentAddress,
                            Phone = x.Phone,
                            MobileNo = x.MobileNo
                        }).ToList();
      
            return data;
        }
        public List<ChamberDto> GetChamber(long memid)
        {
            var owners = _service.GetAll<MemberChambers>().Where(x => x.MemberId == memid && x.Status==Infrastructure.EntityStatus.Active);
            var data = owners.Select(x => new ChamberDto
            {
                Name = x.Chamber.Name
            }).ToList();
            return data;
        }
        public List<AssociationDto> GetAssociation(long memid)
        {
            var owners = _service.GetAll<MemberAssociations>().Where(x => x.MemberId == memid && x.Status == Infrastructure.EntityStatus.Active);
            var data = owners.Select(x => new AssociationDto
            {
                Name = x.Association.Name
            }).ToList();
            return data;
        }
        public List<BusinessCategoryDto> GetBusinessCategory(long memid)
        {
            var owners = _service.GetAll<MemberBusinessCatagories>().Where(x => x.MemberId == memid && x.Status == Infrastructure.EntityStatus.Active);
            var data = owners.Select(x => new BusinessCategoryDto
            {
                Name = x.BusinessCategory.Name
            }).ToList();
            return data;
        }
        public List<AttachmentDto> GetAttachment(long memid)
        {
            //char c1 = a;
            string url = System.Web.HttpContext.Current.Request.Url.Authority;
            var owners = _service.GetAll<Attachment>().Where(x => x.MemberId == memid && x.Status==Infrastructure.EntityStatus.Active);
            var member = GenService.GetById<Member>(memid);

            var data = owners.AsEnumerable().Select(x => new AttachmentDto
            {
                DocumentTypeName = x.DocumentType.Name,
                FilePath = !string.IsNullOrWhiteSpace(x.Path) ? "http://" + url + "/" + x.Path.Replace("\\", "/") : "",//"http://" + url + "/UploadedFiles/" + member.MemberNo + "/" + System.IO.Path.GetFileName(x.Path),

            }).ToList();
            return data;
        }

        public List<CertificateRegisterDetailDto> GetMembershipCertificate(long cid)
        {
            //CertificateRegisterDetail certificate = new CertificateRegisterDetail();
            var certificate = GenService.GetAll<CertificateRegisterDetail>().Where(x=>x.Id==cid).ToList();
            var result = (from cer in certificate
                         select new CertificateRegisterDetailDto
                         {
                             CertificateNo = cer.CertificateNo,
                             CertificateRegisterId = cer.CertificateRegisterId,
                             CertificateStatus = cer.CertificateStatus,
                             Id = cer.Id,
                             IssueDate = cer.IssueDate,
                             ExpiryDate = cer.ExpiryDate,
                             CertificateType = cer.CertificateRegister.CertificateType,
                             CertificateTypeName = cer.CertificateRegister.CertificateType.ToString(),
                             MemberNo = cer.MemberNo,
                             TransactionDate = cer.TransactionDate,
                             MemberName=cer.Member.NameOfOrganization,
                             Address= (cer.Member.ParmanentAddress.AddressLine1!="" ? cer.Member.ParmanentAddress.AddressLine1 + ", " : "") + (cer.Member.ParmanentAddress.AddressLine2!=""? cer.Member.ParmanentAddress.AddressLine2 + ", " :"") + (cer.Member.ParmanentAddress.District!=null? cer.Member.ParmanentAddress.District.DistrictNameEng:"")
                         }).ToList();
          return result;
        }

        public List<FeeCollectionTranDto> GetMemberFeeCollections(long memberid, DateTime fromDate, DateTime toDate)
        {
            
            var collections = GenService.GetAll<FeeCollectionTran>().Where(x => x.MemberId == memberid);
            var moneyreceipts = GenService.GetAll<MoneyReceipt>().Where(x => x.MemberId == memberid);
            if (fromDate != null && fromDate > DateTime.MinValue)
                collections = collections.Where(m => m.SubscriptionDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                collections = collections.Where(m => m.SubscriptionDate <= toDate);

            var result = (from col in collections
                          join mr in moneyreceipts
                          on col.MoneyReceiptNo equals mr.MoneyReceiptNo
                          select new FeeCollectionTranDto
                          {
                              MemberId = col.MemberId,
                              ChequeNo = col.ChequeNo,
                              CollectedAmount = col.CollectedAmount,
                              Id = col.Id,
                              FeeTypeId = col.FeeTypeId,
                              FeeTypeName = "<b>" + col.FeeType.Name + " fee</b> for Year " + col.Year + "<br/> Paid by PO No:" + col.ChequeNo + " of " + col.Bank,
                              SubscriptionDate = col.SubscriptionDate,
                              Bank = col.Bank,
                              MemberNo = col.Member.NameOfOrganization,
                              VoucherNo=mr.VoucherNo

                          }).ToList();
            

            //var result = (from final in data

            //              select new FeeCollectionTranDto
            //              {
            //                  MemberId = col.MemberId,
            //                  ChequeNo = col.ChequeNo,
            //                  CollectedAmount = col.CollectedAmount,
            //                  Id = col.Id,
            //                  FeeTypeId = col.FeeTypeId,
            //                  FeeTypeName = "<b>" + col.FeeType.Name + " fee</b> for Year " + col.Year + "<br/> Paid by PO No:" + col.ChequeNo + " of " + col.Bank,
            //                  SubscriptionDate = col.SubscriptionDate,
            //                  Bank = col.Bank,
            //                  MemberNo = col.Member.NameOfOrganization,
                              
            //              }).ToList();
           
            return result;
        }

        public List<MemberLedgerDto> GetMemberLedgerInfo(long memberid, DateTime fromDate, DateTime toDate)
        {
            var member = GenService.GetAll<Member>().Where(x => x.Id == memberid);
            var result = (from mem in member
                          select new MemberLedgerDto
                          {
                              MemberId = mem.Id,
                              MemberName = mem.NameOfOrganization,
                              FromDate = fromDate,
                              Todate = toDate,
                              Balance = 0
                          }).ToList();
            return result;
        }

        public List<MembershipFeeDetailsReportDto> GetFeewiseReceiptAmount(DateTime fromDate, DateTime toDate)
        {
            var result = new List<MembershipFeeDetailsReportDto>();
            var trans = GenService.GetAll<FeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate).GroupBy(x => new { x.FeeType, x.Year }).ToList();
            result = (from tran in trans
                      select new MembershipFeeDetailsReportDto
                      {
                          FeeName = tran.Key.FeeType.Name,
                          FeeTypeId = tran.Key.FeeType.Id,
                          Amount = (double)tran.Sum(x => x.CollectedAmount),
                          Count = tran.LongCount(),
                          Year = tran.Key.Year
                      }).ToList();
            return result;
        }

        public List<FeeWiseMembersDto> GetFeeWiseMemberList(long feetype,DateTime fromDate, DateTime toDate)
        {
            var year = toDate.Year;
            var calendars = new List<long>{ 4, 11 } ;
            var members = new List<FeeCollectionTran>();
            var feeinfo = GenService.GetById<FeeType>(feetype);
            var result = new List<FeeWiseMembersDto>();

            if (feeinfo.CompanyProfileId == 1)
            {
                if(feeinfo.Id==11 || feeinfo.Id==4)
                    members = GenService.GetAll<FeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate && calendars.Contains((long)x.FeeTypeId)).ToList();
                else
                    members = GenService.GetAll<FeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate && x.FeeTypeId == feetype).ToList();
                var moneyreceipts = GenService.GetAll<MoneyReceipt>();
                //var trans = GenService.GetAll<FeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate && x.FeeTypeId==feetype).GroupBy(x => new { x.Member,x.SubscriptionDate}).ToList();
                result = (from mem in members
                          join mr in moneyreceipts
                          on mem.MoneyReceiptNo equals mr.MoneyReceiptNo
                          select new FeeWiseMembersDto
                          {
                              MemberName = mem.Member.NameOfOrganization,
                              MemberId = mem.MemberId,
                              CollectedAmount = (double)mem.CollectedAmount,
                              SubscriptionDate = mem.SubscriptionDate,
                              Year = mem.Year,
                              VoucherNo = mr.VoucherNo,
                              FeeTypeName = ((mem.FeeTypeId==4 || mem.FeeTypeId == 11)?"ATAB Calendar": mem.FeeType.Name)

                          }).ToList();
            }
            else
            {
                var students = GenService.GetAll<CourseFeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate && x.Course == feetype);
                var moneyreceipts = GenService.GetAll<TraineeMoneyReceipt>();
                //var trans = GenService.GetAll<FeeCollectionTran>().Where(x => x.SubscriptionDate >= fromDate && x.SubscriptionDate <= toDate && x.FeeTypeId==feetype).GroupBy(x => new { x.Member,x.SubscriptionDate}).ToList();
                result = (from mem in students
                          join mr in moneyreceipts
                          on mem.MoneyReceiptNo equals mr.MoneyReceiptNo
                          select new FeeWiseMembersDto
                          {
                              MemberName = mem.Trainee.Name,
                              MemberId = mem.TraineeId,
                              CollectedAmount = (double)mem.CollectedAmount,
                              SubscriptionDate = mem.SubscriptionDate,
                              Year = year.ToString(),
                              VoucherNo = mr.VoucherNo,
                              FeeTypeName = mem.Courses.Name

                          }).ToList();
            }

            return result;
        }
    }
}
