﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
  public  class CertificateRegisterStatusSummaryDto
    {
        public long? CompanyProfileId { get; set; }
        public CertificateType CertificateType { get; set; }
        public string CertificateTypeName { get; set; }
        public string CertificateName { get; set; }
        public string StartingNumber { get; set; }
        public int TotalPages { get; set; }
        public int UnUsedCount { get; set; }
        public int IssuedCount { get; set; }
        public int VoidCount { get; set; }
        public int RuinedCount { get; set; }
    }
}
