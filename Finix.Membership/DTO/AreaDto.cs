﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class AreaDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public long? ThanaId { get; set; }
        public string ThanaName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
       
    }
}
