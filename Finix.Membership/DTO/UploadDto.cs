﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Finix.Membership.DTO
{
    public class UploadDto
    {
        public long Id { get; set; }
        public HttpPostedFileBase PhotoFile { get; set; }
    }
}
