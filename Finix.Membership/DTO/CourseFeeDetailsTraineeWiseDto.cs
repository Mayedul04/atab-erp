﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
  public  class CourseFeeDetailsTraineeWiseDto
    {
        public long PendingId { get; set; }
        public long TraineeId { get; set; }
        public long Course { get; set; }
        public string CourseName { get; set; }
        public decimal CourseFee { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal Due { get; set; }
        public bool IsChecked { get; set; }
        public bool IsDue { get; set; }
        public string ForYear { get; set; }
    }
}
