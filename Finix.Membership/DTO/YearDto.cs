﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
  public  class YearDto
    {
        public string Year { get; set; }
    }
}
