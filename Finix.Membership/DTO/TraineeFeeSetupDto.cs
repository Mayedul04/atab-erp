﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class TraineeFeeSetupDto
    {
        public List<FeeTypeDto> FeeTypeIds { get; set; }
        public List<TraineeDto> Trainees { get; set; }
    }
}
