﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class FeeCollectionTranDto
    {
        public long Id { get; set; }
        public long? MemberId { get; set; }
        public long? PendingFeeId { get; set; }
        public long? FeeTypeId { get; set; } 
        public DateTime? SubscriptionDate { get; set; }
        public string SubscriptionDateTxt { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ChequeDateTxt { get; set; }
        public string Year { get; set; }
        public string FromYear { get; set; }
        public string ToYear { get; set; }
        public string Bank { get; set; }
        
        public PayType PayType { get; set; }
        public string PayTypeName { get; set; }
        public decimal CollectedAmount { get; set; }
        public string ChequeNo { get; set; }
        public string MemberNo { get; set; }
        public string FeeTypeName { get; set; }
        public string VoucherNo { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
