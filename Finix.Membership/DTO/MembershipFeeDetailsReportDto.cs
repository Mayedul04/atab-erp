﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class MembershipFeeDetailsReportDto
    {
        public long? FeeTypeId { get; set; }
        public string FeeName { get; set; }
        public string Year { get; set; }
        public long Count { get; set; }
        public double Amount { get; set; }
        public bool IsLiability { get; set; }

    }
}
