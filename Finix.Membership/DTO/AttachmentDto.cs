﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Finix.Membership.DTO
{
    public class AttachmentDto
    {
        public long? Id { get; set; }
        public string Path { get; set; }
        public HttpPostedFileBase PathName { get; set; }
        public string PathTest { get; set; }
        public string FilePath { get; set; }
        public long? DocumentTypeId { get; set; }
        public string DocumentTypeName { get; set; }
        public long? MemberId { get; set; }
        public char ci { get; set; }


    }
}
