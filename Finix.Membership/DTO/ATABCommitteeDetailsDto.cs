﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Finix.Membership.Dto;
using Finix.Membership.Infrastructure;

namespace Finix.Membership.DTO
{
    public class ATABCommitteeDetailsDto
    {
        public long? Id { get; set; }
        public string Photo { get; set; }
        public long? MemberId { get; set; }
        public string NameOfOrganization { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public Zone Zone { get; set; }
        public string ZoneName { get; set; }
        public Post Post { get; set; }
        public string PostName { get; set; }
        public CommitteeType CommitteeType { get; set; }
        public string CommitteeTypeName { get; set; }
        public DateTime? JoiningDate { get; set; }
        public string JoiningDateTxt { get; set; }
        public DateTime? EndOfTenure { get; set; }
        public string EndOfTenureTxt { get; set; }
        public string YearRange { get; set; }
    }
}
