﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Finix.Membership.DTO
{
    public class OwnerDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string NID { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Res { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public bool IsContactPerson { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public string PhotoPath { get; set; }
        public string PhotoTest { get; set; }
        public long? MemberId { get; set; }
        public bool AddressCheck { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
