﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class ResponseDto
    {
        public ResponseDto()
        {
            Success = false;
            Message = "";
        }

        //public List<FeeCollectionTranDto> FeeCollectionTranDtos { get; set; }
        public long? Id { get; set; }

        public String Complementary { get; set; }
        public bool? Success { get; set; }
        public string Message { get; set; }
    }
}
