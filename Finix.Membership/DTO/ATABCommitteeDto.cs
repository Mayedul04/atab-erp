﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Finix.Membership.Dto;
using Finix.Membership.DTO;
using Finix.Membership.Infrastructure;
using Finix.Membership.Infrastructure.Models;

namespace Finix.Membership.Dto
{
    public class ATABCommitteeDto
    {
        public long? Id { get; set; }
        public string Photo { get; set; }
        public long? MemberId { get; set; }
        public MemberDto Member { get; set; }
        public MemberautofilLoadDto LoadMemberautofilLoad { get; set; }
        public HttpPostedFileBase PhotoFile { get; set; }
        public string PhotoName { get; set; }
        public string NameOfOrganization { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long? AddressId { get; set; }
        public AddressDto Address { get; set; }
        public string Remarks { get; set; }
        public List<TenureHistoryDto> TenureHistoryDetails { get; set; }
        public List<long> RemovedTenures { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
