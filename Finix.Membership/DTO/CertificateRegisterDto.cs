﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
  public  class CertificateRegisterDto
    {
        public void CertificateRegisterDetailDto()
        {
            Details = new List<CertificateRegisterDetailDto>();
        }
        public long? Id { get; set; }
        public string CertificateName { get; set; }
        public CertificateType CertificateType { get; set; }

        public long? CompanyProfileId { get; set; }
        public string CompanyProfileName { get; set; }
        public string Prefix { get; set; }
        public string StartingNumber { get; set; }
        public string EndingNumber { get; set; }
        public long? TotalPages { get; set; }
        public DateTime? BookIssueDate { get; set; }
        public IEnumerable<CertificateRegisterDetailDto> Details { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
