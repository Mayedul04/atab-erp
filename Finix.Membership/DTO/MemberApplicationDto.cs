﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Finix.Membership.Infrastructure.Models;

namespace Finix.Membership.DTO
{
    public class MemberApplicationDto
    {
        public long? Id { get; set; }
        public string NameOfOrganization { get; set; }
        public string MemberNo { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public DateTime? EstablishmentDate { get; set; }
        public OwnershipStatus OwnershipStatus { get; set; }
        public MembershipStatus MembershipStatus { get; set; }
        public string ZoneName { get; set; }
        public Zone Zone { get; set; }
        public string OwnershipStatusName { get; set; }
        public string MembershipStatusName { get; set; }
        public List<MemberBusinessCatagoriesDto> BusinessCategories { get; set; }
        public List<MemberAssociationsDto> Associations { get; set; }
        public List<MemberChambersDto> Chambers { get; set; }
        public List<MemberPendingFeeDto> MemberPendingFees { get; set; }
        public List<Zone> ZoneList { get; set; }
        public DateTime? IncorporationDate { get; set; }
        public string IncorporationDateText { get; set; }
        public DateTime? RenewalIssueDate { get; set; }
        public string RenewalIssueDateText { get; set; }
        public DateTime? RenewalDate { get; set; }
        public string RenewalDateText { get; set; }
        public DateTime? LicenseDate { get; set; }
        public string LicenseDateText { get; set; }
        public string TradeLicenseNo { get; set; }
        public string RenewalNo { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativeNID { get; set; }
        public string MRNo { get; set; }
        public DateTime? DateOfReceived { get; set; }
        public DateTime? IssueDate { get; set; }
        public int FromYear { get; set; }
        public string PONo { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public HttpPostedFileBase TradeLicenseFile { get; set; }
        public HttpPostedFileBase RenewalFile { get; set; }
        public HttpPostedFileBase IncorporationFile { get; set; }
        public HttpPostedFileBase RepresentativePhotoFile { get; set; }

        public string TradeLicenseFileTest { get; set; }
        public string RenewalFileTest { get; set; }
        public string IncorporationFileTest { get; set; }
        public string RepresentativePhotoFileTest { get; set; }

        public string TradeLicenseFilePath { get; set; }
        public string RenewalFilePath { get; set; }
        public string IncorporationFilePath { get; set; }
        public string RepresentativePhotoFilePath { get; set; }
        public string OwnerPhotoFilePath { get; set; }

        public List<long> ChamberIds { get; set; }
        public List<long> BusinessCategoryIds { get; set; }
        public List<long> AssociationIds { get; set; }

        public bool AddressCheck { get; set; }
        public bool IsDisable { get; set; }

        public List<Chamber> ChamberList { get; set; }
        public List<BusinessCategory> BusinessCategoryList { get; set; }
        public List<Association> AssociationList { get; set; }

        public List<OwnerDto> OwnerList { get; set; }
        public List<FeeCollectionTranDto> PaymentList { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public string PayTypeName { get; set; }
        public decimal CollectedAmount { get; set; }
        public string FeeTypeName { get; set; }
        public string MemberName { get; set; }

        public string PreAddressLine1 { set; get; }
        public string PreAddressLine2 { set; get; }
        public string PrePostalCode { set; get; }
        public string PreFax { set; get; }
        
        public string PreCellPhoneNo { set; get; }
        public string PreWebsite { set; get; }
        public string PrePhoneNo { set; get; }
        public string PreEmail { set; get; }

        public long? PreDivisionId { set; get; }
        public long? PreDistrictId { set; get; }
        public long? PreThanaId { set; get; }
        public long? PreAreaId { set; get; }

        public string ParAddressLine1 { set; get; }
        public string ParAddressLine2 { set; get; }
        public string ParPostalCode { set; get; }

        public string ParCellPhoneNo { set; get; }
        public string ParPhoneNo { set; get; }
        public string ParEmail { set; get; }

        public long? ParDivisionId { set; get; }
        public long? ParDistrictId { set; get; }
        public long? ParThanaId { set; get; }
        public long? ParAreaId { set; get; }

        public OwnerDto ContactPerson { get; set; }

        public string ContactPersonName { set; get; }
        public string ContactPersonDesignation { set; get; }
        public string ContactPersonNID { set; get; }

        public string ContactPersonMobileNo { set; get; }
        public string ContactPersonPhone { set; get; }
        public string ContactPersonEmail { set; get; }

        public string ContactPresentAddress { set; get; }
        public string ContactPersonPermanentAddress { set; get; }
        public AddressDto PresentAddress { set; get; }
        public AddressDto ParmanentAddress { set; get; }
        public string Address { get; set; }
        public string ImagePath { get; set; }
        public string Designation { get; set; }
        public string PresentAddresses { set; get; }
        public string ParmanentAddresses { set; get; }
        public string PostCode { set; get; }
    }
}
