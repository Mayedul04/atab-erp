﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberautofilLoadDto
    {
        public long key { get; set; }
        public string value { get; set; }
        public long address { get; set; }
    }
}
