﻿
using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class TraineeMoneyReceiptDetailsDto
    {
        public long? Id { get; set; }
        public long MoneyReceiptId { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public string Year { get; set; }
        public PayType PayType { get; set; }
        public string PayTypeName { get; set; }
        public decimal CollectedAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
