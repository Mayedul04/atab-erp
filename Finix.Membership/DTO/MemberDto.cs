﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Finix.Membership.Infrastructure.Models;

namespace Finix.Membership.DTO
{
    public class MemberDto
    {
        public long Id { get; set; }
        public string MemberNo { get; set; }
        public string NameOfOrganization { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public DateTime? EstablishmentDate { get; set; }
        public OwnershipStatus OwnershipStatus { get; set; }
        public MembershipStatus MembershipStatus { get; set; }
        public string ZoneName { get; set; }
        public Zone Zone { get; set; }
        public string OwnershipStatusName { get; set; }
        public string MembershipStatusName { get; set; }
        public List<MemberBusinessCatagoriesDto> BusinessCategories { get; set; }
        public List<MemberAssociationsDto> Associations { get; set; }
        public List<MemberChambersDto> Chambers { get; set; }
        public List<MemberPendingFeeDto> MemberPendingFees { get; set; }
        public DateTime? IncorporationDate { get; set; }
        public string IncorporationDateText { get; set; }
        public DateTime? RenewalDate { get; set; }
        public string RenewalDateText { get; set; }
        public DateTime? RenewalIssueDate { get; set; }
        public string RenewalIssueDateText { get; set; }
        public DateTime? LicenseDate { get; set; }
        public string LicenseDateText { get; set; }
        public string TradeLicenseNo { get; set; }
        public string RenewalNo { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativeNID { get; set; }
        public string MRNo { get; set; }
        public DateTime? DateOfReceived { get; set; }
        public string DateOfReceivedText { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueDateText { get; set; }
        public string PONo { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public long? CountryId { set; get; }
        public string CountryName { get; set; }
        public long? DivisionId { set; get; }
        public string DivisionNameEng { get; set; }
        public long? DistrictId { set; get; }
        public string DistrictNameEng { get; set; }
        public long? ThanaId { set; get; }
        public string ThanaNameEng { get; set; }
        public long? AreaId { set; get; }
        public string AreaName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public HttpPostedFileBase TradeLicenseFile { get; set; }
        public HttpPostedFileBase RenewalFile { get; set; }
        public HttpPostedFileBase IncorporationFile { get; set; }
        public HttpPostedFileBase RepresentativePhotoFile { get; set; }

        public string TradeLicenseFileTest { get; set; }
        public string RenewalFileTest { get; set; }
        public string IncorporationFileTest { get; set; }
        public string RepresentativePhotoFileTest { get; set; }

        public string TradeLicenseFilePath { get; set; }
        public string RenewalFilePath { get; set; }
        public string IncorporationFilePath { get; set; }
        public string RepresentativePhotoFilePath { get; set; }

        public List<long> ChamberIds { get; set; }
        public List<long> BusinessCategoryIds { get; set; }
        public List<long> AssociationIds { get; set; }
        public bool AddressCheck { get; set; }
        public bool? IsQuantitive { get; set; }
        public int Quantity { get; set; }
        public List<Chamber> ChamberList { get; set; }
        public List<BusinessCategory> BusinessCategoryList { get; set; }
        public List<Association> AssociationList { get; set; }
        public long? PresentAddressId { get; set; }
        public long? ParmanentAddressId { get; set; }
        public AddressDto PresentAddress { set; get; }
        public int FromYear { get; set; }
        public AddressDto ParmanentAddress { set; get; }
        public bool? IsChecked { get; set; }
       
    }
}
