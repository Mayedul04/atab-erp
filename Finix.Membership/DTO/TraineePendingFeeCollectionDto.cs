﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class TraineePendingFeeCollectionDto
    {
        public long TraineeId { get; set; }
        public string TraineeNo { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public List<CourseFeeDetailsTraineeWiseDto> PendingFeeList { get; set; }

    }
}
