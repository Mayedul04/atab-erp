﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class FeeCollectionRenewalDto
    {
        public List<long> FeeTypeIds { get; set; }
        public List<MemberDto> Members { get; set; }
    }
}
