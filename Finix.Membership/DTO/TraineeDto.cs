﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Finix.Membership.DTO
{
    public class TraineeDto
    {
        public long? Id { get; set; }
        public string TraineeNo { get; set; }
        public string Name { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public string GenderName { get; set; }
        public Religion? Religion { get; set; }
        public string ReligionName { get; set; }
        public string Occupation { get; set; }
        public string LastAcademicQualification { get; set; }
        public long? AddressId { get; set; }
        public TraineeAddressDto Address { get; set; }
        public List<TraineeCoursesDto> Courses { get; set; }
        public SourceOfInformation? SourceOfInformation { get; set; }
        public string SourceOfInformationName { get; set; }
        public string OtherSourceOfInformation { get; set; }
        public DateTime? DateOfAdmission { get; set; }
        public string DateOfAdmissionText { get; set; }
        public string Batch { get; set; }
        public string MRNo { get; set; }
        public decimal TotalFeeAmount { get; set; }
        public decimal TotalCollectedAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public string AccountsOfficer { get; set; }
        public string AdmissionOfficer { get; set; }
        public string UrlOfPhoto { get; set; }
        public string VirtualUrlOfPhoto { get; set; }
        public HttpPostedFileBase UrlOfPhotoName { get; set; }
        public string UrlOfSignature { get; set; }
        public string VirtualUrlOfSignature { get; set; }
        public HttpPostedFileBase UrlOfSignatureName { get; set; }
        public int FromYear { get; set; }
        public bool IsFeePending { get; set; }
        public bool? IsChecked { get; set; }
        public decimal Quantity { get; set; }
        public bool? IsQuantitive { get; set; }

        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }

    public class TraineeCoursesDto
    {
        public long? Id { get; set; }
        public long TraineeId { get; set; }
        public long Course { get; set; }
        public string CourseName { get; set; }
        public decimal CourseFee { get; set; }
        public decimal CollectedAmount { get; set; }
        public string MRNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class TraineeFilesDto
    {
        public long? Id { get; set; }
        public long TraineeId { get; set; }
        public string PhotoPath { get; set; }
        public HttpPostedFileBase PhotoPathName { get; set; }
        public string SignaurePath { get; set; }
        public HttpPostedFileBase SignaurePathName { get; set; }

    }
}
