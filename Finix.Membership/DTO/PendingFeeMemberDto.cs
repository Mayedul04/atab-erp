﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Membership.Infrastructure;

namespace Finix.Membership.DTO
{
    public class PendingFeeMemberDto
    {
        public long? MemberPendingFeeId { get; set; }
        public long Id { get; set; }
        public string MemberNo { get; set; }
        public string NameOfOrganization { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public DateTime? EstablishmentDate { get; set; }
        public OwnershipStatus OwnershipStatus { get; set; }
        public MembershipStatus MembershipStatus { get; set; }
        public string ZoneName { get; set; }
        public string OwnershipStatusName { get; set; }
        public string MembershipStatusName { get; set; }
        public long? BusinessCategoryId { get; set; }
        public List<MemberBusinessCatagoriesDto> BusinessCategories { get; set; }
        public List<MemberAssociationsDto> Associations { get; set; }
        public List<MemberChambersDto> Chambers { get; set; }
        public List<MemberPendingFeeDto> MemberPendingFees { get; set; }
        public DateTime? IncorporationDate { get; set; }
        public string IncorporationDateText { get; set; }
        public DateTime? RenewalDate { get; set; }
        public string RenewalDateText { get; set; }
        public DateTime? LicenseDate { get; set; }
        public string LicenseDateText { get; set; }
        public string TradeLicenseNo { get; set; }
        public string RenewalNo { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativeNID { get; set; }
        public string MRNo { get; set; }
        public DateTime? DateOfReceived { get; set; }
        public DateTime? IssueDate { get; set; }
        public string PONo { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public Zone Zone { get; set; }
        public long? CountryId { set; get; }
        public string CountryName { get; set; }
        public long? DivisionId { set; get; }
        public string DivisionNameEng { get; set; }
        public long? DistrictId { set; get; }
        public string DistrictNameEng { get; set; }
        public long? ThanaId { set; get; }
        public string ThanaNameEng { get; set; }
        public long? AreaId { set; get; }
        public string AreaName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public string TradeLicenseFileTest { get; set; }
        public string RenewalFileTest { get; set; }
        public string IncorporationFileTest { get; set; }
        public string RepresentativePhotoFileTest { get; set; }

        public string TradeLicenseFilePath { get; set; }
        public string RenewalFilePath { get; set; }
        public string IncorporationFilePath { get; set; }
        public string RepresentativePhotoFilePath { get; set; }
        public string PaymentStatus { get; set; }
        

    }
}
