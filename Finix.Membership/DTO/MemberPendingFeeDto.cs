﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Finix.Membership.DTO
{
    public class MemberPendingFeeDto
    {
        public long? Id { get; set; }
        public long? MemberId { get; set; }       
        public long? FeeTypeId { get; set; }
        public decimal Amount { get; set; }
        public bool CollectionStatus { get; set; }
        public string Remarks { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public bool AdmissionFeeCheckBox { get; set; }
        public bool AnnualSubscriptionFeeCheckBox { get; set; }

        public List<long> FeeTypeIds { get; set; }
        public List<FeeTypeDto> FeeTypeList { get; set; }
    }
}
