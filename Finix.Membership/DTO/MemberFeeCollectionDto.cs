﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberFeeCollectionDto
    {
        public List<FeeCollectionTranDto> FeeCollectionTrans { get; set; }
        public List<MemberDetailsFeeWiseDto> PendingFeeList { get; set; }
        public string BankAccHead { get; set; }
        public string MRAuthorizedPerson { get; set; }
        public string Description { get; set; }
        public PayType PayType { get; set; }
        public decimal? TotalPayableAmount { get; set; }
    }
}
