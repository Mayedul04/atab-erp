﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberDetailsFeeWiseDto
    {
        public long PendingFeeId { get; set; }
        public long MemberId { get; set; }
        public long MemberPendingFeeId { get; set; }
        public long FeeTypeId { get; set; }
        public string FeeTypeName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal Due { get; set; }
        public string ForYear { get; set; }
        public bool IsChecked { get; set; }
        public bool IsDue { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public string SubscriptionDateTxt { get; set; }
    }
}
