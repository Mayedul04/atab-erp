﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MembershipReportDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Zone { get; set; }
        public string Area { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Designation { get; set; }
        public string NID { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string FromDateText { get; set; }
        public string ToDateText { get; set; }
        public decimal Amount { get; set; }
     
    }
}
