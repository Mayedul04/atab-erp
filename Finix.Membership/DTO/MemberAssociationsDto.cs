﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberAssociationsDto
    {
        public long? MemberId { get; set; }
        public long? AssociationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
