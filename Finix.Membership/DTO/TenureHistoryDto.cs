﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Membership.Dto;
using Finix.Membership.Infrastructure;

namespace Finix.Membership.Dto
{
    public class TenureHistoryDto
    {
        public long? Id { get; set; }
        public long MemberCommitteId { get; set; }
        public  ATABCommitteeDto ATABCommittee { get; set; }
        public Zone Zone { get; set; }
        public Post Post { get; set; }
        public CommitteeType CommitteeType { get; set; }
        public DateTime? JoiningDate { get; set; }
        public string JoiningDateTxt { get; set; }
        public DateTime? EndOfTenure { get; set; }
        public string EndOfTenureTxt { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
