﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Membership.Infrastructure.Models;

namespace Finix.Membership.DTO
{
    public class MoneyReceiptDto
    {
        public long? Id { get; set; }
        public string MRType { get; set; }
        public long? MemberId { get; set; }
        public string MemberName { get; set; }
        public long? TraineeId { get; set; }
        public string TraineeName { get; set; }
        public string MoneyReceiptNo { get; set; }
        public string AccountOfficer { get; set; }
        public string AuthorizedPerson { get; set; }
        public string Description { get; set; }
        public string VoucherNo { get; set; }
        public Boolean IsVoucher { get; set; }
        public DateTime? CreateDate { get; set; }
        public List<MoneyReceiptDetailsDto> MoneyReceiptDetails { get; set; }
        public decimal TotalCollectedAmount { get; set; }
    }
}
