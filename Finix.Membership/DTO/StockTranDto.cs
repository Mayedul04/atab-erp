﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class StockTranDto
    {
        public long? Id { get; set; }
        public StockTranType StockTranType { get; set; }
        public long FromOfficeId { get; set; }
        public long ToOfficeId { get; set; }
        public string ChallanNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranDateText { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal TotalPrice { get; set; }
        public string Remarks { get; set; }
        public List<StockTranDetailDto> Details { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
    }
    public class StockTranDetailDto
    {
        public long? Id { get; set; }
        public long StockTranId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
        public decimal? OpeningQuantity { get; set; }
        public decimal? ReceivedQuantity { get; set; }
        public decimal? IssuedQuantity { get; set; }
        public decimal? ClosingQuantity { get; set; }
    }
}
