﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class CourseFeeCollectionDto
    {
        public long Id { get; set; }
        public long? TraineeId { get; set; }
        public long? PendingFeeId { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public string SubscriptionDateTxt { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ChequeDateTxt { get; set; }
        public string ForYear { get; set; }
        public string Bank { get; set; }
        public long? Course { get; set; }
        public string CourseName { get; set; }
        public PayType PayType { get; set; }
        public string PayTypeName { get; set; }
        public decimal CollectedAmount { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
