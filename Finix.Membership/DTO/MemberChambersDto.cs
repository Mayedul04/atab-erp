﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberChambersDto
    {
        public long? MemberId { get; set; }
        public long? ChamberId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
