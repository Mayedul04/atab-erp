﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class TraineeAddressDto
    {
        public long? Id { set; get; }
        public long? CountryId { set; get; }
        public string CountryName { get; set; }
        public long? DivisionId { set; get; }
        public string DivisionNameEng { get; set; }
        public long? DistrictId { set; get; }
        public string DistrictNameEng { get; set; }
        public long? ThanaId { set; get; }
        public string ThanaNameEng { get; set; }
        public long? AreaId { set; get; }
        public string AreaName { get; set; }
        public string PhoneNo { get; set; }
        public string CellPhoneNo { get; set; }
        public string Email { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string PostalCode { get; set; }
        public bool SameAddressCheck { get; set; }
        public long? MemberId { set; get; }
        public string Fax { get; set; }
        public string Website { get; set; }
    }
}
