﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class AutofillDto
    {
        public string prefix { get; set; }
        public List<long> exclusionList { get; set; }
    }
}
