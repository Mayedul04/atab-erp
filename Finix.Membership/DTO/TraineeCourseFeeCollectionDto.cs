﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
  public  class TraineeCourseFeeCollectionDto
    {
        public List<CourseFeeCollectionDto> FeeCollectionTrans { get; set; }
        public List<CourseFeeDetailsTraineeWiseDto> PendingFeeList { get; set; }
        public decimal? TotalPayableAmount { get; set; }
        public PayType PayType { get; set; }
        public string BankAccHead { get; set; }
        public string Description { get; set; }
        public string MRAuthorizedPerson { get; set; }
    }
}
