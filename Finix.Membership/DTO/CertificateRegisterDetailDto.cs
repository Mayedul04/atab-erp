﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class CertificateRegisterDetailDto
    {
        public long Id { get; set; }
        public string CertificateNo { get; set; }
        public string CertificateTypeName { get; set; }
        public CertificateType CertificateType { get; set; }
        public long CertificateRegisterId { get; set; }
        public CertificateStatus CertificateStatus { get; set; }
        public string CertificateStatusName { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public long? IssuedBy { get; set; }
        public string IssuedPerson { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long? MemberNo { get; set; }
        public string MemberName { get; set; }
        public string Address { get; set; }
        public long? TraineeNo { get; set; }
        public string StudentName { get; set; }
        public string Result { get; set; }
        public string FatherName { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
