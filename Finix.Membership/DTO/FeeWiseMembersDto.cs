﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public class FeeWiseMembersDto 
    {
        public string MemberName { get; set; }
        public long? MemberId { get; set; }
        public string VoucherNo { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public double CollectedAmount { get; set; }
        public string Year { get; set; }
        public string FeeTypeName { get; set; }

    }
}
