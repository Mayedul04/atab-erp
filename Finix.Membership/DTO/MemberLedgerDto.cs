﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
   public  class MemberLedgerDto
    {
        public long MemberId { get; set; }
        public string MemberName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime Todate { get; set; }
        public decimal Balance { get; set; }
        public ICollection<FeeCollectionTranDto> Collections { get; set; }
    }
}
