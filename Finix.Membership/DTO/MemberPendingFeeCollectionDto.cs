﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class MemberPendingFeeCollectionDto
    {
        public long MemberId { get; set; }     
        public string MemberNo { get; set; }
        public string NameOfOrganization { get; set; }
        public string Phone { get; set; }
        public List<MemberDetailsFeeWiseDto> PendingFeeList { get; set; }

    }
}
