﻿using Finix.Membership.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.DTO
{
    public class FeeTypeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public bool Mandatory { get; set; }
        public bool IsQuantitive { get; set; }
        public bool AccountReceivable { get; set; }
        public int RecuringType { get; set; }
        public int CompanyProfileId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public bool FeeCheck { get; set; }
        public bool Exists { get; set; }
    }
}
