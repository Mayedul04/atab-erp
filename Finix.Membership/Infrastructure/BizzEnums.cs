﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure
{

    public enum RoleEnum { Admin = 3, SuperUser = -2 }
    public enum LCType { None = 0, Irecoverable = 1 }
    public enum BankAccountType { None = 0, Savings = 1, Current = 2, STD }
    
    public enum CompanyType { Company = 1, Branch = 2, Project = 3 }
    public enum Course
    {
        [Display(Name = "Diploma in Travel & Tourism")]
        Diploma_in_Travel_and_Tourism = 1,
        [Display(Name = "Certification Course on Reservation")]
        Certification_Course_on_Reservation = 2,
        [Display(Name = "IATA BSP Link Operation")]
        IATA_BSP_Link_Operation = 3,
        [Display(Name = "Re-Exam Fee")]
        Re_Exam_Fee = 3,
        [Display(Name = "ID Card")]
        ID_Card = 4
    }
    public enum CurrencyType { BDT = 1, USD = 2 }
    public enum PurchaseOrderStatus { Ordered = 1, PartialReceived, Received, PartialCancelled, Cancelled }
    public enum TranType { Receive = 1, Return = 2, Cancel = 3 }
    //public enum AccTranType { Receive = 1, Payment = 2, BankTran = 3, Adjustment = 4 }
    public enum StockTranType { Receive = 1, Return = 2, Tranfer = 3, Disburse = 4, Damage = 5, Wastage = 6  }
    public enum Gender { Male = 1, Female = 2 }
    public enum MaritalStatus { Married = 1, UnMarried = 2, Other }
    public enum EmploymentStatus { Permanent = 1, Contractual = 2, Temporary = 3 }
    public enum BloodGroup { O_Positive = 1, O_Negative, A_Positive, A_Negative, AB_Positive, AB_Negetive, B_Positive, B_Negative }
    public enum Religion { Islam = 1, Hindu = 2, Buddist, Christian, Other }
    public enum OfficeType { HO = 1, Regional, Area }
    public enum UnitType { Wing = 1, Dept, Section }
    public enum EmployeeType { Contractual, Permanent }
    public enum EducationLevel { SSC = 1, HSC = 2, Graduate = 3, PostGraduate = 4, Others = 5 }
    public enum ResidenceStatus { Resident = 1, Non_Resident = 2 }
    public enum HomeOwnership { Rented = 1, Own = 2, [Display(Name = "Family Owned")] Family = 3, [Display(Name = "Employer Owned")] Employer = 4, Others = 5 }
    public enum UpozilaOrThana { Upozilla = 1, Thana = 2 }
    public enum TrainingType { InHouse, External, Foreign }
    public enum DegreeLevel { PhD, Master, Batchelor, HSC, SSC, UnderSSC, MBA, Diploma, Other }
    public enum DegreeMajor { Science, Arts, Commerce, Humanities }
    public enum Workshift { Day, Night, Both }
    public enum UploadedFileType { EmployeePhoto, TrainingDocument }
    public enum LocationType { GeneralStructure, MarketStructure }
    /// <summary>
    ///  Outside BD: Division=State; District=City;
    /// </summary>
    public enum LocationLevel { Division = 1, District, Thana, Area }
    public enum LeaveApplicableTo { Male, Female, All }
    public enum LeaveApplicationStatus { Draft, Applied, ApprovedByDept, ApprovedByHr, Halted, Rejected, Forward }

    public enum CurrentYear { True = 1, False = 0 }

    public enum HolidayType
    {
        Public = 1,
        Festival = 2
    }
    public enum WFTypes { LeaveApplication, LoanApplication, Appraisal, PaymentVoucher }
    public enum WFStatus { Pending, Executed, Postponed, Deadlocked }
    public enum WFActorType { Initiator, Middleman, Finisher }
    public enum LeaveSupervisionType { Approved = 1, Cancelled, Modified, Forward, Applied }

    public enum AppraisalType
    {
        Regular = 1,
        Special = 1
    }
    public enum TourType { OfficialTour, CompanyTour, Other }
    public enum TourStatus { Planned, Ongoing, Completed, Cancelled, Halted }

    public enum AppraisalStatus
    {
        NotStarted = 1,
        Started = 2,
        Postponed = 3,
        Finished = 4,
        Cancelled = 5
    }
    public enum Month { January = 1, February, March, April, May, June, July, August, September, October, November, December }
    public enum SalaryItemType
    {
        Basic, Allowance, PF, OverTime, Loan, SalaryAdvance
    }
    public enum EmployeeHistoryEnum { transfer = 1, promotion, resignation, join }
    public enum SalaryProcessStatus { Draft, Edited, Finalized }
    public enum SalaryItemStatus { Active, Inactive }
    public enum GradeStepSalaryStatus { Active, Inactive }
    public enum SalaryItemContributionType { Add, Deduct, Other }
    public enum SourceOfInformation
    {
        Newspaper = 1,
        Website = 2,
        Email = 3,
        Friends = 4,
        Others = 5
    }
    public enum EmployeeStatus { Active, Inactive }

    public enum DepriciationType { decline = 0, Increase = 1 }
    public enum DepriciationModel { StraightLine = 1, DecliningBalance = 2, SumOfYears = 3 }
    public enum DemoSupplier { RFL = 100, Regal = 101, AkhtarFurniture = 102, General = 103 }
    public enum DemoEmployee { MrX = 200, MrY = 201, MsZ = 202, MrU = 203 }
    public enum ServiceType { Warranty = 1, Guaranty = 2 }

    public enum PointOfDepreciation { Manual = 1, Revaluation = 2, Dispose = 3, OnCreate = 4 }

    public enum ReportGroupBy
    {
        [Display(Name = "Category Name")]
        CategoryName = 1,
        [Display(Name = "Supplier")]
        SupplierName = 2,
        [Display(Name = "Depreciation Model")]
        ModelName = 3,
        [Display(Name = "EmployeeName")]
        EmployeeName = 4,
        [Display(Name = "Service Type")]
        ServiceTypeName = 5
    }


    public enum TransferType
    {
        [Display(Name = "Employee to Employee")]
        Employee = 1,
        [Display(Name = "Office to Office")]
        Office = 2,
        [Display(Name = "Building to Building")]
        Building = 3,
        [Display(Name = "Floor to Floor")]
        Floor = 4,
        [Display(Name = "Room to Room")]
        Room = 5,
        [Display(Name = "Office to Building")]
        OffBuild = 6,
        [Display(Name = "Office to Floor")]
        OffFloor = 7,
        [Display(Name = "Office to Room")]
        OffRoom = 8,
        [Display(Name = "Building to Floor")]
        BuildFloor = 9,
        [Display(Name = "Building to Room")]
        BuildRoom = 10,
        [Display(Name = "Floor to Room")]
        FloorRoom = 11
    }

    public enum LocationTier
    {
        [Display(Name = "Company")]
        Company = 1,
        [Display(Name = "Office")]
        Office = 2,
        [Display(Name = "Building")]
        Building = 3,
        [Display(Name = "Floor")]
        Floor = 4,
        [Display(Name = "Room")]
        Room = 5,
        [Display(Name = "Employee")]
        Employee = 6
    }
    public enum CategoryLevel
    {
        [Display(Name = "Primary")]
        Primary = 1,
        [Display(Name = "Secondary")]
        Secondary = 2

    }

    public enum AccountHeadRefType
    {
        [Display(Name = "Income")]
        Income = 1,
        [Display(Name = "Receivable")]
        Receivable = 2,
        [Display(Name = "Liability")]
        Liability = 3,
        [Display(Name = "Cash in Hand")]
        CashInHand = 4,
        [Display(Name = "Cash at Bank")]
        CashAtBank = 5,
        [Display(Name = "Income ATTI")]
        Income_ATTI = 6,
        [Display(Name = "Receivable ATTI")]
        Receivable_ATTI = 7
    }

    public enum Zone { Dhaka = 0, Chittagong = 1 ,Sylhet = 2}
    public enum OwnershipStatus { Incorporated = 0, Partnership = 1, Proprietorship = 2 ,None=3}
    public enum MembershipStatus { Rejected = -1, Pending = 0, Approved = 1 }
    public enum PayType
    {
        [Display(Name = "Cash")]
        Cash = 1,
        [Display(Name = "Cheque")]
        Cheque = 2,
        [Display(Name = "Pay Order")]
        PayOrder = 3
    }
    public enum Post
    {
        [Display(Name = "Chairman")]
        Chairman = 1,
        [Display(Name = "Vice Chairman")]
        ViceChairman = 2,
        [Display(Name = "Executive Vice Chairman")]
        ExctViceChairman = 3,
        [Display(Name = "Secretary General")]
        SecretaryGnrl = 4,
        [Display(Name = "Secretary")]
        Secretary = 5,
        [Display(Name = "Joint Secretary")]
        JointSecretary = 6,
        [Display(Name = "Deputy Secretary")]
        DeptSecretary = 7,
        [Display(Name = "Finance Secretary")]
        FinanceSecretary = 8,
        [Display(Name = "Public Relation Secretary")]
        PubRelSecretary = 9,
        [Display(Name = "Cultural Secretary")]
        CulturalSecretary = 10,
        [Display(Name = "Member")]
        Member = 11
    }
    public enum CertificateType
    {
        [Display(Name = "Membership Certificate")]
        Membership = 1,
        [Display(Name = "Renewal Certificate")]
        Renewal = 2,
        [Display(Name = "Diploma in Travel & Tourism Certificate")]
        Diploma_in_Travel_and_Tourism = 3,
        [Display(Name = "Certification Course on Reservation")]
        Certification_Course_on_Reservation = 4,
        [Display(Name = "IATA BSP Link Operation Certificate")]
        IATA_BSP_Link_Operation = 5
    }

    public enum CertificateStatus { UnUsed = 1, Issued = 2, Void = 3, Ruined = 4 }

    //committee
    public enum CommitteeType
    {
        [Display(Name = "Effective Committee")]
        EffectiveCommittee = 1,
        [Display(Name = "Dhaka Committee")]
        DhakaCommittee = 2,
        [Display(Name = "Sylhet Committee")]
        SylhetCommittee = 3,
        [Display(Name = "Chittagong Committee")]
        ChittagongCommittee = 4
    }

}