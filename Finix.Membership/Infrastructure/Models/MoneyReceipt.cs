﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class MoneyReceipt: Entity
    {
        public long? MemberId { get; set; }
        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
        public string MoneyReceiptNo { get; set; }
        public string AuthorizedPerson { get; set; }
        public string Description { get; set; }
        public string VoucherNo { get; set; }
        public virtual ICollection<MoneyReceiptDetails> MoneyReceiptDetails { get; set; }
        public decimal TotalCollectedAmount { get; set; }
    }
}
