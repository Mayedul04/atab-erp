﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Member")]
    public class Member : Entity
    {
        public string MemberNo { get; set; }
        public string NameOfOrganization { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public DateTime? EstablishmentDate { get; set; }
        public OwnershipStatus OwnershipStatus { get; set; }
        public MembershipStatus MembershipStatus { get; set; }
        public virtual ICollection<MemberBusinessCatagories> BusinessCategories { get; set; }
        public virtual ICollection<MemberAssociations> Associations { get; set; }
        public virtual ICollection<MemberChambers> Chambers { get; set; }
        public virtual ICollection<MemberPendingFee> MemberPendingFees { get; set; }
        public DateTime? LicenseDate { get; set; }
        public string TradeLicenseNo { get; set; }
        public string TradeLicenseFile { get; set; }
        public DateTime? RenewalDate { get; set; }
        public string RenewalNo { get; set; }
        public string RenewalFile { get; set; }
        public DateTime? RenewalIssueDate { get; set; }
        public DateTime? IncorporationDate { get; set; }
        public string IncorporationFile { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativeNID { get; set; }
        public string RepresentativePhotoFile { get; set; }
        public DateTime? DateOfReceived { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string MRNo { get; set; }
        public string PONo { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public Zone Zone { get; set; }
        public long? PresentAddressId { get; set; }
        [ForeignKey("PresentAddressId")]
        public virtual Address PresentAddress { get; set; }
        public long? ParmanentAddressId { get; set; }
        [ForeignKey("ParmanentAddressId")]
        public virtual Address ParmanentAddress { get; set; }

    }
}
