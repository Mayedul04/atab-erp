﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Trainee")]
    public class Trainee : Entity
    {
        public string TraineeNo { get; set; }
        public string Name { get; set; }
        public string FathersName { get; set; }
        public string MothersName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public Religion? Religion { get; set; }
        public string Occupation { get; set; }
        public string LastAcademicQualification { get; set; }
        public long? AddressId { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
        public virtual ICollection<TraineeCourses> Courses { get; set; }
        public SourceOfInformation? SourceOfInformation { get; set; }
        public string OtherSourceOfInformation { get; set; }
        public DateTime? DateOfAdmission { get; set; }
        public string Batch { get; set; }
        public string MRNo { get; set; }
        public decimal TotalFeeAmount { get; set; }
        public decimal TotalCollectedAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public string AccountsOfficer { get; set; }
        public string AdmissionOfficer { get; set; }
        public string UrlOfPhoto { get; set; }
        public string UrlOfSignature { get; set; }
        public string Result { get; set; }
    }

    [Table("TraineeCourses")]
    public class TraineeCourses : Entity
    {
        public long TraineeId { get; set; }
        [ForeignKey("TraineeId"), InverseProperty("Courses")]
        public virtual Trainee Trainee { get; set; }
        public long Course { get; set; }
        [ForeignKey("Course")]
        public virtual FeeType Courses { get; set; }
        public decimal CourseFee { get; set; }
        public decimal CollectedAmount { get; set; }
        public string MRNo { get; set; }

        public DateTime? EffectiveDate { get; set; }
    }
}
