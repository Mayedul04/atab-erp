﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("FeeCollectionTran")]
    public class FeeCollectionTran : Entity
    {
        public long? PendingFeeId { get; set; }
        [ForeignKey("PendingFeeId")]
        public virtual MemberPendingFee PendingFee { get; set; }
        public long? MemberId { get; set; }
        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
        public long? FeeTypeId { get; set; }
        [ForeignKey("FeeTypeId")]
        public virtual FeeType FeeType { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string Year { get; set; }
        public string Bank { get; set; }
        public string ChequeNo { get; set; }
        public string MoneyReceiptNo { get; set; }
        public PayType PayType { get; set; }
        public decimal CollectedAmount { get; set; }
    }
}
