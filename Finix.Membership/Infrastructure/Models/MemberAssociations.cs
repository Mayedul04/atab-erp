﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class MemberAssociations :Entity
    {
        public long? MemberId { get; set; }
        [ForeignKey("MemberId"), InverseProperty("Associations")]
        public virtual Member Member { get; set; }
        public long? AssociationId { get; set; }
        [ForeignKey("AssociationId")]
        public virtual Association Association { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
