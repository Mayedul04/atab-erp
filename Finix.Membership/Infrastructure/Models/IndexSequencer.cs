﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("IndexSequencer")]
    public class IndexSequencer : Entity
    {
        public long OfficeId { get; set; }
        public string MemberNo { get; set; }
        public string TraineeNo { get; set; }
        public string MoneyReceiptNo { get; set; }
        public string BankMoneyReceiptNo { get; set; }
        public string AdvanceSlipNo { get; set; }
        public string ChallanNo { get; set; }
        public string InvoiceNo { get; set; }

    }
}