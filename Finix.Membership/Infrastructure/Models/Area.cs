﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Area")]
    public class Area : Entity
    {
        public string Name { get; set; }
        public long? ThanaId { get; set; }
        [ForeignKey("ThanaId")]
        public virtual Thana Thana { get; set; }
    }
}
