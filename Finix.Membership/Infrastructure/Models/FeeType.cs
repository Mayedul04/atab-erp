﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("FeeType")]
    public class FeeType : Entity
    {
        public string Name { get; set; }
        public bool Mandatory { get; set; }
        public bool AccountReceivable { get; set; }
        public bool IsQuantitive { get; set; }
        public int RecuringType { get; set; }
        public decimal Amount { get; set; }
        public int CompanyProfileId { get; set; }
    }
}
