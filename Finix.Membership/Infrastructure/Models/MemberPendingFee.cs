﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class MemberPendingFee :Entity
    {
        public long? MemberId { get; set; }
        [ForeignKey("MemberId"), InverseProperty("MemberPendingFees")]
        public virtual Member Member {get;set;}
        public long? FeeTypeId { get; set; }
        [ForeignKey("FeeTypeId")]
        public virtual FeeType FeeType { get; set; }
        public decimal Amount { get; set; }
        public bool CollectionStatus { get; set; }
        public string Remarks { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }
}
