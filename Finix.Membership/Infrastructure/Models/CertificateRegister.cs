﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
   public class CertificateRegister :Entity
    {
        public string CertificateName { get; set; }
        public CertificateType CertificateType { get; set; }
        public long CompanyProfileId { get; set; }
        public string Prefix { get; set; }
        public string StartingNumber { get; set; }
        public string EndingNumber { get; set; }
        public long TotalPages { get; set; }
        public DateTime BookIssueDate { get; set; } 
        public ICollection<CertificateRegisterDetail> Details { get; set; }
    }
}
