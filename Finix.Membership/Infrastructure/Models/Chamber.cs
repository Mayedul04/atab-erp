﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Chamber")]
    public class Chamber : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
