﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Owner")]
    public class Owner : Entity
    {
        public string Name { get; set; }
        public string Designation { get; set; }
        public string NID { get; set; }
        public string Phone { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string ResidentPhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public bool IsContactPerson { get; set; }
        public string Photo { get; set; }
        public long? MemberId { set; get; }
        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }
    }
}
