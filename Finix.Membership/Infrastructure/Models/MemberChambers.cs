﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class MemberChambers : Entity
    {
        public long? MemberId { get; set; }
        [ForeignKey("MemberId"), InverseProperty("Chambers")]
        public virtual Member Member { get; set; }
        public long? ChamberId { get; set; }
        [ForeignKey("ChamberId")]
        public virtual Chamber Chamber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
