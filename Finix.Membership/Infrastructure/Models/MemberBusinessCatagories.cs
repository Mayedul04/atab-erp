﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class MemberBusinessCatagories : Entity
    {
        public long? MemberId { get; set; }
        [ForeignKey("MemberId"), InverseProperty("BusinessCategories")]
        public virtual Member Member { get; set; }
        public long? BusinessCategoryId { get; set; }
        [ForeignKey("BusinessCategoryId")]
        public virtual BusinessCategory BusinessCategory { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
