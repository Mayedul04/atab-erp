﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("StockPosition")]
    public class StockPosition : Entity
    {
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public long OfficeId { get; set; }
        public decimal Balance { get; set; }
    }
    [Table("Product")]
    public class Product : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
