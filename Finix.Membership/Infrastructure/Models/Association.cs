﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("Association")]
    public class Association : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
