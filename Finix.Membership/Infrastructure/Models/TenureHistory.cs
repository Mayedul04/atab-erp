﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    public class TenureHistory :Entity
    {
        public long MemberCommitteId { get; set; }
        [ForeignKey("MemberCommitteId"),InverseProperty("TenureHistoryDetails")]
        public virtual ATABCommittee ATABCommittee { get; set; }
        public Zone Zone { get; set; }
        public Post Post { get; set; }
        public CommitteeType CommitteeType { get; set; }
        public DateTime? JoiningDate { get; set; }
        public DateTime? EndOfTenure { get; set; }

    }
}
