﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
    [Table("StockTran")]
    public class StockTran : Entity
    {
        public StockTranType StockTranType { get; set; }
        public long FromOfficeId { get; set; }
        public long ToOfficeId { get; set; }
        public string ChallanNo { get; set; }
        public DateTime TranDate { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal TotalPrice { get; set; }
        public string Remarks { get; set; }
        public virtual ICollection<StockTranDetail> Details { get; set; }
    }
    [Table("StockTranDetail")]
    public class StockTranDetail : Entity
    {
        public long StockTranId { get; set; }
        [ForeignKey("StockTranId"), InverseProperty("Details")]
        public virtual StockTran StockTran { get; set; }
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }

    }
}
