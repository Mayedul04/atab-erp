﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
   public class TraineeMoneyReceipt : Entity
    {
        public long? TraineeId { get; set; }
        [ForeignKey("TraineeId")] 
        public virtual Trainee Trainee { get; set; }
        public string MoneyReceiptNo { get; set; }
        public string AuthorizedPerson { get; set; }
        public string VoucherNo { get; set; }
        public string Description { get; set; }
        public virtual ICollection<TraineeMoneyReceiptDetails> MoneyReceiptDetails { get; set; }
        public decimal TotalCollectedAmount { get; set; }
    }
}
