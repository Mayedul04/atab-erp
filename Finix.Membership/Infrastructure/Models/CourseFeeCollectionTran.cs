﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
   public class CourseFeeCollectionTran:Entity
    {
        public long? TraineeId { get; set; }
        [ForeignKey("TraineeId")]
        public virtual Trainee Trainee { get; set; }
        public long? PendingFeeId { get; set; }
        [ForeignKey("PendingFeeId")]
        public virtual TraineeCourses PendingFee { get; set; }

        public long Course { get; set; }
        [ForeignKey("Course")]
        public virtual FeeType Courses { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ForYear { get; set; }
        public string Bank { get; set; }
        public string MoneyReceiptNo { get; set; }
        public string ChequeNo { get; set; }
        public PayType PayType { get; set; }
        public decimal CollectedAmount { get; set; }
    }
}
