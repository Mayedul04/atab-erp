﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
  public  class CertificateRegisterDetail :Entity
    {
        public string CertificateNo { get; set; }
        public long CertificateRegisterId { get; set; }
        public CertificateStatus CertificateStatus { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public long? IssuedBy { get; set; }
        public string IssuedPerson { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long? MemberNo { get; set; }
        [ForeignKey("MemberNo")]
        public virtual Member Member { get; set; }
        public long? TraineeNo { get; set; }
        [ForeignKey("TraineeNo")]
        public virtual Trainee Trainee { get; set; }
        [ForeignKey("CertificateRegisterId"), InverseProperty("Details")]
        public virtual CertificateRegister CertificateRegister { get; set; }
    }
}
