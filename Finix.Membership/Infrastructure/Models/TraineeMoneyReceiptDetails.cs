﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure.Models
{
   public class TraineeMoneyReceiptDetails :Entity
    {
        public long MoneyReceiptId { get; set; }
        [ForeignKey("MoneyReceiptId"), InverseProperty("MoneyReceiptDetails")]
        public virtual TraineeMoneyReceipt MoneyReceipt { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public string Year { get; set; }
        public string ChequeNo { get; set; }
        public PayType PayType { get; set; }
        public decimal CollectedAmount { get; set; }
    }
}
