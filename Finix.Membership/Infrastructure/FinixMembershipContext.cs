﻿using Finix.Membership.Infrastructure.Models;
using Finix.Membership.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.Membership.Infrastructure
{
    public interface IFinixMembershipContext
    {
    }
    public partial class FinixMembershipContext : DbContext, IFinixMembershipContext
    {
        #region ctor
        static FinixMembershipContext()
        {
            Database.SetInitializer<FinixMembershipContext>(null);
        }

        public FinixMembershipContext()
            : base("Name=FinixMembershipContext")
        {
            Database.SetInitializer(new FinixMembershipContextInitializer());
        }
        #endregion

        #region Models

        public DbSet<Association> Associations { get; set; }
        public DbSet<Chamber> Chambers { get; set; }
        public DbSet<Member> Members { get; set; }
        //public DbSet<FeeCollectionSummary> FeeCollectionSummaries { get; set; }
        public DbSet<FeeCollectionTran> FeeCollectionTrans { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<FeeType> FeeTypes { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Thana> Thanas { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<BusinessCategory> BusinessCategories { get; set; }
        public DbSet<IndexSequencer> IndexSequencers { get; set; }
        public DbSet<MemberPendingFee> MemberFeeLists { get; set; }
        public DbSet<Trainee> Trainee { get; set; }
        public DbSet<TraineeCourses> TraineeCourses { get; set; }
        public DbSet<MoneyReceipt> MoneyReceipts { get; set; }
        public DbSet<MoneyReceiptDetails> MoneyReceiptDetails { get; set; }
        public DbSet<TraineeMoneyReceipt> TraineeMoneyReceipts { get; set; }
        public DbSet<TraineeMoneyReceiptDetails> TraineeMoneyReceiptDetails { get; set; }
        public DbSet<CourseFeeCollectionTran> CourseFeeCollectionTran { get; set; }
        public DbSet<AccGroupRefTypeMapping> AccGroupRefTypeMapping { get; set; }
        public DbSet<AccountsMapping> AccountsMapping { get; set; } //ATABCommittee
        public DbSet<ATABCommittee> ATABCommittees { get; set; }
        public DbSet<TenureHistory> TenureHistories { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<StockPosition> StockPosition { get; set; }
        public DbSet<StockTran> StockTran { get; set; }
        public DbSet<StockTranDetail> StockTranDetail { get; set; }
        public DbSet<CertificateRegister> CertificateRegister { get; set; }
        public DbSet<CertificateRegisterDetail> CertificateRegisterDetail { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            FluentConfigurator.ConfigureGenericSettings(modelBuilder);
            FluentConfigurator.ConfigureMany2ManyMappings(modelBuilder);
            FluentConfigurator.ConfigureOne2OneMappings(modelBuilder);
        }
    }

    internal class FinixMembershipContextInitializer : CreateDatabaseIfNotExists<FinixMembershipContext> //CreateDatabaseIfNotExists<CardiacCareContext>
    {
        protected override void Seed(FinixMembershipContext context)
        {
            var seedDataPath = MembershipSystem.SeedDataPath;
            var dropDb = MembershipSystem.DropDB;
            if (string.IsNullOrWhiteSpace(seedDataPath))
                return;
            var folders = Directory.GetDirectories(seedDataPath).ToList().OrderBy(x => x);
            var msg = "";
            bool error = false;
            try
            {
                foreach (var folder in folders)
                {
                    msg += string.Format("processing for: {0}{1}", folder, Environment.NewLine);

                    var fileDir = Path.Combine(new[] { seedDataPath, folder });
                    var sqlFiles = Directory.GetFiles(fileDir, "*.sql").OrderBy(x => x).ToList();
                    foreach (var file in sqlFiles)
                    {
                        try
                        {
                            msg += string.Format("processing for: {0}{1}", file, Environment.NewLine);
                            context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                            msg += string.Format("Done....{0}", Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            error = true;
                            msg += string.Format("Failed!....{0}", Environment.NewLine);
                            msg += string.Format("{0}{1}", ex.Message, Environment.NewLine);
                        }
                    }
                }
                if (error)
                {
                    throw new Exception(msg);
                }
                base.Seed(context);
            }
            catch (Exception ex)
            {
                msg = "Error Occured while inserting seed data" + Environment.NewLine;
                if (dropDb)
                {
                    context.Database.Delete();
                    msg += ("Database is droped" + Environment.NewLine);
                }
                var errFile = seedDataPath + "\\seed_data_error.txt";
                msg += ex.Message;
                File.WriteAllText(errFile, msg);

            }
        }

    }
}
