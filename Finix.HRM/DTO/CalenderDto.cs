﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class CalendarDto
    {
        public long Id { get; set; }
        public long Year { get; set; }
        public long Month { get; set; }
        public bool? IsHoliday { get; set; }
        public DateTime? PresentDate { get; set; }
        public long HolidayType { get; set; }
        //public string WeekendOne { get; set; }
        //public string Weekendtwo { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
