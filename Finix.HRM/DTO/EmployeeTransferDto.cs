﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class EmployeeTransferDto
    {
        public EmployeeTransferDto()
        { }

        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationName { get; set; }
        public UnitType UnitType { get; set; }
        public string UnitTypeName { get; set; }
        //public long OfficeUnitId { get; set; }
        public long? OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
        public long? OfficeLayerId { get; set; }
        public long? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long EmployeeTypeId { get; set; }
        public string EmployeeTypeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? GradeStepId { get; set; }
        public string GradeStepName { get; set; }
        public int JobStatus { get; set; }
        public string JobStatusName { get; set; }
        public string Remarks { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public int EmployeeHistoryId { get; set; }
    }
}
