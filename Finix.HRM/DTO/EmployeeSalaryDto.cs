﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class EmployeeSalaryDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long GradeId { get; set; }
        public string GradeName { get; set; }
        public decimal Salary { get; set; }
    }
}