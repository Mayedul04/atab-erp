﻿using Finix.HRM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class AccHeadMappingDto
    {
        public long? Id { get; set; }
        public long? RefId { get; set; }
        public string RefName { get; set; }
        public AccountHeadRefType RefType { get; set; }
        public string RefTypeName { get; set; }
        public string AccountHeadCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
