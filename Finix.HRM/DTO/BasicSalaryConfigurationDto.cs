﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class BasicSalaryConfigurationDto
    {
        public long? Id { get; set; }
        public decimal WeekendOvertimeByBasicPer { get; set; }
        public decimal NationalHolidayOtByBasicPer { get; set; }
        public int MonthDays { get; set; }
        public DateTime OfficeInTime { get; set; }
        public DateTime OfficeOutTime { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
