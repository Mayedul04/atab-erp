﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class OfficeLayerDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
       // public List<Office> Offices { get; set; }
    }

    public class OfficeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long OfficeLayerId { get; set; }
        public string OfficeLayerName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
    }

    public class OfficeUnitDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int UnitType { get; set; }
        public string UnitTypeName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public bool? Checked { get; set; }
    }

    public class OfficePositionDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long OfficeLayerId { get; set; }
        public long? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long? DefaultDesignationId { get; set; }
        public string DefaultDesignationName { get; set; }
        public long PositionWeight { get; set; }
    }

    public class OrganogramDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long DesignationId { get; set; }
        public string DesignationName { get; set; }
        public long OfficeLayerId { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long UnitType { get; set; }
        public long OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
        public long PositionId { get; set; }
        public string PositionName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public long? GradeId { get; set; }
        public string GradeName { get; set; }
        public bool? Pick { get; set; }
    }

    public class OfficeUnitSettingDto
    {
        public long Id { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long UnitType { get; set; }
        public long OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
        public long? ParentUnitId { get; set; }
        public string ParentUnitName { get; set; }
        public int Sl { get; set; }
    }
}
