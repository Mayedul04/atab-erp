﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class LeaveApplicationDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string OfficeUnitName { get; set; }
        public string Range { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public double TotalDays { get; set; }
        public long EntitledLeaveDays { get; set; }
        public long LeaveTaken { get; set; }
        public long LeaveTypeId { get; set; }
        public string LeaveTypeName { get; set; }
        public string Description { get; set; }
        public int CurrentLeaveStatus { get; set; }
        public string CurrentLeaveStatusName { get; set; }
        public long? SubstitutorId { get; set; }
        public string SubstitutorName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public long ParentId { get; set; }
        public bool? Pick { get; set; }
        public long? SeniorId { get; set; }
    }

    public class LeaveSupervisionDto
    {
        public long Id { get; set; }
        public long LeaveApplicationId { get; set; }
        public string EmployeeName { get; set; }
        public long SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int ActionType { get; set; }
        //public DateTime ModifiedFromDate { get; set; }
        //public DateTime ModifiedToDate { get; set; }
        //public long ModifiedTotalDays { get; set; }
        //public long ModifiedLeaveTypeId { get; set; }
        public string SupervisorComments { get; set; }
        //public List<string> LeaveComments { get; set; }
    }

    public class EmployeeLeaveStatusDto
    {
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DesignationName { get; set; }
        public string OfficeUnitName { get; set; }
        public string EmployeeTypeName { get; set; }
    }

    public class LeaveStatusDto
    {

        public string LeaveTypeName { get; set; }
        public int LeaveDays { get; set; }
        public int MaxCarryForward { get; set; }
        public int Enjoyed { get; set; }
        public int Processing { get; set; }
        public int Remaining { get; set; }
    }
}
