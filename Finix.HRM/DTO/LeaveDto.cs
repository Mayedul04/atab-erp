﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class LeaveTypeDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int LeaveDays { get; set; }
        public int MaxCarryForward { get; set; }
        public int LeaveApplicableTo { get; set; }
        public string LeaveApplicableToName { get; set; }

    }



}