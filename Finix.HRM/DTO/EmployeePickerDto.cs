﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class EmployeePickerDto
    {
        public EmployeePickerDto()
        { }
        public long? Id { get; set; }
        public long? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationName { get; set; }
        public long? GradeId { get; set; }
        public string GradeName { get; set; }
        public UnitType? UnitType { get; set; }
        public long? OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
        public long? OfficeLayerId { get; set; }
        public long? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public int? GenderId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool? Pick { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }
}
