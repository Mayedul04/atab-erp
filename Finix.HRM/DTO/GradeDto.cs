﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;

namespace Finix.HRM.DTO
{
    public class GradeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<Designation> DesignationList { get; set; }
    }

    public class GradeStepDto
    {
        public long Id { get; set; }
        public long GradeId { get; set; }
        public string GradeName { get; set; }
        public string StepName { get; set; }
        public int StepNo { get; set; }
        public string Description { get; set; }
    }


}
