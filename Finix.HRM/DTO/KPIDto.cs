﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class KPIDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        //public string DisplayName { get; set; }
        public long KPIGroupId { get; set; }
        public string KPIGroupName { get; set; }
        public long KPITypeId { get; set; }
        public String KPITypeName { get; set; }
        public int? KpiDisplayOrder { get; set; }
        public int? KpiGroupDisplayOrder { get; set; }
        public bool IsKPISelected { get; set; }
        public int KpiWeight { get; set; }
        public int? KpiGroupWeight { get; set; }
        public int KPIMarks { get; set; }
        public int KPIMarksEvaluator1 { get; set; }
        public int KPIMarksEvaluator2 { get; set; }
        public long PerformanceAppraisalId { get; set; }
    }

    public class KPIGroupDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int KPIGroupWeight { get; set; }
        public bool IsGroupSelected { get; set; }
        public List<KPIDto> KPIList { get; set; }
    }
    public class ApraisalInterfaceKPIDto
    {
        public KPIGroupDto KPIGroupDto { get; set; }

        public List<KPIDto> KPIDtoList { get; set; }

    }
}
