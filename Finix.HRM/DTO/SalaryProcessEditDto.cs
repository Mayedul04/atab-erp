﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class SalaryProcessEditDto
    {
        public long Id { get; set; }
        public string OfficeName { get; set; }
        public string UnitName { get; set; }
        public long EmpId { get; set; }
        public string EmpName { get; set; }
        public long Year { get; set; }
        public string Month { get; set; }
        public long SalaryItemId { get; set; }
        public string SalaryItemName { get; set; }
        public decimal CalculatedValue { get; set; }
        public decimal EditedValue { get; set; }
        public string Comments { get; set; }
    }
}
