﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class DesignationKPIDto
    {
        public long Id { get; set; }
        //public string Name { get; set; }
        public long GradeId { get; set; }

        public long DesignationId { get; set; }
        public string DesignationName { get; set; }
        public long KPITypeId { get; set; }

        public long KPIId { get; set; }
        public string KPIName { get; set; }
        public double Value { get; set; }
    }
}
