﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class SalaryItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsTaxable { get; set; }
        public decimal TaxPercent { get; set; }
        public SalaryItemType SalaryItemType { get; set; }
        public string SalaryItemTypeName { get; set; }
        public int DisplayOrder { get; set; }
        public bool IspercentOfBasic { get; set; }
        public int Ceiling { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int ContributionType { get; set; }
        public string ContributionTypeName { get; set; }
        public bool? Pick { get; set; }
        public bool IsEditable { get; set; }
    }

    public class GradeStepSalaryItemDto
    {
        public long Id { get; set; }
        public long GradeId { get; set; }
        public string GradeName { get; set; }
        public long GradeStepId { get; set; }
        public string GradeStepName { get; set; }
        public long SalaryItemId { get; set; }
        public string SalaryItemName { get; set; }
        public bool IsParcentOfBasic { get; set; }
        public long FiscalYearId { get; set; }
        public int FiscalYear { get; set; }
        public int Month { get; set; }
        public decimal? Amount { get; set; }
        public decimal? SalaryItemUnit { get; set; }
        public int Status { get; set; }
    }

    public class SalaryComponentDto
    {
        public bool Checked { get; set; }
        public string DisplayText { get; set; }
        public string Key { get; set; }
        public string StatusText { get; set; }
        public int StatusVal { get; set; }
    }

}
