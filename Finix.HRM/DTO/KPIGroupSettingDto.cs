﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class KPIGroupSettingDto
    {
        public long Id { get; set; }
        public long AppraisalId { get; set; }
        public string AppraisalName { get; set; }
        public long KPIGroupId { get; set; }
        public string KPIGroupName { get; set; }
        public long KPIId { get; set; }
        public string KPIName { get; set; }
        public int DisplayOrder { get; set; }
        public int Weight { get; set; }
    }
}
