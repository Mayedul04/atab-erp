﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class DesignationDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long GradeId { get; set; }
        public string GradeName { get; set; }
    }
}