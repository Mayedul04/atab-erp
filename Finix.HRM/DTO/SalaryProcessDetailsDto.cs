﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class SalaryProcessDetailsDto
    {
        public long Id { get; set; }
        public long SalaryProcessId { get; set; }
        public long EmployeeId { get; set; }
        public long SalaryItemId { get; set; }
        public decimal ProcessedAmount { get; set; }
        public decimal EditedAmount { get; set; }
        public long EditorId { get; set; }
        public string Comments { get; set; }
        public int LateNumOfDays { get; set; }
    }
}
