﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{

    public class KPITypeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        //public string DisplayName { get; set; }
        public string Description { get; set; }

    }

}
