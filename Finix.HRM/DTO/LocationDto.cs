﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class LocationDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int LocationType { get; set; }
        public int LocationLevel { get; set; }
        public string LocationTypeName { get; set; }
        public string LocationLevelName { get; set; }
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public long? ParentId { get; set; }
        public string ParentLocation { get; set; }
    }

    public class PostOfficeDto
    {
        public long Id { get; set; }
        public long Code { get; set; }
        public string Name { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
    }

    public class AddressDto
    {
        public long Id { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        //public long CountryId { get; set; }
        public long PostalCode { get; set; }
        public long DivisionId { get; set; }
        public long DistrictId { get; set; }
        public long ThanaId { get; set; }
        public long AreaId { get; set; }
        public int PostOfficeId { get; set; }
        public string PostOfficeName { get; set; }
    }
}
