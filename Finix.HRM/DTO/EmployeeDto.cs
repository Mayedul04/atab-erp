﻿using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Finix.HRM.DTO
{

    public class EmployeeDto
    {
        public EmployeeDto()
        {
            BasicInfo = new EmpBasicInfoDto();
            ContactInformation = new ContactInformationDto();
            Education = new DegreeInfoDto();
            TrainingInformation = new TrainingInformationDto();
            EmploymentHistory = new EmploymentHistoryDto();
            JoiningInformation = new JoiningInformationDto();
            UserInformation = new UserInformationDto();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public EmpBasicInfoDto BasicInfo { get; set; }
        public ContactInformationDto ContactInformation { get; set; }
        public DegreeInfoDto Education { get; set; }
        public TrainingInformationDto TrainingInformation { get; set; }
        public EmploymentHistoryDto EmploymentHistory { get; set; }
        public UserInformationDto UserInformation { get; set; }
        public JoiningInformationDto JoiningInformation { get; set; }

    }

    public class EmpBasicInfoDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long EmpCode { get; set; }
        public Designation Designation { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationName { get; set; }
        public string FatherFirstName { get; set; }
        public string FatherLastName { get; set; }
        public string MotherFirstName { get; set; }
        public string MotherLastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Gender { get; set; }
        public string GenderName { get; set; }
        public int? MaritalStatus { get; set; }
        public string MaritalStatusName { get; set; }
        public int? Religion { get; set; }
        public string ReligionName { get; set; }
        public int? BloodGroup { get; set; }
        public string BloodGroupName { get; set; }
        public Country Country { get; set; }
        public long? CountryId { get; set; }
        public string CountryName { get; set; }
        public string NID { get; set; }
        public DateTime JoiningDate { get; set; }
        public int? EmployeeType { get; set; }
        public string EmployeeTypeName { get; set; }
        public int? EmployeeStatus { get; set; }
        public string EmployeeStatusName { get; set; }
        public long? TrainingId { get; set; }
        public string TrainingName { get; set; }
        public long? DegreeId { get; set; }
        public string DegreeName { get; set; }
        public long? EmploymentHistoryId { get; set; }
        public string EmploymentHistoryName { get; set; }
        public long? OfficeId { get; set; }
        public string ContactNo { get; set; }
        public string Photo { get; set; }
    }
    public class ContactInformationDto
    {
        public long Id { get; set; }
        public AddressDto PresentAddress { get; set; }
        public string PresentAddressDetails { get; set; }
        public AddressDto ParmanentAddress { get; set; }
        public string ParmanentAddressDetails { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string EmergencyContactPerson { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string EmergencyContactRelation { get; set; }
        public long EmployeeId { get; set; }
    }
    public class EmployeeEducationDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public long DegreeId { get; set; }
        public string DegreeName { get; set; }
        public long InstituteId { get; set; }
        public string InstituteName { get; set; }
        public long PassingYear { get; set; }
        public double Result { get; set; }
        public string Summary { get; set; }
    }
    public class TrainingInformationDto
    {
        public long Id { get; set; }
        public long TrainingVendorId { get; set; }
        public string TrainingVendorName { get; set; }
        public long TrainingId { get; set; }
        public string TrainingName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TrainingHours { get; set; }
        public long EmployeeId { get; set; }
    }

    public class JoiningInformationDto
    {
        public long Id { get; set; }
        public DateTime? JoiningDate { get; set; }
        public long? CompanyProfileId { get; set; }
        public long? OfficeId { get; set; }
        public long? OfficeUnitId { get; set; }
        public long? GradeId { get; set; }
        public long? DesignationId { get; set; }
        public long EmployeeType { get; set; }
        public long WorkShift { get; set; }
        public long EmployeeId { get; set; }
        public long? GradeStepId { get; set; }

    }

    public class UserInformationDto
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsEmployee { get; set; }
        public long EmployeeId { get; set; }

    }
    public class DepartmentDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class EmployeeAttendanceDto
    {
        public long Id { get; set; }
        public long EmpId { get; set; }
        public long EmpCode { get; set; }
        public string EmpName { get; set; }
        public string DesignationName { get; set; }
        public DateTime Date { get; set; }
        public DateTime InTime { get; set; }
        public DateTime OutTime { get; set; }
        public DateTime OfficeInTime { get; set; }
        public DateTime OfficeOutTime { get; set; }
        public int Late { get; set; }
        public int OverTime { get; set; }
    }

    public class EmploymentHistoryDto
    {
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyBusiness { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string AreaofExperiences { get; set; }
        public string Responsibilities { get; set; }
        public string CompanyAddress { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long EmployeeId { get; set; }
    }

    //public class AddressDto
    //{
    //    public long Id { get; set; }
    //    public string AddressLine1 { get; set; }
    //    public string AddressLine2 { get; set; }
    //    public long PostalCode { get; set; }
    //    public string District { get; set; }
    //    public string Thana { get; set; }
    //    public string Area { get; set; }
    //}
}
