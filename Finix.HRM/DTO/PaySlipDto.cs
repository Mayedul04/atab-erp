﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class PaySlipDTO
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int? Gender { get; set; }
        public string GenderName { get; set; }
        public int? MaritalStatus { get; set; }
        public string MaritalStatusName { get; set; }
        public int? Religion { get; set; }
        public string ReligionName { get; set; }
        public int? BloodGroup { get; set; }
        public string NID { get; set; }
        public string PermanentAddress { get; set; }
        public string PresentAddress { get; set; }
        public long FiscalYearId { get; set; }
        public string FiscalYearName { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
        public string Month { get; set; }
        public DateTime ProcessDate { get; set; }
        public int Status { get; set; }
        public long SalaryItemId { get; set; }
        public string SalaryItemName { get; set; }
        public int ContributionType { get; set; }
        public string ContributionTypeName { get; set; }
        public decimal ProcessedAmount { get; set; }
        public decimal EditedAmount { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
