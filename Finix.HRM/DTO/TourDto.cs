﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class TourDto
    {
        public long Id { get; set; }
        public string TourName { get; set; }
        public int TourType { get; set; }
        public string TourTypeName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; }
        public int TourStatus { get; set; }
        public string TourStatusName { get; set; }
 
    }
    public class TourParticipantDto
    {
        public long Id { get; set; }
        public long TourId { get; set; }
        public string TourName { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? SubstitutorId { get; set; }
        public string SubstitutorName { get; set; }
        public string Remarks { get; set; }
    }
}
