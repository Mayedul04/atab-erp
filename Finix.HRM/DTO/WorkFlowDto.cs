﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class WFSettingDto
    {
        public long Id { get; set; }
        public string Name { get; set;}
        public int WFType { get; set; }
        public string WFTypeName { get; set; }
        public int TotalSteps { get; set; }
        public int TotalActiveWF { get; set; }
        public string Description { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long OfficeUnitId { get; set; }
        public string OfficeUnitName { get; set; }
    }

    public class WFStepDto
    {
        public long Id { get; set; }
        public long WFSettingId { get; set; }
        public string WFSettingName { get; set; }
        public int StepNo { get; set; }
        public long ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public int ExecutorType { get; set; }
        public string ExecutorTypeName {get; set;}
    }

    public class WorkFlowDto
    {
        public long Id { get; set; }
        public long WFSettingId { get; set; }
        public string WFSettingName { get; set; }
        public long WFStepId { get; set; }
        public int ObjId { get; set; }
        public int WFStatus { get; set; }
        public string WFStatusName { get; set; }
        public string Comments { get; set; }
    }
}
