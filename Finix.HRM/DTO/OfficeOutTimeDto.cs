﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class OfficeOutTimeDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long ApprovedById { get; set; }
        public string ApprovedByName { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public long? SubstituteId { get; set; }
        public string SubstituteName { get; set; }
    }

    public class OverTimeRuleDto
    {
        public long Id { get; set; }
        public long DesignationId { get; set; }
        public string DesignationName { get; set; }
        public bool IsIllegibleForOT { get; set; }
        public int RatePerHour { get; set; }
        public bool IsLateAttendanceMonitored { get; set; }
        public int DayForLateAttendance { get; set; }
        public bool IsLeaveEncashed { get; set; }
        public bool IsIllegibleForBonus { get; set; }
        public int BonusRatePerHour { get; set; }
    }
}
