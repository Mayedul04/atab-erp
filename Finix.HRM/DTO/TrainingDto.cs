﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class VendorDto
    {
        public long Id { get; set; }
        public string VendorName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public int PostOfficeId { get; set; }
        public long PostalCode { get; set; }
        //public string AddressLine1 { get; set; }
        //public string AddressLine2 { get; set; }
        // public long DivisionId { get; set; }
        public string DivisionName { get; set; }
        //public long DistrictId { get; set; }
        public string DistrictName { get; set; }
        //public long ThanaId { get; set; }
        public string ThanaName { get; set; }
        //public long? AreaId { get; set; }
        public string AreaName { get; set; }
    }

    public class TrainingDto
    {
        public long Id { get; set; }
        public string TrainingName { get; set; }
        public int TrainingType { get; set; }
        public string TrainingTypeName { get; set; }

    }
}
