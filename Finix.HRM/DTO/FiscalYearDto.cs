﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.DTO
{
    public class FiscalYearDto
    {
        public long Id { get; set; }
        public int Year { get; set; }
        public bool CurrentYear { get; set; }
        public DateTime QuarterOne { get; set; }
        public string QuarterOneTxt { get; set; }
        public DateTime QuarterTwo { get; set; }
        public string QuarterTwoTxt { get; set; }
        public DateTime QuarterThree { get; set; }
        public string QuarterThreeTxt { get; set; }
        public DateTime QuarterFour { get; set; }
        public string QuarterFourTxt { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
    }

    public class YearMonthForProcessDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
