﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;

namespace Finix.HRM.DTO
{
    public class AppraisalDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long FiscalYearId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartDateTxt { get; set; }
        public string EndDateTxt { get; set; }
        public AppraisalType AppraisalType { get; set; }
        public AppraisalStatus AppraisalStatus { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime LastSubmissionDate { get; set; }
        public DateTime LastE1Date { get; set; }
        public DateTime LastE2Date { get; set; }
        public string EffectiveDateTxt { get; set; }
        public string LastSubmissionDateTxt { get; set; }
        public string LastE1DateTxt { get; set; }
        public string LastE2DateTxt { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }

    }

    public class PerformanceAppraisalDto
    {
        public long Id { get; set; }
        public long DesignationId { get; set; }

        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public List<KPIGroupDto> KPIGroupList { get; set; }

    }

    public class EmployeeEvaluationDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationName { get; set; }
        public DateTime? JoiningDate { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public long SeniorId { get; set; }
        public string Remarks { get; set; }
        public string Recommendation { get; set; }
        public List<KPIGroupDto> KPIGroupList { get; set; }
    }

    public class EvaluationMasterDetailDto
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DesignationName { get; set; }
        public long EvaluationYear { get; set; }
        public int EvaluationStatus { get; set; }
        public string EvaluationStatusName { get; set; }
        public long? LastForwardedTo { get; set; }
        public long LastEvaluatedBy { get; set; }
        public string LastEvaluatedByName { get; set; }
        public string Remarks { get; set; }
        public string Recommendation { get; set; }
    }
    public class SuperDto
    {
        public long EvaluatorId { get; set; }
        public string EvaluatorName { get; set; }
        public double KPIMarks { get; set; }
    }
    public class EvaluationReportDto
    {
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public long? EmpCode { get; set; }
        public long DesignationId { get; set; }
        public string DesignationName { get; set; }
        public long EvaluationYear { get; set; }
        public List<SuperDto> Evaluations { get; set; }

    }

    public class EvaluationWeightSetupDto
    {
        public long Id { get; set; }
        public DateTime SettingDate { get; set; }
        public int SelfWeight { get; set; }
        public int Supervisor1Weight { get; set; }
        public int Supervisor2Weight { get; set; }
        public bool IsActive { get; set; }
    }
}
