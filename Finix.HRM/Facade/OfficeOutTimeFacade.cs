﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;


namespace Finix.HRM.Facade
{
    public class OfficeOutTimeFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        public List<OfficeOutTimeDto> GetAllOfficeOutTime()
        {
            var data = _service.GetAll<OfficeOutTime>().ToList();
            return Mapper.Map<List<OfficeOutTimeDto>>(data);
        }

        public ResponseDto SaveOfficeOutTime(OfficeOutTimeDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new OfficeOutTime();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<OfficeOutTime>(model.Id);


                    entity.EmployeeId = model.EmployeeId;
                    entity.ApprovedById = model.ApprovedById;
                    entity.FromTime = model.FromTime;
                    entity.ToTime = model.ToTime;
                    entity.SubstituteId = model.SubstituteId;

                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
                else
                {
                    entity = Mapper.Map<OfficeOutTime>(model);
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Appraisal Kpi Group Setting Saved Successfully";
            return response;
        }

        public ResponseDto DeleteOfficeOutTime(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<OfficeOutTime>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Appraisal KPI Group Deleted Successfully";
            return response;
        }

        public List<EmployeeDto> GetEmployees()
        {
            var data = _service.GetAll<Employee>();
            //var mapData = Mapper.Map<EmployeeDto>(data);
            var result =
                from em in data
                join pr in _service.GetAll<Person>() on em.PersonId equals pr.Id into extra
                from ext in extra.DefaultIfEmpty()
                select new EmployeeDto()
                {
                    Id = em.Id,
                    Name = ext.FirstName + " " + ext.LastName
                };
            return result.ToList();
        }

        public List<EmployeeAttendanceDto> GetTodaysAttendance()
        {
            var attendances = GenService.GetAll<EmployeeAttendance>().Where(x => x.Date == DateTime.Today);
            var data = attendances.Select(x => new EmployeeAttendanceDto
            {
                Id = x.Id,
                Date = x.Date,
                EmpId = x.EmpId,
                EmpName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                InTime = x.InTime,
                OutTime = x.OutTime,
                Late = x.Late,
                OverTime = x.OverTime
            }).ToList();
            return data;
        }

        public List<EmployeeAttendanceDto> GetEmployeeAttendances()
        {
            var attendances = GenService.GetAll<EmployeeAttendance>();
            //TimeSpan tt=new TimeSpan(09,00,00);
            List<EmployeeAttendanceDto> list = new List<EmployeeAttendanceDto>();
            foreach (var item in attendances)
            {
                EmployeeAttendanceDto empattendance = new EmployeeAttendanceDto();
                empattendance.Id = item.Id;
                empattendance.Date = item.Date;
                empattendance.EmpId = item.EmpId;
                empattendance.EmpName = item.Employee.Person.FirstName + " " + item.Employee.Person.LastName;
                empattendance.InTime = item.InTime;
                empattendance.OutTime = item.OutTime;
                
                empattendance.Late = item.Late;
                empattendance.OverTime = item.OverTime;
                list.Add(empattendance);
            }
           
            return list;
        }

        public Object GetInAndOutTime()
        {
            var InTime = GenService.GetAll<BasicSalaryConfiguration>().FirstOrDefault().OfficeInTime;
            var OutTime = GenService.GetAll<BasicSalaryConfiguration>().FirstOrDefault().OfficeOutTime;
            return new { OfficeInTime = InTime, OfficeOutTime = OutTime };
        }

        public Employee GetEmpByEmpCode(long EmpCode)
        {
            var emp = GenService.GetAll<Employee>().Where(x => x.EmpCode == EmpCode).FirstOrDefault();
            return emp;
        }


        public EmployeeAttendance GetAttendanceByEmpAndDate(long EmpId,DateTime dt) 
        {
            var att = GenService.GetAll<EmployeeAttendance>().Where(x => x.EmpId == EmpId && x.Date == dt).FirstOrDefault();
            return att;
        
        }

        public void SaveEmployeeAttendance(EmployeeAttendanceDto empattenddto)
        {
            if (empattenddto.Id > 0)
            {
                var empattend = GenService.GetById<EmployeeAttendance>(empattenddto.Id);
                empattend.EmpId = empattenddto.EmpId;
                empattend.Date = empattenddto.Date;
                empattend.InTime = empattenddto.InTime;
                empattend.OutTime = empattenddto.OutTime;
                empattend.Late = empattenddto.Late;
                empattend.OverTime = empattenddto.OverTime;
            }
            else
            {
                EmployeeAttendance att = new EmployeeAttendance();
                att.EmpId = empattenddto.EmpId;
                
                att.Date = empattenddto.Date;
                att.InTime = empattenddto.InTime;
                att.OutTime = empattenddto.OutTime;
                att.Late = empattenddto.Late;
                att.OverTime = empattenddto.OverTime;
                GenService.Save(att);
                //GenService.Save(new EmployeeAttendance
                //{
                //    EmpId = empattenddto.EmpId,
                //    Date = empattenddto.Date,
                //    InTime = empattenddto.InTime,
                //    OutTime = empattenddto.OutTime,
                //    Late = empattenddto.Late,
                //    OverTime = empattenddto.OverTime
                //});
            }
            GenService.SaveChanges();
        }

        public void DeleteEmployeeAttendance(int id)
        {
            GenService.Delete<EmployeeAttendance>(id);
            GenService.SaveChanges();
        }

        public List<OverTimeRuleDto> GetOverTimeRules()
        {
            var designations = GenService.GetAll<Designation>();
            var overtimerules = GenService.GetAll<SalaryConfiguration>();
            if (overtimerules.Count() == 0)
            {
                var data = designations.Select(x => new OverTimeRuleDto
                {
                    DesignationId = x.Id,
                    DesignationName = x.Name,
                    IsIllegibleForOT = false,
                    RatePerHour = 0,
                    IsLateAttendanceMonitored = false,
                    DayForLateAttendance = 0,
                    IsLeaveEncashed = false
                }).ToList();
                return data;
            }
            else
            {
                List<OverTimeRuleDto> list = new List<OverTimeRuleDto>();
                foreach (var item in designations)
                {

                    var des = GenService.GetAll<SalaryConfiguration>().Where(x => x.DesignationId == item.Id).FirstOrDefault();
                    if (des != null)
                    {
                        var dto = new OverTimeRuleDto();
                        dto.DesignationId = des.DesignationId;
                        dto.DesignationName = des.Designation.Name;
                        dto.IsIllegibleForOT = des.IsIllegibleForOT;
                        dto.RatePerHour = des.RatePerHour;
                        dto.IsLateAttendanceMonitored = des.IsLateAttendanceMonitored;
                        dto.DayForLateAttendance = des.DayForLateAttendance;
                        dto.IsLeaveEncashed = des.IsLeaveEncashed;
                        list.Add(dto);
                    }
                    else
                    {
                        var dto = new OverTimeRuleDto();
                        dto.DesignationId = item.Id;
                        dto.DesignationName = item.Name;
                        dto.IsIllegibleForOT = false;
                        dto.RatePerHour = 0;
                        dto.IsLateAttendanceMonitored = false;
                        dto.DayForLateAttendance = 0;
                        dto.IsLeaveEncashed = false;
                        list.Add(dto);
                    }

                }
                return list;
            }
        }


        public void SaveOverTimeRule(List<OverTimeRuleDto> list)
        {
            var overtimes = GenService.GetAll<SalaryConfiguration>();
            if (overtimes.Count()==0)
            {
                foreach (var item in list)
                {
                    SalaryConfiguration overtime = new SalaryConfiguration();
                    overtime.DesignationId = item.DesignationId;
                    overtime.IsIllegibleForOT = item.IsIllegibleForOT;
                    overtime.RatePerHour =item.RatePerHour;
                    overtime.IsLateAttendanceMonitored =item.IsLateAttendanceMonitored;
                    overtime.DayForLateAttendance =item.DayForLateAttendance;
                    overtime.IsLeaveEncashed = item.IsLeaveEncashed;
                    GenService.Save(overtime);
                }
            }
            else
            {

                foreach (var item in list)
                {
                    
                    var otrule = GenService.GetAll<SalaryConfiguration>().Where(x => x.DesignationId == item.DesignationId).FirstOrDefault();
                    if (otrule==null)
                    {
                        GenService.Save(new SalaryConfiguration
                        {
                            DesignationId=item.DesignationId,
                            IsIllegibleForOT=item.IsIllegibleForBonus,
                            RatePerHour=item.RatePerHour,
                            IsLateAttendanceMonitored=item.IsLateAttendanceMonitored,
                            DayForLateAttendance=item.DayForLateAttendance,
                            IsLeaveEncashed=item.IsLeaveEncashed
                        });
                    }
                    else
                    {
                        otrule.IsIllegibleForOT = item.IsIllegibleForOT;
                        otrule.RatePerHour = item.RatePerHour;
                        otrule.IsLateAttendanceMonitored = item.IsLateAttendanceMonitored;
                        otrule.DayForLateAttendance = item.DayForLateAttendance;
                        otrule.IsLeaveEncashed = item.IsLeaveEncashed;
                        GenService.Save(otrule);
                    }
                    //otrule.DesignationId = item.DesignationId;
                    
                }
            }
            GenService.SaveChanges();
        }
        public List<BasicSalaryConfigurationDto> GetBasicConfiguration()
        {
            var data = _service.GetAll<BasicSalaryConfiguration>().ToList();
            return Mapper.Map<List<BasicSalaryConfigurationDto>>(data);
        }


    }

    }

