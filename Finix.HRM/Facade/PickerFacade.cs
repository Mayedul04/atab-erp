﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;
using Finix.HRM.Service;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure.Models;

namespace Finix.HRM.Facade
{
    public class PickerFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();

        public List<EmployeePickerDto> GetSelectedEmployees(int? unitId, int? officeUnitId, int? officeLayerId, int? officeId, int? gradeId, int? designationId, int? genderId)
        {

            var employee = _service.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);
            var employeeInfo = employee.Select(em => new EmployeePickerDto
            {
                EmployeeId = em != null ? em.Id : 0,
                EmployeeName = em != null ? em.Person.FirstName + " " + em.Person.LastName : "",
                DesignationId = em != null ? (long)em.DesignationId : 0,
                DesignationName = em != null ? em.Designation.Name : "",
                UnitType = em != null ? em.OfficeUnit.UnitType : 0,
                OfficeUnitId = em != null ? em.OfficeUnit.Id : 0,
                OfficeUnitName = em != null ? em.OfficeUnit.Name : "",
                OfficeLayerId = em != null ? em.Office.OfficeLayerId : 0,
                OfficeId = em != null ? em.Office.Id : 0,
                OfficeName = em != null ? em.Office.Name : "",
                GradeId = em != null ? em.Designation.Grade.Id : 0,
                GradeName = em != null ? em.Designation.Grade.Name : "",
                GenderId = em != null ? (int)em.Person.Gender : 0,
                Pick = false
            }).ToList();
            if (unitId != 0 && unitId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.UnitType == (UnitType)unitId).ToList();
            }
            if (officeUnitId != 0 && officeUnitId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeUnitId == officeUnitId).ToList();
            }
            if (officeLayerId != 0 && officeLayerId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeLayerId == officeLayerId).ToList();
            }
            if (officeId != 0 && officeId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeId == officeId).ToList();
            }
            if (gradeId != 0 && gradeId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.GradeId == gradeId).ToList();
            }
            if (designationId != 0 && designationId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.DesignationId == designationId).ToList();
            }
            if (genderId != 0 && genderId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.GenderId == genderId).ToList();
            }
            return employeeInfo;
        }
        public void GetPickedEmployees(List<EmployeePickerDto> model)
        {

        }
        public List<OrganogramDto> GetSelectedOrganograms(long? unitId, long? officeUnitId, long? officeLayerId, long? officeId, long? parentId, long? designationId, long? empId, long? positionId)
        {

            var employee = _service.GetAll<OrganoGram>().Where(x => x.Status == EntityStatus.Active);
            var employeeInfo = employee.Select(em => new OrganogramDto
            {
                EmployeeId = em != null ? em.EmployeeId : 0,
                EmployeeName = em != null ? em.Employee.Person.FirstName + " " + em.Employee.Person.LastName : "",
                DesignationId = em != null ? (long)em.DesignationId : 0,
                DesignationName = em != null ? em.Designation.Name : "",
                //UnitType = em != null ? em.OfficeUnit.UnitType : (long?)0,
                OfficeUnitId = em != null ? em.OfficeUnit.Id : 0,
                OfficeUnitName = em != null ? em.OfficeUnit.Name : "",
                OfficeLayerId = em != null ? em.Office.OfficeLayerId : 0,
                OfficeId = em != null ? em.Office.Id : 0,
                OfficeName = em != null ? em.Office.Name : "",
                GradeId = em != null ? em.Designation.Grade.Id : 0,
                GradeName = em != null ? em.Designation.Grade.Name : "",
                ParentId = em != null ? em.ParentId : null,
                PositionId = em != null ? em.PositionId : 0,
                //ParentName = em != null ? em.Parent : "",
                Pick = false
            }).ToList();
            if (unitId != 0 && unitId != null)
            {
                //employeeInfo = employeeInfo.Where(x => x.UnitType == unitId).ToList();
            }
            if (officeUnitId != 0 && officeUnitId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeUnitId == officeUnitId).ToList();
            }
            if (officeLayerId != 0 && officeLayerId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeLayerId == officeLayerId).ToList();
            }
            if (officeId != 0 && officeId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeId == officeId).ToList();
            }
            if (positionId != 0 && positionId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.PositionId == positionId).ToList();
            }
            if (parentId != 0 && parentId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.ParentId == parentId).ToList();
            }
            if (designationId != 0 && designationId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.DesignationId == designationId).ToList();
            }
            if (empId != 0 && empId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.EmployeeId == empId).ToList();
            }
            return employeeInfo;
        }
        public List<OrganogramDto> GetSelectedEmployeesForOrganogram(long? unitId, long? officeUnitId, long? officeLayerId, long? officeId, long? parentId, long? designationId, long? empId, long? positionId)
        {
            var employee = _service.GetAll<Employee>().Where(x => x.Status == EntityStatus.Active);
            var organogram = _service.GetAll<OrganoGram>().Where(x => x.Status == EntityStatus.Active);
            if (organogram.Any())
            {
                var employeeIds = organogram.Select(r => r.EmployeeId).ToList();
                employee = employee.Where(r => employeeIds.Contains(r.Id));

            }
            var employeeInfo = employee.Select(em => new OrganogramDto
            {
                EmployeeId = em != null ? em.Id : 0,
                EmployeeName = em != null ? em.Person.FirstName + " " + em.Person.LastName : "",
                DesignationId = em != null ? em.DesignationId != null ? (long)em.DesignationId:0 : 0,
                DesignationName = em != null ? em.Designation.Name : "",
                //UnitType = em != null ? em.OfficeUnit.UnitType : (long?)0,
                OfficeUnitId = em != null ? em.OfficeUnitId != null? (long)em.OfficeUnitId :0: 0,
                OfficeUnitName = em != null ? em.OfficeUnit.Name : "",
                OfficeLayerId = em != null ? em.Office != null? em.Office.OfficeLayerId != null ? em.Office.OfficeLayerId :0:0: 0,
                OfficeId = em != null ? em.OfficeId != null?(long)em.OfficeId :0: 0,
                OfficeName = em != null ? em.Office.Name : "",
                GradeId = em != null ? em.Designation != null? em.Designation.GradeId :0: 0,
                GradeName = em != null ? em.Designation != null ? em.Designation.Grade != null?em.Designation.Grade.Name :"":"": "",
                //ParentId = em != null ? em.ParentId : null,
                //PositionId = em != null ? em.PositionId : 0,
                Pick = false
            }).ToList();
            if (unitId != 0 && unitId != null)
            {
                //employeeInfo = employeeInfo.Where(x => x.UnitType == unitId).ToList();
            }
            if (officeUnitId != 0 && officeUnitId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeUnitId == officeUnitId).ToList();
            }
            if (officeLayerId != 0 && officeLayerId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeLayerId == officeLayerId).ToList();
            }
            if (officeId != 0 && officeId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.OfficeId == officeId).ToList();
            }
            if (positionId != 0 && positionId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.PositionId == positionId).ToList();
            }
            if (parentId != 0 && parentId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.ParentId == parentId).ToList();
            }
            if (designationId != 0 && designationId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.DesignationId == designationId).ToList();
            }
            if (empId != 0 && empId != null)
            {
                employeeInfo = employeeInfo.Where(x => x.EmployeeId == empId).ToList();
            }
            return employeeInfo;
        }
    }
}
