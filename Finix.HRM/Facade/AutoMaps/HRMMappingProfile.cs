﻿using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Util;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Finix.HRM.Facade.AutoMaps
{
    public class HRMMappingProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            //CreateMap<Country, CountryDto>();
            //CreateMap<CountryDto, Country>();

            CreateMap<Employee, EmployeeDto>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Person != null ? src.Person.Name : "")); 
            CreateMap<EmployeeDto, Employee>();

            //CreateMap<Module, ModuleDto>();
            //CreateMap<ModuleDto, Module>();

            //CreateMap<Role, RoleDto>();
            //CreateMap<RoleDto, Role>();

            //CreateMap<Infrastructure.Task, TaskDto>();
            //CreateMap<TaskDto, Infrastructure.Task>();

            //CreateMap<User, UserDto>();
            //CreateMap<UserDto, UserDto>();

            CreateMap<Designation, DesignationDto>();
            CreateMap<DesignationDto, Designation>();

            CreateMap<GradeStep, GradeStepDto>();
            CreateMap<GradeStepDto, GradeStep>();




            CreateMap<CalendarDto, Finix.HRM.Infrastructure.Models.Calendar>();
            CreateMap<Finix.HRM.Infrastructure.Models.Calendar, CalendarDto>();


            //CreateMap<FiscalYearDto, FiscalYears>();
            //CreateMap<FiscalYears, FiscalYearDto>();
            CreateMap<FiscalYearDto, FiscalYears>();
            CreateMap<FiscalYears, FiscalYearDto>()
                .ForMember(d => d.QuarterOneTxt, o => o.MapFrom(s => s.QuarterOne.ToString("dd/MM/yyyy")))
                .ForMember(d => d.QuarterTwoTxt, o => o.MapFrom(s => s.QuarterTwo.ToString("dd/MM/yyyy")))
                .ForMember(d => d.QuarterThreeTxt, o => o.MapFrom(s => s.QuarterThree.ToString("dd/MM/yyyy")))
                .ForMember(d => d.QuarterFourTxt, o => o.MapFrom(s => s.QuarterFour.ToString("dd/MM/yyyy")));

            CreateMap<AppraisalDto, Appraisal>();
            CreateMap<Appraisal, AppraisalDto>()
                .ForMember(d => d.StartDateTxt, o => o.MapFrom(s => s.StartDate.ToString("dd/MM/yyyy")))
                .ForMember(d => d.EndDateTxt, o => o.MapFrom(s => s.EndDate.ToString("dd/MM/yyyy")))
                .ForMember(d => d.LastE1DateTxt, o => o.MapFrom(s => s.LastE1Date.ToString("dd/MM/yyyy")))
                .ForMember(d => d.LastE2DateTxt, o => o.MapFrom(s => s.LastE2Date.ToString("dd/MM/yyyy")))
                .ForMember(d => d.LastSubmissionDateTxt, o => o.MapFrom(s => s.LastSubmissionDate.ToString("dd/MM/yyyy")))
                .ForMember(d => d.EffectiveDateTxt, o => o.MapFrom(s => s.EffectiveDate.ToString("dd/MM/yyyy")));

            CreateMap<AppraisalKpiGroupSettingDto, AppraisalKpiGroupSetting>();
            CreateMap<AppraisalKpiGroupSetting, AppraisalKpiGroupSettingDto>();

            CreateMap<KPIGroupSetting, KPIGroupSettingDto>();
            CreateMap<KPIGroupSettingDto, KPIGroupSetting>();

           

            CreateMap<KPIDto, KPI>();
            CreateMap<KPI, KPIDto>();

            CreateMap<SalaryItemDto, SalaryItem>();
            CreateMap<SalaryItem, SalaryItemDto>()
                .ForMember(dest => dest.SalaryItemTypeName,
                    opts => opts.MapFrom(
                        src => Enum.GetName(typeof(SalaryItemType), src.SalaryItemType)))
                        .ForMember(stat => stat.StatusName,
                        op => op.MapFrom(sr => Enum.GetName(typeof(SalaryItemStatus), sr.Status)))
                        .ForMember(st => st.ContributionTypeName,
                        o => o.MapFrom(s => Enum.GetName(typeof(SalaryItemContributionType), s.ContributionType))
                        );

            CreateMap<LeaveApplication, LeaveApplicationDto>();
            CreateMap<LeaveApplicationDto, LeaveApplication>();

            //CreateMap<Employee, EmployeeDto>();
            //CreateMap<EmployeeDto, EmployeeDto>();
            CreateMap<Office, OfficeDto>();
            CreateMap<OfficeDto, Office>();

            CreateMap<LeaveApplication, LeaveApplicationDto>()
                .ForMember(dest => dest.EmployeeName,
                   opts => opts.MapFrom(
                       src => src.Employee.Person.FirstName + " " + src.Employee.Person.LastName))
               .ForMember(dest => dest.SubstitutorId,
                   opts => opts.MapFrom(
                       src => src.Employee.Person.FirstName + " " + src.Employee.Person.LastName));

            CreateMap<BasicSalaryConfiguration, BasicSalaryConfigurationDto>();
            CreateMap<BasicSalaryConfigurationDto, BasicSalaryConfiguration>();

            CreateMap<AccHeadMapping, AccHeadMappingDto>();
            CreateMap<AccHeadMappingDto, AccHeadMapping>();
        }

        //private static void ExtendedMappings()
        //{

        //    Mapper.CreateMap<SubModule, SubModuleDto>()
        //        .ForMember(d => d.ModuleId, o => o.MapFrom(m => m.ModuleId))
        //        .ForMember(d => d.ModuleName, o => o.MapFrom(m => m.Module.Name));
        //    Mapper.CreateMap<SubModuleDto, SubModule>();

        //    Mapper.CreateMap<Menu, MenuDto>()
        //        .ForMember(d => d.SubModuleId, o => o.MapFrom(m => m.SubModuleId))
        //        .ForMember(d => d.SubModuleName, o => o.MapFrom(m => m.SubModule.Name));

        //    Mapper.CreateMap<MenuDto, Menu>();
        //}
    }
}
