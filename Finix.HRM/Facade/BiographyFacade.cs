﻿using AutoMapper;
using Finix.Auth.Facade;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;
using Finix.HRM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
  public class BiographyFacade
    {
        private readonly GenService _service = new GenService();
        private UserFacade _user = new UserFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        public List<EmployeeTransferDto> GetEmployeeDepartmentWise(long? officeid, long? officeUnitId)
        {
            List<EmployeeTransferDto> data = new List<EmployeeTransferDto>();
            List<Employee> employees = new List<Employee>();
            if (officeid != null)
            {
                employees = _service.GetAll<Employee>().Where(x => x.Status == EntityStatus.Active && x.OfficeId == officeid).ToList();
                if (officeUnitId != null)
                    employees = employees.Where(x => x.OfficeUnitId == officeUnitId).ToList();
            }
            else
            {
                if (officeUnitId != null)
                    employees = _service.GetAll<Employee>().Where(x => x.Status == EntityStatus.Active && x.OfficeUnitId == officeUnitId).ToList();
                else
                    employees = _service.GetAll<Employee>().Where(x => x.Status == EntityStatus.Active).ToList();
            }
               
            data = employees.Select(x => new EmployeeTransferDto
            {
                EmployeeId=x.Id,
                EmployeeName = x.Person.FirstName + " " + x.Person.LastName,
                DesignationName = (x.Designation != null ? x.Designation.Name : ""),
                OfficeName = x.Office.Name,
                OfficeUnitId = (x.OfficeUnit != null ? (long)x.OfficeUnitId : 0),
                OfficeUnitName = (x.OfficeUnit != null ? x.OfficeUnit.Name : ""),
                OfficeId = (long)x.OfficeId,
                EmployeeTypeId = (long)x.EmployeeType,
                EmployeeTypeName = (x.EmployeeType >0 ? Util.UiUtil.GetDisplayName(x.EmployeeType) : "")

            }).ToList();
            
            return data;
        }

        public List<OfficeUnitDto> GetOfficeUnit(long officeid)
        {
            var unitsettings = _service.GetAll<OfficeUnitSetting>().Where(x => x.OfficeId == officeid).ToList();
            var officeunits = _service.GetAll <OfficeUnit>().Where(x => x.Status == EntityStatus.Active).ToList();
            
            var data = (from sett in unitsettings
                       join unit in officeunits
                       on sett.OfficeUnitId equals unit.Id
                       select new OfficeUnitDto
                       {
                        Id = unit.Id,
                        Name = unit.Name,
                        UnitType = (int)unit.UnitType,
                        UnitTypeName = unit.UnitType.ToString()
                        //ParentId = (long)unit.ParentId,
                        //ParentName = (unit.ParentId==null? "" : unit.ParentUnit.Name) 
                        }).ToList();
            return data;
        }
        public EmpBasicInfoDto GetBasicInfoById(int empid)
        {
            var emp = _service.GetById<Employee>(empid);
            EmpBasicInfoDto basicdto = new EmpBasicInfoDto();
            basicdto.FirstName = (emp.Person.FirstName!=null? emp.Person.FirstName:"") ;
            basicdto.LastName = emp.Person.LastName;
            basicdto.Name = emp.Person.FirstName + " " + emp.Person.LastName;
            if (emp.Person.Father != null)
            {
                basicdto.FatherFirstName = emp.Person.Father.FirstName;
                basicdto.FatherLastName = emp.Person.Father.LastName;
            }
            if (emp.Person.Mother != null)
            {
                basicdto.MotherFirstName = emp.Person.Mother.FirstName;
                basicdto.MotherLastName = emp.Person.Mother.LastName;
            }
            if (emp != null)
            {
                basicdto.DateOfBirth = emp.Person.DateOfBirth;
                basicdto.Gender = (int)emp.Person.Gender;
                basicdto.GenderName= (emp.Person.Gender!=null? UiUtil.GetDisplayName(emp.Person.Gender):"");
                basicdto.MaritalStatus = (int)emp.Person.MaritalStatus;
                basicdto.MaritalStatusName = (emp.Person.MaritalStatus != null ? UiUtil.GetDisplayName(emp.Person.MaritalStatus):"");
                basicdto.Religion = (int)emp.Person.Religion;
                basicdto.ReligionName = (emp.Person.Religion != null ? UiUtil.GetDisplayName(emp.Person.Religion) : "");
                basicdto.BloodGroup = (int)emp.Person.BloodGroup;
                basicdto.BloodGroupName = (emp.Person.BloodGroup != null ? UiUtil.GetDisplayName(emp.Person.BloodGroup):"");
                basicdto.CountryId = emp.Person.CountryId;
                basicdto.CountryName = emp.Person.Nationality.Name;
                basicdto.DesignationId = emp.DesignationId;
                basicdto.DesignationName = (emp.Designation != null ? emp.Designation.Name: "");
                basicdto.NID = emp.Person.NID;
                basicdto.EmpCode = emp.EmpCode != null ? (long)emp.EmpCode : (long)0;
                basicdto.EmployeeType = (int)emp.EmployeeType;
                basicdto.EmployeeTypeName = (emp.EmployeeType != null ? UiUtil.GetDisplayName(emp.EmployeeType) :"");
            }
            return basicdto;
        }

        public List<EmployeeEducationDto> GetEmpEducations(long empid)
        {
            var educations = _service.GetAll<EmployeeEducation>().Where(x => x.EmployeeId == empid);
            var data = educations.Select(x => new EmployeeEducationDto
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                DegreeId = x.DegreeId,
                DegreeName = x.Degree.Name,
                InstituteId = x.InstituteId,
                InstituteName = x.Institute.Name,
                PassingYear = x.PassingYear,
                Result = x.Result,
                Summary = x.Summary
            }).ToList();
            return data;
        }
        public List<TrainingInformationDto> GetTrainingInfo(long empid)
        {
            var traininginfos = _service.GetAll<EmployeeTraining>().Where(x => x.EmployeeId == empid);
            var data = traininginfos.Select(x => new TrainingInformationDto
            {
                Id = x.Id,
                TrainingVendorId = x.TrainingVendorId,
                TrainingVendorName = x.TrainingVendor.VendorName,
                TrainingId = x.TrainingId,
                TrainingName = x.Training.TrainingName,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                TrainingHours = x.TrainingHours,
                EmployeeId = x.EmployeeId
            }).ToList();
            return data;
        }
        public List<EmploymentHistoryDto> GetExperinceInfo(long empid)
        {
            var traininginfos = _service.GetAll<EmploymentHistory>().Where(x => x.EmployeeId == empid);
            var data = traininginfos.Select(x => new EmploymentHistoryDto
            {
                Id = x.Id,
                CompanyName = x.CompanyName,
                CompanyBusiness = x.CompanyBusiness,
                Designation = x.Designation,
                Department = x.Department,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                AreaofExperiences = x.AreaofExperiences,
                EmployeeId = x.EmployeeId,
                Responsibilities=x.Responsibilities,
                CompanyAddress=x.CompanyAddress
            }).ToList();
            return data;
        }
        public ContactInformationDto GetContactInfoById(long empid)
        {
            var emp = _service.GetById<Employee>(empid);
            ContactInformationDto contactdto = new ContactInformationDto();
            if (emp.Person.PresentAddressId != null && emp.Person.PermanentAddressId != null)
            {
                contactdto.PresentAddress = new AddressDto
                {
                    AddressLine1 = emp.Person.PresentAddress.AddressLine1,
                    AddressLine2 = emp.Person.PresentAddress.AddressLine2,
                    PostalCode = emp.Person.PresentAddress.PostOffice.Code
                };
                contactdto.PresentAddressDetails = emp.Person.PresentAddress.AddressLine1 + ", " + emp.Person.PresentAddress.AddressLine2 + ", P.O: " + emp.Person.PresentAddress.PostOffice.Code;
                contactdto.ParmanentAddress = new AddressDto
                {
                    AddressLine1 = emp.Person.PermanentAddress.AddressLine1,
                    AddressLine2 = emp.Person.PermanentAddress.AddressLine2,
                    PostalCode = emp.Person.PermanentAddress.PostOffice.Code
                };
                contactdto.ParmanentAddressDetails = emp.Person.PermanentAddress.AddressLine1 + ", " + emp.Person.PermanentAddress.AddressLine2 + ", P.O: " + emp.Person.PermanentAddress.PostOffice.Code;
                contactdto.PhoneNo = emp.Person.PhoneNo;
                contactdto.Email = emp.Person.Email;
                contactdto.EmergencyContactPerson = emp.Person.EmergencyContactPerson == null ? "" : emp.Person.EmergencyContactPerson.FirstName;
                contactdto.EmergencyContactPhone = emp.Person.EmergencyContactPersonPhone;
                contactdto.EmergencyContactRelation = emp.Person.EmergencyContactPersonRelation;
            }

            return contactdto;
        }
    }
}
