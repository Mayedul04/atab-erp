﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;
using Finix.HRM.DTO;

namespace Finix.HRM.Facade
{
    public class CalendarFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        public List<CalendarDto> GetAllDateData(long year, long month, string dayOne, string dayTwo)
        {
            int daysInMonth = System.DateTime.DaysInMonth((int)year, (int)month);
            var dates = _service.GetAll<Calendar>().Where(r => r.Month == month && r.Year == year).ToList();
            var data = dates.Select(x => new CalendarDto
            {
                Id = x.Id,
                Year = x.Year,
                Month = x.Month,
                PresentDate = x.PresentDate,
                HolidayType = x.HolidayType,
                IsHoliday = x.HolidayType != 0 ? true : false

            }).ToList();
            if (data.Count <= 0)
            {
                var datanew = new List<CalendarDto>();
                for (int i = 1; i <= daysInMonth; i++)
                {

                    var thisYear = new DateTime((int)year, (int)month, i);
                    var dayOfWeek = thisYear.DayOfWeek.ToString();

                    var eachData = new CalendarDto();
                    eachData.Year = year;
                    eachData.Month = month;
                    eachData.PresentDate = thisYear.Date;

                    if (String.CompareOrdinal(dayOfWeek, dayOne) != 0 && String.CompareOrdinal(dayOfWeek, dayTwo) != 0)
                    {
                        datanew.Add(eachData);
                    }
                    else
                    {
                        eachData.IsHoliday = true;
                        eachData.HolidayType = (int)CalendarWeekendDay.Weekend;
                        datanew.Add(eachData);
                    }
                    // HolidayType = ,
                }
                return datanew;
            }

            foreach (var calendarDto in data.ToList())
            {
                if (calendarDto.PresentDate != null)
                {
                    var thisYear = new DateTime((int)year, (int)month, calendarDto.PresentDate.Value.Day);
                    var dayOfWeek = thisYear.DayOfWeek.ToString();
                    if (String.CompareOrdinal(dayOfWeek, dayOne) == 0 || String.CompareOrdinal(dayOfWeek, dayTwo) == 0)
                    {
                        data.Remove(calendarDto);
                    }
                }
            }

            return data;
        }

        public object SaveDate(List<CalendarDto> model)
        {
            string response = null;
            var entities = new List<Calendar>();
            foreach (var calendarDto in model)
            {
                Calendar entity;// = new Calendar();
                try
                {
                    if (calendarDto.Id > 0)
                    {
                        entity = _service.GetById<Calendar>(calendarDto.Id);
                        calendarDto.CreateDate = entity.CreateDate;
                        calendarDto.CreatedBy = entity.CreatedBy;


                        entity = _service.GetById<Calendar>(calendarDto.Id);
                        if (entity == null)
                            entity = new Calendar();


                        entity.Year = calendarDto.Year;
                        entity.Month = calendarDto.Month;
                        entity.PresentDate = calendarDto.PresentDate;
                        entity.HolidayType = calendarDto.HolidayType;

                        entity.EditDate = DateTime.Now;
                    }
                    else
                    {

                        entity = new Calendar
                        {
                            Year = calendarDto.Year,
                            Month = calendarDto.Month,
                            PresentDate = calendarDto.PresentDate,
                            HolidayType = calendarDto.HolidayType

                        };
                        entity.CreateDate = DateTime.Now;
                    }
                    entity.Status = EntityStatus.Active;
                    entities.Add(entity);


                }
                catch (Exception ex)
                {
                    response = "Error Occoured " + ex;

                }
            }
            _service.Save(entities);
            response = "Calendar Saved Successfully ";
            return response;
        }

        public List<HolidayTypesDto> GetHolidayTypes()
        {
            var holidayTypesDto = GenService.GetAll<HolidayTypes>();
            var data = holidayTypesDto.Select(x => new HolidayTypesDto
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();
            return data;
        }

        public ResponseDto SaveHolidayTypes(HolidayTypesDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new HolidayTypes();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<HolidayTypes>(model.Id);
                    //model.Id = entity.Id;
                    entity.Name = model.Name;
                    entity.EditDate = model.EditDate;
                    entity.EditedBy = model.EditedBy;
                    entity.Status = EntityStatus.Active;

                }
                else
                {
                    entity = new HolidayTypes
                    {
                        Name = model.Name,
                        CreateDate = DateTime.Now,
                        Status = EntityStatus.Active
                    };

                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Holiday Types Saved Successfully";
            return response;

        }

        public ResponseDto DeleteHolidayTypes(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<HolidayTypes>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Holiday Types Deleted Successfully";
            return response;
        }
        #region FiscalYear

        public List<FiscalYearDto> GetAllFiscalYears()
        {
            var fiscalYears = _service.GetAll<FiscalYears>().ToList();
            var data = Mapper.Map<List<FiscalYearDto>>(fiscalYears);

            return data;
        }
        #endregion
        public ResponseDto SaveFiscalYear(FiscalYearDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new FiscalYears();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<FiscalYears>(model.Id);

                    entity.Year = model.Year;
                    entity.CurrentYear = model.CurrentYear;
                    entity.QuarterOne = model.QuarterOne;
                    entity.QuarterTwo = model.QuarterTwo;
                    entity.QuarterThree = model.QuarterThree;
                    entity.QuarterFour = model.QuarterFour;
                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;

                }
                else
                {
                    entity = new FiscalYears
                    {
                        Year = model.Year,
                        CurrentYear = model.CurrentYear,
                        QuarterOne = model.QuarterOne,
                        QuarterTwo = model.QuarterTwo,
                        QuarterThree = model.QuarterThree,
                        QuarterFour = model.QuarterFour,
                        CreateDate = DateTime.Now,
                        Status = EntityStatus.Active
                    };

                }
                _service.Save(entity);
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
                return response;
            }
            response.Success = true;
            response.Message = "Fiscal Year Saved Successfully";
            return response;
        }
        //public ResponseDto SaveFiscalYear(FiscalYearDto model)
        //{
        //    ResponseDto response = new ResponseDto();
        //    var entity = new FiscalYears();
        //    try
        //    {
        //        if (model.Id > 0)
        //        {
        //            entity = _service.GetById<FiscalYears>(model.Id);

        //            entity.Year = model.Year;
        //            entity.CurrentYear = model.CurrentYear;
        //            entity.QuarterOne = model.QuarterOne;
        //            entity.QuarterTwo = model.QuarterTwo;
        //            entity.QuarterThree = model.QuarterThree;
        //            entity.QuarterFour = model.QuarterFour;
        //            entity.EditDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;

        //        }
        //        else
        //        {
        //            entity = new FiscalYears
        //            {
        //                Year = model.Year,
        //                CurrentYear = model.CurrentYear,
        //                QuarterOne = model.QuarterOne,
        //                QuarterTwo = model.QuarterTwo,
        //                QuarterThree = model.QuarterThree,
        //                QuarterFour = model.QuarterFour,
        //                CreateDate = DateTime.Now,
        //                Status = EntityStatus.Active
        //            };

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "Exception Occoured " + ex;
        //    }

        //    _service.Save(entity);
        //    response.Success = true;
        //    response.Message = "Fiscal Year Saved Successfully";
        //    return response;

        //}

        public object DeleteFiscalYear(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<FiscalYears>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Fiscal Year Deleted Successfully";
            return response;
        }
    }

}

