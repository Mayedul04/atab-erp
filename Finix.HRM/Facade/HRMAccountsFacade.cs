﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class HRMAccountsFacade : BaseFacade
    {
        public IPagedList<AccHeadMappingDto> GetAccHeadMappingList(int pageSize, int pageCount, string searchString)
        {
            var allAccHeads = (from accMap in GenService.GetAll<AccHeadMapping>().Where(a => a.Status == EntityStatus.Active)
                               //join product in GenService.GetAll<ProductCategory>() on accMap.RefId equals product.Id into ProductList
                               //from p in ProductList.DefaultIfEmpty()
                               //join supplier in GenService.GetAll<Supplier>() on accMap.RefId equals supplier.Id into SupplierList
                               //from s in SupplierList.DefaultIfEmpty()
                               //join bank in GenService.GetAll<Bank>() on accMap.RefId equals bank.Id into BankList
                               //from b in BankList.DefaultIfEmpty()
                               select new AccHeadMappingDto()
                               {
                                   Id = accMap.Id,
                                   AccountHeadCode = accMap.AccountHeadCode,
                                   RefId = accMap.RefId,
                                   RefType = accMap.RefType,
                                   RefTypeName = accMap.RefType.ToString(),//UiUtil.GetDisplayName(accMap.RefType),
                                   RefName = //accMap.RefId != null ?
                                             //(accMap.RefType == AccountHeadRefType.PurchaseProductWise || accMap.RefType == AccountHeadRefType.SalesProductWise) && p != null ? p.Name :
                                             //accMap.RefType == AccountHeadRefType.SupplierPayable && s != null ? s.Name :
                                             //accMap.RefType == AccountHeadRefType.BankAsAsset && b != null ? b.Name :
                                             //"" :
                                             accMap.RefType.ToString()
                               }).ToList();
            var temp = allAccHeads.OrderBy(r => r.Id).ToList().ToPagedList(pageCount, pageSize);
            return temp;
        }
        public object GetAccHeads(AccountHeadRefType refType, long officeId)
        {
            if (refType == AccountHeadRefType.SalaryExpense || refType == AccountHeadRefType.SalaryPayable)
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                    return new { Code = code, Name = refType.ToString() };
            }
            return null;
        }
        public ResponseDto SaveAccHeadMapping(AccHeadMappingDto dto, long userId)
        {
            var response = new ResponseDto();
            try
            {
                var entity = AutoMapper.Mapper.Map<AccHeadMapping>(dto);
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.Status = EntityStatus.Active;
                GenService.Save(entity);
                response.Success = true;
                response.Message = "Account head mapping successful";
            }
            catch (Exception)
            {
                response.Message = "Mapping not saved, please contact administrator.";
            }
            return response;
        }
        public string GetAccHeadCodeForVoucher(AccountHeadRefType refType, long? refId)
        {
            var data =
                GenService.GetAll<AccHeadMapping>()
                    .Where(a => a.RefType == refType && a.Status == EntityStatus.Active)  /*a.RefId == refId &&*/
                    .FirstOrDefault();

            if (data != null)
                return data.AccountHeadCode;
            else
                return "";
        }
    }
}
