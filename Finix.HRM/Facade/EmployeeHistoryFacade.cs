﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure.Models;

namespace Finix.HRM.Facade
{
    public class EmployeeHistoryFacade:BaseFacade
    {
        public void SaveTransferHistory(EmployeeTransferDto transferdto)
        {
            GenService.Save(new EmployeeJobHistory
            {
                EmployeeId = transferdto.EmployeeId,
                OfficeId = transferdto.OfficeId,
                OfficeUnitId = transferdto.OfficeUnitId,
                GradeStepId = GenService.GetById<Employee>(transferdto.EmployeeId).GradeStepId == null ? null : GenService.GetById<Employee>(transferdto.EmployeeId).GradeStepId,
                JobStatus = EmployeeHistoryEnum.transfer,
                EffectiveDate=DateTime.Today,
                Remarks = transferdto.Remarks
            });
            //GenService.SaveChanges();
            var emp = GenService.GetById<Employee>(transferdto.EmployeeId);
            emp.OfficeId = transferdto.OfficeId;
            emp.OfficeUnitId = transferdto.OfficeUnitId;
            //emp.GradeStepId = transferdto.GradeStepId;
            GenService.SaveChanges();
        }

        public Object GetRespectiveOfficeInfo(int empid)
        {
            var emp = GenService.GetById<Employee>(empid);
            var designationId = GenService.GetAll<Designation>().Where(x => x.Id == emp.DesignationId).FirstOrDefault().Id;
            var officeId = GenService.GetAll<Office>().Where(x => x.Id == emp.OfficeId).FirstOrDefault().Id;
            var officeLayerId = GenService.GetAll<Office>().Where(x => x.Id == emp.OfficeId).FirstOrDefault().OfficeLayerId;
            var unitType = (int)GenService.GetAll<OfficeUnit>().Where(x => x.Id == emp.OfficeUnitId).FirstOrDefault().UnitType;
            var officeUnitId = GenService.GetAll<OfficeUnit>().Where(x => x.Id == emp.OfficeUnitId).FirstOrDefault().Id;
            return new {DesignationId=designationId, OfficeId = officeId, OfficeLayerId = officeLayerId, OfficeUnitType = unitType, OfficeUnitId = officeUnitId };
        }

        public Object GetRespectiveEmpInfo(int empid)
        {
            var emp = GenService.GetById<Employee>(empid);
            var designationId = GenService.GetAll<Designation>().Where(x => x.Id == emp.DesignationId).FirstOrDefault().Name;
            var officeId = GenService.GetAll<Office>().Where(x => x.Id == emp.OfficeId).FirstOrDefault().Name;
            var officeLayerId = GenService.GetAll<Office>().Where(x => x.Id == emp.OfficeId).FirstOrDefault().OfficeLayer.Name;
            var unitType =GenService.GetAll<OfficeUnit>().Where(x => x.Id == emp.OfficeUnitId).FirstOrDefault().UnitType.ToString();
            var officeUnitId = GenService.GetAll<OfficeUnit>().Where(x => x.Id == emp.OfficeUnitId).FirstOrDefault().Name;
            return new { DesignationId = designationId, OfficeId = officeId, OfficeLayerId = officeLayerId, OfficeUnitType = unitType, OfficeUnitId = officeUnitId };
        }
        public Object GetRespectivePostingInfo(int empid)
        {
            var emp = GenService.GetById<Employee>(empid);
            var gradeStepId = GenService.GetAll<GradeStep>().Where(x => x.Id == emp.GradeStepId) == null ? 1 : GenService.GetAll<GradeStep>().Where(x => x.Id == emp.GradeStepId).FirstOrDefault().Id;
            var designationId = GenService.GetAll<Designation>().Where(x => x.Id == emp.DesignationId).FirstOrDefault().Id;
            var gradeId = GenService.GetAll<Designation>().Where(x => x.Id == emp.DesignationId).FirstOrDefault().GradeId;
            return new { GradeStepId = gradeStepId, DesignationId = designationId,GradeId=gradeId };
        }

        public void SavePromotionHistory(EmployeeTransferDto transferdto)
        {
            GenService.Save(new EmployeeJobHistory
            {
                EmployeeId = transferdto.EmployeeId,
                GradeStepId = transferdto.GradeStepId,
                DesignationId = transferdto.DesignationId,
                JobStatus = EmployeeHistoryEnum.promotion,
                EffectiveDate = DateTime.Today,
                Remarks = transferdto.Remarks
            });
            //GenService.SaveChanges();
            var emp = GenService.GetById<Employee>(transferdto.EmployeeId);
            emp.DesignationId = transferdto.DesignationId;
            emp.GradeStepId = transferdto.GradeStepId;
            GenService.SaveChanges();
        }

        public void SaveResignationHistory(EmployeeTransferDto transferdto)
        {
            GenService.Save(new EmployeeJobHistory
            {
                EmployeeId = transferdto.EmployeeId,
                JobStatus = EmployeeHistoryEnum.resignation,
                EffectiveDate = DateTime.Today,
                Remarks = transferdto.Remarks
            });
            var emp = GenService.GetById<Employee>(transferdto.EmployeeId);
            emp.EmployeeStatus = EmployeeStatus.Inactive;
            GenService.SaveChanges();
        }
    }
}
