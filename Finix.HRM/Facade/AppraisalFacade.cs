﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;
using Finix.HRM.DTO;

namespace Finix.HRM.Facade
{
    public class AppraisalFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();

        public List<AppraisalDto> GetAllAppraisals()
        {
            var data = _service.GetAll<Appraisal>().ToList();
            return Mapper.Map<List<AppraisalDto>>(data);
        }

        public ResponseDto SaveAppraisal(AppraisalDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new Appraisal();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<Appraisal>(model.Id);

                    entity.Name = model.Name;
                    entity.FiscalYearId = model.FiscalYearId;
                    entity.StartDate = model.StartDate;
                    entity.EndDate = model.EndDate;
                    entity.AppraisalType = model.AppraisalType;
                    entity.AppraisalStatus = model.AppraisalStatus;
                    entity.LastSubmissionDate = model.LastSubmissionDate;
                    entity.LastE1Date = model.LastE1Date;
                    entity.LastE2Date = model.LastE2Date;
                    entity.Comments = model.Comments;
                    entity.Description = model.Description;
                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
                else
                {
                    entity = Mapper.Map<Appraisal>(model);
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Appraisal Saved Successfully";
            return response;
        }

        public ResponseDto DeleteAppraisal(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<Appraisal>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Appraisal Deleted Successfully";
            return response;
        }

        public List<AppraisalKpiGroupSettingDto> GetAllAppraisalKpiGroupSettings()
        {
            var data = _service.GetAll<AppraisalKpiGroupSetting>().ToList();
            return Mapper.Map<List<AppraisalKpiGroupSettingDto>>(data);
        }

        public ResponseDto SaveAppraisalGroupSettings(AppraisalKpiGroupSettingDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new AppraisalKpiGroupSetting();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<AppraisalKpiGroupSetting>(model.Id);


                    entity.AppraisalId = model.AppraisalId;
                    entity.KPIGroupId = model.KPIGroupId;
                    entity.DisplayOrder = model.DisplayOrder;
                    entity.Weight = model.Weight;

                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
                else
                {
                    entity = Mapper.Map<AppraisalKpiGroupSetting>(model);
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Appraisal Kpi Group Setting Saved Successfully";
            return response;
        }

        public ResponseDto DeleteAppraisalGroupSettings(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<AppraisalKpiGroupSetting>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Appraisal KPI Group Deleted Successfully";
            return response;
        }

        public PerformanceAppraisalDto GetAppraisals(int DesignationId)
        {
            var kpiGroupIdList = GenService.GetById<Designation>(DesignationId).KPIs.Select(x => x.KPIGroupId);
            //var kpiIdList = GenService.GetById<Designation>(DesignationId).KPIs.Select(x => x.Id);
            //var KPIGroups = GenService.GetAll<KPIGroup>().Where(x => kpiGroupIdList.Contains(x.Id));
            var AllKPIGroups = GenService.GetAll<KPIGroup>();
            var appraisalPerformances = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == DesignationId);
            if (appraisalPerformances.Count() == 0)
            {
                PerformanceAppraisalDto dto = new PerformanceAppraisalDto();
                dto.DesignationId = DesignationId;
                dto.PeriodFrom = DateTime.Today;
                dto.PeriodTo = DateTime.Today;
                List<KPIGroupDto> list = new List<KPIGroupDto>();

                foreach (var item in AllKPIGroups)
                {

                    KPIGroupDto grpdto = new KPIGroupDto();
                    grpdto.Id = item.Id;
                    grpdto.IsGroupSelected = false;
                    grpdto.KPIGroupWeight = 0;
                    grpdto.Name = item.Name;
                    //var kpis = item.KPIs.Where(p=>kpiIdList.Contains(p.Id));
                    grpdto.KPIList = item.KPIs.Select(x => new KPIDto
                    {
                        Id = x.Id,
                        IsKPISelected = false,
                        KpiWeight = 0,
                        Name = x.Name
                    }).ToList();
                    list.Add(grpdto);
                }

                dto.KPIGroupList = list;
                return dto;
            }
            else
            {
                PerformanceAppraisalDto dto = new PerformanceAppraisalDto();
                dto.KPIGroupList = new List<KPIGroupDto>();
                dto.DesignationId = DesignationId;
                dto.PeriodFrom = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == DesignationId).FirstOrDefault().FromDate;
                dto.PeriodTo = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == DesignationId).FirstOrDefault().ToDate;

                foreach (var item in AllKPIGroups)
                {
                    var kpigrps = appraisalPerformances.Where(x => x.KPIGroupId == item.Id);

                    if (kpigrps.Count() != 0)
                    {
                        KPIGroupDto grpdto = new KPIGroupDto();
                        grpdto.KPIList = new List<KPIDto>();
                        grpdto.Id = item.Id;
                        grpdto.Name = item.Name;
                        grpdto.IsGroupSelected = kpigrps.FirstOrDefault().IsGroupSelected;
                        grpdto.KPIGroupWeight = kpigrps.FirstOrDefault().KPIGroupWeight;
                        //var kpis = item.KPIs.Where(p => kpiIdList.Contains(p.Id));
                        foreach (var element in item.KPIs)
                        {
                            var appraisal = kpigrps.Where(x => x.KPIId == element.Id).FirstOrDefault();
                            if (appraisal != null)
                            {

                                KPIDto kpidto = new KPIDto();
                                kpidto.Id = appraisal.KPIId;
                                kpidto.IsKPISelected = appraisal.IsKPISelected;
                                kpidto.Name = element.Name;
                                kpidto.KpiWeight = appraisal.KPIWeight;

                                grpdto.KPIList.Add(kpidto);
                            }
                            else
                            {
                                KPIDto kpidto = new KPIDto();
                                kpidto.Id = element.Id;
                                kpidto.IsKPISelected = false;
                                kpidto.Name = element.Name;
                                kpidto.KpiWeight = 0;
                                grpdto.KPIList.Add(kpidto);
                            }
                        }

                        dto.KPIGroupList.Add(grpdto);
                    }
                    else
                    {
                        KPIGroupDto grpdto = new KPIGroupDto();
                        grpdto.Id = item.Id;
                        grpdto.Name = item.Name;
                        grpdto.IsGroupSelected = false;
                        grpdto.KPIGroupWeight = 0;
                        //var kpis = item.KPIs.Where(p => kpiIdList.Contains(p.Id));
                        grpdto.KPIList = item.KPIs.Select(x => new KPIDto
                        {
                            Id = x.Id,
                            IsKPISelected = false,
                            KpiWeight = 0,
                            Name = x.Name
                        }).ToList();
                        dto.KPIGroupList.Add(grpdto);
                    }

                }

                return dto;
            }

        }

        public void SavePerformanceAppraisals(PerformanceAppraisalDto pdto)
        {
            var performanceAppraisallist = GenService.GetAll<PerformanceAppraisal>();
            if (performanceAppraisallist.Count() == 0)
            {
                foreach (var item in pdto.KPIGroupList)
                {
                    foreach (var el in item.KPIList)
                    {
                        PerformanceAppraisal appr = new PerformanceAppraisal();
                        appr.DesignationId = pdto.DesignationId;
                        appr.FromDate = pdto.PeriodFrom;
                        appr.ToDate = pdto.PeriodTo;
                        appr.KPIGroupId = item.Id;
                        appr.IsGroupSelected = item.IsGroupSelected;
                        appr.KPIGroupWeight = item.KPIGroupWeight;
                        appr.KPIId = el.Id;
                        appr.IsKPISelected = el.IsKPISelected;
                        appr.KPIWeight = el.KpiWeight;
                        GenService.Save(appr);
                    }
                }

            }
            else
            {
                foreach (var item in pdto.KPIGroupList)
                {
                    foreach (var el in item.KPIList)
                    {
                        var appraisalkpgrp = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == pdto.DesignationId && x.KPIGroupId == item.Id);
                        if (appraisalkpgrp.Count() != 0)
                        {
                            var appraisalkpi = appraisalkpgrp.Where(x => x.KPIId == el.Id).FirstOrDefault();
                            if (appraisalkpi != null)
                            {
                                appraisalkpi.FromDate = pdto.PeriodFrom;
                                appraisalkpi.ToDate = pdto.PeriodTo;
                                appraisalkpi.KPIGroupId = item.Id;
                                appraisalkpi.KPIGroupWeight = item.KPIGroupWeight;
                                appraisalkpi.IsGroupSelected = item.IsGroupSelected;
                                appraisalkpi.KPIId = el.Id;
                                appraisalkpi.IsKPISelected = el.IsKPISelected;
                                appraisalkpi.KPIWeight = el.KpiWeight;
                            }
                            else
                            {
                                PerformanceAppraisal appr = new PerformanceAppraisal();
                                appr.DesignationId = pdto.DesignationId;
                                appr.FromDate = pdto.PeriodFrom;
                                appr.ToDate = pdto.PeriodTo;
                                appr.KPIGroupId = item.Id;
                                appr.KPIGroupWeight = item.KPIGroupWeight;
                                appr.IsGroupSelected = item.IsGroupSelected;
                                appr.KPIId = el.Id;
                                appr.IsKPISelected = el.IsKPISelected;
                                appr.KPIWeight = el.KpiWeight;
                                GenService.Save(appr);
                            }

                        }
                        else
                        {

                            PerformanceAppraisal appr = new PerformanceAppraisal();
                            appr.DesignationId = pdto.DesignationId;
                            appr.FromDate = pdto.PeriodFrom;
                            appr.ToDate = pdto.PeriodTo;
                            appr.KPIGroupId = item.Id;
                            appr.IsGroupSelected = item.IsGroupSelected;
                            appr.KPIGroupWeight = item.KPIGroupWeight;
                            appr.KPIId = el.Id;
                            appr.IsKPISelected = el.IsKPISelected;
                            appr.KPIWeight = el.KpiWeight;
                            GenService.Save(appr);
                        }
                    }
                }

            }

            GenService.SaveChanges();
        }

        public PerformanceAppraisalDto GetAppraisalPerformances(int empid, long appraisalid)
        {
            Employee emp = GenService.GetById<Employee>(empid);
            var kpiGroupIdList = emp.Designation.KPIs.Select(x => x.KPIGroup.Id);
            var kpiIdList = emp.Designation.KPIs.Select(x => x.Id);
            var appraisalkpigroupSettings = GenService.GetAll<AppraisalKpiGroupSetting>()
                .Where(x => kpiGroupIdList.Contains(x.KPIGroupId))
                .OrderBy(x => x.DisplayOrder);
            PerformanceAppraisalDto performanceAppraisalDto = new PerformanceAppraisalDto();
            //performanceAppraisalDto.EmployeeId = emp.Id;
            // performanceAppraisalDto.EmployeeName = emp.Person.Name;
            //performanceAppraisalDto.FiscalYear = GenService.GetById<Appraisal>(appraisalid).FiscalYear.Year;
            // performanceAppraisalDto.OfficeUnitName = emp.OfficeUnit.Name;
            // performanceAppraisalDto.OfficeName = emp.Office.Name;
            performanceAppraisalDto.PeriodFrom = GenService.GetById<Appraisal>(appraisalid).StartDate;
            performanceAppraisalDto.PeriodTo = GenService.GetById<Appraisal>(appraisalid).EndDate;
            var data = new[]
            {
                    new {
                    Id=1,Name="Main Indicator", DisplayOrder=1, Weight=40,
                    KPI = new[] {
                        new {Id=11,Name="Sub Indicator1", DisplayOrder=1, Weight=10},
                        new {Id=12,Name="Sub Indicator2", DisplayOrder=2, Weight=20},
                        new {Id=13,Name="Sub Indicator3", DisplayOrder=3, Weight=30},
                        new {Id=14,Name="Sub Indicator4", DisplayOrder=4, Weight=40}
                    }
                },
                new {
                    Id=1,Name="Main Indicator", DisplayOrder=1, Weight=30,
                    KPI = new[] {
                        new {Id=11,Name="Sub Indicator1", DisplayOrder=1, Weight=10},
                        new {Id=12,Name="Sub Indicator2", DisplayOrder=2, Weight=20},
                        new {Id=13,Name="Sub Indicator3", DisplayOrder=3, Weight=30},
                        new {Id=14,Name="Sub Indicator4", DisplayOrder=4, Weight=40}
                    }
                },
                new {
                    Id=1,Name="a", DisplayOrder=1, Weight=30,
                    KPI = new[] {
                        new {Id=11,Name="Sub Indicator1", DisplayOrder=1, Weight=10},
                        new {Id=12,Name="Sub Indicator2", DisplayOrder=2, Weight=20},
                        new {Id=13,Name="Sub Indicator3", DisplayOrder=3, Weight=30},
                        new {Id=14,Name="Sub Indicator4", DisplayOrder=4, Weight=40}
                    }
                }
            };

            //performanceAppraisalDto.KPIGroupList = data.Cast<Object>().ToList();

            //List<dynamic> data = new List<dynamic>();
            //foreach (var s in appraisalkpigroupSettings)
            //{
            //    var kpigroupsettinglist = GenService.GetAll<KPIGroupSetting>()
            //        .Where(x => x.KPIGroupId == s.KPIGroupId && x.AppraisalId == appraisalid && kpiIdList.Contains(x.KPIId))
            //        .OrderBy(x => x.DisplayOrder);

            //     data.Add(new
            //    {
            //        Id=s.KPIGroupId,Name=s.KPIGroup.Name,DisplayOrder=s.DisplayOrder,Weight=s.Weight,
            //        //KpiList = emp.Designation.KPIs.Where(x => x.KPIGroupId == s.KPIGroupId).ToList()
            //        KPI = kpigroupsettinglist.Select(x => new
            //        {
            //            Id=x.KPIId,
            //            Name=x.KPI.Name,
            //            DisplayOrder=x.DisplayOrder,
            //            Weight=x.Weight
            //        }).ToList()
            //    });
            //}
            //performanceAppraisalDto.KPIGroupList = data.Cast<Object>().ToList();
            //foreach (var d in data)
            //{
            //}

            return performanceAppraisalDto;
        }

        public EmployeeEvaluationDto GetSelfEvaluation(long employeeid)
        {
            var evalMaster = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == employeeid);
            if (evalMaster.Count() == 0)
            {
                EmployeeEvaluationDto evaldto = new EmployeeEvaluationDto();
                var emp = GenService.GetById<Employee>(employeeid);
                evaldto.EmployeeId = employeeid;
                evaldto.EmployeeName = emp.Person.FirstName + " " + emp.Person.LastName;
                evaldto.DesignationId = emp.DesignationId;
                evaldto.DesignationName = emp.Designation.Name;
                evaldto.JoiningDate = GenService.GetAll<EmployeeJobHistory>().Where(x => x.EmployeeId == employeeid && x.JobStatus == EmployeeHistoryEnum.join).FirstOrDefault().EffectiveDate;
                var perform_appr = new List<PerformanceAppraisal>();
                var performance_appraisal = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year);
                if (performance_appraisal.Any())
                {
                    evaldto.PeriodFrom = performance_appraisal.FirstOrDefault().FromDate;
                    evaldto.PeriodTo = performance_appraisal.FirstOrDefault().ToDate;
                    perform_appr = performance_appraisal.Where(x => x.IsGroupSelected == true && x.IsKPISelected == true).ToList();
                }
                var kpigrpIdList = perform_appr.Select(x => x.KPIGroupId);
                var kpigrps = GenService.GetAll<KPIGroup>().Where(x => kpigrpIdList.Contains(x.Id));
                List<KPIGroupDto> list = new List<KPIGroupDto>();
                foreach (var item in kpigrps)
                {
                    KPIGroupDto grpdto = new KPIGroupDto();
                    grpdto.Id = item.Id;
                    grpdto.Name = item.Name;
                    var kpisidinagroup = perform_appr.Where(x => x.KPIGroupId == item.Id).Select(y => y.KPIId);
                    var kpis = item.KPIs.Where(y => kpisidinagroup.Contains(y.Id));
                    grpdto.KPIList = kpis.Select(p => new KPIDto
                    {
                        Id = p.Id,
                        Name = p.Name,
                        KPIMarks = 0
                    }).ToList();
                    list.Add(grpdto);
                }
                evaldto.KPIGroupList = list;
                return evaldto;
            }
            else
            {
                return null;
            }

        }

        public EmployeeEvaluationDto GetSupervisor1Evaluation(long empid, long evaluationYear)
        {
            var selfEvaluation = GenService.GetAll<EmployeeEvaluation>().Where(x => x.EmployeeId == empid && x.PerformanceAppraisal.FromDate.Year == evaluationYear && x.EvaluatedBy == empid);
            var emp = GenService.GetById<Employee>(empid);
            EmployeeEvaluationDto evaldto = new EmployeeEvaluationDto();
            evaldto.EmployeeId = empid;
            evaldto.EmployeeName = emp.Person.FirstName + " " + emp.Person.LastName;
            evaldto.JoiningDate = GenService.GetAll<EmployeeJobHistory>().Where(x => x.EmployeeId == empid && x.JobStatus == EmployeeHistoryEnum.join).FirstOrDefault().EffectiveDate;
            evaldto.DesignationId = emp.DesignationId;
            evaldto.DesignationName = emp.Designation.Name;
            var performance_appraisal = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year);
            evaldto.PeriodFrom = performance_appraisal.FirstOrDefault().FromDate;
            evaldto.PeriodTo = performance_appraisal.FirstOrDefault().ToDate;
            var perform_appr = performance_appraisal.Where(x => x.IsGroupSelected == true && x.IsKPISelected == true);
            var kpigrpIdList = perform_appr.Select(x => x.KPIGroupId);
            var kpigrps = GenService.GetAll<KPIGroup>().Where(x => kpigrpIdList.Contains(x.Id));
            List<KPIGroupDto> list = new List<KPIGroupDto>();
            foreach (var item in kpigrps)
            {
                KPIGroupDto grpdto = new KPIGroupDto();
                grpdto.KPIList = new List<KPIDto>();
                grpdto.Id = item.Id;
                grpdto.Name = item.Name;
                var kpisidinagroup = perform_appr.Where(x => x.KPIGroupId == item.Id).Select(y => y.KPIId);
                var kpis = item.KPIs.Where(y => kpisidinagroup.Contains(y.Id));
                foreach (var element in kpis)
                {
                    var self_eve = selfEvaluation.Where(x => x.PerformanceAppraisal.KPIGroupId == item.Id && x.PerformanceAppraisal.KPIId == element.Id).FirstOrDefault();
                    KPIDto kpidto = new KPIDto();
                    kpidto.Id = element.Id;
                    kpidto.Name = element.Name;
                    kpidto.PerformanceAppraisalId = self_eve.PerformanceAppraisalId;
                    kpidto.KPIMarks = self_eve.KPIMarks;
                    kpidto.KPIMarksEvaluator1 = 0;
                    grpdto.KPIList.Add(kpidto);
                }

                list.Add(grpdto);
            }
            evaldto.KPIGroupList = list;
            return evaldto;
        }

        public EvaluationReportDto GetEvaluationReport(long employeeId, DateTime PeriodFrom, DateTime PeriodTo)
        {
            var employee = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == employeeId && x.EvaluationYear == PeriodFrom.Year && x.EvaluationStatus == EmployeeEvaluationStatus.Done);
            if (employee.Count() == 0)
            {
                return null;
            }
            else
            {
                var emp_evaluations = GenService.GetAll<EmployeeEvaluation>().Where(x => x.EmployeeId == employeeId && x.PerformanceAppraisal.FromDate >= PeriodFrom && x.PerformanceAppraisal.ToDate <= PeriodTo);
                EvaluationReportDto reportDto = new EvaluationReportDto();
                var empEvals = from evaluation in emp_evaluations
                               group evaluation by evaluation.EvaluatedBy into newGroup
                               orderby newGroup.Key
                               select newGroup;
                reportDto.EmpCode = emp_evaluations.FirstOrDefault().Employee.EmpCode;
                reportDto.EmployeeName = emp_evaluations.FirstOrDefault().Employee.Person.FirstName + " " + emp_evaluations.FirstOrDefault().Employee.Person.LastName;
                reportDto.DesignationName = emp_evaluations.FirstOrDefault().Employee.Designation.Name;
                reportDto.EvaluationYear = PeriodFrom.Date.Year;
                reportDto.Evaluations = new List<SuperDto>();
                foreach (var item in empEvals)
                {
                    var grps = from eval in item
                               group eval by eval.PerformanceAppraisal.KPIGroupId into Group
                               orderby Group.Key
                               select Group;

                    SuperDto super = new SuperDto();
                    super.EvaluatorId = item.FirstOrDefault().EvaluatedBy;
                    super.EvaluatorName = item.FirstOrDefault().Evaluator.Person.FirstName + " " + item.FirstOrDefault().Evaluator.Person.LastName;
                    super.KPIMarks = 0;
                    foreach (var element in grps)
                    {
                        double marks = 0;
                        foreach (var el in element)
                        {
                            double m = (el.PerformanceAppraisal.KPIWeight * .01);
                            marks = marks + el.KPIMarks * (el.PerformanceAppraisal.KPIWeight * .01);

                        }
                        super.KPIMarks = super.KPIMarks + marks * (element.FirstOrDefault().PerformanceAppraisal.KPIGroupWeight * .01);

                    }
                    super.KPIMarks = Math.Round(super.KPIMarks, 2);
                    reportDto.Evaluations.Add(super);
                }
                return reportDto;
            }

        }
        public EmployeeEvaluationDto GetSupervisor2Evaluation(long empid, long evaluationYear)
        {
            var selfEvaluation = GenService.GetAll<EmployeeEvaluation>().Where(x => x.EmployeeId == empid && x.PerformanceAppraisal.FromDate.Year == evaluationYear && x.EvaluatedBy == empid);
            var supervisorOneEvaluation = GenService.GetAll<EmployeeEvaluation>().Where(x => x.EmployeeId == empid && x.PerformanceAppraisal.FromDate.Year == evaluationYear && x.EvaluatedBy != empid);
            var emp = GenService.GetById<Employee>(empid);
            EmployeeEvaluationDto evaldto = new EmployeeEvaluationDto();
            evaldto.EmployeeId = empid;
            evaldto.EmployeeName = emp.Person.FirstName + " " + emp.Person.LastName;
            evaldto.JoiningDate = GenService.GetAll<EmployeeJobHistory>().Where(x => x.EmployeeId == empid && x.JobStatus == EmployeeHistoryEnum.join).FirstOrDefault().EffectiveDate;
            evaldto.DesignationId = emp.DesignationId;
            evaldto.DesignationName = emp.Designation.Name;
            var performance_appraisal = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year);
            evaldto.PeriodFrom = performance_appraisal.FirstOrDefault().FromDate;
            evaldto.PeriodTo = performance_appraisal.FirstOrDefault().ToDate;
            var perform_appr = performance_appraisal.Where(x => x.IsGroupSelected == true && x.IsKPISelected == true);
            var kpigrpIdList = perform_appr.Select(x => x.KPIGroupId);
            var kpigrps = GenService.GetAll<KPIGroup>().Where(x => kpigrpIdList.Contains(x.Id));
            List<KPIGroupDto> list = new List<KPIGroupDto>();
            foreach (var item in kpigrps)
            {
                KPIGroupDto grpdto = new KPIGroupDto();
                grpdto.KPIList = new List<KPIDto>();
                grpdto.Id = item.Id;
                grpdto.Name = item.Name;
                var kpisidinagroup = perform_appr.Where(x => x.KPIGroupId == item.Id).Select(y => y.KPIId);
                var kpis = item.KPIs.Where(y => kpisidinagroup.Contains(y.Id));
                foreach (var element in kpis)
                {
                    var self_eve = selfEvaluation.Where(x => x.PerformanceAppraisal.KPIGroupId == item.Id && x.PerformanceAppraisal.KPIId == element.Id).FirstOrDefault();
                    var superOne_eve = supervisorOneEvaluation.Where(x => x.PerformanceAppraisal.KPIGroupId == item.Id && x.PerformanceAppraisal.KPIId == element.Id).FirstOrDefault();
                    KPIDto kpidto = new KPIDto();
                    kpidto.Id = element.Id;
                    kpidto.Name = element.Name;
                    kpidto.PerformanceAppraisalId = self_eve.PerformanceAppraisalId;
                    kpidto.KPIMarks = self_eve.KPIMarks;
                    kpidto.KPIMarksEvaluator1 = superOne_eve.KPIMarks;
                    kpidto.KPIMarksEvaluator2 = 0;
                    grpdto.KPIList.Add(kpidto);
                }

                list.Add(grpdto);
            }
            evaldto.KPIGroupList = list;
            return evaldto;
        }
        public bool IsTopmost(long empid)
        {
            var emp = GenService.GetAll<OrganoGram>().Where(x => x.EmployeeId == empid).FirstOrDefault();
            if (emp.DeptHeadId == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsEvaluationSubmitted(long empid, long year)
        {
            var eval = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == empid && x.EvaluationYear == year).FirstOrDefault();
            if (eval != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsEvaluationForwarded(long empid, long year, long supervisorid)
        {
            var eval = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == empid && x.EvaluationYear == year && x.LastEvaluatedBy == supervisorid).FirstOrDefault();
            if (eval != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void SubmitSelfEvaluation(EmployeeEvaluationDto evaldto)
        {
            var mappings = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year && x.IsGroupSelected == true && x.IsKPISelected == true);
            foreach (var item in evaldto.KPIGroupList)
            {
                foreach (var element in item.KPIList)
                {
                    var mapping = mappings.Where(p => p.KPIGroupId == item.Id && p.KPIId == element.Id).FirstOrDefault();
                    EmployeeEvaluation empeval = new EmployeeEvaluation();
                    empeval.EmployeeId = evaldto.EmployeeId;
                    empeval.PerformanceAppraisalId = mapping.Id;
                    empeval.KPIMarks = element.KPIMarks;
                    empeval.EvaluatedBy = evaldto.EmployeeId;
                    empeval.ForwardedTo = evaldto.SeniorId;
                    GenService.Save(empeval);
                }
            }
            GenService.Save(new EvaluationMasterDetail
            {
                EmployeeId = evaldto.EmployeeId,
                EvaluationYear = DateTime.Today.Year,
                EvaluationStatus = EmployeeEvaluationStatus.Submitted,
                LastForwardedTo = evaldto.SeniorId,
                LastEvaluatedBy = evaldto.EmployeeId,
                Remarks = evaldto.Remarks,
                Recommendation = evaldto.Recommendation
            });
            GenService.SaveChanges();
        }

        public void SubmitSupervisorOneEvaluation(EmployeeEvaluationDto evaldto, long evaluatorid)
        {
            // var mappings = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year && x.IsGroupSelected==true && x.IsKPISelected==true);
            foreach (var item in evaldto.KPIGroupList)
            {
                foreach (var element in item.KPIList)
                {
                    //var mapping = mappings.Where(p => p.KPIGroupId == item.Id && p.KPIId == element.Id).FirstOrDefault();
                    EmployeeEvaluation empeval = new EmployeeEvaluation();
                    empeval.EmployeeId = evaldto.EmployeeId;
                    empeval.PerformanceAppraisalId = element.PerformanceAppraisalId;
                    empeval.KPIMarks = element.KPIMarksEvaluator1;
                    empeval.EvaluatedBy = evaluatorid;
                    if (evaldto.SeniorId == 0)
                    {
                        empeval.ForwardedTo = null;
                    }
                    else
                    {
                        empeval.ForwardedTo = evaldto.SeniorId;
                    }
                    GenService.Save(empeval);
                }

            }
            var evalFromMaster = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == evaldto.EmployeeId && x.EvaluationYear == DateTime.Today.Year).FirstOrDefault();
            evalFromMaster.EvaluationStatus = evaldto.SeniorId == 0 ? EmployeeEvaluationStatus.Done : EmployeeEvaluationStatus.Forwarded;
            if (evaldto.SeniorId == 0)
            {
                evalFromMaster.LastForwardedTo = null;
            }
            else
            {
                evalFromMaster.LastForwardedTo = evaldto.SeniorId;
            }
            evalFromMaster.LastEvaluatedBy = evaluatorid;
            evalFromMaster.Remarks = evaldto.Remarks;
            evalFromMaster.Recommendation = evaldto.Recommendation;
            GenService.SaveChanges();

        }

        public void SubmitSupervisorTwoEvaluation(EmployeeEvaluationDto evaldto, long evaluatorid)
        {
            // var mappings = GenService.GetAll<PerformanceAppraisal>().Where(x => x.DesignationId == evaldto.DesignationId && x.FromDate.Year == DateTime.Today.Year && x.IsGroupSelected==true && x.IsKPISelected==true);
            foreach (var item in evaldto.KPIGroupList)
            {
                foreach (var element in item.KPIList)
                {
                    //var mapping = mappings.Where(p => p.KPIGroupId == item.Id && p.KPIId == element.Id).FirstOrDefault();
                    EmployeeEvaluation empeval = new EmployeeEvaluation();
                    empeval.EmployeeId = evaldto.EmployeeId;
                    empeval.PerformanceAppraisalId = element.PerformanceAppraisalId;
                    empeval.KPIMarks = element.KPIMarksEvaluator2;
                    empeval.EvaluatedBy = evaluatorid;
                    empeval.ForwardedTo = null;
                    GenService.Save(empeval);
                }

            }
            var evalFromMaster = GenService.GetAll<EvaluationMasterDetail>().Where(x => x.EmployeeId == evaldto.EmployeeId && x.EvaluationYear == DateTime.Today.Year).FirstOrDefault();
            evalFromMaster.EvaluationStatus = EmployeeEvaluationStatus.Done;
            evalFromMaster.LastForwardedTo = null;
            evalFromMaster.LastEvaluatedBy = evaluatorid;
            evalFromMaster.Remarks = evaldto.Remarks;
            evalFromMaster.Recommendation = evaldto.Recommendation;
            GenService.SaveChanges();

        }

        public List<EvaluationMasterDetailDto> GetPendingEvaluationList(long empid)
        {
            var des = GenService.GetById<Employee>(empid).Designation;
            var pendings = GenService.GetAll<EvaluationMasterDetail>().Where(x => (x.EvaluationStatus == EmployeeEvaluationStatus.Submitted || x.EvaluationStatus == EmployeeEvaluationStatus.Forwarded) && x.LastForwardedTo == empid);
            List<EvaluationMasterDetailDto> list = new List<EvaluationMasterDetailDto>();
            foreach (var item in pendings)
            {
                EvaluationMasterDetailDto dto = new EvaluationMasterDetailDto();
                dto.EmployeeId = item.EmployeeId;
                dto.EmployeeName = GenService.GetById<Employee>(item.EmployeeId).Person.FirstName + " " + GenService.GetById<Employee>(item.EmployeeId).Person.LastName;
                dto.DesignationName = des.Name;
                dto.EvaluationYear = item.EvaluationYear;
                dto.EvaluationStatus = (int)item.EvaluationStatus;
                dto.EvaluationStatusName = item.EvaluationStatus.ToString();
                dto.LastForwardedTo = item.LastForwardedTo;
                dto.LastEvaluatedBy = item.LastEvaluatedBy;
                dto.LastEvaluatedByName = GenService.GetById<Employee>(item.LastEvaluatedBy).Person.FirstName + " " + GenService.GetById<Employee>(item.LastEvaluatedBy).Person.LastName;
                dto.Remarks = item.Remarks;
                dto.Recommendation = item.Recommendation;
                list.Add(dto);
            }
            return list;
        }


        public List<EmployeeDto> GetSeniorsList(int id)
        {
            List<OrganoGram> orgs = new List<OrganoGram>();
            OrganoGram emp = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == id).FirstOrDefault();
            OrganoGram boss1 = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == emp.DeptHeadId).FirstOrDefault();
            if (boss1 != null)
            {
                orgs.Add(boss1);
                OrganoGram boss2 = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == boss1.DeptHeadId).FirstOrDefault();
                if (boss2 != null)
                {
                    orgs.Add(boss2);
                }

            }
            var data = orgs.Select(x => new EmployeeDto
            {
                Id = x.EmployeeId,
                Name = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName
            }).ToList();
            return data;

        }

        public EvaluationWeightSetupDto GetEvaluationWeightSetup()
        {
            var setup = GenService.GetAll<EvaluationWeightSetup>().Where(x => x.IsActive).ToList();
            EvaluationWeightSetupDto setupDto = new EvaluationWeightSetupDto();
            if (setup.Any())
            {
                var singleSetup= setup.FirstOrDefault();
                setupDto.Id = singleSetup.Id;
                setupDto.SettingDate = singleSetup.SettingDate;
                setupDto.SelfWeight = singleSetup.SelfWeight;
                setupDto.Supervisor1Weight = singleSetup.Supervisor1Weight;
                setupDto.Supervisor2Weight = singleSetup.Supervisor2Weight;
            }

            return setupDto;
        }

        public void SaveEvaluationWeightSetup(EvaluationWeightSetupDto dto)
        {
            var setup = GenService.GetAll<EvaluationWeightSetup>().Where(x => x.IsActive == true).FirstOrDefault();
            if (setup != null)
            {
                setup.IsActive = false;
            }

            GenService.Save(new EvaluationWeightSetup
            {
                SettingDate = DateTime.Today,
                SelfWeight = dto.SelfWeight,
                Supervisor1Weight = dto.Supervisor1Weight,
                Supervisor2Weight = dto.Supervisor2Weight,
                IsActive = true
            });

            GenService.SaveChanges();
        }

    }
}
