﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using System.Globalization;
using AutoMapper;
using Finix.Auth.Facade;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;


namespace Finix.HRM.Facade
{
    public class LeaveSupervisionFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private UserFacade _user = new UserFacade();
        public List<LeaveApplicationDto> GetApplicationsForSupervisions()
        {
            var leaveapplications = GenService.GetAll<LeaveApplication>().ToList();
            List<LeaveApplicationDto> list = new List<LeaveApplicationDto>();

            foreach (var item in leaveapplications)
            {
                LeaveApplicationDto dto = new LeaveApplicationDto();
                dto.Id = item.Id;
                dto.EmployeeName = item.Employee.Person.FirstName + " " + item.Employee.Person.LastName;
                dto.OfficeUnitName = item.Employee.OfficeUnit == null ? null : item.Employee.OfficeUnit.Name;
                dto.Description = item.Description;
                dto.LeaveTypeName = item.LeaveType.Name;
                dto.Range = item.FromDate.ToString("ddd, MMM d, yyyy") + "-" + item.ToDate.ToString("ddd, MMM d, yyyy");
                dto.TotalDays = (long)item.TotalDays;
                dto.Comments = item.Comments;
                dto.CurrentLeaveStatusName = item.CurrentLeaveStatus.ToString();
                list.Add(dto);
            }

            return list;
        }

        public void ModifySupervisedLeaveApplication(LeaveSupervisionDto leavesupervisiondto)
        {
            int status = 0;
            if (leavesupervisiondto.ActionType == 5)
            {
                leavesupervisiondto.ActionType = (int)LeaveSupervisionType.Cancelled;
                status = 5;
            }
            else if (leavesupervisiondto.ActionType == 2)
            {
                leavesupervisiondto.ActionType = (int)LeaveSupervisionType.Approved;
                status = 2;
            }

            GenService.Save(new LeaveSupervision
            {
                LeaveApplicationId = leavesupervisiondto.LeaveApplicationId,
                SupervisorId = null,
                ActionType = (LeaveSupervisionType)leavesupervisiondto.ActionType,
                Comments = leavesupervisiondto.SupervisorComments,

            });
            LeaveApplication app = GenService.GetById<LeaveApplication>(leavesupervisiondto.LeaveApplicationId);
            app.CurrentLeaveStatus = (LeaveApplicationStatus)status;
            GenService.SaveChanges();
        }

        public List<object> GetCommentHistory()
        {
            var list = GenService.GetAll<LeaveSupervision>();
            List<object> commentlist = new List<object>();

            foreach (var item in list)
            {
                var comment = new { Commenter = "Supervisor", Comment = item.Comments };
                commentlist.Add(comment);
            }
            return commentlist;
        }

        public List<EmployeeLeaveStatusDto> GetAllEmployees()
        {
            var employees = GenService.GetAll<Employee>();
            var data = employees.Select(x => new EmployeeLeaveStatusDto
            {
                EmployeeId = x.Id,
                EmployeeName = x.Person.FirstName + " " + x.Person.LastName,
                DesignationName = x.Designation.Name,
                OfficeUnitName = x.OfficeUnit.Name,
                EmployeeTypeName = x.EmployeeType.ToString()
            }).ToList();
            return data;
        }
        public List<LeaveStatusDto> GetLeaveStatusesForEmployee(int empid)
        {
            Employee emp = GenService.GetById<Employee>(empid);
            var leaveapplications = GenService.GetAll<LeaveApplication>().Where(x => x.EmployeeId == empid);
            var leaveTypeIdList = leaveapplications.Select(x => x.LeaveTypeId).ToList();
            var leavetypes = new List<LeaveStatusDto>();
            var leaveTypes =
                from leaveapplication in leaveapplications
                group leaveapplication by leaveapplication.LeaveTypeId into newGroup
                orderby newGroup.Key
                select newGroup;

            foreach (var typeGroup in leaveTypes)
            {
                int enjoyed = 0, processing = 0; LeaveType ltype = new LeaveType(); int td = 0; int remaining = 0;
                //Console.WriteLine("Key: {0}", nameGroup.Key);
                foreach (var type in typeGroup)
                {
                    ltype = GenService.GetById<LeaveType>(type.LeaveTypeId);
                    td += ltype.LeaveDays;

                    if (type.CurrentLeaveStatus == LeaveApplicationStatus.ApprovedByHr || type.CurrentLeaveStatus == LeaveApplicationStatus.ApprovedByDept)
                    {
                        enjoyed += (int)type.TotalDays; //ltype.LeaveDays;
                        processing += 0;
                        remaining += td - enjoyed;
                    }
                    else if (type.CurrentLeaveStatus == LeaveApplicationStatus.Applied || type.CurrentLeaveStatus == LeaveApplicationStatus.Halted || type.CurrentLeaveStatus == LeaveApplicationStatus.Draft)
                    {
                        processing += ltype.LeaveDays;
                        enjoyed += 0;
                        remaining += ltype.LeaveDays;
                    }
                    // var LeaveTypeName = ltype.Name;

                }
                leavetypes.Add(new LeaveStatusDto { LeaveTypeName = ltype.Name, LeaveDays = td, MaxCarryForward = ltype.MaxCarryForward, Enjoyed = enjoyed, Processing = processing, Remaining = remaining });
            }
            return leavetypes;

        }
        //public List<LeaveStatusDto> GetLeaveStatusesForEmployee_old(int empid)
        //{
        //    Employee emp = GenService.GetById<Employee>(empid);
        //    LeaveApplication leaveapplication=GenService.GetAll<LeaveApplication>().FirstOrDefault(x=>x.EmployeeId==empid);
        //    List<LeaveType> leavetypes;
        //    if (emp.Person.Gender==Gender.Male)
        //    {
        //        leavetypes = GenService.GetAll<LeaveType>().Where(x => x.LeaveApplicableTo == LeaveApplicableTo.Male || x.LeaveApplicableTo==LeaveApplicableTo.All).ToList();
        //    }
        //    else
        //    {
        //        leavetypes = GenService.GetAll<LeaveType>().Where(x => x.LeaveApplicableTo == LeaveApplicableTo.Female || x.LeaveApplicableTo == LeaveApplicableTo.All).ToList();
        //    }
        //    var data = leavetypes.Select(x => new LeaveStatusDto
        //    {
        //        LeaveTypeName = x.Name,
        //        LeaveDays = x.LeaveDays,
        //        MaxCarryForward = x.MaxCarryForward,
        //        Enjoyed = leaveapplication.CurrentLeaveStatus == LeaveApplicationStatus.ApprovedByHr ? (int)leaveapplication.TotalDays : 0,
        //        Processing = leaveapplication.CurrentLeaveStatus == LeaveApplicationStatus.Applied ? (int)leaveapplication.TotalDays : 0,
        //        Remaining = leaveapplication.CurrentLeaveStatus == LeaveApplicationStatus.ApprovedByHr && x.LeaveDays > leaveapplication.TotalDays ? x.LeaveDays - (int)leaveapplication.TotalDays : 0
        //    }).ToList();
        //    return data;
        //}

        public List<LeaveApplicationDto> GetLeaveApplicationForSession(int empid)
        {
            var check = GenService.GetAll<LeaveSupervision>();
            var leaveapps = GenService.GetAll<LeaveApplication>().Where(x => x.EmployeeId == empid);
            var data = (from lvSup in GenService.GetAll<LeaveSupervision>()
                        join lvApp in leaveapps on lvSup.LeaveApplicationId equals lvApp.Id into extra
                        from ex in extra.DefaultIfEmpty()
                        select new LeaveApplicationDto
                        {
                            Id = ex != null ? ex.Id : 0,
                            EmployeeName = ex.Employee.Person.FirstName + " " + ex.Employee.Person.LastName,
                            FromDate = ex.FromDate,
                            ToDate = ex.ToDate,
                            TotalDays = ex.TotalDays,
                            LeaveTypeId = ex != null ? ex.LeaveTypeId : 0,
                            LeaveTypeName = ex.LeaveType.Name,
                            CurrentLeaveStatus = (int)ex.CurrentLeaveStatus,
                            CurrentLeaveStatusName = ex.CurrentLeaveStatus.ToString(),
                            Description = ex.Description,
                            SubstitutorId = ex != null ? ex.SubstitutorId : 0,
                            SubstitutorName = ex.Substitutor.Person.FirstName + " " + ex.Substitutor.Person.LastName,
                            Address = ex.Address,
                            Email = ex.Email,
                            Phone = ex.Phone,
                            Comments = ex.Comments,
                            SeniorId = lvSup != null ? lvSup.SupervisorId : 0
                        }).Where(i => i.Id != 0).ToList();
            return data;

        }

        public LeaveApplicationDto GetLeaveApplicationDetailForSession(int leaveappid)
        {
            LeaveApplicationDto dto = new LeaveApplicationDto();
            var leaveapp = GenService.GetById<LeaveApplication>(leaveappid);
            dto.Description = leaveapp.Description;
            //dto.SubstitutorId = leaveapp.SubstitutorId;
            dto.SubstitutorName = leaveapp.SubstitutorId == null ? null : leaveapp.Substitutor.Person.FirstName + " " + leaveapp.Substitutor.Person.LastName;
            dto.Address = leaveapp.Address;
            dto.Email = leaveapp.Email;
            dto.Phone = leaveapp.Phone;
            dto.Comments = leaveapp.Comments;
            return dto;
        }

        public List<LeaveApplicationDto> GetLeaveApplicationsForReport(long? monthId, long? leaveTypeId, DateTime? fromDate, DateTime? toDate)
        {
            var leaveapplications = GenService.GetAll<LeaveApplication>().ToList();
            if (leaveTypeId != null)
            {
                leaveapplications = leaveapplications.Where(i => i.LeaveTypeId == leaveTypeId).ToList();
            }
            if (fromDate != null)
            {
                leaveapplications = leaveapplications.Where(i => i.FromDate >= fromDate).ToList();
            }
            if (toDate != null)
            {
                leaveapplications = leaveapplications.Where(i => i.FromDate <= toDate).ToList();
            }
            if (monthId != null)
            {
                leaveapplications = leaveapplications.Where(i => i.FromDate.Month == monthId).ToList();
            }
            var result = leaveapplications.Select(x => new LeaveApplicationDto
            {
                Id = x.Id,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                OfficeUnitName = x.Employee.OfficeUnit == null ? null : x.Employee.OfficeUnit.Name,
                Description = x.Description,
                LeaveTypeName = x.LeaveType.Name,
                Range = x.FromDate.ToString("ddd, MMM d, yyyy") + "-" + x.ToDate.ToString("ddd, MMM d, yyyy"),
                TotalDays = (long)x.TotalDays,
                Comments = x.Comments,
                CurrentLeaveStatusName = x.CurrentLeaveStatus.ToString()
            });
            return result.ToList();
        }

        public List<LeaveApplicationDto> GetLeaveApplicationsForOfficeReport(long? officeId)
        {
            List<LeaveApplicationDto> data = new List<LeaveApplicationDto>();
            var leaveapplications = GenService.GetAll<LeaveApplication>().ToList();
            var result = leaveapplications.Select(x => new LeaveApplicationDto
            {
                Id = x.Id,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                OfficeUnitName = x.Employee.OfficeUnit == null ? null : x.Employee.OfficeUnit.Name,
                OfficeId = x.Employee.OfficeId == 0 ? 0 : (long)x.Employee.OfficeId,
                OfficeName = x.Employee.Office.Name,
                Description = x.Description,
                LeaveTypeName = x.LeaveType.Name,
                EntitledLeaveDays = x.LeaveType.LeaveDays,
                Range = x.FromDate.ToString("ddd, MMM d, yyyy") + "-" + x.ToDate.ToString("ddd, MMM d, yyyy"),
                TotalDays = (long)x.TotalDays,
                Comments = x.Comments,
                CurrentLeaveStatusName = x.CurrentLeaveStatus.ToString()
            }).Where(r => r.OfficeId == officeId);

            var groupLeaveApplication = from d in result
                                        group d by new
                                        {
                                            d.EmployeeName,
                                            d.OfficeUnitName,
                                            d.OfficeId,
                                            d.OfficeName,
                                            d.LeaveTypeName,
                                        }
                                            into grp
                                            select new
                                            {

                                                EmployeeName = grp.Key.EmployeeName,
                                                OfficeUnitName = grp.Key.OfficeUnitName,
                                                OfficeId = grp.Key.OfficeId,
                                                OfficeName = grp.Key.OfficeName,
                                                LeaveTypeName = grp.Key.LeaveTypeName,
                                                EntitledLeaveDays = grp.ToList().OrderByDescending(x => x.EntitledLeaveDays).FirstOrDefault(),
                                                TotalDays = grp.Sum(c => c.TotalDays),
                                                LeaveTaken = 0
                                            };
            foreach (var key in groupLeaveApplication)
            {
                key.EntitledLeaveDays.LeaveTaken = (long)(key.EntitledLeaveDays.EntitledLeaveDays - key.TotalDays);
                key.EntitledLeaveDays.TotalDays = key.TotalDays;
                data.Add(key.EntitledLeaveDays);
            }

            return data.ToList();
        }

        public List<LeaveApplicationDto> GetLeaveApplicationsForApproval(long id)
        {
            var orgno = GenService.GetAll<LeaveSupervision>().Where(r => r.SupervisorId == id);
            var dummy = new LeaveSupervision();
            var leaveData = GenService.GetAll<LeaveApplication>().Where(r => r.CurrentLeaveStatus == LeaveApplicationStatus.Applied);
            var data = (from lv in leaveData
                        join org in orgno on lv.Id equals org.LeaveApplicationId into Extra
                        from ex in Extra.DefaultIfEmpty()
                        select new LeaveApplicationDto
                        {
                            Id = lv.Id,
                            EmployeeId = lv.EmployeeId,
                            EmployeeName = lv.Employee.Person.FirstName + " " + lv.Employee.Person.LastName,
                            FromDate = lv.FromDate,
                            ToDate = lv.ToDate,
                            TotalDays = lv.TotalDays,
                            LeaveTypeId = lv.LeaveTypeId,
                            LeaveTypeName = lv.LeaveType.Name,
                            Description = lv.Description,
                            CurrentLeaveStatus = (int)lv.CurrentLeaveStatus,
                            CurrentLeaveStatusName = lv.CurrentLeaveStatus.ToString(),
                            SubstitutorId = lv.SubstitutorId,
                            SubstitutorName = lv.Substitutor.Person.FirstName + " " + lv.Substitutor.Person.LastName,
                            Address = lv.Address,
                            Email = lv.Email,
                            Phone = lv.Phone,
                            Comments = lv.Comments,
                            ParentId = ex != null && ex.SupervisorId != null ? (long)ex.SupervisorId : 0
                        }).Where(p => p.ParentId != 0 && p.ParentId == id);
            return data.ToList();

        }

        public List<EmployeeDto> GetSeniorsList(int id)
        {
            List<OrganoGram> aDtos = new List<OrganoGram>();
            OrganoGram boss1 = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == id).FirstOrDefault();
            if (boss1 != null && boss1.Parent != null)
            {
                //aDtos.Add(boss1);
                OrganoGram boss2 = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == boss1.Parent.EmployeeId).FirstOrDefault();
                if (boss2 != null)
                {
                    aDtos.Add(boss2);
                }
            }
            var data = (from org in aDtos
                        join emp in GenService.GetAll<Employee>() on org.EmployeeId equals emp.Id into extra
                        from ext in extra.DefaultIfEmpty()
                        select new EmployeeDto()
                        {
                            Id = ext != null ? ext.Id : 0,
                            Name = ext != null ? ext.Person.FirstName + " " + ext.Person.LastName : ""
                        }).Where(r => r.Id != id).ToList();
            var result = from d in data
                         group d by new { d.Id, d.Name }
                             into grp
                             select new EmployeeDto
                                          {
                                              Id = grp.Key.Id,
                                              Name = grp.Key.Name

                                          };
            return result.ToList();

        }

        public ResponseDto SaveLeaveSupervisor(LeaveApplicationDto leaveApplication)
        {
            //throw new NotImplementedException();
            ResponseDto response = new ResponseDto();
            LeaveSupervision entity = new LeaveSupervision();
            var leaveApp = new LeaveApplication();
            try
            {
                entity = _service.GetAll<LeaveSupervision>().Where(i => i.LeaveApplicationId == (int)leaveApplication.Id).FirstOrDefault();
                //entity.LeaveApplicationId = leaveApplication.Id;
                entity.SupervisorId = leaveApplication.SeniorId;
                entity.EditDate = DateTime.Now;
                entity.ActionType = LeaveSupervisionType.Forward;
                //entity.Status = EntityStatus.Active;

                leaveApp = _service.GetById<LeaveApplication>(leaveApplication.Id);
                leaveApp.CurrentLeaveStatus = LeaveApplicationStatus.Forward;

            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }
            _service.Save(leaveApp);
            _service.Save(entity);

            response.Success = true;
            response.Message = "Leave Supervision Saved Successfully";
            return response;
        }

        public long GetEmployeeId(long userId)
        {
            var employeeId = _user.GetEmployeeIdByUserId(userId);
            return employeeId;
            //long result = _service.GetAll<User>().Where(r => r.Id == userId).Select(r => r.EmployeeId).FirstOrDefault();
            //return result;

        }
    }
}
