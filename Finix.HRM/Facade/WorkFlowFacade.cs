﻿using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class WorkFlowFacade:BaseFacade
    {
        public List<WFSettingDto> getAllWFSettings()
        {
            var workflowsettings = GenService.GetAll<WFSetting>();
            var data = workflowsettings.Select(x => new WFSettingDto
            {
                Id = x.Id,
                Name=x.Name,
                WFTypeName = x.WFType.ToString(),
                TotalSteps = x.TotalSteps,
                TotalActiveWF = x.TotalActiveWF,
                Description = x.Description,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                OfficeUnitId = x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name
            }).ToList();
            return data;
        }

        public void SaveWFSetting(WFSettingDto wfsettingdto)
        {
            WFSetting wfsetting = GenService.GetById<WFSetting>(wfsettingdto.Id);
            if (wfsettingdto.Id>0)
            {
                wfsetting.WFType =(WFTypes)wfsettingdto.WFType;
                wfsetting.TotalSteps = wfsettingdto.TotalSteps;
                wfsetting.TotalActiveWF = wfsettingdto.TotalActiveWF;
                wfsetting.OfficeId = wfsettingdto.OfficeId;
                wfsetting.OfficeUnitId = wfsettingdto.OfficeUnitId;
                wfsetting.Description = wfsettingdto.Description;
            }
            else
            {
                GenService.Save(new WFSetting
                {
                    WFType=(WFTypes)wfsettingdto.WFType,
                    TotalSteps=wfsettingdto.TotalSteps,
                    TotalActiveWF=wfsettingdto.TotalActiveWF,
                    OfficeId=wfsettingdto.OfficeId,
                    OfficeUnitId=wfsettingdto.OfficeUnitId,
                    Description=wfsettingdto.Description
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteWFSetting(int id)
        {
            GenService.Delete<WFSetting>(id);
            GenService.SaveChanges();
        }

        public List<WFStepDto> GetAllWFSteps()
        {
            var wfsteps = GenService.GetAll<WFStep>();
            var data = wfsteps.Select(x => new WFStepDto
            {
                Id=x.Id,
                WFSettingId=x.WFSettingId,
                WFSettingName=x.WFSetting.Name,
                StepNo=x.StepNo,
                ExecutorId=x.ExecutorId,
                ExecutorName=x.Executor.Person.FirstName+" "+x.Executor.Person.LastName,
                ExecutorType=(int)x.ExecutorType,
                ExecutorTypeName=x.ExecutorType.ToString()
            }).ToList();
            return data;
        }

        public void SaveWFStep(WFStepDto wfstepdto)
        {
            WFStep wfstep = GenService.GetById<WFStep>(wfstepdto.Id);
            if (wfstepdto.Id>0)
            {
                wfstep.WFSettingId = wfstepdto.WFSettingId;
                wfstep.StepNo = wfstepdto.StepNo;
                wfstep.ExecutorId = wfstepdto.ExecutorId;
                wfstep.ExecutorType =(WFActorType)wfstepdto.ExecutorType;

            }
            else
            {
                GenService.Save(new WFStep
                {
                    WFSettingId=wfstepdto.WFSettingId,
                    StepNo=wfstepdto.StepNo,
                    ExecutorId=wfstepdto.ExecutorId,
                    ExecutorType=(WFActorType)wfstepdto.ExecutorType
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteWFStep(int id)
        {
            GenService.Delete<WFStep>(id);
            GenService.SaveChanges();
        }

        public List<WorkFlowDto> GetAllWorkFlows()
        {
            var workflows = GenService.GetAll<WorkFlow>();
            var data = workflows.Select(x => new WorkFlowDto
            {
                Id=x.Id,
                WFSettingId=x.SettingId,
                WFSettingName=x.WFSetting.Name,
                WFStepId=x.StepId,
                ObjId=x.ObjId,
                WFStatus=(int)x.WFStatus,
                WFStatusName=x.WFStatus.ToString(),
                Comments=x.Comments
            }).ToList();
            return data;
        }

        public void SaveWorkFlow(WorkFlowDto workflowdto)
        {
            WorkFlow wf = GenService.GetById<WorkFlow>(workflowdto.Id);
            if (workflowdto.Id>0)
            {
                wf.SettingId = workflowdto.WFSettingId;
                wf.StepId = workflowdto.WFStepId;
                wf.WFStatus = (WFStatus)workflowdto.WFStatus;
                wf.ObjId=workflowdto.ObjId;
                wf.Comments = workflowdto.Comments;
            }
            else
            {
                GenService.Save(new WorkFlow
                {
                    SettingId = workflowdto.WFSettingId,
                    StepId = workflowdto.WFStepId,
                    ObjId = workflowdto.ObjId,
                    WFStatus = (WFStatus)workflowdto.WFStatus,
                    Comments = workflowdto.Comments
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteWorkFlow(int id)
        {
            GenService.Delete<WorkFlow>(id);
            GenService.SaveChanges();
        }
    }
}
