﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class EducationFacade : BaseFacade
    {
        public List<InstituteDto> GetInstitutes()
        {
            var institutes = GenService.GetAll<Institute>();
            var data = institutes.Select(x => new InstituteDto
            {
                Id = x.Id,
                Name = x.Name,
                ContactPerson = x.ContactPerson,
                ContactNo = x.ContactNo,
                Email = x.Email,
                PostalCode = x.Address.PostOffice.Code,
                //AddressLine1=x.Address.AddressLine1,
                //AddressLine2=x.Address.AddressLine2,
                AreaName = x.Address.PostOffice.Location.Name,
                //AreaId=x.Address.PostOffice.LocationId,
                //ThanaId=x.Address.PostOffice.Location.ParentLocation.Id,
                ThanaName = x.Address.PostOffice.Location.ParentLocation.Name,
                //DistrictId = x.Address.PostOffice.Location.ParentLocation.ParentLocation.Id,
                DistrictName = x.Address.PostOffice.Location.ParentLocation.ParentLocation.Name,
                //DivisionId = x.Address.PostOffice.Location.ParentLocation.ParentLocation.ParentLocation.Id,
                DivisionName = x.Address.PostOffice.Location.ParentLocation.ParentLocation.ParentLocation.Name
            }).ToList();
            return data;
        }

        public void SaveInstitute(InstituteDto institutedto)
        {
            if (institutedto.Id > 0)
            {
                var institute = GenService.GetById<Institute>(institutedto.Id);
                institute.Name = institutedto.Name;
                institute.ContactPerson = institutedto.ContactPerson;
                institute.ContactNo = institutedto.ContactNo;
                institute.Email = institutedto.Email;
                //institute.Address.AddressLine1 = institutedto.AddressLine1;
                //institute.Address.AddressLine2 = institutedto.AddressLine2;
                institute.Address.PostOfficeId = institutedto.PostalCode;
            }
            else
            {
                GenService.Save(new Institute
                {
                    Name = institutedto.Name,
                    ContactPerson = institutedto.ContactPerson,
                    ContactNo = institutedto.ContactNo,
                    Email = institutedto.Email,
                    Address = new Address
                    {
                        //AddressLine1=institutedto.AddressLine1,
                        //AddressLine2=institutedto.AddressLine2,
                        PostOfficeId = institutedto.PostalCode

                    }

                });
            }
            GenService.SaveChanges();
        }

        public void DeleteInstitute(int id)
        {
            GenService.Delete<Institute>(id);
            GenService.SaveChanges();
        }

        public List<DegreeInfoDto> GetDegrees()
        {
            var degrees = GenService.GetAll<Degree>();
            var data = degrees.Select(x => new DegreeInfoDto
            {
                Id = x.Id,
                Name = x.Name,
                DegreeLevel = (int)x.DegreeLevel,
                DegreeLevelName = x.DegreeLevel.ToString(),
                DegreeMajor = (int)x.DegreeMajor,
                DegreeMajorName = x.DegreeMajor.ToString(),
                Duration = x.DurationInYear,

            }).ToList();
            return data;
        }

        public void SaveDegree(DegreeInfoDto degreeinfodto)
        {
            if (degreeinfodto.Id > 0)
            {
                var degree = GenService.GetById<Degree>(degreeinfodto.Id);
                degree.Name = degreeinfodto.Name;
                degree.DegreeLevel = (DegreeLevel)degreeinfodto.DegreeLevel;
                degree.DegreeMajor = (DegreeMajor)degreeinfodto.DegreeMajor;
                degree.DurationInYear = degreeinfodto.Duration;

            }
            else
            {
                GenService.Save(new Degree
                {
                    Name = degreeinfodto.Name,
                    DegreeLevel = (DegreeLevel)degreeinfodto.DegreeLevel,
                    DegreeMajor = (DegreeMajor)degreeinfodto.DegreeMajor,
                    DurationInYear = degreeinfodto.Duration,

                });
            }
            GenService.SaveChanges();
        }

        public void DeleteDegree(int id)
        {
            GenService.Delete<Degree>(id);
            GenService.SaveChanges();
        }

        public List<EmployeeEducationDto> GetEmpEducations(int id)
        {
            var educations = GenService.GetAll<EmployeeEducation>().Where(x => x.EmployeeId == id);
            var data = educations.Select(x => new EmployeeEducationDto
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                DegreeId = x.DegreeId,
                DegreeName = x.Degree.Name,
                InstituteId = x.InstituteId,
                InstituteName = x.Institute.Name,
                PassingYear = x.PassingYear,
                Result = x.Result,
                Summary = x.Summary
            }).ToList();
            return data;
        }

        public void SaveEmpEducation(EmployeeEducationDto empeducationdto)
        {
            if (empeducationdto.Id > 0)
            {
                var edu = GenService.GetById<EmployeeEducation>(empeducationdto.Id);
                edu.EmployeeId = empeducationdto.EmployeeId;
                edu.DegreeId = empeducationdto.DegreeId;
                edu.InstituteId = empeducationdto.InstituteId;
                edu.PassingYear = empeducationdto.PassingYear;
                edu.Result = empeducationdto.Result;
                edu.Summary = empeducationdto.Summary;
            }
            else
            {
                GenService.Save(new EmployeeEducation
                {
                    EmployeeId = empeducationdto.EmployeeId,
                    DegreeId = empeducationdto.DegreeId,
                    InstituteId = empeducationdto.InstituteId,
                    PassingYear = empeducationdto.PassingYear,
                    Result = empeducationdto.Result,
                    Summary = empeducationdto.Summary
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteEmpEducation(int id)
        {
            GenService.Delete<EmployeeEducation>(id);
            GenService.SaveChanges();
        }


        public List<DegreeInfoDto> GetDegreeByInstitute(int? instituteid)
        {
            //var institute = GenService.GetById<Institute>(instituteid);
            //var degrees = institute.Degrees;
            var degrees = GenService.GetAll<Degree>();
            var data = degrees.Select(x => new DegreeInfoDto
            {
                Id = x.Id,
                Name = x.Name

            }).ToList();
            return data;
        }

        public List<EmployeeEducationDto> GetAllEducations()
        {
            var educations = GenService.GetAll<EmployeeEducation>();
            var data = educations.Select(x => new EmployeeEducationDto
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                DegreeId = x.DegreeId,
                DegreeName = x.Degree.Name,
                InstituteId = x.InstituteId,
                InstituteName = x.Institute.Name,
                PassingYear = x.PassingYear,
                Result = x.Result,
                Summary = x.Summary
            }).ToList();
            return data;
        }

        public List<DegreeInfoDto> GetAllDegrees()
        {
            var degrees = GenService.GetAll<Degree>();

            var data = degrees.Select(x => new DegreeInfoDto
            {
                Id = x.Id,
                Name = x.Name

            }).ToList();
            return data;
        }
    }
}
