﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.Accounts.DTO;
using Finix.Auth.Facade;
using Finix.HRM.Service;

namespace Finix.HRM.Facade
{
    public class SalaryProcessFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private readonly OfficeFacade officefacade;
        private readonly HRMAccountsFacade _accounts = new HRMAccountsFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        public SalaryProcessFacade()
        {
            officefacade = new OfficeFacade();

        }
        public List<DesignationDto> GetDesignationList()
        {
            var data = _service.GetAll<Designation>();
            return Mapper.Map<List<DesignationDto>>(data);
        }

        public List<SalaryItemDto> GetDesignationWiseGradestep(int designationId)
        {
            var slaryItem = _service.GetAll<SalaryItem>().Where(r => r.Id != (int)LateAttendanceSalaryItem.Overtime && r.Id != (int)LateAttendanceSalaryItem.LateSalaryItem);
            return Mapper.Map<List<SalaryItemDto>>(slaryItem);
        }

        public List<OfficeUnitDto> GetDesignationWiseOfficeUnit(int designationId)
        {
            var employees = _service.GetAll<Employee>().Where(r => r.DesignationId == designationId);
            var result = (from emp in employees
                          join offUnit in _service.GetAll<OfficeUnit>() on emp.OfficeUnitId equals offUnit.Id
                          select new OfficeUnitDto()
                          {
                              Id = offUnit != null ? offUnit.Id : 0,
                              Name = offUnit != null ? offUnit.Name : ""
                          }).ToList();
            var grpResult = from d in result
                            group d by new { d.Id, d.Name }
                                into grp
                            select new OfficeUnitDto
                            {
                                Id = grp.Key.Id,
                                Name = grp.Key.Name,
                            };
            return grpResult.ToList();
        }

        public List<OfficeDto> GetDesignationWiseOffice(int officeUnit)
        {
            var employees = _service.GetAll<Employee>(); //.Where(r => r.OfficeUnitId == officeUnit)
            var result = (from emp in employees
                          join offUnit in _service.GetAll<Office>() on emp.OfficeId equals offUnit.Id
                          select new OfficeDto()
                          {
                              Id = offUnit != null ? offUnit.Id : 0,
                              Name = offUnit != null ? offUnit.Name : ""
                          }).ToList();
            // return result;
            var grpResult = from d in result
                            group d by new { d.Id, d.Name }
                                into grp
                            select new OfficeDto
                            {
                                Id = grp.Key.Id,
                                Name = grp.Key.Name,
                            };
            return grpResult.ToList();
        }

        public List<EmployeeDto> GetOfficeWiseEmployee(int officeId)
        {
            var employees = _service.GetAll<Employee>().Where(r => r.OfficeId == officeId);
            var empLoyeeGradeStepSalaryItem = (from emp in employees
                                               join gradeStp in _service.GetAll<GradeStep>() on emp.GradeStepId equals gradeStp.Id into extra
                                               from othStSal in extra.DefaultIfEmpty()
                                               select new
                                               {
                                                   EmployeeId = emp.Id,
                                                   EmployeeName=emp.Person.FirstName + " " + emp.Person.LastName,
                                                   DesignationId = emp.DesignationId,
                                                   GradeId = emp.Designation.GradeId,
                                                   GradeName = emp.Designation.Grade.Name,
                                                   GradeStepId = emp.GradeStepId != null ? emp.GradeStepId : 0,
                                                   GradeStepName = othStSal != null ? othStSal.StepName : ""
                                               });
            var checkGrade = empLoyeeGradeStepSalaryItem.Where(r => r.GradeStepId != 0);
            var result = (from emp in employees
                              //join offUnit in _service.GetAll<OfficeUnit>() on emp.OfficeUnitId equals offUnit.Id
                          select new EmployeeDto()
                          {
                              Id = emp != null ? emp.Id : 0,
                              Name = emp != null ? emp.Person.FirstName + " " + emp.Person.LastName : ""
                          }).ToList();
            //var grpResult = from d in result
            //                group d by new { d.Id, d.Name }
            //                    into grp
            //                select new EmployeeDto
            //                {
            //                    Id = grp.Key.Id,
            //                    Name = grp.Key.Name,
            //                };
            var grpResult = from d in checkGrade
                            group d by new { d.EmployeeId, d.EmployeeName }
                                into grp
                            select new EmployeeDto
                            {
                                Id = grp.Key.EmployeeId,
                                Name = grp.Key.EmployeeName,
                            };
            return grpResult.ToList();
            //return result;
        }

        public ResponseDto SaveSalaryProcess(SalaryProcessDto salaryProcessDto)
        {
            var salaryProcess = new SalaryProcess();
            var voucherList = new List<VoucherDto>();
            ResponseDto aResponseDto = new ResponseDto();
            //var salaryProcessDetails = new SalaryProcessDetail();
            var salaryProcessDetailsList = new List<SalaryProcessDetail>();
            //var salaryProcessDetailsList = new List<SalaryProcessDetailsDto>();;
            if (salaryProcessDto.FiscalYearId == 0)
            {
                aResponseDto.Message = "Please Provide Year";
                return aResponseDto;
            }
            salaryProcess.FiscalYearId =
                _service.GetAll<FiscalYears>().Where(r => r.Year == salaryProcessDto.FiscalYearId).Select(r => r.Id).FirstOrDefault();
            if (salaryProcess.FiscalYearId == 0)
            {
                aResponseDto.Message = "Fiscal Year Is Not Set";
                return aResponseDto;
            }
            salaryProcess.OfficeId = salaryProcessDto.OfficeId;
            if (salaryProcessDto.OfficeId == 0)
            {
                aResponseDto.Message = "Please Provide Office";
                return aResponseDto;
            }
            salaryProcess.OfficeUnitId =
                officefacade.GetOfficeUnitByOffice((int)salaryProcessDto.OfficeId).Select(r => r.Id).FirstOrDefault();
            if (salaryProcess.OfficeUnitId == 0)
            {
                aResponseDto.Message = "Please set office unit for this office";
                return aResponseDto;
            }
            if (salaryProcessDto.Month == 0)
            {
                aResponseDto.Message = "Please Provide Month";
                return aResponseDto;
            }
            salaryProcess.Month = salaryProcessDto.Month;
            salaryProcess.ProcessDate = DateTime.Now;
            salaryProcess.Status = (SalaryProcessStatus)EntityStatus.Active;
            var getSalProcess =
                _service.GetAll<SalaryProcess>()
                    .Where(
                        i =>
                            i.FiscalYearId == salaryProcess.FiscalYearId && i.Month == salaryProcess.Month &&
                            i.OfficeId == salaryProcess.OfficeId && i.OfficeUnitId == salaryProcess.OfficeUnitId);
            if (getSalProcess.Any())
            {

                aResponseDto.Success = false;
                aResponseDto.Message = "Salary Process Already Done For this Office.Please Contact With Admin";
                return aResponseDto;

            }

            var employees = _service.GetAll<Employee>().Where(r => r.OfficeId == salaryProcessDto.OfficeId).ToList();
            employees = employees.Where(r => r.GradeStepId != null).ToList();
            if (!employees.Any())
            {
                aResponseDto.Success = false;
                aResponseDto.Message = "Employee Doesn't Exist in the Office";
                return aResponseDto;
            }
            var empLoyeeGradeStepSalaryItem = (from emp in employees
                                               join gradeStp in _service.GetAll<GradeStep>() on emp.GradeStepId equals gradeStp.Id into extra
                                               from othStSal in extra.DefaultIfEmpty()
                                               select new
                                               {
                                                   EmployeeId = emp.Id,
                                                   DesignationId = emp.DesignationId,
                                                   GradeId = emp.Designation.GradeId,
                                                   GradeName = emp.Designation.Grade.Name,
                                                   GradeStepId = emp.GradeStepId != null ? emp.GradeStepId : 0,
                                                   GradeStepName = othStSal != null ? othStSal.StepName : ""
                                               });
            var checkGrade = empLoyeeGradeStepSalaryItem.Where(r => r.GradeStepId == 0);
            if (checkGrade.Any())
            {
                aResponseDto.Success = false;
                aResponseDto.Message = "No Grade Step is Set For The Following Grade " + empLoyeeGradeStepSalaryItem.Select(r => r.GradeName).FirstOrDefault();
                return aResponseDto;
            }
            else
            {
                var salaryItemCheck = (from grStep in empLoyeeGradeStepSalaryItem
                                       join gradeStp in _service.GetAll<GradeStepSalaryItem>().Where(r => r.Status == GradeStepSalaryStatus.Active) on grStep.GradeStepId equals
                                           gradeStp.GradeStepId
                                       //into extra
                                       //from othStSal in extra.DefaultIfEmpty()
                                       select new
                                       {
                                           GradeStepId = grStep.GradeStepId,
                                           GradeStepName = grStep.GradeStepName,
                                           SalaryItemId = gradeStp != null ? gradeStp.SalaryItemId : 0,
                                           SalaryItemName = gradeStp != null ? gradeStp.SalaryItem.Name : ""
                                       }).Where(r => r.SalaryItemId == 0);
                //.Where(r=>r.SalaryItemId == 0 );
                if (salaryItemCheck.Any())
                {
                    aResponseDto.Success = false;
                    aResponseDto.Message = "No Salary Item is Set For The Following Grade Step " + salaryItemCheck.Select(r => r.GradeStepName).FirstOrDefault();
                    return aResponseDto;
                }
            }
            var salaryItemdt =
                salaryProcessDto.SalaryItems.Where(i => i.Pick == true);
            if (!salaryItemdt.Any())
            {
                aResponseDto.Message = "Please Select Salary Items";
                return aResponseDto;
            }
            var test = _service.GetAll<GradeStepSalaryItem>().Where(r => r.Status == GradeStepSalaryStatus.Active).ToList();

            var selectedSalaryItem = Mapper.Map<List<SalaryItem>>(salaryItemdt);
            var data = (from emp in empLoyeeGradeStepSalaryItem
                            //join grStep in _service.GetAll<GradeStep>().ToList() on emp.Designation.Grade.Id equals grStep.GradeId into extra
                            //from gradeStep in extra.DefaultIfEmpty()
                        join grStepsal in _service.GetAll<GradeStepSalaryItem>().Where(r => r.Status == GradeStepSalaryStatus.Active).ToList() on emp.GradeStepId equals grStepsal.GradeStepId into anthextra //gradeStep.Id
                        from gradeStSal in anthextra.DefaultIfEmpty()
                        join stepSalItem in selectedSalaryItem on gradeStSal.SalaryItem.Id equals stepSalItem.Id into othextra
                        from othStSal in othextra.DefaultIfEmpty()
                        select new SalaryProcessDetailsDto
                        {
                            SalaryProcessId = 0,
                            EmployeeId = emp.EmployeeId,
                            SalaryItemId = othStSal != null ? othStSal.Id : 0,
                            ProcessedAmount = (decimal)(gradeStSal != null ? gradeStSal.Amount != null ? gradeStSal.Amount : 0 : 0),
                            EditedAmount = (decimal)(gradeStSal != null ? gradeStSal.Amount != null ? gradeStSal.Amount : 0 : 0),
                            LateNumOfDays = 0,

                        }).Where(r => r.SalaryItemId != 0).ToList();
            List<SalaryProcessDetailsDto> grpResult = (from d in data
                                                       group d by new { d.SalaryItemId, d.EmployeeId }
                                                           into grp
                                                       select new SalaryProcessDetailsDto
                                                       {
                                                           SalaryProcessId = 0,
                                                           EmployeeId = grp.Key.EmployeeId,
                                                           SalaryItemId = grp.Key.SalaryItemId,
                                                           ProcessedAmount = grp.Sum(c => c.ProcessedAmount),
                                                           EditedAmount = grp.Sum(c => c.EditedAmount),
                                                           LateNumOfDays = 0
                                                       }).ToList();
            var onlySingleEmployee = (from em in grpResult
                                      group em by new { em.EmployeeId }
                                          into grp
                                      select new SalaryProcessDetailsDto
                                      {
                                          EmployeeId = grp.Key.EmployeeId,
                                          LateNumOfDays = 0
                                      });
            //var dummy = new List<SalaryProcessDetailsDto>();
            foreach (var salaryProcessDetailsDto in onlySingleEmployee)
            {

                var empAttendence = _service.GetAll<EmployeeAttendance>().Where(r => r.EmpId == salaryProcessDetailsDto.EmployeeId);
                var designation =
                                    _service.GetAll<Employee>()
                                        .Where(e => e.Id == salaryProcessDetailsDto.EmployeeId).Select(r => r.DesignationId).FirstOrDefault();

                var gradeId =
                          _service.GetSingleById<Employee>(salaryProcessDetailsDto.EmployeeId).Designation.GradeId;
                //var test1 =  _service.GetAll<GradeStepSalaryItem>().Where(r => r.Grade.Id == gradeId && r.SalaryItemId == (int) LateAttendanceSalaryItem.Basic && r.Status == GradeStepSalaryStatus.Active);
                var basicSalaryItem = _service.GetAll<SalaryItem>().Where(r => r.Name.Contains("Basic"));
                var basicSalaryItemId = basicSalaryItem != null ? _service.GetAll<SalaryItem>().Where(r => r.Name.Contains("Basic")).FirstOrDefault().Id : (int)LateAttendanceSalaryItem.Basic;
                var firstOrDefault = _service.GetAll<GradeStepSalaryItem>().Where(r => r.Grade.Id == gradeId && r.SalaryItemId == (int)basicSalaryItemId && r.Status == GradeStepSalaryStatus.Active).Select(r => r.Amount).FirstOrDefault();
                if (firstOrDefault != null)
                {
                    decimal basicSalaryAmount =
                        (decimal)firstOrDefault;

                    BasicSalaryConfiguration basicSalConfig = _service.GetAll<BasicSalaryConfiguration>().FirstOrDefault();
                    int officeTimeSpan = basicSalConfig.OfficeOutTime.Hour - basicSalConfig.OfficeInTime.Hour;
                    if (empAttendence.Any())
                    {

                        var salConfig =
                            _service.GetAll<SalaryConfiguration>().FirstOrDefault(r => r.DesignationId == designation);

                        if (salConfig.IsLateAttendanceMonitored)
                        {
                            int dayForLateAtten = salConfig.DayForLateAttendance;
                            int lateDay = 0;
                            int deductForNumOfDays = 0;
                            foreach (var atten in empAttendence)
                            {
                                int getMonth = atten.Date.Month;
                                int year = atten.Date.Year;
                                if (getMonth == (int)salaryProcessDto.Month && year == salaryProcessDto.FiscalYearId)
                                {
                                    if (atten.Late > 0) //atten.Late!= null && 
                                    {
                                        lateDay++;
                                    }
                                }
                            }
                            deductForNumOfDays = lateDay / dayForLateAtten;

                            //int numberOfWorkingDays =
                            //    _service
                            //        .GetAll<Calendar>(
                            //            ).Count(r => r.Year == salaryProcessDto.FiscalYearId &&
                            //                r.Month == (long) salaryProcessDto.Month);
                            decimal deductionAmount = (basicSalaryAmount / basicSalConfig.MonthDays) * deductForNumOfDays;
                            salaryProcessDetailsDto.EditedAmount = deductionAmount;
                            salaryProcessDetailsDto.ProcessedAmount = deductionAmount;
                            salaryProcessDetailsDto.LateNumOfDays = deductForNumOfDays;
                            salaryProcessDetailsDto.SalaryItemId = (int)LateAttendanceSalaryItem.LateSalaryItem;
                            salaryProcessDetailsDto.SalaryProcessId = 0;

                        }
                        else
                        {
                            salaryProcessDetailsDto.EditedAmount = 0;
                            salaryProcessDetailsDto.ProcessedAmount = 0;
                            salaryProcessDetailsDto.LateNumOfDays = 0;
                            salaryProcessDetailsDto.SalaryItemId = (int)LateAttendanceSalaryItem.LateSalaryItem;
                            salaryProcessDetailsDto.SalaryProcessId = 0;
                        }
                        //dummy.Add(salaryProcessDetailsDto);
                        grpResult.Add(salaryProcessDetailsDto);

                        var salBonusConfig =
                            _service.GetAll<SalaryConfiguration>().FirstOrDefault(r => r.DesignationId == designation);
                        if (salBonusConfig.IsIllegibleForOvertime)
                        {
                            //int dayForLateAtten = salConfig.DayForLateAttendance;
                            //int lateDay = 0;
                            //int deductForNumOfDays = 0;
                            decimal overtimeAmount = 0;
                            decimal perOvertime = 0;
                            decimal weekendOtCount = 0;
                            decimal notionalOtCount = 0;
                            foreach (var atten in empAttendence)
                            {
                                int getMonth = atten.Date.Month;
                                int year = atten.Date.Year;
                                if (getMonth == (int)salaryProcessDto.Month && year == salaryProcessDto.FiscalYearId)
                                {
                                    if (atten.OverTime > 0) //atten.Late!= null && 
                                    {
                                        perOvertime = perOvertime + (decimal)atten.OverTime / 60;
                                        if (perOvertime > salBonusConfig.MaximumOvertimeHour)
                                        {
                                            perOvertime = salBonusConfig.MaximumOvertimeHour;
                                        }

                                    }
                                }
                                int holidayType =
                                    (int)_service.GetAll<Calendar>()
                                        .Where(r => r.PresentDate == atten.Date)
                                        .Select(r => r.HolidayType).FirstOrDefault();
                                //if (holidayType<=0)
                                //{
                                //    aResponseDto.Message = "Calendar Isn't Saved for the date " + atten.Date;
                                //    return aResponseDto;
                                //}
                                if (holidayType == (int)CalendarWeekendDay.Weekend)
                                {
                                    weekendOtCount++;

                                }
                                else if (holidayType != 0)
                                {
                                    notionalOtCount++;
                                }


                            }
                            int weekendOtCountHour = (int)weekendOtCount * officeTimeSpan;
                            int notionalOtCountHour = (int)notionalOtCount * officeTimeSpan;

                            //if (perOvertime > salBonusConfig.MaximumOvertimeHour)
                            //{
                            //    perOvertime = salBonusConfig.MaximumOvertimeHour;
                            //}
                            int diffPerWeek = (int)(perOvertime + weekendOtCountHour + notionalOtCountHour);
                            if (diffPerWeek > salBonusConfig.MaximumOvertimeHour)
                            {
                                int diff = salBonusConfig.MaximumOvertimeHour - diffPerWeek;
                                if (perOvertime > diff)
                                {
                                    //int flag =(int)perOvertime - weekendOtCountHour;
                                    perOvertime = perOvertime - diff;
                                    diff = 0;
                                }
                                else
                                {
                                    diff = (int)(diff - perOvertime);
                                    perOvertime = 0;
                                }
                                if (weekendOtCountHour > diff && diff > 0)
                                {

                                    weekendOtCountHour = weekendOtCountHour - diff;
                                    diff = 0;
                                }
                                else
                                {
                                    diff = (int)(diff - weekendOtCountHour);
                                    weekendOtCountHour = 0;
                                    notionalOtCountHour = notionalOtCountHour - diff;
                                    //int flag = weekendOtCountHour - (int)perOvertime;
                                }
                            }

                            overtimeAmount = overtimeAmount +
                                             (perOvertime * ((basicSalaryAmount / basicSalConfig.MonthDays) / officeTimeSpan));
                            overtimeAmount = overtimeAmount +
                                             (weekendOtCountHour *
                                              ((basicSalaryAmount * basicSalConfig.WeekendOvertimeByBasicPer) / 100));
                            overtimeAmount = overtimeAmount +
                                             (notionalOtCount *
                                              ((notionalOtCountHour * basicSalConfig.NationalHolidayOtByBasicPer) / 100));
                            //overtimeAmount = basicSalaryAmount * perOvertime;

                            SalaryProcessDetailsDto aProcessDetails = new SalaryProcessDetailsDto();
                            aProcessDetails.EmployeeId = salaryProcessDetailsDto.EmployeeId;
                            aProcessDetails.EditedAmount = overtimeAmount;
                            aProcessDetails.ProcessedAmount = overtimeAmount;
                            aProcessDetails.SalaryProcessId = 0;
                            aProcessDetails.SalaryItemId = (int)LateAttendanceSalaryItem.Overtime;
                            grpResult.Add(aProcessDetails);
                        }
                    }
                    //else
                    //{

                    //    aResponseDto.Message = "Employee Attendance Wasn't Saved in the storage";
                    //    return aResponseDto;
                    //}
                }
                #region purchase voucher entry
                //var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.SalaryExpense, null);
                ////var product = GenService.GetById<Product>(item.ProductId);
                //if (string.IsNullOrEmpty(accHeadCode))
                //{
                //    aResponseDto.Message = "Salary Payable account head not found for the product. Please contact system administrator.";
                //    return aResponseDto;
                //}
                //var voucher = new VoucherDto();
                //voucher.AccountHeadCode = accHeadCode;
                //voucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                //voucher.Description = "In time of purchase of product against direct purchase - " + entity.ChallanNo;
                //voucher.Credit = 0;
                //voucher.Debit = item.NetAmount;
                //voucher.CompanyProfileId = ;
                //voucher.VoucherDate = stockTran.TranDate;
                //voucherList.Add(voucher);
                #endregion
            }
            //long employeeid = leavesupervisionfacade.GetEmployeeId(SessionHelper.UserProfile.UserId);
            salaryProcessDetailsList = grpResult.Select(x => new SalaryProcessDetail
            {
                SalaryProcessId = x.SalaryProcessId, //salProcessId,
                EmployeeId = x.EmployeeId,
                SalaryItemId = x.SalaryItemId,
                ProcessedAmount = x.ProcessedAmount,
                EditedAmount = x.EditedAmount,
                CreateDate = DateTime.Now,
                Status = EntityStatus.Active,
                EditorId = salaryProcessDto.EditorId
                //LateNumOfDays =
            }).ToList();
            salaryProcess.ProcessDetails = salaryProcessDetailsList;
            _service.Save(salaryProcess);
            aResponseDto.Success = true;
            aResponseDto.Message = "Salary Process Successfully Saved";

            //var salProcessId = salaryProcess.Id;
            // _service.Save(salaryProcessDetailsList);
            //var resul = from 
            //return data;

            //grpResult.Add(dummy);
            return aResponseDto;
        }

        public List<SalaryItemDto> GetSalaryItems()
        {
            var slaryItem = _service.GetAll<SalaryItem>();
            return Mapper.Map<List<SalaryItemDto>>(slaryItem);
        }

        public List<SalaryProcessDto> GetSalaryProcesses(int? salaryItemId, int? year, int? month)
        {
            var data = _service.GetAll<SalaryProcessDetail>().ToList();
            var result = data.Select(x => new SalaryProcessDto
            {
                FiscalYearId = x.SalaryProcess.FiscalYearId,
                FiscalYearName = x.SalaryProcess.FiscalYear.Year.ToString(),
                Month = x.SalaryProcess.Month,
                MonthName = x.SalaryProcess.Month.ToString(),
                //SalaryItemId = x.ProcessDetails.Select(y=>y.SalaryItemId).ToList()
                SalaryItemId = x.SalaryItemId,
                SalaryItemName = x.SalaryItem.Name,
                Amount = x.EditedAmount
            });
            var final = (from em in result
                         group em by new { em.FiscalYearId, em.FiscalYearName, em.Month, em.SalaryItemId, em.SalaryItemName, em.MonthName }
                             into grp
                         select new SalaryProcessDto
                         {
                             //EmployeeId = grp.Key.EmployeeId,
                             FiscalYearId = grp.Key.FiscalYearId,
                             FiscalYearName = grp.Key.FiscalYearName,
                             Month = grp.Key.Month,
                             MonthName = grp.Key.MonthName,
                             //SalaryItemId = x.ProcessDetails.Select(y=>y.SalaryItemId).ToList()
                             SalaryItemId = grp.Key.SalaryItemId,
                             SalaryItemName = grp.Key.SalaryItemName,
                             Amount = grp.Sum(c => c.Amount)
                         });
            if (salaryItemId != null)
            {
                final = final.Where(x => x.SalaryItemId == salaryItemId);
            }
            if (year != null)
            {
                final = final.Where(x => x.FiscalYearName == year.ToString());
            }
            if (month != null)
            {
                final = final.Where(x => x.Month == (Month)month);
            }
            return final.ToList();
        }

        public ResponseDto SalaryPosting(long fiscalYearId, long month, long officeId,long userId)
        {
            ResponseDto response = new ResponseDto();
            decimal netSalaryAmount = 0;
            try
            {
               var fiscalYear =
               _service.GetAll<FiscalYears>().Where(r => r.Year == fiscalYearId).Select(r => r.Id).FirstOrDefault();
                var salaryProcesses = GenService.GetAll<SalaryProcess>().Where(r => r.FiscalYearId == fiscalYear && r.OfficeId == officeId && r.Month == (Month)month && r.Status == (SalaryProcessStatus)EntityStatus.Active);
                if (salaryProcesses.Any())
                {
                    var details = new List<SalaryProcessDetail>();
                    var salaryProcess = salaryProcesses.FirstOrDefault();
                    if (salaryProcess != null && salaryProcess.ProcessDetails.Any())
                    {
                        var salaryItems = GenService.GetAll<SalaryItem>().Where(r => r.ContributionType == SalaryItemContributionType.Add);
                        if (salaryItems.Any())
                        {
                            var salaryItemIds = salaryItems.Select(r => r.Id);
                            details = salaryProcess.ProcessDetails.Where(r => salaryItemIds.Contains(r.SalaryItemId)).ToList();
                            netSalaryAmount = details.Sum(r => r.EditedAmount);
                        }


                    }
                }
                var voucherList = new List<VoucherDto>();
                #region purchase voucher entry
                var accHeadCode = (officeId==1? _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.SalaryExpense, null): "5010300034");
                //var productAccount = GenService.GetById<Product>(aDetail.ProductId);
                if (string.IsNullOrEmpty(accHeadCode))
                {
                    response.Message = "Salary Expense account head not found. Please contact system administrator.";
                    return response;
                }
                var voucher = new VoucherDto();
                voucher.AccountHeadCode = accHeadCode;
                voucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                voucher.Description = "Salary Expence";
                voucher.Credit = 0;
                voucher.Debit = netSalaryAmount;
                voucher.CompanyProfileId = 1;
                voucher.VoucherDate = _companyProfileFacade.DateToday(1).Date;
                voucherList.Add(voucher);
                #endregion
                //response.Success = false;
                //response.Message = "Purchase Order Received Failed";
                #region contra voucher entry 
                if (voucherList.Count > 0)
                {
                    var CreditVoucher = new VoucherDto();

                    CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                    CreditVoucher.Description = "Salary From Islami Bank for Employees";
                    CreditVoucher.Credit = voucherList.Sum(c => c.Debit);
                    CreditVoucher.Debit = 0;
                    CreditVoucher.CompanyProfileId = 1;
                    CreditVoucher.VoucherDate = _companyProfileFacade.DateToday(1).Date;

                    var CreditAccHead = (officeId == 1 ? _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.CashAtBank, null) : "1020202001"); ;

                    if (string.IsNullOrEmpty(CreditAccHead))
                    {
                        response.Message = "Supplier payable head not found. Please contact system administrator.";
                        return response;
                    }
                    CreditVoucher.AccountHeadCode = CreditAccHead;
                    voucherList.Add(CreditVoucher);

                }



                #endregion

                if (voucherList.Count > 0)
                {

                    var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                    var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                    voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                    var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, officeId, userId);
                    //response.Id = entity.Id;
                    if (accountsResponse.Success != null && !(bool)accountsResponse.Success)
                    {
                        response.Message = "Salary Posting Saving Failed. " + accountsResponse.Message;
                        //tran.Dispose();
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Salary Posting Failed";
                return response;
            }
            response.Success = true;
            response.Message = "Salary Posting Successful";
            return response;

        }
    }
}
