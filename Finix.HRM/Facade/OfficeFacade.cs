﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;
using Finix.Auth.Facade;


namespace Finix.HRM.Facade
{
    public class OfficeFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private readonly CompanyProfileFacade _office = new CompanyProfileFacade();
        public List<OfficeLayerDto> GetOfficeLayers()
        {
            var officelayers = GenService.GetAll<OfficeLayer>();
            var data = officelayers.Select(x => new OfficeLayerDto
            {
                Id = x.Id,
                Name = x.Name,
                Level = x.Level

            }).ToList();
            return data;
        }
        public void SaveOfficeLayer(OfficeLayerDto officelayerdto)
        {
            if (officelayerdto.Id > 0)
            {
                var officelayer = GenService.GetById<OfficeLayer>(officelayerdto.Id);
                officelayer.Name = officelayerdto.Name;
                officelayer.Level = officelayerdto.Level;
                GenService.Save(officelayer);

            }
            else
            {
                GenService.Save(new OfficeLayer
                {
                    Name = officelayerdto.Name,
                    Level = officelayerdto.Level
                });
            }
            GenService.SaveChanges();
        }


        public void DeleteOfficeLayer(long id)
        {
            GenService.Delete<OfficeLayer>(id);
            GenService.SaveChanges();
        }


        public List<OfficeDto> GetOffice(long? id = null)
        {
            var offices = GenService.GetAll<Office>();
            if (id.HasValue)
                offices = offices.Where(x => x.Id == id);

            var data = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.OfficeLayerId,
                OfficeLayerName = x.OfficeLayer.Name,
                ParentId = x.ParentId,
                ParentName = x.ParentOffice.Name
            }).ToList();
            return ApplyJqFilter(data).ToList();
        }
        public List<OfficeDto> GetOffices(long? id = null)
        {
            var offices = GenService.GetAll<Office>();
            if (id.HasValue)
                offices = offices.Where(x => x.Id == id);

            var data = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return data;
        }
        public long SaveOffice(OfficeDto officedto,long userId,long officeId)
        {
            var office = new Office();
            if (officedto.Id > 0)
            {
                office = GenService.GetById<Office>(officedto.Id);
                office.Name = officedto.Name;
                office.ParentId = officedto.ParentId > 0 ? officedto.ParentId : null;
                office.OfficeLayerId = officedto.OfficeLayerId;
                _office.SaveOffice(officedto.Id, office.Name, office.ParentId, userId,officeId);
                //_location.(officedto.Id, office.Name, office.ParentId, userId, officeId);
                
            }
            else
            {
                //GenService.Save(new Office
                //{
                //    Name = officedto.Name,
                //    OfficeLayerId = officedto.OfficeLayerId,
                //    ParentId = officedto.ParentId == 0 ? null : officedto.ParentId
                //});
                office = new Office();
                office.Name = officedto.Name;
                office.OfficeLayerId = officedto.OfficeLayerId;
                office.ParentId = officedto.ParentId > 0 ? officedto.ParentId : null;
                GenService.Save(office);
                _office.SaveOffice(office.Id, office.Name, office.ParentId, userId,officeId);
            }
            //_office.SaveOffice(officedto)
           
            GenService.SaveChanges();
            return office.Id;
        }

        public void DeleteOffice(int id)
        {
            GenService.Delete<Office>(id);
            GenService.SaveChanges();
        }

        public List<OfficeDto> GetCorrespondingParentOffices(int officelayerid)
        {
            List<OfficeDto> officelist = new List<OfficeDto>();
            var officeLayer = GenService.GetById<OfficeLayer>(officelayerid);
            int officelevel = 0;
            if (officeLayer != null)
            {
                officelevel = officeLayer.Level;
                if (officelevel == 0)
                {
                    officelist = new List<OfficeDto> {
                        new OfficeDto{Id=0,Name="No parent"}
                    };
                    return officelist;
                }
                var offices = GenService.GetAll<Office>().Where(x => x.OfficeLayer.Id == officelevel);
                officelist = offices.Select(x => new OfficeDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    OfficeLayerId = x.OfficeLayerId,
                    OfficeLayerName = x.OfficeLayer.Name,
                    ParentId = x.ParentId,
                    ParentName = x.ParentOffice.Name
                }).ToList();
            }
            return officelist;
        }

        public List<OfficeUnitDto> GetOfficeUnit(long? id = null)
        {
            var officeunits = GenService.GetAll<OfficeUnit>();
            if (id.HasValue)
                officeunits = GenService.GetAll<OfficeUnit>().Where(x => x.Id == id);
            var data = officeunits.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                UnitType = (int)x.UnitType,
                UnitTypeName = x.UnitType.ToString(),
                ParentId = x.ParentId,
                ParentName = x.ParentUnit.Name
            }).ToList();
            return data;
        }

        public void SaveOfficeUnit(OfficeUnitDto officeunitdto)
        {
            if (officeunitdto.Id > 0)
            {
                var officeunit = GenService.GetById<OfficeUnit>(officeunitdto.Id);
                officeunit.Name = officeunitdto.Name;
                officeunit.UnitType = (UnitType)officeunitdto.UnitType;
                officeunit.ParentId = officeunitdto.ParentId == 0 ? null : officeunitdto.ParentId;
            }
            else
            {
                GenService.Save(new OfficeUnit
                {
                    Name = officeunitdto.Name,
                    ParentId = officeunitdto.ParentId == 0 ? null : officeunitdto.ParentId,
                    UnitType = (UnitType)officeunitdto.UnitType
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteOfficeUnit(int id)
        {
            GenService.Delete<OfficeUnit>(id);
            GenService.SaveChanges();
        }

        public List<OfficeUnitDto> GetOfficeUnitsByUnitType(int unittype)
        {
            var officeunits = GenService.GetAll<OfficeUnit>().Where(x => (int)x.UnitType == unittype);
            var data = officeunits.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                ParentId = x.ParentId

            }).ToList();
            return data;
        }
        //public List<OfficeUnitSettingDto> GetOfficeUnitSettingsByOfficeId(int officeid)
        //{
        //    var officeunitsettings = GenService.GetAll<OfficeUnitSetting>().Where(x=>x.OfficeId==officeid).OrderBy(x => x.Sl);
        //    var data = officeunitsettings.Select(x => new OfficeUnitSettingDto
        //    {
        //        Id = x.Id,
        //        OfficeId = x.OfficeId,
        //        OfficeName = x.Office.Name,
        //        UnitType=(int)x.OfficeUnit.UnitType,
        //        OfficeUnitId = x.OfficeUnitId,
        //        OfficeUnitName = x.OfficeUnit.Name,
        //        ParentUnitId = x.ParentUnitId,
        //        ParentUnitName = x.ParentUnit.Name,
        //        Sl = x.Sl
        //    }).ToList();

        //    return data;
        //}
        public List<OfficeUnitSettingDto> GetOfficeUnitSettingsByOfficeId(int officeid)
        {
            var officeunitsettings = GenService.GetAll<OfficeUnitSetting>().Where(x => x.OfficeId == officeid).OrderBy(x => x.Sl);
            var data = officeunitsettings.Select(x => new OfficeUnitSettingDto
            {
                Id = x.Id,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                UnitType = (int)x.OfficeUnit.UnitType,
                OfficeUnitId = x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name,
                ParentUnitId = x.ParentUnitId,
                ParentUnitName = x.ParentUnit.Name,
                Sl = x.Sl
            }).ToList();

            return data;
        }
        public void SaveOfficeUnitSetting(OfficeUnitSettingDto officeunitsettingdto)
        {
            if (officeunitsettingdto.Id > 0)
            {
                var officeunitsetting = GenService.GetById<OfficeUnitSetting>(officeunitsettingdto.Id);
                officeunitsetting.OfficeId = officeunitsettingdto.OfficeId;
                officeunitsetting.OfficeUnitId = officeunitsettingdto.OfficeUnitId;
                officeunitsetting.ParentUnitId = officeunitsettingdto.ParentUnitId == 0 ? null : officeunitsettingdto.ParentUnitId;
                officeunitsetting.Sl = officeunitsettingdto.Sl;
            }
            else
            {
                GenService.Save(new OfficeUnitSetting
                {
                    OfficeId = officeunitsettingdto.OfficeId,
                    OfficeUnitId = officeunitsettingdto.OfficeUnitId,
                    ParentUnitId = officeunitsettingdto.ParentUnitId == 0 ? null : officeunitsettingdto.ParentUnitId,
                    Sl = officeunitsettingdto.Sl
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteOfficeUnitSetting(int id)
        {
            GenService.Delete<OfficeUnitSetting>(id);
            GenService.SaveChanges();
        }
        public List<OfficeUnitDto> GetCorrespondingParentUnit(int unittype)
        {
            List<OfficeUnitDto> officeunitlist;
            var officeunits = GenService.GetAll<OfficeUnit>().Where(x => (int)(x.UnitType) == unittype - 1);
            officeunitlist = officeunits.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                ParentId = x.ParentId,
                ParentName = x.ParentUnit.Name,
                UnitType = (int)x.UnitType,
                UnitTypeName = x.UnitType.ToString()
            }).ToList();

            return officeunitlist;
        }

        public List<OfficeDto> GetOfficesByLayer(int officelevel)
        {
            var offices = GenService.GetAll<Office>().Where(x => x.OfficeLayer.Level == officelevel);
            var officelist = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.OfficeLayerId,
                OfficeLayerName = x.OfficeLayer.Name,
                ParentId = x.ParentId,
                ParentName = x.ParentOffice.Name
            }).ToList();

            return officelist;
        }

        public List<OfficePositionDto> GetOfficePositions()
        {
            var officepositions = GenService.GetAll<OfficePosition>();
            var data = officepositions.Select(x => new OfficePositionDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.Office.OfficeLayerId,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                DefaultDesignationId = x.DefaultDesignationId,
                DefaultDesignationName = x.DefaultDesignation.Name,
                PositionWeight = x.PositionWeight
            }).ToList();
            return data;
        }
        //GetOfficePositionByOfficeSettings
        public List<OfficePositionDto> GetOfficePositionByOfficeSettings(long? officeLayerId, long? officeId, long? designationId)
        {
            var officepositions = GenService.GetAll<OfficePosition>();
            var data = officepositions.Select(x => new OfficePositionDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.Office.OfficeLayerId,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                DefaultDesignationId = x.DefaultDesignationId,
                DefaultDesignationName = x.DefaultDesignation.Name,
                PositionWeight = x.PositionWeight
            }).ToList();

            if (officeLayerId != 0 && officeLayerId != null)
            {
                data = data.Where(x => x.OfficeLayerId == officeLayerId).ToList();
            }
            if (officeId != 0 && officeId != null)
            {
                data = data.Where(x => x.OfficeId == officeId).ToList();
            }
            if (designationId != 0 && designationId != null)
            {
                data = data.Where(x => x.DefaultDesignationId == designationId).ToList();
            }

            return data;
        }

        public void SaveOfficePosition(OfficePositionDto officepositiondto)
        {
            if (officepositiondto.Id > 0)
            {
                OfficePosition officeposition = GenService.GetById<OfficePosition>(officepositiondto.Id);
                officeposition.Name = officepositiondto.Name;
                officeposition.OfficeId = officepositiondto.OfficeId;
                officeposition.DefaultDesignationId = officepositiondto.DefaultDesignationId;
                officeposition.PositionWeight = officepositiondto.PositionWeight;
            }
            else
            {
                GenService.Save(new OfficePosition
                {
                    Name = officepositiondto.Name,
                    OfficeId = officepositiondto.OfficeId,
                    DefaultDesignationId = officepositiondto.DefaultDesignationId,
                    PositionWeight = officepositiondto.PositionWeight
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteOfficePosition(int id)
        {
            GenService.Delete<OfficePosition>(id);
            GenService.SaveChanges();
        }

        public List<OfficeDto> GetOfficesByLayerId(int officelayerid)
        {
            var offices = GenService.GetAll<Office>().Where(x => x.OfficeLayerId == officelayerid);
            var data = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.OfficeLayerId,
                ParentId = x.ParentId
            }).ToList();

            return data;
        }

        public List<OrganogramDto> GetOrganograms()
        {
            var organograms = GenService.GetAll<OrganoGram>();
            var data = organograms.Select(x => new OrganogramDto
            {
                Id = x.Id,
                //Name = x.Name,
                EmployeeId = x.EmployeeId,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                DesignationId = x.DesignationId,
                DesignationName = x.Designation.Name,
                OfficeLayerId = x.Office.OfficeLayerId,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                UnitType = (int)x.OfficeUnit.UnitType,
                OfficeUnitId = x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name,
                PositionId = x.PositionId,
                PositionName = x.OfficePosition.Name,
                ParentId = x.ParentId,
                ParentName = x.ParentId == null ? null : x.Parent.Employee.Person.FirstName + " " + x.Parent.Employee.Person.LastName
            }).ToList();
            return data;
        }

        public List<OrganogramTestDto> GetOrganogramsTest()
        {
            var organograms = GenService.GetAll<OrganoGram>();
            //List<OrganogramTestDto> OrganogramTestList;
            List<OrganogramTestDto> OrganogramTestList = new List<OrganogramTestDto>();
            //int c = 0;
            foreach (var item in organograms)
            {
                OrganogramTestDto testDto = new OrganogramTestDto();
                testDto.Id = item.Id;
                //testDto.Name = item.Name;
                testDto.DesignationId = item.DesignationId;
                testDto.DesignationName = item.Designation.Name;
                testDto.OfficeLayerId = item.Office.OfficeLayerId;
                testDto.OfficeId = item.OfficeId;
                testDto.OfficeName = item.Office.Name;
                testDto.UnitType = (int)item.OfficeUnit.UnitType;
                testDto.OfficeUnitId = item.OfficeUnitId;
                testDto.OfficeUnitName = item.OfficeUnit.Name;
                testDto.PositionId = item.PositionId;
                testDto.PositionName = item.OfficePosition.Name;
                testDto.ParentId = item.ParentId;
                testDto.ParentName = item.ParentId == null ? null : item.Parent.Employee.Person.FirstName;
                //int count = GenService.GetAll<OrganoGram>().Where(x => x.ParentId == testDto.ParentId).Count();
                //testDto.level = item.ParentId==null?0:(int)item.ParentId;
                int count = 0;
                var pp = item.Parent;
                while (pp != null)
                {
                    count++;
                    pp = pp.Parent;
                }
                testDto.level = count;
                testDto.parent_id = item.ParentId;
                testDto.isLeaf = false;
                var p = GenService.GetAll<OrganoGram>().Where(x => x.ParentId == item.Id).FirstOrDefault();
                testDto.expanded = p == null ? false : true;
                testDto.loaded = true;
                OrganogramTestList.Add(testDto);
            }

            var list = OrganogramTestList;
            return OrganogramTestList;
        }
        public ResponseDto SaveOrganogram(OrganogramDto organogramdto, long userId)
        {
            ResponseDto response = new ResponseDto();
            if (organogramdto.Id > 0)
            {
                OrganoGram organogram = GenService.GetById<OrganoGram>(organogramdto.Id);
                organogram.Name = organogramdto.EmployeeName;
                organogram.EmployeeId = organogramdto.EmployeeId;
                organogram.DesignationId = organogramdto.DesignationId;
                organogram.OfficeId = organogramdto.OfficeId;
                organogram.OfficeUnitId = organogramdto.OfficeUnitId;
                organogram.PositionId = organogramdto.PositionId;
                organogram.ParentId = organogramdto.ParentId == 0 ? null : organogramdto.ParentId;
                organogram.EditedBy = userId;
                organogram.EditDate = DateTime.Now;
            }
            else
            {
                var organogram = GenService.GetAll<OrganoGram>().Where(o => o.EmployeeId == organogramdto.EmployeeId).FirstOrDefault();
                if (organogram == null)
                {
                    GenService.Save(new OrganoGram
                    {
                        Name = organogramdto.EmployeeName,
                        EmployeeId = organogramdto.EmployeeId,
                        DesignationId = organogramdto.DesignationId,
                        OfficeId = organogramdto.OfficeId,
                        OfficeUnitId = organogramdto.OfficeUnitId,
                        PositionId = organogramdto.PositionId,
                        ParentId = organogramdto.ParentId == 0 ? null : organogramdto.ParentId,
                        CreateDate = DateTime.Now,
                        CreatedBy = userId
                    });
                    response.Message = "Organogram Save Successfully";
                }
                else
                {
                    response.Message = "Organogram Already Exist";
                }

            }
            GenService.SaveChanges();
            return response;
        }
        //public void SaveOrganogram(OrganogramDto organogramdto)
        //{
        //    if (organogramdto.Id>0)
        //    {
        //        OrganoGram organogram = GenService.GetById<OrganoGram>(organogramdto.Id);
        //        //organogram.Name = organogramdto.Name;
        //        organogram.EmployeeId = organogramdto.EmployeeId;
        //        organogram.DesignationId = organogramdto.DesignationId;
        //        organogram.OfficeId = organogramdto.OfficeId;
        //        organogram.OfficeUnitId = organogramdto.OfficeUnitId;
        //        organogram.PositionId = organogramdto.PositionId;
        //        organogram.ParentId = organogramdto.ParentId==0?null:organogramdto.ParentId;
        //    }
        //    else
        //    {
        //        GenService.Save(new OrganoGram
        //        {
        //            //Name=organogramdto.Name,
        //            EmployeeId=organogramdto.EmployeeId,
        //            DesignationId=organogramdto.DesignationId,
        //            OfficeId=organogramdto.OfficeId,
        //            OfficeUnitId=organogramdto.OfficeUnitId,
        //            PositionId=organogramdto.PositionId,
        //            ParentId=organogramdto.ParentId==0?null:organogramdto.ParentId
        //        });
        //    }
        //    GenService.SaveChanges();
        //}

        public void DeleteOrganogram(int id)
        {
            GenService.Delete<OrganoGram>(id);
            GenService.SaveChanges();
        }

        public List<OfficeUnitDto> GetCorrespondingOfficeUnit(int unittype)
        {
            var offficeunitlist = GenService.GetAll<OfficeUnit>().Where(x => x.UnitType == (UnitType)unittype);
            var data = offficeunitlist.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                UnitType = (int)x.UnitType,
                ParentId = x.ParentId
            }).ToList();
            return data;
        }

        public List<OrganogramDto> GetParentOrganogramsByPosition(int positionid, int officeid)
        {
            long posweight = GenService.GetAll<OfficePosition>().FirstOrDefault(p => p.Id == positionid).PositionWeight;
            var organogramlist = GenService.GetAll<OrganoGram>().Where(x => x.OfficeId == officeid && x.OfficePosition.PositionWeight <= posweight);
            var data = organogramlist.Select(x => new OrganogramDto
            {
                Id = x.Id,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                ParentId = x.ParentId

            }).ToList();
            if (GenService.GetAll<OfficePosition>().FirstOrDefault(x => x.Id == positionid).PositionWeight == 1)
            {
                OrganogramDto dto = new OrganogramDto { Id = 0, EmployeeName = "No Parent" };
                data.Add(dto);
            }
            return data;
        }

        //public Object GetEmployeesByOffice(int officeid)
        //{
        //    var employees = GenService.GetAll<Employee>().Where(x => x.OfficeId == officeid).ToList();

        //    var officepositions = GenService.GetAll<OfficePosition>().Where(x => x.OfficeId == officeid).ToList();
        //    return new { Employees = employees, OfficePositions = officepositions };

        //}
        //public EmployeeOfficePositionDto GetEmployeesByOffice(int officeid)
        //{
        //    EmployeeOfficePositionDto aEmployeeOfficePositionDto = new EmployeeOfficePositionDto();
        //    var employees = GenService.GetAll<Employee>().Where(x => x.OfficeId == officeid).ToList();
        //    //aEmployeeOfficePositionDto.Employees.ForEach(r => r.Name = r.BasicInfo.Name);
        //    aEmployeeOfficePositionDto.Employees = Mapper.Map<List<EmployeeDto>>(employees);
        //    var officepositions = GenService.GetAll<OfficePosition>().Where(x => x.OfficeId == officeid).ToList();
        //    aEmployeeOfficePositionDto.OfficePositions = Mapper.Map<List<OfficePositionDto>>(officepositions);
        //    return aEmployeeOfficePositionDto;

        //}

        public EmployeeOfficePositionDto GetEmployeesByOffice(int officeid)
        {
            EmployeeOfficePositionDto aEmployeeOfficePositionDto = new EmployeeOfficePositionDto();
            var employees = GenService.GetAll<Employee>().Where(x => x.OfficeId == officeid).ToList();
            //aEmployeeOfficePositionDto.Employees.ForEach(r => r.Name = r.BasicInfo.Name);
            aEmployeeOfficePositionDto.Employees = Mapper.Map<List<EmployeeDto>>(employees);
            //var officepositions = GenService.GetAll<OfficePosition>().Where(x => x.OfficeId == officeid).ToList();
            //aEmployeeOfficePositionDto.OfficePositions = Mapper.Map<List<OfficePositionDto>>(officepositions);
            return aEmployeeOfficePositionDto;

        }
        //Add by Bristy
        public List<EmployeeDto> GetEmployeeListByOffice(int officeid, long? officeLayerId, long? designationId)
        {
            //EmployeeOfficePositionDto aEmployeeOfficePositionDto = new EmployeeOfficePositionDto();
            var employees = GenService.GetAll<Employee>().Where(x => x.OfficeId == officeid).ToList();
            //var employeeDto = Mapper.Map<List<EmployeeDto>>(employees);
            //var emplist = employeeDto.Select(x => new EmployeeDto
            //{
            //    Id = x.Id,
            //    Name = x.Name
            //}).ToList();
            if (officeLayerId != 0 && officeLayerId != null)
            {
                employees = employees.Where(x => x.Office != null && x.Office.OfficeLayerId == officeLayerId).ToList();
            }
            if (designationId != 0 && designationId != null)
            {
                employees = employees.Where(x => x.DesignationId == designationId).ToList();
            }
            var employeeDto = Mapper.Map<List<EmployeeDto>>(employees);
            var emplist = employeeDto.Select(x => new EmployeeDto
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return emplist;

        }
        public List<OfficeUnitDto> GetOfficeLayerById(int officelayerid)
        {
            List<OfficeUnitDto> officelist;

            if (officelayerid == 0)
            {
                officelist = new List<OfficeUnitDto>
                {
                   new OfficeUnitDto{Id=0,Name="No parent"}

                };
                return officelist;
            }
            var offices = GenService.GetAll<OfficeUnit>().Where(x => x.UnitType == (UnitType)officelayerid);
            officelist = offices.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                UnitType = (int)x.UnitType,

                ParentId = x.ParentId
            }).ToList();

            return officelist;

        }

        public List<OfficeDto> GetAllOffices(int officeLayerId)
        {

            List<OfficeDto> officelist;

            if (officeLayerId == 0)
            {
                officelist = new List<OfficeDto>
                {
                   new OfficeDto{Id=0,Name="No parent"}

                };
                return officelist;
            }
            var offices = GenService.GetAll<Office>().Where(x => x.OfficeLayerId == officeLayerId);
            officelist = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.OfficeLayerId,
                ParentId = x.ParentId
            }).ToList();

            return officelist;
        }

        public List<OfficeLayerDto> GetAllOfficeLayersJsonResult()
        {
            var offficeunitlist = GenService.GetAll<OfficeLayer>();
            var data = offficeunitlist.Select(x => new OfficeLayerDto
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return data;
        }

        public List<OfficeUnitDto> GetOfficetUnits(int unittype)
        {
            List<OfficeUnitDto> officeunitlist;
            var officeunits = GenService.GetAll<OfficeUnit>().Where(x => (int)(x.UnitType) == unittype);
            officeunitlist = officeunits.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                ParentId = x.ParentId,
                ParentName = x.ParentUnit.Name,
                UnitType = (int)x.UnitType,
                UnitTypeName = x.UnitType.ToString()
            }).ToList();

            return officeunitlist;
        }

        public List<OfficeDto> GetAllActiveOffices()
        {
            List<OfficeDto> officelist;
            var offices = GenService.GetAll<Office>();
            officelist = offices.Select(x => new OfficeDto
            {
                Id = x.Id,
                Name = x.Name,
                OfficeLayerId = x.OfficeLayerId,
                ParentId = x.ParentId
            }).ToList();

            return officelist;
        }

        public List<OfficeUnitDto> GetOfficeUnitByOffice(int officeid)
        {
            var offunitsettings = GenService.GetAll<OfficeUnitSetting>().Where(x => x.OfficeId == officeid);
            var unitIdList = offunitsettings.Select(x => x.OfficeUnitId);
            var offunits = GenService.GetAll<OfficeUnit>().Where(x => unitIdList.Contains(x.Id));
            var data = offunits.Select(x => new OfficeUnitDto
            {
                Id = x.Id,
                Name = x.Name,
                Checked = false

            }).ToList();
            return data;
        }
        public List<OrganogramDto> GetOrganogramByParentId(int parentId)
        {

            var organogramlist = GenService.GetAll<OrganoGram>().Where(x => x.ParentId == parentId);
            if (organogramlist.Any())
            {
                var data = organogramlist.Select(x => new OrganogramDto
                {
                    Id = x.Id,
                    EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                    ParentId = x.ParentId

                }).ToList();
                return data;
            }

            //if (GenService.GetAll<OrganogramDto>().FirstOrDefault(x => x.P == positionid).PositionWeight == 1)
            //{
            //    OrganogramDto dto = new OrganogramDto { Id = 0, EmployeeName = "No Parent" };
            //    data.Add(dto);
            //}
            return null;
        }
    }
}
