﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class GradeStepFacade:BaseFacade
    {
        public List<GradeStepDto> GetAllGradeSteps()
        {
            var GradeSteps = GenService.GetAll<GradeStep>();
            var data = GradeSteps.Select(x => new GradeStepDto
            {
                Id = x.Id,
                GradeId = x.GradeId,
                GradeName = x.Grade.Name,
                StepName = x.StepName,
                StepNo = x.StepNo,
                Description = x.Description
            }).ToList();
            return data;
        }

        public void SaveGradeStep(GradeStepDto gradestepdto)
        {
            var gradeStep = GenService.GetById<GradeStep>(gradestepdto.Id);
            if (gradestepdto.Id>0)
            {
                gradeStep.GradeId = gradestepdto.GradeId;
                gradeStep.StepName = gradestepdto.StepName;
                gradeStep.StepNo = gradestepdto.StepNo;
                gradeStep.Description = gradestepdto.Description;
            }
            else
            {
                GenService.Save(new GradeStep
                {
                    GradeId = gradestepdto.GradeId,
                    StepName = gradestepdto.StepName,
                    StepNo = gradestepdto.StepNo,
                    Description = gradestepdto.Description
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteGradeStep(int id)
        {
            GenService.Delete<GradeStep>(id);
            GenService.SaveChanges();
        }

        public List<GradeStepDto> GetGradeStepByGrade(int gradeid)
        {
            var gradesteps = GenService.GetAll<GradeStep>().Where(x => x.GradeId == gradeid);
            var data = gradesteps.Select(x => new GradeStepDto
            {
                Id=x.Id,
                StepName=x.StepName,
                GradeId=x.GradeId,
                Description=x.Description,
                StepNo=x.StepNo
            }).ToList();
            return data;
        }

        public List<GradeStepSalaryItemDto> GetGradeStepSalaryItems()
        {
            var gradestepsalaryitems = GenService.GetAll<GradeStepSalaryItem>().Where(r => r.Status == GradeStepSalaryStatus.Active).ToList();
            var data = gradestepsalaryitems.Select(x => new GradeStepSalaryItemDto
            {
                Id=x.Id,
                GradeId=x.GradeId,
                GradeName=x.Grade.Name,
                GradeStepId = x.GradeStepId,
                GradeStepName=x.GradeStep.StepName,
                SalaryItemId=x.SalaryItemId,
                SalaryItemName=x.SalaryItem.Name,
                SalaryItemUnit=x.SalaryItemUnit,
                Amount=x.Amount,
                Status=(int)x.Status
            }).ToList();
            return data;
        }
        public void SaveGradeStepSalary(GradeStepSalaryItemDto gradestepsalaryitemdto)
        {
           
           // var basicitem=GenService.GetAll<GradeStepSalaryItem>().Where(x=>x.SalaryItem.IsEditable==false && x.GradeId==gradestepsalaryitemdto.GradeId && x.GradeStepId==gradestepsalaryitemdto.GradeStepId).FirstOrDefault().SalaryItemUnit;
            if (gradestepsalaryitemdto.Id>0)
            {
                var gradestepsalaryitem = GenService.GetById<GradeStepSalaryItem>(gradestepsalaryitemdto.Id);
                gradestepsalaryitem.GradeId = gradestepsalaryitemdto.GradeId;
                gradestepsalaryitem.GradeStepId = gradestepsalaryitemdto.GradeStepId;
                gradestepsalaryitem.SalaryItemId = gradestepsalaryitemdto.SalaryItemId;
                gradestepsalaryitem.SalaryItemUnit = gradestepsalaryitemdto.SalaryItemUnit;
                if (gradestepsalaryitem.SalaryItem.IsPercentOfBasic==true)
                {
                    var basicitem=GenService.GetAll<GradeStepSalaryItem>().Where(x=>x.SalaryItem.IsEditable==false && x.GradeId==gradestepsalaryitemdto.GradeId && x.GradeStepId==gradestepsalaryitemdto.GradeStepId).FirstOrDefault().SalaryItemUnit;
                    gradestepsalaryitem.Amount =gradestepsalaryitemdto.SalaryItemUnit * (basicitem / 100);
                }
                else
                {
                    gradestepsalaryitem.Amount = gradestepsalaryitemdto.SalaryItemUnit;
                }
                
                gradestepsalaryitem.Status = GradeStepSalaryStatus.Active;
            }
            else
            {
                GradeStepSalaryItem item = new GradeStepSalaryItem();
                item.Id = gradestepsalaryitemdto.Id;
                item.GradeId = gradestepsalaryitemdto.GradeId;
                item.GradeStepId = gradestepsalaryitemdto.GradeStepId;
                item.SalaryItemId = gradestepsalaryitemdto.SalaryItemId;
                item.SalaryItemUnit = gradestepsalaryitemdto.SalaryItemUnit;
                if (GenService.GetById<SalaryItem>(gradestepsalaryitemdto.SalaryItemId).IsPercentOfBasic == true)
                {
                    var basicitem = GenService.GetAll<GradeStepSalaryItem>().Where(x => x.SalaryItem.IsEditable == false && x.GradeId == gradestepsalaryitemdto.GradeId && x.GradeStepId == gradestepsalaryitemdto.GradeStepId).FirstOrDefault().SalaryItemUnit;
                    item.Amount = gradestepsalaryitemdto.SalaryItemUnit * (basicitem / 100);
                }
                else
                {
                    item.Amount = gradestepsalaryitemdto.SalaryItemUnit;
                }

                item.Status = GradeStepSalaryStatus.Active;
                GenService.Save(item);
                
            }

            GenService.SaveChanges();
        }

       
        public List<GradeStepSalaryItemDto> GetGradeStepSalaryDto(int gradeid,int? gradestepid)
        {
            var SalaryItems = GenService.GetAll<SalaryItem>().Where(x => x.SalaryItemStatus == SalaryItemStatus.Active && x.Id != (int)LateAttendanceSalaryItem.LateSalaryItem && x.Id != (int)LateAttendanceSalaryItem.Overtime);
            var GradeSalaryItems = new List<GradeStepSalaryItem>();
            if (gradestepid != null)
            {
                 GradeSalaryItems = GenService.GetAll<GradeStepSalaryItem>().Where(x => x.Status == GradeStepSalaryStatus.Active && x.GradeId == gradeid && x.GradeStepId == gradestepid).ToList();
            }
           
           if (GradeSalaryItems.Count()==0)
           {
               var data = SalaryItems.Select(x => new GradeStepSalaryItemDto
               {
                   Id=x.Id,
                   SalaryItemId=x.Id,
                   SalaryItemName=x.Name,
                   IsParcentOfBasic=x.IsPercentOfBasic,
                   SalaryItemUnit=0,
                   Amount=0
                   
               }).ToList();
               return data;
           }
           else
           {
               List<GradeStepSalaryItemDto> dtos = new List<GradeStepSalaryItemDto>();
               foreach (var sal in SalaryItems)
               {
                   var grsal = GradeSalaryItems.Where(i => i.SalaryItemId == sal.Id).FirstOrDefault();
                   if (grsal!=null)
                   {
                       var dto = new GradeStepSalaryItemDto();
                       dto.Id = grsal.Id;
                       dto.SalaryItemId = grsal.SalaryItemId;
                       dto.SalaryItemName = grsal.SalaryItem.Name;
                       dto.IsParcentOfBasic = grsal.SalaryItem.IsPercentOfBasic;
                       dto.SalaryItemUnit = grsal.SalaryItemUnit;
                       dto.Amount = grsal.Amount;
                       dtos.Add(dto);
                   }
                   else
                   {
                       var dto = new GradeStepSalaryItemDto();
                       dto.SalaryItemId = sal.Id;
                       dto.SalaryItemName = sal.Name;
                       dto.IsParcentOfBasic = sal.IsPercentOfBasic;
                       dto.SalaryItemUnit = 0;
                       dto.Amount = 0;
                       dtos.Add(dto);
                   }
               }
              
               return dtos;
           } 
        }

        public void SaveGradeStepSalarySetUp(int gradeid,int gradestepid,List<GradeStepSalaryItemDto> list)
        {
            var gradesteplist = GenService.GetAll<GradeStepSalaryItem>().Where(x=>x.GradeId==gradeid && x.GradeStepId==gradestepid);
            if (gradesteplist.Count()==0)
            {
                foreach (var item in list)
                {
                    GradeStepSalaryItem element=new GradeStepSalaryItem();
                        element.GradeId=gradeid;
                        element.GradeStepId=gradestepid;
                        //SalaryItemId=GenService.GetAll<SalaryItem>().Where(x=>x.Name==item.SalaryItemName).FirstOrDefault().Id,
                        element.SalaryItemId=item.SalaryItemId;
                        element.SalaryItemUnit=item.SalaryItemUnit;
                        if (GenService.GetById<SalaryItem>(item.SalaryItemId).IsPercentOfBasic == true)
                        {
                            var basicunit = list.First(i => i.SalaryItemName == "Basic").SalaryItemUnit;
                            element.Amount = basicunit * (item.SalaryItemUnit / 100);
                        }
                        else
                        {
                            element.Amount = item.SalaryItemUnit;
                        }
                        //element.Amount = item.SalaryItemUnit;
                        element.Status = GradeStepSalaryStatus.Active;
                        GenService.Save(element);
                }
            }
            else
            {
                foreach (var item in list)
                {
                    var grsalary = GenService.GetAll<GradeStepSalaryItem>().Where(x => x.GradeId==gradeid && x.GradeStepId==gradestepid && x.SalaryItemId == item.SalaryItemId && x.Status==GradeStepSalaryStatus.Active).FirstOrDefault();
                    if (grsalary!=null)
                    {
                        if (grsalary.SalaryItemUnit!=item.SalaryItemUnit)
                        {
                            grsalary.Status = GradeStepSalaryStatus.Inactive;
                            if (grsalary.SalaryItem.Name=="Basic")
                            {
                                var grsal = GenService.GetAll<GradeStepSalaryItem>().Where(x => x.GradeId == gradeid && x.GradeStepId == gradestepid);
                                foreach (var i in grsal)
                                {
                                    i.Status = GradeStepSalaryStatus.Inactive;
                                }
                            }

                        }
                        else
                        {
                            continue;
                        }
                    }
                    GradeStepSalaryItem gradesal = new GradeStepSalaryItem();
                    gradesal.GradeId = gradeid;
                    gradesal.GradeStepId = gradestepid;
                    gradesal.SalaryItemId = item.SalaryItemId;
                    gradesal.SalaryItemUnit = item.SalaryItemUnit;
                    if (GenService.GetById<SalaryItem>(item.SalaryItemId).IsPercentOfBasic == true)
                    {
                        var basicunit = list.First(i => i.SalaryItemName == "Basic").SalaryItemUnit;
                        gradesal.Amount = basicunit * (item.SalaryItemUnit / 100);
                    }
                    else
                    {
                        gradesal.Amount = item.SalaryItemUnit;
                    }
                    //gradesal.Amount = item.SalaryItemUnit;
                    gradesal.Status = GradeStepSalaryStatus.Active;
                    GenService.Save(gradesal);
                }
            }
            GenService.SaveChanges();
        }
        public void DeleteGradeStepSalaryItem(int id)
        {
            GenService.Delete<GradeStepSalaryItem>(id);
            GenService.SaveChanges();
        }
    }
}
