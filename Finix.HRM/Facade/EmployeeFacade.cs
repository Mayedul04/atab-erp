﻿using Finix.HRM.Infrastructure;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.Auth.DTO;
using Finix.Auth.Facade;
using Finix.HRM.Service;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure.Models;
using ResponseDto = Finix.HRM.DTO.ResponseDto;

namespace Finix.HRM.Facade
{
    public class EmployeeFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private UserFacade _user = new UserFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();

        public List<EmployeeDto> GetOnlyEmployeeList()
        {
            var employees = GenService.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);

            List<EmployeeDto> data = new List<EmployeeDto>();
            foreach (var emp in employees.ToList())
            {
                var dto = new EmployeeDto();
                dto.Id = emp.Id;
                dto.Name = emp.Person.FirstName + " " + emp.Person.LastName;
                dto.JoiningInformation = GetJoiningInfo((int)emp.Id);
                data.Add(dto);
            }
            return data;
        }
        public List<EmpBasicInfoDto> GetEmployeeListForEntryPage(List<OfficeProfileDto> officeProfiles)
        {
            //try{ var employee = GenService.GetAll<Employee>();} catch (Exception ex){}
            var employees = GenService.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);
            if (officeProfiles.Any())
            {
                var officeIds = officeProfiles.Select(r => r.Id);
                employees = employees.Where(r => officeIds.Contains((long)r.OfficeId));
            }
            var data = employees.Select(x => new EmpBasicInfoDto
            {
                Id = x.Id,
                FirstName = x.Person.FirstName,
                LastName = x.Person.LastName,
                DateOfBirth = x.Person.DateOfBirth,
                BloodGroup = (int)x.Person.BloodGroup,
                Gender = (int)x.Person.Gender,
                MaritalStatus = (int)x.Person.MaritalStatus,
                Religion = (int)x.Person.Religion,
                NID = x.Person.NID,
                FatherFirstName = x.Person.FatherId != null ?x.Person.Father.FirstName != null ? x.Person.Father.FirstName : "" : "" ,
                FatherLastName = x.Person.FatherId != null ? x.Person.Father.LastName != null ? x.Person.Father.LastName : "" : "",
                MotherFirstName = x.Person.MotherId != null ? x.Person.Mother.FirstName != null ? x.Person.Mother.FirstName : "" : "",
                MotherLastName = x.Person.MotherId != null ? x.Person.Mother.FirstName != null ? x.Person.Mother.FirstName : "" : "",
                CountryId = x.Person.CountryId,
                EmployeeType = (int)x.EmployeeType,
                EmployeeTypeName = x.EmployeeType.ToString()
            }).ToList();
            return data;
        }

        public List<EmployeeDto> GetEmployees()
        {
            var employees = GenService.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);

            List<EmployeeDto> data = new List<EmployeeDto>();
            foreach (var emp in employees.ToList())
            {
                var dto = new EmployeeDto();
                dto.Id = emp.Id;
                dto.Name = emp.Person.FirstName + " " + emp.Person.LastName;
                dto.BasicInfo = new EmpBasicInfoDto
                {
                    Id = emp.Person.Id,
                    Name = emp.Person.FirstName + " " + emp.Person.LastName,
                    FirstName = emp.Person.FirstName,
                    LastName = emp.Person.LastName,
                    DateOfBirth = emp.Person.DateOfBirth,
                    BloodGroup = (int)emp.Person.BloodGroup,
                    Gender = (int)emp.Person.Gender,
                    MaritalStatus = (int)emp.Person.MaritalStatus,
                    Religion = (int)emp.Person.Religion,
                    NID = emp.Person.NID,
                    FatherFirstName = emp.Person.FatherId == null ? "" : emp.Person.Father.FirstName,
                    FatherLastName = emp.Person.FatherId == null ? "" : emp.Person.Father.LastName,
                    MotherFirstName = emp.Person.MotherId == null ? "" : emp.Person.Mother.FirstName,
                    MotherLastName = emp.Person.MotherId == null ? "" : emp.Person.Mother.LastName,
                    CountryId = emp.Person.CountryId

                };
                dto.ContactInformation = new ContactInformationDto
                {
                    
                };
                dto.Education = new DegreeInfoDto
                {

                };
                dto.JoiningInformation = GetJoiningInfo((int)emp.Id);
                //dto.JoiningInformation = new JoiningInformationDto
                //{

                //};
                
                dto.TrainingInformation = new TrainingInformationDto
                {

                };
                dto.UserInformation = new UserInformationDto
                {

                };
                data.Add(dto);
            }
            return data;

        }

        public List<EmpBasicInfoDto> GetEmployeeList()
        {
            //try{ var employee = GenService.GetAll<Employee>();} catch (Exception ex){}
            var employees = GenService.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);
            var data = employees.Select(x => new EmpBasicInfoDto
            {
                Id = x.Id,
                FirstName = x.Person.FirstName != null ?x.Person.FirstName:"",
                LastName = x.Person.LastName != null ? x.Person.LastName:"",
                DateOfBirth = x.Person.DateOfBirth,
                BloodGroup = (int)x.Person.BloodGroup,
                Gender = (int)x.Person.Gender,
                MaritalStatus = (int)x.Person.MaritalStatus,
                Religion = (int)x.Person.Religion,
                NID = x.Person.NID,
                FatherFirstName = x.Person.FatherId == null ? "" : x.Person.Father.FirstName,
                FatherLastName = x.Person.FatherId == null ? "" : x.Person.Father.LastName,
                MotherFirstName = x.Person.MotherId == null ? "" : x.Person.Mother.FirstName,
                MotherLastName = x.Person.MotherId == null ? "" : x.Person.Mother.LastName,
                CountryId = x.Person.CountryId,

                EmployeeType = (int)x.EmployeeType,
                EmployeeTypeName = x.EmployeeType.ToString()
            }).ToList();
            return data;
        }
        public void SaveEmployee(EmpBasicInfoDto basicinfodto, int? id = null)
        {
            Employee emp = GenService.GetAll<Employee>().SingleOrDefault(x => x.Id == id);
            if (emp != null)
            {
                emp.Person.FirstName = basicinfodto.FirstName;
                emp.Person.LastName = basicinfodto.LastName;
                emp.Person.BloodGroup = (BloodGroup)basicinfodto.BloodGroup;
                emp.Person.CountryId = basicinfodto.CountryId;
                emp.OfficeId = basicinfodto.OfficeId;
                emp.Person.DateOfBirth = basicinfodto.DateOfBirth;
                if (emp.Person.FatherId == null)
                {
                    emp.Person.Father = new Person
                    {
                        FirstName = basicinfodto.FatherFirstName,
                        LastName = basicinfodto.FatherLastName,
                        Gender = Gender.Male,
                        MaritalStatus = MaritalStatus.Married,
                        Religion = (Religion)basicinfodto.Religion,
                        CountryId = basicinfodto.CountryId

                    };
                }
                else
                {
                    emp.Person.Father.FirstName = basicinfodto.FatherFirstName;
                    emp.Person.Father.LastName = basicinfodto.FatherLastName;
                }

                if (emp.Person.MotherId == null)
                {
                    emp.Person.Mother = new Person
                    {
                        FirstName = basicinfodto.MotherFirstName,
                        LastName = basicinfodto.MotherLastName,
                        Gender = Gender.Female,
                        MaritalStatus = MaritalStatus.Married,
                        Religion = (Religion)basicinfodto.Religion,
                        CountryId = basicinfodto.CountryId
                    };
                }
                else
                {
                    emp.Person.Mother.FirstName = basicinfodto.MotherFirstName;
                    emp.Person.Mother.LastName = basicinfodto.MotherLastName;
                }

                emp.Person.NID = basicinfodto.NID;
                emp.Person.Religion = (Religion)basicinfodto.Religion;
                emp.Person.MaritalStatus = (MaritalStatus)basicinfodto.MaritalStatus;
                emp.Person.Gender = (Gender)basicinfodto.Gender;

            }
            else
            {
                Person p;
                p = new Person();
                //GenService.Save(p= new Person
                //{
                p.CountryId = basicinfodto.CountryId;
                p.DateOfBirth = basicinfodto.DateOfBirth;
                p.Father = new Person
                {
                    FirstName = basicinfodto.FatherFirstName,
                    LastName = basicinfodto.FatherLastName,
                    Gender = Gender.Male,
                    MaritalStatus = MaritalStatus.Married,
                    Religion = (Religion)basicinfodto.Religion,
                    CountryId = basicinfodto.CountryId

                };
                
                p.Mother = new Person
                {
                    FirstName = basicinfodto.MotherFirstName,
                    LastName = basicinfodto.MotherLastName,
                    Gender = Gender.Female,
                    MaritalStatus = MaritalStatus.Married,
                    Religion = (Religion)basicinfodto.Religion,
                    CountryId = basicinfodto.CountryId
                };
                p.BloodGroup = (BloodGroup)basicinfodto.BloodGroup;
                p.Gender = (Gender)basicinfodto.Gender;
                p.MaritalStatus = (MaritalStatus)basicinfodto.MaritalStatus;
                p.NID = basicinfodto.NID;
                p.Religion = (Religion)basicinfodto.Religion;
                p.FirstName = basicinfodto.FirstName;
                p.LastName = basicinfodto.LastName;

                //});

                GenService.Save(p);
                //GenService.Save(new Employee
                //{
                Employee e = new Employee();
                e.EmpCode = _companyProfileFacade.GetEmployeeCode();
                e.OfficeId = basicinfodto.OfficeId;
                e.PersonId = p.Id;
                e.EmployeeType = basicinfodto.EmployeeType != null ?(EmployeeType)basicinfodto.EmployeeType : EmployeeType.Contractual;
                e.EmployeeStatus = EmployeeStatus.Active;
                GenService.Save(e);
                //JoiningDate=basicinfodto.JoiningDate
                //});

            }

            GenService.SaveChanges();
        }

        public void DeleteEmployee(int id)
        {
            GenService.Delete<Employee>(id);
            GenService.SaveChanges();
        }

        public List<TrainingInformationDto> GetTrainingInfo(int id)
        {
            var traininginfos = GenService.GetAll<EmployeeTraining>().Where(x => x.EmployeeId == id);
            var data = traininginfos.Select(x => new TrainingInformationDto
            {
                Id = x.Id,
                TrainingVendorId = x.TrainingVendorId,
                TrainingVendorName = x.TrainingVendor.VendorName,
                TrainingId = x.TrainingId,
                TrainingName = x.Training.TrainingName,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                TrainingHours = x.TrainingHours,
                EmployeeId = x.EmployeeId
            }).ToList();
            return data;
        }

        public void SaveEmployeeTraining(TrainingInformationDto traininginfodto)//id must be passsed during save,otherwise if list is empty,empid will be null
        {
            if (traininginfodto.Id > 0)
            {
                var emptraining = GenService.GetById<EmployeeTraining>(traininginfodto.Id);
                emptraining.TrainingVendorId = traininginfodto.TrainingVendorId;
                emptraining.TrainingId = traininginfodto.TrainingId;
                emptraining.StartDate = traininginfodto.StartDate;
                emptraining.EndDate = traininginfodto.EndDate;
                emptraining.TrainingHours = traininginfodto.TrainingHours;
                emptraining.EmployeeId = traininginfodto.EmployeeId;
            }
            else
            {
                GenService.Save(new EmployeeTraining
                {
                    TrainingVendorId = traininginfodto.TrainingVendorId,
                    TrainingId = traininginfodto.TrainingId,
                    StartDate = traininginfodto.StartDate,
                    EndDate = traininginfodto.EndDate,
                    TrainingHours = traininginfodto.TrainingHours,
                    EmployeeId = traininginfodto.EmployeeId
                });
            }
            GenService.SaveChanges();
        }

        public List<EmploymentHistoryDto> GetEmploymentHistory(int id)
        {
            var traininginfos = GenService.GetAll<EmploymentHistory>().Where(x => x.EmployeeId == id);
            var data = traininginfos.Select(x => new EmploymentHistoryDto
            {
                Id = x.Id,
                CompanyName = x.CompanyName,
                CompanyBusiness = x.CompanyBusiness,
                CompanyAddress = x.CompanyAddress,
                Designation = x.Designation,
                Department = x.Department,
                AreaofExperiences = x.AreaofExperiences,
                Responsibilities = x.Responsibilities,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                EmployeeId = x.EmployeeId
            }).ToList();
            return data;
        }

        public void SaveEmploymentHistory(EmploymentHistoryDto empHistorydto)//id must be passsed during save,otherwise if list is empty,empid will be null
        {
            if (empHistorydto.Id > 0)
            {
                var emptraining = GenService.GetById<EmploymentHistory>(empHistorydto.Id);
                emptraining.CompanyName = empHistorydto.CompanyName;
                emptraining.CompanyBusiness = empHistorydto.CompanyBusiness;
                emptraining.CompanyAddress = empHistorydto.CompanyAddress;
                emptraining.Designation = empHistorydto.Designation;
                emptraining.Department = empHistorydto.Department;
                emptraining.AreaofExperiences = empHistorydto.AreaofExperiences;
                emptraining.Responsibilities = empHistorydto.Responsibilities;
                emptraining.StartDate = empHistorydto.StartDate;
                emptraining.EndDate = empHistorydto.EndDate;
                emptraining.EmployeeId = empHistorydto.EmployeeId;
            }
            else
            {
                GenService.Save(new EmploymentHistory
                {
                    CompanyName = empHistorydto.CompanyName,
                    CompanyBusiness = empHistorydto.CompanyBusiness,
                    CompanyAddress = empHistorydto.CompanyAddress,
                    Designation = empHistorydto.Designation,
                    Department = empHistorydto.Department,
                    AreaofExperiences = empHistorydto.AreaofExperiences,
                    Responsibilities = empHistorydto.Responsibilities,
                    StartDate = empHistorydto.StartDate,
                    EndDate = empHistorydto.EndDate,
                    EmployeeId = empHistorydto.EmployeeId
                });
            }
            GenService.SaveChanges();
        }
        public void DeleteEmploymentHistory(int id)
        {
            GenService.Delete<EmploymentHistory>(id);
            GenService.SaveChanges();
        }
        public void DeleteEmployeeTraining(int id)
        {
            GenService.Delete<EmployeeTraining>(id);
            GenService.SaveChanges();
        }

        public JoiningInformationDto GetJoiningInfo(int id)
        {
            Employee emp = GenService.GetById<Employee>(id);
            JoiningInformationDto joindto = new JoiningInformationDto();

            joindto.JoiningDate = emp.JoiningDate;
            joindto.CompanyProfileId = emp.CompanyProfileId;
            joindto.OfficeId = emp.OfficeId;
            joindto.OfficeUnitId = emp.OfficeUnitId;
            if (emp.DesignationId.HasValue)
            {
                long gradeid = GenService.GetAll<Designation>().FirstOrDefault(x => x.Id == emp.DesignationId).GradeId;
                joindto.GradeId = gradeid;
                joindto.DesignationId = emp.DesignationId;
            }

            joindto.EmployeeType = (int)emp.EmployeeType;
            if (emp.Workshift.HasValue)
            {
                joindto.WorkShift = (int)emp.Workshift;
            }

            if (emp.GradeStepId.HasValue)
            {
                joindto.GradeStepId = emp.GradeStepId;
            }
            return joindto;
        }
        public void SaveJoiningInfo(JoiningInformationDto joininginfo)
        {
            Employee emp = GenService.GetById<Employee>(joininginfo.EmployeeId);
            //emp.JoiningDate = joininginfo.JoiningDate;
            emp.OfficeId = joininginfo.OfficeId;
            emp.OfficeUnitId = joininginfo.OfficeUnitId;
            emp.CompanyProfileId = joininginfo.CompanyProfileId;
            emp.DesignationId = joininginfo.DesignationId;
            emp.EmployeeType = (EmployeeType)joininginfo.EmployeeType;
            emp.GradeStepId = joininginfo.GradeStepId;
            emp.Workshift = (Workshift)joininginfo.WorkShift;

            var jobhistory = GenService.GetAll<EmployeeJobHistory>().Where(x => x.EmployeeId == emp.Id && x.JobStatus == EmployeeHistoryEnum.join).FirstOrDefault();
            if (jobhistory != null)
            {
                jobhistory.OfficeId = joininginfo.OfficeId;
                jobhistory.OfficeUnitId = joininginfo.OfficeUnitId;
                jobhistory.DesignationId = joininginfo.DesignationId;
                jobhistory.GradeStepId = joininginfo.GradeStepId;
            }
            else
            {
                GenService.Save(new EmployeeJobHistory
                {
                    EmployeeId = emp.Id,
                    DesignationId = joininginfo.DesignationId,
                    EffectiveDate = DateTime.Today,
                    OfficeId = joininginfo.OfficeId,
                    OfficeUnitId = joininginfo.OfficeUnitId,
                    JobStatus = EmployeeHistoryEnum.join,
                    GradeStepId = joininginfo.GradeStepId,
                    Remarks = "Joined"
                });
            }
            GenService.SaveChanges();
        }
        public EmpBasicInfoDto GetBasicInfoById(long empid)
        {
            var emp = GenService.GetById<Employee>(empid);
            EmpBasicInfoDto basicdto = new EmpBasicInfoDto();
            basicdto.FirstName = emp.Person.FirstName;
            basicdto.LastName = emp.Person.LastName;
            basicdto.Name = basicdto.FirstName + " " + basicdto.LastName;
            if (emp.Person.Father != null)
            {
                basicdto.FatherFirstName = emp.Person.Father.FirstName;
                basicdto.FatherLastName = emp.Person.Father.LastName;
            }
            if (emp.Person.Mother != null)
            {
                basicdto.MotherFirstName = emp.Person.Mother.FirstName;
                basicdto.MotherLastName = emp.Person.Mother.LastName;
            }
            if (emp != null)
            {
                basicdto.DateOfBirth = emp.Person.DateOfBirth;
                basicdto.Gender = (int)emp.Person.Gender;
                basicdto.MaritalStatus = (int)emp.Person.MaritalStatus;
                basicdto.Religion = (int)emp.Person.Religion;
                basicdto.BloodGroup = (int)emp.Person.BloodGroup;
                basicdto.CountryId = emp.Person.CountryId;
                basicdto.OfficeId = emp.OfficeId;
                basicdto.NID = emp.Person.NID;
                basicdto.EmpCode = emp.EmpCode != null ? (long)emp.EmpCode : (long)0;
            }
            return basicdto;
        }

        public void SaveContactInfo(ContactInformationDto contactinfodto, int id)
        {
            Employee emp = GenService.GetAll<Employee>().SingleOrDefault(x => x.Id == id);
            if (emp.Person.PresentAddressId == null && emp.Person.PermanentAddressId == null)
            {
                emp.Person.PermanentAddress = new Address
                {
                    AddressLine1 = contactinfodto.ParmanentAddress.AddressLine1,
                    AddressLine2 = contactinfodto.ParmanentAddress.AddressLine2,
                    PostOfficeId = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Code == contactinfodto.ParmanentAddress.PostalCode).Id,

                };
                emp.Person.PresentAddress = new Address
                {
                    AddressLine1 = contactinfodto.PresentAddress.AddressLine1,
                    AddressLine2 = contactinfodto.PresentAddress.AddressLine2,
                    PostOfficeId = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Code == contactinfodto.PresentAddress.PostalCode).Id
                };
            }
            else
            {
                emp.Person.PermanentAddress.PostOfficeId = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Code == contactinfodto.ParmanentAddress.PostalCode).Id;
                emp.Person.PresentAddress.PostOfficeId = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Code == contactinfodto.ParmanentAddress.PostalCode).Id;

            }
            emp.Person.PhoneNo = contactinfodto.PhoneNo;
            emp.Person.Email = contactinfodto.Email;
            emp.Person.EmergencyContactPerson = new Person { FirstName = contactinfodto.EmergencyContactPerson };
            //emp.Person.EmergencyContactPerson = contactinfodto.EmergencyContactPerson;
            emp.Person.EmergencyContactPersonPhone = contactinfodto.EmergencyContactPhone;
            emp.Person.EmergencyContactPersonRelation = contactinfodto.EmergencyContactRelation;
            GenService.SaveChanges();
        }

        public ContactInformationDto GetContactInfoById(int empid)
        {
            var emp = GenService.GetById<Employee>(empid);
            ContactInformationDto contactdto = new ContactInformationDto();
            if (emp.Person.PresentAddressId != null && emp.Person.PermanentAddressId != null)
            {
                contactdto.PresentAddress = new AddressDto
                {
                    AddressLine1 = emp.Person.PresentAddress.AddressLine1,
                    AddressLine2 = emp.Person.PresentAddress.AddressLine2,

                    PostalCode = emp.Person.PresentAddress.PostOffice.Code
                };
                contactdto.ParmanentAddress = new AddressDto
                {
                    AddressLine1 = emp.Person.PermanentAddress.AddressLine1,
                    AddressLine2 = emp.Person.PermanentAddress.AddressLine2,

                    PostalCode = emp.Person.PermanentAddress.PostOffice.Code
                };
                contactdto.PhoneNo = emp.Person.PhoneNo;
                contactdto.Email = emp.Person.Email;
                contactdto.EmergencyContactPerson = emp.Person.EmergencyContactPerson == null ? "" : emp.Person.EmergencyContactPerson.FirstName;
                contactdto.EmergencyContactPhone = emp.Person.EmergencyContactPersonPhone;
                contactdto.EmergencyContactRelation = emp.Person.EmergencyContactPersonRelation;
            }

            return contactdto;
        }

        public void SaveUserInformation(UserInformationDto userinfo, int id)
        {
            var user = GenService.GetAll<User>().SingleOrDefault(x => x.EmployeeId == id);
            if (user != null)
            {
                user.UserName = userinfo.Username;
                user.Password = userinfo.Password;
                user.EmployeeId = userinfo.EmployeeId;
                user.IsActive = true;
            }
            else
            {
                GenService.Save(new User
                {
                    UserName = userinfo.Username,
                    Password = userinfo.Password,
                    EmployeeId = userinfo.EmployeeId,
                    IsActive = true
                });
            }
            GenService.SaveChanges();
        }

        public UserInformationDto GetUserInfoById(int empid)
        {
            var user = _user.GetUserInfoById(empid); //GenService.GetAll<User>().SingleOrDefault(x => x.EmployeeId == empid);
            UserInformationDto userdto = new UserInformationDto();
            userdto.UserId = user.Id;
            userdto.Username = user.UserName;
            userdto.Password = user.Password;
            userdto.EmployeeId = user.EmployeeId != null ? (long)user.EmployeeId : 0;
            return userdto;
        }
        #region Employee Transfer
        public List<EmployeeTransferDto> GetAllEmployeeInfo()
        {

            var employeeTrasfer = _service.GetAll<EmployeeJobHistory>().ToList();
            if (employeeTrasfer.Any())
            {
                var employeeInfo = (from pr in _service.GetAll<Employee>()
                                    where pr.EmployeeStatus.Equals(EmployeeStatus.Active)
                                    join em in employeeTrasfer on pr.Id equals em.EmployeeId
                                    join des in _service.GetAll<Designation>() on em.DesignationId equals des.Id into emdes
                                    join offun in _service.GetAll<OfficeUnit>() on em.OfficeUnitId equals offun.Id into offunit
                                    join ofce in _service.GetAll<Office>() on em.OfficeId equals ofce.Id into ofce
                                    from ex in emdes.DefaultIfEmpty()
                                    from off in offunit.DefaultIfEmpty()
                                    from offic in ofce.DefaultIfEmpty()
                                    select new EmployeeTransferDto()
                                    {
                                        EmployeeId = em.Id,
                                        EmployeeName = pr.Person.FirstName + " " + pr.Person.LastName,
                                        DesignationId = (long)em.DesignationId,
                                        DesignationName = ex.Name,
                                        UnitType = off.UnitType,
                                        OfficeUnitId = em.OfficeUnitId,
                                        OfficeUnitName = off.Name,
                                        OfficeLayerId = offic.OfficeLayerId,
                                        OfficeId = em.OfficeId,
                                        OfficeName = offic.Name,
                                        EffectiveDate = em.EffectiveDate,
                                        ToDate = em.ToDate
                                    }).ToList();
                return employeeInfo;
            }
            return null;
        }
        #endregion
        public List<EmployeeTransferDto> GetEmployeeTransfers()
        {
            var emptransfers = GenService.GetAll<EmployeeJobHistory>();
            var data = emptransfers.Select(x => new EmployeeTransferDto
            {
                Id = x.Id,
                DesignationId = x.DesignationId,
                DesignationName = x.Designation.Name,
                EmployeeId = x.EmployeeId,
                UnitType = x.OfficeUnit.UnitType,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                OfficeUnitId = x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name,
                OfficeLayerId = x.Office.OfficeLayerId,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                EffectiveDate = x.EffectiveDate,
                ToDate = x.ToDate,
                GradeStepName = x.GradeStep.StepName,
                JobStatusName = x.JobStatus.ToString(),
                Remarks = x.Remarks
            }).ToList();
            return data;
        }
        public ResponseDto SaveEmployeeTransfer(EmployeeTransferDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new EmployeeJobHistory();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<EmployeeJobHistory>(model.Id);

                    entity.EmployeeId = model.EmployeeId;
                    entity.DesignationId = model.DesignationId;
                    entity.OfficeUnitId = model.OfficeUnitId;
                    entity.OfficeId = model.OfficeId;
                    entity.EffectiveDate = model.EffectiveDate;
                    entity.GradeStepId = model.GradeStepId;
                    entity.JobStatus = (EmployeeHistoryEnum)model.JobStatus;
                    entity.Remarks = model.Remarks;
                    entity.ToDate = model.ToDate;
                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;

                }
                else
                {
                    var check = _service.GetAll<EmployeeJobHistory>().Where(x => x.EmployeeId == model.EmployeeId && x.ToDate == null).FirstOrDefault();
                    if (check != null)
                    {
                        check.ToDate = DateTime.Now;
                        _service.Save(check);
                    }

                    entity = new EmployeeJobHistory
                    {
                        EmployeeId = model.EmployeeId,
                        DesignationId = model.DesignationId,
                        OfficeUnitId = model.OfficeUnitId,
                        OfficeId = model.OfficeId,
                        EffectiveDate = model.EffectiveDate,
                        ToDate = model.ToDate,
                        GradeStepId = model.GradeStepId,
                        JobStatus = (EmployeeHistoryEnum)model.JobStatus,
                        Remarks = model.Remarks,
                        CreateDate = DateTime.Now,
                        Status = EntityStatus.Active
                    };

                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Employee Transfer Saved Successfully";
            return response;
        }


        public object DeleteEmployeeTransfer(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<EmployeeJobHistory>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "Employee Transfer Deleted Successfully";
            return response;
        }

        public Object GetContactForEmployee()
        {
            Employee emp = GenService.GetById<Employee>(1);
            return new { Email = emp.Person.Email, Phone = emp.Person.PhoneNo };
        }


        public Object GetSubstituteLeave(int empid)
        {
            List<LeaveType> leavetypelist;
            var emp = GenService.GetById<Employee>(empid);
            var employees = GenService.GetAll<Employee>().Where(x => x.Id != empid).ToList();
            var data = employees.Select(x => new EmpBasicInfoDto
            {
                Id = x.Id,
                FirstName = x.Person.FirstName,
                LastName = x.Person.LastName
            }).ToList();
            if (emp.Person.Gender == Gender.Male)
            {
                leavetypelist = GenService.GetAll<LeaveType>().Where(x => x.LeaveApplicableTo == LeaveApplicableTo.Male || x.LeaveApplicableTo == LeaveApplicableTo.All).ToList();
            }
            else
            {
                leavetypelist = GenService.GetAll<LeaveType>().Where(x => x.LeaveApplicableTo == LeaveApplicableTo.Female || x.LeaveApplicableTo == LeaveApplicableTo.All).ToList();
            }
            return new { emplist = data, email = emp.Person.Email, phone = emp.Person.PhoneNo, leavetypelist = leavetypelist };
        }


        public List<EmployeeTransferDto> GetEmployeeDetails(int? id)
        {
            var data = new List<EmployeeTransferDto>();
            if (id != null)
            {
                data = GetAllEmployeeInfo().Where(i => i.EmployeeId == id).ToList();
            }
            else
            {
                data = GetAllEmployeeInfo().ToList();
            }

            return data;

        }

        public List<EmployeeDto> GetAllActiveEmployees()
        {
            var employees = GenService.GetAll<Employee>().Where(x => x.EmployeeStatus == EmployeeStatus.Active);
            var result = employees.Select(x => new EmployeeDto
            {
                Id = x.Id,
                Name = x.Person.FirstName + " " + x.Person.LastName
            });
            return result.ToList();

        }

        public List<EmployeeTransferDto> GetEmployeeDetailsOfficewise(int? id)
        {
            var data = GetAllEmployeeInfo().Where(i => i.OfficeId == id && i.ToDate == null).ToList();
            return data;
        }

        public List<LeaveApplicationDto> GetEmployeesCurrentlyOnLeave()
        {
            var emps = GenService.GetAll<LeaveApplication>().Where(x => DateTime.Today >= x.FromDate && DateTime.Today <= x.ToDate);
            List<LeaveApplicationDto> dtos = new List<LeaveApplicationDto>();
            foreach (var item in emps)
            {
                LeaveApplicationDto dto = new LeaveApplicationDto();
                dto.EmployeeName = item.Employee.Person.FirstName + " " + item.Employee.Person.LastName;
                dto.LeaveTypeName = item.LeaveType.Name;
                dto.FromDate = item.FromDate.Date;
                dto.ToDate = item.ToDate.Date;
                dto.SubstitutorName = item.SubstitutorId == null ? null : item.Substitutor.Person.FirstName + " " + item.Substitutor.Person.LastName;
                dtos.Add(dto);
            }

            return dtos;
        }
        public List<PaySlipDTO> GetPayslipForEmployee(int? year, int? month, int? id)
        {
            var employeeTrasfer = _service.GetAll<Employee>();
            //decimal total = 0;
            if (id != null)
            {
                employeeTrasfer = employeeTrasfer.Where(i => i.Id == id);
            }

            var employeeInfo = (from sal in _service.GetAll<SalaryProcess>().Where(i => i.Month == (Month)month && i.FiscalYear.Year == year)
                                from saldetail in sal.ProcessDetails //.Where(i => i.EmployeeId == id)
                                join em in employeeTrasfer on saldetail.EmployeeId equals em.Id
                                join pr in _service.GetAll<Person>() on em.PersonId equals pr.Id
                                join des in _service.GetAll<Designation>() on em.DesignationId equals des.Id into emdes
                                join gr in _service.GetAll<Grade>() on em.Designation.GradeId equals gr.Id into extra
                                join offun in _service.GetAll<OfficeUnit>() on em.OfficeUnitId equals offun.Id into offunit
                                join ofce in _service.GetAll<Office>() on em.OfficeId equals ofce.Id into ofce
                                from ex in emdes.DefaultIfEmpty()
                                from off in offunit.DefaultIfEmpty()
                                from offic in ofce.DefaultIfEmpty()
                                from ext in extra.DefaultIfEmpty()
                                select new PaySlipDTO()
                                {
                                    EmployeeId = em.Id,
                                    EmployeeName = pr.FirstName + " " + pr.LastName,
                                    DesignationId = (long)em.DesignationId,
                                    DesignationName = ex.Name,
                                    OfficeUnitId = off!= null? off.Id:0,
                                    OfficeUnitName = off != null ? off.Name :"",
                                    OfficeId = offic != null ?  offic.Id:1,
                                    OfficeName = offic.Name,
                                    Gender = (int)pr.Gender,
                                    GenderName = "",//Enum.GetName(typeof(Gender),pr.Gender)
                                    MaritalStatus = (int)pr.MaritalStatus,
                                    MaritalStatusName = "",// Enum.GetName(typeof(MaritalStatus), pr.Gender)
                                    SalaryItemId = saldetail.SalaryItem.Id,
                                    SalaryItemName = saldetail.SalaryItem.Name,
                                    ContributionType = (int)saldetail.SalaryItem.ContributionType,
                                    ContributionTypeName = "",//Enum.GetName(typeof(SalaryItemContributionType),saldetail.SalaryItem.ContributionType)
                                    ProcessedAmount = saldetail.ProcessedAmount,
                                    EditedAmount = saldetail.EditedAmount,
                                    //TotalAmount = (int)saldetail.SalaryItem.ContributionType==0 ? total + saldetail.EditedAmount :total - saldetail.EditedAmount 
                                }).OrderBy(p => p.ContributionType).ThenBy(p => p.SalaryItemName).ToList();
            var previousSum = 0;
            foreach (var data in employeeInfo)
            {
                data.ContributionTypeName = Enum.GetName(typeof(SalaryItemContributionType), data.ContributionType);
                data.GenderName = Enum.GetName(typeof(Gender), data.Gender);
                data.MaritalStatusName = Enum.GetName(typeof(MaritalStatus), data.MaritalStatus);
                data.Month = Enum.GetName(typeof(Month), month);
                data.FiscalYearName = year.ToString();
                if (data.ContributionType == 0)
                    data.TotalAmount = previousSum + data.EditedAmount;
                else
                    data.TotalAmount = previousSum - data.EditedAmount;
            }
            return employeeInfo;
        }
        public List<EmployeeTransferDto> GetEmployeeDepartmentWise(int? officeUnitId, int? empType)
        {
            //throw new NotImplementedException();
            var employees = _service.GetAll<Employee>();
            var data = employees.Select(x => new EmployeeTransferDto
            {
                EmployeeId=x.Id,
                EmployeeName = x.Person.FirstName + " " + x.Person.LastName,
                DesignationName = x.Designation.Name,
                OfficeName = x.Office.Name,
                OfficeUnitId = (long)x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name,
                OfficeId = (long)x.OfficeId,
                EmployeeTypeId = (long)x.EmployeeType,
                //EmployeeTypeName =
            }).ToList(); //.Where(x => x.OfficeUnitId == officeUnitId && x.EmployeeTypeId == empType)
            foreach (var item in data)
            {
                item.EmployeeTypeName = Enum.GetName(typeof(EmployeeType), item.EmployeeTypeId);
            }
            if (officeUnitId != null)
            {
                data = data.Where(r => r.OfficeUnitId == officeUnitId).ToList();
            }
            if (empType != null)
            {
                data = data.Where(r => r.EmployeeTypeId == empType).ToList();
            }
            return data;
        }

        public List<PaySlipDTO> GetSalaryWiseEmployee(int? from, int? to)
        {
            var employeeTrasfer = _service.GetAll<Employee>();
            var employeeInfo = (from sal in _service.GetAll<SalaryProcess>()
                                from saldetail in sal.ProcessDetails //.Where(i => i.EmployeeId == id)
                                join em in employeeTrasfer on saldetail.EmployeeId equals em.Id
                                join pr in _service.GetAll<Person>() on em.PersonId equals pr.Id
                                join des in _service.GetAll<Designation>() on em.DesignationId equals des.Id into emdes
                                join gr in _service.GetAll<Grade>() on em.Designation.GradeId equals gr.Id into extra
                                join offun in _service.GetAll<OfficeUnit>() on em.OfficeUnitId equals offun.Id into offunit
                                join ofce in _service.GetAll<Office>() on em.OfficeId equals ofce.Id into ofce
                                from ex in emdes.DefaultIfEmpty()
                                from off in offunit.DefaultIfEmpty()
                                from offic in ofce.DefaultIfEmpty()
                                from ext in extra.DefaultIfEmpty()
                                select new PaySlipDTO()
                                {
                                    EmployeeId = em.Id,
                                    EmployeeName = pr.FirstName + " " + pr.LastName,
                                    DesignationId = (long)em.DesignationId,
                                    DesignationName = ex.Name,
                                    OfficeUnitId = off.Id,
                                    OfficeUnitName = off.Name,
                                    OfficeId = offic.Id,
                                    OfficeName = offic.Name,
                                    Gender = (int)pr.Gender,
                                    GenderName = "",//Enum.GetName(typeof(Gender),pr.Gender)
                                    MaritalStatus = (int)pr.MaritalStatus,
                                    MaritalStatusName = "",// Enum.GetName(typeof(MaritalStatus), pr.Gender)
                                    SalaryItemId = saldetail.SalaryItem.Id,
                                    SalaryItemName = saldetail.SalaryItem.Name,
                                    ContributionType = (int)saldetail.SalaryItem.ContributionType,
                                    ContributionTypeName = "",//Enum.GetName(typeof(SalaryItemContributionType),saldetail.SalaryItem.ContributionType)
                                    ProcessedAmount = saldetail.ProcessedAmount,
                                    EditedAmount = saldetail.EditedAmount,

                                }).OrderBy(p => p.ContributionType).ThenBy(p => p.SalaryItemName).ToList();
            var previousSum = 0;
            foreach (var data in employeeInfo)
            {
                data.ContributionTypeName = Enum.GetName(typeof(SalaryItemContributionType), data.ContributionType);
                data.GenderName = Enum.GetName(typeof(Gender), data.Gender);
                data.MaritalStatusName = Enum.GetName(typeof(MaritalStatus), data.MaritalStatus);
                if (data.ContributionType == 0)
                    data.TotalAmount = previousSum + data.EditedAmount;
                else
                    data.TotalAmount = previousSum - data.EditedAmount;
            }
            var groupempSalary = (from d in employeeInfo
                                  group d by new { d.EmployeeId, d.EmployeeName, d.OfficeUnitName, d.OfficeId, d.OfficeName, d.DesignationName, d.GenderName }
                                      into grp
                                  select new PaySlipDTO
                                  {

                                      EmployeeName = grp.Key.EmployeeName,
                                      DesignationName = grp.Key.DesignationName,
                                      GenderName = grp.Key.GenderName,
                                      OfficeUnitName = grp.Key.OfficeUnitName,
                                      OfficeId = grp.Key.OfficeId,
                                      OfficeName = grp.Key.OfficeName,
                                      TotalAmount = grp.Sum(c => c.TotalAmount)

                                  }).ToList();
            if (from != null)
            {
                groupempSalary = groupempSalary.Where(x => x.TotalAmount >= from).ToList();
            }
            if (to != null)
            {
                groupempSalary = groupempSalary.Where(x => x.TotalAmount <= to).ToList();
            }

            return groupempSalary;
        }

        public object GetBasicWiseEmployee(int? trainId, int? educationId, int? bloodId, DateTime? fromDate, DateTime? toDate)
        {
            //throw new NotImplementedException();
            var employees = _service.GetAll<Employee>();
            var data = (from emp in employees
                        join ed in _service.GetAll<EmployeeEducation>() on emp.Id equals ed.EmployeeId into extr
                        from edct in extr.DefaultIfEmpty()
                        join deg in _service.GetAll<Degree>() on edct.DegreeId equals deg.Id into dgr
                        join tr in _service.GetAll<EmployeeTraining>() on emp.Id equals tr.EmployeeId into ex
                        from ext in ex.DefaultIfEmpty()
                        join emptr in _service.GetAll<Training>() on ext.TrainingId equals emptr.Id into trn
                        from dgree in dgr.DefaultIfEmpty()
                        from train in trn.DefaultIfEmpty()
                        select new EmpBasicInfoDto()
                        {
                            Id = emp.Id,
                            Name = emp.Person.FirstName + " " + emp.Person.LastName,
                            BloodGroup = (int)emp.Person.BloodGroup,
                            DesignationName = emp.Designation.Name,
                            ContactNo = emp.Person.PhoneNo,
                            Gender = (int)emp.Person.Gender,
                            MaritalStatus = (int)emp.Person.MaritalStatus,
                            Religion = (int)emp.Person.Religion,
                            NID = emp.Person.NID,
                            CountryId = emp.Person.CountryId,
                            EmployeeType = (int)emp.EmployeeType,
                            EmployeeTypeName = emp.EmployeeType.ToString(),
                            TrainingId = ext == null ? 0 : ext.TrainingId,
                            TrainingName = ext == null ? "" : train.TrainingName,
                            DegreeId = dgree == null ? 0 : dgree.Id,
                            DegreeName = dgree == null ? "" : dgree.Name

                        }).ToList();
            foreach (var item in data)
            {
                item.BloodGroupName = Enum.GetName(typeof(BloodGroup), item.BloodGroup);
            }
            if (trainId != null)
            {
                data = data.Where(x => x.TrainingId == trainId).ToList();
            }
            if (educationId != null)
            {
                data = data.Where(x => x.DegreeId == educationId).ToList();
            }
            if (bloodId != null)
            {
                data = data.Where(x => x.BloodGroup == bloodId).ToList();
            }
            return data;
        }

        public List<EmployeeTransferDto> GetJobHisWiseEmployee(int? empHistoryId, DateTime? fromDate, DateTime? toDate)
        {
            //throw new NotImplementedException();
            var emptransfers = GenService.GetAll<EmployeeJobHistory>();
            var data = emptransfers.Select(x => new EmployeeTransferDto
            {
                Id = x.Id,
                DesignationId = x.DesignationId,
                DesignationName = x.Designation.Name,
                EmployeeId = x.EmployeeId,
                UnitType = x.OfficeUnit.UnitType,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                OfficeUnitId = x.OfficeUnitId,
                OfficeUnitName = x.OfficeUnit.Name,
                OfficeLayerId = x.Office.OfficeLayerId,
                OfficeId = x.OfficeId,
                OfficeName = x.Office.Name,
                EffectiveDate = x.EffectiveDate,
                ToDate = x.ToDate,
                GradeStepName = x.GradeStep.StepName,
                JobStatusName = x.JobStatus.ToString(),
                EmployeeHistoryId = (int)x.JobStatus,
                Remarks = x.Remarks
            }).ToList();
            if (empHistoryId != null)
            {
                data = data.Where(x => x.EmployeeHistoryId == empHistoryId).ToList();
            }
            if (fromDate != null)
            {
                data = data.Where(x => x.EffectiveDate >= fromDate).ToList();
            }
            if (toDate != null)
            {
                data = data.Where(x => x.EffectiveDate <= toDate).ToList();
            }

            return data;
        }

        public List<EmployeeTransferDto> GetPromotionWiseEmployee(DateTime? fromDate, DateTime? toDate)
        {
            //throw new NotImplementedException();
            var emptransfers = GenService.GetAll<EmployeeJobHistory>().Where(r => r.JobStatus == EmployeeHistoryEnum.promotion);
            var data = (from emp in GenService.GetAll<Employee>()
                        join tr in emptransfers on emp.Id equals tr.EmployeeId into extra
                        from ex in extra.DefaultIfEmpty()
                        select new EmployeeTransferDto
                        {
                            Id = emp.Id,
                            DesignationId = emp.DesignationId,
                            DesignationName = emp.Designation.Name,
                            EmployeeId = emp.Id,
                            //UnitType = emp.OfficeUnit.UnitType,
                            EmployeeName = emp.Person.FirstName + " " + emp.Person.LastName,
                            OfficeUnitId = emp.OfficeUnitId,
                            OfficeUnitName = emp.OfficeUnit.Name,
                            OfficeLayerId = emp.Office.OfficeLayerId,
                            OfficeId = emp.OfficeId,
                            OfficeName = emp.Office.Name,
                            EffectiveDate = ex != null ? ex.EffectiveDate : DateTime.MinValue,
                            ToDate = ex != null ? ex.ToDate : DateTime.MinValue,
                            GradeStepName = ex != null ? ex.GradeStep.StepName : "",
                            JobStatusName = ex != null ? ex.JobStatus.ToString() : "",
                            EmployeeHistoryId = ex != null ? (int)ex.JobStatus : 0,
                            Remarks = ex.Remarks
                        }).ToList();

            if (fromDate != null)
            {
                data = data.Where(x => x.EffectiveDate < fromDate).ToList();
            }
            //if (toDate != null)
            //{
            //    data = data.Where(x => x.EffectiveDate > toDate).ToList();
            //}

            return data;
        }

        public List<GradeStepDto> GetGradeStepsByGradeId(int? id)
        {
            //throw new NotImplementedException();
            var data = _service.GetAll<GradeStep>().Where(r => r.GradeId == id).ToList();
            return Mapper.Map<List<GradeStepDto>>(data);
        }

        public List<OfficeDto> GetEmployeeChildOffices(long userId)
        {
            List<OfficeDto> officeList = new List<OfficeDto>();
            var employeeId = _user.GetEmployeeIdByUserId(userId);
            if (employeeId > 0)
            {
                var employeeData = _service.GetById<Employee>(employeeId);
                
                if (employeeData != null && employeeData.OfficeId != null && employeeData.OfficeId >0)
                {
                    var parentoffice = _service.GetById<Office>((long)employeeData.OfficeId);
                    officeList.Add(Mapper.Map<OfficeDto>(parentoffice));
                    var offices = _service.GetAll<Office>().Where(r => r.ParentId == employeeData.OfficeId).ToList() ;
                    officeList.AddRange(Mapper.Map<List<OfficeDto>>(offices));
                }
               
            }
            return officeList;
        }
    }


}

