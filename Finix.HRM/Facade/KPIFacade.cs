﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.Service;

namespace Finix.HRM.Facade
{
    public class KPIFacade:BaseFacade
    {
        private readonly GenService _service = new GenService();
        public List<KPITypeDto> GetKPITypes()
        {
            var kpitypes = GenService.GetAll<KPIType>().ToList();
            var data = kpitypes.Select(x => new KPITypeDto
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToList();
            return data;
        }

        public void SaveKPIType(KPITypeDto kpitypedto)
        {
            if (kpitypedto.Id > 0)
            {
                var kpitype = GenService.GetById<KPIType>(kpitypedto.Id);
                kpitype.Name = kpitypedto.Name;
                kpitype.Description = kpitypedto.Description;
                GenService.Save(kpitype);

            }
            else
            {
                GenService.Save(new KPIType
                {
                    Name = kpitypedto.Name,
                    Description = kpitypedto.Description

                });
            }
            GenService.SaveChanges();

        }

        public void DeleteKPIType(long id)
        {
            GenService.Delete<KPIType>(id);
            GenService.SaveChanges();
        }

        public List<KPIDto> GetKPIs(int kpigroupid = 0)
        {
            var kpis = kpigroupid > 0
               ? GenService.GetAll<KPI>()
                   .Where(x => x.KPIGroupId == kpigroupid).ToList()
               : GenService.GetAll<KPI>().ToList();
            var data = kpis.Select(x => new KPIDto
            {
                Id = x.Id,
                Name = x.Name,
                KPITypeId = x.KPITypeId,
                KPITypeName = x.KPIType.Name,
                KPIGroupId=(long)x.KPIGroupId,
                KPIGroupName=x.KPIGroupId==null?null:x.KPIGroup.Name,
            }).ToList();
            return data;
        }

        public void SaveKPI(KPIDto kpidto)
        {
            if (kpidto.Id > 0)
            {
                var kpi = GenService.GetById<KPI>(kpidto.Id);
                kpi.Name = kpidto.Name;
                kpi.KPITypeId = kpidto.KPITypeId;
                kpi.KPIGroupId = kpidto.KPIGroupId;

            }
            else
            {
                GenService.Save(new KPI
                {
                    Name = kpidto.Name,
                    KPITypeId = kpidto.KPITypeId,
                    KPIGroupId=kpidto.KPIGroupId
                });

            }
            GenService.SaveChanges();
        }

        public void DeleteKPI(int id)
        {
            GenService.Delete<KPI>(id);
            GenService.SaveChanges();
        }

        

        

       

        public List<KPIDto> GetCorrespondingKPI(int kpitypeid)
        {
            var kpilist = GenService.GetAll<KPI>().Where(x => x.KPITypeId == kpitypeid);
            var data = kpilist.Select(x => new KPIDto
            {
                Id = x.Id,
                KPITypeId = x.KPITypeId,
                KPITypeName = x.KPIType.Name,
                Name = x.Name
            }).ToList();
            return data;
        }


        public List<KPIGroupDto> GetKPIGroups(int kpigroupid = 0)
        {
            var kpigroups = kpigroupid > 0
               ? GenService.GetAll<KPIGroup>()
                   .Where(x => x.Id == kpigroupid).ToList()
               : GenService.GetAll<KPIGroup>().ToList();
            var data = kpigroups.Select(x => new KPIGroupDto
            {
                Id = x.Id,
                Name = x.Name,
                Description=x.Description
               
            }).ToList();
            return data;
        }

        public void SaveKPIGroup(KPIGroupDto kpigroupdto)
        {
            if (kpigroupdto.Id > 0)
            {
                var kpigroup = GenService.GetById<KPIGroup>(kpigroupdto.Id);
                kpigroup.Name = kpigroupdto.Name;
                kpigroup.Description = kpigroupdto.Description;
                
            }
            else
            {
                GenService.Save(new KPIGroup
                {
                    Name = kpigroupdto.Name,
                    Description = kpigroupdto.Description
                    

                });

            }
            GenService.SaveChanges();
        }

        public void DeleteKPIGroup(int id)
        {
            GenService.Delete<KPIGroup>(id);
            GenService.SaveChanges();
        }

        public List<KPIDto> GetKPIForAppraisal()
        {
            var data = _service.GetAll<KPI>().ToList();
            return Mapper.Map<List<KPIDto>>(data);
        }

        public List<KPIGroupSettingDto> GetAllKpiGroupSettings()
        {
            var data = _service.GetAll<KPIGroupSetting>().ToList();
            return Mapper.Map<List<KPIGroupSettingDto>>(data);
        }

        public ResponseDto SaveKPIGroupSettings(KPIGroupSettingDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new KPIGroupSetting();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<KPIGroupSetting>(model.Id);

                    entity.AppraisalId = model.AppraisalId;
                    entity.KPIGroupId = model.KPIGroupId;
                    entity.KPIId = model.KPIId;
                    entity.DisplayOrder = model.DisplayOrder;
                    entity.Weight = model.Weight;
             
                    entity.EditDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
                else
                {
                    entity = Mapper.Map<KPIGroupSetting>(model);
                    entity.CreateDate = DateTime.Now;
                    entity.Status = EntityStatus.Active;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "KPI Group Settingd Saved Successfully";
            return response;
        }
        public ResponseDto DeleteKPIGroupSettings(int id)
        {
            ResponseDto response = new ResponseDto();
            _service.Delete<KPIGroupSetting>(id);
            GenService.SaveChanges();
            response.Success = true;
            response.Message = "KPI Group Setting Deleted Successfully";
            return response;
        }
    }
}
