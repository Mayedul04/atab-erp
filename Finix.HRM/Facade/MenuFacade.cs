﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;
using System.Globalization;
using Finix.HRM.Service;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.DTO;

namespace Finix.HRM.Facade
{
    public class MenuFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        //public List<ModuleDto> GetModules()
        //{
        //    var modules = GenService.GetAll<Module>();

        //    var data = modules.Select(x => new ModuleDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        DisplayName = x.DisplayName,
        //        Sl = x.Sl,
        //        Description = x.Description
        //    }).ToList();
        //    return data;
        //}

        //public void SaveModule(ModuleDto moduleDto)
        //{
        //    if (moduleDto.Id > 0)
        //    {
        //        var module = GenService.GetById<Module>(moduleDto.Id);
        //        module.Name = moduleDto.Name;
        //        module.DisplayName = moduleDto.DisplayName;
        //        module.Sl = moduleDto.Sl;
        //        module.Description = moduleDto.Description;
        //        GenService.Save(module);

        //    }
        //    else
        //    {
        //        GenService.Save(new Module
        //        {
        //            Name = moduleDto.Name,
        //            DisplayName = moduleDto.DisplayName,
        //            Sl = moduleDto.Sl,
        //            Description = moduleDto.Description
        //        });
        //    }
        //    GenService.SaveChanges();
        //}

        //public void DeleteModule(long id)
        //{
        //    GenService.Delete<Module>(id);
        //    GenService.SaveChanges();
        //}

        
        //public List<SubModuleDto> GetSubModules(int moduleId = 0)  // retreive submodule by moduleid
        //{
        //    //var subModules = GenService.GetAll<SubModule>().Where(x => x.ModuleId == moduleId).ToList();
        //    var subModules = moduleId > 0
        //        ? GenService.GetAll<SubModule>()
        //            .Where(x => x.ModuleId == moduleId).ToList()
        //        : GenService.GetAll<SubModule>().ToList();
        //    var data = subModules.Select(x => new SubModuleDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        DisplayName = x.DisplayName,
        //        Description = x.Description,
        //        Sl = x.Sl,
        //        ModuleId = x.ModuleId,
        //        ModuleName = x.Module.Name,

        //    }).ToList();
        //    return data;

        //}

        public PostOfficeDto GetPostOfficeByPostalCode(long postalcode)
        {
            var postoffice = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Code == postalcode);
            if (postoffice != null)
            {
                PostOfficeDto podto = new PostOfficeDto();
                podto.Id = postoffice.Id;
                podto.Code = postoffice.Code;
                podto.Name = postoffice.Name;
                podto.LocationId = postoffice.LocationId;
                return podto;
            }
            else
            {
                return null;
            }

            // long AreaId = postoffice.LocationId;
            // addresslist.Add(PostOfficeId);
        }
        public long GetAreaId(long locid)
        {
            Location loc = GenService.GetAll<Location>().SingleOrDefault(x => x.Id == locid);
            return loc.Id;
        }

        public long? GetParentId(long locid)
        {
            Location loc = GenService.GetAll<Location>().SingleOrDefault(x => x.Id == locid);
            return loc.ParentId;
        }

        public List<DesignationDto> getCorrespondingDesignations(int gradeid)
        {
            var deslist = GenService.GetAll<Designation>().Where(x => x.GradeId == gradeid);
            var data = deslist.Select(x => new DesignationDto
            {
                Id = x.Id,
                Name = x.Name,
                GradeId = x.GradeId,
                GradeName = x.Grade.Name
            }).ToList();
            return data;
        }

        //public void SaveSubModule(SubModuleDto submoduledto)
        //{
        //    if (submoduledto.Id > 0)
        //    {
        //        var submodule = GenService.GetById<SubModule>(submoduledto.Id);
        //        submodule.Name = submoduledto.Name;
        //        submodule.DisplayName = submoduledto.DisplayName;
        //        submodule.Sl = submoduledto.Sl;
        //        submodule.Description = submoduledto.Description;

        //        submodule.ModuleId = submoduledto.ModuleId;

        //    }
        //    else
        //    {
        //        GenService.Save(new SubModule
        //        {
        //            Name = submoduledto.Name,
        //            DisplayName = submoduledto.DisplayName,
        //            Sl = submoduledto.Sl,
        //            Description = submoduledto.Description,
        //            ModuleId = submoduledto.ModuleId,

        //        });

        //    }
        //    GenService.SaveChanges();
        //}

        //public void DeleteSubModule(long id)
        //{
        //    GenService.Delete<SubModule>(id);
        //    GenService.SaveChanges();

        //}

        //public List<MenuDto> GetMenus(long submoduleId = 0)
        //{
        //    var menus = submoduleId > 0
        //        ? GenService.GetAll<Menu>()
        //            .Where(x => x.SubModuleId == submoduleId).ToList()
        //        : GenService.GetAll<Menu>().ToList();

        //    var data = menus.Select(x => new MenuDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        DisplayName = x.DisplayName,
        //        Description = x.Description,
        //        Sl = x.Sl,
        //        ModuleName = x.SubModule.Module.Name,
        //        ModuleId = x.SubModule.ModuleId,
        //        SubModuleName = x.SubModule.Name,
        //        SubModuleId = x.SubModuleId,
        //        Url = x.Url
        //    }).ToList();
        //    return data;
        //}

        //public void SaveMenu(MenuDto menudto)
        //{
        //    if (menudto.Id > 0)
        //    {
        //        var menu = GenService.GetById<Menu>(menudto.Id);
        //        menu.Name = menudto.Name;
        //        menu.DisplayName = menudto.DisplayName;
        //        menu.Sl = menudto.Sl;
        //        menu.Description = menudto.Description;
        //        menu.Url = menudto.Url;

        //        menu.SubModuleId = menudto.SubModuleId;

        //    }
        //    else
        //    {
        //        GenService.Save(new Menu
        //        {
        //            Name = menudto.Name,
        //            DisplayName = menudto.DisplayName,
        //            Sl = menudto.Sl,
        //            Description = menudto.Description,
        //            SubModuleId = menudto.SubModuleId,
        //            Url = menudto.Url

        //        });

        //    }
        //    GenService.SaveChanges();
        //}

        //public void DeleteMenu(long id)
        //{
        //    GenService.Delete<Menu>(id);
        //    GenService.SaveChanges();

        //}


        //public List<TaskDto> GetTasks()
        //{
        //    var tasks = GenService.GetAll<Finix.HRM.Infrastructure.Task>();
        //    var data = tasks.Select(x => new TaskDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        Description = x.Description
        //    }).ToList();
        //    return data;
        //}

        //public void SaveTask(TaskDto taskdto)
        //{
        //    if (taskdto.Id > 0)
        //    {
        //        var task = GenService.GetById<Finix.HRM.Infrastructure.Task>(taskdto.Id);
        //        task.Name = taskdto.Name;
        //        task.Description = taskdto.Description;

        //    }
        //    else
        //    {
        //        GenService.Save(new Finix.HRM.Infrastructure.Task
        //        {
        //            Name = taskdto.Name,
        //            Description = taskdto.Description

        //        });

        //    }

        //    GenService.SaveChanges();
        //}

        //public void DeleteTask(long id)
        //{
        //    GenService.Delete<Finix.HRM.Infrastructure.Task>(id);
        //    GenService.SaveChanges();
        //}

        //public List<RoleDto> GetRoles()
        //{
        //    var roles = GenService.GetAll<Role>().ToList();
        //    var data = roles.Select(x => new RoleDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        Description = x.Description
        //    }).ToList();
        //    return data;
        //}

        //public void SaveRole(RoleDto roleDto)
        //{
        //    if (roleDto.Id > 0)
        //    {
        //        var role = GenService.GetById<Role>(roleDto.Id);
        //        role.Name = roleDto.Name;
        //        role.Description = roleDto.Description;
        //        GenService.Save(role);

        //    }
        //    else
        //    {
        //        GenService.Save(new Role
        //        {
        //            Name = roleDto.Name,
        //            Description = roleDto.Description
        //        });
        //    }
        //    GenService.SaveChanges();
        //}

        //public void DeleteRole(long id)
        //{
        //    GenService.Delete<Role>(id);
        //    GenService.SaveChanges();
        //}

        public List<GradeDto> GetGrades()
        {
            var grades = GenService.GetAll<Grade>().ToList();
            var data = grades.Select(x => new GradeDto
            {
                Id = x.Id,
                Name = x.Name

            }).ToList();
            return data;
        }

        public void SaveGrade(GradeDto gradedto)
        {
            if (gradedto.Id > 0)
            {
                var grade = GenService.GetById<Grade>(gradedto.Id);
                grade.Name = gradedto.Name;

                GenService.Save(grade);

            }
            else
            {
                GenService.Save(new Grade
                {
                    Name = gradedto.Name,

                });
            }
            GenService.SaveChanges();
        }

        public void DeleteGrade(long id)
        {
            GenService.Delete<Grade>(id);
            GenService.SaveChanges();
        }

        public List<DesignationDto> GetDesignations()
        {
            var designs = GenService.GetAll<Designation>().ToList();
            var data = designs.Select(x => new DesignationDto
            {
                Id = x.Id,
                Name = x.Name,
                GradeId = x.GradeId,
                GradeName = x.Grade.Name

            }).ToList();
            return data;
        }

        public List<DesignationDto> GetCorrespondingDesignationsAndSteps(int gradeid)
        {
            var designs = GenService.GetAll<Designation>().Where(x => x.GradeId == gradeid);
            var deslist = designs.Select(x => new DesignationDto
            {
                Id = x.Id,
                Name = x.Name,
                GradeId = x.GradeId,
                GradeName = x.Grade.Name

            }).ToList();
            //var gradesteps = GenService.GetAll<GradeStep>().Where(x => x.GradeId == gradeid);
            //var gradeStepListDto = gradesteps.Select(x => new GradeStepDto
            //{
            //    Id = x.Id,
            //    StepName = x.StepName
            //}).ToList();
            //return new { Designations=deslist,GradeSteps=gradeStepListDto};
            return deslist;
        }
        //public List<DesignationDto> GetCorrespondingDesignationsAndSteps(int gradeid)
        //{
        //    var designs = GenService.GetAll<Designation>().Where(x => x.GradeId == gradeid);
        //    var deslist = designs.Select(x => new DesignationDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        GradeId = x.GradeId,
        //        GradeName = x.Grade.Name

        //    }).ToList();
        //    //var gradesteps = GenService.GetAll<GradeStep>().Where(x => x.GradeId == gradeid);
        //    //var gradeStepListDto = gradesteps.Select(x => new GradeStepDto
        //    //{
        //    //    Id = x.Id,
        //    //    StepName = x.StepName
        //    //}).ToList();
        //    //return new { Designations=deslist,GradeSteps=gradeStepListDto};
        //    return deslist;
        //}
        public void SaveDesignation(DesignationDto designationdto)
        {
            if (designationdto.Id > 0)
            {
                var design = GenService.GetById<Designation>(designationdto.Id);
                design.Name = designationdto.Name;
                design.GradeId = designationdto.GradeId;
                GenService.Save(design);

            }
            else
            {
                GenService.Save(new Designation
                {
                    Name = designationdto.Name,
                    GradeId = designationdto.GradeId

                });
            }
            GenService.SaveChanges();
        }

        public void DeleteDesignation(long id)
        {
            GenService.Delete<Designation>(id);
            GenService.SaveChanges();
        }


        public void DeleteEmployee(long id)
        {
            GenService.Delete<Employee>(id);
            GenService.SaveChanges();
        }

        public List<CompanyProfileDto> GetCompanyProfiles()
        {
            var companyprofiles = GenService.GetAll<CompanyProfile>();

            var data = companyprofiles.Select(x => new CompanyProfileDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,


            }).ToList();
            return data;
        }

        public List<LeaveTypeDto> GetLeaveTypes()
        {
            var leavetypes = GenService.GetAll<LeaveType>();
            var data = leavetypes.Select(x => new LeaveTypeDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                LeaveDays = x.LeaveDays,
                MaxCarryForward = x.MaxCarryForward,
                LeaveApplicableTo = (int)x.LeaveApplicableTo,
                LeaveApplicableToName = x.LeaveApplicableTo.ToString()
            }).ToList();
            return data;
        }

        public void DeleteLeaveType(long id)
        {
            GenService.Delete<LeaveType>(id);
            GenService.SaveChanges();
        }
        public void SaveLeaveTypes(LeaveTypeDto leavetypedto)
        {
            if (leavetypedto.Id > 0)
            {
                var leavetype = GenService.GetById<LeaveType>(leavetypedto.Id);
                leavetype.Code = leavetypedto.Code;
                leavetype.Name = leavetypedto.Name;
                leavetype.LeaveDays = leavetypedto.LeaveDays;
                leavetype.MaxCarryForward = leavetypedto.MaxCarryForward;
                leavetype.LeaveApplicableTo = (LeaveApplicableTo)leavetypedto.LeaveApplicableTo;
                GenService.Save(leavetype);

            }
            else
            {
                GenService.Save(new LeaveType
                {
                    Code = leavetypedto.Code,
                    Name = leavetypedto.Name,
                    LeaveDays = leavetypedto.LeaveDays,
                    MaxCarryForward = leavetypedto.MaxCarryForward,
                    LeaveApplicableTo = (LeaveApplicableTo)leavetypedto.LeaveApplicableTo
                });
            }
            GenService.SaveChanges();
        }



        //public List<DesignationDto> GetCorrespondingDesignations(int gradeId)
        //{
        //    var designations = gradeId > 0
        //        ? GenService.GetAll<Designation>()
        //            .Where(x => x.GradeId == gradeId).ToList()
        //        : GenService.GetAll<Designation>().ToList();

        //    var data = designations.Select(x => new DesignationDto
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        GradeId = x.GradeId,
        //        GradeName = x.Grade.Name

        //    }).ToList();
        //    return data;
        //}

        public List<LeaveApplicationDto> GetLeaveApplications()
        {
            var leaveapplications = GenService.GetAll<LeaveApplication>();
            var data = leaveapplications.Select(x => new LeaveApplicationDto
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                EmployeeName = x.Employee.Person.FirstName + " " + x.Employee.Person.LastName,
                FromDate = x.FromDate,
                ToDate = x.ToDate,
                TotalDays = x.TotalDays,
                LeaveTypeId = x.LeaveTypeId,
                LeaveTypeName = x.LeaveType.Name,
                Description = x.Description,
                CurrentLeaveStatus = (int)x.CurrentLeaveStatus,
                CurrentLeaveStatusName = x.CurrentLeaveStatus.ToString(),
                SubstitutorId = x.SubstitutorId,
                SubstitutorName = x.Substitutor.Person.FirstName + " " + x.Substitutor.Person.LastName,
                Address = x.Address,
                Email = x.Email,
                Phone = x.Phone,
                Comments = x.Comments
            }).ToList();
            return data;
        }

        public LeaveApplicationDto GetLeaveApplicationById(int id)
        {
            LeaveApplication LeaveApplication = GenService.GetById<LeaveApplication>(id);
            LeaveApplicationDto dto = new LeaveApplicationDto();
            dto.Description = LeaveApplication.Description;
            dto.CurrentLeaveStatusName = LeaveApplication.CurrentLeaveStatus.ToString();
            dto.SubstitutorName = LeaveApplication.Substitutor.Person.Name;
            dto.Address = LeaveApplication.Address;
            dto.Email = LeaveApplication.Email;
            dto.Phone = LeaveApplication.Phone;
            dto.Comments = LeaveApplication.Comments;
            return dto;
        }
        public void SaveLeaveApplication(LeaveApplicationDto leaveappdto)
        {
            var entity = new LeaveSupervision();
            var entityLeave = new LeaveApplication();
            //OrganoGram boss1 = GenService.GetAll<OrganoGram>().Where(r => r.EmployeeId == id).Fi rstOrDefault();
            if (leaveappdto.Id > 0)
            {
                entityLeave = _service.GetById<LeaveApplication>(leaveappdto.Id);
                entityLeave.CurrentLeaveStatus = (LeaveApplicationStatus)leaveappdto.CurrentLeaveStatus;


                entity = _service.GetAll<LeaveSupervision>().Where(r => r.LeaveApplicationId == leaveappdto.Id).FirstOrDefault();
                entity.EditDate = DateTime.Now;
                entity.LeaveApplicationStatus = (LeaveApplicationStatus)leaveappdto.CurrentLeaveStatus;

            }
            else
            {
                entityLeave.EmployeeId = leaveappdto.EmployeeId;
                entityLeave.FromDate = leaveappdto.FromDate;
                entityLeave.ToDate = leaveappdto.ToDate;
                entityLeave.LeaveTypeId = leaveappdto.LeaveTypeId;
                entityLeave.TotalDays = leaveappdto.TotalDays;
                entityLeave.Description = leaveappdto.Description;
                entityLeave.CurrentLeaveStatus = LeaveApplicationStatus.Applied;
                entityLeave.SubstitutorId = leaveappdto.SubstitutorId;
                entityLeave.Address = leaveappdto.Address;
                entityLeave.Email = leaveappdto.Email;
                entityLeave.Phone = leaveappdto.Phone;
                entityLeave.Comments = leaveappdto.Comments;



                entity.LeaveApplicationId = leaveappdto.Id;
                entity.SupervisorId = leaveappdto.SeniorId;
                entity.CreateDate = DateTime.Now;
                entity.ActionType = LeaveSupervisionType.Forward;
                entity.LeaveApplicationStatus = LeaveApplicationStatus.Applied;
                // Comments = leaveappdto.Comments

            }
            _service.Save(entityLeave);
            entity.LeaveApplicationId = entityLeave.Id;
            _service.Save(entity);
            _service.SaveChanges();
        }

        public void DeleteLeaveApplication(long id)
        {
            GenService.Delete<LeaveApplication>(id);
            GenService.SaveChanges();
        }




    }
    //public string GetLeaveApplicationStatus() 
    //{
    //    List<LeaveApplicationStatus> theList = Enum.GetValues(typeof(LeaveApplicationStatus)).Cast<LeaveApplicationStatus>().ToList();
    //}
}



