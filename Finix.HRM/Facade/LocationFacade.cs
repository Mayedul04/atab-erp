﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.DTO;

namespace Finix.HRM.Facade
{
    public class LocationFacade : BaseFacade
    {

        public List<CountryDto> GetCountry(long? id = null)
        {
            var countries = GenService.GetAll<Country>();
            if (id.HasValue)
                countries = countries.Where(x => x.Id == id);

            var data = countries.Select(x => new CountryDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name
            }).ToList();
            return data;
        }

        public void SaveCountry(CountryDto countrydto)
        {
            if (countrydto.Id > 0)
            {
                var country = GenService.GetById<Country>(countrydto.Id);
                country.Code = countrydto.Code;
                country.Name = countrydto.Name;
                GenService.Save(country);

            }
            else
            {
                GenService.Save(new Country
                {
                    Code = countrydto.Code,
                    Name = countrydto.Name
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteCountry(long id)
        {
            GenService.Delete<Country>(id);
            GenService.SaveChanges();
        }
        public List<LocationDto> GetLocation(long? id = null)
        {
            var locations = GenService.GetAll<Location>();
            if (id.HasValue)
                locations = locations.Where(x => x.Id == id);
            var data = locations.Select(x => new LocationDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                LocationType = (int)x.LocationType,
                LocationLevel = (int)x.LocationLevel,
                LocationTypeName = x.LocationType.ToString(),//Enum.GetName(typeof(LocationType), x.LocationType),
                LocationLevelName = x.LocationLevel.ToString(), //Enum.GetName(typeof(LocationLevel), x.LocationLevel),
                ParentLocation = x.ParentLocation == null ? "" : x.ParentLocation.Name,
                ParentId = x.ParentId,
                CountryId = x.CountryId,
                CountryName = x.Country == null ? "" : x.Country.Name

            }).ToList();

            return ApplyJqFilter(data).ToList();
        }

        public void SaveLocation(LocationDto locationdto)
        {

            if (locationdto.Id > 0)
            {
                var location = GenService.GetById<Location>(locationdto.Id);
                location.CountryId = locationdto.CountryId;
                location.LocationType = (LocationType)locationdto.LocationType;
                location.LocationLevel = (LocationLevel)locationdto.LocationLevel;
                location.ParentId = locationdto.ParentId == 0 ? null : locationdto.ParentId;
                location.Code = locationdto.Code;
                location.Name = locationdto.Name;
            }
            else
            {
                GenService.Save(new Location
                {
                    CountryId = locationdto.CountryId,
                    LocationType = (LocationType)locationdto.LocationType,
                    LocationLevel = (LocationLevel)locationdto.LocationLevel,
                    ParentId = locationdto.ParentId == 0 ? null : locationdto.ParentId,
                    Code = locationdto.Code,
                    Name = locationdto.Name,


                });

            }
            GenService.SaveChanges();
        }

        public void DeleteLocation(long id)
        {
            GenService.Delete<Location>(id);
            GenService.SaveChanges();
        }

        public List<LocationDto> GetCorrespondingParentLocations(int loclevel)
        {
            List<LocationDto> loclist;
            var locations = GenService.GetAll<Location>().Where(x => (int)(x.LocationLevel) == loclevel - 1);
            loclist = locations.Select(x => new LocationDto
            {
                Id = x.Id,
                CountryId = x.CountryId,
                CountryName = x.Country.Name,
                LocationType = (int)(x.LocationType),
                LocationTypeName = x.LocationType.ToString(),
                LocationLevel = (int)(x.LocationLevel),
                LocationLevelName = x.LocationLevel.ToString(),
                ParentId = x.ParentId,
                ParentLocation = x.ParentLocation.Name,
                Code = x.Code,
                Name = x.Name
            }).ToList();

            return loclist;
        }
        public List<LocationDto> GetLastLocations()
        {
            var loclist = GenService.GetAll<Location>().Where(x => (int)x.LocationLevel == 4);
            var data = loclist.Select(x => new LocationDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name
            }).ToList();
            return data;
        }

        public List<LocationDto> GetLocationHierarchy(long? parentid)
        {
            var locations = GenService.GetAll<Location>().Where(x => x.ParentId == null).ToList();
            if (parentid.HasValue)
            {
                locations = GenService.GetAll<Location>().Where(x => x.ParentId == parentid).ToList();
            }
            var data = locations.Select(x => new LocationDto
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return data;

        }

        public List<PostOfficeDto> GetPostOffices()
        {
            var postoffices = GenService.GetAll<PostOffice>();

            var data = postoffices.Select(x => new PostOfficeDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                LocationName = x.Location.Name

            }).ToList();
            return data;
        }

        public void SavePostOffice(PostOfficeDto postofficedto)
        {
            if (postofficedto.Id > 0)
            {
                var postoffice = GenService.GetById<PostOffice>(postofficedto.Id);
                postoffice.Code = postofficedto.Code;
                postoffice.Name = postofficedto.Name;
                postoffice.LocationId = postofficedto.LocationId;
            }
            else
            {
                GenService.Save(new PostOffice
                {

                    Code = postofficedto.Code,
                    Name = postofficedto.Name,
                    LocationId = postofficedto.LocationId

                });

            }
            GenService.SaveChanges();
        }

        public void DeletePostOffice(int id)
        {
            GenService.Delete<PostOffice>(id);
            GenService.SaveChanges();
        }

        public List<CountryDto> GetCountryList()
        {
            var countries = GenService.GetAll<Country>();
            var data = countries.Select(x => new CountryDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
            }).ToList();
            return data;
        }
    }
}
