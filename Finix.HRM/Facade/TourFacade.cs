﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class TourFacade:BaseFacade
    {
        public List<TourDto> GetAllTours()
        {
            var tourlist = GenService.GetAll<Tour>();
            var data = tourlist.Select(x => new TourDto
            {
                Id=x.Id,
                TourName=x.TourName,
                TourType=(int)x.TourType,
                TourTypeName=x.TourType.ToString(),
                FromDate=x.FromDate,
                ToDate=x.ToDate,
                Description=x.Description,
                TourStatus=(int)x.TourStatus,
                TourStatusName=x.TourStatus.ToString()
            }).ToList();
            return data;
        }

        public void SaveTour(TourDto tourdto)
        {
            Tour tour = GenService.GetById<Tour>(tourdto.Id);
            if (tourdto.Id>0)
            {
                tour.TourName = tourdto.TourName;
                tour.TourType = (TourType)tourdto.TourType;
                tour.FromDate = tourdto.FromDate;
                tour.ToDate = tourdto.ToDate;
                tour.Description = tourdto.Description;
                tour.TourStatus = (TourStatus)tourdto.TourStatus;
            }
            else
            {
                GenService.Save(new Tour
                {
                    TourName=tourdto.TourName,
                    TourType=(TourType)tourdto.TourType,
                    FromDate=tourdto.FromDate,
                    ToDate=tourdto.ToDate,
                    Description=tourdto.Description,
                    TourStatus=(TourStatus)tourdto.TourStatus
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteTour(int id)
        {
            GenService.Delete<Tour>(id);
            GenService.SaveChanges();
        }

        public List<TourParticipantDto> GetAllTourParticipants()
        {
            var tourparticipants = GenService.GetAll<TourParticipant>();
            var data = tourparticipants.Select(x => new TourParticipantDto
            {
                Id=x.Id,
                TourId=x.TourId,
                TourName=x.Tour.TourName,
                EmployeeId=x.EmployeeId,
                EmployeeName=x.Employee.Person.FirstName+" "+x.Employee.Person.LastName,
                SubstitutorId=x.SubstituteId,
                SubstitutorName=x.Substitute.Id==null?null:x.Substitute.Person.FirstName+" "+x.Substitute.Person.LastName,
                Remarks=x.Remarks
            }).ToList();
            return data;
        }

        public void SaveTourParticipant(TourParticipantDto tourparticipantdto)
        {
            var tourparticipant = GenService.GetById<TourParticipant>(tourparticipantdto.Id);
            if (tourparticipantdto.Id>0)
            {
                tourparticipant.TourId = tourparticipantdto.TourId;
                tourparticipant.EmployeeId = tourparticipantdto.EmployeeId;
                tourparticipant.SubstituteId = tourparticipantdto.SubstitutorId==0?null:tourparticipantdto.SubstitutorId;
                tourparticipant.Remarks = tourparticipantdto.Remarks;
            }
            else
            {
                GenService.Save(new TourParticipant
                {
                    TourId = tourparticipantdto.TourId,
                    EmployeeId = tourparticipantdto.EmployeeId,
                    SubstituteId = tourparticipantdto.SubstitutorId==0?null:tourparticipantdto.SubstitutorId,
                    Remarks = tourparticipantdto.Remarks
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteTourParticipant(int id)
        {
            GenService.Delete<TourParticipant>(id);
            GenService.SaveChanges();
        }

        public List<TourParticipantDto> GetCorrespondingTourParticipants(long TourId)
        {
            var tourparticipants = GenService.GetAll<TourParticipant>().Where(x => x.TourId == TourId);
            var data = tourparticipants.Select(x => new TourParticipantDto
            {
                Id=x.Id,
                TourId=x.TourId,
                TourName=x.Tour.TourName,
                EmployeeId=x.EmployeeId,
                EmployeeName=x.Employee.Person.FirstName+" "+x.Employee.Person.LastName,
                SubstitutorId=x.SubstituteId,
                SubstitutorName = x.SubstituteId == null ? null : x.Substitute.Person.FirstName + " " + x.Substitute.Person.LastName,
                Remarks=x.Remarks
            }).ToList();
            return data;
        }
    }
}
