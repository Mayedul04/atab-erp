﻿using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Facade
{
    public class TrainingFacade : BaseFacade
    {
        public List<VendorDto> GetVendors()
        {
            var vendors = GenService.GetAll<TrainingVendor>();
            var data = vendors.Select(x => new VendorDto
            {
                Id = x.Id,
                ContactNo = x.ContactNo,
                ContactPerson = x.ContactPerson,
                PostalCode = x.Address.PostOffice.Code,
                //AddressLine1=x.Address.AddressLine1,
                //AddressLine2=x.Address.AddressLine2,
                //AreaId=x.Address.PostOffice.LocationId,
                AreaName = x.Address.PostOffice.Location.Name,
                //ThanaId=x.Address.PostOffice.Location.ParentLocation.Id,
                ThanaName = x.Address.PostOffice.Location.ParentLocation.Name,
                //DistrictId = x.Address.PostOffice.Location.ParentLocation.ParentLocation.Id,
                DistrictName = x.Address.PostOffice.Location.ParentLocation.ParentLocation.Name,
                // DivisionId = x.Address.PostOffice.Location.ParentLocation.ParentLocation.ParentLocation.Id,
                DivisionName = x.Address.PostOffice.Location.ParentLocation.ParentLocation.ParentLocation.Name,
                VendorName = x.VendorName,
                Email = x.Email
            }).ToList();
            return data;
        }

        public Object GetAddressFromPostCode(long PostalId)
        {
            PostOffice po = GenService.GetAll<PostOffice>().SingleOrDefault(x => x.Id == PostalId);
            var address = new
            {
                AreaName = po.Location.Name,
                ThanaName = po.Location.ParentLocation.Name,
                DistrictName = po.Location.ParentLocation.ParentLocation.Name,
                DivisionName = po.Location.ParentLocation.ParentLocation.ParentLocation.Name

            };
            return address;
        }
        public void SaveVendor(VendorDto vendordto)
        {
            if (vendordto.Id > 0)
            {
                var vendor = GenService.GetById<TrainingVendor>(vendordto.Id);
                vendor.VendorName = vendordto.VendorName;
                vendor.ContactNo = vendordto.ContactNo;
                vendor.ContactPerson = vendordto.ContactPerson;
                vendor.Email = vendordto.Email;
                vendor.Address.PostOfficeId = vendordto.PostOfficeId;
            }
            else
            {
                GenService.Save(new TrainingVendor
                {
                    VendorName = vendordto.VendorName,
                    ContactNo = vendordto.ContactNo,
                    ContactPerson = vendordto.ContactPerson,
                    Email = vendordto.Email,
                    Address = new Address
                    {
                        // AddressLine1 = vendordto.AddressLine1,
                        // AddressLine2 = vendordto.AddressLine2,
                        PostOfficeId = vendordto.PostOfficeId

                    }

                });
            }
            GenService.SaveChanges();
        }

        public void DeleteVendor(int id)
        {
            GenService.Delete<TrainingVendor>(id);
            GenService.SaveChanges();
        }

        public List<TrainingDto> GetTrainings()
        {
            var trainings = GenService.GetAll<Training>();
            var data = trainings.Select(x => new TrainingDto
            {
                Id = x.Id,
                TrainingName = x.TrainingName,
                TrainingType = (int)x.TrainingType,
                TrainingTypeName = x.TrainingType.ToString()
            }).ToList();
            return data;
        }


        public void SaveTraining(TrainingDto trainingdto)
        {
            if (trainingdto.Id > 0)
            {
                var training = GenService.GetById<Training>(trainingdto.Id);
                training.TrainingName = trainingdto.TrainingName;
                training.TrainingType = (TrainingType)trainingdto.TrainingType;
            }
            else
            {
                GenService.Save(new Training
                {
                    TrainingName = trainingdto.TrainingName,
                    TrainingType = (TrainingType)trainingdto.TrainingType,
                });
            }
            GenService.SaveChanges();
        }

        public void DeleteTraining(int id)
        {
            GenService.Delete<Training>(id);
            GenService.SaveChanges();
        }

        public List<TrainingDto> GetTrainingByVendors(int? vendorid)
        {
            //var trainings = GenService.GetById<TrainingVendor>(vendorid).Trainings;
            var trainings = GenService.GetAll<Training>();
            var data = trainings.Select(x => new TrainingDto
            {
                Id = x.Id,
                TrainingName = x.TrainingName
            }).ToList();
            return data;
        }



    }
}
