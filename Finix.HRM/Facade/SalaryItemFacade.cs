﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.HRM.DTO;
using Finix.HRM.Infrastructure;
using Finix.HRM.Infrastructure.Models;
using Finix.HRM.Service;

namespace Finix.HRM.Facade
{
    public class SalaryItemFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();

        #region Old Salary
        //public List<SalaryGradeDto> GetAllSalaryGrade()
        //{
        //    var data = _service.GetAll<SalaryGrade>().ToList();
        //    return Mapper.Map<List<SalaryGradeDto>>(data);
        //}

        //public ResponseDto SaveSalaryGrade(SalaryGradeDto model)
        //{
        //    ResponseDto response = new ResponseDto();
        //    var entity = new SalaryGrade();
        //    try
        //    {
        //        if (model.Id > 0)
        //        {
        //            entity = _service.GetById<SalaryGrade>(model.Id);
        //            entity.Name = model.Name;
        //            entity.EditDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //        else
        //        {
        //            entity = Mapper.Map<SalaryGrade>(model);
        //            entity.CreateDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "Exception Occoured " + ex;
        //    }

        //    _service.Save(entity);
        //    response.Success = true;
        //    response.Message = "Appraisal Kpi Group Setting Saved Successfully";
        //    return response;
        //}

        //public List<FieldDto> GetAllFields()
        //{
        //    var data = _service.GetAll<Field>().ToList();
        //    return Mapper.Map<List<FieldDto>>(data);
        //}

        //public ResponseDto SaveFields(FieldDto model)
        //{
        //    ResponseDto response = new ResponseDto();
        //    var entity = new Field();
        //    try
        //    {
        //        if (model.Id > 0)
        //        {
        //            entity = _service.GetById<Field>(model.Id);
        //            entity.Name = model.Name;
        //            entity.TotalSalaryPercentageDecimal = model.TotalSalaryPercentageDecimal;
        //            entity.EditDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //        else
        //        {
        //            entity = Mapper.Map<Field>(model);
        //            entity.CreateDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "Exception Occoured " + ex;
        //    }

        //    _service.Save(entity);
        //    response.Success = true;
        //    response.Message = "Field Saved Successfully";
        //    return response;
        //}

        //public List<PayScaleDto> GetAllPayScales()
        //{
        //    var data = _service.GetAll<PayScale>().ToList();
        //    return Mapper.Map<List<PayScaleDto>>(data);
        //}

        //public ResponseDto SaveGetAllPayScale(PayScaleDto model)
        //{
        //    ResponseDto response = new ResponseDto();
        //    var entity = new PayScale();
        //    try
        //    {
        //        if (model.Id > 0)
        //        {
        //            entity = _service.GetById<PayScale>(model.Id);
        //            entity.Name = model.Name;
        //            entity.BasicPercentage = model.BasicPercentage;
        //            entity.CommunicationPercentage = model.CommunicationPercentage;
        //            entity.RentPercentage = model.RentPercentage;
        //            entity.MedicalPercentage = model.MedicalPercentage;
        //            entity.TelephonePercentage = model.TelephonePercentage;
        //            entity.GradeId = model.GradeId;
        //            entity.TrainningAllowancePercentage = model.TrainningAllowancePercentage;
        //            entity.IncrementFieldId = model.IncrementFieldId;
        //            entity.DecreamentFieldId = model.DecreamentFieldId;

        //            entity.EditDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //        else
        //        {
        //            entity = Mapper.Map<PayScale>(model);
        //            entity.CreateDate = DateTime.Now;
        //            entity.Status = EntityStatus.Active;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = "Exception Occoured " + ex;
        //    }

        //    _service.Save(entity);
        //    response.Success = true;
        //    response.Message = "Pay Scale Saved Successfully";
        //    return response;
        //}
        #endregion
        public List<SalaryItemDto> GetAllSalaryItems()
        {
            var data = _service.GetAll<SalaryItem>().ToList();
            return Mapper.Map<List<SalaryItemDto>>(data);
        }

       public ResponseDto SaveSalaryItem(SalaryItemDto model)
        {
            ResponseDto response = new ResponseDto();
            var entity = new SalaryItem();
            try
            {
                if (model.Id > 0)
                {
                    entity = _service.GetById<SalaryItem>(model.Id);
                    entity.Name = model.Name;
                    entity.Description = model.Description;
                    entity.IsTaxable = model.IsTaxable;
                    entity.TaxPercent = model.TaxPercent;
                    entity.SalaryItemType = model.SalaryItemType;
                    entity.DisplayOrder = model.DisplayOrder;
                    entity.IsPercentOfBasic= model.IspercentOfBasic;
                    entity.EditDate = DateTime.Now;
                    //entity.Status = EntityStatus.Active;
                    entity.Ceiling = model.Ceiling;
                    entity.Status =(SalaryItemStatus)model.Status;
                    entity.ContributionType =(SalaryItemContributionType)model.ContributionType;

                }
                else
                {
                    entity = Mapper.Map<SalaryItem>(model);
                    entity.CreateDate = DateTime.Now;
                    //entity.Status = EntityStatus.Active;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Exception Occoured " + ex;
            }

            _service.Save(entity);
            response.Success = true;
            response.Message = "Salary Item Saved Successfully";
            return response;
        }

       public List<YearMonthForProcessDto> GetYearMonthCombo()
       {
           List<YearMonthForProcessDto> dtos = new List<YearMonthForProcessDto>();
           var FiscalYears = GenService.GetAll<FiscalYears>();
           var months = Enum.GetValues(typeof(Month)).Cast<Month>().ToList();
           foreach (var item in FiscalYears)
           {
               foreach (var monthitem in months)
               {
                   YearMonthForProcessDto dto = new YearMonthForProcessDto();
                   dto.Key = item.Id + "_" + (int)monthitem;
                   dto.Value = item.Year + "_" + monthitem.ToString();
                   dtos.Add(dto);
               }
               
           }
           return dtos;
       }

       public List<SalaryProcessEditDto> GetSalaryProcessesForPeriod(string YearMonth,int? empid, long officeid)
       {
            List<int> numbers = YearMonth.Split('_').Select(Int32.Parse).ToList();
            var SalaryProcesses = new List<SalaryProcessDetail>();
            //List<int> numbers = YearMonth.Split(' ').Select(Int32.Parse).ToList();
            //int YearId = numbers[3];
            //int Month = numbers[1];
            int stroeYear = numbers[0];
            var Years = GenService.GetAll<FiscalYears>().FirstOrDefault(y => y.Year== stroeYear);
            int YearId = (int)Years.Id;

            int Month = numbers[1];
            var salaryprocess = GenService.GetAll<SalaryProcess>().Where(x => x.FiscalYearId == YearId && x.Month == (Month)Month && x.OfficeId == officeid).FirstOrDefault();
            //if(empid!=null)
            //  SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month && x.EmployeeId == empid).ToList();
            //else
            //    SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month).ToList();
            if (empid != null)
                SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcessId == salaryprocess.Id && x.EmployeeId == empid).ToList();
            else
                SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcessId == salaryprocess.Id).ToList();

            //if (empid!=null)
            //    SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month && x.EmployeeId==empid).ToList();
            //else
            //    SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month).ToList();
            List<SalaryProcessEditDto> dtos = new List<SalaryProcessEditDto>();
           foreach (var item in SalaryProcesses)
           {
               SalaryProcessEditDto dto = new SalaryProcessEditDto();
               dto.Id = item.Id;
               dto.OfficeName = item.SalaryProcess.Office.Name;
               dto.UnitName = item.SalaryProcess.OfficeUnit.Name;
               dto.SalaryItemId = item.SalaryItemId;
               dto.SalaryItemName = item.SalaryItem.Name;
               dto.Month =item.SalaryProcess.Month.ToString();
               dto.Year = item.SalaryProcess.FiscalYear.Year;
               dto.EmpName = item.Employee.Person.FirstName + " " + item.Employee.Person.LastName;
               dto.CalculatedValue = item.ProcessedAmount;
               dto.EditedValue = item.EditedAmount;
               dto.Comments = item.Comments;
               dtos.Add(dto);
           }
           
           return dtos;
       }

       public Object GetTotalSalaryByTimePeriod(string YearMonth, int? empid, long officeid)
       {
            List<int> numbers = YearMonth.Split('_').Select(Int32.Parse).ToList();
            var SalaryProcesses = new List<SalaryProcessDetail>();
            //List<int> numbers = YearMonth.Split(' ').Select(Int32.Parse).ToList();
            //int YearId = numbers[3];
            //int Month = numbers[1];
            int stroeYear = numbers[0];
            var Years = GenService.GetAll<FiscalYears>().FirstOrDefault(y => y.Year == stroeYear);
            int YearId = (int)Years.Id;
            int Month = numbers[1];
            var salaryprocess = GenService.GetAll<SalaryProcess>().Where(x => x.FiscalYearId == YearId && x.Month == (Month)Month && x.OfficeId == officeid).FirstOrDefault();
            //if(empid!=null)
            //  SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month && x.EmployeeId == empid).ToList();
            //else
            //    SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcess.FiscalYearId == YearId && (int)x.SalaryProcess.Month == Month).ToList();
            if (empid != null)
                SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcessId== salaryprocess.Id && x.EmployeeId==empid).ToList();
            else
                SalaryProcesses = GenService.GetAll<SalaryProcessDetail>().Where(x => x.SalaryProcessId == salaryprocess.Id).ToList();

            var CalculatedSalary = 0; var EditedSalary = 0;
           foreach (var item in SalaryProcesses)
           {
               if (item.SalaryItem.ContributionType==SalaryItemContributionType.Add)
               {
                   CalculatedSalary = CalculatedSalary + (int)item.ProcessedAmount;
                   EditedSalary = EditedSalary + (int)item.EditedAmount;
               }
               else if (item.SalaryItem.ContributionType == SalaryItemContributionType.Deduct)
               {
                   CalculatedSalary = CalculatedSalary - (int)item.ProcessedAmount;
                   EditedSalary = EditedSalary - (int)item.EditedAmount;
               }
           }
           return new { CalculatedTotal = CalculatedSalary, EditedTotal = EditedSalary };
       }

       public void SaveSalaryProcessDetail(SalaryProcessEditDto dto)
       {
           if (dto.Id>0)
           {
               var sal = GenService.GetById<SalaryProcessDetail>(dto.Id);
               sal.EditedAmount = dto.EditedValue;
               sal.Comments = dto.Comments;
           }
           GenService.SaveChanges();
       }

       public void DeleteSalaryProcess(int id)
       {
           var processid = GenService.GetAll<SalaryProcessDetail>().Where(x => x.Id == id).FirstOrDefault().SalaryProcessId;
           GenService.Delete<SalaryProcessDetail>(id);
           GenService.Delete<SalaryProcess>(processid);
           GenService.SaveChanges();
       }

       public List<SalaryComponentDto> GetSalaryComponents()
       {
           List<SalaryComponentDto> componentList = new List<SalaryComponentDto>{
               new SalaryComponentDto
               {
                   Checked=false,
                   DisplayText="Basic",
                   StatusText="Pending",
                   Key="1",
                   StatusVal=1
               },
               new SalaryComponentDto
               {
                   Checked=false,
                   DisplayText="Bonus",
                   Key="2",
                   StatusText="Pending",
                   StatusVal=1
               },
               new SalaryComponentDto
               {
                   Checked=false,
                   DisplayText="Incentive",
                   Key="3",
                   StatusText="Pending",
                   StatusVal=1
               },
               new SalaryComponentDto
               {
                   Checked=false,
                   DisplayText="Overtime",
                   Key="4",
                   StatusText="Pending",
                   StatusVal=1
               }
           };
           return componentList;
       }

       public List<SalaryComponentDto> DoSalaryProcess(List<SalaryComponentDto> salProcess)
       {
           foreach (var item in salProcess)
           {
               if (item.Checked==true && item.StatusVal==1)
               {

                   item.StatusText = "On Process";
                   item.StatusVal = 2;
               }
           }
           return salProcess;
       }

      
    }
}
