﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Finix.HRM.Util;
using Finix.HRM.Infrastructure.Models;

namespace Finix.HRM.Infrastructure
{
    public interface IFinixHRMContext
    {
    }

    public partial class FinixHRMContext : DbContext, IFinixHRMContext
    {
        #region ctor
        static FinixHRMContext()
        {
            Database.SetInitializer<FinixHRMContext>(null);
        }

        public FinixHRMContext()
            : base("Name=FinixHRMContext")
        {
            Database.SetInitializer(new FinixAccountsContextInitializer());
        }
        #endregion

        #region Models
        public DbSet<Country> Countries { get; set; }
        public DbSet<CompanyProfile> CompanyProfiles { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<OfficeLayer> OfficeLayers { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<OfficeUnit> OfficeUnits { get; set; }
        public DbSet<OfficePosition> OfficePositions { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<OrganoGram> OrganoGrams { get; set; }
        public DbSet<WFStep> WFSteps { get; set; }
        public DbSet<WorkFlow> WorkFlows { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<SubModule> SubModules { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<AppraisalKpiGroupSetting> AppraisalKpiGroupSettings { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<AppraisalEmployee> AppraisalEmployees { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresss { get; set; }
        public DbSet<LeaveType> LeaveTypes { get; set; }
        public DbSet<LeaveApplication> LeaveApplications { get; set; }
        public DbSet<LeaveSupervision> LeaveSupervisions { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<PostOffice> PostOffices { get; set; }
        public DbSet<KPIType> KPITypes { get; set; }
        public DbSet<KPI> KPIs { get; set; }
        public DbSet<KPIGroup> KPIGroups { get; set; }
        public DbSet<TrainingVendor> TrainingVendors { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<EmployeeTraining> EmployeeTrainings { get; set; }
        public DbSet<EmployeeEducation> EmployeeEducation { get; set; }
        public DbSet<EmploymentHistory> EmploymentHistory { get; set; }
        public DbSet<UploadedFile> UploadedFiles { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<HolidayTypes> HolidayTypes { get; set; }
        public DbSet<FiscalYears> FiscalYears { get; set; }
        public DbSet<EmployeeJobHistory> EmployeeJobHistories { get; set; }
        public DbSet<WFSetting> WFSettings { get; set; }
        public DbSet<Appraisal> Appraisals { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TourParticipant> TourParticipants { get; set; }
        public DbSet<GradeStep> GradeSteps { get; set; }
        public DbSet<KPIGroupSetting> KPIGroupSettings { get; set; }
        public DbSet<SalaryItem> SalaryItem { get; set; }
        public DbSet<GradeStepSalaryItem> GradeStepSalaryItems { get; set; }
        public DbSet<AppraisalComment> AppraisalComments { get; set; }
        public DbSet<AppraisalScore> AppraisalScores { get; set; }
        public DbSet<AppraisalGoal> AppraisalGoals { get; set; }
        public DbSet<AppraisalSupervision> AppraisalSupervisions { get; set; }
        public DbSet<SalaryProcess> SalaryProcesses { get; set; }
        public DbSet<SalaryProcessDetail> SalaryProcessDetails { get; set; }
        public DbSet<OfficeUnitSetting> OfficeUnitSettings { get; set; }
        public DbSet<EmployeeAttendance> EmployeeAttendances { get; set; }
        public DbSet<SalaryConfiguration> SalaryConfigurations { get; set; }
        public DbSet<BasicSalaryConfiguration> BasicSalaryConfiguration { get; set; }
        public DbSet<PerformanceAppraisal> PerformanceAppraisals { get; set; }
        public DbSet<EmployeeEvaluation> EmployeeEvaluations { get; set; }
        public DbSet<EvaluationMasterDetail> EvaluationMasterDetails { get; set; }
        public DbSet<EvaluationWeightSetup> EvaluationWeightSetup { get; set; }
        public DbSet<AccHeadMapping> AccHeadMapping { get; set; }
        public DbSet<AccGroupRefTypeMapping> AccGroupRefTypeMapping { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            FluentConfigurator.ConfigureGenericSettings(modelBuilder);
            FluentConfigurator.ConfigureMany2ManyMappings(modelBuilder);
            FluentConfigurator.ConfigureOne2OneMappings(modelBuilder);
        }
    }

    internal class FinixAccountsContextInitializer : CreateDatabaseIfNotExists<FinixHRMContext> //CreateDatabaseIfNotExists<CardiacCareContext>
    {
        protected override void Seed(FinixHRMContext context)
        {
            var seedDataPath = HRMSystem.SeedDataPath;
            var dropDb = HRMSystem.DropDB;
            if (string.IsNullOrWhiteSpace(seedDataPath))
                return;
            var folders = Directory.GetDirectories(seedDataPath).ToList().OrderBy(x => x);
            var msg = "";
            bool error = false;
            try
            {
                foreach (var folder in folders)
                {
                    msg += string.Format("processing for: {0}{1}", folder, Environment.NewLine);

                    var fileDir = Path.Combine(new[] { seedDataPath, folder });
                    var sqlFiles = Directory.GetFiles(fileDir, "*.sql").OrderBy(x => x).ToList();
                    foreach (var file in sqlFiles)
                    {
                        try
                        {
                            msg += string.Format("processing for: {0}{1}", file, Environment.NewLine);
                            context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                            msg += string.Format("Done....{0}", Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            error = true;
                            msg += string.Format("Failed!....{0}", Environment.NewLine);
                            msg += string.Format("{0}{1}", ex.Message, Environment.NewLine);
                        }
                    }
                }
                if (error)
                {
                    throw new Exception(msg);
                }
                base.Seed(context);
            }
            catch (Exception ex)
            {
                msg = "Error Occured while inserting seed data" + Environment.NewLine;
                if (dropDb)
                {
                    context.Database.Delete();
                    msg += ("Database is droped" + Environment.NewLine);
                }
                var errFile = seedDataPath + "\\seed_data_error.txt";
                msg += ex.Message;
                File.WriteAllText(errFile, msg);

            }
        }

    }
}
