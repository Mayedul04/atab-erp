﻿//using Finix.Auth.Infrastructure;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.HRM.Infrastructure
{
    public enum EntityStatus { Active = 1, Inactive = 0, Deleted = -1 }
    public class Entity
    {
        public Entity()
        {
            Status = EntityStatus.Active;
            CreateDate = DateTime.Now;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        // audit fields 
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
