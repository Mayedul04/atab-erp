﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class SalaryItem : Entity
    {
        public SalaryItem() { }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPercentOfBasic { get; set; }
        public bool IsTaxable { get; set; }
        public decimal TaxPercent { get; set; }
        public SalaryItemStatus SalaryItemStatus { get; set; }
        public SalaryItemType SalaryItemType { get; set; }
        public int DisplayOrder { get; set; }
        public int Ceiling { get; set; }
        public bool IsEditable { get; set; }
        public SalaryItemStatus Status { get; set; }
        public SalaryItemContributionType ContributionType { get; set; }
    }

    public class GradeStepSalaryItem : Entity
    {
        public long GradeId { get; set; }
        [ForeignKey("GradeId")]
        public virtual Grade Grade { get; set; }
        public long GradeStepId { get; set; }
        [ForeignKey("GradeStepId")]
        public virtual GradeStep GradeStep { get; set; }
        public long SalaryItemId { get; set; }
        [ForeignKey("SalaryItemId")]
        public virtual SalaryItem SalaryItem { get; set; }
        public decimal? Amount { get; set; }
        public decimal? SalaryItemUnit { get; set; }
        public GradeStepSalaryStatus Status { get; set; }
    }

    public class SalaryProcess : Entity
    {
        public SalaryProcess()
        {
            //ProcessDetails = new List<SalaryProcessDetail>();
        }
        //public long Id { get; set; }
        public long FiscalYearId { get; set; }
        [ForeignKey("FiscalYearId")]
        public virtual FiscalYears FiscalYear { get; set; }
        public long OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }

        public long OfficeUnitId { get; set; }
        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }
        public Month Month { get; set; }
        public DateTime ProcessDate { get; set; }
        public SalaryProcessStatus Status { get; set; }
        public virtual ICollection<SalaryProcessDetail> ProcessDetails { get; set; }
    }

    public class SalaryProcessDetail : Entity
    {
        //public long Id { get; set; }
        public long SalaryProcessId { get; set; }
        [ForeignKey("SalaryProcessId"), InverseProperty("ProcessDetails")]
        public virtual SalaryProcess SalaryProcess { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long SalaryItemId { get; set; }
        [ForeignKey("SalaryItemId")]
        public virtual SalaryItem SalaryItem { get; set; }
        public decimal ProcessedAmount { get; set; }
        public decimal EditedAmount { get; set; }
        public long EditorId { get; set; }
        [ForeignKey("EditorId")]
        public virtual Employee Editor { get; set; }
        public string Comments { get; set; }

    }
}
