﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class Tour : Entity
    {
        public long Id { get; set; }
        public string TourName { get; set; }
        public TourType TourType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; }
        public TourStatus TourStatus { get; set; }
    }

    public class TourParticipant : Entity
    {
        public long TourId { get; set; }
        [ForeignKey("TourId")]
        public Tour Tour { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
        public long? SubstituteId { get; set; }
        [ForeignKey("SubstituteId")]
        public Employee Substitute { get; set; }
        public string Remarks { get; set; }
    }
}
