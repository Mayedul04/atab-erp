﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("FiscalYears")]
    public class FiscalYears : Entity
    {
        public FiscalYears()
        {

        }

        public int Year { get; set; }
        public bool CurrentYear { get; set; }
        public DateTime QuarterOne { get; set; }
        public DateTime QuarterTwo { get; set; }
        public DateTime QuarterThree { get; set; }
        public DateTime QuarterFour { get; set; }
    }
}

