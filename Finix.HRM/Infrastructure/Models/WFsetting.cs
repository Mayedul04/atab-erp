﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{

    public class WFSetting : Entity
    {
        public string Name { get; set; }
        public WFTypes WFType { get; set; }
        public int TotalSteps { get; set; }
        public int TotalActiveWF { get; set; }
        public string Description { get; set; }
        public long OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }
        public long OfficeUnitId { get; set; }
        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }
    }

    public class WFStep : Entity
    {
        public long WFSettingId { get; set; }
        [ForeignKey("WFSettingId")]
        public virtual WFSetting WFSetting { get; set; }
        public int StepNo { get; set; }
        public long ExecutorId { get; set; }
        [ForeignKey("ExecutorId")]
        public virtual Employee Executor { get; set; }
        public WFActorType ExecutorType { get; set; }
    }

    public class WorkFlow : Entity
    {
        public long SettingId { get; set; }
        [ForeignKey("SettingId")]
        public virtual WFSetting WFSetting { get; set; }
        public long StepId { get; set; }
        [ForeignKey("StepId")]
        public virtual WFStep WFStep { get; set; }
        public int ObjId { get; set; }
        public WFStatus WFStatus { get; set; }
        public string Comments { get; set; }

    }
}
