﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("AccGroupRefTypeMapping")]
    public class AccGroupRefTypeMapping : Entity
    {
        public AccountHeadRefType RefType { get; set; }
        public string AccCode { get; set; }
    }
}
