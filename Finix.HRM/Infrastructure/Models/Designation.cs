﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    #region Company Structure
    [Table("CompanyProfile")]
    public class CompanyProfile : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }
    [Table("Grade")]
    public class Grade : Entity
    {
        public Grade()
        {
            Designations = new List<Designation>();
        }
        public string Name { get; set; }
        public virtual ICollection<Designation> Designations { get; set; }
    }

    [Table("Designation")]
    public class Designation : Entity
    {
        public Designation()
        {
            this.KPIs = new List<KPI>();
        }
        public long GradeId { get; set; }
        public string Name { get; set; }

        [ForeignKey("GradeId")]
        public virtual Grade Grade { get; set; }
        public virtual ICollection<KPI> KPIs { get; set; }
    }

    [Table("OfficeLayer")]
    public class OfficeLayer : Entity
    {
        public OfficeLayer()
        {
            Offices = new List<Office>();
        }
        public string Name { get; set; }
        public int Level { get; set; }
        public virtual ICollection<Office> Offices { get; set; }
    }

    [Table("Office")]
    public class Office : Entity
    {
        public Office()
        {
            SubOffices = new List<Office>();
        }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public long OfficeLayerId { get; set; }

        [ForeignKey("OfficeLayerId")]
        public virtual OfficeLayer OfficeLayer { get; set; }

        [ForeignKey("ParentId")]
        public virtual Office ParentOffice { get; set; }
        public virtual ICollection<Office> SubOffices { get; set; }
    }

    [Table("OfficeUnit")]
    public class OfficeUnit : Entity
    {
        public OfficeUnit()
        {
            SubUnits = new List<OfficeUnit>();
        }
        public string Name { get; set; }
        public UnitType UnitType { get; set; }
        public long? ParentId { get; set; }
        // public long OfficeId { get; set; }

        [ForeignKey("ParentId")]
        public virtual OfficeUnit ParentUnit { get; set; }

        //[ForeignKey("OfficeId")]
        //public virtual Office Office { get; set; }
        public virtual ICollection<OfficeUnit> SubUnits { get; set; }
    }

    public class OfficeUnitSetting : Entity
    {

        public long OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }
        public long OfficeUnitId { get; set; }
        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }
        public long? ParentUnitId { get; set; }
        [ForeignKey("ParentUnitId")]
        public virtual OfficeUnit ParentUnit { get; set; }
        public int Sl { get; set; }
    }

    [Table("OfficePosition")]
    public class OfficePosition : Entity
    {
        public string Name { get; set; }
        public long? OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }
        public long? DefaultDesignationId { get; set; }
        [ForeignKey("DefaultDesignationId")]
        public virtual Designation DefaultDesignation { get; set; }
        public long PositionWeight { get; set; }
    }

    [Table("OrganoGram")]
    public class OrganoGram : Entity
    {
        public OrganoGram()
        {
            SubOrganoGrams = new List<OrganoGram>();
        }
        public string Name { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long DesignationId { get; set; }
        public long PositionId { get; set; }
        public long OfficeId { get; set; }
        public long OfficeUnitId { get; set; }
        public long? DeptHeadId { get; set; }
        public long? ParentId { get; set; }
        [ForeignKey("DesignationId")]
        public virtual Designation Designation { get; set; }

        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }

        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }

        [ForeignKey("PositionId")]
        public virtual OfficePosition OfficePosition { get; set; }

        [ForeignKey("ParentId")]
        public virtual OrganoGram Parent { get; set; }
        [ForeignKey("DeptHeadId")]
        public virtual Employee DeptHead { get; set; }
        public virtual ICollection<OrganoGram> SubOrganoGrams { get; set; }

    }
    #endregion

    #region Auth Structure
    [Table("Role")]
    public class Role : Entity
    {
        public Role()
        {
            Tasks = new List<Task>();
            Users = new List<User>();
            Menus = new List<Menu>();

        }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
    [Table("Task")]
    public class Task : Entity
    {
        public Task()
        {
            Roles = new List<Role>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
    [Table("User")]
    public class User : Entity
    {
        public User()
        {
            Roles = new List<Role>();
            Menus = new List<Menu>();
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public long EmployeeId { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
    }

    [Table("Module")]
    public class Module : Entity
    {
        public Module()
        {
            this.SubModules = new List<SubModule>();
        }

        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public int Sl { get; set; }
        public virtual ICollection<SubModule> SubModules { get; set; }
    }

    [Table("SubModule")]
    public class SubModule : Entity
    {
        public SubModule()
        {
            this.Menus = new List<Menu>();
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public int Sl { get; set; }
        public long ModuleId { get; set; }

        [ForeignKey("ModuleId")]
        public virtual Module Module { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
    }

    [Table("Menu")]
    public class Menu : Entity
    {
        public Menu()
        {
            this.Roles = new List<Role>();
            this.Users = new List<User>();
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public int Sl { get; set; }
        public string Url { get; set; }
        public string HeadingText { get; set; }
        public string NoteHtml { get; set; }
        public long SubModuleId { get; set; }

        [ForeignKey("SubModuleId")]
        public virtual SubModule SubModule { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }

    public class GradeStep : Entity
    {
        public long GradeId { get; set; }
        [ForeignKey("GradeId")]
        public Grade Grade { get; set; }
        public string StepName { get; set; }
        public int StepNo { get; set; }
        public string Description { get; set; }
    }
    #endregion

}
