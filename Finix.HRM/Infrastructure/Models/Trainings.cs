﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("TrainingVendor")]
    public class TrainingVendor : Entity
    {
        //public TrainingVendor()
        //{
        //    Trainings = new List<Training>();
        //}

        public string VendorName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }

        public long? AddressId { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
        //public virtual ICollection<Training> Trainings { get; set; }

    }

    [Table("Training")]
    public class Training : Entity
    {
        //public Training()
        //{
        //    TrainingVendors = new List<TrainingVendor>();
        //}
        public string TrainingName { get; set; }
        public TrainingType TrainingType { get; set; }

        //public virtual ICollection<TrainingVendor> TrainingVendors { get; set; }
    }
    [Table("EmployeeTraining")]
    public class EmployeeTraining : Entity
    {
        public long TrainingVendorId { get; set; }
        [ForeignKey("TrainingVendorId")]
        public virtual TrainingVendor TrainingVendor { get; set; }
        public long TrainingId { get; set; }
        // public TrainingType TrainingType { get; set; }
        [ForeignKey("TrainingId")]
        public virtual Training Training { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TrainingHours { get; set; }
        public long EmployeeId { get; set; }
    }

    [Table("UploadedFile")]
    public class UploadedFile : Entity
    {
        public UploadedFileType UploadedFileType { get; set; }
        public string FileNameWithExtension { get; set; }
        public int FileSerialNo { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public long ReferenceId { get; set; }
    }
}
