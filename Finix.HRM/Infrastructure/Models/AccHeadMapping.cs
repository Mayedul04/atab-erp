﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("AccHeadMapping")]
    public class AccHeadMapping : Entity
    {
        public long? RefId { get; set; }
        public AccountHeadRefType RefType { get; set; }
        public string AccountHeadCode { get; set; }
    }
}
