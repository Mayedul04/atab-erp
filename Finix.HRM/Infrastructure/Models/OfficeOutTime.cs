﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{

    public class OfficeOutTime : Entity
    {
        public OfficeOutTime() { }
        public long EmployeeId { get; set; }
        public long ApprovedById { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public long? SubstituteId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        [ForeignKey("ApprovedById")]
        public virtual Employee ApprovedBy { get; set; }
        [ForeignKey("SubstituteId")]
        public virtual Employee Substitute { get; set; }

    }

    public class SalaryConfiguration : Entity
    {
        public long DesignationId { get; set; }
        [ForeignKey("DesignationId")]
        public virtual Designation Designation { get; set; }
        public bool IsIllegibleForOT { get; set; }
        public int RatePerHour { get; set; }
        public bool IsLateAttendanceMonitored { get; set; }
        public int DayForLateAttendance { get; set; }
        public bool IsLeaveEncashed { get; set; }
        public bool IsIllegibleForOvertime { get; set; }
        public int MaximumOvertimeHour { get; set; }
    }
}
