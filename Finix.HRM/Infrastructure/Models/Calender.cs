﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("Calendar")]
    public class Calendar : Entity
    {
        public Calendar()
        {
        }
        public long Year { get; set; }
        public long Month { get; set; }
        public DateTime? PresentDate { get; set; }
        public long HolidayType { get; set; }
        //public string WeekendOne { get; set; }
        //public string Weekendtwo { get; set; }
    }
}