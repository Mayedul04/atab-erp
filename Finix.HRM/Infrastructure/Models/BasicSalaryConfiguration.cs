﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class BasicSalaryConfiguration : Entity
    {
        public BasicSalaryConfiguration()
        {

        }
        public decimal WeekendOvertimeByBasicPer { get; set; }
        public decimal NationalHolidayOtByBasicPer { get; set; }
        public int MonthDays { get; set; }
        public DateTime OfficeInTime { get; set; }
        public DateTime OfficeOutTime { get; set; }
    }
}
