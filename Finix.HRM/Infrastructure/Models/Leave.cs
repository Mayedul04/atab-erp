﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{

    [Table("LeaveType")]
    public class LeaveType : Entity
    {
        public LeaveType()
        {
            this.LeaveApplicableTo = LeaveApplicableTo.All;
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public int LeaveDays { get; set; }
        public int MaxCarryForward { get; set; }
        public LeaveApplicableTo LeaveApplicableTo { get; set; }
    }

    [Table("LeaveApplication")]
    public class LeaveApplication : Entity
    {
        public LeaveApplication()
        {
            CurrentLeaveStatus = LeaveApplicationStatus.Draft;
        }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public double TotalDays { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long LeaveTypeId { get; set; }
        [ForeignKey("LeaveTypeId")]
        public virtual LeaveType LeaveType { get; set; }
        public string Description { get; set; }
        public long? SubstitutorId { get; set; }
        [ForeignKey("SubstitutorId")]
        public virtual Employee Substitutor { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public LeaveApplicationStatus CurrentLeaveStatus { get; set; }
        public string Comments { get; set; }
    }



    [Table("EmployeeLeave")]
    public class EmployeeLeave : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }

    [Table("LeaveProcess")]
    public class LeaveProcess : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }

    [Table("LeaveProcessDetail")]
    public class LeaveProcessDetail : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }

    public class LeaveSupervision : Entity
    {
        public long LeaveApplicationId { get; set; }
        [ForeignKey("LeaveApplicationId")]
        public virtual LeaveApplication LeaveApplication { get; set; }
        public long? SupervisorId { get; set; }
        [ForeignKey("SupervisorId")]
        public virtual Employee Supervisor { get; set; }
        public LeaveSupervisionType ActionType { get; set; }
        public LeaveApplicationStatus LeaveApplicationStatus { get; set; }

        public string Comments { get; set; }

    }
}
