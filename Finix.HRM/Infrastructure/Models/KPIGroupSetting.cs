﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class KPIGroupSetting : Entity
    {
        public KPIGroupSetting() { }

        public long AppraisalId { get; set; }
        public long KPIGroupId { get; set; }
        public long KPIId { get; set; }
        public int DisplayOrder { get; set; }
        public int Weight { get; set; }

        [ForeignKey("KPIId")]
        public virtual KPI KPI { get; set; }

        [ForeignKey("AppraisalId")]
        public virtual Appraisal Appraisal { get; set; }

        [ForeignKey("KPIGroupId")]
        public virtual KPIGroup KPIGroup { get; set; }
    }
}
