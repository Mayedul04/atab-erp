﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("EmploymentHistory")]
    public class EmploymentHistory : Entity
    {
        public string CompanyName { get; set; }
        public string CompanyBusiness { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string AreaofExperiences { get; set; }
        public string Responsibilities { get; set; }
        public string CompanyAddress { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long EmployeeId { get; set; }
    }
}
