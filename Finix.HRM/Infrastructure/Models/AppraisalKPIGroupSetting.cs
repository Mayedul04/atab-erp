﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{

    public class AppraisalKpiGroupSetting : Entity
    {
        public AppraisalKpiGroupSetting() { }
        public long AppraisalId { get; set; }
        public long KPIGroupId { get; set; }
        public int DisplayOrder { get; set; }
        public int Weight { get; set; }

        [ForeignKey("AppraisalId")]
        public virtual Appraisal Appraisal { get; set; }

        [ForeignKey("KPIGroupId")]
        public virtual KPIGroup KPIGroup { get; set; }

    }

    public class AppraisalEmployee : Entity
    {
        public long AppraisalId { get; set; }
        [ForeignKey("AppraisalId")]
        public virtual Appraisal Appraisal { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long DesignationId { get; set; }
        [ForeignKey("DesignationId")]
        public virtual Designation Designation { get; set; }
        public long OfficeUnitId { get; set; }
        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }
        public long OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }
        public long GradeId { get; set; }
        public virtual Grade Grade { get; set; }
        public long GradeStepId { get; set; }
        [ForeignKey("GradeStepId")]
        public virtual GradeStep GradeStep { get; set; }
        public AppraisalStatus AppraisalStatus { get; set; }
    }

    public class AppraisalComment : Entity
    {
        public long ApraisalId { get; set; }
        [ForeignKey("ApraisalId")]
        public virtual Appraisal Appraisal { get; set; }
        public long SupervisorId { get; set; }
        [ForeignKey("SupervisorId")]
        public virtual Employee Supervisor { get; set; }
        public string Comments { get; set; }
    }

    public class AppraisalScore : Entity
    {
        public long AppraisalId { get; set; }
        [ForeignKey("AppraisalId")]
        public virtual Appraisal Appraisal { get; set; }
        public long KPIId { get; set; }
        [ForeignKey("KPIId")]
        public virtual KPI KPI { get; set; }
        public long KPIGroupId { get; set; }
        [ForeignKey("KPIGroupId")]
        public virtual KPIGroup KPIgroup { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long SupervisorId { get; set; }
        [ForeignKey("SupervisorId")]
        public virtual Employee Supervisor { get; set; }
        public int Score { get; set; }

    }

    public class AppraisalGoal : Entity
    {
        public long AppraisalId { get; set; }
        [ForeignKey("AppraisalId")]
        public virtual Appraisal Appraisal { get; set; }
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long SupervisorId { get; set; }
        [ForeignKey("SupervisorId")]
        public virtual Employee Supervisor { get; set; }
        public string GoalText { get; set; }
        public int GoalAcheivementParcentage { get; set; }
    }

    public class AppraisalSupervision : Entity
    {
        public long AppraisalEmployeeId { get; set; }

        [ForeignKey("AppraisalEmployeeId")]
        public virtual AppraisalEmployee AppraisalEmployee { get; set; }
        public long SupervisorId { get; set; }
        [ForeignKey("SupervisorId")]
        public virtual Employee Supervisor { get; set; }
        public int SupervisorLevel { get; set; }

    }
}
