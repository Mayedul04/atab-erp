﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class EmployeeSalary : Entity
    {
        public long EmployeeId { get; set; }
        public long GradeId { get; set; }
        public decimal Salary { get; set; }
    }
}
