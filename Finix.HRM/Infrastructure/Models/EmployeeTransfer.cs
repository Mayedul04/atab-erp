﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class EmployeeJobHistory : Entity
    {
        public EmployeeJobHistory() { }

        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long? DesignationId { get; set; }
        [ForeignKey("DesignationId")]
        public virtual Designation Designation { get; set; }
        public long? OfficeUnitId { get; set; }
        [ForeignKey("OfficeUnitId")]
        public virtual OfficeUnit OfficeUnit { get; set; }
        public long? OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public virtual Office Office { get; set; }
        public long? GradeStepId { get; set; }
        [ForeignKey("GradeStepId")]
        public virtual GradeStep GradeStep { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ToDate { get; set; }
        public EmployeeHistoryEnum JobStatus { get; set; }
        public string Remarks { get; set; }

    }
}
