﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    public class Appraisal : Entity
    {
        public Appraisal()
        {

        }

        public string Name { get; set; }
        public long FiscalYearId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AppraisalType AppraisalType { get; set; }
        public AppraisalStatus AppraisalStatus { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime LastSubmissionDate { get; set; }
        public DateTime LastE1Date { get; set; }
        public DateTime LastE2Date { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }

        [ForeignKey("FiscalYearId")]
        public virtual FiscalYears FiscalYear { get; set; }

    }

    public class PerformanceAppraisal : Entity
    {
        public long DesignationId { get; set; }
        [ForeignKey("DesignationId")]
        public virtual Designation Designation { get; set; }
        public long KPIGroupId { get; set; }
        public bool IsGroupSelected { get; set; }
        [ForeignKey("KPIGroupId")]
        public virtual KPIGroup KPIGroup { get; set; }
        public int KPIGroupWeight { get; set; }
        public long KPIId { get; set; }
        public bool IsKPISelected { get; set; }
        [ForeignKey("KPIId")]
        public virtual KPI KPI { get; set; }
        public int KPIWeight { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class EmployeeEvaluation : Entity
    {
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long PerformanceAppraisalId { get; set; }
        [ForeignKey("PerformanceAppraisalId")]
        public virtual PerformanceAppraisal PerformanceAppraisal { get; set; }
        public int KPIMarks { get; set; }
        public long EvaluatedBy { get; set; }
        [ForeignKey("EvaluatedBy")]
        public virtual Employee Evaluator { get; set; }
        public long? ForwardedTo { get; set; }
        [ForeignKey("ForwardedTo")]
        public virtual Employee Forwarded { get; set; }
    }

    public class EvaluationMasterDetail : Entity
    {
        public long EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public long EvaluationYear { get; set; }
        public EmployeeEvaluationStatus EvaluationStatus { get; set; }
        public long? LastForwardedTo { get; set; }
        [ForeignKey("LastForwardedTo")]
        public virtual Employee LastForwarded { get; set; }
        public long LastEvaluatedBy { get; set; }
        [ForeignKey("LastEvaluatedBy")]
        public virtual Employee LastEvaluated { get; set; }
        public int? ForwardedSupervisorNo { get; set; }
        public string Remarks { get; set; }
        public string Recommendation { get; set; }
    }

    public class EvaluationWeightSetup : Entity
    {
        public DateTime SettingDate { get; set; }
        public int SelfWeight { get; set; }
        public int Supervisor1Weight { get; set; }
        public int Supervisor2Weight { get; set; }
        public bool IsActive { get; set; }
    }
}

