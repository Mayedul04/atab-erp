﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("Institute")]
    public class Institute : Entity
    {
        //public Institute()
        //{
        //    Degrees = new List<Degree>();
        //}
        public string Name { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }

        public long? AddressId { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
        //public virtual ICollection<Degree> Degrees { get; set; }

    }
    [Table("Degree")]
    public class Degree : Entity
    {
        //public Degree()
        //{
        //    Institutes = new List<Institute>();
        //}
        public string Name { get; set; }
        public DegreeLevel DegreeLevel { get; set; }
        public DegreeMajor DegreeMajor { get; set; }
        public decimal DurationInYear { get; set; }

        //public virtual ICollection<Institute> Institutes { get; set; }
    }

    [Table("EmployeeEducation")]
    public class EmployeeEducation : Entity
    {
        public long EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public long DegreeId { get; set; }

        [ForeignKey("DegreeId")]
        public virtual Degree Degree { get; set; }

        public long InstituteId { get; set; }

        [ForeignKey("InstituteId")]
        public virtual Institute Institute { get; set; }

        public long PassingYear { get; set; }
        public double Result { get; set; }
        public string Summary { get; set; }
    }

}
