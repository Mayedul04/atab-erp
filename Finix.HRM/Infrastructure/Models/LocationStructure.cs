﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("Country")]
    public class Country : Entity
    {
        public Country()
        {
            this.Locations = new List<Location>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }

    [Table("Location")]
    public class Location : Entity
    {
        public Location()
        {
            SubLocations = new List<Location>();
            LocationType = LocationType.GeneralStructure;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public LocationType LocationType { get; set; }
        public LocationLevel LocationLevel { get; set; }
        public long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
        public long? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual Location ParentLocation { get; set; }
        public virtual ICollection<Location> SubLocations { get; set; }

    }

    [Table("PostOffice")]
    public class PostOffice : Entity
    {
        public long Code { get; set; }
        public string Name { get; set; }
        public long LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

    }

    [Table("Address")]
    public class Address : Entity
    {
        public long? PostOfficeId { get; set; }
        [ForeignKey("PostOfficeId")]
        public virtual PostOffice PostOffice { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
    }


    [Table("State")]
    public class State : Entity  // no need
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

    }
}
