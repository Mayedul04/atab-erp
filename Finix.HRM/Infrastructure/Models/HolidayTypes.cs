﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{
    [Table("HolidayTypes")]
    public class HolidayTypes : Entity
    {
        public HolidayTypes()
        {
        }

        public string Name { get; set; }

    }
}
