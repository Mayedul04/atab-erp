﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.HRM.Infrastructure.Models
{

    [Table("KPIType")]
    public class KPIType : Entity
    {
        public KPIType()
        {
            this.KPIs = new List<KPI>();
        }
        public string Name { get; set; }
        //public string DisplayName { get; set; }
        public string Description { get; set; }
        public virtual ICollection<KPI> KPIs { get; set; }
    }

    [Table("KPI")]
    public class KPI : Entity
    {
        public KPI()
        {
            this.Designations = new List<Designation>();

        }
        public string Name { get; set; }
        public long? KPIGroupId { get; set; }
        [ForeignKey("KPIGroupId")]
        public virtual KPIGroup KPIGroup { get; set; }
        public long KPITypeId { get; set; }

        [ForeignKey("KPITypeId")]
        public virtual KPIType KPIType { get; set; }
        public virtual ICollection<Designation> Designations { get; set; }

    }

    

    [Table("KPIGroup")]
    public class KPIGroup : Entity
    {
        public KPIGroup()
        {
            this.KPIs = new List<KPI>();
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<KPI> KPIs { get; set; }
    }
}
