﻿using System.Configuration;

namespace Finix.HRM.Util
{
    public class ConfigReader
    {
        public static string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? "";
        }
    }
}