﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Finix.FAMS.Facade
{
   public class DisposalFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();
        private readonly CategoryFacade _cats = new CategoryFacade();

        public List<DisposeLogDto> getDisposeLogs(CategoryLevel? level,  long? catid, DateTime fromdate, DateTime enddate)
        {
            if (level == null)
                level = CategoryLevel.Primary;
            List<DisposeLog> logs = new List<DisposeLog>();

            if (level == CategoryLevel.Primary)
            {
                if (catid != null)
                    logs = _service.GetAll<DisposeLog>().Where(x => x.Asset.Category.ParentID == catid && fromdate <= x.DisposalDate && x.DisposalDate < enddate).ToList();
                else
                    logs = _service.GetAll<DisposeLog>().Where(x => fromdate <= x.DisposalDate && x.DisposalDate < enddate).ToList();
            }
            else
            {
                if (catid != null)
                    logs = _service.GetAll<DisposeLog>().Where(x => x.Asset.AssetCategoryID == catid && fromdate <= x.DisposalDate && x.DisposalDate < enddate).ToList();
                else
                    logs = _service.GetAll<DisposeLog>().Where(x => fromdate <= x.DisposalDate && x.DisposalDate < enddate).ToList();
            }
            
            return Mapper.Map<List<DisposeLogDto>>(logs);
        }
        public int Days(DateTime fromdate, DateTime enddate)
        {
            return DateTime.Today.Subtract(fromdate).Days;
        }
        public ResponseDto SaveDisposeLog(long assetid, long? userId, PointOfDepreciation cause, double? resale, string buyer)
        {
            var entity = new DisposeLogDto();   //Diposal Log Dto
            var entitydata = new DisposeLog();
            var depreciationlog = new DepreciationLog();
            var depreciationdto = new DepreciationLogDto();  // Depreciation Log Dto
            ResponseDto responce = new ResponseDto();
            try
            {
                #region Depreciation Log Data
                //Depeciation Log to Keep Tracking
                depreciationdto = getDepreciationCalculationReady(assetid, cause);
                depreciationlog = Mapper.Map<DepreciationLog>(depreciationdto);
                var lastlogid = depreciationlog.Id;
                depreciationlog.Id = -1;
                depreciationlog.Status = EntityStatus.Active;
                depreciationlog.CreateDate = DateTime.Now;
                depreciationlog.CreatedBy = userId;
                //Depeciation Log Date End 
                #endregion

                #region Disposal Data
                //Dispose Date Start
                entity = getReadyTheCalculation(assetid);
                entitydata = Mapper.Map<DisposeLog>(entity);
                entitydata.Id = -1;
                entitydata.CreateDate = DateTime.Now;
                entitydata.CreatedBy = userId;
                entitydata.Status = EntityStatus.Active;
                if (resale != null && resale > 0)
                {
                    entitydata.ResaleValue = double.Parse(resale.ToString());
                    entitydata.ResaleTo = buyer;
                    var gain = (double.Parse(resale.ToString()) - entity.DisposedValue);
                    if (gain > 0)
                    {
                        entitydata.GainOfDisposal = gain;
                        entitydata.LossOfDisposal = 0;
                    }
                    else
                    {
                        entitydata.LossOfDisposal = gain * (-1);  //Make the Value Positive
                        entitydata.GainOfDisposal = 0;
                    }
                }
                else  //No Gain or Loss without Resale
                {
                    entitydata.GainOfDisposal = 0;
                    entitydata.LossOfDisposal = 0;
                }
                //Dispose Date End  
                #endregion

                using (var tran = new TransactionScope())
                    {
                        try
                        {
                            _service.Save(entitydata);
                            _service.Save(depreciationlog);
                            var logupdate = UpdateLastLog(lastlogid, userId);  //Make Previous Depreciation Log Inactive
                            var assetupdate = UpdateAsset(assetid, userId);  //Make the asset Inactive 
                            responce.Success = true;
                            responce.Message = "Asset Disposed Successfully";
                            tran.Complete();
                        }
                        catch (Exception ex)
                        {
                            tran.Dispose();
                            responce.Success = false;
                            responce.Message = "Something Wrong!";
                        }
                    }
                
            }
            catch (Exception ex)
            {
                responce.Message = "Depreciation Log Save Failed!";
            }
            return responce;
        }
        public List<Asset_DepreciationLogDto> AssetDepreciationLog(long? catid, DateTime fromdate, DateTime enddate)
        {
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (catid !=null)
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.AssetCategoryID == catid && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            else
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            //from _cat in (
            var daydifferences = (from a in openinglog
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, enddate))
                                  }).ToList();


            var combinned = from openning in openinglog
                            join daydif in daydifferences
                            on openning.Id equals daydif.AssetID
                            select new Asset_DepreciationLogDto
                            {
                                AssetCategoryID = openning.Asset.Category.Id,
                                CategoryName = openning.Asset.Category.CategoryName,
                                AssetID = openning.AssetID,
                                BaseValue = openning.Asset.TotalValue,
                                AssetName = openning.Asset.AssetName,
                                AssetCode = openning.Asset.AssetCode,
                                OpeningValue = openning.OpeningValue,
                                AdditionalValue = 0,
                                RevaluationSurplus = (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0),
                                ClosingValue = (openning.OpeningValue + 0 + (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0)),
                                DepriciationRate = openning.Asset.DepriciationRate,
                                DepreciatedAmount = openning.DepreciatedAmount,
                                AdjustedDepreciation = (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0),
                                ClosingDepreciation = (openning.DepreciatedAmount + (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0)),
                                WrittenDownValue = ((openning.OpeningValue + 0 + (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0)) - (openning.DepreciatedAmount + (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0)))

                            };
            List<Asset_DepreciationLogDto> ned = new List<Asset_DepreciationLogDto>();
            ned = Mapper.Map<List<Asset_DepreciationLogDto>>(combinned);
            return ned;
        }
        public DepreciationLogDto getDepreciationCalculationReady(long assetid, PointOfDepreciation cause) //Set the Dto by setting all associate value
        {
            var assetinfo = new AssetDto();
            assetinfo = GetAssetById(assetid);

            double depreamount = 0;
            double baseamount = 0;
            double openingvalue = 0;
            double newwdv = 0;
            double rate = 0;
            int yearremaining = 1;
            int sumofyears = 0;
            int usefulyear = 0;
            int daydifference = 0;
            DateTime assetdate = DateTime.Today;

            DepreciationLogDto dto = new DepreciationLogDto();
            if (cause != PointOfDepreciation.OnCreate)
            {
                dto = getLastLog(assetid);
                openingvalue = dto.WrittenDownValue; ;
                yearremaining = dto.YearRemaining;
                assetdate = dto.CalculatedDate;
            }
            else
            {
                openingvalue = assetinfo.TotalValue;
            }

            baseamount = assetinfo.TotalValue;
            dto.DepriciationModel = assetinfo.DepriciationModel;
            rate = assetinfo.DepriciationRate;
            usefulyear = assetinfo.UsefulLife;
            daydifference = DateTime.Today.Subtract(assetdate).Days;
            dto.PointOfDepreciation = cause;


            if (dto.DepreciationModelName == "SumOfYears")
            {
                for (int i = 1; i <= usefulyear; i++)
                {
                    sumofyears += i;
                }
                depreamount = (openingvalue * yearremaining) / sumofyears;
                newwdv = openingvalue - depreamount;
                dto.YearRemaining = yearremaining - 1;
            }
            else if (dto.DepreciationModelName == "DecliningBalance")
            {
                if (daydifference > 0)
                    depreamount = (openingvalue * rate * daydifference) / (100 * 365);
                newwdv = openingvalue - depreamount;
            }
            else
            {
                if (daydifference > 0)
                    depreamount = (baseamount * rate * daydifference) / (100 * 365);
                newwdv = openingvalue - depreamount;
            }

            dto.DepreciatedAmount = depreamount;
            dto.DisposalValue = newwdv; //Diapose All Resudual Value
            dto.WrittenDownValue = 0;  //To Dispose WDV =0
            dto.CalculatedDate = DateTime.Now;
            dto.OpeningValue = openingvalue;
            


            return Mapper.Map<DepreciationLogDto>(dto);
        }
        public DisposeLogDto getReadyTheCalculation(long assetid) //Set the Dto by setting all associate value
        {
            var enddate = DateTime.Today;
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (assetid > 0)
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.Id == assetid).ToList();
            var data = (from openning in openinglog
                            select new DisposeLogDto
                            {
                                AssetID = assetid,
                                BaseValue = openning.Asset.TotalValue,
                                AccumulatedDepreciation = openning.DepreciatedAmount + (openning.CalculatedDate < enddate ? ((Days(openning.CalculatedDate, enddate) * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.Asset.TotalValue)) / (100 * 365)) : 0),
                                ResaleValue = 0,
                                DisposedValue= openning.WrittenDownValue - (openning.CalculatedDate < enddate ? ((Days(openning.CalculatedDate, enddate) * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.Asset.TotalValue)) / (100 * 365)) : 0),
                                DisposalDate =enddate
                            }).FirstOrDefault();
            //GainOfDisposal = ((openning.Asset.TotalValue - openning.WrittenDownValue > 0) ? openning.Asset.TotalValue - openning.WrittenDownValue : 0),
            //LossOfDisposal = ((openning.WrittenDownValue - openning.Asset.TotalValue > 0) ? openning.WrittenDownValue - openning.Asset.TotalValue : 0),
            
            DisposeLogDto dto = Mapper.Map<DisposeLogDto>(data);
            //dto.DisposalDate = DateTime.Now;
            return dto;
        }
        public DepreciationLogDto getLastLog(long assetid)   //Get the Last Depreciation History
        {
            var data = _service.GetAll<DepreciationLog>().Where(x => x.AssetID == assetid);
            if (data != null)
                data = data.OrderByDescending(x => x.Id);
            DepreciationLogDto dto = new DepreciationLogDto();
            var toprow = data.ToList().FirstOrDefault();
            dto = Mapper.Map<DepreciationLogDto>(toprow);
            return dto;
        }
        public AssetDto GetAssetById(long assetid)
        {
            var asset = _service.GetById<Asset>(assetid);
            return Mapper.Map<AssetDto>(asset);
        }
        public ResponseDto UpdateLastLog(long logid, long? userId)
        {
            var entity = new DepreciationLog();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (logid != null && logid > 0)
                {
                    entity = _service.GetById<DepreciationLog>((long)logid);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Inactive;
                    _service.Save(entity);
                    responce.Success = true;
                    
                }

            }
            catch (Exception ex)
            {
                responce.Message = "Last Log Update Failed";
            }

            return responce;
        }
        public ResponseDto UpdateAsset(long assetid, long? userId)
        {
            var entity = new Asset();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (assetid > 0)
                {
                    entity = _service.GetById<Asset>((long)assetid);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Inactive;
                    _service.Save(entity);
                    responce.Success = true;
                }
            }
            catch (Exception ex)
            {
                responce.Message = "Asset Update Failed";
            }

            return responce;
        }

        public ResponseDto SaveSelectedDispose(List<long> selectedassets, long userId, PointOfDepreciation cause)
        {
            var response = new ResponseDto();

            try
            {
                foreach (long assetid in selectedassets)
                {
                    SaveDisposeLog(assetid, userId, PointOfDepreciation.Dispose, null,"");
                }
                response.Success = true;
                response.Message = "Assets Disposal Successful";
            }
            catch (Exception ex)
            {

                response.Message = "Dispose Calculation Failed!";
            }

            return response;
        }
    }
}
