﻿using Finix.FAMS.Infrastructure;
using Finix.FAMS.Service;
using Finix.FAMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Finix.FAMS.Infrastructure.Models;

namespace Finix.FAMS.Facade
{
  public  class AssetReportFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();
        private readonly DepreciationFacade _depreciation = new DepreciationFacade();
        public List<AssetDto> GetFilturedAssets(CategoryLevel? level, long? catid, DateTime fromDate, DateTime toDate)
        {
            List<Asset> assets = new List<Asset>();
            if (level == null)
                level = CategoryLevel.Secondary;
            if (level == CategoryLevel.Primary)
            {
                if (catid != null)
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && p.Category.ParentID == catid && p.PurchaseDate > fromDate && p.PurchaseDate < toDate).ToList();
                else
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && fromDate < p.PurchaseDate && p.PurchaseDate < toDate).ToList();
            }
            else
            {
                if (catid != null)
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && p.AssetCategoryID == catid && p.PurchaseDate > fromDate && p.PurchaseDate < toDate).ToList();
                else
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && fromDate < p.PurchaseDate && p.PurchaseDate < toDate).ToList();
            }
                return Mapper.Map<List<AssetDto>>(assets);
        }

        public List<TransferLogDto> GetTransferLogs(DateTime fromDate, DateTime toDate)
         {
            fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
            toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);
            List<TransferLog> assets = new List<TransferLog>();            
                assets = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && fromDate < p.TransferDate && p.TransferDate < toDate).ToList();
            return Mapper.Map<List<TransferLogDto>>(assets);
        }
    }
}
