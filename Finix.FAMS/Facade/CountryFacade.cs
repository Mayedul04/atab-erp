﻿using AutoMapper;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace Finix.FAMS.Facade
{
    public class CountryFacade : BaseFacade
    {
        //private GenService service = new GenService();

        /* Developed by - Siddique
         * Developed on - 10 March 2016
         * Objective    - Get all countries mainly for select input 
         */
        public List<CountryDto> GetCountries()
        {
            var data = GenService.GetAll<Country>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<CountryDto>>(data);
        }
    }
}
