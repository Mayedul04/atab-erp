﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Facade
{
   public class LocationFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();
        private readonly CategoryFacade _category = new CategoryFacade();

        public void SaveLocation(long id, string name,long userId)
        {
            Location office;
            office = GenService.GetAll<Location>().Where(s => s.OfficeId == id).FirstOrDefault();
            var parent = GenService.GetAll<Location>().Where(s => s.Level == LocationTier.Company).FirstOrDefault();

            if (office != null)
            {
                office.OfficeId = id;
                office.LocationTitle = name;
                office.ParentID = parent.Id;
                office.Status = EntityStatus.Active;
                office.CreateDate = office.CreateDate;
                office.CreatedBy = office.CreatedBy;
                office.EditDate = DateTime.Now;
                office.EditedBy = userId;
                office.Level = LocationTier.Office;
                GenService.Save(office);
            }
            else
            {
                office = new Location();
                office.OfficeId = id;
                office.LocationTitle = name;
                office.ParentID = parent.Id;
                office.Status = EntityStatus.Active;
                office.CreateDate = DateTime.Now;
                office.CreatedBy = userId;
                office.Level = LocationTier.Office;
                GenService.Save(office);
            }

            GenService.SaveChanges();
            //office.
        }
        public List<LocationDto> GetLocations()
        {
            var locations = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active && r.ParentID > 0);
            return Mapper.Map<List<LocationDto>>(locations);
        }

        public List<LocationDto> GetParentsByLevel(LocationTier? level)
        {
            List<Location> locations = new List<Location>();
            if (level!=null)
                locations = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active &&   r.Level == level-1).ToList();
            else
                locations = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active && r.ParentID>0).ToList();
            return Mapper.Map<List<LocationDto>>(locations);
        }
        public List<LocationDto> GetLocationsByLevel(LocationTier? level, long parent)
        {
            List<Location> locations = new List<Location>();
            if (level != null)
                locations = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active && r.Level == level && r.ParentID==parent).ToList();
            else
                locations = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active && r.ParentID > 0 && r.ParentID == parent).ToList();
            return Mapper.Map<List<LocationDto>>(locations);
        }
        public ResponseDto SaveLocation(LocationDto dto, long? userId)
        {
            var entity = new Location();
            ResponseDto responce = new ResponseDto();
            if (dto.Id != null && dto.Id > 0)
                {
                    entity = _service.GetById<Location>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Location Edited Successfully";
                }
             else
                {
                    entity = Mapper.Map<Location>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    try
                        {
                        _service.Save(entity);
                        responce.Success = true;
                        responce.Message = "Location Saved Successfully";
                        }
                    catch (Exception ex)
                        {
                        responce.Message = "Location Save Failed";
                        }
                 }
          return responce;
        }

        public List<LocationReportDto> GetLocationsReportDetails(LocationTier? level, long? locid)
        {
            List<AssetAcquisition> Assets = new List<AssetAcquisition>();
            if (locid == null)
                locid = 3;
            if (level == null)
                level = LocationTier.Company;
            
            List<Location> locations = getLocations(locid);
            
            foreach (var loc in locations)
            {
                var asset = _service.GetAll<AssetAcquisition>().Where(x => (x.LocationID == loc.Id));
                Assets.AddRange(asset);
            }

            var acquisitions = _service.GetAll<AssetAcquisition>().Where(r => r.Status == EntityStatus.Active);
            var categories =   _service.GetAll<Category>().Where(r => r.Status == EntityStatus.Active && r.ParentID>0).ToList();

            var data = (from cat in categories
                        join acq in Assets
                        on cat.Id equals acq.Asset.Category.ParentID
                        select new LocationReportDto
                        {
                            AssetID = acq.AssetID,
                            AssetCode = acq.Asset.AssetCode,
                            AssetName = acq.Asset.AssetName,
                            CategoryID = acq.Asset.AssetCategoryID,
                            CategoryName = acq.Asset.Category.CategoryName,
                            ParentId = acq.Asset.Category.ParentID,
                            ParentName = cat.CategoryName,
                            LocationID = acq.LocationID,
                            LocationName = acq.Location.LocationTitle,
                            ParentLocID = acq.Location.ParentID,
                            Level = acq.Location.Level
                        });

            List<LocationReportDto> results = new List<LocationReportDto>();
            results= Mapper.Map<List<LocationReportDto>>(data);
           
            return results;
        }

        public List<Location> getLocations(long? id)
        {
            List<Location> List = new List<Location>();
            var parent = _service.GetById<Location>((long)id);
            List.Add(parent);
            var childrens = _service.GetAll<Location>().Where(x => x.ParentID == parent.Id);
            foreach (var child in childrens)
            {
                var sub = getLocations(child.Id);
                if (sub != null)
                    List.AddRange(sub);
            }
            return List;
        }

        public int getCount(long locid)
        {
            List<Location> locations = getLocations(locid);
            int count = 0;
            foreach (var loc in locations)
            {
                var asset = _service.GetAll<AssetAcquisition>().Where(x => (x.LocationID == loc.Id));
                count += asset.Count();
            }
            return count;
        }

        public List<LocationSummaryDto> GetLocationsSummaryReport(LocationTier? level, long? locid)
        {
            if (locid == null)
                locid = 3;
            if (level == null)
                level = LocationTier.Company;
            var location = _service.GetAll<Location>().Where(r => r.Status == EntityStatus.Active && r.Level==level && r.Id==locid).ToList();
            var assetcounts = (from a in location
                               select new
                               {
                                   LocID = a.Id,
                                   Counts = (getCount(a.Id))
                               }).ToList();
            var summary = from loc in location
                          join c in assetcounts
                          on loc.Id equals c.LocID
                          select new LocationSummaryDto
                          {
                              LocationID = loc.Id,
                              LocationName = loc.LocationTitle,
                              Level = loc.Level,
                              LevelName = Util.UiUtil.GetDisplayName(loc.Level),
                              AssetCount = c.Counts
                             
                          };
            List<LocationSummaryDto> results = new List<LocationSummaryDto>();
           
            return Mapper.Map<List<LocationSummaryDto>>(summary);
        }
        public string GetCategoryNameID(long? id)
        {
            Category data = new Category();
            if (id != null)
            {
                data = _service.GetById<Category>((long)id);
                return data.CategoryName;
            }
            else
                return "";
            
        }

        public LocationDto GetLocationbyID(long? id)
        {
            Location data = new Location();
            if (id != null)
                data = _service.GetById<Location>((long)id);
            return Mapper.Map<LocationDto>(data);
        }
    }
}
