﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Finix.FAMS.Facade
{
    public class SupplierFacade : BaseFacade
    {
        //private readonly GenService _service = new GenService();

        public IEnumerable<SupplierDto> GetSuppliers()
        {
            var supplierlist = GenService.GetAll<Supplier>().Where(s => s.Status == EntityStatus.Active).ToList();
            var allCountries = GenService.GetAll<Country>().ToList();
            var supplierDtoList = (from supplier in supplierlist
                                   join country in GenService.GetAll<Country>().ToList() on supplier.CountryId equals country.Id
                                   select new SupplierDto
                                   {
                                       Id = supplier.Id,
                                       CountryName = supplier.CountryId != null ? supplier.Country.Name : "",
                                       CountryId = supplier.CountryId,
                                       Address = supplier.Address,
                                       BankAccountNo = supplier.BankAccountNo,
                                       ContactPerson = supplier.ContactPerson,
                                       Email = supplier.Email,
                                       Fax = supplier.Fax,
                                       Name = supplier.Name,
                                       PayTerms = supplier.PayTerms,
                                       Phone = supplier.Phone
                                   }
                                    );
            return supplierDtoList;

        }

        public SupplierDto GetSupplierById(long Id)
        {
            var supplier = GenService.GetById<Supplier>(Id);
            var supplierDto = Mapper.Map<SupplierDto>(supplier);
            return supplierDto;
        }
        public List<SupplierDto> GetSupplierList()
        {
            var data = GenService.GetAll<Supplier>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<SupplierDto>>(data);
        }
     

        public ResponseDto SaveSupplier(SupplierDto dto, long? userId)
        {
            var entity = new Supplier();
            ResponseDto responce = new ResponseDto();
            //if (dto.Id != null && (dto.Id == 0 || dto.Id == dto.Id))
            //dto.Id = null;
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<Supplier>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Supplier Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Supplier>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Supplier Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                responce.Message = "Supplier Save Failed";
            }
            GenService.SaveChanges();
            return responce;
        }

        public IPagedList<SupplierDto> SupplierList(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, string searchString)
        {

            var suppliers = GenService.GetAll<Supplier>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                suppliers = suppliers.Where(m => m.Name.ToLower().Contains(searchString));
            }
           
            if (fromDate != null && fromDate > DateTime.MinValue)
                suppliers = suppliers.Where(m => m.CreateDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                suppliers = suppliers.Where(m => m.CreateDate <= toDate);
            var supplierList = from supp in suppliers
                              select new SupplierDto
                              {
                                  Id = supp.Id,
                                  Name = supp.Name,
                                  Address = supp.Address,
                                  Email = supp.Email,
                                  Fax = supp.Fax,
                                  Phone = supp.Phone,
                                  ContactPerson = supp.ContactPerson,
                                  BankAccountNo = supp.BankAccountNo,
                                  CountryId = supp.CountryId,
                                  CountryName = supp.CountryId.ToString(),
                                  PayTerms = supp.PayTerms
                                  
                              };

            return supplierList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }
    }
}
