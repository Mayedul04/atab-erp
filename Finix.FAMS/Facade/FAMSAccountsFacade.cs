﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.DTO;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Finix.FAMS.Facade
{
    public class FAMSAccountsFacade : BaseFacade
    {
        public IPagedList<AccHeadMappingDto> GetAccHeadMappingList(int pageSize, int pageCount, string searchString)
        {
            var categories = GenService.GetAll<Category>();
            var allAccHeads = (from accMap in GenService.GetAll<AccountsMapping>().Where(a => a.Status == EntityStatus.Active)
                               join cat in categories
                               on accMap.RefId equals cat.Id
                               select new AccHeadMappingDto()
                               {
                                   Id = accMap.Id,
                                   AccountHeadCode = accMap.AccountHeadCode,
                                   RefId = accMap.RefId,
                                   RefType = accMap.RefType,
                                   RefTypeName = accMap.RefType.ToString(),//UiUtil.GetDisplayName(accMap.RefType),
                                   RefName = cat.CategoryName
                               }).ToList();
            var temp = allAccHeads.OrderBy(r => r.Id).ToList().ToPagedList(pageCount, pageSize);
            return temp;
        }
        public object GetAccHeads(AccountHeadRefType refType, long officeId)
        {
            if (refType == AccountHeadRefType.AssetPurchase )
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else if (refType == AccountHeadRefType.AssetDepreciation)
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else if (refType == AccountHeadRefType.AccumulatedDepreciation)
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else if (refType == AccountHeadRefType.CashInHand)
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                {
                    var data = new Finix.Accounts.Facade.AccountsFacade().GetAccountHeadsByAccountGroupCodeAndOfficeId(code, officeId);
                    if (data != null)
                        return data.Select(d => new { Code = d.Code, Name = d.Name });
                }
            }
            else
            {
                var mapping = GenService.GetAll<AccGroupRefTypeMapping>().Where(a => a.RefType == refType && a.Status == EntityStatus.Active).OrderByDescending(a => a.Id).FirstOrDefault();
                string code = "";
                if (mapping != null)
                    code = mapping.AccCode;
                if (!string.IsNullOrEmpty(code))
                    return new { Code = code, Name = refType.ToString() };
            }
            return null;
        }
        public ResponseDto SaveAccHeadMapping(AccHeadMappingDto dto, long userId)
        {
            var response = new ResponseDto();
            var entity = new AccountsMapping();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = GenService.GetById<AccountsMapping>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    response.Success = true;
                    response.Message = "Account head mapping Update successful";
                }
                else
                {
                    entity = AutoMapper.Mapper.Map<AccountsMapping>(dto);
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    response.Success = true;
                    response.Message = "Account head mapping successful";
                }
                
            }
            catch (Exception)
            {
                response.Message = "Mapping not saved, please contact administrator.";
            }
            return response;
        }
        public string GetAccHeadCodeForVoucher(AccountHeadRefType refType, long? refId)
        {
            var data =
                GenService.GetAll<AccountsMapping>()
                    .Where(a => a.RefId == refId && a.RefType == refType && a.Status == EntityStatus.Active)
                    .FirstOrDefault();

            if (data != null)
                return data.AccountHeadCode;
            else
                return "";
        }

        public List<CategoryDto> GetMainCategory()
        {
            var data = GenService.GetAll<Category>().Where(x=>x.Level==CategoryLevel.Primary);
            return Mapper.Map<List<CategoryDto>>(data);
        }
    }
}
