﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using Finix.HRM.Facade;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Facade
{
    public class AcquisitionFacade
    {
        private readonly GenService _service = new GenService();
        private readonly HRM.Facade.EmployeeFacade _employee = new HRM.Facade.EmployeeFacade();

        public object GetGroupedCategory()
        {
            var MainCategory = _service.GetAll<Category>().Where(i => i.Status == EntityStatus.Active && i.ParentID == 1);
            var SubCategory = _service.GetAll<Category>().Where(i => i.Status == EntityStatus.Active && i.ParentID > 1);
            var Assets = _service.GetAll<Asset>().Where(i => i.Status == EntityStatus.Active);

            var data = (from prime in MainCategory
                        select new GroupDto
                        {
                            Name = prime.CategoryName,
                            children = (from item in Assets.Where(a => a.AssetCategoryID == prime.Id)
                                        select new AssetsOptions
                                        {
                                            AssetID = item.Id,
                                            AssetCode = item.AssetCode
                                        }).ToList()
                        }).ToList();
            return data;
        }

        public List<HRM.DTO.EmployeeDto> GetHREmployeeList(long officeid)
        {
            if (officeid == 4)
                officeid = 1;  //ATAB
            else if (officeid == 8)
                officeid = 2;  //ATTI
            else
                officeid = officeid;
            var employees = _employee.GetEmployees();
            return employees.Where(x => x.JoiningInformation.OfficeId == officeid).ToList();
        }

        public List<LocationDto> getLocations(LocationTier level, long parentid)
        {
            var data = _service.GetAll<Location>().Where(x => x.Level == level && x.ParentID == parentid && x.Status == EntityStatus.Active);
            return Mapper.Map<List<LocationDto>>(data);
        }
        public List<AssetAcquisitionDto> getAssetsByCustodian(long empid)
        {
            var data = _service.GetAll<AssetAcquisition>().Where(x => x.CustodianID == empid && x.Status == EntityStatus.Active);
            
            return Mapper.Map<List<AssetAcquisitionDto>>(data);
        }
        //public List<AssetAcquisitionDto> getAllAcquisition(long officeid)
        //{
        //    var acquisitions = _service.GetAll<AssetAcquisition>().Where(x => x.Status == EntityStatus.Active && x.CustodianID!=null);
        //    var employees = _employee.GetOnlyEmployeeList().Where(x => x.JoiningInformation.OfficeId == officeid);
        //    var result = (from acq in acquisitions
        //                 join emp in employees
        //                 on acq.CustodianID equals emp.Id
        //                 into combined
        //                  from com in combined.DefaultIfEmpty()
        //                  select new AssetAcquisitionDto
        //                 {
        //                     Id=acq.Id,
        //                     AssetID=acq.AssetID,
        //                     AssetName=acq.Asset.AssetName,
        //                     AssetCode=acq.Asset.AssetCode,
        //                     CustodianID= acq.CustodianID != null ? acq.CustodianID : com.Id,
        //                     EmployeeName=(acq.CustodianID!=null? com.Name: "")
        //                 }).ToList();
        //  var data=  Mapper.Map(acquisitions, result);
        //    return data;
        //}

        public ResponseDto SaveSelected(List<long> selectedassets, long custodianid, long userId)
        {
            AssetAcquisition acquisition = new AssetAcquisition();
            AssetAcquisitionDto dto = new AssetAcquisitionDto();
            var response = new ResponseDto();
            try
            {
                foreach (long assetid in selectedassets)
                {
                 
                    SaveAcquisition(assetid, custodianid, userId,PointOfDepreciation.Manual);
                }
                response.Success = true;
                response.Message = "Asset Acquisition have been Saved Successfully";
            }
            catch (Exception ex)
            {

                response.Message = "Failed!";
            }

            return response;
        }

        public ResponseDto SaveAcquisition(long assetid, long? custodianid, long? userId, PointOfDepreciation cause)
        {
            var entity = new AssetAcquisition();
            ResponseDto responce = new ResponseDto();
            
            if (IsExistData(entity.AssetID))
            {
                responce.Success = false;
                responce.Message = "Asset Can not be Assigned. It can be Tranfered from Transfer Menu.";
            }
            else
            {
                if (cause != PointOfDepreciation.OnCreate)
                {
                    entity = _service.GetAll<AssetAcquisition>().Where(x => x.AssetID == assetid).FirstOrDefault();
                    entity = _service.GetById<AssetAcquisition>(entity.Id);
                    if (custodianid != null)
                        entity.CustodianID = custodianid;
                    entity.Status = EntityStatus.Active;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                }
                else
                {
                    entity.AssetID = assetid;
                    entity.LocationID = 4;  //By Default Office
                    if (custodianid != null)
                        entity.CustodianID = custodianid;
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                }
                
                try
                {
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Asset Acquisition have been Saved Successfully";
                }
                catch (Exception ex)
                {
                    responce.Success = false;
                    responce.Message = "Failed!!";
                }
            }
            return responce;
        }

        public bool IsExistData(long assetid)
        {
            var data = _service.GetAll<AssetAcquisition>().Where(x => x.AssetID == assetid && x.CustodianID!=null).FirstOrDefault();
            if (data != null)
                return true;
            else
                return false;
        }
        public List<Location> getLocations(long id)
        {
            List<Location> List = new List<Location>();
            var parent = _service.GetById<Location>((long)id);
            List.Add(parent);
            var childrens = _service.GetAll<Location>().Where(x => x.ParentID == parent.Id);
            foreach (var child in childrens)
            {
                var sub = getLocations(child.Id);
                if (sub != null)
                    List.AddRange(sub);
            }
            return List;
        }
        public List<AssetAcquisitionDto> getAssetsByParentID(long parentid, long? catid)
        {
            List<AssetAcquisition> Assets = new List<AssetAcquisition>();
            List <AssetAcquisition> innerasset = new List<AssetAcquisition>();
            List<Location> locations = getLocations(parentid);
            foreach (var loc in locations)
            {
                if(catid!=null)
                    innerasset = _service.GetAll<AssetAcquisition>().Where(x =>  x.LocationID == loc.Id && x.CustodianID==null && (x.Asset.AssetCategoryID==catid || x.Asset.Category.ParentID == catid)).ToList();
                else
                    innerasset = _service.GetAll<AssetAcquisition>().Where(x =>  x.LocationID == loc.Id && x.CustodianID == null).ToList();
                if(innerasset!=null)
                 Assets.AddRange(innerasset);
            }
            return Mapper.Map<List<AssetAcquisitionDto>>(Assets);
        }
    }

    public class GroupDto
    {

        public string Name { get; set; }
        public List<AssetsOptions> children { get; set; }
    }

    

    public class AssetsOptions
    {
        public long AssetID { get; set; }
        public string AssetCode { get; set; }

    }
}
