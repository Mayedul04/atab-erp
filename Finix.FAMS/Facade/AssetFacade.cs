﻿using AutoMapper;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using Finix.FAMS.Infrastructure;
using Finix.Auth.DTO;
using Finix.FAMS.Util;
using Finix.FAMS.DTO;
using Finix.Accounts.DTO;
using System.Transactions;
using Finix.Auth.Facade;
using PagedList;

namespace Finix.FAMS.Facade
{
  public  class AssetFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();
        private readonly DepreciationFacade _depreciation = new DepreciationFacade();
        private readonly AcquisitionFacade _acqisition = new AcquisitionFacade();
        private readonly FAMSAccountsFacade _accounts = new FAMSAccountsFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();

        public List<AssetDto> GetAssetsByCategory(CategoryLevel? level, long? catid)
        {
            List<Asset> assets = new List<Asset>();
            if (level == null)
                level = CategoryLevel.Secondary;
            if (level == CategoryLevel.Primary)
            {
                if (catid > 0)
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && p.Category.ParentID == catid).ToList();
                else
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active).ToList();
            }
           else
            {
                if (catid > 0)
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active && p.AssetCategoryID == catid).ToList();
                else
                    assets = _service.GetAll<Asset>().Where(p => p.Status == EntityStatus.Active).ToList();
            }
            
            return Mapper.Map<List<AssetDto>>(assets);
        }

        public List<CategoryDto> GetCategoryList()
        {
            var data = GenService.GetAll<Category>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<CategoryDto>>(data);
        }

        public long getCategoryParentId(long catid)
        {
            long parent = 0;
            var data = GenService.GetById<Category>(catid);
            parent = (long)data.ParentID;
            return parent;
        }
       
        public ResponseDto SaveAsset(AssetDto dto, long? userId, long officeid)
        {
            var entity = new Asset();
            ResponseDto responce = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = _service.GetById<Asset>((long)dto.Id);
                    
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Asset Edited Successfully";

                }
                else
                {
                    entity = Mapper.Map<Asset>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;

                    var currentFiscalYear = _companyProfileFacade.GetCurrentFiscalYear(officeid);
                    entity.PurchaseDate = ((entity.PurchaseDate != null && entity.PurchaseDate >= currentFiscalYear) ? entity.PurchaseDate.Date : DateTime.Now.Date);


                   // #region purchase voucher entry
                   // var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.AssetPurchase, getCategoryParentId(entity.AssetCategoryID));
                   //// var purchaseasset = GenService.GetById<Asset>(entity.Id);
                   // if (string.IsNullOrEmpty(accHeadCode))
                   // {
                   //     responce.Message = "Purchase account head not found for the Asset " + entity.AssetName + ". Please contact system administrator.";
                   //     return responce;
                   // }
                   // var voucher = new VoucherDto();
                   // voucher.AccountHeadCode = accHeadCode;
                   // voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                   // voucher.Description = "In time of purchase of Asset " + entity.AssetName + " against direct purchase.";
                   // voucher.Credit = 0;
                   // voucher.Debit = (decimal)entity.TotalValue;
                   // voucher.CompanyProfileId = officeid;
                   // voucher.VoucherDate = entity.PurchaseDate;
                   // voucherList.Add(voucher);
                    
                   // #endregion

                   // #region contra voucher entry 
                   // if (voucherList.Count > 0)
                   // {
                   //     var CreditVoucher = new VoucherDto();

                   //     CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                   //     CreditVoucher.Description = "In time of Asset purchase.";
                   //     CreditVoucher.Credit = voucherList.Sum(c => c.Debit);
                   //     CreditVoucher.Debit = 0;
                   //     CreditVoucher.CompanyProfileId = officeid;
                   //     CreditVoucher.VoucherDate = entity.PurchaseDate;
                        
                   //             var CashAccHead = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.CashInHand, null);
                   //             if (string.IsNullOrEmpty(CashAccHead))
                   //             {
                   //                 responce.Message = "Cash in hand, not found. Please contact system Administrator.";
                   //                 return responce;
                   //             }
                   //     CreditVoucher.AccountHeadCode = CashAccHead;
                   //     voucherList.Add(CreditVoucher);
                   // }
                    
                   // #endregion
                    using (var tran = new TransactionScope()){
                        try
                        {
                            _service.Save(entity);
                            var log = _depreciation.GenerateDepreciationLog(entity.Id, userId, PointOfDepreciation.OnCreate);
                            var acqisition = _acqisition.SaveAcquisition(entity.Id,null,userId, PointOfDepreciation.OnCreate);
                            _service.Save(log);
                            tran.Complete();
                            
                        }
                        catch(Exception ex)
                        {
                            tran.Dispose();
                            responce.Success = false;
                            responce.Message = "Asset Saved Failed!";
                        }
                    }
                    //_depreciation.SaveDepreciationLog(entity.Id,userId,PointOfDepreciation.OnCreate);
                    if (voucherList.Count > 0)
                    {
                        var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                        var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                        voucherList.ForEach(v => v.VoucherNo = voucherNumber);
                        
                        var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, officeid, (long)userId);
                        responce.Id = entity.Id;
                        UpdateAssetWithVoucher(entity.Id,userId, voucherNumber);
                        if (!(bool)accountsResponse.Success)
                        {
                            responce.Message = "Asset Purchase Saving Failed. " + accountsResponse.Message;
                            return responce;
                        }
                    }

                    responce.Success = true;
                    responce.Message = "Asset Saved Successfully";
                    
                }
            }
            catch (Exception ex)
            {
                responce.Success = false;
                responce.Message = "Asset Save Failed!";
            }
            _service.SaveChanges();
            return responce;
        }

        public AssetDto GetAssetInformation(long id)
        {
            var asset = GenService.GetById<Asset>(id);
            AssetDto assetdto = new AssetDto();
            assetdto = Mapper.Map<AssetDto>(asset);
            return assetdto;
        }

        public ResponseDto UpdateAssetWithVoucher(long assetid,long? userId, string vouchercode)
        {
            var entity = new Asset();
            ResponseDto response = new ResponseDto();

            if (assetid != null && assetid > 0)
            {
                entity = _service.GetById<Asset>((long)assetid);
                entity.VoucherCode = vouchercode;
                entity.EditDate = DateTime.Now;
                entity.EditedBy = userId;
                entity.Status = EntityStatus.Active;
                _service.Save(entity);
                response.Success = true;
                response.Message = "Asset Edited Successfully";
            }
            return response;
        }

        //public ResponseDto SaveVoucher(VoucherDto dto, long? userId)
        //{
        //    var entity = new Asset();
        //    ResponseDto responce = new ResponseDto();

            //    try
            //    {
            //        if (dto.Id != null && dto.Id > 0)
            //        {
            //            entity = _service.GetById<Asset>((long)dto.Id);
            //            dto.BankAccountHead = entity.AssetCode;
            //            dto.CreateDate = entity.CreateDate;
            //            dto.CreatedBy = entity.CreatedBy;
            //            entity = Mapper.Map(dto, entity);
            //            entity.EditDate = DateTime.Now;
            //            entity.EditedBy = userId;
            //            entity.Status = EntityStatus.Active;
            //            _service.Save(entity);
            //            responce.Success = true;
            //            responce.Message = "Asset Edited Successfully";
            //        }
            //        else
            //        {
            //            entity = Mapper.Map<Asset>(dto);
            //            entity.Status = EntityStatus.Active;
            //            entity.CreateDate = DateTime.Now;
            //            entity.CreatedBy = userId;
            //            _service.Save(entity);
            //            responce.Success = true;
            //            responce.Message = " Successfully";
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        responce.Message = "Asset Save Failed";
            //    }
            //    _service.SaveChanges();
            //    return responce;
            //}

        public string GetLatestCode(AssetDto dto)
        {
            CategoryDto catdetails= GetCategoryById(dto.AssetCategoryID);
            dto.CategoryName = catdetails.CategoryName;
            var lastentered = "0";
            int newvalue = 0;
            IEnumerable<Asset> filteredassets = _service.GetAll<Asset>().Where(c => c.AssetCategoryID == dto.AssetCategoryID);
            if (filteredassets.IsAny())
            {
                lastentered = filteredassets.OrderByDescending(x => x.Id).First().AssetCode;
                String[] substrings = lastentered.ToString().Split('/');
                newvalue = Convert.ToInt32(substrings.Last()) + 1;
            }
            else
            {
                
                newvalue =+ 1;
            }
              
            string prefix="";
            if (newvalue.ToString().Length == 1)
                prefix = "000";
            else if (newvalue.ToString().Length == 2)
                prefix = "00";
            else if (newvalue.ToString().Length == 3)
                prefix = "0";
            else
                prefix = "";
            string latestcode = "Finix" + "/" + dto.CategoryName.ToString() + "/" + dto.PurchaseDate.Year.ToString() + "/" + prefix + newvalue.ToString() ;
            return latestcode;
            
        }

        public CategoryDto GetCategoryById(long catid)
        {
            var category = _service.GetById<Category>(catid);
            return Mapper.Map<CategoryDto>(category);
        }

        public AssetDto GetAssetById(long assetid)
        {
            var asset = _service.GetById<Asset>(assetid);
            return Mapper.Map<AssetDto>(asset);
        }

        public List<CategoryDto> getSecondaryCategory()
        {
            var data = GenService.GetAll<Category>().Where(c => c.Status == EntityStatus.Active && c.Level==CategoryLevel.Secondary).ToList();
            return Mapper.Map<List<CategoryDto>>(data);
        }

        public IPagedList<AssetDto> AssetList(DateTime? fromDate, DateTime? toDate, int pageSize, int pageCount, string searchString, long? catid)
        {

            var assets = GenService.GetAll<Asset>().Where(m => m.Status == EntityStatus.Active);
            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                assets = assets.Where(m => m.AssetName.ToLower().Contains(searchString));
            }
            if (catid != null)
            {
                assets = assets.Where(m => m.AssetCategoryID == catid);
            }
            if (fromDate != null && fromDate > DateTime.MinValue)
                assets = assets.Where(m => m.PurchaseDate >= fromDate);
            if (toDate != null && toDate < DateTime.MaxValue)
                assets = assets.Where(m => m.PurchaseDate <= toDate);
            var membersList = from asset in assets
                              select new AssetDto
                              {
                                  Id = asset.Id,
                                  AssetCode = asset.AssetCode,
                                  AssetName = asset.AssetName,
                                  PurchaseDate = asset.PurchaseDate,
                                  AssetCategoryID = asset.AssetCategoryID,
                                  CategoryName=asset.Category.CategoryName,
                                  SupplierID=asset.SupplierID,
                                  SupplierName=asset.Supplier.Name,
                                  DepriciationModel=asset.DepriciationModel,
                                  ModelName= asset.DepriciationModel.ToString(),
                                  DepriciationRate=asset.DepriciationRate,
                                  TotalValue=asset.TotalValue,
                              };
            
            return membersList.OrderBy(r => r.Id).ToPagedList(pageCount, pageSize);

        }
    }
}
