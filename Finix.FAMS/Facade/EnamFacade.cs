﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.FAMS.DTO;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Util;

namespace Finix.FAMS.Facade
{
  public  class EnumFacade : BaseFacade
    {
        public List<EnumDto> GetDepreciationType()
        {
            var typeList = Enum.GetValues(typeof(DepriciationType))
               .Cast<DepriciationType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetDepreciationModel()
        {
            var typeList = Enum.GetValues(typeof(DepriciationModel))
               .Cast<DepriciationModel>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetSuppliers()
        {
            var typeList = Enum.GetValues(typeof(DemoSupplier))
               .Cast<DemoSupplier>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetEmployees()
        {
            var typeList = Enum.GetValues(typeof(DemoEmployee))
               .Cast<DemoEmployee>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetServiceType()
        {
            var typeList = Enum.GetValues(typeof(ServiceType))
               .Cast<ServiceType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetPointOfDepreciation()
        {
            var typeList = Enum.GetValues(typeof(PointOfDepreciation))
               .Cast<PointOfDepreciation>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetReportGroupby()
        {
            var typeList = Enum.GetValues(typeof(ReportGroupBy))
               .Cast<ReportGroupBy>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = t.ToString(),
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetTransferType()
        {
            var typeList = Enum.GetValues(typeof(TransferType))
               .Cast<TransferType>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }

        public List<EnumDto> GetLocationLevels()
        {
            var typeList = Enum.GetValues(typeof(LocationTier))
               .Cast<LocationTier>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }
        public List<EnumDto> GetCategoryLevels()
        {
            var typeList = Enum.GetValues(typeof(CategoryLevel))
               .Cast<CategoryLevel>()
               .Select(t => new EnumDto
               {
                   Id = ((int)t),
                   Name = UiUtil.GetDisplayName(t)
               });
            return typeList.ToList();
        }
    }
}
