﻿using AutoMapper;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Auth.DTO;

namespace Finix.FAMS.Facade
{
   public class CategoryFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();

        public IEnumerable<CategoryDto> GetCategoryList()
        {
            var categorylist = GenService.GetAll<Category>().Where(s => s.Status == EntityStatus.Active && s.ParentID != null && s.ParentID>0).ToList();

            var categoryDtoList = (from category in categorylist

                                   select new CategoryDto
                                   {
                                       Id = category.Id,
                                       Code = category.Code,
                                       Level=category.Level,
                                       LevelName=Util.UiUtil.GetDisplayName(category.Level),
                                       CategoryName = category.CategoryName,
                                       ParentId = category.ParentID,
                                       ParentName = GetCategorybyID(category.ParentID).CategoryName,
                                       Details = category.Details,
                                   }
                                    );
            return categoryDtoList;

        }

        public List<CategoryDto> GetCategoryListForSelect()
        {
            var data = GenService.GetAll<Category>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<CategoryDto>>(data);
        }
        public ResponseDto SaveCategories(CategoryDto dto, long? userId)
        {
            var entity = new Category();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (dto.Id != null && dto.Id > 0)
                {
                    entity = _service.GetById<Category>((long)dto.Id);
                    dto.CreateDate = entity.CreateDate;
                    dto.CreatedBy = entity.CreatedBy;
                    entity = Mapper.Map(dto, entity);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Asset Category Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Category>(dto);
                    entity.Status = EntityStatus.Active;
                    entity.CreateDate = DateTime.Now;
                    entity.CreatedBy = userId;
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Asset Category Saved Successfully";
                }
            }
            catch (Exception)
            {
                responce.Message = "Asset Category Save Failed";
            }
            _service.SaveChanges();
            return responce;
        }
        public List<CategoryDto> GetCategoryListbyParent(long parent)
        {
            var data = GenService.GetAll<Category>().Where(c => c.ParentID == parent && c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<CategoryDto>>(data);
        }

        public List<CategoryDto> getFilturedCategory(CategoryLevel? level, long? catid)
        {
            List<Category> categories = new List<Category>();
            if (level == null)
                level =  CategoryLevel.Primary;
            if (level == CategoryLevel.Primary)
            {
                if (catid != null)
                    categories = _service.GetAll<Category>().Where(x => x.Status == EntityStatus.Active && x.ParentID == catid && x.Level == CategoryLevel.Primary).OrderBy(x => x.Id).ToList();
                else
                    categories = _service.GetAll<Category>().Where(x => x.Status == EntityStatus.Active && x.Level == CategoryLevel.Primary).OrderBy(x => x.Id).ToList();
            }
            else
            {
                if (catid != null)
                    categories = _service.GetAll<Category>().Where(x => x.Status == EntityStatus.Active && x.ParentID == catid && x.Level == CategoryLevel.Secondary).OrderBy(x => x.Id).ToList();
                else
                    categories = _service.GetAll<Category>().Where(x => x.Status == EntityStatus.Active && x.Level == CategoryLevel.Secondary).OrderBy(x => x.Id).ToList();
            }
            var categoryDtoList = (from category in categories
                                   select new CategoryDto
                                   {
                                       Id = category.Id,
                                       Code = category.Code,
                                       Level = category.Level,
                                       LevelName = Util.UiUtil.GetDisplayName(category.Level),
                                       CategoryName = category.CategoryName,
                                       ParentId = category.ParentID,
                                       ParentName = GetCategorybyID(category.ParentID).CategoryName,
                                       Details = category.Details,
                                   }).ToList();
            return categoryDtoList;

        }

        public CategoryDto GetCategorybyID(long? id)
        {
            Category data = new Category();
            if(id!=null )
                data = _service.GetById<Category>((long)id);
            
            return Mapper.Map<CategoryDto>(data);
        }
        public List<CategoryDto> GetParentsByLevel(CategoryLevel? level)
        {
            List<Category> locations = new List<Category>();
            if (level != null)
                locations = _service.GetAll<Category>().Where(r => r.Status == EntityStatus.Active && r.Level == level - 1).ToList();
            else
                locations = _service.GetAll<Category>().Where(r => r.Status == EntityStatus.Active && r.ParentID > 0).ToList();
            return Mapper.Map<List<CategoryDto>>(locations);
        }
    }
}
