﻿using AutoMapper;
using Finix.Accounts.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.DTO;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Facade.AutoMaps
{
   public class FAMSMappingProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            CategoryFacade _category = new CategoryFacade();
             AccHeadDto _accountsDto=new AccHeadDto();
           

            CreateMap<Category, CategoryDto>()
             .ForMember(d => d.LevelName, o => o.MapFrom(s => s.Level > 0 ? UiUtil.GetDisplayName(s.Level) : ""));
            CreateMap<CategoryDto, Category>();
            //  .ForMember(d => d.ParentName, o => o.MapFrom(s => _category.GetCategorybyID(s.ParentID).CategoryName))
            //CreateMap<AccountsMapping, AccountsMappingDto>();
            //CreateMap<AccountsMappingDto, AccountsMapping>();

            CreateMap<AccountsMapping, AccHeadMappingDto>();
            CreateMap<AccHeadMappingDto, AccountsMapping>();

            CreateMap<Asset, AssetDto>()
             .ForMember(d => d.CategoryName, o => o.MapFrom(s => (s.Category.CategoryName)))
             .ForMember(d => d.SupplierName, o => o.MapFrom(s => s.Supplier.Name))
             .ForMember(d => d.ModelName, o => o.MapFrom(s => s.DepriciationModel > 0 ? UiUtil.GetDisplayName(s.DepriciationModel) : ""))
             .ForMember(d => d.ServiceTypeName, o => o.MapFrom(s => s.ServiceTypes > 0 ? UiUtil.GetDisplayName(s.ServiceTypes) : ""));
           


            CreateMap<AssetDto, Asset>()
                .ForMember(d => d.Supplier, o => o.Ignore());

            CreateMap<DepreciationLog, DepreciationLogDto>()
                .ForMember(d => d.AssetName, o => o.MapFrom(s => s.Asset.AssetName))
                .ForMember(d => d.BaseValue, o => o.MapFrom(s => s.Asset.TotalValue))
                .ForMember(d => d.DepreciationModelName, o => o.MapFrom(s => s.DepriciationModel > 0 ? UiUtil.GetDisplayName(s.DepriciationModel) : ""))
                .ForMember(d => d.CauseOfDepreciation, o => o.MapFrom(s => s.PointOfDepreciation > 0 ? UiUtil.GetDisplayName(s.PointOfDepreciation) : ""));

            CreateMap<DepreciationLogDto, DepreciationLog>()
                .ForMember(d => d.DepriciationModel, o => o.MapFrom(s=> s.DepriciationModel))
                .ForMember(d => d.PointOfDepreciation, o => o.MapFrom(s => s.PointOfDepreciation))
                .ForMember(d => d.DisposedValue, o => o.MapFrom(s => s.DisposalValue));

            CreateMap<Asset, Asset_DepreciationLogDto>()
                .ForMember(d => d.AssetCategoryID, o => o.MapFrom(s => s.AssetCategoryID))
                .ForMember(d => d.AssetID, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.AssetName, o => o.MapFrom(s => s.AssetName))
                .ForMember(d => d.AssetCode, o => o.MapFrom(s => s.AssetCode))
                .ForMember(d => d.BaseValue, o => o.MapFrom(s => s.TotalValue))
                .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.Category.CategoryName))
                .ForMember(d => d.DepriciationModel, o => o.MapFrom(s => s.DepriciationModel))
                .ForMember(d => d.ModelName, o => o.MapFrom(s => UiUtil.GetDisplayName(s.DepriciationModel)))
                .ForMember(d => d.DepriciationRate, o => o.MapFrom(s => s.DepriciationRate))
                .ForMember(d => d.PurchaseDate, o => o.MapFrom(s => s.PurchaseDate));

            CreateMap<DepreciationLog, Asset_DepreciationLogDto>()
                .ForMember(d => d.OpeningValue, o => o.MapFrom(s => s.OpeningValue))
                .ForMember(d => d.PointOfDepreciation, o => o.MapFrom(s => s.PointOfDepreciation))
                .ForMember(d => d.WrittenDownValue, o => o.MapFrom(s => s.WrittenDownValue))
                .ForMember(d => d.RevaluationSurplus, o => o.MapFrom(s => s.RevaluationSurplus))
                .ForMember(d => d.DisposedValue, o => o.MapFrom(s => s.DisposedValue))
                .ForMember(d => d.YearRemaining, o => o.MapFrom(s => s.YearRemaining))
                .ForMember(d => d.CalculatedDate, o => o.MapFrom(s => s.CalculatedDate))
                .ForMember(d => d.DepreciatedAmount, o => o.MapFrom(s => s.DepreciatedAmount));

            CreateMap<TransferLog, TransferLogDto>()
                .ForMember(d => d.AssetName, o => o.MapFrom(s => s.Asset.AssetName))
                .ForMember(d => d.AssetCode, o => o.MapFrom(s => s.Asset.AssetCode))
                .ForMember(d => d.TransferTypeName, o => o.MapFrom(s => s.TransferType > 0 ?UiUtil.GetDisplayName(s.TransferType):""))
                .ForMember(d => d.SourceTitle, o => o.MapFrom(s => (s.TransferType == Infrastructure.TransferType.Employee ? s.SourceEmployee.Person.Name : s.SourceLocation.LocationTitle)))
                .ForMember(d => d.DestinationTitle, o => o.MapFrom(s => (s.TransferType == Infrastructure.TransferType.Employee ? s.DestinationEmployee.Person.Name : s.Destination.LocationTitle)));
            CreateMap<TransferLogDto, TransferLog>();

            CreateMap<AssetAcquisition, AssetAcquisitionDto>()
                .ForMember(d => d.AssetName, o => o.MapFrom(s => s.Asset.AssetName))
                .ForMember(d => d.AssetCode, o => o.MapFrom(s => s.Asset.AssetCode))
                .ForMember(d => d.LocationName, o => o.MapFrom(s => s.Location.LocationTitle));
            CreateMap<AssetAcquisitionDto, AssetAcquisition>();

            CreateMap<DisposeLog, DisposeLogDto>()
                .ForMember(d => d.AssetName, o => o.MapFrom(s => s.Asset.AssetName))
                .ForMember(d => d.AssetCode, o => o.MapFrom(s => s.Asset.AssetCode))
                .ForMember(d => d.DisposedValue, o => o.MapFrom(s => s.Asset.TotalValue-s.AccumulatedDepreciation));
            CreateMap<DisposeLogDto, DisposeLog>();

            CreateMap<Location, LocationDto>()
                .ForMember(d => d.ParentName, o => o.MapFrom(s => s.Locations.LocationTitle))
                .ForMember(d => d.LevelName,  o => o.MapFrom(s => s.Level > 0 ? UiUtil.GetDisplayName(s.Level) : ""));
            CreateMap<LocationDto, Location>();

            

            CreateMap<Designation, DesignationDto>()
                .ForMember(d => d.ParentName, o => o.MapFrom(s => s.ParentDesignation.Name));
            CreateMap<DesignationDto, Designation>();

            CreateMap<Thana, ThanaDto>();
            CreateMap<ThanaDto, Thana>();

            CreateMap<OfficeDesignationArea, OfficeDesignationAreaDto>();
            CreateMap<OfficeDesignationAreaDto, OfficeDesignationArea>();

            CreateMap<District, DistrictDto>()
                 .ForMember(d => d.Name, o => o.MapFrom(s => s.DistrictNameEng));
            CreateMap<DistrictDto, District>();

            CreateMap<Upazila, UpzilaDto>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.UpazilaNameEng));
            CreateMap<UpzilaDto, Upazila>();

            CreateMap<Employee, EmployeeDto>()
                 .ForMember(d => d.Name, o => o.MapFrom(s => s.PersonId != null ? s.Person.FirstName + " " + s.Person.LastName : ""))
                 .ForMember(d => d.EmployeeTypeName, o => o.MapFrom(s => UiUtil.GetDisplayName(s.EmployeeType)))
                 .ForMember(d => d.JoiningDateText, o => o.MapFrom(s => s.JoiningDate != null ? ((DateTime)s.JoiningDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""));
            CreateMap<EmployeeDto, Employee>();

            CreateMap<OfficeDesignationSetting, OfficeDesignationSettingDto>()
                .ForMember(d => d.ParentDesignationSettingName, o => o.MapFrom(s => s.ParentOfficeDesignationSetting.Name))
                .ForMember(d => d.OfficeName, o => o.MapFrom(s => s.Office.Name))
                .ForMember(d => d.DesignationName, o => o.MapFrom(s => s.Designation.Name));
            CreateMap<OfficeDesignationSettingDto, OfficeDesignationSetting>();

            CreateMap<EmployeeDesignationMapping, EmployeeDesignationMappingDto>()
                .ForMember(d => d.EmployeeName, o => o.MapFrom(s => s.Employee.Person.FirstName + " " + s.Employee.Person.LastName))
                .ForMember(d => d.OfficeDesignationSettingName, o => o.MapFrom(s => s.OfficeDesignationSetting.Name))
                .ForMember(d => d.EmpCode, o => o.MapFrom(s => s.Employee.EmpCode));
            CreateMap<EmployeeDesignationMappingDto, EmployeeDesignationMapping>();

            CreateMap<Country, CountryDto>();
            CreateMap<CountryDto, Country>();


            CreateMap<Division, DivisionDto>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.DivisionNameEng));
            CreateMap<DivisionDto, Division>();

            CreateMap<Supplier, SupplierDto>();
            CreateMap<SupplierDto, Supplier>();




        }
    }
}
