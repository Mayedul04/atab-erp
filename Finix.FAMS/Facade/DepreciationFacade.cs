﻿using AutoMapper;
using Finix.Auth.DTO;
using Finix.FAMS.Dto;
using Finix.FAMS.DTO;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Finix.FAMS.Util;
using Finix.Accounts.DTO;
using Finix.Auth.Facade;

namespace Finix.FAMS.Facade
{
   public class DepreciationFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();
        private readonly CategoryFacade _cats = new CategoryFacade();
        private readonly FAMSAccountsFacade _accounts = new FAMSAccountsFacade();
        private readonly CompanyProfileFacade _companyProfileFacade = new CompanyProfileFacade();
        public List<DepreciationLogDto> GetDepreciationLog()
        {
            var logs = _service.GetAll<DepreciationLog>().Where(c => c.Status == EntityStatus.Active).ToList();
            return Mapper.Map<List<DepreciationLogDto>>(logs);
        }

        public List<CategoryDto> GetCategoriesByLevel(CategoryLevel? level)
        {
            List<Category> cats = new List<Category>();
            if (level != null)
                cats = _service.GetAll<Category>().Where(r => r.Status == EntityStatus.Active && r.Level == level).ToList();
            else
                cats = _service.GetAll<Category>().Where(r => r.Status == EntityStatus.Active && r.ParentID > 0).ToList();
            return Mapper.Map<List<CategoryDto>>(cats);
        }

        public List<Asset_DepreciationLogDto> GetAssetsLogByCategory(long? catid)
        {
            List<Asset> assets=new List<Asset>();
            List<DepreciationLog> logs = new List<DepreciationLog>();
            if (catid!=null)
            {
                assets = _service.GetAll<Asset>().Where(c => c.AssetCategoryID == catid && c.Status == EntityStatus.Active).ToList();
                logs =   _service.GetAll<DepreciationLog>().Where(c => c.Asset.AssetCategoryID==catid && c.Status == EntityStatus.Active).ToList();
            }
                
            else
            {
                assets = _service.GetAll<Asset>().Where(c => c.Status == EntityStatus.Active).ToList();
                logs = _service.GetAll<DepreciationLog>().Where(c => c.Status == EntityStatus.Active).ToList();
            }
                
             
            var daydifferences = (from a in logs
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, DateTime.Today))
                                  }).ToList();
            
            var data = from asset in assets
                       join log in logs
                       on asset.Id equals log.AssetID
                       join daydif in daydifferences
                       on log.Id equals daydif.AssetID
                       select new Asset_DepreciationLogDto
                       {
                           AssetID=asset.Id,
                           AssetCategoryID = asset.AssetCategoryID,
                           AssetCode = asset.AssetCode,
                           AssetName=asset.AssetName,
                           DepriciationRate=asset.DepriciationRate,
                           CategoryName=asset.Category.CategoryName,
                           DepriciationModel=asset.DepriciationModel,
                           ModelName= UiUtil.GetDisplayName(asset.DepriciationModel),
                           OpeningValue = log.OpeningValue,
                           PointOfDepreciation =log.PointOfDepreciation,
                           DepreciatedAmount=log.DepreciatedAmount,
                           RevaluationSurplus=log.RevaluationSurplus,
                           YearRemaining=log.YearRemaining,
                           CalculatedDate=log.CalculatedDate,
                           AdjustedDepreciation = ((daydif.Days * asset.DepriciationRate * (asset.DepriciationModel == DepriciationModel.DecliningBalance ? log.WrittenDownValue : log.OpeningValue)) / (100 * 365)),
                           WrittenDownValue = (log.OpeningValue + log.RevaluationSurplus) - (log.DepreciatedAmount + ((daydif.Days * asset.DepriciationRate * (asset.DepriciationModel == DepriciationModel.DecliningBalance ? log.WrittenDownValue : log.OpeningValue)) / (100 * 365)))
                       };
            return Mapper.Map<List<Asset_DepreciationLogDto>>(data);
        }
        public Category getCategoryId(long assetid)
        {
            Category category = new Category();
            var data = _service.GetById<Asset>(assetid);
            category = _service.GetById<Category>(data.AssetCategoryID);
            return category;
        }
        public ResponseDto SaveDepreciationLog(long assetid, long? userId, PointOfDepreciation cause, double? surplus, long officeid)
        {
            double totaldepreciated = 0;
            var entity = new DepreciationLog();
            var dto = new DepreciationLogDto();
            ResponseDto responce = new ResponseDto();
            var voucherList = new List<VoucherDto>();
            try
            {
                
                dto= getReadyTheCalculation(assetid, cause);
                entity = Mapper.Map<DepreciationLog>(dto);
                var lastlogid = entity.Id;
                entity.Id = -1;
                entity.AssetID = assetid;
                entity.Status = EntityStatus.Active;
                entity.CreateDate = DateTime.Now;
                entity.CreatedBy = userId;
                entity.CalculatedDate = DateTime.Now.Date;
                Category category = new Category();
                category = getCategoryId(assetid);
                if (surplus !=null || surplus>0)
                {
                    entity.RevaluationSurplus = double.Parse(surplus.ToString());
                    entity.WrittenDownValue = entity.WrittenDownValue + entity.RevaluationSurplus;
                }
                   
                else
                    entity.RevaluationSurplus = 0;
                if (entity.DepreciatedAmount > 0 || entity.RevaluationSurplus > 0)
                {
                    totaldepreciated += entity.DepreciatedAmount;

                    var currentFiscalYear = _companyProfileFacade.GetCurrentFiscalYear(officeid);
                    entity.CalculatedDate = ((entity.CalculatedDate != null && entity.CalculatedDate >= currentFiscalYear) ? entity.CalculatedDate : DateTime.Now.Date);

                    #region depreciaton voucher entry
                    var accHeadCode = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.AssetDepreciation, category.ParentID);
                    // var purchaseasset = GenService.GetById<Asset>(entity.Id);
                    if (string.IsNullOrEmpty(accHeadCode))
                    {
                        responce.Message = "Depreciation account head not found for the Asset " + entity.Asset.AssetName + ". Please contact system administrator.";
                        return responce;
                    }
                    var voucher = new VoucherDto();
                    voucher.AccountHeadCode = accHeadCode;
                    voucher.AccTranType = Accounts.Infrastructure.AccTranType.Receive;
                    voucher.Description = "In time of Depreciation of Asset against Accumulated Depreciation.";
                    voucher.Credit = 0;
                    voucher.Debit = (decimal)totaldepreciated;
                    voucher.CompanyProfileId = officeid;
                    voucher.VoucherDate = entity.CalculatedDate;
                    voucherList.Add(voucher);

                    #endregion

                    #region contra voucher entry 
                    if (voucherList.Count > 0)
                    {
                        var CreditVoucher = new VoucherDto();

                        CreditVoucher.AccTranType = Accounts.Infrastructure.AccTranType.Payment;
                        CreditVoucher.Description = "In time of Asset purchase.";
                        CreditVoucher.Credit = voucherList.Sum(c => c.Debit);
                        CreditVoucher.Debit = 0;
                        CreditVoucher.CompanyProfileId = officeid;
                        CreditVoucher.VoucherDate = entity.CalculatedDate;

                        var AccDepreciation = _accounts.GetAccHeadCodeForVoucher(AccountHeadRefType.AccumulatedDepreciation, null);
                        if (string.IsNullOrEmpty(AccDepreciation))
                        {
                            responce.Message = "Accumulated Depreciation, not found. Please contact system Administrator.";
                            return responce;
                        }
                        CreditVoucher.AccountHeadCode = AccDepreciation;
                        voucherList.Add(CreditVoucher);
                    }

                    #endregion


                    using (var tran = new TransactionScope())
                    {
                        try
                        {
                            _service.Save(entity);
                            var data = UpdateLastLog(lastlogid, userId);
                            responce.Success = true;
                            responce.Message = "Depreciation Log have been Saved Successfully. Total Deprecited Amount : " + totaldepreciated;
                            tran.Complete();
                        }
                        catch (Exception ex)
                        {
                            tran.Dispose();
                            responce.Success = false;
                            responce.Message = "Something Wrong!";
                        }    
                    }
                    //_depreciation.SaveDepreciationLog(entity.Id,userId,PointOfDepreciation.OnCreate);
                    if (voucherList.Count > 0)
                    {
                        var genAccountsFacade = new Finix.Accounts.Facade.AccountsFacade();
                        var voucherNumber = "JV-" + _companyProfileFacade.GetUpdateJvNo();
                        voucherList.ForEach(v => v.VoucherNo = voucherNumber);

                        var accountsResponse = genAccountsFacade.SaveVoucher(voucherList, officeid, (long)userId);
                        responce.Id = entity.Id;
                         if (!(bool)accountsResponse.Success)
                        {
                            responce.Message = "Asset Depreciation Failed. " + accountsResponse.Message;
                            return responce;
                        }
                    }

                }
                else
                {
                    responce.Success= false;
                    responce.Message = "There is no possible depreciation!";
                }
             }
            catch (Exception)
            {
                responce.Message = "Depreciation Log Save Failed!";
            }
            
            return responce;
        }
        public DepreciationLog GenerateDepreciationLog(long assetid, long? userId, PointOfDepreciation cause)
        {
            var entity = new DepreciationLog();
            var dto = new DepreciationLogDto();   

            dto = getReadyTheCalculation(assetid, cause);
            entity = Mapper.Map<DepreciationLog>(dto);
            entity.AssetID = assetid;
            entity.Status = EntityStatus.Active;
            entity.CreateDate = DateTime.Now;
            entity.CreatedBy = userId;

            return entity;
        }

        public DepreciationLogDto getReadyTheCalculation(long assetid, PointOfDepreciation cause) //Set the Dto by setting all associate value
        {
            var assetinfo = new AssetDto();
            assetinfo = GetAssetById(assetid);
            
            double depreamount = 0;
            double baseamount = 0;
            double openingvalue = 0;
            double newwdv = 0;
            double rate = 0;
            int yearremaining = 1;
            int sumofyears = 0;
            int usefulyear = 0;
            int daydifference = 0;
            DateTime assetdate = DateTime.Today;
                        
            DepreciationLogDto dto = new DepreciationLogDto();
            if (cause != PointOfDepreciation.OnCreate)
            {
                dto = getLastLog(assetid);
                openingvalue = dto.WrittenDownValue; 
                yearremaining = dto.YearRemaining;
                assetdate = dto.CalculatedDate;
                daydifference = DateTime.Today.Subtract(assetdate).Days;
            }
            else
            {
                dto.AssetID = assetid;
                dto.AssetName = assetinfo.AssetName;
                openingvalue = assetinfo.TotalValue;
                assetdate = assetinfo.PurchaseDate;
                daydifference = 0;
            }
            
            baseamount = assetinfo.TotalValue;
            dto.DepriciationModel = assetinfo.DepriciationModel;
            rate = assetinfo.DepriciationRate;
            usefulyear = assetinfo.UsefulLife;
            // var currentdate = DateTime.Today.AddYears(-1);
            
            //daydifference = DateTime.Today.Subtract(assetdate).Days;
            dto.PointOfDepreciation = cause;
            

            if (dto.DepreciationModelName == "SumOfYears")
            {
                for (int i = 1; i <= usefulyear; i++)
                {
                    sumofyears += i;
                }
                depreamount = (openingvalue * yearremaining) / sumofyears;
                newwdv = openingvalue - depreamount;
                dto.YearRemaining = yearremaining - 1;
            }
            else if (dto.DepreciationModelName == "DecliningBalance")
            {
                if (daydifference > 0)
                    depreamount = (openingvalue * rate * daydifference) / (100 * 365);
                newwdv = openingvalue - depreamount;
            }
            else
            {
                if (daydifference > 0)
                    depreamount = (baseamount * rate * daydifference) / (100 * 365);
                newwdv = openingvalue - depreamount;
            }

            dto.DepreciatedAmount = depreamount;
            dto.WrittenDownValue = newwdv;
            //dto.CalculatedDate = DateTime.Now;
            dto.CalculatedDate = assetinfo.PurchaseDate;
            dto.OpeningValue = openingvalue;
                
         
            return Mapper.Map<DepreciationLogDto>(dto);
        }

        public DepreciationLogDto getLastLog(long assetid)   //Get the Last Depreciation History
        {
            
            var  data = _service.GetAll<DepreciationLog>().Where(x=> x.AssetID == assetid);
            if (data != null)
                data = data.OrderByDescending(x => x.Id);
           
            DepreciationLogDto dto = new DepreciationLogDto();
            var toprow = data.ToList().FirstOrDefault();
            dto= Mapper.Map<DepreciationLogDto>(toprow);
            return dto;
        }

        public ResponseDto SaveSelectedDepreciation(List<long> selectedassets, long userId,PointOfDepreciation cause, long officeid)
        {
            var response = new ResponseDto();
           
                try
                {
                    foreach (long assetid in selectedassets)
                    {
                        SaveDepreciationLog(assetid, userId, PointOfDepreciation.Manual,0,officeid);
                    }
                    response.Success = true;
                    response.Message = "Depreciation Calculation Successful";
                }
                catch (Exception ex)
                {
                    
                    response.Message = "Depreciation Calculation Failed!";
                }
           
            return response;
        }
        public DepreciationLogDto GetDepreciationLogById(long logid)
        {
            var depreciations = _service.GetById<DepreciationLog>(logid);
            return Mapper.Map<DepreciationLogDto>(depreciations);
        }

        public AssetDto GetAssetById(long assetid)
        {
            var asset = _service.GetById<Asset>(assetid);
            return Mapper.Map<AssetDto>(asset);
        }
        public List<Asset_DepreciationLogDto> GetFilturedAssets(CategoryLevel? level, long? catid, double? resval, DateTime fromdate, DateTime enddate, string code)
        {
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (level == null)
                level = CategoryLevel.Primary;
            if (level == CategoryLevel.Primary)
            {
                if (catid != null)
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.Category.ParentID == catid && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.Category.ParentID).ToList();
                else
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            }
            else
            {
                if (catid != null)
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.AssetCategoryID == catid && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.AssetCategoryID).ToList();
                else
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.PointOfDepreciation != PointOfDepreciation.Dispose).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            }
            
            
            var daydifferences = (from a in openinglog
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, enddate))
                                  }).ToList();


            var combinned = from openning in openinglog
                            join daydif in daydifferences
                            on openning.Id equals daydif.AssetID
                            select new Asset_DepreciationLogDto
                            {
                                AssetCategoryID = openning.Asset.Category.Id,
                                CategoryName = openning.Asset.Category.CategoryName,
                                AssetID = openning.AssetID,
                                BaseValue = openning.Asset.TotalValue,
                                AssetName = openning.Asset.AssetName,
                                AssetCode = openning.Asset.AssetCode,
                                PurchaseDate= openning.Asset.PurchaseDate,
                                OpeningValue = openning.OpeningValue,
                                ModelName=UiUtil.GetDisplayName(openning.DepriciationModel),
                                AdditionalValue = 0,
                                RevaluationSurplus = (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0),
                                ClosingValue = (openning.OpeningValue + 0 + (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0)),
                                DepriciationRate = openning.Asset.DepriciationRate,
                                DepreciatedAmount = openning.DepreciatedAmount,
                                AdjustedDepreciation = (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0),
                                ClosingDepreciation = (openning.DepreciatedAmount + (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0)),
                                WrittenDownValue = ((openning.OpeningValue + 0 + (openning.PointOfDepreciation == PointOfDepreciation.Revaluation ? openning.RevaluationSurplus : 0)) - (openning.DepreciatedAmount + (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0)))

                            };
            List<Asset_DepreciationLogDto> ned = new List<Asset_DepreciationLogDto>();
            ned = Mapper.Map<List<Asset_DepreciationLogDto>>(combinned);
            ned = ned.Where(x => fromdate <= x.PurchaseDate && x.PurchaseDate <= enddate).ToList();
            if (code != "")
                ned = ned.Where(x => x.AssetCode==code).ToList();
            if (resval != null)
                ned = ned.Where(x => x.WrittenDownValue <=resval).ToList();
            return ned;
  
        }
        public IEnumerable<CategoryDto> GetCategoryList()
        {
            var categorylist = _service.GetAll<Category>().Where(s => s.Status == EntityStatus.Active && s.ParentID != null && s.ParentID==1 ).ToList();

            var categoryDtoList = (from category in categorylist

                                   select new CategoryDto
                                   {
                                       Id = category.Id,
                                       Code = category.Code,
                                       CategoryName = category.CategoryName,
                                       ParentId = category.ParentID,
                                       Details = category.Details,
                                       ParentName = "Primary"
                                   }
                                    );
            return categoryDtoList;

        }
        
        public int Days(DateTime fromdate, DateTime enddate)
        {
            return enddate.Subtract(fromdate).Days;
        }

        public List<CategorySummaryDto> CalculateCategrorySummaryNew(long? catid, DateTime fromdate, DateTime enddate)
        {
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (catid != null)
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.AssetCategoryID == catid).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            else
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active).OrderBy(x => x.Asset.AssetCategoryID).ToList();


            var daydifferences = (from a in openinglog
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, enddate))
                                  }).ToList();
            var combinned = from _cat in (from openning in openinglog
                                          join daydif in daydifferences
                                          on openning.Id equals daydif.AssetID
                                          select new
                                          {
                                              CatID = openning.Asset.AssetCategoryID,
                                              CategoryName = openning.Asset.Category.CategoryName,
                                              OpeningValue = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue),
                                              AdditionalValue = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0),
                                              RevaluationSurplus = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0),
                                              DisposedValue = openning.DisposedValue,
                                              ClosingValue = (((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue),
                                              Rate = openning.Asset.DepriciationRate,
                                              OpeningDepreciation = openning.DepreciatedAmount,
                                              AdjustedDepreciation = (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0),
                                              ClosingDepreciation = (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)),
                                              WrittenDownValue = ((((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue) - (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)))
                                          })
                                                    group _cat by new { _cat.CatID, _cat.CategoryName } into categories
                                                    select new CategorySummaryDto
                                                    {
                                                        CategoryID = categories.Key.CatID,
                                                        CategoryName = categories.Key.CategoryName,
                                                        OpeningValue = categories.Sum(a => a.OpeningValue),
                                                        AdditionalValue = categories.Sum(a => a.AdditionalValue),
                                                        RevaluationSurplus = categories.Sum(a => a.RevaluationSurplus),
                                                        DisposedValue = categories.Sum(a => a.DisposedValue),
                                                        ClosingValue = categories.Sum(a => a.ClosingValue),
                                                        Rate = categories.Average(a => a.Rate),
                                                        OpeningDepreciation = categories.Sum(a => a.OpeningDepreciation),
                                                        AdjustedDepreciation = categories.Sum(a => a.AdjustedDepreciation),
                                                        ClosingDepreciation = categories.Sum(a => a.ClosingDepreciation),
                                                        WrittenDownValue = categories.Sum(a => a.WrittenDownValue)
                                                    };
            return Mapper.Map<List<CategorySummaryDto>>(combinned);
        }
        public List<CategorySummaryDto> CalculatePrimaryCategrorySummary(long? catid, DateTime fromdate, DateTime enddate)
        {
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
                if (catid != null)
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.Category.ParentID == catid).OrderBy(x => x.Asset.Category.ParentID).ToList();
                else
                    openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active).OrderBy(x => x.Asset.Category.ParentID).ToList();
          
               var daydifferences = (from a in openinglog
                                 select new
                                 {
                                  AssetID= a.Id,
                                  Days= (Days(a.CalculatedDate, enddate))
                                 }).ToList();
            List<Category> cats = new List<Category>();
            cats = _service.GetAll<Category>().Where(x => x.ParentID > 0 && x.Level==CategoryLevel.Secondary).ToList();
            
            var combinned = from _final in (from _cat in (from openning in openinglog
                                           join daydif in daydifferences
                                           on openning.Id equals daydif.AssetID
                                           select new
                                           {
                                               CatID =                openning.Asset.AssetCategoryID,
                                               CategoryName =         openning.Asset.Category.CategoryName,
                                               OpeningValue =         ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue),
                                               AdditionalValue =      ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0),
                                               RevaluationSurplus =   ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0),
                                               DisposedValue =        openning.DisposedValue,
                                               ClosingValue =         (((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue),
                                               Rate =                 openning.Asset.DepriciationRate,
                                               OpeningDepreciation =  openning.DepreciatedAmount,
                                               AdjustedDepreciation = (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0),
                                               ClosingDepreciation = (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)),
                                               WrittenDownValue =    ((((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue) - (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)))
                                           })
                             group _cat by new { _cat.CatID, _cat.CategoryName } into categories
                             join  _catinfo in cats
                             on categories.Key.CatID equals _catinfo.Id
                             select new CategorySummaryDto
                             {
                                 CategoryID =           categories.Key.CatID,
                                 CategoryName =         categories.Key.CategoryName,
                                 CategoryParentID =     (long)_catinfo.ParentID,
                                 CategoryParentName =   _catinfo.Categories.CategoryName,
                                 OpeningValue =         categories.Sum(a => a.OpeningValue),
                                 AdditionalValue =      categories.Sum(a => a.AdditionalValue),
                                 RevaluationSurplus =   categories.Sum(a => a.RevaluationSurplus),
                                 DisposedValue =        categories.Sum(a => a.DisposedValue),
                                 ClosingValue =         categories.Sum(a => a.ClosingValue),
                                 Rate =                 categories.Average(a => a.Rate),
                                 OpeningDepreciation =  categories.Sum(a => a.OpeningDepreciation),
                                 AdjustedDepreciation = categories.Sum(a => a.AdjustedDepreciation),
                                 ClosingDepreciation =  categories.Sum(a => a.ClosingDepreciation),
                                 WrittenDownValue =     categories.Sum(a => a.WrittenDownValue)
                             })
                                                      group _final by new { _final.CategoryParentID, _final.CategoryParentName } into primarycat
                                                      select new CategorySummaryDto
                                                      {
                                                          CategoryID =           primarycat.Key.CategoryParentID,
                                                          CategoryName =         primarycat.Key.CategoryParentName,
                                                          OpeningValue =         primarycat.Sum(a => a.OpeningValue),
                                                          AdditionalValue =      primarycat.Sum(a => a.AdditionalValue),
                                                          RevaluationSurplus =   primarycat.Sum(a => a.RevaluationSurplus),
                                                          DisposedValue =        primarycat.Sum(a => a.DisposedValue),
                                                          ClosingValue =         primarycat.Sum(a => a.ClosingValue),
                                                          Rate =                 primarycat.Average(a => a.Rate),
                                                          OpeningDepreciation =  primarycat.Sum(a => a.OpeningDepreciation),
                                                          AdjustedDepreciation = primarycat.Sum(a => a.AdjustedDepreciation),
                                                          ClosingDepreciation =  primarycat.Sum(a => a.ClosingDepreciation),
                                                          WrittenDownValue =     primarycat.Sum(a => a.WrittenDownValue)
                                                      };

            return Mapper.Map<List<CategorySummaryDto>>(combinned);
        }

        public List<Asset_DepreciationLogDto> DetailsGroupSummary(long? catid, DateTime fromdate, DateTime enddate)
        {
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (catid != null)
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.AssetCategoryID == catid).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            else
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            //from _cat in (
            var daydifferences = (from a in openinglog
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, enddate))
                                  }).ToList();


            var combinned = from openning in openinglog
                                          join daydif in daydifferences
                                          on openning.Id equals daydif.AssetID
                                          select new Asset_DepreciationLogDto
                                          {
                                              AssetCategoryID = openning.Asset.Category.Id,
                                              CategoryName = openning.Asset.Category.CategoryName,
                                              AssetID=openning.AssetID,
                                              BaseValue=openning.Asset.TotalValue,
                                              AssetName=openning.Asset.AssetName,
                                              AssetCode=openning.Asset.AssetCode,
                                              OpeningValue = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue),
                                              AdditionalValue = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0),
                                              DisposedValue= openning.DisposedValue,
                                              RevaluationSurplus = ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0),
                                              ClosingValue = (((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue),
                                              DepriciationRate = openning.Asset.DepriciationRate,
                                              DepreciatedAmount =    openning.DepreciatedAmount,
                                              AdjustedDepreciation = (openning.PointOfDepreciation!=PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0):0) ,
                                              ClosingDepreciation =  (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)),
                                              WrittenDownValue =     ((((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0)- openning.DisposedValue) - (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)))
                                              
                                          };
            List<Asset_DepreciationLogDto> ned = new List<Asset_DepreciationLogDto>();
            ned = Mapper.Map<List<Asset_DepreciationLogDto>>(combinned);
            return ned;
        }

        public List<AssetStockDto> CalculateCategroryStock(CategoryLevel? level, long? catid, DateTime fromdate, DateTime enddate)
        {
            if (level == null)
                level = CategoryLevel.Primary;
            List<DepreciationLog> openinglog = new List<DepreciationLog>();
            if (catid != null)
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active && x.Asset.AssetCategoryID == catid).OrderBy(x => x.Asset.AssetCategoryID).ToList();
            else
                openinglog = _service.GetAll<DepreciationLog>().Where(x => x.Status == EntityStatus.Active).OrderBy(x => x.Asset.AssetCategoryID).ToList();

            

            var daydifferences = (from a in openinglog
                                  select new
                                  {
                                      AssetID = a.Id,
                                      Days = (Days(a.CalculatedDate, enddate))
                                  }).ToList();
            if (level == CategoryLevel.Primary)
            {
                List<Category> cats = new List<Category>();
                cats = _service.GetAll<Category>().Where(x => x.ParentID > 0 && x.Level == CategoryLevel.Secondary).ToList();
                var final = from _final in (from _cat in
                                 (from openning in openinglog
                                  join daydif in daydifferences
                                  on openning.Id equals daydif.AssetID
                                  select new
                                  {
                                      CatID = openning.Asset.AssetCategoryID,
                                      CategoryName = openning.Asset.Category.CategoryName,
                                      AssetCount = (openning.PointOfDepreciation == PointOfDepreciation.Dispose ? 0 : 1),
                                      WrittenDownValue = ((((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue) - (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)))
                                  })
                            group _cat by new { _cat.CatID, _cat.CategoryName } into categories
                            join _catinfo in cats
                            on categories.Key.CatID equals _catinfo.Id
                            select new AssetStockDto
                            {
                                CategoryID = categories.Key.CatID,
                                CategoryName = categories.Key.CategoryName,
                                ParentID = (long)_catinfo.ParentID,
                                ParentName = _catinfo.Categories.CategoryName,
                                AssetCount = categories.Sum(a => a.AssetCount),
                                TotalWDV = categories.Sum(a => a.WrittenDownValue)
                            })
                            group _final by new { _final.ParentID, _final.ParentName } into primarycat
                            select new AssetStockDto
                            {
                                CategoryID =   primarycat.Key.ParentID,
                                CategoryName = primarycat.Key.ParentName,
                                AssetCount =   primarycat.Sum(a => a.AssetCount),
                                TotalWDV =     primarycat.Sum(a => a.TotalWDV)
                            };
                return Mapper.Map<List<AssetStockDto>>(final);
            }
            else
            {
                var combinned = from _cat in
                                (from openning in openinglog
                                 join daydif in daydifferences
                                 on openning.Id equals daydif.AssetID
                                 select new
                                 {
                                     CatID = openning.Asset.AssetCategoryID,
                                     CategoryName = openning.Asset.Category.CategoryName,
                                     AssetCount = (openning.PointOfDepreciation == PointOfDepreciation.Dispose ? 0 : 1),
                                     WrittenDownValue = ((((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? 0 : openning.OpeningValue) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.OnCreate) ? (openning.WrittenDownValue) : 0) + ((fromdate <= openning.CalculatedDate && openning.CalculatedDate <= enddate && openning.PointOfDepreciation == PointOfDepreciation.Revaluation) ? openning.RevaluationSurplus : 0) - openning.DisposedValue) - (openning.DepreciatedAmount + (openning.PointOfDepreciation != PointOfDepreciation.Dispose ? (openning.CalculatedDate < enddate ? ((daydif.Days * openning.Asset.DepriciationRate * (openning.DepriciationModel == DepriciationModel.DecliningBalance ? openning.WrittenDownValue : openning.OpeningValue)) / (100 * 365)) : 0) : 0)))
                                 })
                                group _cat by new { _cat.CatID, _cat.CategoryName } into categories
                                select new AssetStockDto
                                {
                                    CategoryID = categories.Key.CatID,
                                    CategoryName = categories.Key.CategoryName,
                                    AssetCount = categories.Sum(a => a.AssetCount),
                                    TotalWDV = categories.Sum(a => a.WrittenDownValue)
                                };
                return Mapper.Map<List<AssetStockDto>>(combinned);
            } 
 
        }
        public ResponseDto UpdateLastLog(long logid, long? userId)
        {
            var entity = new DepreciationLog();
            ResponseDto responce = new ResponseDto();

            try
            {
                if (logid != null && logid > 0)
                {
                    entity = _service.GetById<DepreciationLog>((long)logid);
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Inactive;
                    _service.Save(entity);
                    responce.Success = true;
                    responce.Message = "Last Log Updated Successfully";
                }

            }
            catch (Exception ex)
            {
                responce.Message = "Failed";
            }

            return responce;
        }
      
    }
}
