﻿using AutoMapper;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using Finix.Auth.DTO;
using System.Transactions;
using PagedList;

namespace Finix.FAMS.Facade
{

    public class AssetTransferFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();
        private readonly EnumFacade _enams = new EnumFacade();

        public List<EmployeeDto> getSourceEmployee()
        {
            var employees = _service.GetAll<Employee>();
            return Mapper.Map<List<EmployeeDto>>(employees);
        }
        public List<EmployeeDto> getDestinatioEmployee(long exceptid)
        {
            var employees = _service.GetAll<Employee>().Where(x => x.Id != exceptid);
            return Mapper.Map<List<EmployeeDto>>(employees);
        }
        public List<LocationDto> getSources(LocationTier level, long? parent)
        {
            List<Location> sources = new List<Location>();
            if (parent != null)
                sources = _service.GetAll<Location>().Where(x => x.Level == level && x.ParentID == parent).ToList();
            else
                sources = _service.GetAll<Location>().Where(x => x.Level == level).ToList();
            return Mapper.Map<List<LocationDto>>(sources);
        }
        public List<LocationDto> getDetinations(LocationTier level, long? parent, long? exceptid)
        {
            List<Location> destinations = new List<Location>();
            if (parent != null)
            {
                if (exceptid != null)
                    destinations = _service.GetAll<Location>().Where(x => x.Level == level && x.ParentID == parent && x.Id != exceptid).ToList();
                else
                    destinations = _service.GetAll<Location>().Where(x => x.Level == level && x.ParentID == parent).ToList();
            }

            else
            {
                if (exceptid != null)
                    destinations = _service.GetAll<Location>().Where(x => x.Level == level && x.Id != exceptid).ToList();
                else
                    destinations = _service.GetAll<Location>().Where(x => x.Level == level).ToList();
            }

            return Mapper.Map<List<LocationDto>>(destinations);
        }
        public List<Location> getLocations(long? id)
        {
            List<Location> List = new List<Location>();
            var parent = _service.GetById<Location>((long)id);
            List.Add(parent);
            var childrens = _service.GetAll<Location>().Where(x => x.ParentID == parent.Id);
            foreach (var child in childrens)
            {
                var sub = getLocations(child.Id);
                if (sub != null)
                    List.AddRange(sub);
            }
            return List;
        }
        public List<AssetAcquisitionDto> getAssetsByID(TransferType type, long? byid)
        {
            List<AssetAcquisition> Assets = new List<AssetAcquisition>();
            if (type == TransferType.Employee)
            {
                Assets = _service.GetAll<AssetAcquisition>().Where(x => (x.CustodianID == byid)).ToList();
            }
            else
            {
                List<Location> locations = getLocations(byid);
                foreach (var loc in locations)
                {
                    var asset = _service.GetAll<AssetAcquisition>().Where(x => (type == TransferType.Employee ? x.CustodianID == loc.Id : x.LocationID == loc.Id));
                    Assets.AddRange(asset);
                }
            }
            return Mapper.Map<List<AssetAcquisitionDto>>(Assets);
        }
        public ResponseDto SaveTransferLog(TransferLogDto dto, long? userId, long assetid)
        {
            var entity = new TransferLog();
            ResponseDto responce = new ResponseDto();
            entity = Mapper.Map<TransferLog>(dto);
            if (entity.TransferType == TransferType.Employee)
            {
                entity.SourceEmployeeID = dto.SourceEmployeeID;
                entity.DestinationEmployeeID = dto.DestinationEmployeeID;
            }
            else
            {
                entity.SourceID = dto.SourceID;
                entity.DestinationID = dto.DestinationID;
            }
            entity.Status = EntityStatus.Active;
            entity.CreateDate = DateTime.Now;
            entity.TransferDate = DateTime.Now;
            if (entity.AssetID <= 0)
                entity.AssetID = assetid;
            entity.CreatedBy = userId;
            using (var tran = new TransactionScope())
            {
                try
                {
                    _service.Save(entity);
                    var assetupdate = UpdateAsset(assetid, userId, entity.DestinationEmployeeID, entity.DestinationID);
                    tran.Complete();
                    responce.Success = true;
                    responce.Message = "Trasfer Log have been Saved Successfully";
                }
                catch (Exception ex)
                {
                    responce.Success = false;
                    responce.Message = "Failed!!";
                }
            }
            return responce;
        }
        public ResponseDto TransferSelected(TransferLogDto dto, List<long> selectedassets, long? userId)
        {
            var response = new ResponseDto();
            try
            {
                foreach (long assetid in selectedassets)
                {
                    SaveTransferLog(dto, userId, assetid);
                }
                response.Success = true;
                response.Message = "Asset Transfer Successful";
            }
            catch (Exception ex)
            {

                response.Message = "Asset Transfer Failed!";
            }
            return response;
        }
        public ResponseDto UpdateAsset(long assetid, long? userId, long? desteid, long? destoid)
        {
            AssetAcquisition entity = new AssetAcquisition();
            ResponseDto responce = new ResponseDto();
            try
            {
                if (assetid != null && assetid > 0)
                {
                    entity = new AssetAcquisition();
                    entity = GenService.GetAll<AssetAcquisition>().Where(x => x.AssetID == assetid).FirstOrDefault();
                    entity = GenService.GetById<AssetAcquisition>((long)entity.Id);
                    if (desteid != null)
                        entity.CustodianID = desteid;
                    if (destoid != null)
                        entity.LocationID = destoid;
                    entity.EditDate = DateTime.Now;
                    entity.EditedBy = userId;
                    entity.Status = EntityStatus.Active;
                    GenService.Save(entity);
                    responce.Success = true;
                    responce.Message = "Asset Acquisition Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                responce.Message = "Asset Save Failed";
            }
            return responce;
        }

        public IPagedList<AssetAcquisitionDto> TransferlogSourcePagedList(int pageSize, int pageCount, string searchString, long? byid) //List<AssetAcquisitionDto>
        {
            List<AssetAcquisition> Assets = new List<AssetAcquisition>();

            List<Location> locations = getLocations(byid);
            foreach (var loc in locations)
            {
                var asset = _service.GetAll<AssetAcquisition>().Where(x => x.LocationID == loc.Id);
                Assets.AddRange(asset);
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                Assets = Assets
                    .Where(s => s.Asset.AssetName.ToLower().Contains(searchString) || s.Asset.AssetCode.ToLower().Contains(searchString)).ToList();
            }
            var temp = Mapper.Map<List<AssetAcquisitionDto>>(Assets.OrderBy(r => r.Id)).ToPagedList(pageCount, pageSize);

            return temp;//Mapper.Map<List<AssetAcquisitionDto>>(temp);
        }

        public IPagedList<AssetAcquisitionDto> TransferlogDestinationPagedList(int pageSize, int pageCount, string searchString, long? byid) //List<AssetAcquisitionDto>
        {
            List<AssetAcquisition> Assets = new List<AssetAcquisition>();

            List<Location> locations = getLocations(byid);
            foreach (var loc in locations)
            {
                var asset = _service.GetAll<AssetAcquisition>().Where(x => x.LocationID == loc.Id);
                Assets.AddRange(asset);
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                Assets = Assets
                    .Where(s => s.Asset.AssetName.ToLower().Contains(searchString) || s.Asset.AssetCode.ToLower().Contains(searchString)).ToList();
            }
            var temp = Mapper.Map<List<AssetAcquisitionDto>>(Assets.OrderBy(r => r.Id)).ToPagedList(pageCount, pageSize);
            return temp;//Mapper.Map<List<AssetAcquisitionDto>>(temp);
        }

    }


}
