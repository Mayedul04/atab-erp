﻿using AutoMapper;
using Finix.FAMS.Dto;
using Finix.FAMS.Infrastructure;
using Finix.FAMS.Infrastructure.Models;
using Finix.FAMS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Facade
{
   public class TransferReportFacade
    {
         private readonly GenService _service = new GenService();

        public List<Location> getLocations(long? id)
        {
            List<Location> List = new List<Location>();
            var parent = _service.GetById<Location>((long)id);
            List.Add(parent);
            var childrens = _service.GetAll<Location>().Where(x => x.ParentID == parent.Id);
            foreach (var child in childrens)
            {
                var sub = getLocations(child.Id);
                if (sub != null)
                    List.AddRange(sub);
            }
            return List;
        }

        public Location getParent(long? id)
        {
            var data = _service.GetById<Location>((long)id);
            Location parent= new Location();
            if (data.ParentID != null)
                parent = _service.GetById<Location>((long)data.ParentID);
            else parent = null;
            return parent;
        }
        
        public List<TransferLogDto> GetTransferLogs(DateTime fromDate, DateTime toDate, long? srcid, long? destid)
        {
            string sourcedetails = "";
            string destinationdetails = "";
            fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
            toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);
            List<TransferLog> log = new List<TransferLog>();
            List<Location> srclocations = new List<Location>();
            List<Location> destlocations = new List<Location>();
            List<TransferLog> assets = new List<TransferLog>();
            List<TransferLog> finallog = new List<TransferLog>();
            if (srcid != null)
            {
                srclocations = getLocations(srcid);
                Location Parent = getParent(srcid);
                while (Parent.Level!=LocationTier.Company)
                {
                    sourcedetails = Parent.LocationTitle + ", " + sourcedetails;
                    Parent = getParent(Parent.Id);
                }
                sourcedetails += _service.GetById<Location>((long)srcid).LocationTitle;
                foreach (var loc in srclocations)
                {
                    var asset = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.SourceID == loc.Id && p.TransferType != TransferType.Employee && fromDate < p.TransferDate && p.TransferDate < toDate);
                    assets.AddRange(asset);
                }
            }
            else
            {
                assets = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.TransferType != TransferType.Employee && fromDate < p.TransferDate && p.TransferDate < toDate).ToList();
            }
            if (destid != null)
            {
                destlocations = getLocations(destid);
                Location DestParent = getParent(destid);
                while (DestParent.Level != LocationTier.Company)
                {
                    destinationdetails = DestParent.LocationTitle + ", " + destinationdetails;
                    DestParent = getParent(DestParent.Id);
                }
                destinationdetails += _service.GetById<Location>((long)destid).LocationTitle;
                foreach (var loc in destlocations)
                {
                    if (assets.Count > 0)
                        log = assets.Where(p => p.DestinationID == loc.Id).ToList();
                    else
                        log = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.DestinationID == loc.Id).ToList();
                    finallog.AddRange(log);
                }
            }
            else
            {
                if (assets.Count > 0)
                    finallog.AddRange(assets);
            }
            sourcedetails = sourcedetails.TrimEnd(',');
            destinationdetails = destinationdetails.TrimEnd(',');
            
            List<Category> category = new List<Category>();
            category = _service.GetAll<Category>().ToList();
            var result = from asset in finallog
                         join cat in category
                         on asset.Asset.Category.ParentID equals cat.Id
                         select new TransferLogDto
                         {
                             AssetID = asset.AssetID,
                             AssetCode = asset.Asset.AssetCode,
                             AssetName = asset.Asset.AssetName,
                             TransferTypeName = Util.UiUtil.GetDisplayName(asset.TransferType),
                             SourceID = asset.SourceID,
                             SourceTitle = asset.SourceLocation.LocationTitle,
                             DestinationID = asset.DestinationID,
                             DestinationTitle = asset.Destination.LocationTitle,
                             CategoryId = asset.Asset.AssetCategoryID,
                             CategoryName = asset.Asset.Category.CategoryName,
                             ParentId = cat.Id,
                             ParentName = cat.CategoryName,
                             TransferDate = asset.TransferDate,
                             TransferDetails = asset.TransferDetails,
                             SourceSpecificLocation = asset.SourceSpecificLocation,
                             DestinationSpecificLocation = asset.DestinationSpecificLocation,
                             SourceDetails = sourcedetails,
                             DestinationDetails=destinationdetails
                         };

            return Mapper.Map<List<TransferLogDto>>(result);
        }

        public List<TransferSummaryDto> GetTransferSummary(DateTime fromDate, DateTime toDate, long? srcid, long? destid)
        {
            string sourcedetails = "";
            string destinationdetails = "";
            fromDate = fromDate <= Convert.ToDateTime("01/01/1753") ? Convert.ToDateTime("01/01/1753") : fromDate;
            toDate = toDate >= DateTime.MaxValue ? DateTime.MaxValue : toDate.AddDays(1);
            List<TransferLog> log = new List<TransferLog>();
            List<Location> srclocations = new List<Location>();
            List<Location> destlocations = new List<Location>();
            List<TransferLog> assets = new List<TransferLog>();
            List<TransferLog> finallog = new List<TransferLog>();
            if (srcid != null)
            {
                srclocations = getLocations(srcid);
                Location Parent = getParent(srcid);
                while (Parent.Level != LocationTier.Company)
                {
                    sourcedetails = Parent.LocationTitle + ", " + sourcedetails;
                    Parent = getParent(Parent.Id);
                }
                sourcedetails += _service.GetById<Location>((long)srcid).LocationTitle;
                foreach (var loc in srclocations)
                {
                    
                    var asset = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.SourceID == loc.Id && p.TransferType != TransferType.Employee && fromDate < p.TransferDate && p.TransferDate < toDate);
                    assets.AddRange(asset);
                }
            }
            else
            {
                assets= _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.TransferType != TransferType.Employee && fromDate < p.TransferDate && p.TransferDate < toDate).ToList();
            }

            if (destid != null)
            {
                destlocations = getLocations(destid);
                Location DestParent = getParent(destid);
                while (DestParent.Level != LocationTier.Company)
                {
                    destinationdetails = DestParent.LocationTitle + ", " + destinationdetails;
                    DestParent = getParent(DestParent.Id);
                }
                destinationdetails += _service.GetById<Location>((long)destid).LocationTitle;
                foreach (var loc in destlocations)
                {
                    if (assets.Count > 0)
                        log = assets.Where(p => p.DestinationID == loc.Id).ToList();
                    else
                        log = _service.GetAll<TransferLog>().Where(p => p.Status == EntityStatus.Active && p.DestinationID == loc.Id).ToList();
                    finallog.AddRange(log);
                }
            }
            else
            {
                if (assets.Count > 0)
                    finallog.AddRange(assets);
            }
            sourcedetails = sourcedetails.TrimEnd(',');
            destinationdetails = destinationdetails.TrimEnd(',');

            
            List<Category> category = new List<Category>();
            category = _service.GetAll<Category>().ToList();
            var result = (from asset in finallog
                         select new TransferLogDto
                         {
                             AssetID = asset.AssetID,
                             AssetCode = asset.Asset.AssetCode,
                             AssetName = asset.Asset.AssetName,
                             TransferTypeName = Util.UiUtil.GetDisplayName(asset.TransferType),
                             SourceID = asset.SourceID,
                             SourceTitle = asset.SourceLocation.LocationTitle,
                             DestinationID = asset.DestinationID,
                             DestinationTitle = asset.Destination.LocationTitle,
                             CategoryId = asset.Asset.AssetCategoryID,
                             CategoryName = asset.Asset.Category.CategoryName,
                             TransferDate = asset.TransferDate,
                             TransferDetails = asset.TransferDetails,
                             SourceSpecificLocation = asset.SourceSpecificLocation,
                             DestinationSpecificLocation = asset.DestinationSpecificLocation,
                             SourceDetails = sourcedetails,
                             DestinationDetails = destinationdetails
                         }).ToList();
            var summary = from res in result
                          group res by new { res.CategoryId, res.CategoryName } into groupedsum
                          join cat in category
                          on groupedsum.Key.CategoryId equals cat.Id
                          select new TransferSummaryDto
                          {
                              ParentCategoryID=(long)cat.ParentID,
                              ParentCategoryName= GetCategorybyID(cat.ParentID).CategoryName,
                              CategoryID = (long)groupedsum.Key.CategoryId,
                              CategoryName=groupedsum.Key.CategoryName,
                              SourceDetails=sourcedetails,
                              DestinationDetails=destinationdetails,
                              AssetCount=groupedsum.Count()
                          };

            return Mapper.Map<List<TransferSummaryDto>>(summary);
        }

        public CategoryDto GetCategorybyID(long? id)
        {
            Category data = new Category();
            if (id != null)
                data = _service.GetById<Category>((long)id);

            return Mapper.Map<CategoryDto>(data);
        }
    }
}
