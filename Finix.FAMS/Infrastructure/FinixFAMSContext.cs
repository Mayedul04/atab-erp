﻿using Finix.FAMS.Infrastructure;
using System;
using System.IO;
using System.Linq;

using Finix.FAMS.Util;
using System.Data.Entity;
using Finix.FAMS.Infrastructure.Models;

namespace Finix.FAMS.Infrastructure
{
    public interface IFinixFAMSContext
    {
    }
    public partial class FinixFAMSContext : DbContext, IFinixFAMSContext
    {
        #region ctor
        static FinixFAMSContext()
        {
            Database.SetInitializer<FinixFAMSContext>(null);
        }

        public FinixFAMSContext()
            : base("Name=FinixFAMSContext")
        {
            Database.SetInitializer(new FinixFAMSContextInitializer());
        }
        #endregion

        #region Models

        public DbSet<Category> Category { get; set; }
        public DbSet<AccountsMapping> AccountsMapping { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<AssetAcquisition> AssetAcquisition { get; set; }
        public DbSet<DepreciationLog> DepreciationLog { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<TransferLog> TransferLog { get; set; }
        public DbSet<DisposeLog> DisposeLog { get; set; }
        public DbSet<DesignationRoleMapping> DesignationRoleMappings { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeDesignationMapping> EmployeeDesignationMapping { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<OfficeDesignationArea> OfficeDesignationAreas { get; set; }
        public DbSet<OfficeDesignationSetting> OfficeDesignationSettings { get; set; }
        public DbSet<OfficeLayer> OfficeLayers { get; set; }
        public DbSet<Thana> Thanas { get; set; }
        public DbSet<Upazila> Upazilas { get; set; }
        public DbSet<AccGroupRefTypeMapping> AccGroupRefTypeMapping { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            FluentConfigurator.ConfigureGenericSettings(modelBuilder);
            FluentConfigurator.ConfigureMany2ManyMappings(modelBuilder);
            FluentConfigurator.ConfigureOne2OneMappings(modelBuilder);
        }
    }

    internal class FinixFAMSContextInitializer : CreateDatabaseIfNotExists<FinixFAMSContext> //CreateDatabaseIfNotExists<CardiacCareContext>
    {
        protected override void Seed(FinixFAMSContext context)
        {
            var seedDataPath = FAMSystem.SeedDataPath;
            var dropDb = FAMSystem.DropDB;
            if (string.IsNullOrWhiteSpace(seedDataPath))
                return;
            var folders = Directory.GetDirectories(seedDataPath).ToList().OrderBy(x => x);
            var msg = "";
            bool error = false;
            try
            {
                foreach (var folder in folders)
                {
                    msg += string.Format("processing for: {0}{1}", folder, Environment.NewLine);

                    var fileDir = Path.Combine(new[] { seedDataPath, folder });
                    var sqlFiles = Directory.GetFiles(fileDir, "*.sql").OrderBy(x => x).ToList();
                    foreach (var file in sqlFiles)
                    {
                        try
                        {
                            msg += string.Format("processing for: {0}{1}", file, Environment.NewLine);
                            context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                            msg += string.Format("Done....{0}", Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            error = true;
                            msg += string.Format("Failed!....{0}", Environment.NewLine);
                            msg += string.Format("{0}{1}", ex.Message, Environment.NewLine);
                        }
                    }
                }
                if (error)
                {
                    throw new Exception(msg);
                }
                base.Seed(context);
            }
            catch (Exception ex)
            {
                msg = "Error Occured while inserting seed data" + Environment.NewLine;
                if (dropDb)
                {
                    context.Database.Delete();
                    msg += ("Database is droped" + Environment.NewLine);
                }
                var errFile = seedDataPath + "\\seed_data_error.txt";
                msg += ex.Message;
                File.WriteAllText(errFile, msg);

            }
        }

    }
}
