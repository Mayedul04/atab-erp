﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
   public class TransferLog : Entity
    {
        public TransferType TransferType { get; set; }
        public long? SourceEmployeeID { get; set; }
        [ForeignKey("SourceEmployeeID")]
        public virtual Employee SourceEmployee { get; set; }
        public long? DestinationEmployeeID { get; set; }
        [ForeignKey("DestinationEmployeeID")]
        public virtual Employee DestinationEmployee { get; set; }
        public long? SourceID { get; set; }
        [ForeignKey("SourceID")]
        public virtual Location SourceLocation { get; set; }
        public long? DestinationID { get; set; }
        [ForeignKey("DestinationID")]
        public virtual Location Destination { get; set; }
        public long AssetID { get; set; }
        [ForeignKey("AssetID")]
        public virtual Asset Asset { get; set; }
        public string SourceSpecificLocation { get; set; }
        public string DestinationSpecificLocation { get; set; }
        public DateTime TransferDate { get; set; }
        public string TransferDetails { get; set; }

    }
}
