﻿using Finix.Auth.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
   public class Asset : Entity
    {
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public long? SupplierID { get; set; }
        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }
        public long AssetCategoryID { get; set; }
        [ForeignKey("AssetCategoryID")]
        public virtual Category Category { get; set; }
        //public int DepreciationModel { get; set; }
        public DepriciationModel DepriciationModel { get; set; }
        public double DepriciationRate { get; set; }
        public int UsefulLife { get; set; }
        public string Location { get; set; }
        public double OpeningDepreciation { get; set; }
        public string VoucherCode { get; set; }
        public string Description { get; set; }
        public double BaseValue { get; set; }
        public double Tax { get; set; }
        public double VAT { get; set; }
        public double TotalValue { get; set; }
        public DateTime PurchaseDate { get; set; }
        //public int ServiceType { get; set; }
        public ServiceType ServiceTypes { get; set; }
        public int ServicePeriod { get; set; }

       
    }
}
