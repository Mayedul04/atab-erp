﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
  public class AccGroupRefTypeMapping :Entity
    {
        public AccountHeadRefType RefType { get; set; }
        public string AccCode { get; set; }
    }
}
