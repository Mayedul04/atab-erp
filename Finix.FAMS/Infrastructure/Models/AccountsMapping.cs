﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finix.FAMS.Infrastructure.Models
{
   public class AccountsMapping : Entity
    {
        public long? RefId { get; set; }
        public AccountHeadRefType RefType { get; set; }
        public string AccountHeadCode { get; set; }
    
    }
}
