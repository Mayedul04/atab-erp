﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
  public  class DisposeLog :Entity
    {
        public long AssetID { get; set; }
        [ForeignKey("AssetID")]
        public virtual Asset Asset { get; set; }
        public double AccumulatedDepreciation { get; set; }
        public double BaseValue { get; set; }
        public double ResaleValue { get; set; }
        public string ResaleTo { get; set; }
        public double GainOfDisposal { get; set; }
        public double LossOfDisposal { get; set; }
        public DateTime DisposalDate { get; set; }
    }
}
