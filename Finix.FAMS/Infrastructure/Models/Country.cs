﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
    [Table("Country")]
    public class Country : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
