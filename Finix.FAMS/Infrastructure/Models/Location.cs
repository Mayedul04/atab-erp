﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
   public class Location :Entity
    {
        public long? OfficeId { get; set; }
        public string LocationTitle { get; set; }
        public string Details { get; set; }
        public long? ParentID { get; set; }
        [ForeignKey("ParentID")]
        public virtual Location Locations { get; set; }
        public LocationTier Level { get; set; }

    }
}
