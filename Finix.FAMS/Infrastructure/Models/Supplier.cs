﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
    [Table("Supplier")]
    public class Supplier : Entity
    {
        public Supplier()
        {
           // Products = new List<Product>();
            //Orders = new List<PurchaseOrder>();
        }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string BankAccountNo { get; set; }
        public string PayTerms { get; set; }
        public long? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
        public bool IsActive { get; set; }        
       

    }
}
