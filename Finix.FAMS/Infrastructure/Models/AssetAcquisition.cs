﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
   public class AssetAcquisition : Entity
    {
       
        public long AssetID { get; set; }
        [ForeignKey("AssetID")]
        public virtual Asset Asset { get; set; }
        public long? CustodianID { get; set; }
        public long? LocationID { get; set; }
        public virtual Location Location { get; set; }
        public string SpecificLocation { get; set; }
        public string Remark { get; set; }

    }
}
