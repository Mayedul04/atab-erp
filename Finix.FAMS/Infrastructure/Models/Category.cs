﻿using Finix.Auth.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
    [Table("AssetCategory")]
   public class Category : Entity
    {
        public string CategoryName { get; set; }
        public long? ParentID { get; set; }
        [ForeignKey("ParentID")]
        public virtual Category Categories { get; set; }
        public CategoryLevel Level { get; set; }
        public string Code { get; set; }
        public string Details { get; set; }
        public virtual ICollection<AccountsMapping> AccountsMappings { get; set; }

    }
}
