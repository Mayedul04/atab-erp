﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Infrastructure.Models
{
   public class DepreciationLog : Entity
    {
        public long AssetID { get; set; }
        [ForeignKey("AssetID")]
        public virtual Asset Asset { get; set; }
        public double OpeningValue { get; set; }
        public double DepreciatedAmount { get; set; }
        public DepriciationModel? DepriciationModel { get; set; }
        public PointOfDepreciation? PointOfDepreciation { get; set; }
        public Double WrittenDownValue { get; set; }
        public double RevaluationSurplus { get; set; }
        public double DisposedValue { get; set; }
        public int YearRemaining { get; set; }
        public DateTime CalculatedDate { get; set; }
    }
}
