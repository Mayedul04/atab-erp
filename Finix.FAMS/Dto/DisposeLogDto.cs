﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
    public class DisposeLogDto
    {
        public long? Id { get; set; }
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public double AccumulatedDepreciation { get; set; }
        public double BaseValue { get; set; }
        public double ResaleValue { get; set; }
        public string ResaleTo { get; set; }
        public double DisposedValue { get; set; }
        public double GainOfDisposal { get; set; }
        public double LossOfDisposal { get; set; }
        public DateTime DisposalDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
