﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
  public  class CategoryDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string CategoryName { get; set; }
        public string Details { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public CategoryLevel Level { get; set; }
        public string LevelName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
