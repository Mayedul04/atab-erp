﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Finix.FAMS.Infrastructure.Models;

namespace Finix.FAMS.DTO
{
    public class GradeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<Designation> DesignationList { get; set; }
    }

   


}
