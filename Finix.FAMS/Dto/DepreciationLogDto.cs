﻿using Finix.FAMS.Infrastructure;
using System;


namespace Finix.FAMS.Dto
{
   public class DepreciationLogDto
    {
        public long Id { get; set; }
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public double BaseValue { get; set; }
        public double OpeningValue { get; set; }
        public double DepreciatedAmount { get; set; }
        public double RevaluationSurplus { get; set; }
        public double DisposalValue { get; set; }
        public DepriciationModel DepriciationModel { get; set; }
        public string DepreciationModelName { get; set; }
        public PointOfDepreciation PointOfDepreciation { get; set; }
        public string CauseOfDepreciation { get; set; }
        public Double WrittenDownValue { get; set; }
        public int YearRemaining { get; set; }
        public DateTime CalculatedDate { get; set; }
        public string CalculatedDateString { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
