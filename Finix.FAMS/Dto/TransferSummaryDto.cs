﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
  public  class TransferSummaryDto
    {
        public long ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string SourceDetails { get; set; }
        public string DestinationDetails { get; set; }
        public int AssetCount { get; set; }
    }
}
