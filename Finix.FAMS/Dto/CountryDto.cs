﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
    public class CountryDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
    }
}
