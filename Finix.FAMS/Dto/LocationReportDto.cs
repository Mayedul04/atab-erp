﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class LocationReportDto
    {
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public long? CustodianID { get; set; }
        public string EmployeeName { get; set; }
        public long? LocationID { get; set; }
        public string LocationName { get; set; }
        public long? ParentLocID { get; set; }
        public string ParentLocationName { get; set; }
        public LocationTier Level { get; set; }
        
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }

    }
}
