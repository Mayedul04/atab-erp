﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class AssetDto
    {
        public long Id { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public long? SupplierID { get; set; }
        public string SupplierName { get; set; }
        public long AssetCategoryID { get; set; }
        public string CategoryName { get; set; }
        //public int DepreciationModel { get; set; }
        public DepriciationModel DepriciationModel { get; set; }
        public string ModelName { get; set; }
        public double OpeningDepreciation { get; set; }
        public double DepriciationRate { get; set; }
        public int UsefulLife { get; set; }
        public string VoucherCode { get; set; }
        public string Description { get; set; }
        public double BaseValue { get; set; }
        public double Tax { get; set; }
        public double VAT { get; set; }
        public double TotalValue { get; set; }
        public string Location { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string PurchaseDateText { get; set; }
        //public int ServiceType { get; set; }
        public ServiceType ServiceTypes { get; set; }
        public string ServiceTypeName { get; set; }
        public int ServicePeriod { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public bool Pass { get; set; }

    }
}
