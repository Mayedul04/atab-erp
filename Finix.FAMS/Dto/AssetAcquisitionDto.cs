﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class AssetAcquisitionDto
    {
        public long Id { get; set; }
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public long? CustodianID { get; set; }
        public string EmployeeName { get; set; }
        public long? LocationID { get; set; }
        public string LocationName { get; set; }
        public long CompanyID { get; set; }
        public string CompanyName { get; set; }
        public long OfficeID { get; set; }
        public string OfficeName { get; set; }
        public long? BuildingID { get; set; }
        public string BuildingName { get; set; }
        public long? FloorID { get; set; }
        public string FloorName { get; set; }
        public long? RoomID { get; set; }
        public string RoomName { get; set; }
        public string SpecificLocation { get; set; }
        public string Remark { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
        public bool Pass { get; set; }
    }
}
