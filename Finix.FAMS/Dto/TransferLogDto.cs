﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
  public  class TransferLogDto
    {
        public long Id { get; set; }
        public TransferType TransferType { get; set; }
        public string TransferTypeName { get; set; }
        public long? SourceEmployeeID { get; set; }
        public long? SourceID { get; set; }
        public string SourceTitle { get; set; }
        public long? DestinationEmployeeID { get; set; }
        public long? DestinationID { get; set; }
        public string DestinationTitle { get; set; }
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public string SourceSpecificLocation { get; set; }
        public string DestinationSpecificLocation { get; set; }
        public string TransferDetails { get; set; }
        public DateTime TransferDate { get; set; }
        public string TransferDateText { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus? Status { get; set; }
        public bool Pass { get; set; }
        public string SourceDetails { get; set; }
        public string DestinationDetails { get; set; }
    }
}
