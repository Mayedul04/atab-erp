﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
  public  class LocationSummaryDto
    {
        public long? LocationID { get; set; }
        public string LocationName { get; set; }
        public long? ParentLocID { get; set; }
        public string ParentLocationName { get; set; }
        public LocationTier Level { get; set; }
        public string LevelName { get; set; }
        public int AssetCount { get; set; }
    }
}
