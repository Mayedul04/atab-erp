﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
  public  class AccountsMappingDto
    {
        public long ID { get; set; }
        public long? RefId { get; set; }
        public string CategoryName { get; set; }
        public string AccountHeadCode { get; set; }
        public AccountHeadRefType RefType { get; set; }
        public string AccountHeadName { get; set; }
        public int DepricationType { get; set; }
        public string DepricationTypeName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
