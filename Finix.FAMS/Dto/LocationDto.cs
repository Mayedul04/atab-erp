﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class LocationDto
    {
        public long Id { get; set; }
        public long? OfficeId { get; set; }
        public string LocationTitle { get; set; }
        public string Details { get; set; }
        public long? ParentID { get; set; }
        public string ParentName { get; set; }
        public LocationTier Level { get; set; }
        public string LevelName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public long? CreatedBy { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}
