﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class AssetStockDto
    {
       public long CategoryID { get; set; }
       public string CategoryName { get; set; }
       public long ParentID { get; set; }
       public string ParentName { get; set; }
       public long AssetCount { get; set; }
       public double TotalWDV { get; set; }
    }
}
