﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{
   public class CategorySummaryDto
    {
        public long CategoryID { get; set; }
        public long CategoryParentID { get; set; }
        public string CategoryParentName { get; set; }
        public string CategoryName { get; set; }
        public double OpeningValue { get; set; }
        public double AdditionalValue { get; set; }
        public double ClosingValue { get; set; }
        public double Rate { get; set; }
        public double OpeningDepreciation { get; set; }
        public double AdjustedDepreciation { get; set; }
        public double ClosingDepreciation { get; set; }
        public double RevaluationSurplus { get; set; }
        public double DisposedValue { get; set; }
        public double WrittenDownValue { get; set; }
        public DateTime FromDate { get; set; }
        public string FromDateText { get; set; }
        public DateTime EndDate { get; set; }
        public string EndDateText { get; set; }
    }
}
