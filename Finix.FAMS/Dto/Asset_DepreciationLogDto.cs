﻿using Finix.FAMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finix.FAMS.Dto
{

    //To Keep the Assets Info and Logs Together
  public  class Asset_DepreciationLogDto
    {
        public long AssetCategoryID { get; set; }
        public string CategoryName { get; set; }
        public long AssetID { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public double BaseValue { get; set; }
        public DepriciationModel DepriciationModel { get; set; }
        public string ModelName { get; set; }
        public double DepriciationRate { get; set; }
        public double OpeningValue { get; set; }
        public double AdditionalValue { get; set; }
        public double ClosingValue { get; set; }
        public double DepreciatedAmount { get; set; }
        public Double AdjustedDepreciation { get; set; }
        public double ClosingDepreciation { get; set; }
        public PointOfDepreciation? PointOfDepreciation { get; set; }
        public Double WrittenDownValue { get; set; }
        public double RevaluationSurplus { get; set; }
        public double DisposedValue { get; set; }
        public int YearRemaining { get; set; }
        public DateTime CalculatedDate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool Pass { get; set; }
    }
}
