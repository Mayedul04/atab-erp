﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finix.Auth.DTO;
using Finix.Auth.Infrastructure;
using Finix.Auth.Service;
using AutoMapper;
using System.Transactions;

namespace Finix.Auth.Facade
{
    public class CompanyProfileFacade : BaseFacade
    {
        private readonly GenService _service = new GenService();

        public void SaveOffice(long id,string name,long? parentId,long userId,long officeId)
        {
            OfficeProfile office;
            office = GenService.GetAll<OfficeProfile>().Where(s => s.OfficeId == id).FirstOrDefault();

            if(office !=  null)
            {
                office.OfficeId = id;
                office.Name = name;
                office.ParentId = parentId;
                office.Status = EntityStatus.Active;
                office.CreateDate = office.CreateDate;
                office.CreatedBy = office.CreatedBy;
                office.SystemDate = DateToday(officeId);
                office.EditedBy = userId;
                office.EditDate = DateTime.Now;
                office.CompanyType = CompanyType.Company;
                office.EmployeeNo = 1;
                office.FiscalYear = DateTime.Now;
                GenService.Save(office);
            }
            else
            {
                office = new OfficeProfile();
                office.OfficeId = id;
                office.Name = name;
                office.ParentId = parentId;
                office.Status = EntityStatus.Active;
                office.CreateDate = DateTime.Now;
                office.CreatedBy = userId;
                office.SystemDate = DateToday(officeId);
                office.CompanyType = CompanyType.Company;
                office.EmployeeNo = 1;
                office.FiscalYear = DateTime.Now;
                GenService.Save(office);
            }
          
            GenService.SaveChanges();
            //office.
        }

        public ResponseDto SaveCompanyProfile(OfficeProfileDto companyDto, long? userId)
        {
            var response = new ResponseDto();
            OfficeProfile entity;

            if (companyDto.Id > 0)
            {
                entity = _service.GetById<OfficeProfile>(companyDto.Id);
                entity.EditDate = DateTime.Now;
                if (userId != null)
                    entity.EditedBy = userId;

                entity.Name = companyDto.Name;
                entity.Address = companyDto.Address;
                entity.PhoneNo = companyDto.PhoneNo;
                entity.Email = companyDto.Email;
                entity.Fax = companyDto.Fax;
                entity.ContactPerson = companyDto.ContactPerson;
                entity.CompanyType = companyDto.CompanyType;
                //entity.CompanyTypeName = companyDto.CompanyTypeName;
                entity.ParentId = companyDto.ParentId;
                //entity.ParentName 
            }
            else
            {
                entity = Mapper.Map<OfficeProfile>(companyDto);
                entity.CreateDate = DateTime.Now;
                if (userId != null)
                    entity.CreatedBy = userId;

            }
            try
            {
                _service.Save(entity);
                response.Success = true;
            }
            catch (Exception)
            {

            }
            return response;
        }
        public string GetUpdateBillNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nBill = Convert.ToDecimal(cp.BillNo.Trim()) + 1;
                cp.BillNo = "" + nBill;
                _service.Save(cp);
                return "BN-" + nBill;
            }
            catch (Exception)
            {

                throw;
            }


        }

        public string GetUpdateInvoiceNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nIv = Convert.ToDecimal(cp.InvoiceNo.Trim()) + 1;
                cp.InvoiceNo = "" + nIv;
                _service.Save(cp);
                return "CIV-" + nIv;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public string GetUpdateCr()
        //{
        //    try
        //    {
        //        var cp = _service.GetById<CompanyProfile>(1);
        //        var nCr = Convert.ToDecimal(cp.InvoiceNo.Trim()) + 1;
        //        cp.InvoiceNo = "" + nCr;
        //        _service.Save(cp);
        //        return "CV-" + nCr;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        //public string GetUpdateJv()
        //{
        //    try
        //    {
        //        var cp = _service.GetById<CompanyProfile>(1);
        //        var nJv = Convert.ToDecimal(cp.InvoiceNo.Trim()) + 1;
        //        cp.InvoiceNo = "" + nJv;
        //        _service.Save(cp);
        //        return "JV-" + nJv + "-" + DateTime.Now.Year.ToString();
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        public long GetEmployeeCode()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                long nVn = 0;
                if (cp != null)
                {
                    if (cp.EmployeeNo == 0)
                    {
                        nVn = 000000001;
                        cp.EmployeeNo = nVn;
                        _service.Save(cp);
                    }
                    else
                    {
                        nVn = cp.EmployeeNo + 1;
                        cp.EmployeeNo = nVn;
                        _service.Save(cp);
                    }

                }

                return nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string GetUpdateVoucherNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.VoucherNo.Trim()) + 1;
                cp.VoucherNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUpdateCDvNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.CDvNo.Trim()) + 1;
                cp.CDvNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUpdateBDvNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.BDvNo.Trim()) + 1;
                cp.BDvNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUpdateCCvNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.CCvNo.Trim()) + 1;
                cp.CCvNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUpdateBCvNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.BCvNo.Trim()) + 1;
                cp.BCvNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUpdateJvNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nVn = Convert.ToDecimal(cp.JvNo.Trim()) + 1;
                cp.JvNo = "" + nVn;
                _service.Save(cp);
                return "" + DateTime.Now.Year + "-" + nVn;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public string GetUpdateInvoiceNo()
        //{
        //    try
        //    {
        //        var cp = _service.GetById<CompanyProfile>(1);
        //        var nInv = Convert.ToDecimal(cp.InvoiceNo.Trim()) + 1;
        //        cp.InvoiceNo = "" + nInv;
        //        _service.Save(cp);
        //        return "INV-" + nInv;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public string GetUpdatedChalanNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var nCln = Convert.ToDecimal(cp.ChallanNo.Trim()) + 1;
                cp.ChallanNo = "" + nCln;
                _service.Save(cp);
                return "CLN-" + nCln;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetUpdatedRequisitionNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                decimal nCln = 0;
                if (cp == null)
                {
                    nCln = 00000000;
                }
                nCln = Convert.ToDecimal(cp.RequisitionNo.Trim()) + 1;
                cp.RequisitionNo = "" + nCln;
                _service.Save(cp);
                return "RQN-" + nCln;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetUpdatedPreProdNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var prdno = Convert.ToDecimal(cp.PreProdNo.Trim()) + 1;
                cp.PreProdNo = "" + prdno;
                _service.Save(cp);
                return "PPDN-" + prdno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetUpdatedProdNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var prdno = Convert.ToDecimal(cp.ProdNo.Trim()) + 1;
                cp.ProdNo = "" + prdno;
                _service.Save(cp);
                return "PDN-" + prdno;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetUpdatedOrderNo()
        {
            try
            {
                var cp = _service.GetById<OfficeProfile>(1);
                var prdno = Convert.ToDecimal(cp.OrderNo.Trim()) + 1;
                cp.OrderNo = "" + prdno;
                _service.Save(cp);
                return "SO-" + prdno;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResponseDto UpdateClosingDate(List<long> companyIds)
        {
            ResponseDto response = new ResponseDto();
            var model = _service.GetAll<OfficeProfile>().Where(o => companyIds.Contains(o.Id)).ToList();
            var entity = new OfficeProfile();
            using (var tran = new TransactionScope())
            {
                try
                {
                    model.ForEach(m => m.SystemDate = m.SystemDate.AddDays(1));
                    _service.Save(model);
                    response.Success = true;
                    response.Message = "Date Update Successfully";
                    tran.Complete();
                }
                catch (Exception)
                {
                    response.Message = "Date Updated Failed";
                    tran.Dispose();
                    //return false;
                }
            }
            return response;
        }


        public DateTime DateToday(long? CompanyId)
        {
            DateTime SystemDate;
            if (CompanyId != null && CompanyId > 0)
            {
                var company = _service.GetById<OfficeProfile>((long)CompanyId);
                if (company != null)
                    SystemDate = company.SystemDate;
                else
                    SystemDate = DateTime.MinValue;
            }
            else
            {
                SystemDate = _service.GetAll<OfficeProfile>().Select(r => r.SystemDate).FirstOrDefault();
            }
            return SystemDate;
        }

        public DateTime GetCurrentFiscalYear()
        {
            return _service.GetById<OfficeProfile>(1).FiscalYear;
        }
        public DateTime GetCurrentFiscalYear(long officeId)
        {
            return _service.GetById<OfficeProfile>((long)officeId).FiscalYear;
        }

        public List<OfficeProfileDto> GetAllCompanyProfiles()
        {
            return Mapper.Map<List<OfficeProfileDto>>(_service.GetAll<OfficeProfile>().ToList());
        }

        public List<OfficeProfileDto> GetAllActiveCompanyProfiles()
        {
            var temp = Mapper.Map<List<OfficeProfileDto>>(_service.GetAll<OfficeProfile>().Where(c => c.Status == EntityStatus.Active).ToList());
            return Mapper.Map<List<OfficeProfileDto>>(_service.GetAll<OfficeProfile>().Where(c => c.Status == EntityStatus.Active).ToList());
        }

        public OfficeProfileDto GetCompanyProfileById(long CompanyProfileId)
        {
            return Mapper.Map<OfficeProfileDto>(_service.GetById<OfficeProfile>(CompanyProfileId));
        }

        public List<OfficeProfileDto> GetAccessbleSubCompaniesAndProjects(long UserId, long OfficeId)
        {
            var list = new List<OfficeProfileDto>();
            var directPermissions = _service.GetAll<UserOfficeApplication>().Where(u => u.UserId == UserId && u.Status == EntityStatus.Active).ToList();
            if (directPermissions.Where(d => d.OfficeId == OfficeId).Any())
                list.Add(Mapper.Map<OfficeProfileDto>(directPermissions.Where(d => d.OfficeId == OfficeId).FirstOrDefault().CompanyProfile));
            var filteredPermissions = FilterSubPremissions(directPermissions.Select(d => d.OfficeId).ToList(), OfficeId);
            if (filteredPermissions != null)
                list.AddRange(filteredPermissions);
            return list;
        }

        public List<OfficeProfileDto> GetAllSubCompaniesAndProjects(long CompanyId)
        {
            var list = new List<OfficeProfileDto>();
            var subCompanies = _service.GetAll<OfficeProfile>().Where(c => c.ParentId == CompanyId).ToList();
            if (subCompanies != null && subCompanies.Count > 0)
            {
                list.AddRange(Mapper.Map<List<OfficeProfileDto>>(subCompanies));
                foreach (var company in subCompanies)
                {

                    list.AddRange(GetAllSubCompaniesAndProjects(company.Id));
                }
                return list;
            }
            else
                return new List<OfficeProfileDto>();
        }

        #region helpers
        private List<OfficeProfileDto> FilterSubPremissions(List<long> companyPermissions, long CurrentCompany)
        {
            var result = new List<OfficeProfileDto>();
            var subCompanies = _service.GetAll<OfficeProfile>().Where(c => c.ParentId == CurrentCompany).ToList();
            if (subCompanies == null || subCompanies.Count < 1)
                return null;
            var permissions = subCompanies.Where(s => companyPermissions.Contains(s.Id)).ToList();
            result.AddRange(Mapper.Map<List<OfficeProfileDto>>(permissions));


            foreach (var company in subCompanies)
            {
                var permission = FilterSubPremissions(companyPermissions, company.Id);
                if (permission != null && permission.Count > 0)
                    result.AddRange(permission);
            }
            return result;
        }
        #endregion

        public List<OfficeProfileDto> GetOfficeProfile()
        {
            var data = GenService.GetAll<OfficeProfile>().Where(r => r.CompanyType != CompanyType.Project).ToList();
            return Mapper.Map<List<OfficeProfileDto>>(data);
        }

        public ResponseDto SaveOfficeProfile(OfficeProfileDto dto, long userId)
        {
            ResponseDto response = new ResponseDto();
            //var empId = _user.GetEmployeeIdByUserId(userId);
            try
            {
                if (dto.Id > 0)
                {
                    var prev = GenService.GetById<OfficeProfile>(dto.Id);
                    dto.CreateDate = prev.CreateDate;
                    dto.FiscalYear = prev.FiscalYear;
                    dto.CreatedBy = prev.CreatedBy;
                    dto.Status = prev.Status;
                    dto.SystemDate = prev.SystemDate;
                    dto.EditDate = DateTime.Now;
                    dto.ParentId = prev.ParentId;
                    prev = Mapper.Map(dto, prev);
                    GenService.Save(prev);
                    response.Success = true;
                    response.Message = "Project Edited Successfully";
                }
                else
                {
                    var office = GenService.GetAll<OfficeProfile>().OrderByDescending(r => r.Id);
                    var data = Mapper.Map<OfficeProfile>(dto);
                    if (office.Any())
                    {
                        var firstOrDefault = office.FirstOrDefault();
                        if (firstOrDefault != null) data.SystemDate = firstOrDefault.SystemDate;
                    }
                    else
                    {
                        data.SystemDate = DateTime.Now;
                    }
                    //data.EmpId = empId;
                    //SystemDate
                    data.FiscalYear = DateTime.Now;
                    data.VoucherNo =
                        data.JvNo = "00000000";
                    data.CDvNo = "00000000";
                    data.BDvNo = "00000000";
                    data.CCvNo = "00000000";
                    data.BCvNo = "00000000";
                    data.BillNo = "00000000";
                    data.ChallanNo = "00000000";
                    data.InvoiceNo = "00000000";
                    data.PreProdNo = "00000000";
                    data.ProdNo = "00000000";
                    data.Status = EntityStatus.Active;
                    data.CreatedBy = userId;
                    data.CreateDate = DateTime.Now;
                    GenService.Save(data);
                    response.Id = data.Id;
                    response.Success = true;
                    response.Message = "Project Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                response.Message = "Error Message : " + ex;
            }
            return response;
        }

        public List<OfficeProfileDto> GetAllOfficeProfile()
        {
            var data = GenService.GetAll<OfficeProfile>();
            return Mapper.Map<List<OfficeProfileDto>>(data);
        }
        public ResponseDto TakeDBBackup()
        {
            var response = new ResponseDto();
            var authDbBackup = DBBackupAuth();
            var mainDbBackup = DBBackupCardiac();

            response.Success = true;
            if (authDbBackup && mainDbBackup)
                response.Message = "Database backup successful";
            else if (authDbBackup || mainDbBackup)
                response.Message = "Database backup successful with error";
            else
                response.Message = "Database backup failed";

            return response;
        }
        public DateTime LastClosingDate(long? officeId)
        {
            DateTime closingDate;
            if (officeId != null)
                closingDate = _service.GetById<OfficeProfile>((long)officeId).SystemDate;
            else
                closingDate = _service.GetAll<OfficeProfile>().Select(c => c.SystemDate).FirstOrDefault();
            return closingDate;
        }
        public List<long> GetDependentIds(long officeId)
        {
            var projects = _service.GetAll<OfficeProfile>().Where(l => l.ParentId == officeId && l.CompanyType == CompanyType.Project);
            if (projects != null && projects.Count() > 0)
                return projects.Select(p => p.Id).ToList();
            return null;
        }
    }
}
