﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finix.Auth.Infrastructure;

namespace Finix.Auth.DTO
{
    public class UserCompanyApplicationDto
    {
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public long? CompanyProfileId { get; set; }
        public string CompanyProfileName { get; set; }
        public List<ApplicationDto> Applications { get; set; }
    }

    public class UserOfficeApplicationDto
    {
        public long? Id { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long OfficeId { get; set; }
        public string OfficeName { get; set; }
        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditDate { get; set; }
        public long? EditedBy { get; set; }
        public EntityStatus Status { get; set; }
    }
}